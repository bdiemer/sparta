/*************************************************************************************************
 *
 * This unit merges the catalog data with SPARTA splashback data.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_MERGE_RSP_H_
#define _MORIA_MERGE_RSP_H_

#include "moria_global.h"
#include "moria_config.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void mergeAnalysisRsp(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		int halo_idx);

#endif

