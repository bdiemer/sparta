/*************************************************************************************************
 *
 * Functions to output various statistics about moria and the halo population.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <time.h>

#include "moria_statistics.h"
#include "../../src/memory.h"
#include "../../src/halos/halo.h"
#include "../../src/halos/halo_definitions.h"
#include "../../src/analyses/analysis_rsp.h"

/*************************************************************************************************
 * INTERNAL CONSTANTS
 *************************************************************************************************/

#define ENUM2(x, y) #y,
const char *MORIA_T0_NAMES[MORIA_T0_N] =
	{MORIA_TIMERS0};
const char *MORIA_T1_NAMES[MORIA_T1_N] =
	{MORIA_TIMERS1};
#undef ENUM2

#define N_MAX_STATUS 8

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void printSubstatusDiff(MoriaConfig *moria_cfg, HaloSet *hs, int idx_def_sub, int h_idx);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initTimers(MoriaTimers *timers)
{
	int i;

	for (i = 0; i < MORIA_T0_N; i++)
	{
		timers->timers_0[i] = 0.0;
	}
	for (i = 0; i < MORIA_T1_N; i++)
	{
		timers->timers_1[i] = 0.0;
		timers->timers_1_cum[i] = 0.0;
	}
}

void resetTimersSnapshot(MoriaTimers *timers)
{
	int i;

	for (i = 0; i < MORIA_T1_N; i++)
	{
		timers->timers_1_cum[i] += timers->timers_1[i];
		timers->timers_1[i] = 0.0;
	}
}

/*
 * Print statistics about changes in population. Possibilities are:
 *
 * host -> host
 * host -> sub
 * sub  -> host
 * sub  -> sub (same host)
 * sub  -> sub (different host)
 */
void printHaloStatistics(MoriaConfig *moria_cfg, SpartaData *sd, HaloSet *hs)
{
	int i, j, n_valid_def, n_hh, n_hs, n_sh, n_ss_same, n_ss_diff, n_is_sub, n_hst, n_sub,
			n_changed, status_counter[N_MAX_STATUS], status_counter_hst[N_MAX_STATUS],
			status_counter_sub[N_MAX_STATUS], status_counter_moria[N_MAX_STATUS],
			status_counter_moria_hst[N_MAX_STATUS], status_counter_moria_sub[N_MAX_STATUS],
			status_counter_moria_all[3];
	int_least8_t status, *mask_hostsub;
	float pfac_cat, pfac_all, pfac_all_hst, pfac_all_sub, pfac_spa, pfac_spa_hst, pfac_spa_sub,
			pfac_moria, pfac_moria_hst, pfac_moria_sub;
	char cut_q_str[20];
	HaloDefinition *def;

	/*
	 * Generate general host/sub mask
	 */
	n_sub = 0;
	mask_hostsub = (int_least8_t*) memAlloc(__FFL__, MID_MORIA_TMP,
			sizeof(int_least8_t) * hs->n_tot);
	for (i = 0; i < hs->n_tot; i++)
	{
		if (!hs->out_mask[i])
		{
			mask_hostsub[i] = -1;
			continue;
		}

		if (hs->hd[i].cat_upid == UPID_HOST)
		{
			mask_hostsub[i] = 0;
		} else
		{
			mask_hostsub[i] = 1;
			n_sub++;
		}
	}
	n_hst = hs->n_out_cat - n_sub;

	pfac_cat = 100.0 / (float) hs->n_cat;
	pfac_all = 100.0 / (float) hs->n_out_cat;
	pfac_all_hst = 100.0 / (float) n_hst;
	pfac_all_sub = 100.0 / (float) n_sub;

	/*
	 * Output information about the sample in general, not specific to any definition
	 */
	haloDefinitionToString(moria_cfg->cut_def, cut_q_str);
	output(0,
			"Found %d halos in catalog, %d (%.1f%%) pass %s cut, out of those %.1f%% hosts and %.1f%% subs.\n",
			hs->n_cat, hs->n_out_cat, cut_q_str, pfac_cat * hs->n_out_cat, pfac_all * n_hst,
			pfac_all * n_sub);

	/*
	 * Output information about the completeness of the SPARTA data. This is definition
	 * specific because the different masses pick different halos to keep.
	 */
	for (i = 0; i < N_MAX_STATUS; i++)
		status_counter[i] = 0;

	for (i = 0; i < hs->n_tot; i++)
	{
		if (!hs->out_mask[i])
			continue;

		switch (hs->hd[i].status_sparta)
		{
		case HALO_STATUS_NOT_FOUND:
			status_counter[0]++;
			break;
		case HALO_STATUS_HOST:
			status_counter[1]++;
			if (mask_hostsub[i] != 0)
				error(__FFL__,
						"Found sub status from catalog but host from SPARTA (status %d, cat pid %ld).\n",
						hs->hd[i].status_sparta, hs->hd[i].cat_upid);
			break;
		case HALO_STATUS_SUB:
		case HALO_STATUS_BECOMING_SUB:
		case HALO_STATUS_BECOMING_HOST:
		case HALO_STATUS_BECOMING_SUB_HOST:
		case HALO_STATUS_SWITCHED_HOST:
			status_counter[2]++;
			if (mask_hostsub[i] != 1)
				error(__FFL__,
						"Found host status from catalog but sub from SPARTA (status %d, cat pid %ld).\n",
						hs->hd[i].status_sparta, hs->hd[i].cat_upid);
			break;
		default:
			error(__FFL__, "Unknown halo status, %d.\n", hs->hd[i].status_sparta);
			break;
		}
	}
	output(1, "Sparta status:                  N/A      Host       Sub\n");
	output(1, "  Fraction:                  %5.1f%%    %5.1f%%    %5.1f%%\n",
			pfac_all * status_counter[0], pfac_all * status_counter[1],
			pfac_all * status_counter[2]);

	/*
	 * We define fractions of the SPARTA halo samples against all halos except those that have
	 * status NOT_FOUND, since they never existed as far as MORIA is concerned.
	 */
	pfac_spa = 100.0 / (float) (hs->n_out_cat - status_counter[0]);
	pfac_spa_hst = 100.0 / (float) (status_counter[1]);
	pfac_spa_sub = 100.0 / (float) (status_counter[2]);

	/*
	 * Output acc rate status
	 */
	for (i = 0; i < N_MAX_STATUS; i++)
	{
		status_counter[i] = 0;
		status_counter_hst[i] = 0;
		status_counter_sub[i] = 0;
	}
	for (i = 0; i < hs->n_tot; i++)
	{
		if (!hs->out_mask[i])
			continue;

		status = hs->hd[i].status_acc_rate;
		status_counter[status]++;
		if (mask_hostsub[i] == 0)
			status_counter_hst[status]++;
		else if (mask_hostsub[i] == 1)
			status_counter_sub[status]++;
		else
			error(__FFL__, "Unknown host/sub identifier, %d.\n", mask_hostsub[i]);
	}
	if (status_counter[0] > 0)
		error(__FFL__, "Found undefined mass accretion rate status, internal error.\n");
	output(1,
			"Mass acc. rate status:           OK rdcd-tdyn     model    sub-OK  sub-rdcd sub-model sb-mdl-nw\n");
	output(1,
			"  Fraction (all):            %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%\n",
			pfac_all * status_counter[1], pfac_all * status_counter[2],
			pfac_all * status_counter[3], pfac_all * status_counter[4],
			pfac_all * status_counter[5], pfac_all * status_counter[6],
			pfac_all * status_counter[7]);
	output(1,
			"  Fraction (hosts):          %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%\n",
			pfac_all_hst * status_counter_hst[1], pfac_all_hst * status_counter_hst[2],
			pfac_all_hst * status_counter_hst[3], pfac_all_hst * status_counter_hst[4],
			pfac_all_hst * status_counter_hst[5], pfac_all_hst * status_counter_hst[6],
			pfac_all_hst * status_counter_hst[7]);
	output(1,
			"  Fraction (subs):           %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%\n",
			pfac_all_sub * status_counter_sub[1], pfac_all_sub * status_counter_sub[2],
			pfac_all_sub * status_counter_sub[3], pfac_all_sub * status_counter_sub[4],
			pfac_all_sub * status_counter_sub[5], pfac_all_sub * status_counter_sub[6],
			pfac_all_sub * status_counter_sub[7]);

	/*
	 * Analysis status for rsp
	 */
	for (i = 0; i < N_MAX_STATUS; i++)
	{
		status_counter[i] = 0;
		status_counter_hst[i] = 0;
		status_counter_sub[i] = 0;

		status_counter_moria[i] = 0;
		status_counter_moria_hst[i] = 0;
		status_counter_moria_sub[i] = 0;
	}
	for (i = 0; i < 3; i++)
		status_counter_moria_all[i] = 0;

	// Count statuses
	for (i = 0; i < hs->n_tot; i++)
	{
		if (!hs->out_mask[i])
			continue;

		// If not valid in SPARTA at all, do not count status
		if (hs->hd[i].status_sparta == HALO_STATUS_NOT_FOUND)
			continue;

		status = hs->hd[i].status_sparta_rsp;
		status_counter[status]++;
		if (mask_hostsub[i] == 0)
			status_counter_hst[status]++;
		else if (mask_hostsub[i] == 1)
			status_counter_sub[status]++;
		else
			error(__FFL__, "Unknown host/sub identifier, %d.\n", mask_hostsub[i]);

		// If not success, let's also output what MORIA did to guess a value
		if (status != ANL_RSP_STATUS_SUCCESS)
		{
			status_counter_moria_all[0]++;

			status = hs->hd[i].status_moria_rsp;
			status_counter_moria[status]++;
			if (mask_hostsub[i] == 0)
			{
				status_counter_moria_all[1]++;
				status_counter_moria_hst[status]++;
			} else if (mask_hostsub[i] == 1)
			{
				status_counter_moria_all[2]++;
				status_counter_moria_sub[status]++;
			} else
			{
				error(__FFL__, "Unknown host/sub identifier, %d.\n", mask_hostsub[i]);
			}
		}
	}
	pfac_moria = 100.0 / (float) status_counter_moria_all[0];
	pfac_moria_hst = 100.0 / (float) status_counter_moria_all[1];
	pfac_moria_sub = 100.0 / (float) status_counter_moria_all[2];

	// Not-found status should not occur since we've ignored halos without a valid SPARTA output
	if (status_counter[ANALYSIS_STATUS_HALO_NOT_SAVED] > 0)
		error(__FFL__,
				"Found status ANALYSIS_STATUS_HALO_NOT_SAVED after ignoring halos without Sparta output.\n");

	// Print
	output(1,
			"Analysis rsp status:             OK   not_vld    no_anl  insf_evt  insf_wgt |  gs-model   gs-past   gs-ftre  gs-intrp\n");
	output(1,
			"  Fraction (all):            %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%% |    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%\n",
			pfac_spa * status_counter[1], pfac_spa * status_counter[2],
			pfac_spa * status_counter[4], pfac_spa * status_counter[5],
			pfac_spa * status_counter[6], pfac_moria * status_counter_moria[2],
			pfac_moria * status_counter_moria[3], pfac_moria * status_counter_moria[4],
			pfac_moria * status_counter_moria[5]);
	output(1,
			"  Fraction (hosts):          %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%% |    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%\n",
			pfac_spa_hst * status_counter_hst[1], pfac_spa_hst * status_counter_hst[2],
			pfac_spa_hst * status_counter_hst[4], pfac_spa_hst * status_counter_hst[5],
			pfac_spa_hst * status_counter_hst[6], pfac_moria_hst * status_counter_moria_hst[2],
			pfac_moria_hst * status_counter_moria_hst[3],
			pfac_moria_hst * status_counter_moria_hst[4],
			pfac_moria_hst * status_counter_moria_hst[5]);
	output(1,
			"  Fraction (subs):           %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%% |    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%\n",
			pfac_spa_sub * status_counter_sub[1], pfac_spa_sub * status_counter_sub[2],
			pfac_spa_sub * status_counter_sub[4], pfac_spa_sub * status_counter_sub[5],
			pfac_spa_sub * status_counter_sub[6], pfac_moria_sub * status_counter_moria_sub[2],
			pfac_moria_sub * status_counter_moria_sub[3],
			pfac_moria_sub * status_counter_moria_sub[4],
			pfac_moria_sub * status_counter_moria_sub[5]);

	/*
	 * Analysis status for hps
	 */
	output(1, "Analysis hps status:             OK    no_anl  so_small  so_large  oct_zero\n");
	for (j = 0; j < moria_cfg->n_defs_all; j++)
	{
		def = &(moria_cfg->all_defs[j]);
		if (!isSpartaHpsDefinition(def))
			continue;

		for (i = 0; i < N_MAX_STATUS; i++)
		{
			status_counter[i] = 0;
			status_counter_hst[i] = 0;
			status_counter_sub[i] = 0;
		}

		// Count statuses
		for (i = 0; i < hs->n_tot; i++)
		{
			if (!hs->out_mask[i])
				continue;

			// If not valid in SPARTA at all, do not count status
			if (hs->hd[i].status_sparta == HALO_STATUS_NOT_FOUND)
				continue;

			// Map to status + 2
			status = hs->hd_def_status[j][i] + 2;
			status_counter[status]++;
			if (mask_hostsub[i] == 0)
				status_counter_hst[status]++;
			else if (mask_hostsub[i] == 1)
				status_counter_sub[status]++;
			else
				error(__FFL__, "Unknown status identifier, %d.\n", mask_hostsub[i]);
		}

		// Not-found status should not occur since we've ignored halos without a valid SPARTA output
		if (status_counter[MORIA_STATUS_HALO_DOES_NOT_EXIST + 2] > 0)
		{
			for (i = 0; i < N_MAX_STATUS; i++)
				output(0, "Status counter %d = %d\n", i, status_counter[i]);
			error(__FFL__,
					"Found status ANALYSIS_STATUS_HALO_NOT_SAVED after ignoring halos without SPARTA output.\n");
		}

		// Print
		output(1, "  %-15s (all):     %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%\n",
				moria_cfg->all_defs_str[j], pfac_spa * status_counter[3],
				pfac_spa * status_counter[1], pfac_spa * status_counter[4],
				pfac_spa * status_counter[5], pfac_spa * status_counter[6]);
		output(1, "  %-15s (hosts):   %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%\n",
				moria_cfg->all_defs_str[j], pfac_spa_hst * status_counter_hst[3],
				pfac_spa_hst * status_counter_hst[1], pfac_spa_hst * status_counter_hst[4],
				pfac_spa_hst * status_counter_hst[5], pfac_spa_hst * status_counter_hst[6]);
		output(1, "  %-15s (subs):    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%    %5.1f%%\n",
				moria_cfg->all_defs_str[j], pfac_spa_sub * status_counter_sub[3],
				pfac_spa_sub * status_counter_sub[1], pfac_spa_sub * status_counter_sub[4],
				pfac_spa_sub * status_counter_sub[5], pfac_spa_sub * status_counter_sub[6]);
	}

	/*
	 * Output information about the host-sub relations
	 */
	output(1,
			"Definition                 subs  changed       host->host        host->sub        sub->host    sub->sub,same    sub->sub,diff\n");
	for (j = 0; j < moria_cfg->n_defs_sub; j++)
	{
		n_valid_def = 0;
		n_hh = 0;
		n_hs = 0;
		n_sh = 0;
		n_ss_same = 0;
		n_ss_diff = 0;
		for (i = 0; i < hs->n_tot; i++)
		{
			if (!hs->out_mask[i])
				continue;

			if (hs->hd_upid_defs[j][i] == UPID_NOT_SET)
				continue;

			if (hs->hd[i].cat_upid == UPID_HOST)
			{
				if (hs->hd_upid_defs[j][i] == UPID_HOST)
				{
					n_hh++;
				} else
				{
					n_hs++;
					printSubstatusDiff(moria_cfg, hs, j, i);
				}
			} else
			{
				if (hs->hd_upid_defs[j][i] == UPID_HOST)
				{
					n_sh++;
					printSubstatusDiff(moria_cfg, hs, j, i);
				} else
				{
					if (hs->hd_upid_defs[j][i] == hs->hd[i].cat_upid)
					{
						n_ss_same++;
					} else
					{
						n_ss_diff++;
						printSubstatusDiff(moria_cfg, hs, j, i);
					}
				}
			}
			n_valid_def++;
		}
		n_is_sub = n_hs + n_ss_same + n_ss_diff;
		n_changed = n_hs + n_sh + n_ss_diff;
		output(1,
				"  %-20s   %5.1f%%   %5.1f%% %7d (%5.1f%%) %7d (%5.1f%%) %7d (%5.1f%%) %7d (%5.1f%%) %7d (%5.1f%%)\n",
				moria_cfg->output_sub_defs_str[j], pfac_all * n_is_sub, pfac_all * n_changed, n_hh,
				pfac_all * n_hh, n_hs, pfac_all * n_hs, n_sh, pfac_all * n_sh, n_ss_same,
				pfac_all * n_ss_same, n_ss_diff, pfac_all * n_ss_diff);
	}

	/*
	 * Free memory
	 */
	memFree(__FFL__, MID_MORIA_TMP, mask_hostsub, sizeof(int_least8_t) * hs->n_tot);
}

/*
 * Print a detailed diff for halos that were assigned a different host/sub status by moria and
 * the original halo finder. We assume that the original halo finder works in the DEF_R200M_BOUND
 * definition.
 */
void printSubstatusDiff(MoriaConfig *moria_cfg, HaloSet *hs, int idx_def_sub, int h_idx)
{
	if (!moria_cfg->log_substatus_diff)
		return;

	int i, j, idx_def_all;
	double d, dd;
	char *def_str;

	idx_def_all = moria_cfg->output_sub_defs[idx_def_sub].idx;
	def_str = moria_cfg->output_sub_defs_str[idx_def_sub];

	output(0, "*** DIFF: def %-15s: Sub %9ld new upid %9ld old %9ld\n", def_str, hs->hd[h_idx].id,
			hs->hd_upid_defs[idx_def_sub][h_idx], hs->hd[h_idx].cat_upid);
	output(0, "    DIFF: def %-15s: Sub data status %2d xyz %6.3f %6.3f %6.3f  def %.2e\n", def_str,
			hs->hd[h_idx].status_sparta, hs->hd[h_idx].x[0], hs->hd[h_idx].x[1], hs->hd[h_idx].x[2],
			hs->hd_defs[idx_def_all][h_idx]);

	for (i = 0; i < hs->n_tot; i++)
	{
		if (!hs->out_mask[i])
			continue;

		if (hs->hd[i].id == hs->hd[h_idx].cat_upid)
		{
			dd = 0.0;
			for (j = 0; j < 3; j++)
			{
				d = hs->hd[i].x[j] - hs->hd[h_idx].x[j];
				dd += d * d;
			}
			dd = sqrt(dd) * 1000.0;
			output(0,
					"    DIFF: def %-15s: Old host status %2d xyz %6.3f %6.3f %6.3f  def %.2e  R %.2e  d %.2e\n",
					def_str, hs->hd[i].status_sparta, hs->hd[i].x[0], hs->hd[i].x[1],
					hs->hd[i].x[2], hs->hd_defs[idx_def_all][h_idx], hs->hd_defs[idx_def_all][i],
					dd);
		}
	}

	for (i = 0; i < hs->n_tot; i++)
	{
		if (!hs->out_mask[i])
			continue;

		if (hs->hd[i].id == hs->hd_upid_defs[idx_def_sub][h_idx])
		{
			dd = 0.0;
			for (j = 0; j < 3; j++)
			{
				d = hs->hd[i].x[j] - hs->hd[h_idx].x[j];
				dd += d * d;
			}
			dd = sqrt(dd) * 1000.0;
			output(0, "*** DIFF: def %-15s: New host xyz %6.3f %6.3f %6.3f  def %.2e  d %.2e\n",
					def_str, hs->hd[i].x[0], hs->hd[i].x[1], hs->hd[i].x[2],
					hs->hd_defs[idx_def_all][i], dd);
		}
	}
}

/*
 * The per-snapshot timing is always in seconds; that should suffice.
 */
void printMoriaTimingStatistics(MoriaConfig *moria_cfg, MoriaTimers *timers)
{
	int j;
	double pc_1_fac, t1_total, perc, time_unit;

	if (moria_cfg->log_level_timing == 0)
		return;
	printLine(0);

	time_unit = 1.0 / (double) CLOCKS_PER_SEC;
	t1_total = 0.0;
	for (j = 0; j < MORIA_T1_N; j++)
		t1_total += (double) timers->timers_1[j] * time_unit;
	pc_1_fac = 100.0 / t1_total;

	output(0, "Timing                          Time       Percentage\n");
	output(0, "------------------------------------------------------------\n");

	for (j = 0; j < MORIA_T1_N; j++)
	{
		perc = (double) timers->timers_1[j] * time_unit * pc_1_fac;
		if ((perc > TIMING_MIN_PRINT_FRAC * 100.0) || (moria_cfg->log_level_timing > 1))
			output(0, "%-25s     %6.2f      %5.1f\n", MORIA_T1_NAMES[j],
					(double) timers->timers_1[j] * time_unit, perc);

	}
	output(0, "------------------------------------------------------------\n");
	output(0, "%-25s     %6.2f      %5.1f\n", "Total", t1_total, 100.0);
}

/*
 * For the final timing, we switch to minutes if the time was too long.
 */
void printMoriaTimingStatisticsFinal(MoriaConfig *moria_cfg, MoriaTimers *timers)
{
	int i, j;
	double pc_0_fac, t0_total, perc, time_unit;
	char unit_str[4];

	time_unit = 1.0 / (double) CLOCKS_PER_SEC;
	t0_total = 0.0;
	for (i = 0; i < MORIA_T0_N; i++)
		t0_total += (double) timers->timers_0[i] * time_unit;

	if (t0_total > 999.0)
	{
		time_unit /= 60.0;
		t0_total /= 60.0;
		sprintf(unit_str, "%s", "min");
	} else
	{
		sprintf(unit_str, "%s", "sec");
	}
	pc_0_fac = 100.0 / t0_total;

	output(0, "Timing (%s)                    Time       Percentage\n", unit_str);
	output(0, "------------------------------------------------------------\n");

	for (i = 0; i < MORIA_T0_N; i++)
	{
		perc = (double) timers->timers_0[i] * time_unit * pc_0_fac;
		output(0, "%-25s     %6.2f      %5.1f\n", MORIA_T0_NAMES[i],
				(double) timers->timers_0[i] * time_unit, perc);

		if (i == MORIA_T0_SNAPS)
		{
			for (j = 0; j < MORIA_T1_N; j++)
			{
				perc = (double) timers->timers_1_cum[j] * time_unit * pc_0_fac;
				output(0, "   %-25s     %6.2f      %5.1f\n", MORIA_T1_NAMES[j],
						(double) timers->timers_1_cum[j] * time_unit, perc);
			}
		}
	}
	output(0, "------------------------------------------------------------\n");
	output(0, "%-25s     %6.2f      %5.1f\n", "Total", t0_total, 100.0);
}
