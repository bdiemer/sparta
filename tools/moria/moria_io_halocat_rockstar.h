/*************************************************************************************************
 *
 * I/O routines for Rockstar.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_IO_HALOCAT_ROCKSTAR_H_
#define _MORIA_IO_HALOCAT_ROCKSTAR_H_

#include "moria_global.h"
#include "moria_config.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

HaloSet* getHaloSetFromRockstar(MoriaConfig *moria_cfg, SnapshotData *snap, int n_halos_extra);
void readCatalogFieldsRockstar(MoriaConfig *moria_cfg, HaloSet *hs, int snap_idx);
void outputCatalogRockstar(MoriaConfig *moria_cfg, HaloSet *hs, int snap_idx);

#endif
