/*************************************************************************************************
 *
 * This unit merges the catalog data with SPARTA data.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_MERGE_H_
#define _MORIA_MERGE_H_

#include "moria_global.h"
#include "moria_config.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void findSpartaIndex(SpartaData *sd, MoriaSorter *sorters, HaloData *hd, int snap_idx);
void resolveStatusConflicts(MoriaConfig *moria_cfg, SpartaData *sd, HaloData *hd, int snap_idx);
void moriaProcessHalo(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		int halo_idx);

#endif
