/*************************************************************************************************
 *
 * This unit determines subhalo relations given a set of halo positions and radii.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "moria_hostsub.h"
#include "../../src/global_types.h"
#include "../../src/memory.h"
#include "../../src/tree.h"
#include "../../src/halos/halo.h"

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int compareHaloQ(const void *a, const void *b);
int excludeHaloFromHostSub(MoriaConfig *moria_cfg, HaloData *hd);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * This function computes the subhalo relations separately for each radius definition.
 */
void computeSubhaloRelations(MoriaConfig *moria_cfg, SnapshotData *snap, HaloSet *hs)
{
	int i, j, k, def_idx, sub_idx, host_idx, n_hostsub;
	Particle *p_sort, *p_tree;
	TreeResults *tr_res;
	Tree *tree;

	/*
	 * Set the number of halos for which we are computing host-sub relations. For example, we are
	 * not including ghosts in this calculation.
	 */
	n_hostsub = hs->n_cat;
	//n_hostsub = hs->n_tot;

	/*
	 * Allocate upid_defs for the first time. Note that those are allocated for all halos, even if
	 * they are not included in the hostsub routine. We will set those PIDs at the end.
	 */
	hs->hd_upid_defs = (long int**) memAlloc(__FFL__, MID_MORIA_HALOS,
			sizeof(long int*) * moria_cfg->n_defs_sub);
	for (j = 0; j < moria_cfg->n_defs_sub; j++)
	{
		hs->hd_upid_defs[j] = (long int*) memAlloc(__FFL__, MID_MORIA_HALOS,
				sizeof(long int) * hs->n_tot);
		memset(hs->hd_upid_defs[j], 0, sizeof(long int) * hs->n_tot);
	}

	/*
	 * Put the halos into an array of particles, since the tree unit uses particles. We
	 * re-purpose the x-velocity to contain the radius, the y-velocity to contain the original
	 * index of the halos, and the z-velocity to contain the field by which the halos are ordered.
	 * Some quantities remain the same for all halos, so we need to set them only once.
	 */
	p_sort = (Particle*) memAlloc(__FFL__, MID_MORIA_TMP, sizeof(Particle) * n_hostsub);
	p_tree = (Particle*) memAlloc(__FFL__, MID_MORIA_TMP, sizeof(Particle) * n_hostsub);
	for (i = 0; i < n_hostsub; i++)
	{
		p_sort[i].id = hs->hd[i].id;
		p_sort[i].x[0] = hs->hd[i].x[0];
		p_sort[i].x[1] = hs->hd[i].x[1];
		p_sort[i].x[2] = hs->hd[i].x[2];
		// Radius; will be set later as it depends on the definition
		p_sort[i].v[0] = 0.0;
		// Index
		p_sort[i].v[1] = (float) i;
		// Order quantity
		if (excludeHaloFromHostSub(moria_cfg, &(hs->hd[i])))
			p_sort[i].v[2] = 0.0;
		else
			p_sort[i].v[2] = hs->hd_defs[moria_cfg->order_def.idx][i];

#if MORIA_CAREFUL
		if ((p_sort[i].v[2] < 0.0) || isnan(p_sort[i].v[2]) || isinf(p_sort[i].v[2]))
			error(__FFL__,
					"Found negative/nan/inf order quantity %.2e, definition %s, def idx %d.\n",
					p_sort[i].v[2], moria_cfg->all_defs_str[moria_cfg->order_def.idx],
					moria_cfg->order_def.idx);
#endif

		debugHaloMoria(hs->hd[i].id,
				"computeSubhaloRelations() found halo, order quantity %s = %.2e.",
				moria_cfg->all_defs_str[moria_cfg->order_def.idx], p_sort[i].v[2]);
	}

	/*
	 * Sort halos and build tree. We have to be careful: building the tree changes the order of the
	 * array! Thus, we copy the entire array first.
	 */
	memcpy(p_tree, p_sort, sizeof(Particle) * n_hostsub);
	tree = treeInit(n_hostsub, p_tree);
	qsort(p_sort, n_hostsub, sizeof(Particle), &compareHaloQ);

	/*
	 * Run over all sub definitions. Each time we need to reset the radius, but the other quantities
	 * stay the same.
	 */
	for (j = 0; j < moria_cfg->n_defs_sub; j++)
	{
		/*
		 * We initially set all halos to host.
		 */
		for (i = 0; i < n_hostsub; i++)
			hs->hd_upid_defs[j][i] = UPID_HOST;

		/*
		 * If there are fewer than two halos, there is no need to run this function.
		 */
		if (n_hostsub < 2)
			return;

		/*
		 * We will need the index of this definition in the radius array. Notice that this is NOT
		 * the index in the upid_defs array, which simply runs to output_n_sub_defs.
		 */
		def_idx = moria_cfg->output_sub_defs[j].idx;

		/*
		 * Go through halos, starting with the largest (according to the order quantity), and find
		 * their subhalos. If we find subhalos, we check whether they already have a host ID (which
		 * we leave alone, since that halo must be larger). If not, we give them the ID of this
		 * halo, or it's a host.
		 */
		for (i = 0; i < n_hostsub - 1; i++)
		{
			/*
			 * The radius we use depends on the halo definition. Since the array is sorted, we need
			 * to revert back to the original array.
			 *
			 * If a halo is not being output and we want to exclude sub halos from the host-sub
			 * relations, we assign it a radius of zero.
			 */
			host_idx = (int) p_sort[i].v[1];

			if (excludeHaloFromHostSub(moria_cfg, &(hs->hd[host_idx])))
				p_sort[i].v[0] = 0.0;
			else
				p_sort[i].v[0] = hs->hd_defs[def_idx][host_idx] * snap->comoving_conversion;

#if MORIA_CAREFUL
			if (p_sort[i].v[0] < 0.0)
			{
				int def_status;
				long int upid_cat;

				def_status = hs->hd_def_status[def_idx][host_idx];
				upid_cat = hs->hd[host_idx].cat_upid;

				error(__FFL__,
						"Invalid radius value %+.1e in def %s, ID %ld, pid %ld cat pid %ld main N %.0f status %d.\n",
						p_sort[i].v[0], moria_cfg->output_sub_defs_str[j], hs->hd[host_idx].id,
						hs->hd_upid_defs[j][host_idx], upid_cat,
						hs->hd_defs[moria_cfg->main_def_catalog.idx][host_idx]
								/ moria_cfg->particle_mass, def_status);
			}
#endif

			/*
			 * We can use the simple (rather than corrected) tree search function because we don't care
			 * about the actual coordinates of the found halos, just that they are within the search
			 * radius.
			 */
			tr_res = treeResultsInit();
			treeFindSpherePeriodicSimple(tree, tr_res, p_sort[i].x, p_sort[i].v[0]);

			debugHaloMoria(p_sort[i].id, "Host/sub routine found %d potential subs; orderq = %.2e.",
					tr_res->num_points, p_sort[i].v[2]);

			for (k = 0; k < tr_res->num_points; k++)
			{
				/*
				 * Identify the original ID of this potential sub. The tree search finds the halo
				 * itself, so we need to ignore that one.
				 */
				sub_idx = (int) tr_res->points[k]->v[1];
				if (host_idx == sub_idx)
					continue;

				debugHaloMoria(p_sort[i].id, "Sub %4d, ID %ld, pid %9ld, q %.2e.", k,
						tr_res->points[k]->id, hs->hd_upid_defs[j][sub_idx],
						tr_res->points[k]->v[2]);
				debugHaloMoria(tr_res->points[k]->id,
						"Found as potential sub of halo ID %ld, q %.2e.", p_sort[i].id,
						p_sort[i].v[2]);

				/*
				 * If this halo is already a subhalo, nothing to do here
				 */
				if (hs->hd_upid_defs[j][sub_idx] != UPID_HOST)
					continue;

				/*
				 * Note that the order quantity need not be radius, meaning that the halo we've found
				 * as a potential sub can be larger (in order_q) than the current host. In that case,
				 * we do not mark it as a subhalo.
				 */
				if (tr_res->points[k]->v[2] >= p_sort[i].v[2])
					continue;

				/*
				 * At this point, mark the halo as a subhalo.
				 */
				hs->hd_upid_defs[j][sub_idx] = p_sort[i].id;

				/*
				 * This halo is a host so far, but within this halo which is already a subhalo
				 * itself. The config parameter hostsub_assign_subparent_id decides whether this halo becomes
				 * a sub (of the host of this halo) or not.
				 */
				if ((moria_cfg->hostsub_assign_subparent_id)
						&& (hs->hd_upid_defs[j][host_idx] != UPID_HOST))
					hs->hd_upid_defs[j][sub_idx] = hs->hd_upid_defs[j][host_idx];
			}
			treeResultsFree(tr_res);
		}
	}

	/*
	 * Free memory
	 */
	treeFree(&tree);
	memFree(__FFL__, MID_MORIA_TMP, p_sort, sizeof(Particle) * n_hostsub);
	memFree(__FFL__, MID_MORIA_TMP, p_tree, sizeof(Particle) * n_hostsub);

	/*
	 * If we are dealing with ghosts, simply copy their current PID; it will be the same regardless
	 * of definition.
	 */
	if (moria_cfg->include_ghosts)
	{
		for (i = n_hostsub; i < hs->n_tot; i++)
		{
			for (j = 0; j < moria_cfg->n_defs_sub; j++)
				hs->hd_upid_defs[j][i] = hs->hd[i].cat_upid;
		}
	}
}

/*
 * This function contains the conditions for a halo to not be considered for host-sub relations.
 * In particular, we exclude a halo if it does not make the output and the user has chosen to
 * ignore such halos, or if it is a ghost (which can only happen if ghosts are included).
 */
int excludeHaloFromHostSub(MoriaConfig *moria_cfg, HaloData *hd)
{
	return ((!hd->do_output && moria_cfg->hostsub_restrict_to_output)
			|| (moria_cfg->include_ghosts && isGhostStatus(hd->status_sparta)));
}

int compareHaloQ(const void *a, const void *b)
{
	if (((Particle*) a)->v[2] < ((Particle*) b)->v[2])
		return 1;
	else if (((Particle*) a)->v[2] > ((Particle*) b)->v[2])
		return -1;
	else
		return 0;
}
