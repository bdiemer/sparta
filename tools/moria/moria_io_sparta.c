/*************************************************************************************************
 *
 * I/O routines for moria.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <hdf5.h>
#include <sys/stat.h>

#include "moria_global.h"
#include "moria_io_sparta.h"
#include "../../src/global.h"
#include "../../src/memory.h"
#include "../../src/utils.h"
#include "../../src/io/io_hdf5.h"
#include "../../src/halos/halo_definitions.h"

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void intializeAnalysisData(AnalysisData *al_data);
void readAnalaysisMetadata(AnalysisData *al_data, hid_t file_id, char *group_name);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Set a few fields in the moria config and create the SpartaData structure. At this point, we
 * only set fields that are independent of any user choices, e.g., of what data needs to be loaded.
 */
SpartaData* readSpartaConfig(MoriaConfig *moria_cfg)
{
	int i, n_snap_tmp;
	float tmp_float;
	SpartaData *sd;
	hid_t file_id, group_id;
	struct stat file_info;

	sd = (SpartaData*) memAlloc(__FFL__, MID_MORIA_SPARTA, sizeof(SpartaData));

	/*
	 * Open HDF5 file
	 */
	output(4, "Reading SPARTA config...\n");
	if (stat(moria_cfg->sparta_file, &file_info) != 0)
		error(__FFL__, "SPARTA file %s does not exist, please check the file path.\n",
				moria_cfg->sparta_file);
	file_id = H5Fopen(moria_cfg->sparta_file, H5F_ACC_RDONLY, H5P_DEFAULT);
	hdf5CheckForError(__FFL__, file_id, "opening SPARTA file");

	/*
	 * Read snapshot redshifts and moria_cfg
	 */
	output(4, "    Reading group simulation...\n");
	group_id = H5Gopen(file_id, "/simulation", H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "simulation", H5I_GROUP);

	// Note that these go into a different memory ID
	hdf5ReadAttributeFloat1D(group_id, "snap_a", &(moria_cfg->n_snaps), &(moria_cfg->snap_a),
			MID_MORIA_CONFIG);
	hdf5ReadAttributeFloat1D(group_id, "snap_z", &(moria_cfg->n_snaps), &(moria_cfg->snap_z),
			MID_MORIA_CONFIG);
	hdf5ReadAttributeFloat1D(group_id, "snap_t", &(moria_cfg->n_snaps), &(moria_cfg->snap_t),
			MID_MORIA_CONFIG);
	hdf5ReadAttributeFloat1D(group_id, "snap_tdyn", &(moria_cfg->n_snaps), &(moria_cfg->snap_t_dyn),
			MID_MORIA_CONFIG);

	hdf5ReadAttributeFloat(group_id, "particle_mass", &(moria_cfg->particle_mass));
	hdf5ReadAttributeFloat(group_id, "box_size", &(moria_cfg->box_size));
	hdf5ReadAttributeFloat(group_id, "Omega_m", &tmp_float);
	moria_cfg->cosmo.Omega_m = (double) tmp_float;
	hdf5ReadAttributeFloat(group_id, "Omega_L", &tmp_float);
	moria_cfg->cosmo.Omega_L = (double) tmp_float;
	hdf5ReadAttributeFloat(group_id, "h", &tmp_float);
	moria_cfg->cosmo.h = (double) tmp_float;

	H5Gclose(group_id);

	/*
	 * Config group
	 */
	output(4, "    Reading group config...\n");
	group_id = H5Gopen(file_id, "/config", H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "config", H5I_GROUP);

	hdf5ReadAttributeInt(group_id, "cat_type", &(moria_cfg->cat_type));
	hdf5ReadAttributeString(group_id, "cat_path", &(sd->n_cat_path), &(sd->cat_path),
			MID_MORIA_SPARTA);
	hdf5ReadAttributeString(group_id, "output_path", &(sd->n_out_path), &(sd->out_path),
			MID_MORIA_SPARTA);
	hdf5ReadAttributeInt(group_id, "output_min_n200m", &(moria_cfg->output_min_n200m));
	hdf5ReadAttributeInt(group_id, "output_compression_level",
			&(moria_cfg->sparta_output_compression_level));

	H5Gclose(group_id);

	/*
	 * Compiled config
	 */
	output(4, "    Reading group config/compile...\n");
	group_id = H5Gopen(file_id, "/config/compile", H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "config", H5I_GROUP);

	hdf5ReadAttributeInt(group_id, "OUTPUT_GHOSTS", &(moria_cfg->sparta_has_ghosts));

	H5Gclose(group_id);

	/*
	 * To get dimensions, check halo ID array (without loading it, that'll happen later)
	 */
	output(4, "    Reading group halos...\n");
	group_id = H5Gopen(file_id, "/halos", H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "halos", H5I_GROUP);

	hdf5GetDatasetDimensions2D(group_id, "id", &(sd->n_halos), &n_snap_tmp);
	H5Gclose(group_id);
	if (n_snap_tmp != moria_cfg->n_snaps)
		if (n_snap_tmp != moria_cfg->n_snaps)
			error(__FFL__, "Found %d snapshots in halo ID SPARTA array, expected %d.\n", n_snap_tmp,
					moria_cfg->n_snaps);

	/*
	 * Load metadata for analyses
	 */
	intializeAnalysisData(&(sd->anl_rsp));
	intializeAnalysisData(&(sd->anl_hps));

	readAnalaysisMetadata(&(sd->anl_rsp), file_id, "/anl_rsp");
	readAnalaysisMetadata(&(sd->anl_hps), file_id, "/anl_hps");

	/*
	 * Initialize attributes
	 */
	for (i = 0; i < MORIA_MAX_CONFIG_PARS_MAIN; i++)
		hdf5InitializeAttribute(&(sd->config_main[i]));
	for (i = 0; i < MORIA_MAX_CONFIG_PARS_COMPILE; i++)
		hdf5InitializeAttribute(&(sd->config_compile[i]));
	for (i = 0; i < MORIA_MAX_CONFIG_PARS_ANL; i++)
		hdf5InitializeAttribute(&(sd->config_rsp[i]));
	for (i = 0; i < MORIA_MAX_CONFIG_PARS_ANL; i++)
		hdf5InitializeAttribute(&(sd->config_hps[i]));

	/*
	 * Finalize
	 */
	H5Fclose(file_id);

	return sd;
}

void intializeAnalysisData(AnalysisData *al_data)
{
	int i;

	al_data->anl_exists = 0;
	al_data->anl_is_loaded = 0;
	al_data->n_anl = 0;
	al_data->n_defs = 0;
	al_data->n_datasets = 0;
	al_data->n_snaps = 0;
	al_data->snap_idxs = NULL;
	al_data->snap_idxs_in_anl = NULL;
	al_data->anl_idx = NULL;
	al_data->status = NULL;
	for (i = 0; i < MORIA_MAX_ANL_DATASETS; i++)
		initializeNDArray(&(al_data->datasets[i]));
	for (i = 0; i < MORIA_MAX_ANL_DATASETS; i++)
		initHaloDefinition(&(al_data->defs[i]));
}

void readAnalaysisMetadata(AnalysisData *al_data, hid_t file_id, char *group_name)
{
	int i, object_type;
	hid_t group_id;
	hsize_t n_ds;
	char ds_name[MAX_DS_CHARS];
	HaloDefinition def;

	/*
	 * Check if the analysis even exists. If not, there is not much to do.
	 */
	if (!hdf5GroupExists(file_id, group_name))
		return;

	/*
	 * The group exists. First, test the size of the output array.
	 */
	output(4, "    Reading group %s...\n", group_name);
	group_id = H5Gopen(file_id, group_name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, file_id, group_id, group_name, H5I_GROUP);
	hdf5GetDatasetDimensions(group_id, "halo_id", &(al_data->n_anl));

	/*
	 * We get a listing of all datasets and convert them to halo definitions, if applicable.
	 */
	H5Gget_num_objs(group_id, &n_ds);
	for (i = 0; i < n_ds; i++)
	{
		H5Gget_objname_by_idx(group_id, (hsize_t) i, ds_name, (size_t) MAX_DS_CHARS);
		object_type = H5Gget_objtype_by_idx(group_id, (size_t) i);

		if (object_type != H5G_DATASET)
			continue;

		if (stringStartsWith(ds_name, "halo_"))
			continue;

		if (stringStartsWith(ds_name, "status"))
			continue;

		/*
		 * Check for new or old halo definition conversion. If neither works, this dataset does not
		 * appear to represent a mass definition.
		 */
		def = stringToHaloDefinition(ds_name, 1);
		if (def.type == HDEF_TYPE_INVALID)
			def = stringToHaloDefinition(ds_name, 0);
		if (def.type == HDEF_TYPE_INVALID)
			continue;
		def.source = HDEF_SOURCE_SPARTA;

		output(3, "In SPARTA analysis, found halo definition %s.\n", ds_name);

		al_data->defs[al_data->n_defs] = def;
		al_data->n_defs++;
	}
	H5Gclose(group_id);

	al_data->anl_exists = 1;
	al_data->anl_status_loaded = 0;
}

/*
 * In this function, we do the actual data reading (the results of which depend on the config).
 */
void readSpartaData(MoriaConfig *moria_cfg, SpartaData *sd)
{
	int i, j, n_snap_tmp, n_halos_tmp, n_dim_tmp, do_read_rsp, do_read_hps, found_ds, convert_ds;
	char def_str_file[DEFAULT_HALO_DEF_STRLEN], tmpstr[DEFAULT_HALO_DEF_STRLEN],
			ds_names_loaded[MORIA_MAX_ANL_DATASETS][DEFAULT_HALO_DEF_STRLEN];
	hid_t file_id, group_id;
	NDArray *nda;

	/*
	 * Open file
	 */
	output(2, "Reading SPARTA file %s...\n", moria_cfg->sparta_file);
	file_id = H5Fopen(moria_cfg->sparta_file, H5F_ACC_RDONLY, H5P_DEFAULT);

	/*
	 * Read full config to be copied into output files
	 */
	group_id = H5Gopen(file_id, "/config", H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "config", H5I_GROUP);
	sd->n_config_main = hdf5ReadAllAttributes(group_id, sd->config_main, MORIA_MAX_CONFIG_PARS_MAIN,
			MID_MORIA_SPARTA);
	H5Gclose(group_id);

	group_id = H5Gopen(file_id, "/config/compile", H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "config", H5I_GROUP);
	sd->n_config_compile = hdf5ReadAllAttributes(group_id, sd->config_compile,
	MORIA_MAX_CONFIG_PARS_COMPILE, MID_MORIA_SPARTA);
	H5Gclose(group_id);

	group_id = H5Gopen(file_id, "/config/anl_rsp", H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "config", H5I_GROUP);
	sd->n_config_rsp = hdf5ReadAllAttributes(group_id, sd->config_rsp, MORIA_MAX_CONFIG_PARS_ANL,
			MID_MORIA_SPARTA);
	H5Gclose(group_id);

	group_id = H5Gopen(file_id, "/config/anl_hps", H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "config", H5I_GROUP);
	sd->n_config_hps = hdf5ReadAllAttributes(group_id, sd->config_hps, MORIA_MAX_CONFIG_PARS_ANL,
			MID_MORIA_SPARTA);
	H5Gclose(group_id);

	/*
	 * Some fields are always read, namely ID, status, and the internal R200m
	 */
	output(2, "    Reading group halos...\n");
	group_id = H5Gopen(file_id, "/halos", H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "halos", H5I_GROUP);

	hdf5ReadDataset2D(group_id, "status", DTYPE_INT8, &n_halos_tmp, &n_snap_tmp,
			(void***) &(sd->sparta_status), MID_MORIA_SPARTA);
	if (n_snap_tmp != moria_cfg->n_snaps)
		error(__FFL__, "Found %d snapshots in status SPARTA array, expected %d.\n", n_snap_tmp,
				moria_cfg->n_snaps);
	if (n_halos_tmp != sd->n_halos)
		error(__FFL__, "Found %d halos in status SPARTA array, expected %d.\n", n_halos_tmp,
				sd->n_halos);

	hdf5ReadDataset(group_id, "status_final", DTYPE_INT8, &n_halos_tmp,
			(void**) &(sd->sparta_status_final), MID_MORIA_SPARTA);
	if (n_halos_tmp != sd->n_halos)
		error(__FFL__, "Found %d halos in status_final SPARTA array, expected %d.\n", n_halos_tmp,
				sd->n_halos);

	hdf5ReadDataset2D(group_id, "id", DTYPE_INT64, &n_halos_tmp, &n_snap_tmp,
			(void***) &(sd->halo_ids), MID_MORIA_SPARTA);
	if (n_snap_tmp != moria_cfg->n_snaps)
		error(__FFL__, "Found %d snapshots in halo ID SPARTA array, expected %d.\n", n_snap_tmp,
				moria_cfg->n_snaps);
	if (n_halos_tmp != sd->n_halos)
		error(__FFL__, "Found %d halos in halo ID SPARTA array, expected %d.\n", n_halos_tmp,
				sd->n_halos);

	hdf5ReadDataset2D(group_id, "R200m", DTYPE_FLOAT, &n_halos_tmp, &n_snap_tmp,
			(void***) &(sd->halo_R200m), MID_MORIA_SPARTA);
	if (n_snap_tmp != moria_cfg->n_snaps)
		error(__FFL__, "Found %d snapshots in R200m SPARTA array, expected %d.\n", n_snap_tmp,
				moria_cfg->n_snaps);
	if (n_halos_tmp != sd->n_halos)
		error(__FFL__, "Found %d halos in R200m SPARTA array, expected %d.\n", n_halos_tmp,
				sd->n_halos);

	/*
	 * The parent ID, coordinates, and velocity are loaded only if we are including
	 * halos that do not have catalog equivalents.
	 */
	if (moria_cfg->load_sparta_pid)
	{
		hdf5ReadDataset2D(group_id, "parent_id", DTYPE_INT64, &n_halos_tmp, &n_snap_tmp,
				(void***) &(sd->halo_pids), MID_MORIA_SPARTA);
		if (n_snap_tmp != moria_cfg->n_snaps)
			error(__FFL__, "Found %d snapshots in halo PID SPARTA array, expected %d.\n",
					n_snap_tmp, moria_cfg->n_snaps);
		if (n_halos_tmp != sd->n_halos)
			error(__FFL__, "Found %d halos in halo PID SPARTA array, expected %d.\n", n_halos_tmp,
					sd->n_halos);
	}

	if (moria_cfg->load_sparta_xv)
	{
		hdf5ReadDataset3D(group_id, "position", DTYPE_FLOAT, &n_halos_tmp, &n_snap_tmp, &n_dim_tmp,
				(void****) &(sd->halo_x), MID_MORIA_SPARTA);
		if (n_snap_tmp != moria_cfg->n_snaps)
			error(__FFL__, "Found %d snapshots in halo x SPARTA array, expected %d.\n", n_snap_tmp,
					moria_cfg->n_snaps);
		if (n_halos_tmp != sd->n_halos)
			error(__FFL__, "Found %d halos in halo x SPARTA array, expected %d.\n", n_halos_tmp,
					sd->n_halos);
		if (n_dim_tmp != 3)
			error(__FFL__, "Found dimensionality %d in halo x SPARTA array, expected 3.\n",
					n_halos_tmp);

		hdf5ReadDataset3D(group_id, "velocity", DTYPE_FLOAT, &n_halos_tmp, &n_snap_tmp, &n_dim_tmp,
				(void****) &(sd->halo_v), MID_MORIA_SPARTA);
		if (n_snap_tmp != moria_cfg->n_snaps)
			error(__FFL__, "Found %d snapshots in halo v SPARTA array, expected %d.\n", n_snap_tmp,
					moria_cfg->n_snaps);
		if (n_halos_tmp != sd->n_halos)
			error(__FFL__, "Found %d halos in halo v SPARTA array, expected %d.\n", n_halos_tmp,
					sd->n_halos);
		if (n_dim_tmp != 3)
			error(__FFL__, "Found dimensionality %d in halo v SPARTA array, expected 3.\n",
					n_halos_tmp);
	}

	output(2, "    Found %d halos and %d snapshots.\n", sd->n_halos, moria_cfg->n_snaps);
	H5Gclose(group_id);

#if MORIA_DEBUG_HALO
	int debug_idx, debug_j;

	debug_idx = -1;
	for (i = 0; i < sd->n_halos; i++)
	{
		for (debug_j = 0; debug_j < moria_cfg->n_snaps; debug_j++)
		{
			if (sd->halo_ids[i][debug_j] == MORIA_DEBUG_HALO)
			{
				debugHaloMoria(sd->halo_ids[i][debug_j],
						"Found in SPARTA halos at idx %d, snap %d.", i, debug_j);
				debug_idx = i;
				break;
			}
		}
		if (debug_idx >= 0)
			break;
	}
#endif

	/*
	 * Determine whether we have any Rsp-related or HaloProps definitions to read.
	 */
	do_read_rsp = 0;
	for (i = 0; i < moria_cfg->n_defs_all; i++)
	{
		if (isSpartaRspDefinition(&(moria_cfg->all_defs[i])))
			do_read_rsp = 1;
	}
	sd->anl_rsp.anl_is_loaded = do_read_rsp;

	do_read_hps = 0;
	for (i = 0; i < moria_cfg->n_defs_all; i++)
	{
		if (isSpartaHpsDefinition(&(moria_cfg->all_defs[i])))
			do_read_hps = 1;
	}
	sd->anl_hps.anl_is_loaded = do_read_hps;

	/*
	 * Read Rsp analysis. We go through the datasets that should come from this analysis, read
	 * them, and store the index back in the config.
	 */
	if (do_read_rsp)
	{
		output(2, "    Reading group anl_rsp...\n");

		/*
		 * First read config to get snaps for this analysis
		 */
		group_id = H5Gopen(file_id, "/config/anl_rsp", H5P_DEFAULT);
		hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "config/anl_rsp", H5I_GROUP);

		hdf5ReadAttributeInt1D(group_id, "snap_idxs", &(sd->anl_rsp.n_snaps),
				&(sd->anl_rsp.snap_idxs), MID_MORIA_SPARTA);
		if (sd->anl_rsp.n_snaps < 1)
			error(__FFL__, "Found %d snapshots in Rsp analysis.\n", sd->anl_rsp.n_snaps);
		if (sd->anl_rsp.n_snaps > MORIA_MAX_N_A)
			error(__FFL__, "Found %d snapshots in Rsp analysis, max %d. Increase MORIA_MAX_N_A.\n",
					sd->anl_rsp.n_snaps, MORIA_MAX_N_A);

		hdf5ReadAttributeInt1D(group_id, "snap_idxs_in_anl", &n_snap_tmp,
				&(sd->anl_rsp.snap_idxs_in_anl), MID_MORIA_SPARTA);
		if (n_snap_tmp != moria_cfg->n_snaps)
			if (sd->anl_rsp.n_snaps < 1)
				error(__FFL__, "Found %d snapshots in Rsp analysis, expected %d.\n", n_snap_tmp,
						moria_cfg->n_snaps);

		H5Gclose(group_id);

		/*
		 * Now read data. From the status field, we check whether there are the expected number of
		 * snapshots.
		 */
		group_id = H5Gopen(file_id, "/anl_rsp", H5P_DEFAULT);
		hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "anl_rsp", H5I_GROUP);

		hdf5ReadDataset(group_id, "halo_first", DTYPE_INT64, &n_halos_tmp,
				(void**) &(sd->anl_rsp.anl_idx), MID_MORIA_SPARTA);
		if (n_halos_tmp != sd->n_halos)
			error(__FFL__, "Found %d halos in halo_first Rsp analysis array, expected %d.\n",
					n_halos_tmp, sd->n_halos);

		// Read status
		hdf5ReadDataset2D(group_id, "status", DTYPE_INT8, &(sd->anl_rsp.n_anl), &n_snap_tmp,
				(void***) &(sd->anl_rsp.status), MID_MORIA_SPARTA);
		if (n_snap_tmp != sd->anl_rsp.n_snaps)
			error(__FFL__,
					"Found status array with %d snaps in status of SPARTA Rsp analysis, expected %d.\n",
					n_snap_tmp, sd->anl_rsp.n_snaps);
		sd->anl_rsp.anl_status_loaded = 1;

		// Read halo definitions
		sd->anl_rsp.n_datasets = 0;
		for (i = 0; i < moria_cfg->n_defs_all; i++)
		{
			if (!isSpartaRspDefinition(&(moria_cfg->all_defs[i])))
				continue;

			// Temporarily turn off source so it doesn't get output
			moria_cfg->all_defs[i].source = HDEF_SOURCE_ANY;
			haloDefinitionToString(moria_cfg->all_defs[i], def_str_file);
			moria_cfg->all_defs[i].source = HDEF_SOURCE_SPARTA;

			// Read dataset
			output(4, "    Analysis rsp, reading dataset %s...\n", def_str_file);
			nda = &(sd->anl_rsp.datasets[sd->anl_rsp.n_datasets]);
			hdf5ReadNDArray(group_id, def_str_file, nda, MID_MORIA_SPARTA);
			if (nda->n[0] != sd->anl_rsp.n_anl)
				error(__FFL__,
						"Found array with %d analyses in SPARTA Rsp analysis, expected %d.\n",
						nda->n[0], sd->anl_rsp.n_anl);
			if (nda->n[1] != sd->anl_rsp.n_snaps)
				error(__FFL__,
						"Found status array with %d snaps in SPARTA Rsp analysis, expected %d.\n",
						nda->n[1], sd->anl_rsp.n_snaps);

			moria_cfg->all_defs[i].idx = sd->anl_rsp.n_datasets;
			sd->anl_rsp.n_datasets++;
		}
		H5Gclose(group_id);

#if MORIA_DEBUG_HALO
		if (debug_idx >= 0)
			debugHaloMoria(MORIA_DEBUG_HALO, "SPARTA anl_rsp idx is %ld.",
					sd->anl_rsp.anl_idx[debug_idx]);
#endif
	}

	/*
	 * Read HaloProps analysis
	 */
	if (do_read_hps)
	{
		output(2, "    Reading group anl_hps...\n");

		/*
		 * First read config to get snaps for this analysis
		 */
		group_id = H5Gopen(file_id, "/config/anl_hps", H5P_DEFAULT);
		hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "config/anl_hps", H5I_GROUP);

		hdf5ReadAttributeInt1D(group_id, "snap_idxs", &(sd->anl_hps.n_snaps),
				&(sd->anl_hps.snap_idxs), MID_MORIA_SPARTA);
		if (sd->anl_hps.n_snaps < 1)
			error(__FFL__, "Found %d snapshots in haloprops analysis.\n", sd->anl_hps.n_snaps);
		if (sd->anl_hps.n_snaps > MORIA_MAX_N_A)
			error(__FFL__,
					"Found %d snapshots in haloprops analysis, max %d. Increase MORIA_MAX_N_A.\n",
					sd->anl_hps.n_snaps, MORIA_MAX_N_A);

		hdf5ReadAttributeInt1D(group_id, "snap_idxs_in_anl", &n_snap_tmp,
				&(sd->anl_hps.snap_idxs_in_anl), MID_MORIA_SPARTA);
		if (n_snap_tmp != moria_cfg->n_snaps)
			if (sd->anl_hps.n_snaps < 1)
				error(__FFL__, "Found %d snapshots in haloprops analysis, expected %d.\n",
						n_snap_tmp, moria_cfg->n_snaps);

		H5Gclose(group_id);

		/*
		 * Now read data group
		 */
		group_id = H5Gopen(file_id, "/anl_hps", H5P_DEFAULT);
		hdf5CheckForErrorOpen(__FFL__, file_id, group_id, "anl_hps", H5I_GROUP);

		hdf5ReadDataset(group_id, "halo_first", DTYPE_INT64, &n_halos_tmp,
				(void**) &(sd->anl_hps.anl_idx), MID_MORIA_SPARTA);
		if (n_halos_tmp != sd->n_halos)
			error(__FFL__, "Found %d halos in halo_first haloprops analysis array, expected %d.\n",
					n_halos_tmp, sd->n_halos);

		for (i = 0; i < moria_cfg->n_defs_all; i++)
		{
			if (!isSpartaHpsDefinition(&(moria_cfg->all_defs[i])))
				continue;

			found_ds = 0;
			convert_ds = 0;

			// Generate the string identifier expected in the file
			moria_cfg->all_defs[i].source = HDEF_SOURCE_ANY;
			haloDefinitionToString(moria_cfg->all_defs[i], def_str_file);
			moria_cfg->all_defs[i].source = HDEF_SOURCE_SPARTA;

			/*
			 * Check whether the field exists. If not, it could be that we need to switch R/M.
			 */
			if (hdf5ObjectExists(group_id, def_str_file))
			{
				found_ds = 1;
			} else if ((moria_cfg->all_defs[i].quantity == HDEF_Q_RADIUS)
					|| (moria_cfg->all_defs[i].quantity == HDEF_Q_MASS))
			{
				moria_cfg->all_defs[i].source = HDEF_SOURCE_ANY;
				if (moria_cfg->all_defs[i].quantity == HDEF_Q_RADIUS)
					moria_cfg->all_defs[i].quantity = HDEF_Q_MASS;
				else if (moria_cfg->all_defs[i].quantity == HDEF_Q_MASS)
					moria_cfg->all_defs[i].quantity = HDEF_Q_RADIUS;
				haloDefinitionToString(moria_cfg->all_defs[i], def_str_file);
				moria_cfg->all_defs[i].source = HDEF_SOURCE_SPARTA;
				if (moria_cfg->all_defs[i].quantity == HDEF_Q_RADIUS)
					moria_cfg->all_defs[i].quantity = HDEF_Q_MASS;
				else if (moria_cfg->all_defs[i].quantity == HDEF_Q_MASS)
					moria_cfg->all_defs[i].quantity = HDEF_Q_RADIUS;

				if (hdf5ObjectExists(group_id, def_str_file))
				{
					found_ds = 1;
					convert_ds = 1;
				}
			}

			/*
			 * If we have not found the field now, it is truly missing and we abort.
			 */
			if (!found_ds)
				error(__FFL__,
						"Could not find dataset %s in SPARTA HaloProps analysis, needed for definition %s.\n",
						def_str_file, moria_cfg->all_defs_str[i]);

			/*
			 * Now we must check that the field has not been loaded already; this could happen if
			 * multiple definitions rely on the same field. If we are converting, we note that in
			 * the flag field of the definition; otherwise, the merge routine would later have no
			 * idea that the idx is pointed to a different definition.
			 */
			found_ds = 0;
			for (j = 0; j < sd->anl_hps.n_datasets; j++)
			{
				if (strcmp(def_str_file, ds_names_loaded[j]) == 0)
				{
					moria_cfg->all_defs[i].idx = j;
					moria_cfg->all_defs[i].do_convert = convert_ds;
					found_ds = 1;
					break;
				}
			}

			/*
			 * If we have not set this definition to a previously loaded dataset, we need to load
			 * the new dataset and its status.
			 */
			if (!found_ds)
			{
				// Load actual definition
				output(4, "    Analysis hps, reading dataset %s...\n", def_str_file);
				nda = &(sd->anl_hps.datasets[sd->anl_hps.n_datasets]);
				hdf5ReadNDArray(group_id, def_str_file, nda, MID_MORIA_SPARTA);
				if (nda->n[0] != sd->anl_hps.n_anl)
					error(__FFL__,
							"Array %s has %d analyses in SPARTA haloprops analysis, expected %d.\n",
							def_str_file, nda->n[0], sd->anl_hps.n_anl);
				if (nda->n[1] != sd->anl_hps.n_snaps)
					error(__FFL__,
							"Array %s has %d snaps in SPARTA haloprops analysis, expected %d.\n",
							def_str_file, nda->n[1], sd->anl_hps.n_snaps);

				// Set index; the status index will be this index + 1
				moria_cfg->all_defs[i].idx = sd->anl_hps.n_datasets;
				moria_cfg->all_defs[i].do_convert = convert_ds;

				// Increase the dataset counter, set string ID
				sprintf(ds_names_loaded[sd->anl_hps.n_datasets], "%s", def_str_file);
				sd->anl_hps.n_datasets++;

				// Load definition status (must be the field after definition itself)
				sprintf(tmpstr, "status_%s", def_str_file);
				strcpy(def_str_file, tmpstr);
				output(4, "    Analysis hps, reading dataset %s...\n", def_str_file);
				nda = &(sd->anl_hps.datasets[sd->anl_hps.n_datasets]);
				hdf5ReadNDArray(group_id, def_str_file, nda, MID_MORIA_SPARTA);
				if (nda->n[0] != sd->anl_hps.n_anl)
					error(__FFL__,
							"Array %s has %d analyses in SPARTA haloprops analysis, expected %d.\n",
							def_str_file, nda->n[0], sd->anl_hps.n_anl);
				if (nda->n[1] != sd->anl_hps.n_snaps)
					error(__FFL__,
							"Array %s has %d snaps in SPARTA haloprops analysis, expected %d.\n",
							def_str_file, nda->n[1], sd->anl_hps.n_snaps);

				// Increase the dataset counter, set string ID
				sprintf(ds_names_loaded[sd->anl_hps.n_datasets], "%s", def_str_file);
				sd->anl_hps.n_datasets++;
			}
		}

		H5Gclose(group_id);

#if MORIA_DEBUG_HALO
		if (debug_idx >= 0)
			debugHaloMoria(MORIA_DEBUG_HALO, "SPARTA anl_hps idx is %ld.",
					sd->anl_hps.anl_idx[debug_idx]);
#endif
	}

	/*
	 * Close file
	 */
	H5Fclose(file_id);
	printLine(2);
}

/*
 * Note the dimensions of the arrays we are freeing. The anl_idx has dimensions of the size of all
 * halos in the sparta file, whereas the rest of the analysis data has dimensions of the number of
 * analysis records / the snapshots in this analysis.
 *
 * Also note that some fields have the HDF5 (contiguous) memory layout.
 */
void freeSpartaAnalysis(MoriaConfig *moria_cfg, AnalysisData *al, int n_halos_sparta)
{
	int j;

	if (!al->anl_is_loaded)
		return;

	if (al->anl_idx != NULL)
		memFree(__FFL__, MID_MORIA_SPARTA, al->anl_idx, sizeof(long int) * n_halos_sparta);

	if (al->anl_status_loaded)
	{
		memFree(__FFL__, MID_MORIA_SPARTA, al->status[0],
				sizeof(int_least8_t) * al->n_anl * al->n_snaps);
		memFree(__FFL__, MID_MORIA_SPARTA, al->status, sizeof(int_least8_t*) * al->n_anl);
	}

	memFree(__FFL__, MID_MORIA_SPARTA, al->snap_idxs, sizeof(int) * al->n_snaps);
	memFree(__FFL__, MID_MORIA_SPARTA, al->snap_idxs_in_anl, sizeof(int) * moria_cfg->n_snaps);

	for (j = 0; j < MORIA_MAX_ANL_DATASETS; j++)
		freeNDArray(__FFL__, MID_MORIA_SPARTA, &(al->datasets[j]));
}

void freeSpartaDataMain(MoriaConfig *moria_cfg, SpartaData *sd)
{
	int i;

	freeSpartaAnalysis(moria_cfg, &(sd->anl_rsp), sd->n_halos);
	freeSpartaAnalysis(moria_cfg, &(sd->anl_hps), sd->n_halos);

	memFree(__FFL__, MID_MORIA_SPARTA, sd->cat_path, sd->n_cat_path * sizeof(char));
	memFree(__FFL__, MID_MORIA_SPARTA, sd->out_path, sd->n_out_path * sizeof(char));

	/*
	 * Fields that are always loaded
	 */
	memFree(__FFL__, MID_MORIA_SPARTA, sd->sparta_status[0],
			sizeof(int_least8_t) * sd->n_halos * moria_cfg->n_snaps);
	memFree(__FFL__, MID_MORIA_SPARTA, sd->sparta_status, sizeof(int_least8_t*) * sd->n_halos);

	memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_ids[0],
			sizeof(long int) * sd->n_halos * moria_cfg->n_snaps);
	memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_ids, sizeof(long int*) * sd->n_halos);

	memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_R200m[0],
			sizeof(float) * sd->n_halos * moria_cfg->n_snaps);
	memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_R200m, sizeof(float*) * sd->n_halos);

	/*
	 * Ghost fields
	 */
	if (moria_cfg->load_sparta_pid)
	{
		memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_pids[0],
				sizeof(long int) * sd->n_halos * moria_cfg->n_snaps);
		memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_pids, sizeof(long int*) * sd->n_halos);
	}
	if (moria_cfg->load_sparta_xv)
	{
		memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_x[0][0],
				sizeof(float) * sd->n_halos * moria_cfg->n_snaps * 3);
		for (i = 0; i < sd->n_halos; i++)
			memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_x[i], sizeof(float*) * moria_cfg->n_snaps);
		memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_x, sizeof(float*) * sd->n_halos);

		memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_v[0][0],
				sizeof(float) * sd->n_halos * moria_cfg->n_snaps * 3);
		for (i = 0; i < sd->n_halos; i++)
			memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_v[i], sizeof(float*) * moria_cfg->n_snaps);
		memFree(__FFL__, MID_MORIA_SPARTA, sd->halo_v, sizeof(float*) * sd->n_halos);
	}
}

void freeSpartaDataConfig(MoriaConfig *moria_cfg, SpartaData *sd)
{
	int i;

	memFree(__FFL__, MID_MORIA_SPARTA, sd->sparta_status_final, sizeof(int_least8_t) * sd->n_halos);

	/*
	 * Attributes
	 */
	for (i = 0; i < sd->n_config_main; i++)
		hdf5FreeAttribute(&(sd->config_main[i]), MID_MORIA_SPARTA);
	for (i = 0; i < sd->n_config_compile; i++)
		hdf5FreeAttribute(&(sd->config_compile[i]), MID_MORIA_SPARTA);
	for (i = 0; i < sd->n_config_rsp; i++)
		hdf5FreeAttribute(&(sd->config_rsp[i]), MID_MORIA_SPARTA);
	for (i = 0; i < sd->n_config_hps; i++)
		hdf5FreeAttribute(&(sd->config_hps[i]), MID_MORIA_SPARTA);

	/*
	 * Dataset itself
	 */
	memFree(__FFL__, MID_MORIA_SPARTA, sd, sizeof(SpartaData));
}
