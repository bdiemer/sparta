/*************************************************************************************************
 *
 * This unit writes and reads restart files, i.e., memory dumps, for MORIA.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_IO_RESTART_H_
#define _MORIA_IO_RESTART_H_

#include "moria_global.h"
#include "moria_config.h"
#include "moria_statistics.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void moriaReadOrWriteRestartFiles(int rw, int *snap_idx, MoriaConfig *moria_cfg,
		MoriaTimers *timers, SpartaData **sd, TreeSet **ts);

#endif
