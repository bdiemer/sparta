/*************************************************************************************************
 *
 * Global constants, structures, and routines.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_GLOBAL_H_
#define _MORIA_GLOBAL_H_

#include <moria.h>

#include "../../src/global.h"
#include "../../src/global_types.h"
#include "../../src/config.h"
#include "../../src/cosmology.h"
#include "../../src/io/io_hdf5.h"

/*************************************************************************************************
 * CONSTANTS (STATUS FIELDS)
 *************************************************************************************************/

/*
 * General analysis status codes for all analyses
 */
#define MORIA_STATUS_HALO_DOES_NOT_EXIST -2
#define MORIA_STATUS_ANL_DOES_NOT_EXIST -1
#define MORIA_STATUS_UNDEFINED 0
#define MORIA_STATUS_SPARTA_SUCCESS 1

/*
 * Rsp analysis codes
 */
#define MORIA_RSP_STATUS_ESTIMATED_MODEL 2
#define MORIA_RSP_STATUS_ESTIMATED_PAST 3
#define MORIA_RSP_STATUS_ESTIMATED_FUTURE 4
#define MORIA_RSP_STATUS_ESTIMATED_INTERPOLATED 5

/*
 * Haloprops analysis codes
 */
#define MORIA_HPS_STATUS_SO_TOO_SMALL 2
#define MORIA_HPS_STATUS_SO_TOO_LARGE 3
#define MORIA_HPS_STATUS_ORB_ZERO 4

/*
 * Mass accretion rate status codes
 */
#define ACCRATE_STATUS_UNDEFINED 0
#define ACCRATE_STATUS_SUCCESS 1
#define ACCRATE_STATUS_REDUCED_INTERVAL 2
#define ACCRATE_STATUS_MODEL 3
#define ACCRATE_STATUS_SUB_SUCCESS 4
#define ACCRATE_STATUS_SUB_REDUCED_INTERVAL 5
#define ACCRATE_STATUS_SUB_MODEL 6
#define ACCRATE_STATUS_SUB_MODEL_NOW 7

/*************************************************************************************************
 * CONSTANTS (CODES)
 *************************************************************************************************/

#define MORIA_INVALID_IDX -1

/*
 * Output codes indicating host/sub status
 */
#define UPID_NOT_SET -2l
#define UPID_HOST -1l

/*
 * Possible units in which the cut threshold can be passed. The particles_vmax unit converts a
 * particle number into the (roughly) equivalent vmax.
 */
#define MORIA_UNITS \
ENUM(MORIA_UNITS_NONE, none)\
ENUM(MORIA_UNITS_PARTICLES, particles)\
ENUM(MORIA_UNITS_PARTICLES_VMAX, particles_vmax)

#define ENUM(x, y) x,
enum
{
	MORIA_UNITS/*space*/MORIA_N_UNITS
};
#undef ENUM

/*************************************************************************************************
 * CONSTANTS (MEMORY LIMITS)
 *************************************************************************************************/

/*
 * Maximum number of radius definitions, catalog fields, and snapshots. The corresponding fields
 * occupy very little memory, which is why the numbers are generous. Conversely, these numbers
 * should never be used in a memory-intensive field!
 */
#define MORIA_MAX_DEFS 100
#define MORIA_MAX_ANL_DATASETS 100
#define MORIA_MAX_CAT_FIELDS 100
#define MORIA_MAX_N_A 1000

/*
 * Derived lengths: the total number of defs could, in theory, be the sum of output defs and sub
 * defs, plus order and cut quantities.
 */
#define MORIA_MAX_DEFS_ALL (MORIA_MAX_DEFS + MORIA_MAX_DEFS + 2)

#define MAX_DS_CHARS 100

#define MORIA_MAX_CONFIG_PARS_MAIN 150
#define MORIA_MAX_CONFIG_PARS_COMPILE 100
#define MORIA_MAX_CONFIG_PARS_ANL 20

/*************************************************************************************************
 * CONSTANTS (OTHER)
 *************************************************************************************************/

/*
 * When trying to find a previous snapshot to compute the accretion rate (or other properties) over
 * a dynamical time, the desired time cannot generally be matched exactly. This number determines
 * the maximum difference in scale factor a, if the difference is larger the computation of the
 * mass accretion rate cannot be completed.
 *
 * Furthermore, we require that the interval spans at least DYNTIME_MIN_N snapshots to avoid noise.
 */
#define MORIA_DYNTIME_TOLERANCE 0.05
#define MORIA_DYNTIME_MIN_N 2

/*************************************************************************************************
 * DEBUG MACROS
 *************************************************************************************************/

#if MORIA_DEBUG_HALO
#define debugHaloMoria(args...) printDebugHaloMoria(args)
#else
#define debugHaloMoria(args...) {}
#endif

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

/*
 * A structure representing analyses loaded from Sparta. The idea is that an analysis contains
 * any number of datasets and/or halo definitions.
 *
 * Note that snap_idxs has dimensions of the number of snapshots output in this analysis, and
 * points into the array of all snapshots (see config), whereas snap_idxs_in_anl has dimensions
 * of the number of all snapshots and points into the arrays in this analysis (with -1 if the
 * analysis does not exist at a given snapshot).
 *
 * Note that the number of definitions (n_defs) is the number of all definitions found (which are
 * stored in the defs array), whereas n_datasets is the number of actually loaded datasets; they
 * will not be the same typically.
 */
typedef struct
{
	int_least8_t anl_exists;
	int_least8_t anl_is_loaded;
	int_least8_t anl_status_loaded;
	int n_anl;
	int n_snaps;
	int *snap_idxs;
	int *snap_idxs_in_anl;
	long int *anl_idx;
	int_least8_t **status;

	int n_defs;
	HaloDefinition defs[MORIA_MAX_ANL_DATASETS];

	int n_datasets;
	NDArray datasets[MORIA_MAX_ANL_DATASETS];
} AnalysisData;

/*
 * This struct contains the SPARTA data loaded from file. Note that the list of Rsp definitions is
 * contained only in the config object, but the same indices apply to the rsp_data objects in this
 * struct.
 *
 * The arrays could be NDArray types, but storing the pointers directly makes accessing them more
 * convenient throughout the code.
 */
typedef struct
{
	// SPARTA paths (which can be overwritten in moria_cfg)
	char *cat_path;
	char *out_path;
	int n_cat_path;
	int n_out_path;

	// Halos
	int n_halos;
	int_least8_t **sparta_status;
	int_least8_t *sparta_status_final;
	long int **halo_ids;
	float **halo_R200m;

	// For ghosts / halos without catalog data
	long int **halo_pids;
	float ***halo_x;
	float ***halo_v;

	// Analyses
	AnalysisData anl_rsp;
	AnalysisData anl_hps;

	// Config data to be copied
	int n_config_main;
	int n_config_compile;
	int n_config_rsp;
	int n_config_hps;
	Hdf5Attribute config_main[MORIA_MAX_CONFIG_PARS_MAIN];
	Hdf5Attribute config_compile[MORIA_MAX_CONFIG_PARS_COMPILE];
	Hdf5Attribute config_rsp[MORIA_MAX_CONFIG_PARS_ANL];
	Hdf5Attribute config_hps[MORIA_MAX_CONFIG_PARS_ANL];
} SpartaData;

typedef struct
{
	int *field_order;
	char parse_str[3000];
	int last_num;
} CatalogHeader;

/*
 * This struct combines the fundamental data from the catalog and from Sparta. The status fields
 * represent:
 *
 * status_sparta     The Sparta status code for the halo at the given snapshot (host/sub/etc)
 * status_acc_rate   Whether the mass accretion rate was computed from SPARTA masses, catalog masses,
 *                   or estimated using the fitting function.
 * status_sparta_rsp The Sparta status code for the Rsp analysis (success/not enough weight etc)
 * status_moria_rsp  The MORIA status code for the Rsp analysis (from SPARTA, guess etc)
 */
typedef struct
{
	int_least8_t do_output;

	int_least8_t status_sparta;
	int_least8_t status_sparta_rsp;
	int_least8_t status_moria_rsp;
	int_least8_t status_acc_rate;

	int_least8_t mmp;
	int_least8_t phantom;

	long int id;
	long int cat_upid;
	long int desc_id;

	int sparta_idx;

	float x[3];
	float v[3];
	float R200m_all_spa;
	float M200m_all_spa;
	float Rmain_cat;
	float Mmain_cat;
	float R_ref;
	float M_ref;
	float nu200m;
	float acc_rate_200m_dyn;
} HaloData;

/*
 * The set of halos that exist at one snapshot. HaloData is initially set from the halo catalog,
 * but this dataset can later be added to.
 *
 * The CatalogDataFields are added later and thus have a different memory layout.
 *
 * defs and upid_defs would be more elegantly included in hd itself, but their size is not known
 * a priori as it depends on the number of definitions chosen by the user. To optimize the memory
 * consumption, they are allocated dynamically in synchronicity with hd. The dimensions of these
 * fields are:
 *
 * hd_defs          (n_defs_all, n_halos)
 * hd_def_status    (n_defs_all, n_halos)
 * hd_upid_defs     (n_defs_sub, n_halos)
 */
typedef struct
{
	int n_tot;
	int n_cat;
	int n_out_cat;
	int_least8_t *out_mask;
	HaloData *hd;
	float **hd_defs;
	int_least8_t **hd_def_status;
	long int **hd_upid_defs;
	NDArray *cf;
} HaloSet;

/*
 * If we are outputting a tree file as well as catalogs, we must keep track of the relation between
 * halos at different snapshots. This is accomplished with a set of fields that we keep track of
 * for each halo history. We do not know the number of halos a priori and must thus be ready to
 * reallocate the memory. The fields are:
 *
 * is_alive    Has this halo history ended?
 * do_output   Has the halo reached the threshold for being written to file at any past snapshot?
 * id          The ID (catalog or SPARTA-assigned) of the halo in the current snapshot
 * desc_id     The ID in the next snapshot
 * sparta_idx  The index of the halo in the SPARTA file. This should remain constant over snapshots
 * merge_idx   The tree index into which this halo merges (if it does not survive until the end)
 *
 * Note that id and sparta_idx are not strictly necessary, but we keep track of the ID in case
 * there are errors. The sparta_idx provides a stringent check that everything is OK, as the
 * progenitor-descendant relationships are independently determined in SPARTA.
 */
typedef struct
{
	int_least8_t is_alive;
	int_least8_t do_output;
	HaloID id;
	HaloID desc_id;
	int sparta_idx;
	int merge_idx;
	int merge_snap;
	float merge_order_q;
} TreeHalo;

/*
 * The TreeSet is mostly just a collection of TreeHalo objects with variable size. However, it also
 * contains pointers to arrays with indices that are allocated at particular points during runtime:
 *
 * halo_tree_idxs   Tree indices for each halo; specific to each snapshot, and reallocated at each
 *                  snapshot to match the size of the current halo array.
 * tree_out_idxs    Tree indices sorted into output order (of size n_tree_items); only allocated
 *                  when sorting before writing the tree files.
 */
typedef struct
{
	int n;
	int n_alloc;
	int n_out;
	TreeHalo *th;
	int *halo_tree_idxs;
	int *tree_out_idxs;
} TreeSet;

typedef struct
{
	long int id;
	int idx;
} MoriaSorter;

typedef struct
{
	int snap_idx;
	float a;
	float z;
	float t;
	float rho_200m;
	float rho_vir;
	float rho_catmain;
	float Om;
	float comoving_conversion;

	int snap_idx_tdyn;
	float a_tdyn;
	float z_tdyn;
	float t_tdyn;
	float rho_200m_tdyn;

	float rho_threshold_defs[MORIA_MAX_DEFS_ALL];
} SnapshotData;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void printDebugHaloMoria(HaloID id, const char *format, ...);

int compareIndexSorters(const void *a, const void *b);
int unitsFromString(char *str);
void unitsToString(int u, char *str);

int isSpartaRspDefinition(HaloDefinition *def);
int isSpartaHpsDefinition(HaloDefinition *def);
int defHasStatus(HaloDefinition *def);

#endif
