/*************************************************************************************************
 *
 * This unit merges the catalog data with SPARTA data.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "moria_merge_hps.h"
#include "../../src/analyses/analysis_haloprops.h"
#include "../../src/halos/halo_so.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void mergeAnalysisHaloProps(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		int halo_idx)
{
	int j, anl_idx, anl_snap_idx;
	float sparta_rm_value, rho_threshold;
	int_least8_t sparta_def_status;
	HaloDefinition *def;
	HaloData *hd;

	/*
	 * Shortcut to the HaloData record for this halo. We can't pass that directly because we also
	 * need the defs / upid_defs fields from the haloset.
	 */
	hd = &(hs->hd[halo_idx]);

	/*
	 * Find the correct snapshot index in this analysis. If this snapshot does not exist, throw an
	 * error.
	 */
	anl_snap_idx = sd->anl_hps.snap_idxs_in_anl[snap->snap_idx];
	if (anl_snap_idx < 0)
		error(__FFL__, "Could not find haloprops analysis at snapshot %d, redshift %.2f.\n",
				snap->snap_idx, moria_cfg->snap_z[snap->snap_idx]);

	/*
	 * Set the redshift index and status.
	 */
	if (hd->sparta_idx != -1)
		anl_idx = sd->anl_hps.anl_idx[hd->sparta_idx];
	else
		anl_idx = -1;

	/*
	 * Now go through all hps definitions; the code looks more elegant this way since we need to
	 * loop over definitions in any case.
	 */
	for (j = 0; j < moria_cfg->n_defs_all; j++)
	{
		def = &(moria_cfg->all_defs[j]);

		if (!isSpartaHpsDefinition(def))
			continue;

		if (hd->sparta_idx == -1)
		{
			hs->hd_def_status[j][halo_idx] = MORIA_STATUS_HALO_DOES_NOT_EXIST;
			continue;
		}

		if (anl_idx == -1)
		{
			hs->hd_def_status[j][halo_idx] = MORIA_STATUS_ANL_DOES_NOT_EXIST;
			continue;
		}

		/*
		 * At this point we have a valid HPS analysis at hand and check the SPARTA status for this
		 * definition. Note that the def_idx that points into the SPARTA array is valid, but spaced
		 * such that def_idx + 1 is the corresponding status.
		 *
		 * If the flag is set in the definition, that means we need to convert radius and mass.
		 */
		sparta_def_status = sd->anl_hps.datasets[def->idx + 1].q_int8_2[anl_idx][anl_snap_idx];

		if (sparta_def_status == ANL_HPS_STATUS_SUCCESS)
		{
			sparta_rm_value = sd->anl_hps.datasets[def->idx].q_float_2[anl_idx][anl_snap_idx];
			if (def->do_convert)
			{
				/*
				 * First check: we can only convert SO definitions. If the flag is set in another
				 * definition, that's an error.
				 */
#if MORIA_CAREFUL
				if (def->type != HDEF_TYPE_SO)
					error(__FFL__,
							"Found conversion flag in HPS definition %s. Can only convert SO.\n",
							moria_cfg->all_defs_str[j]);
#endif
				rho_threshold = snap->rho_threshold_defs[j];
#if MORIA_CAREFUL
				if (rho_threshold < 0.0)
					error(__FFL__, "Found non-SO density threshold for definition %d, %s.\n", j,
							moria_cfg->all_defs_str[j]);
#endif
				if (def->quantity == HDEF_Q_RADIUS)
				{
					hs->hd_defs[j][halo_idx] = soMtoR(sparta_rm_value, rho_threshold);
				} else if (def->quantity == HDEF_Q_MASS)
				{
					hs->hd_defs[j][halo_idx] = soRtoM(sparta_rm_value, rho_threshold);
				} else
				{
					error(__FFL__,
							"Found quantity %d instead of SO definition, cannot convert R/M in definition %s.\n",
							def->quantity, moria_cfg->all_defs_str[j]);
				}
			} else
			{
				hs->hd_defs[j][halo_idx] = sparta_rm_value;
			}
			hs->hd_def_status[j][halo_idx] = MORIA_STATUS_SPARTA_SUCCESS;

			debugHaloMoria(hd->id, "Def %2d %-15s SO from SPARTA %.2e", j,
					moria_cfg->all_defs_str[j], hs->hd_defs[j][halo_idx]);
		} else
		{
			/*
			 * If the status is not SUCCESS, we introduce a sanity check that we are finding some
			 * known error status and not just garbage.
			 */
			if (sparta_def_status == ANL_HPS_STATUS_SO_TOO_SMALL)
			{
				hs->hd_defs[j][halo_idx] = 0.0;
				hs->hd_def_status[j][halo_idx] = MORIA_HPS_STATUS_SO_TOO_SMALL;
			} else if (sparta_def_status == ANL_HPS_STATUS_SO_TOO_LARGE)
			{
				hs->hd_defs[j][halo_idx] = 0.0;
				hs->hd_def_status[j][halo_idx] = MORIA_HPS_STATUS_SO_TOO_LARGE;
			} else if (sparta_def_status == ANL_HPS_STATUS_ORB_ZERO)
			{
				hs->hd_defs[j][halo_idx] = 0.0;
				hs->hd_def_status[j][halo_idx] = MORIA_HPS_STATUS_ORB_ZERO;
			} else
			{
				error(__FFL__,
						"Found invalid HaloProps status %d in definition %s, halo ID %ld, sparta_idx %d, anl_idx %d.\n",
						sparta_def_status, moria_cfg->all_defs_str[j], hd->id, hd->sparta_idx,
						anl_idx);
			}
		}
	}

	/*
	 * Find the correct snapshot index in this analysis. If this snapshot does not exist, throw an
	 * error.
	 */
	anl_snap_idx = sd->anl_rsp.snap_idxs_in_anl[snap->snap_idx];
	if (anl_snap_idx < 0)
		error(__FFL__, "Could not find Rsp analysis at snapshot %d, redshift %.2f.\n",
				snap->snap_idx, moria_cfg->snap_z[snap->snap_idx]);
}
