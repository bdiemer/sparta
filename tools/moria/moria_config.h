/*************************************************************************************************
 *
 * The moria_cfg routines for moria.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_CONFIG_H_
#define _MORIA_CONFIG_H_

#include "moria_global.h"

/*************************************************************************************************
 * CONFIGURATION DEFAULTS
 *************************************************************************************************/

// Input
#define MORIA_DEFAULT_MAIN_DEF_CAT_QUANTITY HDEF_Q_MASS
#define MORIA_DEFAULT_MAIN_DEF_CAT_TYPE HDEF_TYPE_SO
#define MORIA_DEFAULT_MAIN_DEF_CAT_SUBTYPE HDEF_SUBTYPE_SO_MATTER
#define MORIA_DEFAULT_MAIN_DEF_CAT_OVERDENSITY 200.0
#define MORIA_DEFAULT_MAIN_DEF_CAT_PTLSEL HDEF_PTLSEL_BOUND

/*
 * Cosmology. Note that only those parameters have defaults that the user does not have to set
 * manually because they are well constrained. This is not the case for Omega_b, ns, sigma8 etc.
 */
#define MORIA_DEFAULT_COSMO_FLAT 1
#define MORIA_DEFAULT_COSMO_OMEGA_R 0.0
#define MORIA_DEFAULT_COSMO_W -1.0
#define MORIA_DEFAULT_COSMO_T_CMB 2.725
#define MORIA_DEFAULT_COSMO_PS_TYPE POWERSPECTRUM_TYPE_EH98

// Log
#define MORIA_DEFAULT_LOG_LEVEL 1
#define MORIA_DEFAULT_LOG_LEVEL_MEMORY DEFAULT_LOG_LEVEL_MEMORY
#define MORIA_DEFAULT_LOG_LEVEL_TIMING 1
#define MORIA_DEFAULT_LOG_FLUSH 1
#define MORIA_DEFAULT_LOG_SUBSTATUS_DIFF 0

// Error levels
#define MORIA_DEFAULT_ERR_LEVEL_MORE_SNAPS_THAN_CAT ERR_LEVEL_WARNING
#define MORIA_DEFAULT_ERR_LEVEL_SNAP_OUTSIDE_RANGE ERR_LEVEL_WARNING
#define MORIA_DEFAULT_ERR_LEVEL_MAIN_DEF_NOT_200M ERR_LEVEL_WARNING
#define MORIA_DEFAULT_ERR_LEVEL_ACC_RATE_UNDEFINED ERR_LEVEL_WARNING
#define MORIA_DEFAULT_ERR_LEVEL_CAT_FIELD_CHANGED ERR_LEVEL_WARNING

// Algoritm
#define MORIA_DEFAULT_CUT_DEF_QUANTITY HDEF_Q_VCIRC
#define MORIA_DEFAULT_CUT_DEF_TYPE HDEF_TYPE_VMAX
#define MORIA_DEFUALT_CUT_DEF_TIME HDEF_TIME_PEAK
#define MORIA_DEFAULT_CUT_THRESHOLD 1000.0
#define MORIA_DEFAULT_CUT_UNITS MORIA_UNITS_PARTICLES

#define MORIA_DEFAULT_ORDER_DEF_QUANTITY HDEF_Q_VCIRC
#define MORIA_DEFAULT_ORDER_DEF_TYPE HDEF_TYPE_VMAX
#define MORIA_DEFAULT_HOSTSUB_ASSIGN_SUBPARENT_ID 0
#define MORIA_DEFAULT_HOSTSUB_RESTRICT_TO_OUTPUT 0
#define MORIA_DEFAULT_INCLUDE_GHOSTS 1
#define MORIA_DEFAULT_ADJUST_PHANTOM_XV 1
#define MORIA_DEFAULT_MAX_MASS_RATIO_SPARTA 2.0
#define MORIA_DEFAULT_MAX_RSP_MODEL_DIFF_FACTOR 2.0
#define MORIA_DEFAULT_MAX_RSP_MODEL_DIFF_SIGMA 5.0

// Output
#define MORIA_DEFAULT_OUTPUT_RESTART_FILES 1
#define MORIA_DEFAULT_OUTPUT_RESTART_EVERY 10
#define MORIA_DEFAULT_OUTPUT_ORIGINAL 1
#define MORIA_DEFAULT_OUTPUT_HDF5 1
#define MORIA_DEFAULT_OUTPUT_TREE 0
#define MORIA_DEFAULT_SNAP_A -1

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

/*
 * Note the many halo definitions listed in this config. Each of those structs contains an index.
 * For the user-chosen output definitions, order, and cut quantities, those indices point into the
 * all_defs main array. In all_defs itself, the indices may point into SPARTA analyses that also
 * carry a number of definitions.
 */
typedef struct
{
	/*
	 * --------------------------------------------------------------------------------------------
	 * User-defined config parameters
	 * --------------------------------------------------------------------------------------------
	 */

	// Input
	char sparta_file[200];
	char catalog_dir[200];
	HaloDefinition main_def_catalog;
	int rockstar_strict_so;
	int rockstar_bound_props;
	Cosmology cosmo;

	// Console output
	int log_level;
	int log_level_memory;
	int log_level_timing;
	int log_flush;
	int log_substatus_diff;

	// Error levels
	int err_level_more_snaps_than_cat;
	int err_level_snap_outside_range;
	int err_level_main_def_not_200m;
	int err_level_acc_rate_undefined;
	int err_level_cat_field_changed;

	// Algorithm
	HaloDefinition cut_def;
	float cut_threshold_in_units;
	int cut_units;
	float cut_threshold;
	HaloDefinition order_def;
	int hostsub_assign_subparent_id;
	int hostsub_restrict_to_output;
	int include_ghosts;
	int adjust_phantom_xv;
	float max_mass_ratio_sparta;
	float max_rsp_model_diff_factor;
	float max_rsp_model_diff_sigma;

	// Output
	char output_dir_original[200];
	char output_dir_hdf5[200];
	char output_dir_tmp[200];
	char output_filename_tree[200];
	int output_restart_files;
	int output_restart_every;
	int output_original;
	int output_hdf5;
	int output_tree;
	int output_compression_level;
	float output_a[MORIA_MAX_N_A];
	char output_rm_defs_str[MORIA_MAX_DEFS][DEFAULT_HALO_DEF_STRLEN];
	char output_sub_defs_str[MORIA_MAX_DEFS][DEFAULT_HALO_DEF_STRLEN];
	char output_cat_fields[MORIA_MAX_CAT_FIELDS][CAT_FIELD_LENGTH];

	/*
	 * --------------------------------------------------------------------------------------------
	 * From SPARTA config
	 * --------------------------------------------------------------------------------------------
	 */

	// I/O from SPARTA
	int cat_type;
	int output_min_n200m;
	int sparta_has_ghosts;
	int sparta_output_compression_level;

	// Simulation
	float box_size;
	float particle_mass;
	int n_snaps;
	float *snap_a;
	float *snap_z;
	float *snap_t;
	float *snap_t_dyn;
	float *snap_rho_200m;
	float *snap_rho_vir;
	float *snap_rho_catmain;

	/*
	 * --------------------------------------------------------------------------------------------
	 * Derived config parameters
	 * --------------------------------------------------------------------------------------------
	 */
	char output_dir_restart[200];
	char output_dir_treetmp[200];

	HaloDefinition output_rm_defs[MORIA_MAX_DEFS];
	HaloDefinition output_sub_defs[MORIA_MAX_DEFS];
	HaloDefinition all_defs[MORIA_MAX_DEFS_ALL];
	char all_defs_str[MORIA_MAX_DEFS_ALL][DEFAULT_HALO_DEF_STRLEN];

	int n_a;
	int n_defs_rm;
	int n_defs_sub;
	int n_defs_all;
	int n_defs_sparta;
	int n_defs_catalog;
	int n_cat_fields;

	int load_all_halos;
	int do_ghosts;
	int load_sparta_pid;
	int load_sparta_xv;

	/*
	 * Config flags that will change after initialization
	 */
	int warnings_field_name_done;
} MoriaConfig;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void setMoriaConfigDefaults(MoriaConfig *moria_cfg);
void readMoriaConfigFile(char *filename, MoriaConfig *moria_cfg);
void setSpartaConfig(MoriaConfig *moria_cfg);
void processMoriaConfig(MoriaConfig *moria_cfg, SpartaData *sd);
void processMoriaConfigRestart(MoriaConfig *moria_cfg);
void printMoriaConfig(MoriaConfig *moria_cfg, SpartaData *sd);
void freeMoriaConfig(MoriaConfig *moria_cfg);

#endif
