/*************************************************************************************************
 *
 * I/O routines for moria.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/stat.h>

#include "moria_io_halocat.h"
#include "moria_io_halocat_rockstar.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * This function must be implemented by every catalog type. The routine must set mandatory fields
 * such as the halo position, and also the requested catalog halo definitions.
 */
HaloSet* getHaloSetFromCatalog(MoriaConfig *moria_cfg, SnapshotData *snap, int n_halos_extra)
{
	HaloSet *halo_set;

	halo_set = NULL;
	switch (moria_cfg->cat_type)
	{
	case CAT_TYPE_ROCKSTAR:
		halo_set = getHaloSetFromRockstar(moria_cfg, snap, n_halos_extra);
		break;
	default:
		error(__FFL__, "[Main] No routine to read catalog file for catalog type %d.\n",
				moria_cfg->cat_type);
		break;
	}

	return halo_set;
}

/*
 * This function must be implemented by every catalog type. The routine reads additional catalog
 * fields that were specified by the user, and that are to be output into some other output format.
 * This is not done in the initial routine where the halo set is created because the number of
 * halos might have been strongly reduced by now, saving memory. Also, this function does not need
 * to be executed at all if only the native catalog format is output.
 */
void readCatalogFields(MoriaConfig *moria_cfg, HaloSet *hs, int snap_idx)
{
	switch (moria_cfg->cat_type)
	{
	case CAT_TYPE_ROCKSTAR:
		readCatalogFieldsRockstar(moria_cfg, hs, snap_idx);
		break;
	default:
		error(__FFL__, "[Main] No routine to read catalog for catalog type %d.\n",
				moria_cfg->cat_type);
		break;
	}

	return;
}

/*
 * This function must be implemented by every catalog type. The routine outputs a catalog that
 * copies the original catalog file and adds the relevant Sparta data as selected by the user.
 */
void outputOriginal(MoriaConfig *moria_cfg, HaloSet *hs, int snap_idx)
{
	switch (moria_cfg->cat_type)
	{
	case CAT_TYPE_ROCKSTAR:
		outputCatalogRockstar(moria_cfg, hs, snap_idx);
		break;
	default:
		error(__FFL__, "[Main] No routine to output catalog for catalog type %d.\n",
				moria_cfg->cat_type);
		break;
	}

	return;
}
