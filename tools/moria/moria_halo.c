/*************************************************************************************************
 *
 * Routines related to the halo set.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>

#include "moria_halo.h"
#include "moria_config.h"
#include "../../src/memory.h"
#include "../../src/halos/halo.h"
#include "../../src/analyses/analysis_rsp.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Initialize HaloSet. Note that we are not yet allocating the upids at this point, they will be
 * generated once we know the final halo set.
 */
HaloSet* initializeHaloSet(int n_defs, int n_halos)
{
	int i, j;
	HaloSet *hs;

	hs = (HaloSet*) memAlloc(__FFL__, MID_MORIA_HALOS, sizeof(HaloSet));

	hs->n_out_cat = 0;
	hs->n_tot = n_halos;

	hs->hd = (HaloData*) memAlloc(__FFL__, MID_MORIA_HALOS, sizeof(HaloData) * hs->n_tot);
	for (i = 0; i < hs->n_tot; i++)
		initializeHaloData(&(hs->hd[i]));

	hs->hd_defs = (float**) memAlloc(__FFL__, MID_MORIA_HALOS, sizeof(float*) * n_defs);
	for (i = 0; i < n_defs; i++)
	{
		hs->hd_defs[i] = (float*) memAlloc(__FFL__, MID_MORIA_HALOS, sizeof(float) * hs->n_tot);
		memset(hs->hd_defs[i], 0, sizeof(float) * hs->n_tot);
	}

	hs->hd_def_status = (int_least8_t**) memAlloc(__FFL__, MID_MORIA_HALOS,
			sizeof(int_least8_t*) * n_defs);
	for (i = 0; i < n_defs; i++)
	{
		hs->hd_def_status[i] = (int_least8_t*) memAlloc(__FFL__, MID_MORIA_HALOS,
				sizeof(int_least8_t) * hs->n_tot);
		for (j = 0; j < hs->n_tot; j++)
			hs->hd_def_status[i][j] = MORIA_STATUS_UNDEFINED;
	}

	hs->hd_upid_defs = NULL;
	hs->cf = NULL;

	return hs;
}

void freeHaloSet(MoriaConfig *moria_cfg, HaloSet *hs)
{
	int i;

	if (hs->n_tot > 0)
	{
		memFree(__FFL__, MID_MORIA_HALOS, hs->hd, sizeof(HaloData) * hs->n_tot);

		if (hs->out_mask != NULL)
			memFree(__FFL__, MID_MORIA_HALOS, hs->out_mask, sizeof(int_least8_t) * hs->n_tot);

		if (hs->hd_defs != NULL)
		{
			for (i = 0; i < moria_cfg->n_defs_all; i++)
				memFree(__FFL__, MID_MORIA_HALOS, hs->hd_defs[i], sizeof(float) * hs->n_tot);
			memFree(__FFL__, MID_MORIA_HALOS, hs->hd_defs, sizeof(float*) * moria_cfg->n_defs_all);
		}

		if (hs->hd_def_status != NULL)
		{
			for (i = 0; i < moria_cfg->n_defs_all; i++)
				memFree(__FFL__, MID_MORIA_HALOS, hs->hd_def_status[i],
						sizeof(int_least8_t) * hs->n_tot);
			memFree(__FFL__, MID_MORIA_HALOS, hs->hd_def_status,
					sizeof(int_least8_t*) * moria_cfg->n_defs_all);
		}

		if (hs->hd_upid_defs != NULL)
		{
			for (i = 0; i < moria_cfg->n_defs_sub; i++)
				memFree(__FFL__, MID_MORIA_HALOS, hs->hd_upid_defs[i],
						sizeof(long int) * hs->n_tot);
			memFree(__FFL__, MID_MORIA_HALOS, hs->hd_upid_defs,
					sizeof(long int*) * moria_cfg->n_defs_sub);
		}
	}

	if (hs->cf != NULL)
	{
		for (i = 0; i < moria_cfg->n_cat_fields; i++)
			freeNDArray(__FFL__, MID_MORIA_CAT, &(hs->cf[i]));
		memFree(__FFL__, MID_MORIA_CAT, hs->cf, sizeof(NDArray) * moria_cfg->n_cat_fields);
	}
	memFree(__FFL__, MID_MORIA_HALOS, hs, sizeof(HaloSet));
}

void initializeHaloData(HaloData *hd)
{
	int i;

	hd->do_output = 0;
	hd->status_sparta = HALO_STATUS_NONE;
	hd->status_sparta_rsp = ANL_RSP_STATUS_UNDEFINED;
	hd->status_moria_rsp = MORIA_STATUS_UNDEFINED;
	hd->status_acc_rate = ACCRATE_STATUS_UNDEFINED;

	hd->sparta_idx = INVALID_IDX;
	hd->id = INVALID_ID;
	hd->cat_upid = INVALID_ID;
	hd->desc_id = INVALID_ID;
	hd->mmp = 0;
	hd->phantom = 0;

	for (i = 0; i < 3; i++)
	{
		hd->x[i] = 0.0;
		hd->v[i] = 0.0;
	}

	hd->R200m_all_spa = INVALID_R;
	hd->M200m_all_spa = INVALID_M;
	hd->Rmain_cat = INVALID_R;
	hd->Mmain_cat = INVALID_M;
	hd->nu200m = INVALID_NU;
	hd->acc_rate_200m_dyn = INVALID_ACCRATE;
}

/*
 * Count the ghosts in this snapshot.
 */
int countGhostHalos(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap)
{
	int i, snap_idx, n_ghosts;

	snap_idx = snap->snap_idx;
	n_ghosts = 0;
	for (i = 0; i < sd->n_halos; i++)
	{
		if (isGhostStatus(sd->sparta_status[i][snap_idx]))
			n_ghosts++;
	}

	return n_ghosts;
}

/*
 * Take ghost halos from SPARTA data and create new halos in the halo set, as if they had been in
 * the catalog. This way, we do not need to worry about the output mask or catalog fields, which
 * are allocated later.
 */
void addGhostHalos(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		int n_ghosts)
{
	if (n_ghosts == 0)
		return;

	int i, j, k, d, snap_idx;
	int_least8_t status;
	HaloData *new_hd;

	/*
	 * Add ghosts to halo set. Here, we need to only set the halo ID and SPARTA index, the rest of
	 * the properties will be added later together with all the other halos.
	 *
	 * However, this halo comes without certain catalog properties of course. We reconstruct those
	 * to guarantee that the merger trees will work.
	 */
	snap_idx = snap->snap_idx;
	j = hs->n_cat;
	for (i = 0; i < sd->n_halos; i++)
	{
		status = sd->sparta_status[i][snap_idx];
		if (!isGhostStatus(status))
			continue;

		new_hd = &(hs->hd[j]);

		new_hd->sparta_idx = i;
		new_hd->id = sd->halo_ids[i][snap_idx];

		/*
		 * We need to mimic the catalog's logic for parent and descendant ID. If we are at the
		 * final snapshot, there is obviously no descendant. If we are not, we use the SPARTA
		 * history to check whether this is the ghost's final snapshot.
		 */
		new_hd->cat_upid = sd->halo_pids[i][snap_idx];
		if (snap_idx == moria_cfg->n_snaps - 1)
		{
			new_hd->desc_id = INVALID_ID;
			new_hd->mmp = 1;
		} else
		{
			new_hd->desc_id = sd->halo_ids[i][snap_idx + 1];
			if (new_hd->desc_id == INVALID_ID)
				new_hd->mmp = 0;
			else
				new_hd->mmp = 1;
		}

		/*
		 * Set position and velocity from SPARTA, there is no ambiguity here since there is no
		 * catalog data
		 */
		for (d = 0; d < 3; d++)
		{
			new_hd->x[d] = sd->halo_x[i][snap_idx][d];
			new_hd->v[d] = sd->halo_v[i][snap_idx][d];
		}

		/*
		 * Set invalid for definitions that are loaded from catalog for normal halos
		 */
		for (k = 0; k < moria_cfg->n_defs_all; k++)
		{
			if (moria_cfg->all_defs[k].source == HDEF_SOURCE_CATALOG)
				hs->hd_defs[k][j] = INVALID_R;
		}

		debugHaloMoria(new_hd->id,
				"Added ghost ID %18ld, SPARTA idx %4d, pid %8ld, desc_id %18ld, mmp %d, x [%.1f %.1f %.1f].",
				new_hd->id, new_hd->sparta_idx, new_hd->cat_upid, new_hd->desc_id, new_hd->mmp,
				new_hd->x[0], new_hd->x[1], new_hd->x[2]);

		j++;
	}

	if (j != hs->n_tot)
		error(__FFL__, "Unexpected index.\n");
}
