/*************************************************************************************************
 *
 * This unit writes and reads restart files, i.e., memory dumps, for MORIA.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <hdf5.h>
#include <time.h>

#include "moria_io_restart.h"
#include "../../src/io/io_restart.h"
#include "../../src/memory.h"

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void moriaRestartFilePath(MoriaConfig *moria_cfg, char *str);
void moriaRestartFileName(MoriaConfig *moria_cfg, char *str, int snap_idx);

void readWriteSpartaAnalysis(FILE *f, int rw, AnalysisData *al, int n_snaps, int n_halos);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void moriaRestartFilePath(MoriaConfig *moria_cfg, char *str)
{
	sprintf(str, "%s", moria_cfg->output_dir_restart);
}

void moriaRestartFileName(MoriaConfig *moria_cfg, char *str, int snap_idx)
{
	char path[200];

	moriaRestartFilePath(moria_cfg, path);
	sprintf(str, "%s/restart_%04d.bin", path, snap_idx);
}

int moriaGetRestartFileNumber(char *filename)
{
	int idx;

	idx = -1;
	sscanf(filename, "restart_%d", &idx);

	return idx;
}

void readWriteSpartaAnalysis(FILE *f, int rw, AnalysisData *al, int n_snaps, int n_halos)
{
	int i;

	readWriteOperationAllocate(f, rw, (void**) &(al->snap_idxs), sizeof(int) * al->n_snaps,
			MID_MORIA_SPARTA);

#if MORIA_CAREFUL
	for (i = 0; i < al->n_snaps; i++)
	{
		if ((al->snap_idxs[i] < 0) || (al->snap_idxs[i] >= n_snaps))
			error(__FFL__,
					"During restart, found invalid snapshot index %d (max %d) in analysis snap indices.\n",
					al->snap_idxs[i], n_snaps - 1);
	}
#endif

	readWriteOperationAllocate(f, rw, (void**) &(al->snap_idxs_in_anl), sizeof(int) * n_snaps,
			MID_MORIA_SPARTA);
	readWriteOperationAllocate(f, rw, (void**) &(al->anl_idx), sizeof(long int) * n_halos,
			MID_MORIA_SPARTA);

	if (al->anl_status_loaded)
	{
		readWriteOperationAllocate2D(f, rw, (void***) &(al->status), sizeof(int_least8_t),
				al->n_anl, al->n_snaps, MID_MORIA_SPARTA);
	}

	for (i = 0; i < al->n_datasets; i++)
	{
		readWriteNDArray(f, rw, &(al->datasets[i]), MID_MORIA_SPARTA);
	}
}

void readWriteAttributes(FILE *f, int rw, Hdf5Attribute *attrs, int n_attrs, int mem_id)
{
	int i;

	for (i = 0; i < n_attrs; i++)
	{
		if (attrs[i].n_alloc > 0)
			readWriteOperationAllocate(f, rw, &(attrs[i].p_void),
					attrs[i].n_alloc * datatypeSize(attrs[i].dtype), mem_id);
	}
}

void moriaReadOrWriteRestartFiles(int rw, int *snap_idx, MoriaConfig *moria_cfg,
		MoriaTimers *timers, SpartaData **sd, TreeSet **ts)
{
	int i, reading, writing, file_idx, n_files_tot, n_snp, n_h;
	clock_t cur_time, time_tmp;
	FILE *f;
	char fn1[400], fn2[400];
	struct dirent **files;
	MemoryStatistics mem_stats_tmp;
	SpartaData *sdp;

	// Shortcuts
	reading = (rw == 0);
	writing = (rw == 1);

	/*
	 * If reading, we need to find the last valid restart file.
	 */
	if (reading)
	{
		// Load list of files in restart path
		moriaRestartFilePath(moria_cfg, fn1);
		n_files_tot = scandir(fn1, &files, 0, alphasort);
		if (n_files_tot < 0)
			error(__FFL__, "[Main] Could not open restart directory '%s'.\n", fn1);
		if (n_files_tot == 0)
			error(__FFL__, "[Main] Did not find any restart files in restart directory '%s'.\n",
					fn1);

		// Find highest snapshot index; due to alphabetical sorting, that should be the last file
		file_idx = moriaGetRestartFileNumber(files[n_files_tot - 1]->d_name);
		output(0, "Restarting from snapshot index %d.\n", file_idx);
	} else
	{
		file_idx = *snap_idx;
	}

	/*
	 * If reading, we need to be careful with the memory statistics as they will be a combination
	 * of the current and stored fields. We back up the current field into a temporary variable.
	 *
	 * If writing, we initialize the mem_stats_tmp field to avoid compiler warnings.
	 */
	if (reading)
		memcpy(&mem_stats_tmp, &mem_stats, sizeof(MemoryStatistics));
	else
		memset(&mem_stats_tmp, 0, sizeof(MemoryStatistics));

	/*
	 * Open file
	 */
	moriaRestartFileName(moria_cfg, fn1, file_idx);
	if (reading)
	{
		output(2, "Reading restart file %s\n", fn1);
		f = fopen(fn1, "rb");
	} else
	{
		f = fopen(fn1, "wb");
	}
	if (f == NULL)
		error(__FFL__, "Could not open restart file %s.\n", fn1);

	/*
	 * Read/write 1: SPARTA config
	 *
	 * We do this first so that we recover settings such as the log level.
	 */
	readWriteOperation(f, rw, &config, sizeof(ConfigData));

	/*
	 * Read/write 2: Timers
	 *
	 * If writing, we need to fix the timing statistics before writing them to disk; otherwise,
	 * they contain random timing values. The logic is that during writing, the current values
	 * should be changed only temporarily, meaning the same time should be added and subtracted.
	 *
	 * While reading, we preseve the INIT time since that has been running and add it to the
	 * init time from the original run.
	 */
	if (reading)
		output(2, "Reading timers...\n");
	cur_time = clock();
	time_tmp = 0.0;
	if (writing)
	{
		timers->timers_0[MORIA_T0_SNAPS] += cur_time;
		timers->timers_1[MORIA_T1_RESTART] += cur_time;
	}
	if (reading)
		time_tmp = timers->timers_0[MORIA_T0_INIT];
	readWriteOperation(f, rw, timers, sizeof(MoriaTimers));
	if (writing)
	{
		timers->timers_0[MORIA_T0_SNAPS] -= cur_time;
		timers->timers_1[MORIA_T1_RESTART] -= cur_time;
	}
	if (reading)
	{
		timers->timers_0[MORIA_T0_INIT] += time_tmp;
		resetTimersSnapshot(timers);
	}

	/*
	 * Read/write 3: Memory statistics
	 *
	 * Deal with memory statistics before allocating global memory
	 */
	if (reading)
		output(2, "Reading memory statistics...\n");
	readWriteOperation(f, rw, &mem_stats, sizeof(MemoryStatistics));
	if (reading)
	{
		mem_stats.cur_tot = mem_stats_tmp.cur_tot;
		for (i = 0; i < MID_N; i++)
			mem_stats.current[i] = mem_stats_tmp.current[i];
	}

	/*
	 * Read/write 4: MORIA config
	 *
	 * We reset the spline interpolation in the cosmology.
	 */
	readWriteOperation(f, rw, snap_idx, sizeof(int));
	readWriteOperation(f, rw, moria_cfg, sizeof(MoriaConfig));

	readWriteOperationAllocate(f, rw, (void**) &(moria_cfg->snap_a),
			sizeof(float) * moria_cfg->n_snaps, MID_MORIA_CONFIG);
	readWriteOperationAllocate(f, rw, (void**) &(moria_cfg->snap_z),
			sizeof(float) * moria_cfg->n_snaps, MID_MORIA_CONFIG);
	readWriteOperationAllocate(f, rw, (void**) &(moria_cfg->snap_t),
			sizeof(float) * moria_cfg->n_snaps, MID_MORIA_CONFIG);
	readWriteOperationAllocate(f, rw, (void**) &(moria_cfg->snap_t_dyn),
			sizeof(float) * moria_cfg->n_snaps, MID_MORIA_CONFIG);
	readWriteOperationAllocate(f, rw, (void**) &(moria_cfg->snap_rho_200m),
			sizeof(float) * moria_cfg->n_snaps, MID_MORIA_CONFIG);
	readWriteOperationAllocate(f, rw, (void**) &(moria_cfg->snap_rho_vir),
			sizeof(float) * moria_cfg->n_snaps, MID_MORIA_CONFIG);
	readWriteOperationAllocate(f, rw, (void**) &(moria_cfg->snap_rho_catmain),
			sizeof(float) * moria_cfg->n_snaps, MID_MORIA_CONFIG);

	if (reading)
	{
		resetCosmology(&(moria_cfg->cosmo));
		resetCosmologySplines(&(moria_cfg->cosmo));
	}

	/*
	 * Read/write 5: SPARTA data
	 */
	if (reading)
		output(2, "Reading SPARTA data...\n");

	readWriteOperationAllocate(f, rw, (void**) sd, sizeof(SpartaData), MID_MORIA_SPARTA);

	sdp = *sd;
	n_snp = moria_cfg->n_snaps;
	n_h = sdp->n_halos;

	if (reading)
		output(2, "Found %d halos and %d snapshots...\n", n_h, n_snp);

	readWriteOperationAllocate(f, rw, (void**) &(sdp->cat_path), sizeof(char) * sdp->n_cat_path,
			MID_MORIA_SPARTA);
	readWriteOperationAllocate(f, rw, (void**) &(sdp->out_path), sizeof(char) * sdp->n_out_path,
			MID_MORIA_SPARTA);
	readWriteOperationAllocate2D(f, rw, (void***) &(sdp->sparta_status), sizeof(int_least8_t), n_h,
			n_snp, MID_MORIA_SPARTA);
	readWriteOperationAllocate(f, rw, (void**) &(sdp->sparta_status_final),
			sizeof(int_least8_t) * n_h, MID_MORIA_SPARTA);
	readWriteOperationAllocate2D(f, rw, (void***) &(sdp->halo_ids), sizeof(long int), n_h, n_snp,
			MID_MORIA_SPARTA);

	readWriteOperationAllocate2D(f, rw, (void***) &(sdp->halo_R200m), sizeof(float), n_h, n_snp,
			MID_MORIA_SPARTA);

	if (moria_cfg->load_sparta_pid)
	{
		readWriteOperationAllocate2D(f, rw, (void***) &(sdp->halo_pids), sizeof(long int), n_h,
				n_snp, MID_MORIA_SPARTA);
	}

	if (moria_cfg->load_sparta_xv)
	{
		readWriteOperationAllocate3D(f, rw, (void****) &(sdp->halo_x), sizeof(float), n_h, n_snp, 3,
				MID_MORIA_SPARTA);
		readWriteOperationAllocate3D(f, rw, (void****) &(sdp->halo_v), sizeof(float), n_h, n_snp, 3,
				MID_MORIA_SPARTA);
	}

	if (sdp->anl_rsp.anl_is_loaded)
	{
		readWriteSpartaAnalysis(f, rw, &(sdp->anl_rsp), n_snp, n_h);
	}

	if (sdp->anl_hps.anl_is_loaded)
	{
		readWriteSpartaAnalysis(f, rw, &(sdp->anl_hps), n_snp, n_h);
	}

	readWriteAttributes(f, rw, sdp->config_main, sdp->n_config_main, MID_MORIA_SPARTA);
	readWriteAttributes(f, rw, sdp->config_compile, sdp->n_config_compile, MID_MORIA_SPARTA);
	readWriteAttributes(f, rw, sdp->config_rsp, sdp->n_config_rsp, MID_MORIA_SPARTA);
	readWriteAttributes(f, rw, sdp->config_hps, sdp->n_config_hps, MID_MORIA_SPARTA);

	/*
	 * Read/write 6: Tree Set
	 */
	if (moria_cfg->output_tree)
	{
		if (reading)
			output(2, "Reading tree data...\n");
		readWriteOperationAllocate(f, rw, (void**) ts, sizeof(TreeSet), MID_MORIA_TREE);
		readWriteOperationAllocate(f, rw, (void**) &((*ts)->th), sizeof(TreeHalo) * (*ts)->n_alloc,
				MID_MORIA_TREE);
		if (reading)
		{
			(*ts)->halo_tree_idxs = NULL;
			(*ts)->tree_out_idxs = NULL;
		}
	}

	/*
	 * Done with file reading / writing. At this point, the main process needs to check on the
	 * child process that is copying the HDF5 output file to/from the restart folder.
	 */
	fclose(f);

	/*
	 * Remove old files
	 */
	if (writing)
	{
		moriaRestartFilePath(moria_cfg, fn1);
		n_files_tot = scandir(fn1, &files, 0, alphasort);
		for (i = 0; i < n_files_tot; i++)
		{
			file_idx = moriaGetRestartFileNumber(files[i]->d_name);
			if ((file_idx >= 0) && (file_idx < *snap_idx))
			{
				sprintf(fn2, "%s/%s", fn1, files[i]->d_name);
				output(4, "Removing outdated restart file %s.\n", fn2);
				remove(fn2);
			}
		}
	}

	if (reading)
		printLine(0);
}
