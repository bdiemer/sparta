/*************************************************************************************************
 *
 * The main work routines of moria.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_RUN_H_
#define _MORIA_RUN_H_

#include "moria_global.h"
#include "moria_config.h"
#include "moria_statistics.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void moriaProcessSimulation(MoriaConfig *moria_cfg, MoriaTimers *timers, SpartaData **sd_p,
		int restart);

#endif
