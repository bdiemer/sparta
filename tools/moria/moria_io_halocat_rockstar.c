/*************************************************************************************************
 *
 * I/O routines for Rockstar.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/stat.h>

#include "moria_global.h"
#include "moria_halo.h"
#include "moria_io.h"
#include "moria_io_halocat_rockstar.h"
#include "../models/models_rsp.h"
#include "../../src/global_types.h"
#include "../../src/memory.h"
#include "../../src/halos/halo.h"
#include "../../src/halos/halo_definitions.h"
#include "../../src/halos/halo_so.h"
#include "../../src/halos/halo_ghosts.h"
#include "../../src/io/io_hdf5.h"
#include "../../src/io/io_halocat_rockstar.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * The maximum length of a catalog line
 */
#define CAT_LINE_NCHAR 5000

/*
 * Catalog fields to be read when parsing a line. When adding to this array, the function
 * parseCatalogLine() must be updated as well.
 *
 * Note that this list contains only those fields that are essential for moria, not all fields
 * one might be interested in. Additional fields can be added via the config.
 */
#define MORIA_CAT_FIELDS \
ENUM(MORIA_CAT_ID, id)\
ENUM(MORIA_CAT_DESC_ID, desc_id)\
ENUM(MORIA_CAT_UPID, upid)\
ENUM(MORIA_CAT_MMP, mmp?)\
ENUM(MORIA_CAT_PHANTOM, phantom)\
ENUM(MORIA_CAT_X, x)\
ENUM(MORIA_CAT_Y, y)\
ENUM(MORIA_CAT_Z, z)\
ENUM(MORIA_CAT_VX, vx)\
ENUM(MORIA_CAT_VY, vy)\
ENUM(MORIA_CAT_VZ, vz)

#define ENUM(x, y) x,
enum
{
	MORIA_CAT_FIELDS/*space*/MORIA_N_CAT_FIELDS
};
#undef ENUM

#define ENUM(x, y) #y,
const char *MORIA_CATALOG_FIELD_NAMES[MORIA_N_CAT_FIELDS] =
	{MORIA_CAT_FIELDS};
#undef ENUM

/*
 * Known data types; float and int are easy to distinguish from the text files, but int and
 * long are not.
 */
#define MORIA_KNOWN_CAT_TYPES \
ENUM(id, DTYPE_INT64)\
ENUM(desc_id, DTYPE_INT64)\
ENUM(upid, DTYPE_INT64)\
ENUM(desc_pid, DTYPE_INT64)\
ENUM(Breadth_first_ID, DTYPE_INT64)\
ENUM(Depth_first_ID, DTYPE_INT64)\
ENUM(Tree_root_ID, DTYPE_INT64)\
ENUM(Orig_halo_ID, DTYPE_INT64)\
ENUM(Next_coprogenitor_depthfirst_ID, DTYPE_INT64)\
ENUM(Last_progenitor_depthfirst_ID, DTYPE_INT64)\
ENUM(Last_mainleaf_depthfirst_ID, DTYPE_INT64)\
ENUM(Tidal_ID, DTYPE_INT64)\
ENUM(Future_merger_MMP_ID, DTYPE_INT64)

#define ENUM(x, y) KNOWN##x,
enum
{
	MORIA_KNOWN_CAT_TYPES/*space*/MORIA_N_KNOWN_CAT_TYPES
};
#undef ENUM

#define ENUM(x, y) #x,
const char *MORIA_KNOWN_CAT_TYPE_NAMES[MORIA_N_KNOWN_CAT_TYPES] =
	{MORIA_KNOWN_CAT_TYPES};
#undef ENUM

#define ENUM(x, y) y,
const int MORIA_KNOWN_CAT_TYPE_TYPES[MORIA_N_KNOWN_CAT_TYPES] =
	{MORIA_KNOWN_CAT_TYPES};
#undef ENUM

enum
{
	STEP_HEADER, STEP_COMMENTS, STEP_LINES
};

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void parseCatalogLine(char *line, double *values, int n_fields, char *parse_str, int *field_order);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

HaloSet* getHaloSetFromRockstar(MoriaConfig *moria_cfg, SnapshotData *snap, int n_halos_extra)
{
	int i, j, k, n_fields_load, convert_status[MORIA_MAX_CAT_FIELDS], *field_order;
	float rho_threshold;
	double cat_values[MORIA_MAX_CAT_FIELDS];
	char cat_name[100], line[CAT_LINE_NCHAR], parse_str[3000],
			cat_fields[MORIA_MAX_CAT_FIELDS][CAT_FIELD_LENGTH];
	FILE *f_in;
	HaloSet *hs;

	/*
	 * Run through config, find out fields that need to be loaded. This includes the default list
	 * as well as halo definitions.
	 */
	for (i = 0; i < MORIA_N_CAT_FIELDS; i++)
	{
		sprintf(cat_fields[i], "%s", MORIA_CATALOG_FIELD_NAMES[i]);
		convert_status[i] = ROCKSTAR_CONVERT_STATUS_NONE;
	}
	j = MORIA_N_CAT_FIELDS;

	for (i = 0; i < j; i++)
		output(3, "    Catalog field %2d is %-15s\n", i, cat_fields[i]);

	for (i = 0; i < moria_cfg->n_defs_all; i++)
	{
		if (moria_cfg->all_defs[i].source == HDEF_SOURCE_CATALOG)
		{
			if (j >= MORIA_MAX_CAT_FIELDS)
				error(__FFL__,
						"Too many catalog fields to load (%d, max %d). Increase MORIA_MAX_CAT_FIELDS.\n",
						j,
						MORIA_MAX_CAT_FIELDS);
			convert_status[i] = haloDefinitionToRockstar(moria_cfg->all_defs[i],
					moria_cfg->main_def_catalog, moria_cfg->rockstar_strict_so,
					moria_cfg->rockstar_bound_props, cat_fields[j]);
			output(3, "    Catalog field %2d is %-15s (definition %-15s convert R/M %d)\n", j,
					cat_fields[j], moria_cfg->all_defs_str[i], convert_status[i]);
			j++;
		}
	}

	n_fields_load = j;
	if (n_fields_load != MORIA_N_CAT_FIELDS + moria_cfg->n_defs_catalog)
		error(__FFL__, "Expected %d fields to load from catalog, found %d.\n",
				MORIA_N_CAT_FIELDS + moria_cfg->n_defs_catalog, n_fields_load);

	/*
	 * Run through halos #1: Open catalog file, parse header.
	 */
	getCatalogFileNameRockstar(snap->a, cat_name);
	f_in = fopen(cat_name, "r");
	if (f_in == NULL)
		error(__FFL__, "Could not open catalog file %s.\n", cat_name);
	fgets(line, CAT_LINE_NCHAR, f_in);
	field_order = (int*) memAlloc(__FFL__, MID_MORIA_TMP, sizeof(int) * n_fields_load);
	parseCatalogHeaderRockstar(line, cat_fields, n_fields_load, parse_str, &field_order, NULL);

	/*
	 * Run through file, count halos
	 */
	i = 0;
	while (fgets(line, CAT_LINE_NCHAR, f_in) != NULL)
	{
		if (line[0] != '#')
			i++;
	}

	/*
	 * Create the halo set. Here, we need n_cat HaloData and (n_defs * n_cat) defs fields.
	 */
	hs = initializeHaloSet(moria_cfg->n_defs_all, i + n_halos_extra);
	hs->n_cat = i;

	/*
	 * Run through file again, this time parsing lines.
	 */
	fseek(f_in, 0, SEEK_SET);

	i = 0;
	while (fgets(line, CAT_LINE_NCHAR, f_in) != NULL)
	{
		if (line[0] == '#')
			continue;

		/*
		 * Read the catalog line, convert to values
		 */
		parseCatalogLine(line, cat_values, n_fields_load, parse_str, field_order);

		/*
		 * Set output data. We cannot assume that the catalog fields are contiguous in the memory
		 * layout. Note that the last snapshot in MORIA is not necessarily the last snapshot in the
		 * simulation, meaning that mmp can be 0 according to the catalog, which makes no sense.
		 * Instead, we force it to be 1 at the final snapshot. Likewise, if SPARTA did not run all
		 * the way to the end of the simulation, there can be desc_ids that have no equivalent
		 * anywhere.
		 */
		hs->hd[i].id = (HaloID) cat_values[MORIA_CAT_ID];
		hs->hd[i].cat_upid = (HaloID) cat_values[MORIA_CAT_UPID];

		if (snap->snap_idx == moria_cfg->n_snaps - 1)
		{
			hs->hd[i].mmp = 1;
			hs->hd[i].desc_id = INVALID_ID;
		} else
		{
			hs->hd[i].mmp = (int_least8_t) cat_values[MORIA_CAT_MMP];
			hs->hd[i].desc_id = (HaloID) cat_values[MORIA_CAT_DESC_ID];
		}
		hs->hd[i].phantom = (int_least8_t) cat_values[MORIA_CAT_PHANTOM];
		hs->hd[i].x[0] = (float) cat_values[MORIA_CAT_X];
		hs->hd[i].x[1] = (float) cat_values[MORIA_CAT_Y];
		hs->hd[i].x[2] = (float) cat_values[MORIA_CAT_Z];
		hs->hd[i].v[0] = (float) cat_values[MORIA_CAT_VX];
		hs->hd[i].v[1] = (float) cat_values[MORIA_CAT_VY];
		hs->hd[i].v[2] = (float) cat_values[MORIA_CAT_VZ];

		k = MORIA_N_CAT_FIELDS;
		for (j = 0; j < moria_cfg->n_defs_all; j++)
		{
			if (moria_cfg->all_defs[j].source == HDEF_SOURCE_CATALOG)
			{
				hs->hd_defs[j][i] = cat_values[k];
				debugHaloMoria(hs->hd[i].id,
						"Assigning def %2d, catalog def %2d, %-15s cat field %-12s convert %d, value %.4e.",
						j, k - MORIA_N_CAT_FIELDS, moria_cfg->all_defs_str[j], cat_fields[k],
						convert_status[j], hs->hd_defs[j][i]);
				k++;
			}
		}
		i++;
	}

	fclose(f_in);

	/*
	 * Convert SO definitions back from mass if necessary. Radii are in comoving units in Rockstar,
	 * so we need to convert them to physical.
	 */
	for (j = 0; j < moria_cfg->n_defs_all; j++)
	{
		if (moria_cfg->all_defs[j].source == HDEF_SOURCE_CATALOG)
		{
			if (convert_status[j] == ROCKSTAR_CONVERT_STATUS_M_TO_R)
			{
				rho_threshold = snap->rho_threshold_defs[j];
#if MORIA_CAREFUL
				if (rho_threshold < 0.0)
					error(__FFL__, "Found non-SO density threshold for definition %d, %s.\n", j,
							moria_cfg->all_defs_str[j]);
#endif
				for (i = 0; i < hs->n_cat; i++)
				{
					hs->hd_defs[j][i] = soMtoR(hs->hd_defs[j][i], rho_threshold);

					debugHaloMoria(hs->hd[i].id,
							"Converting def %2d, %-15s to radius %.4e, threshold %.5e.", j,
							moria_cfg->all_defs_str[j], hs->hd_defs[j][i], rho_threshold);
				}

			} else if (moria_cfg->all_defs[j].quantity == HDEF_Q_RADIUS)
			{
				for (i = 0; i < hs->n_cat; i++)
					hs->hd_defs[j][i] *= snap->a;
			}
		}
	}

	memFree(__FFL__, MID_MORIA_TMP, field_order, sizeof(int) * n_fields_load);

	return hs;
}

void readCatalogFieldsRockstar(MoriaConfig *moria_cfg, HaloSet *hs, int snap_idx)
{
	int i, j, k, idx, *field_order, *field_idxs, n_load;
	double cat_values[MORIA_MAX_CAT_FIELDS];
	char parse_str[3000], *cp, *line_pt, cat_name[100], line[CAT_LINE_NCHAR],
			line_header[CAT_LINE_NCHAR];
	FILE *f_in;

	/*
	 * Create data structure for fields to be read
	 */
	if (hs->cf != NULL)
		error(__FFL__, "Catalog data already allocated.\n");

	if (moria_cfg->load_all_halos)
		n_load = hs->n_tot;
	else
		n_load = hs->n_out_cat;

	hs->cf = (NDArray*) memAlloc(__FFL__, MID_MORIA_CAT, sizeof(NDArray) * moria_cfg->n_cat_fields);
	for (i = 0; i < moria_cfg->n_cat_fields; i++)
	{
		initializeNDArray(&(hs->cf[i]));
		hs->cf[i].n_dim = 1;
		hs->cf[i].n[0] = n_load;
	}

	/*
	 * Open file, read first line and parse for cat fields
	 */
	getCatalogFileNameRockstar(moria_cfg->snap_a[snap_idx], cat_name);
	f_in = fopen(cat_name, "r");
	if (f_in == NULL)
		error(__FFL__, "Could not open catalog file %s.\n", cat_name);

	fgets(line_header, CAT_LINE_NCHAR, f_in);
	field_order = (int*) memAlloc(__FFL__, MID_MORIA_TMP, sizeof(int) * moria_cfg->n_cat_fields);
	field_idxs = (int*) memAlloc(__FFL__, MID_MORIA_TMP, sizeof(int) * moria_cfg->n_cat_fields);
	parseCatalogHeaderRockstar(line_header, moria_cfg->output_cat_fields, moria_cfg->n_cat_fields,
			parse_str, &field_order, &field_idxs);

	/*
	 * Run through comment lines, load first line with an actual halo to get formats. We simply
	 * look for '.' in floats, if it is not there we assume it's an int.
	 */
	while (fgets(line_header, CAT_LINE_NCHAR, f_in) != NULL)
	{
		if (line_header[0] != '#')
			break;
	}
	strcpy(line, line_header);
	line_pt = line_header;
	idx = 0;
	while (1)
	{
		cp = strsep(&line_pt, " ");
		if (cp == NULL)
			break;
		if (strlen(cp) == 0)
			continue;
		for (i = 0; i < moria_cfg->n_cat_fields; i++)
		{
			if (idx == field_idxs[i])
			{
				if (strchr(cp, '.'))
					hs->cf[i].dtype = DTYPE_FLOAT;
				else
					hs->cf[i].dtype = DTYPE_INT32;
			}
		}
		idx++;
	}

	/*
	 * Check for fields that are in known types
	 */
	for (i = 0; i < moria_cfg->n_cat_fields; i++)
	{
		for (j = 0; j < MORIA_N_KNOWN_CAT_TYPES; j++)
		{
			if (strcmp(moria_cfg->output_cat_fields[i], MORIA_KNOWN_CAT_TYPE_NAMES[j]) == 0)
				hs->cf[i].dtype = MORIA_KNOWN_CAT_TYPE_TYPES[j];
		}
	}

	/*
	 * Reserve the appropriate memory for each catalog field
	 */
	for (i = 0; i < moria_cfg->n_cat_fields; i++)
		allocateNDArray(__FFL__, MID_MORIA_CAT, &(hs->cf[i]));

	/*
	 * Output information about the loaded fields.
	 */
	for (i = 0; i < moria_cfg->n_cat_fields; i++)
	{
		output(3, "    Loading catalog field %-35s position %2d, idx %2d, type %d\n",
				moria_cfg->output_cat_fields[i], field_order[i], field_idxs[i], hs->cf[i].dtype);
	}

	/*
	 * Run through all lines in the file. If the halo is in the output, parse the line and
	 * assign the fields to the correct pointer type. Note that the index in the halo set depends
	 * on whether we are outputting all halos (in which case the cf array has the same size as
	 * all catalog halos and k=j), or whether we are only outputting certain halos (in which case
	 * the cf has dimensions of n_cat_out).
	 */
	j = 0;
	k = 0;
	while (1)
	{
		if (moria_cfg->load_all_halos || (hs->out_mask[j]))
		{
			parseCatalogLine(line, cat_values, moria_cfg->n_cat_fields, parse_str, field_order);

			for (i = 0; i < moria_cfg->n_cat_fields; i++)
			{
				switch (hs->cf[i].dtype)
				{
				case DTYPE_INT32:
					hs->cf[i].q_int32[k] = (int) cat_values[i];
					break;
				case DTYPE_INT64:
					hs->cf[i].q_int64[k] = (long int) cat_values[i];
					break;
				case DTYPE_FLOAT:
					hs->cf[i].q_float[k] = (float) cat_values[i];
					break;
				default:
					error(__FFL__, "Data type %d not allowed for catalog field.\n",
							hs->cf[i].dtype);
					break;
				}
			}

			debugHaloMoria(hs->hd[j].id, "Assigning catalog fields.");
			k++;
		}

		j++;
		if (fgets(line, CAT_LINE_NCHAR, f_in) == NULL)
			break;
	}

	fclose(f_in);

	/*
	 * Sanity check: once we have run through the catalog halos, all other halos must be ghosts.
	 */
#if MORIA_CAREFUL
	for (i = j; i < hs->n_tot; i++)
	{
		if (!isGhostID(hs->hd[i].id))
			error(__FFL__, "Found non-ghost halo after catalog halos (idx %d/%d, ID %ld).\n", i,
					hs->n_tot, hs->hd[i].id);
	}
#endif

	memFree(__FFL__, MID_MORIA_TMP, field_order, sizeof(int) * moria_cfg->n_cat_fields);
	memFree(__FFL__, MID_MORIA_TMP, field_idxs, sizeof(int) * moria_cfg->n_cat_fields);
}

/*
 * This function adds fields either to the header or a standard catalog line. Those operations are
 * combined into one function to make sure that the fields and their order are always consistent.
 *
 * We need to be careful with the output halo definitions, we only add those that were not taken
 * from the catalog in the first place.
 */
void addToCatalogGeneric(MoriaConfig *moria_cfg, HaloSet *hs, char *line, int *num, int h_idx,
		FILE *f, int step)
{
	int i, def_idx;
	char tmp_str[200];
	HaloDefinition *def;
	HaloData *hd;

	if (step == STEP_LINES)
		hd = &(hs->hd[h_idx]);
	else
		hd = NULL;

	/*
	 * Default status fields and outputs (R200m, Gamma etc)
	 */
	if (step == STEP_HEADER)
	{
		(*num)++;
		sprintf(line, "%s status_sparta(%d)", line, (*num));

	} else if (step == STEP_COMMENTS)
	{
		fputs(
				"#status_sparta: The status of the halo at this snapshot in SPARTA. See SPARTA docs for values.\n",
				f);
	} else if (step == STEP_LINES)
	{
		sprintf(line, "%s %2d", line, hd->status_sparta);
	} else
	{
		error(__FFL__, "Unknown step, %d.\n", step);
	}

	if (step == STEP_HEADER)
	{
		(*num)++;
		sprintf(line, "%s R200m_all_spa_internal(%d)", line, *num);
	} else if (step == STEP_COMMENTS)
	{
		fputs(
				"#R200m_all_spa_internal: The halo radius R200m (in physical kpc/h) used internally by SPARTA; computed from all particles for hosts, depends on settings for subs.\n",
				f);
	} else if (step == STEP_LINES)
	{
		sprintf(line, "%s %9.3e", line, hd->R200m_all_spa);
	}

	if (step == STEP_HEADER)
	{
		(*num)++;
		sprintf(line, "%s M200m_all_spa_internal(%d)", line, *num);
	} else if (step == STEP_COMMENTS)
	{
		fputs(
				"#M200m_all_spa_internal: The mass corresponding to R200m_all_spa_internal in units of Msun/h.\n",
				f);
	} else if (step == STEP_LINES)
	{
		sprintf(line, "%s %9.3e", line, hd->M200m_all_spa);
	}

	if (step == STEP_HEADER)
	{
		(*num)++;
		sprintf(line, "%s nu200m_internal(%d)", line, *num);
	} else if (step == STEP_COMMENTS)
	{
		fputs("#nu200m_internal: The peak height corresponding to M200m_all_spa_internal.\n", f);
	} else if (step == STEP_LINES)
	{
		sprintf(line, "%s %6.3f", line, hd->nu200m);
	}

	if (step == STEP_HEADER)
	{
		(*num)++;
		sprintf(line, "%s status_acc_rate(%d)", line, (*num));
	} else if (step == STEP_COMMENTS)
	{
		fputs(
				"#status_acc_rate: Status of the accretion rate field; this quantity cannot always be computed. See MORIA docs for values.\n",
				f);
	} else if (step == STEP_LINES)
	{
		sprintf(line, "%s %2d", line, hd->status_acc_rate);
	}

	if (step == STEP_HEADER)
	{
		(*num)++;
		sprintf(line, "%s acc_rate_200m_dyn(%d)", line, *num);
	} else if (step == STEP_COMMENTS)
	{
		fputs(
				"#acc_rate_200m_dyn: Accretion rate in units of d ln(M200m) / d ln(a) measured over one crossing time. See status_acc_rate for how this was computed.\n",
				f);
	} else if (step == STEP_LINES)
	{

		sprintf(line, "%s %9.3e", line, hd->acc_rate_200m_dyn);
	}

	/*
	 * Rsp statuses and fields
	 */
	if (hs->hd[0].status_moria_rsp != MORIA_STATUS_ANL_DOES_NOT_EXIST)
	{
		if (step == STEP_HEADER)
		{
			(*num)++;
			sprintf(line, "%s status_sparta_rsp(%d)", line, *num);
		} else if (step == STEP_COMMENTS)
		{
			fputs(
					"#status_sparta_rsp: The status of the rsp analysis in SPARTA; see documentation for values.\n",
					f);
		} else if (step == STEP_LINES)
		{
			sprintf(line, "%s %2d", line, hd->status_sparta_rsp);
		}

		if (step == STEP_HEADER)
		{
			(*num)++;
			sprintf(line, "%s status_moria_rsp(%d)", line, *num);
		} else if (step == STEP_COMMENTS)
		{
			fputs(
					"#status_moria_rsp: The status of the rsp analysis in MORIA; see documentation for values. If the analysis fails in SPARTA, MORIA may guess values in certain situations.\n",
					f);
		} else if (step == STEP_LINES)
		{
			sprintf(line, "%s %2d", line, hd->status_moria_rsp);
		}

		for (i = 0; i < moria_cfg->n_defs_rm; i++)
		{
			def_idx = moria_cfg->output_rm_defs[i].idx;
			def = &(moria_cfg->all_defs[def_idx]);

			if (!isSpartaRspDefinition(def))
				continue;

			if (step == STEP_HEADER)
			{
				(*num)++;
				sprintf(line, "%s %s(%d)", line, moria_cfg->output_rm_defs_str[i], *num);
			} else if (step == STEP_COMMENTS)
			{
				haloDefinitionToLabelType(def, tmp_str);
				if (def->quantity == HDEF_Q_RADIUS)
				{
					sprintf(line,
							"#%s: Splashback radius in physical kpc/h according to definition %s.\n",
							moria_cfg->output_rm_defs_str[i], tmp_str);
				} else if (def->quantity == HDEF_Q_MASS)
				{
					sprintf(line, "#%s: Splashback mass in Msun/h according to definition %s.\n",
							moria_cfg->output_rm_defs_str[i], tmp_str);
				} else
				{
					error(__FFL__, "Unknown definition quantity, %d.\n", def->quantity);
				}
				fputs(line, f);
			} else if (step == STEP_LINES)
			{
				sprintf(line, "%s %9.3e", line, hs->hd_defs[def_idx][h_idx]);
			}
		}
	}

	/*
	 * Haloprops definitions (status and output for each definition)
	 */
	for (i = 0; i < moria_cfg->n_defs_rm; i++)
	{
		def_idx = moria_cfg->output_rm_defs[i].idx;
		def = &(moria_cfg->all_defs[def_idx]);

		if (!isSpartaHpsDefinition(def) || (def->source == HDEF_SOURCE_CATALOG))
			continue;

		if (defHasStatus(def))
		{
			if (step == STEP_HEADER)
			{
				(*num)++;
				sprintf(line, "%s status_moria_hps_%s(%d)", line, moria_cfg->all_defs_str[def_idx],
						*num);
			} else if (step == STEP_COMMENTS)
			{
				sprintf(line,
						"#status_moria_hps_%s: Status of definition in SPARTA/MORIA (see documentation for values).\n",
						moria_cfg->all_defs_str[def_idx]);
				fputs(line, f);
			} else
			{
				sprintf(line, "%s %2d", line, hs->hd_def_status[def_idx][h_idx]);
			}
		}

		if (step == STEP_HEADER)
		{
			(*num)++;
			sprintf(line, "%s %s(%d)", line, moria_cfg->output_rm_defs_str[i], *num);
		} else if (step == STEP_COMMENTS)
		{
			if (def->type == HDEF_TYPE_SO)
			{
				if (def->quantity == HDEF_Q_RADIUS)
				{
					sprintf(line, "#%s: SO radius in physical kpc/h.\n",
							moria_cfg->output_rm_defs_str[i]);
				} else if (def->quantity == HDEF_Q_MASS)
				{
					sprintf(line, "#%s: SO mass in Msun/h.\n", moria_cfg->output_rm_defs_str[i]);
				} else
				{
					error(__FFL__, "Unknown definition quantity, %d.\n", def->quantity);
				}
			} else if (def->type == HDEF_TYPE_ORBITING)
			{
				if (def->subtype == HDEF_SUBTYPE_ORB_ALL)
				{
					sprintf(line,
							"#%s: Mass of all particles that have ever orbited halo in Msun/h.\n",
							moria_cfg->output_rm_defs_str[i]);
				} else if (def->quantity == HDEF_Q_RADIUS)
				{
					sprintf(line,
							"#%s: Radius that includes percentile of orbiting particles in physical kpc/h.\n",
							moria_cfg->output_rm_defs_str[i]);
				} else if (def->quantity == HDEF_Q_MASS)
				{
					sprintf(line, "#%s: Mass of percentile of orbiting particles in Msun/h.\n",
							moria_cfg->output_rm_defs_str[i]);
				} else
				{
					error(__FFL__, "Unknown definition quantity, %d.\n", def->quantity);
				}
			} else
			{
				error(__FFL__, "Unknown definition type, %d.\n", def->type);
			}
			fputs(line, f);
		} else if (step == STEP_LINES)
		{
			sprintf(line, "%s %9.3e", line, hs->hd_defs[def_idx][h_idx]);
		}
	}

	/*
	 * UPIDs
	 */
	for (i = 0; i < moria_cfg->n_defs_sub; i++)
	{
		if (step == STEP_HEADER)
		{
			(*num)++;
			sprintf(line, "%s parent_id_%s(%d)", line, moria_cfg->output_sub_defs_str[i], *num);
		} else if (step == STEP_COMMENTS)
		{
			sprintf(line, "#parent_id_%s: Parent ID of halo according to radius definition %s.\n",
					moria_cfg->output_sub_defs_str[i], moria_cfg->output_sub_defs_str[i]);
			fputs(line, f);
		} else if (step == STEP_LINES)
		{
			sprintf(line, "%s %8ld", line, hs->hd_upid_defs[i][h_idx]);
		}
	}
}

void addToCatalogHeader(MoriaConfig *moria_cfg, HaloSet *hs, char *line, int *num)
{
	addToCatalogGeneric(moria_cfg, hs, line, num, -1, NULL, STEP_HEADER);
}

void addToCatalogComments(MoriaConfig *moria_cfg, HaloSet *hs, char *line, FILE *f)
{
	addToCatalogGeneric(moria_cfg, hs, line, NULL, -1, f, STEP_COMMENTS);
}

void addToCatalogLine(MoriaConfig *moria_cfg, HaloSet *hs, char *line, int h_idx)
{
	addToCatalogGeneric(moria_cfg, hs, line, NULL, h_idx, NULL, STEP_LINES);
}

void outputCatalogRockstar(MoriaConfig *moria_cfg, HaloSet *hs, int snap_idx)
{
	int j, i1, i2, last_num, len_fn, len_ext, len_beg, comments_added;
	char temp_str[400], cat_name[100], line[CAT_LINE_NCHAR], line_out[CAT_LINE_NCHAR],
			file_beg[100], file_ext[20], *c, *c2;
	FILE *f_in, *f_out;

	/*
	 * Open catalog file again.
	 */
	getCatalogFileNameRockstar(moria_cfg->snap_a[snap_idx], cat_name);
	f_in = fopen(cat_name, "r");
	if (f_in == NULL)
		error(__FFL__, "Could not open catalog file %s.\n", cat_name);

	/*
	 * Create an output file name. The user can add a suffix which needs to be added before the
	 * file extension.
	 */
	c = strrchr(cat_name, '.');
	c2 = strrchr(cat_name, '/') + 1;
	len_fn = strlen(cat_name);
	len_ext = len_fn - (c - cat_name);
	len_beg = len_fn - len_ext - (c2 - cat_name);
	memcpy(file_beg, c2, len_beg);
	file_beg[len_beg] = '\0';
	memcpy(file_ext, c, len_ext);
	file_ext[len_ext] = '\0';
	sprintf(temp_str, "%s/%s%s", moria_cfg->output_dir_original, file_beg, file_ext);

	f_out = fopen(temp_str, "w");
	if (f_out == NULL)
		error(__FFL__, "Could not open output file %s.\n", temp_str);

	/*
	 * Read header line, parse; find number of existing fields
	 */
	fgets(line, CAT_LINE_NCHAR, f_in);
	line[strlen(line) - 1] = '\0';
	i1 = strlen(line);
	while (line[i1] != ')')
		i1--;
	i2 = i1;
	while (line[i2] != '(')
		i2--;
	i2 += 1;
	strncpy(temp_str, line + i2, i1 - i2);
	temp_str[i1 - i2] = '\0';
	last_num = atoi(temp_str);

	/*
	 * Add fields to header line, output to file.
	 */
	strcpy(line_out, line);
	addToCatalogHeader(moria_cfg, hs, line_out, &last_num);
	sprintf(line_out, "%s\n", line_out);
	fputs(line_out, f_out);

	/*
	 * Copy comment lines, then go through halo lines, add fields. Note that the counting is simple
	 * here because we have all catalog halos in the halo set, regardless of whether they are being
	 * output. The cat_fields arrays do not follow this convention, but we are not using them here.
	 */
	j = 0;
	comments_added = 0;
	while (fgets(line, CAT_LINE_NCHAR, f_in) != NULL)
	{
		if (line[0] == '#')
		{
			fputs(line, f_out);
			continue;
		}

		/*
		 * Add to comments
		 */
		if (!comments_added)
		{
			addToCatalogComments(moria_cfg, hs, line_out, f_out);
			comments_added = 1;
		}

		/*
		 * Only output lines above the particle limit, but increase the counter also if we have kept
		 * all halos in the halo array.
		 */
		if (hs->out_mask[j])
		{
			strcpy(line_out, line);
			line_out[strlen(line_out) - 1] = '\0';

			addToCatalogLine(moria_cfg, hs, line_out, j);

			sprintf(line_out, "%s\n", line_out);
			fputs(line_out, f_out);
		}
		j++;
	}

	fclose(f_in);
	fclose(f_out);
}

/*
 * Note that the limit in this function is not set by some macro but simply by the number of fields
 * we pass to sscanf.
 */
void parseCatalogLine(char *line, double *values, int n_fields, char *parse_str, int *field_order)
{
	double tmp[100];
	int j;

	if (n_fields > 100)
		error(__FFL__, "Trying to load %d catalog fields, max. is 100.\n");

	sscanf(line, parse_str, &(tmp[0]), &(tmp[1]), &(tmp[2]), &(tmp[3]), &(tmp[4]), &(tmp[5]),
			&(tmp[6]), &(tmp[7]), &(tmp[8]), &(tmp[9]), &(tmp[10]), &(tmp[11]), &(tmp[12]),
			&(tmp[13]), &(tmp[14]), &(tmp[15]), &(tmp[16]), &(tmp[17]), &(tmp[18]), &(tmp[19]),
			&(tmp[20]), &(tmp[21]), &(tmp[22]), &(tmp[23]), &(tmp[24]), &(tmp[25]), &(tmp[26]),
			&(tmp[27]), &(tmp[28]), &(tmp[29]), &(tmp[30]), &(tmp[31]), &(tmp[32]), &(tmp[33]),
			&(tmp[34]), &(tmp[35]), &(tmp[36]), &(tmp[37]), &(tmp[38]), &(tmp[39]), &(tmp[40]),
			&(tmp[41]), &(tmp[42]), &(tmp[43]), &(tmp[44]), &(tmp[45]), &(tmp[46]), &(tmp[47]),
			&(tmp[48]), &(tmp[49]), &(tmp[50]), &(tmp[51]), &(tmp[52]), &(tmp[53]), &(tmp[54]),
			&(tmp[55]), &(tmp[56]), &(tmp[57]), &(tmp[58]), &(tmp[59]), &(tmp[60]), &(tmp[61]),
			&(tmp[62]), &(tmp[63]), &(tmp[64]), &(tmp[65]), &(tmp[66]), &(tmp[67]), &(tmp[68]),
			&(tmp[69]), &(tmp[70]), &(tmp[71]), &(tmp[72]), &(tmp[73]), &(tmp[74]), &(tmp[75]),
			&(tmp[76]), &(tmp[77]), &(tmp[78]), &(tmp[79]), &(tmp[80]), &(tmp[81]), &(tmp[82]),
			&(tmp[83]), &(tmp[84]), &(tmp[85]), &(tmp[86]), &(tmp[87]), &(tmp[88]), &(tmp[89]),
			&(tmp[90]), &(tmp[91]), &(tmp[92]), &(tmp[93]), &(tmp[94]), &(tmp[95]), &(tmp[96]),
			&(tmp[97]), &(tmp[98]), &(tmp[99]));

	for (j = 0; j < n_fields; j++)
		values[j] = tmp[field_order[j]];
}
