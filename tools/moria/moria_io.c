/*************************************************************************************************
 *
 * I/O routines for moria.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <hdf5.h>
#include <sys/stat.h>

#include "moria_global.h"
#include "moria_io.h"
#include "../../src/utils.h"
#include "../../src/memory.h"
#include "../../src/halos/halo.h"
#include "../../src/halos/halo_definitions.h"
#include "../../src/io/io_hdf5.h"

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void treeTmpFilename(char *str, MoriaConfig *moria_cfg, float a);
void writeHDF5Config(MoriaConfig *moria_cfg, SpartaData *sd, hid_t file_id, int is_tree_file);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void createOutputDirectories(MoriaConfig *moria_cfg)
{
	if (moria_cfg->output_original)
		createPath(moria_cfg->output_dir_original, 0);

	if (moria_cfg->output_hdf5)
		createPath(moria_cfg->output_dir_hdf5, 0);

	if (moria_cfg->output_tree)
	{
		createPath(moria_cfg->output_dir_treetmp, 0);
		createPath(moria_cfg->output_filename_tree, 1);
	}

	if (moria_cfg->output_restart_files)
		createPath(moria_cfg->output_dir_restart, 0);
}

void treeTmpFilename(char *str, MoriaConfig *moria_cfg, float a)
{
	sprintf(str, "%s/treetmp_%.4f.hdf5", moria_cfg->output_dir_treetmp, a);
}

void writeHDF5Config(MoriaConfig *moria_cfg, SpartaData *sd, hid_t file_id, int is_tree_file)
{
	int i;
	hid_t grp_conf, grp_sim, grp_config_sparta;
	char temp_str[10000];

	grp_conf = H5Gcreate(file_id, "config", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForError(__FFL__, grp_conf, "creating group %s", "config");

	grp_sim = H5Gcreate(file_id, "simulation", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForError(__FFL__, grp_sim, "creating group %s", "simulation");

	/*
	 * Write config
	 */
	output(3, "    Writing config and simulation info...\n");

	// Input
	hdf5WriteAttributeString(grp_conf, "sparta_file", moria_cfg->sparta_file);
	hdf5WriteAttributeString(grp_conf, "catalog_dir", moria_cfg->catalog_dir);
	haloDefinitionToString(moria_cfg->main_def_catalog, temp_str);
	hdf5WriteAttributeString(grp_conf, "main_def_catalog", temp_str);
	hdf5WriteAttributeInt(grp_conf, "rockstar_strict_so", moria_cfg->rockstar_strict_so);
	hdf5WriteAttributeInt(grp_conf, "rockstar_bound_props", moria_cfg->rockstar_bound_props);

	// Console output
	hdf5WriteAttributeInt(grp_conf, "log_level", moria_cfg->log_level);
	hdf5WriteAttributeInt(grp_conf, "log_level_memory", moria_cfg->log_level_memory);
	hdf5WriteAttributeInt(grp_conf, "log_level_timing", moria_cfg->log_level_timing);
	hdf5WriteAttributeInt(grp_conf, "log_flush", moria_cfg->log_flush);
	hdf5WriteAttributeInt(grp_conf, "log_substatus_diff", moria_cfg->log_substatus_diff);

	// Error levels
	hdf5WriteAttributeInt(grp_conf, "err_level_more_snaps_than_cat",
			moria_cfg->err_level_more_snaps_than_cat);
	hdf5WriteAttributeInt(grp_conf, "err_level_snap_outside_range",
			moria_cfg->err_level_snap_outside_range);
	hdf5WriteAttributeInt(grp_conf, "err_level_main_def_not_200m",
			moria_cfg->err_level_main_def_not_200m);
	hdf5WriteAttributeInt(grp_conf, "err_level_acc_rate_undefined",
			moria_cfg->err_level_acc_rate_undefined);
	hdf5WriteAttributeInt(grp_conf, "err_level_cat_field_changed",
			moria_cfg->err_level_cat_field_changed);

	// Algorithm
	haloDefinitionToString(moria_cfg->cut_def, temp_str);
	hdf5WriteAttributeString(grp_conf, "cut_def", temp_str);
	hdf5WriteAttributeFloat(grp_conf, "cut_threshold_in_units", moria_cfg->cut_threshold_in_units);
	unitsToString(moria_cfg->cut_units, temp_str);
	hdf5WriteAttributeString(grp_conf, "cut_units", temp_str);
	hdf5WriteAttributeFloat(grp_conf, "cut_threshold", moria_cfg->cut_threshold);
	haloDefinitionToString(moria_cfg->order_def, temp_str);
	hdf5WriteAttributeString(grp_conf, "order_def", temp_str);
	hdf5WriteAttributeInt(grp_conf, "hostsub_assign_subparent_id",
			moria_cfg->hostsub_assign_subparent_id);
	hdf5WriteAttributeInt(grp_conf, "hostsub_restrict_to_output",
			moria_cfg->hostsub_restrict_to_output);
	hdf5WriteAttributeInt(grp_conf, "include_ghosts", moria_cfg->include_ghosts);
	hdf5WriteAttributeInt(grp_conf, "adjust_phantom_xv", moria_cfg->adjust_phantom_xv);
	hdf5WriteAttributeInt(grp_conf, "max_mass_ratio_sparta", moria_cfg->max_mass_ratio_sparta);
	hdf5WriteAttributeInt(grp_conf, "max_rsp_model_diff_factor",
			moria_cfg->max_rsp_model_diff_factor);
	hdf5WriteAttributeInt(grp_conf, "max_rsp_model_diff_sigma",
			moria_cfg->max_rsp_model_diff_sigma);

	// Output
	if (moria_cfg->output_original)
		hdf5WriteAttributeString(grp_conf, "output_dir_original", moria_cfg->output_dir_original);
	if (moria_cfg->output_hdf5)
		hdf5WriteAttributeString(grp_conf, "output_dir_hdf5", moria_cfg->output_dir_hdf5);
	if (moria_cfg->output_tree)
	{
		hdf5WriteAttributeString(grp_conf, "output_filename_tree", moria_cfg->output_filename_tree);
		hdf5WriteAttributeString(grp_conf, "output_dir_treetmp", moria_cfg->output_dir_treetmp);
	}
	if (moria_cfg->output_restart_files)
		hdf5WriteAttributeString(grp_conf, "output_dir_restart", moria_cfg->output_dir_restart);
	hdf5WriteAttributeInt(grp_conf, "output_restart_files", moria_cfg->output_restart_files);
	hdf5WriteAttributeInt(grp_conf, "output_restart_every", moria_cfg->output_compression_level);
	hdf5WriteAttributeInt(grp_conf, "output_original", moria_cfg->output_original);
	hdf5WriteAttributeInt(grp_conf, "output_hdf5", moria_cfg->output_hdf5);
	hdf5WriteAttributeInt(grp_conf, "output_tree", moria_cfg->output_tree);
	hdf5WriteAttributeInt(grp_conf, "output_compression_level",
			moria_cfg->output_compression_level);
	hdf5WriteAttributeInt(grp_conf, "output_n_a", moria_cfg->n_a);
	hdf5WriteAttributeFloat1D(grp_conf, "output_a", moria_cfg->n_a, moria_cfg->output_a);

	hdf5WriteAttributeInt(grp_conf, "output_n_rm_defs", moria_cfg->n_defs_rm);
	if (moria_cfg->n_defs_rm > 0)
	{
		sprintf(temp_str, "%s", moria_cfg->output_rm_defs_str[0]);
		for (i = 1; i < moria_cfg->n_defs_rm; i++)
			sprintf(temp_str, "%s, %s", temp_str, moria_cfg->output_rm_defs_str[i]);
	} else
	{
		sprintf(temp_str, "%s", "");
	}
	hdf5WriteAttributeString(grp_conf, "output_rm_defs", temp_str);

	hdf5WriteAttributeInt(grp_conf, "output_n_sub_defs", moria_cfg->n_defs_sub);
	if (moria_cfg->n_defs_sub > 0)
	{
		sprintf(temp_str, "%s", moria_cfg->output_sub_defs_str[0]);
		for (i = 1; i < moria_cfg->n_defs_sub; i++)
			sprintf(temp_str, "%s, %s", temp_str, moria_cfg->output_sub_defs_str[i]);
	} else
	{
		sprintf(temp_str, "%s", "");
	}
	hdf5WriteAttributeString(grp_conf, "output_sub_defs", temp_str);

	hdf5WriteAttributeInt(grp_conf, "output_n_cat_fields", moria_cfg->n_cat_fields);
	if (moria_cfg->n_cat_fields > 0)
	{
		sprintf(temp_str, "%s", moria_cfg->output_cat_fields[0]);
		for (i = 1; i < moria_cfg->n_cat_fields; i++)
			sprintf(temp_str, "%s, %s", temp_str, moria_cfg->output_cat_fields[i]);
	} else
	{
		sprintf(temp_str, "%s", "");
	}
	hdf5WriteAttributeString(grp_conf, "output_cat_fields", temp_str);

	// Copy of SPARTA config
	if (!is_tree_file)
	{
		grp_config_sparta = H5Gcreate(grp_conf, "sparta", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		hdf5CheckForError(__FFL__, grp_config_sparta, "creating group %s", "config/sparta");
		hdf5WriteAllAttributes(grp_config_sparta, sd->config_main, sd->n_config_main);
		H5Gclose(grp_config_sparta);

		grp_config_sparta = H5Gcreate(grp_conf, "sparta_compile", H5P_DEFAULT, H5P_DEFAULT,
		H5P_DEFAULT);
		hdf5CheckForError(__FFL__, grp_sim, "creating group %s", "config/sparta_compile");
		hdf5WriteAllAttributes(grp_config_sparta, sd->config_compile, sd->n_config_compile);
		H5Gclose(grp_config_sparta);

		grp_config_sparta = H5Gcreate(grp_conf, "sparta_rsp", H5P_DEFAULT, H5P_DEFAULT,
		H5P_DEFAULT);
		hdf5CheckForError(__FFL__, grp_sim, "creating group %s", "config/sparta_rsp");
		hdf5WriteAllAttributes(grp_config_sparta, sd->config_rsp, sd->n_config_rsp);
		H5Gclose(grp_config_sparta);

		grp_config_sparta = H5Gcreate(grp_conf, "sparta_hps", H5P_DEFAULT, H5P_DEFAULT,
		H5P_DEFAULT);
		hdf5CheckForError(__FFL__, grp_sim, "creating group %s", "config/sparta_hps");
		hdf5WriteAllAttributes(grp_config_sparta, sd->config_hps, sd->n_config_hps);
		H5Gclose(grp_config_sparta);
	}

	/*
	 * Write simulation properties
	 */
	hdf5WriteAttributeInt(grp_sim, "cosmo_flat", moria_cfg->cosmo.flat);
	hdf5WriteAttributeFloat(grp_sim, "cosmo_h", moria_cfg->cosmo.h);
	hdf5WriteAttributeFloat(grp_sim, "cosmo_Omega_m", moria_cfg->cosmo.Omega_m);
	hdf5WriteAttributeFloat(grp_sim, "cosmo_Omega_b", moria_cfg->cosmo.Omega_b);
	hdf5WriteAttributeFloat(grp_sim, "cosmo_Omega_r", moria_cfg->cosmo.Omega_r);
	hdf5WriteAttributeFloat(grp_sim, "cosmo_Omega_L", moria_cfg->cosmo.Omega_L);
	hdf5WriteAttributeFloat(grp_sim, "cosmo_w", moria_cfg->cosmo.w);
	hdf5WriteAttributeFloat(grp_sim, "cosmo_T_CMB", moria_cfg->cosmo.T_CMB);
	hdf5WriteAttributeFloat(grp_sim, "cosmo_sigma8", moria_cfg->cosmo.sigma8);
	hdf5WriteAttributeFloat(grp_sim, "cosmo_ns", moria_cfg->cosmo.ns);
	hdf5WriteAttributeInt(grp_sim, "cosmo_ps_type", moria_cfg->cosmo.ps_type);

	hdf5WriteAttributeFloat(grp_sim, "box_size", moria_cfg->box_size);
	hdf5WriteAttributeFloat(grp_sim, "particle_mass", moria_cfg->particle_mass);
	hdf5WriteAttributeFloat1D(grp_sim, "snap_a", moria_cfg->n_snaps, moria_cfg->snap_a);
	hdf5WriteAttributeFloat1D(grp_sim, "snap_z", moria_cfg->n_snaps, moria_cfg->snap_z);
	hdf5WriteAttributeFloat1D(grp_sim, "snap_t", moria_cfg->n_snaps, moria_cfg->snap_t);
	hdf5WriteAttributeFloat1D(grp_sim, "snap_tdyn", moria_cfg->n_snaps, moria_cfg->snap_t_dyn);

	H5Gclose(grp_conf);
	H5Gclose(grp_sim);
}

void outputHDF5Catalog(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		int is_tree_file, int *tree_idxs)
{
	int i, idx, n_write;
	hid_t file_id, grp_snap;
	char temp_str[400];
	HaloDefinition *def;
	int_least8_t *mask, *mask_cat;

	/*
	 * Depending on whether we are writing all data, set the count and mask. Also create an output
	 * file name. The user can add a suffix which needs to be added before the file extension.
	 *
	 * The catalog fields need their own mask because we may or may not have loaded catalog values
	 * for all halos. If not, the catalog fields array already has the output dimensions.
	 */
	if (is_tree_file)
	{
		treeTmpFilename(temp_str, moria_cfg, snap->a);
		n_write = hs->n_tot;
		mask = NULL;
		mask_cat = NULL;
	} else
	{
		sprintf(temp_str, "%s/moria_%.4f.hdf5", moria_cfg->output_dir_hdf5, snap->a);
		n_write = hs->n_out_cat;
		mask = hs->out_mask;

		if (moria_cfg->load_all_halos)
		{
			mask_cat = hs->out_mask;
		} else
		{
			mask_cat = NULL;
		}
	}

	/*
	 * Open HDF5 file and write config / sim info
	 */
	output(3, "    Creating HDF5 file...\n");
	file_id = H5Fcreate(temp_str, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForError(__FFL__, file_id, "creating catalog file");
	writeHDF5Config(moria_cfg, sd, file_id, is_tree_file);

	/*
	 * Write info about this particular snapshot (not contained in the catalog output)
	 */
	output(3, "    Writing snapshot data...\n");
	grp_snap = H5Gcreate(file_id, "snapshot", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForError(__FFL__, grp_snap, "creating group %s", "snapshot");

	hdf5WriteAttributeInt(grp_snap, "idx", snap->snap_idx);
	hdf5WriteAttributeFloat(grp_snap, "a", snap->a);
	hdf5WriteAttributeFloat(grp_snap, "z", snap->z);
	hdf5WriteAttributeFloat(grp_snap, "t", snap->t);
	hdf5WriteAttributeFloat(grp_snap, "rho200m", snap->rho_200m);

	hdf5WriteAttributeInt(grp_snap, "idx_tdyn", snap->snap_idx_tdyn);
	hdf5WriteAttributeFloat(grp_snap, "a_tdyn", snap->a_tdyn);
	hdf5WriteAttributeFloat(grp_snap, "z_tdyn", snap->z_tdyn);
	hdf5WriteAttributeFloat(grp_snap, "t_tdyn", snap->t_tdyn);
	hdf5WriteAttributeFloat(grp_snap, "rho200m_tdyn", snap->rho_200m_tdyn);

	H5Gclose(grp_snap);

	/*
	 * Write standard halo fields. The is_most_massive_progenitor field is not necessary in trees
	 * because it is obvious when a tree ends.
	 */
	output(3, "    Writing default halo fields...\n");
	hdf5WriteStructMemberMask(file_id, "status_sparta", DTYPE_INT8, hs->n_tot, sizeof(HaloData),
			offsetof(HaloData, status_sparta), (void*) hs->hd, mask, n_write);
	hdf5WriteStructMemberMask(file_id, "status_acc_rate", DTYPE_INT8, hs->n_tot, sizeof(HaloData),
			offsetof(HaloData, status_acc_rate), (void*) hs->hd, mask, n_write);
	hdf5WriteStructMemberMask(file_id, "id", DTYPE_INT64, hs->n_tot, sizeof(HaloData),
			offsetof(HaloData, id), (void*) hs->hd, mask, n_write);
	hdf5WriteStructMemberMask(file_id, "parent_id_cat", DTYPE_INT64, hs->n_tot, sizeof(HaloData),
			offsetof(HaloData, cat_upid), (void*) hs->hd, mask, n_write);
	hdf5WriteStructMemberMask(file_id, "descendant_id", DTYPE_INT64, hs->n_tot, sizeof(HaloData),
			offsetof(HaloData, desc_id), (void*) hs->hd, mask, n_write);
	if (!is_tree_file)
		hdf5WriteStructMemberMask(file_id, "is_most_massive_progenitor", DTYPE_INT8, hs->n_tot,
				sizeof(HaloData), offsetof(HaloData, mmp), (void*) hs->hd, mask, n_write);

	hdf5WriteStructMemberMask(file_id, "R200m_all_spa_internal", DTYPE_FLOAT, hs->n_tot,
			sizeof(HaloData), offsetof(HaloData, R200m_all_spa), (void*) hs->hd, mask, n_write);
	hdf5WriteStructMemberMask(file_id, "M200m_all_spa_internal", DTYPE_FLOAT, hs->n_tot,
			sizeof(HaloData), offsetof(HaloData, M200m_all_spa), (void*) hs->hd, mask, n_write);
	hdf5WriteStructMemberMask(file_id, "nu200m_internal", DTYPE_FLOAT, hs->n_tot, sizeof(HaloData),
			offsetof(HaloData, nu200m), (void*) hs->hd, mask, n_write);
	hdf5WriteStructMemberMask(file_id, "acc_rate_200m_dyn", DTYPE_FLOAT, hs->n_tot,
			sizeof(HaloData), offsetof(HaloData, acc_rate_200m_dyn), (void*) hs->hd, mask, n_write);

	hdf5WriteStructMember2DMask(file_id, "x", DTYPE_FLOAT, hs->n_tot, 3, sizeof(HaloData),
			offsetof(HaloData, x), (void*) hs->hd, mask, n_write);
	hdf5WriteStructMember2DMask(file_id, "v", DTYPE_FLOAT, hs->n_tot, 3, sizeof(HaloData),
			offsetof(HaloData, v), (void*) hs->hd, mask, n_write);

	/*
	 * Write analysis status if available
	 */
	if (hs->hd[0].status_moria_rsp != MORIA_STATUS_ANL_DOES_NOT_EXIST)
	{
		output(3, "    Writing rsp status fields...\n");
		hdf5WriteStructMemberMask(file_id, "status_sparta_rsp", DTYPE_INT8, hs->n_tot,
				sizeof(HaloData), offsetof(HaloData, status_sparta_rsp), (void*) hs->hd, mask,
				n_write);
		hdf5WriteStructMemberMask(file_id, "status_moria_rsp", DTYPE_INT8, hs->n_tot,
				sizeof(HaloData), offsetof(HaloData, status_moria_rsp), (void*) hs->hd, mask,
				n_write);
	}

	output(3, "    Writing hps status fields...\n");
	for (i = 0; i < moria_cfg->n_defs_all; i++)
	{
		def = &(moria_cfg->all_defs[i]);
		if (!defHasStatus(def))
			continue;

		sprintf(temp_str, "status_moria_hps_%s", moria_cfg->all_defs_str[i]);
		hdf5WriteDatasetMask(file_id, temp_str, DTYPE_INT8, hs->n_tot,
				(void*) (hs->hd_def_status[i]), mask, n_write);
	}

	/*
	 * Write halo definitions, upids, and extra data from catalog
	 */
	output(3, "    Writing halo definitions...\n");
	for (i = 0; i < moria_cfg->n_defs_rm; i++)
	{
		idx = moria_cfg->output_rm_defs[i].idx;
		hdf5WriteDatasetMask(file_id, moria_cfg->output_rm_defs_str[i], DTYPE_FLOAT, hs->n_tot,
				(void*) (hs->hd_defs[idx]), mask, n_write);
	}

	output(3, "    Writing subhalo assignments...\n");
	for (i = 0; i < moria_cfg->n_defs_sub; i++)
	{
		sprintf(temp_str, "parent_id_%s", moria_cfg->output_sub_defs_str[i]);
		hdf5WriteDatasetMask(file_id, temp_str, DTYPE_INT64, hs->n_tot,
				(void*) (hs->hd_upid_defs[i]), mask, n_write);
	}

	/*
	 * When outputting catalog fields, we need to make sure that the names are compatible with the
	 * HDF5 specification. The first time, we may put out warnings if that is not the case.
	 */
	output(3, "    Writing catalog fields...\n");
	for (i = 0; i < moria_cfg->n_cat_fields; i++)
	{
		if (hdf5SafeName(moria_cfg->output_cat_fields[i], temp_str))
		{
			if (!moria_cfg->warnings_field_name_done)
				warningOrError(__FFL__, moria_cfg->err_level_cat_field_changed,
						"Changed catalog field '%s' to '%s' for HDF5 compatibility.\n",
						moria_cfg->output_cat_fields[i], temp_str);
		}
		hdf5WriteNDArrayMask(file_id, temp_str, &(hs->cf[i]), mask_cat, n_write);
	}
	moria_cfg->warnings_field_name_done = 1;

	/*
	 * If we are writing tree files, we need to incude two extra pieces of information: the tree
	 * indices of the halos in this file, and whether the halos were written to the catalog file.
	 * The latter is for the convenience of the tree file user.
	 */
	if (is_tree_file)
	{
		hdf5WriteDataset(file_id, "tree_idxs", DTYPE_INT32, hs->n_tot, (void*) (tree_idxs));
		hdf5WriteDataset(file_id, "mask_cut", DTYPE_INT8, hs->n_tot, (void*) (hs->out_mask));
	}

	H5Fclose(file_id);
}

/*
 * Write the tree file. This function assumes that the tree has been sorted, meaning that the
 * output indices have been set.
 */
void outputHDF5Tree(MoriaConfig *moria_cfg, SpartaData *sd, TreeSet *ts)
{
	int i, j, k, tree_idx, sparta_idx, n_files, object_type, *n_halos_snap, **file_out_idxs,
			*file_tree_idxs, out_idx, n_halos_tmp;
	int_least8_t *file_mask_cut;
	long int offset_file, offset_all;
	char temp_str[400], ds_name[MAX_DS_CHARS];
	char *p_file, *p_all;
	size_t s, s_copy, s_all;
	NDArray nda_file, nda_all, nda_mask_alive, nda_mask_cut, nda_desc_idx, nda_status_final,
			nda_first_snap, nda_last_snap;
	hid_t tree_file, *tmp_files, grp_branch;
	hsize_t n_ds;

	n_files = moria_cfg->n_snaps;
	output(0, "Creating tree file with %d/%d halo histories.\n", ts->n_out, ts->n);

	/*
	 * Create mask arrays that will tell the user where halos were valid and whether they were
	 * written to the catalog files
	 */
	initializeNDArray(&nda_mask_alive);
	nda_mask_alive.n[0] = n_files;
	nda_mask_alive.n[1] = ts->n_out;
	nda_mask_alive.n_dim = 2;
	nda_mask_alive.dtype = DTYPE_INT8;
	allocateNDArray(__FFL__, MID_MORIA_IO, &nda_mask_alive);

	initializeNDArray(&nda_mask_cut);
	nda_mask_cut.n[0] = n_files;
	nda_mask_cut.n[1] = ts->n_out;
	nda_mask_cut.n_dim = 2;
	nda_mask_cut.dtype = DTYPE_INT8;
	allocateNDArray(__FFL__, MID_MORIA_IO, &nda_mask_cut);

	/*
	 * Create utility arrays for first/last snapshot
	 */
	initializeNDArray(&nda_first_snap);
	nda_first_snap.n[0] = ts->n_out;
	nda_first_snap.n_dim = 1;
	nda_first_snap.dtype = DTYPE_INT16;
	allocateNDArray(__FFL__, MID_MORIA_IO, &nda_first_snap);
	for (j = 0; j < ts->n_out; j++)
		nda_first_snap.q_int16[j] = INVALID_IDX;

	initializeNDArray(&nda_last_snap);
	nda_last_snap.n[0] = ts->n_out;
	nda_last_snap.n_dim = 1;
	nda_last_snap.dtype = DTYPE_INT16;
	allocateNDArray(__FFL__, MID_MORIA_IO, &nda_last_snap);
	for (j = 0; j < ts->n_out; j++)
		nda_last_snap.q_int16[j] = INVALID_IDX;

	/*
	 * Create an integer array for the descendant indices. Unlike the other arrays, this is not
	 * set to 0 but -1 where the tree history is not alive, because 0 is a valid index.
	 */
	initializeNDArray(&nda_desc_idx);
	nda_desc_idx.n[0] = n_files;
	nda_desc_idx.n[1] = ts->n_out;
	nda_desc_idx.n_dim = 2;
	nda_desc_idx.dtype = DTYPE_INT32;
	allocateNDArray(__FFL__, MID_MORIA_IO, &nda_desc_idx);
	for (i = 0; i < n_files; i++)
		for (j = 0; j < ts->n_out; j++)
			nda_desc_idx.q_int32_2[i][j] = INVALID_IDX;

	/*
	 * Open HDF5 file and write config / sim info
	 */
	output(3, "    Creating HDF5 tree file...\n");
	tree_file = H5Fcreate(moria_cfg->output_filename_tree, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForError(__FFL__, tree_file, "creating tree file");
	writeHDF5Config(moria_cfg, sd, tree_file, 0);

	/*
	 * Open all temp files for reading
	 */
	tmp_files = (hid_t*) memAlloc(__FFL__, MID_MORIA_IO, sizeof(hid_t) * n_files);
	for (i = 0; i < n_files; i++)
	{
		treeTmpFilename(temp_str, moria_cfg, moria_cfg->snap_a[i]);
		tmp_files[i] = H5Fopen(temp_str, H5F_ACC_RDONLY, H5P_DEFAULT);
		hdf5CheckForError(__FFL__, tmp_files[i], "opening temporary tree file");
	}

	/*
	 * Load tree indices for each data file, record the number of halos. Then create a new index
	 * array that points to -1 if the halo is not being output or directly to the position in the
	 * actual output array if it is.
	 *
	 * We also set the corresponding mask element of each time and halo to True.
	 */
	file_out_idxs = (int**) memAlloc(__FFL__, MID_MORIA_IO, sizeof(int*) * n_files);
	n_halos_snap = (int*) memAlloc(__FFL__, MID_MORIA_IO, sizeof(int) * n_files);

	for (i = 0; i < n_files; i++)
	{
		output(5, "Reading indices in file %d.\n", i);
		hdf5ReadDataset(tmp_files[i], "tree_idxs", DTYPE_INT32, &(n_halos_snap[i]),
				(void**) &file_tree_idxs, MID_MORIA_IO);
		hdf5ReadDataset(tmp_files[i], "mask_cut", DTYPE_INT8, &(n_halos_tmp),
				(void**) &file_mask_cut, MID_MORIA_IO);
		if (n_halos_tmp != n_halos_snap[i])
			error(__FFL__, "Index and mask datasets have different dimensions.\n");

		file_out_idxs[i] = (int*) memAlloc(__FFL__, MID_MORIA_IO, sizeof(int) * n_halos_snap[i]);
		for (k = 0; k < n_halos_snap[i]; k++)
		{
			tree_idx = file_tree_idxs[k];
			if (ts->th[tree_idx].do_output)
			{
				out_idx = ts->tree_out_idxs[tree_idx];
#if MORIA_CAREFUL
				if (out_idx == INVALID_IDX)
					error(__FFL__, "Found invalid index in tree halo %d, ID %ld.\n", tree_idx,
							out_idx);
#endif
				file_out_idxs[i][k] = out_idx;

				nda_mask_alive.q_int8_2[i][out_idx] = 1;
				nda_mask_cut.q_int8_2[i][out_idx] = file_mask_cut[k];

				/*
				 * Update the first/last snap arrays if necessary
				 */
				if (nda_first_snap.q_int16[out_idx] == INVALID_IDX)
					nda_first_snap.q_int16[out_idx] = i;
				nda_last_snap.q_int16[out_idx] = i;

				/*
				 * Assign the descendant index array. If this is the final snapshot, there are no
				 * descendants (all -1). If the halo merges at this snapshot, its merge_idx is its
				 * descendant index. If the halo does not merge, that means it lives on and its
				 * desc idx is just its current idx.
				 */
				if (i == n_files - 1)
					nda_desc_idx.q_int32_2[i][out_idx] = INVALID_IDX;
				else if (ts->th[tree_idx].merge_snap == i)
					nda_desc_idx.q_int32_2[i][out_idx] = ts->th[tree_idx].merge_idx;
				else
					nda_desc_idx.q_int32_2[i][out_idx] = out_idx;
			} else
			{
				file_out_idxs[i][k] = INVALID_IDX;
			}
		}

		memFree(__FFL__, MID_MORIA_IO, file_tree_idxs, sizeof(int) * n_halos_snap[i]);
		memFree(__FFL__, MID_MORIA_IO, file_mask_cut, sizeof(int_least8_t) * n_halos_snap[i]);
	}

	/*
	 * Write masks to file.
	 */
	hdf5WriteNDArray(tree_file, "mask_alive", &nda_mask_alive);
	hdf5WriteNDArray(tree_file, "mask_cut", &nda_mask_cut);
	hdf5WriteNDArray(tree_file, "descendant_index", &nda_desc_idx);

	freeNDArray(__FFL__, MID_MORIA_IO, &nda_mask_alive);
	freeNDArray(__FFL__, MID_MORIA_IO, &nda_mask_cut);
	freeNDArray(__FFL__, MID_MORIA_IO, &nda_desc_idx);

	/*
	 * Write special datasets that do not conform to the n_snaps * n_halos dimensions but have only
	 * one value per tree branch. They are output into a separate directory, branch_data, which
	 * currently contains:
	 *
	 * status_sparta_final  The final SPARTA status; this is not part of haloset because not
	 *                      specific to snapshot. Thus, we load it directly from the SPARTA data.
	 */
	output(3, "    Writing branch data...\n");
	grp_branch = H5Gcreate(tree_file, "branch_data", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForError(__FFL__, grp_branch, "creating group %s", "branch_data");

	initializeNDArray(&nda_status_final);
	nda_status_final.n[0] = ts->n_out;
	nda_status_final.n_dim = 1;
	nda_status_final.dtype = DTYPE_INT8;
	allocateNDArray(__FFL__, MID_MORIA_IO, &nda_status_final);
	for (j = 0; j < ts->n; j++)
	{
		if (!ts->th[j].do_output)
			continue;
		out_idx = ts->tree_out_idxs[j];
		sparta_idx = ts->th[j].sparta_idx;

		if (sparta_idx == -1)
		{
			output(4, "No SPARTA idx in tree idx %d, output idx %d, ID %ld, alive %d.\n", j,
					out_idx, ts->th[j].id, ts->th[j].is_alive);
			nda_status_final.q_int8[out_idx] = HALO_STATUS_NOT_FOUND;
		} else
		{
			nda_status_final.q_int8[out_idx] = sd->sparta_status_final[sparta_idx];
		}
	}
	hdf5WriteNDArray(grp_branch, "status_sparta_final", &nda_status_final);
	hdf5WriteNDArray(grp_branch, "first_snap", &nda_first_snap);
	hdf5WriteNDArray(grp_branch, "last_snap", &nda_last_snap);
	freeNDArray(__FFL__, MID_MORIA_IO, &nda_status_final);
	freeNDArray(__FFL__, MID_MORIA_IO, &nda_first_snap);
	freeNDArray(__FFL__, MID_MORIA_IO, &nda_last_snap);

	H5Gclose(grp_branch);

	/*
	 * Run over objects. First, find their name and type, and continue if they are not standard
	 * datasets. Then combine from dimensions n_halos * ... to n_snaps * n_halos * ...
	 */
	H5Gget_num_objs(tmp_files[0], &n_ds);
	s_all = -1;
	s_copy = -1;
	for (j = 0; j < n_ds; j++)
	{
		object_type = H5Gget_objtype_by_idx(tmp_files[0], (size_t) j);
		if (object_type != H5G_DATASET)
			continue;

		H5Gget_objname_by_idx(tmp_files[0], (hsize_t) j, ds_name, (size_t) MAX_DS_CHARS);
		if (strcmp(ds_name, "tree_idxs") == 0)
			continue;
		if (strcmp(ds_name, "mask_cut") == 0)
			continue;

		/*
		 * Run over files. If first file, create the memory for the combined dataset.
		 */
		for (i = 0; i < n_files; i++)
		{
			initializeNDArray(&nda_file);
			hdf5ReadNDArray(tmp_files[i], ds_name, &nda_file, MID_MORIA_IO);

			if (nda_file.n[0] != n_halos_snap[i])
				error(__FFL__, "Incompatible tree file dimensions (%d, %d).\n", nda_file.n[0],
						n_halos_snap[i]);

			if (i == 0)
			{
				if (nda_file.n_dim == 1)
					output(0,
							"    Assembling dataset  %-35s dimensions [n_halos]       -> [%d x %d].\n",
							ds_name, n_files, ts->n_out);
				else if (nda_file.n_dim == 2)
					output(0,
							"    Assembling dataset  %-35s dimensions [n_halos x %2d]  -> [%d x %d x %d].\n",
							ds_name, nda_file.n[1], n_files, ts->n_out, nda_file.n[1]);
				else
					error(__FFL__, "Unexpected dimensionality\n");

				/*
				 * Allocate a contiguous array of dimensionality n_halos_tree x n_snaps x ...
				 */
				initializeNDArray(&nda_all);
				nda_all.dtype = nda_file.dtype;
				nda_all.n_dim = nda_file.n_dim + 1;
				nda_all.n[0] = n_files;
				nda_all.n[1] = ts->n_out;

				s = datatypeSize(nda_file.dtype);
				s_copy = s;
				s_all = s * n_files * ts->n_out;

				if (nda_file.n_dim == 2)
				{
					nda_all.n[2] = nda_file.n[1];
					s_copy *= nda_file.n[1];
					s_all *= nda_file.n[1];
				}

				allocateNDArray(__FFL__, MID_MORIA_IO, &nda_all);
			}

			/*
			 * Now write the data into the all-dataset in the correct order
			 */
			for (k = 0; k < nda_file.n[0]; k++)
			{
				if (file_out_idxs[i][k] == INVALID_IDX)
					continue;

				offset_file = k * s_copy;
				offset_all = s_copy * (i * ts->n_out + file_out_idxs[i][k]);

#if MORIA_CAREFUL
				if (offset_all > s_all)
					error(__FFL__, "Offset %d too large (max %d).\n", offset_all, s_all);
#endif

				p_file = nda_file.q_bytes + offset_file;
				p_all = nda_all.q_bytes + offset_all;
				memcpy(p_all, p_file, s_copy);
			}

			freeNDArray(__FFL__, MID_MORIA_IO, &nda_file);
		}

		/*
		 * Save dataset to file and free the memory
		 */
		hdf5WriteNDArray(tree_file, ds_name, &nda_all);
		freeNDArray(__FFL__, MID_MORIA_IO, &nda_all);
	}

	/*
	 * Close groups and file, delete temp files, free memory
	 */
	H5Fclose(tree_file);

	for (i = 0; i < n_files; i++)
	{
		H5Fclose(tmp_files[i]);

		treeTmpFilename(temp_str, moria_cfg, moria_cfg->snap_a[i]);
		remove(temp_str);

		memFree(__FFL__, MID_MORIA_IO, file_out_idxs[i], sizeof(int) * n_halos_snap[i]);
	}

	memFree(__FFL__, MID_MORIA_IO, file_out_idxs, sizeof(int*) * n_files);
	memFree(__FFL__, MID_MORIA_IO, n_halos_snap, sizeof(int) * n_files);
	memFree(__FFL__, MID_MORIA_IO, tmp_files, sizeof(hid_t) * n_files);

	printLine(0);
}
