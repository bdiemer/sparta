/*************************************************************************************************
 *
 * This unit implements MORIA's merger tree functionality.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_TREE_H_
#define _MORIA_TREE_H_

#include "moria_global.h"
#include "moria_config.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

TreeSet* initializeTreeSet();
void increaseTreeSet(MoriaConfig *moria_cfg, TreeSet *ts, int n_target);
void freeTreeSet(TreeSet *ts);

void assignHalosToTree(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		TreeSet *ts);
void findTreeMergers(MoriaConfig *moria_cfg, SnapshotData *snap, HaloSet *hs, TreeSet *ts);
void findTreeMergersFinal(MoriaConfig *moria_cfg, HaloSet *hs, TreeSet *ts);
void sortTree(MoriaConfig *moria_cfg, TreeSet *ts);

#endif
