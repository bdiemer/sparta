/*************************************************************************************************
 *
 * This unit determines the splashback data, either from SPARTA or models.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include "moria_run.h"
#include "moria_halo.h"
#include "moria_io.h"
#include "moria_io_restart.h"
#include "moria_io_sparta.h"
#include "moria_io_halocat.h"
#include "moria_merge.h"
#include "moria_hostsub.h"
#include "moria_statistics.h"
#include "moria_tree.h"
#include "../models/models_rsp.h"
#include "../../src/memory.h"
#include "../../src/utils.h"
#include "../../src/statistics.h"
#include "../../src/halos/halo.h"
#include "../../src/io/io_hdf5.h"

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void moriaProcessSnapshot(MoriaConfig *moria_cfg, MoriaTimers *timers, SpartaData *sd, int snap_idx,
		MoriaSorter *sorters, TreeSet *ts);
SnapshotData computeSnapshotData(MoriaConfig *moria_cfg, int snap_idx);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * This function contains the main loop over snapshots, and thus any variables that need to be
 * stored across snapshots.
 */
void moriaProcessSimulation(MoriaConfig *moria_cfg, MoriaTimers *timers, SpartaData **sd_p,
		int restart)
{
	int i, first_i, n_snaps, snap_idx, do_all_snaps;
	clock_t cur_time;
	MoriaSorter *sorters;
	TreeSet *ts = NULL;

	/*
	 * If restarting, we load all fields from the restart files and set the first snap to one snap
	 * after we stopped. If not, we start at zero and initialize the tree structure.
	 */
	if (restart)
	{
		moriaReadOrWriteRestartFiles(0, &first_i, moria_cfg, timers, sd_p, &ts);
		first_i++;
	} else
	{
		first_i = 0;
		if (moria_cfg->output_tree)
			ts = initializeTreeSet();
	}

	/*
	 * Allocate sorters and other permanent (cross-snapshot) memory. Note that we're wrapping the
	 * sorters into the permanent SPARTA data memory ID.
	 */
	sorters = (MoriaSorter*) memAlloc(__FFL__, MID_MORIA_SPARTA,
			sizeof(MoriaSorter) * (*sd_p)->n_halos);

	/*
	 * Decide number of snaps to loop over
	 */
	do_all_snaps = ((moria_cfg->output_a[0] < 0.0) || moria_cfg->output_tree);
	if (do_all_snaps)
		n_snaps = moria_cfg->n_snaps;
	else
		n_snaps = moria_cfg->n_a;

	/*
	 * Loop over snaps
	 */
	cur_time = clock();
	timers->timers_0[MORIA_T0_INIT] += cur_time;
	timers->timers_0[MORIA_T0_SNAPS] -= cur_time;
	timers->timers_1[MORIA_T1_INITFINAL] -= cur_time;

	for (i = first_i; i < n_snaps; i++)
	{
		if (do_all_snaps)
		{
			snap_idx = i;
			output(0, "Snap %3d/%d, index %d/%d, a %.4f\n", i + 1, n_snaps, snap_idx,
					moria_cfg->n_snaps - 1, moria_cfg->snap_a[snap_idx]);
		} else
		{
			snap_idx = closestInArray(moria_cfg->snap_a, moria_cfg->n_snaps,
					moria_cfg->output_a[i]);
			output(0, "Snap %3d/%d, index %d/%d, target a %.4f, actual a %.4f\n", i + 1, n_snaps,
					snap_idx, moria_cfg->n_snaps - 1, moria_cfg->output_a[i],
					moria_cfg->snap_a[snap_idx]);
		}

		/*
		 * Work on this snapshot.
		 */
		moriaProcessSnapshot(moria_cfg, timers, *sd_p, snap_idx, sorters, ts);

		/*
		 * Write restart files if necessary (if we have reached every nth snap, but not for the
		 * first and last snap). Note that we handle restart files by snapshot index rather than
		 * snapshot number, since we may not be going through all snapshots.
		 *
		 * There is a good reason not to write  a restart file for the final snapshot: during that
		 * snapshot, the final tree mergers are assigned, and this process cannot be debugged from
		 * a restart file if it has already happened.
		 */
		if (moria_cfg->output_restart_files && (i > 0) && (i < n_snaps - 1)
				&& (i % moria_cfg->output_restart_every == 0))
		{
			cur_time = clock();
			timers->timers_1[MORIA_T1_INITFINAL] += cur_time;
			timers->timers_1[MORIA_T1_RESTART] -= cur_time;

			output(2, "Writing restart file...\n");
			moriaReadOrWriteRestartFiles(1, &i, moria_cfg, timers, sd_p, &ts);

			cur_time = clock();
			timers->timers_1[MORIA_T1_RESTART] += cur_time;
			timers->timers_1[MORIA_T1_INITFINAL] -= cur_time;
		}

		/*
		 * Print memory statistics
		 */
		printMemoryStatistics(i, 0);

		/*
		 * Print timing statistics and reset timers from this snapshot. When resetting the timers,
		 * we add the snapshot timers to the cumulative. Thus, we need to close out the init/final
		 * timer before doing that, even though it actually keeps running.
		 */
		cur_time = clock();
		timers->timers_1[MORIA_T1_INITFINAL] += cur_time;
		printMoriaTimingStatistics(moria_cfg, timers);
		resetTimersSnapshot(timers);
		timers->timers_1[MORIA_T1_INITFINAL] -= cur_time;

		printLine(0);
	}

	/*
	 * Free SPARTA data, since that is a large chunk of memory that we do not need any more. This
	 * memory can then be used for tree operations.
	 */
	memFree(__FFL__, MID_MORIA_SPARTA, sorters, sizeof(MoriaSorter) * (*sd_p)->n_halos);
	freeSpartaDataMain(moria_cfg, *sd_p);

	cur_time = clock();
	timers->timers_1[MORIA_T1_INITFINAL] += cur_time;
	timers->timers_0[MORIA_T0_SNAPS] += cur_time;

	/*
	 * Combine the temporary tree files, output tree
	 */
	if (moria_cfg->output_tree)
	{
		timers->timers_0[MORIA_T0_TREE] -= cur_time;
		sortTree(moria_cfg, ts);
		outputHDF5Tree(moria_cfg, *sd_p, ts);
		freeTreeSet(ts);
		cur_time = clock();
		timers->timers_0[MORIA_T0_TREE] += cur_time;
	}

	timers->timers_0[MORIA_T0_FINALIZE] -= cur_time;
	freeSpartaDataConfig(moria_cfg, *sd_p);
}

/*
 * This function produces a halo catalog for one snapshot.
 */
void moriaProcessSnapshot(MoriaConfig *moria_cfg, MoriaTimers *timers, SpartaData *sd, int snap_idx,
		MoriaSorter *sorters, TreeSet *ts)
{
	int i, j, n_ghosts, is_final_snap;
	clock_t cur_time;
	HaloSet *hs;
	SnapshotData snap;

	is_final_snap = (snap_idx == moria_cfg->n_snaps - 1);

	/*
	 * Compute snap number one dynamical time ago, and other snapshot-specific quantities.
	 */
	output(2, "Computing snapshot data...\n");
	snap = computeSnapshotData(moria_cfg, snap_idx);
	cur_time = clock();
	timers->timers_1[MORIA_T1_INITFINAL] += cur_time;

	/*
	 * Sort halo IDs at this snapshot
	 */
	timers->timers_1[MORIA_T1_SORT] -= cur_time;
	output(2, "Sorting halos...\n");
	for (j = 0; j < sd->n_halos; j++)
	{
		sorters[j].id = sd->halo_ids[j][snap_idx];
		sorters[j].idx = j;
	}
	qsort(sorters, sd->n_halos, sizeof(MoriaSorter), &compareIndexSorters);
	cur_time = clock();
	timers->timers_1[MORIA_T1_SORT] += cur_time;

	/*
	 * If necessary, count the number of ghost halos so that we can reserve space for them
	 */
	if (moria_cfg->include_ghosts)
	{
		timers->timers_1[MORIA_T1_GHOSTS] -= cur_time;
		output(2, "Counting ghost halos...\n");
		n_ghosts = countGhostHalos(moria_cfg, sd, &snap);
		cur_time = clock();
		timers->timers_1[MORIA_T1_GHOSTS] += cur_time;
	} else
	{
		n_ghosts = 0;
	}

	/*
	 * Read the halo catalog
	 */
	timers->timers_1[MORIA_T1_READCAT] -= cur_time;
	output(2, "Reading halo catalog...\n");
	hs = getHaloSetFromCatalog(moria_cfg, &snap, n_ghosts);
	cur_time = clock();
	timers->timers_1[MORIA_T1_READCAT] += cur_time;

	/*
	 * Find SPARTA equivalents and resolve any conflicts in halo status between the catalog and
	 * SPARTA (they can happen due to errors or ghosts).
	 */
	timers->timers_1[MORIA_T1_SPARTAMATCH] -= cur_time;
	output(2, "Matching halos to SPARTA file...\n");
	for (i = 0; i < hs->n_cat; i++)
		findSpartaIndex(sd, sorters, &(hs->hd[i]), snap_idx);
	if (!is_final_snap)
	{
		output(2, "Resolving status conflicts...\n");
		for (i = 0; i < hs->n_cat; i++)
			resolveStatusConflicts(moria_cfg, sd, &(hs->hd[i]), snap_idx);
	}
	cur_time = clock();
	timers->timers_1[MORIA_T1_SPARTAMATCH] += cur_time;

	/*
	 * If desired, add ghosts to the halo set
	 */
	if (moria_cfg->include_ghosts)
	{
		timers->timers_1[MORIA_T1_GHOSTS] -= cur_time;
		output(2, "Adding new ghost halos...\n");
		addGhostHalos(moria_cfg, sd, &snap, hs, n_ghosts);
		cur_time = clock();
		timers->timers_1[MORIA_T1_GHOSTS] += cur_time;
	}

	/*
	 * Combine the catalog and SPARTA data, create a mask for halos to be written to catalog file.
	 */
	timers->timers_1[MORIA_T1_COMBINE] -= cur_time;
	output(2, "Combining halo data...\n");
	hs->out_mask = (int_least8_t*) memAlloc(__FFL__, MID_MORIA_HALOS,
			sizeof(int_least8_t) * hs->n_tot);
	hs->n_out_cat = 0;
	for (i = 0; i < hs->n_tot; i++)
	{
		moriaProcessHalo(moria_cfg, sd, &snap, hs, i);
		hs->out_mask[i] = hs->hd[i].do_output;
		if (hs->out_mask[i])
			hs->n_out_cat++;
	}
	cur_time = clock();
	timers->timers_1[MORIA_T1_COMBINE] += cur_time;

	/*
	 * Compute subhalo relations
	 */
	timers->timers_1[MORIA_T1_HOSTSUB] -= cur_time;
	output(2, "Computing subhalo relations...\n");
	computeSubhaloRelations(moria_cfg, &snap, hs);
	cur_time = clock();
	timers->timers_1[MORIA_T1_HOSTSUB] += cur_time;

	/*
	 * Compile and print statistics about the halo population
	 */
	timers->timers_1[MORIA_T1_HALOSTATS] -= cur_time;
	printHaloStatistics(moria_cfg, sd, hs);
	cur_time = clock();
	timers->timers_1[MORIA_T1_HALOSTATS] += cur_time;

	/*
	 * Connect halos to the merger tree
	 */
	if (moria_cfg->output_tree)
	{
		timers->timers_1[MORIA_T1_TREE] -= cur_time;
		output(2, "Assigning halos to tree...\n");
		assignHalosToTree(moria_cfg, sd, &snap, hs, ts);
		findTreeMergers(moria_cfg, &snap, hs, ts);
		cur_time = clock();
		timers->timers_1[MORIA_T1_TREE] += cur_time;
	}

	/*
	 * If we are writing HDF5 catalogs or a tree file, we will need the additional catalog fields
	 * now. Whether we load the fields for all halos or only those that will be output depends on
	 * whether we are writing the tree file.
	 *
	 * For the catalog output format, this is not necessary as the entire catalog will be copied
	 * anyway.
	 */
	if (((moria_cfg->output_hdf5) || (moria_cfg->output_tree)) && (moria_cfg->n_cat_fields > 0))
	{
		output(2, "Reading additional catalog data...\n");
		timers->timers_1[MORIA_T1_READCAT2] -= cur_time;
		readCatalogFields(moria_cfg, hs, snap_idx);
		cur_time = clock();
		timers->timers_1[MORIA_T1_READCAT2] += cur_time;
	}

	/*
	 * Write catalog file with subset of halos.
	 */
	if ((moria_cfg->output_hdf5) && (hs->n_out_cat > 0))
	{
		output(2, "Writing HDF5 output file...\n");
		timers->timers_1[MORIA_T1_OUTHDF5] -= cur_time;
		outputHDF5Catalog(moria_cfg, sd, &snap, hs, 0, NULL);
		cur_time = clock();
		timers->timers_1[MORIA_T1_OUTHDF5] += cur_time;
	}

	/*
	 * Write temporary tree file with all halos. After that, we do not need the tree indices of the
	 * halos any longer.
	 */
	if (moria_cfg->output_tree)
	{
		output(2, "Writing temporary file for tree...\n");
		timers->timers_1[MORIA_T1_TREE] -= cur_time;
		outputHDF5Catalog(moria_cfg, sd, &snap, hs, 1, ts->halo_tree_idxs);
		cur_time = clock();
		timers->timers_1[MORIA_T1_TREE] += cur_time;
	}

	/*
	 * Write catalog file in original format.
	 */
	if (moria_cfg->output_original)
	{
		output(2, "Writing original catalog output file...\n");
		timers->timers_1[MORIA_T1_OUTCATALOG] -= cur_time;
		outputOriginal(moria_cfg, hs, snap_idx);
		cur_time = clock();
		timers->timers_1[MORIA_T1_OUTCATALOG] += cur_time;
	}

	/*
	 * If this is the final snapshot, resolve tree mergers at the end of the simulation. This
	 * function needs to be called from within the snapshot function because it does rely on halo
	 * data at the final snapshot, such as the parent ID.
	 *
	 * Even if this is not the last snapshot, we need to free the memory for the tree indices.
	 */
	if (moria_cfg->output_tree)
	{
		timers->timers_1[MORIA_T1_TREE] -= cur_time;
		if (is_final_snap)
		{
			printLine(0);
			output(0, "Resolving tree mergers...\n");
			findTreeMergersFinal(moria_cfg, hs, ts);
		}
		memFree(__FFL__, MID_MORIA_TREE, ts->halo_tree_idxs, sizeof(int) * hs->n_tot);
		ts->halo_tree_idxs = NULL;
		cur_time = clock();
		timers->timers_1[MORIA_T1_TREE] += cur_time;
	}

	/*
	 * Finalize, free halo set
	 */
	timers->timers_1[MORIA_T1_INITFINAL] -= cur_time;
	output(2, "Freeing memory...\n");
	freeHaloSet(moria_cfg, hs);
}

SnapshotData computeSnapshotData(MoriaConfig *moria_cfg, int snap_idx)
{
	int i;
	SnapshotData snap;

	/*
	 * Compute properties at this snapshot
	 */
	snap.snap_idx = snap_idx;
	snap.a = moria_cfg->snap_a[snap_idx];
	snap.z = 1.0 / snap.a - 1.0;
	snap.t = age(&(moria_cfg->cosmo), snap.z);

	snap.rho_200m = moria_cfg->snap_rho_200m[snap_idx];
	snap.rho_vir = moria_cfg->snap_rho_vir[snap_idx];
	snap.rho_catmain = moria_cfg->snap_rho_catmain[snap_idx];

	snap.Om = OmegaM(&(moria_cfg->cosmo), snap.z);
	snap.comoving_conversion = 0.001 / snap.a;

	/*
	 * Compute properties at the snapshot one dynamical time ago
	 */
	snap.z_tdyn = ageInverse(&(moria_cfg->cosmo), snap.t - moria_cfg->snap_t_dyn[snap_idx]);
	if (snap.z_tdyn <= -1.0)
		error(__FFL__, "Could not invert age %.4f Gyr to a redshift.\n",
				snap.t - moria_cfg->snap_t_dyn[snap_idx]);
	snap.a_tdyn = 1.0 / (1.0 + snap.z_tdyn);
	snap.snap_idx_tdyn = closestInArray(moria_cfg->snap_a, moria_cfg->n_snaps, snap.a_tdyn);

	if ((snap.snap_idx - snap.snap_idx_tdyn < MORIA_DYNTIME_MIN_N)
			|| (fabs(snap.a_tdyn - moria_cfg->snap_a[snap.snap_idx_tdyn]) > MORIA_DYNTIME_TOLERANCE))
	{
		snap.snap_idx_tdyn = INVALID_IDX;
		snap.a_tdyn = -1.0;
		snap.z_tdyn = -1.0;
		snap.t_tdyn = -1.0;
		snap.rho_200m_tdyn = -1.0;

		warningOrError(__FFL__, moria_cfg->err_level_acc_rate_undefined,
				"Mass accretion rate cannot be computed for this snapshot (snaps %d and %d, z-target %.4f, a-target %.4f, a %.4f.\n",
				snap.snap_idx, snap.snap_idx_tdyn, snap.z_tdyn, snap.a_tdyn, snap.a);
	} else
	{
		snap.a_tdyn = moria_cfg->snap_a[snap.snap_idx_tdyn];
		snap.z_tdyn = 1.0 / snap.a_tdyn - 1.0;
		snap.t_tdyn = moria_cfg->snap_t[snap.snap_idx_tdyn];
		snap.rho_200m_tdyn = moria_cfg->snap_rho_200m[snap.snap_idx_tdyn];
	}

	for (i = 0; i < moria_cfg->n_defs_all; i++)
	{
		if (moria_cfg->all_defs[i].type == HDEF_TYPE_SO)
		{
			snap.rho_threshold_defs[i] = densityThreshold(&(moria_cfg->cosmo), snap.z,
					&(moria_cfg->all_defs[i]));
		} else
		{
			snap.rho_threshold_defs[i] = -1.0;
		}
	}

	return snap;
}
