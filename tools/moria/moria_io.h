/*************************************************************************************************
 *
 * I/O routines for moria.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_IO_H_
#define _MORIA_IO_H_

#include "moria_global.h"
#include "moria_config.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void createOutputDirectories(MoriaConfig *moria_cfg);
void outputHDF5Catalog(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		int is_tree_file, int *tree_idxs);
void outputHDF5Tree(MoriaConfig *moria_cfg, SpartaData *sd, TreeSet *ts);

#endif
