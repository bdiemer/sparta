/*************************************************************************************************
 *
 * I/O routines for moria.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_IO_HALOCAT_H_
#define _MORIA_IO_HALOCAT_H_

#include "moria_global.h"
#include "moria_config.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

enum
{
	MORIA_CAT_HALODEF,
	MORIA_CAT_ID,
	MORIA_CAT_UPID,
	MORIA_CAT_X,
	MORIA_CAT_Y,
	MORIA_CAT_Z,
	MORIA_CAT_N
};

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

HaloSet* getHaloSetFromCatalog(MoriaConfig *moria_cfg, SnapshotData *snap, int n_halos_extra);
void readCatalogFields(MoriaConfig *moria_cfg, HaloSet *hs, int snap_idx);
void outputOriginal(MoriaConfig *moria_cfg, HaloSet *hs, int snap_idx);

#endif
