/*************************************************************************************************
 *
 * This unit merges the catalog data with SPARTA data.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "moria_merge.h"
#include "moria_merge_rsp.h"
#include "moria_merge_hps.h"
#include "moria_io.h"
#include "../models/models_rsp.h"
#include "../../src/memory.h"
#include "../../src/utils.h"
#include "../../src/cosmology.h"
#include "../../src/halos/halo.h"
#include "../../src/halos/halo_definitions.h"
#include "../../src/halos/halo_so.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Tolerances for the algorithm when determining quantities at slightly different times. Each
 * number multiplies the current dynamical time:
 *
 * TDYN_ACCRATE:        The minimum distance in time for computing the accretion rate. When the
 *                      time interval gets too short, the result becomes very noisy.
 */
#define TOLERANCE_TDYN_ACCRATE 0.25

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void setPhantomXV(MoriaConfig *moria_cfg, HaloData *hd, SpartaData *sd, SnapshotData *snap);
void setHaloMass(MoriaConfig *moria_cfg, HaloSet *hs, HaloData *hd, SnapshotData *snap,
		int halo_idx);
void setMassAccretionRate(MoriaConfig *moria_cfg, HaloData *hd, SpartaData *sd, SnapshotData *snap);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Given a catalog line, set the radii and status for one halo. Most importantly, also determine
 * whether this halo should be added into the output catalog. It should if any mass definition
 * exceeds the necessary number of particles.
 */
void moriaProcessHalo(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		int halo_idx)
{
	int j, halo_valid;
	float cut_q;
	HaloData *hd;

	/*
	 * Shortcut to the HaloData record for this halo. We can't pass that directly because we also
	 * need the defs / upid_defs fields from the haloset.
	 */
	hd = &(hs->hd[halo_idx]);

	/*
	 * Set halo status and other fundamental properties that are always in Sparta data.
	 */
	if (hd->sparta_idx != -1)
	{
		hd->status_sparta = sd->sparta_status[hd->sparta_idx][snap->snap_idx];
		halo_valid = (hd->status_sparta > 0);
		hd->R200m_all_spa = sd->halo_R200m[hd->sparta_idx][snap->snap_idx];
		if (hd->R200m_all_spa < 0.0)
			error(__FFL__, "Found invalid radius\n");
		hd->M200m_all_spa = soRtoM(hd->R200m_all_spa, snap->rho_200m);
	} else
	{
		hd->status_sparta = HALO_STATUS_NOT_FOUND;
		halo_valid = 0;
		hd->R200m_all_spa = INVALID_R;
		hd->M200m_all_spa = INVALID_M;
	}

	/*
	 * Compare status to halo catalog. For example, if halo is sub, that should be reflected.
	 */
	if (halo_valid)
	{
		switch (hd->status_sparta)
		{
		case HALO_STATUS_NOT_FOUND:
		case HALO_STATUS_NONE:
			error(__FFL__, "Halo should not be marked valid with status %d.\n", hd->status_sparta);
			break;
		case HALO_STATUS_DEAD:
			error(__FFL__, "Status %d (dead) should not occur in output file.\n",
					hd->status_sparta);
			break;
		case HALO_STATUS_HOST:
			if (hd->cat_upid != -1)
				error(__FFL__, "Halo ID %ld, found status %d (host) but upid %ld.\n", hd->id,
						hd->status_sparta, hd->cat_upid);
			break;
		case HALO_STATUS_SUB:
		case HALO_STATUS_BECOMING_SUB:
		case HALO_STATUS_BECOMING_HOST:
		case HALO_STATUS_BECOMING_SUB_HOST:
		case HALO_STATUS_SWITCHED_HOST:
			if (hd->cat_upid == -1)
				error(__FFL__, "Halo ID %ld, found status (sub) %d but upid -1, snap %d.\n", hd->id,
						hd->status_sparta, snap->snap_idx);
			break;
		case HALO_STATUS_CONNECTING_HOST:
		case HALO_STATUS_CONNECTING_SUB:
		case HALO_STATUS_CONNECTING_SUB_SWITCH:
		case HALO_STATUS_SWITCHING_HOST:
			error(__FFL__, "Status %d (connecting / switching) should not occur in output file.\n",
					hd->status_sparta);
			break;
		case HALO_STATUS_GHOST_HOST:
			error(__FFL__, "Status %d (ghost host) not implemented in MORIA.\n", hd->status_sparta);
			break;
		case HALO_STATUS_GHOST_SUB:
		case HALO_STATUS_GHOST_SWITCHING:
			if (hd->cat_upid == INVALID_ID)
				error(__FFL__, "Found ghost without parent ID.\n");
			break;
		default:
			error(__FFL__, "Unknown halo status %d in halo ID %ld, sparta idx %d.\n",
					hd->status_sparta, hd->id, hd->sparta_idx);
			break;
		}
	}

	/*
	 * First check on whether we'll output this halo; if the cut is based on a catalog quantity and
	 * the halo does not make the cut, we might as well stop here.
	 */
	hd->do_output = -1;
	cut_q = -1.0;
	if (moria_cfg->cut_def.source == HDEF_SOURCE_CATALOG)
	{
		cut_q = hs->hd_defs[moria_cfg->cut_def.idx][halo_idx];
		hd->do_output = (cut_q >= moria_cfg->cut_threshold);
	}

	/*
	 * If we are compiling a tree, the halo might be output so we need to continue. Similarly, if
	 * we are not compiling a tree but want all halos to be included in host-sub relations, we
	 * need to continue.
	 */
	if (moria_cfg->hostsub_restrict_to_output && !moria_cfg->output_tree && (!hd->do_output))
	{
		debugHaloMoria(hd->id,
				"Halo cut quantity %.2e is below threshold %.2e, discarding from catalog.", cut_q,
				moria_cfg->cut_threshold);
		return;
	}

	/*
	 * For phantoms, we may want to correct the catalog position and velocity to the SPARTA values.
	 */
	if (moria_cfg->adjust_phantom_xv)
		setPhantomXV(moria_cfg, hd, sd, snap);

	/*
	 * Set the main mass, main radius, and peak height; note that no SPARTA data is needed for this step
	 */
	setHaloMass(moria_cfg, hs, hd, snap, halo_idx);

	/*
	 * Set mass accretion rate
	 */
	setMassAccretionRate(moria_cfg, hd, sd, snap);

	/*
	 * Output halo data so far; this is useful for debugging if one of the following merge functions
	 * crashes.
	 */
	debugHaloMoria(hd->id,
			"Assigned sparta_status %d, do_output %d, R200m_all %.2e, M200m_all %.2e, nu200m %.2f, acc rate status %d, Gamma %.2f",
			hd->status_sparta, hd->do_output, hd->R200m_all_spa, hd->M200m_all_spa, hd->nu200m,
			hd->status_acc_rate, hd->acc_rate_200m_dyn);

	/*
	 * Now do the actual combining with the analyses.
	 */
	if (sd->anl_rsp.anl_is_loaded)
	{
		mergeAnalysisRsp(moria_cfg, sd, snap, hs, halo_idx);
	} else
	{
		hd->status_moria_rsp = MORIA_STATUS_ANL_DOES_NOT_EXIST;
	}

	if (sd->anl_hps.anl_is_loaded)
	{
		mergeAnalysisHaloProps(moria_cfg, sd, snap, hs, halo_idx);
	} else
	{
		for (j = 0; j < moria_cfg->n_defs_all; j++)
			hs->hd_def_status[j][halo_idx] = MORIA_STATUS_ANL_DOES_NOT_EXIST;
	}

	/*
	 * If we were not yet able to determine whether this halo should be output (because that depends
	 * on properties from the analyses), we do so now.
	 */
	if (hd->do_output == -1)
	{
		cut_q = hs->hd_defs[moria_cfg->cut_def.idx][halo_idx];
		hd->do_output = (cut_q >= moria_cfg->cut_threshold);
	}
}

/*
 * We do not attempt to replace the phantom position and velocity if the halo was a host because
 * in that case, SPARTA will not have changed it.
 */
void setPhantomXV(MoriaConfig *moria_cfg, HaloData *hd, SpartaData *sd, SnapshotData *snap)
{
	if (!hd->phantom)
		return;

	if (hd->sparta_idx == -1)
		return;

	if (isHostStatus(hd->status_sparta))
		return;

	int d;

	for (d = 0; d < 3; d++)
	{
		debugHaloMoria(hd->id,
				"Replacing phantom x dim %d old %7.3f new %7.3f difference %.2e cMpc/h.", d,
				hd->x[d], sd->halo_x[hd->sparta_idx][snap->snap_idx][d],
				fabs(hd->x[d] - sd->halo_x[hd->sparta_idx][snap->snap_idx][d]));
		debugHaloMoria(hd->id,
				"Replacing phantom v dim %d old %+7.1f new %+7.1f difference %7.1f km/s.", d,
				hd->v[d], sd->halo_v[hd->sparta_idx][snap->snap_idx][d],
				fabs(hd->v[d] - sd->halo_v[hd->sparta_idx][snap->snap_idx][d]));

		hd->x[d] = sd->halo_x[hd->sparta_idx][snap->snap_idx][d];
		hd->v[d] = sd->halo_v[hd->sparta_idx][snap->snap_idx][d];
	}
}

/*
 * Set the catalog mass, we might need it. If the Sparta mass is not available, we set it as
 * a reference mass from the catalog. It is also possible that the catalog mass is not available,
 * which is tolerable. However, we do need one of the two to be valid.
 *
 * We also check for cases where the SPARTA mass is much larger than the catalog mass, which can
 * indicate halos that included other halos nearby. If the ratio exceeds a certain threshold, we
 * adopt the catalog mass.
 */
void setHaloMass(MoriaConfig *moria_cfg, HaloSet *hs, HaloData *hd, SnapshotData *snap,
		int halo_idx)
{
	int use_catalog_rm;
	float ratio;

	if (moria_cfg->main_def_catalog.quantity == HDEF_Q_RADIUS)
	{
		hd->Rmain_cat = hs->hd_defs[moria_cfg->main_def_catalog.idx][halo_idx];
		hd->Mmain_cat = soRtoM(hd->Rmain_cat, snap->rho_catmain);
	} else if (moria_cfg->main_def_catalog.quantity == HDEF_Q_MASS)
	{
		hd->Mmain_cat = hs->hd_defs[moria_cfg->main_def_catalog.idx][halo_idx];
		hd->Rmain_cat = soMtoR(hd->Mmain_cat, snap->rho_catmain);
	} else
	{
		error(__FFL__, "Main catalog quantity must be either radius or mass.\n");
	}

	/*
	 * Decision whether to use the catalog or SPARTA mass and radius. For ghost halos, there is
	 * no choice because there is no catalog data. For other halos, we check whether both exist.
	 * If only one exists, we adopt it. If both exist, we compute their ratio and use the
	 * SPARTA value as long as the ratio does not exceed some threshold.
	 */
	if (isGhostStatus(hd->status_sparta))
	{
		use_catalog_rm = 0;
	} else
	{
		if ((hd->R200m_all_spa > 0.0) && (hd->Rmain_cat > 0.0))
		{
			ratio = hd->R200m_all_spa / hd->Rmain_cat;
			use_catalog_rm = (ratio > moria_cfg->max_mass_ratio_sparta);
		} else
		{
			use_catalog_rm = (hd->Rmain_cat > 0.0);
		}
	}

	if (use_catalog_rm)
	{
		hd->R_ref = hd->Rmain_cat;
		hd->M_ref = hd->Mmain_cat;
	} else
	{
		hd->R_ref = hd->R200m_all_spa;
		hd->M_ref = hd->M200m_all_spa;
	}

	if ((hd->R_ref <= 0.0) || (hd->M_ref <= 0.0))
	{
		error(__FFL__, "Found radius %.2e, mass %.2e (ID %ld, status %d) %d %d %.2e %.2e.\n",
				hd->R_ref, hd->M_ref, hd->id, hd->status_sparta, halo_idx,
				moria_cfg->main_def_catalog.idx,
				hs->hd_defs[moria_cfg->main_def_catalog.idx][halo_idx], hd->R200m_all_spa);
	}

	/*
	 * Compute the peak height from the reference mass.
	 */
	hd->nu200m = peakHeight(&(moria_cfg->cosmo), snap->z, hd->M_ref);
}

/*
 * Compute the mass accretion rate if possible, otherwise approximate it using the model
 */
void setMassAccretionRate(MoriaConfig *moria_cfg, HaloData *hd, SpartaData *sd, SnapshotData *snap)
{
	int i, snap_infall, snap_target_new, is_reduced;
	float R200m, M200m_past, M200m_now, nu200m_past, t_past, z_past, a_past, a_now, tdyn_current,
			t_infall, t_diff_new, t_diff_old;

	/*
	 * We set a negative acc rate status as an indication that no value has been set yet.
	 */
	hd->status_acc_rate = -1;
	snap_infall = -1;

	/*
	 * First check: if there is no SPARTA record of this halo, we have little chance of doing
	 * anything interesting. Also, for the first snapshots in a simulation, there is no one
	 * dynamical time ago, so we should not attempt the calculation.
	 */
	if ((hd->sparta_idx != -1) && (hd->status_sparta > 0) && (snap->snap_idx_tdyn != INVALID_IDX))
	{
		M200m_past = -1.0;
		M200m_now = -1.0;
		a_past = -1.0;
		a_now = -1.0;
		tdyn_current = moria_cfg->snap_t_dyn[snap->snap_idx];

		if (hd->cat_upid == UPID_HOST)
		{
			/*
			 * For hosts, we check whether the snapshot a dynamical time ago was valid. If not,
			 * we go forward in time up to some fraction of a dynamical time.
			 */
			if (sd->sparta_status[hd->sparta_idx][snap->snap_idx_tdyn] == HALO_STATUS_HOST)
			{
				hd->status_acc_rate = ACCRATE_STATUS_SUCCESS;
				R200m = sd->halo_R200m[hd->sparta_idx][snap->snap_idx_tdyn];
				M200m_past = soRtoM(R200m, snap->rho_200m_tdyn);
				M200m_now = hd->M200m_all_spa;
				a_past = snap->a_tdyn;
				a_now = snap->a;

				debugHaloMoria(hd->id, "Setting MAR from snapshot %d, %.2f tdyn ago, status = %d.",
						snap->snap_idx_tdyn,
						(snap->t - snap->t_tdyn) / moria_cfg->snap_t_dyn[snap->snap_idx],
						hd->status_acc_rate);
			} else
			{
				i = snap->snap_idx_tdyn + 1;
				while (i < snap->snap_idx)
				{
					t_past = moria_cfg->snap_t[i];
					if ((snap->t - t_past) < TOLERANCE_TDYN_ACCRATE * tdyn_current)
						break;

					if (sd->sparta_status[hd->sparta_idx][i] == HALO_STATUS_HOST)
					{
						hd->status_acc_rate = ACCRATE_STATUS_REDUCED_INTERVAL;
						R200m = sd->halo_R200m[hd->sparta_idx][i];
						M200m_past = soRtoM(R200m, moria_cfg->snap_rho_200m[i]);
						M200m_now = hd->M200m_all_spa;
						a_past = moria_cfg->snap_a[i];
						a_now = snap->a;

						debugHaloMoria(hd->id,
								"Setting MAR from snapshot %d instead of %d, t-diff %.2f tdyn, status = %d.",
								i, snap->snap_idx_tdyn, (snap->t - t_past) / tdyn_current,
								hd->status_acc_rate);
						break;
					}
					i++;
				}
			}
		} else
		{
			/*
			 * For subs, we attempt to go back to the redshift of accretion. Regardless of how long
			 * ago that happened, we want to compute acc rate at infall rather than a current acc rate.
			 */
			i = snap->snap_idx - 1;
			while (i >= 0)
			{
				if (sd->sparta_status[hd->sparta_idx][i] == HALO_STATUS_HOST)
					break;
				i--;
			}

			/*
			 * If we found a snapshot where the halo was a host, we can attempt a computation again.
			 * We start from one dynamical time ago and move forward.
			 */
			if (i > 0)
			{
				snap_infall = i;
				t_infall = moria_cfg->snap_t[snap_infall];
				tdyn_current = moria_cfg->snap_t_dyn[snap_infall];
				t_past = t_infall - tdyn_current;
				if (t_past > 0.0)
				{
					/*
					 * Find the snapshot closest to t_past. We could invert the time to get a scale
					 * factor and then look in the a array, but that involved root finding and
					 * seems to give indistinguishable results from the simpler algorithm below.
					 */
					snap_target_new = snap_infall;
					t_diff_old = t_infall;
					while (snap_target_new >= 0)
					{
						t_diff_new = fabs(t_past - moria_cfg->snap_t[snap_target_new]);
						if (t_diff_new < t_diff_old)
						{
							t_diff_old = t_diff_new;
						} else
						{
							snap_target_new++;
							break;
						}
						snap_target_new--;
					}
					if (snap_target_new < 0)
						snap_target_new = 0;
					i = snap_target_new;
					is_reduced = 0;
					while (i < snap_infall)
					{
						t_past = moria_cfg->snap_t[i];
						if ((t_infall - t_past) < TOLERANCE_TDYN_ACCRATE * tdyn_current)
							break;
						if (sd->sparta_status[hd->sparta_idx][i] == HALO_STATUS_HOST)
						{
							if (is_reduced)
								hd->status_acc_rate = ACCRATE_STATUS_SUB_REDUCED_INTERVAL;
							else
								hd->status_acc_rate = ACCRATE_STATUS_SUB_SUCCESS;
							R200m = sd->halo_R200m[hd->sparta_idx][i];
							M200m_past = soRtoM(R200m, moria_cfg->snap_rho_200m[i]);
							R200m = sd->halo_R200m[hd->sparta_idx][snap_infall];
							M200m_now = soRtoM(R200m, moria_cfg->snap_rho_200m[snap_infall]);
							a_past = moria_cfg->snap_a[i];
							a_now = moria_cfg->snap_a[snap_infall];
							if (is_reduced)
							{
								debugHaloMoria(hd->id,
										"Setting MAR of sub from snapshot %d instead of %d, t-diff %.2f tdyn, status = %d.",
										i, snap_target_new, (t_infall - t_past) / tdyn_current,
										hd->status_acc_rate);
							} else
							{
								debugHaloMoria(hd->id,
										"Setting MAR of sub, infall %d, target %d, t-diff %.2f tdyn, status = %d.",
										snap_infall, snap_target_new,
										(t_infall - t_past) / tdyn_current, hd->status_acc_rate);
							}
							break;
						}
						i++;
						is_reduced = 1;
					}
				}
			}
		}

		/*
		 * If any non-failure status has been set, we compute acc rate from the past time and mass.
		 */
		if (hd->status_acc_rate > 0)
		{
#if MORIA_CAREFUL
			if (M200m_past < 0.0)
				error(__FFL__, "Found M200m_past not set in MAR computation. Internal error.\n");
			if (M200m_now < 0.0)
				error(__FFL__, "Found M200m_now not set in MAR computation. Internal error.\n");
			if (a_past < 0.0)
				error(__FFL__, "Found a_past not set in MAR computation. Internal error.\n");
			if (a_now < 0.0)
				error(__FFL__, "Found a_now not set in MAR computation. Internal error.\n");
#endif
			hd->acc_rate_200m_dyn = (log(M200m_now) - log(M200m_past)) / (log(a_now) - log(a_past));
		}
	}

	/*
	 * Final possibility: guess from fitting function
	 */
	if (hd->status_acc_rate < 0)
	{
		if (hd->cat_upid == UPID_HOST)
		{
			/*
			 * For hosts, we just use the current redshift and peak height
			 */
			hd->acc_rate_200m_dyn = modelDiemer20Gamma(hd->nu200m, snap->z);
			hd->status_acc_rate = ACCRATE_STATUS_MODEL;

			debugHaloMoria(hd->id, "Setting MAR of host from model (nu %.2f, z %.2f), status = %d.",
					hd->nu200m, snap->z, hd->status_acc_rate);
		} else
		{
			/*
			 * If this is a sub and we don't have SPARTA data, the situation is particularly dire: we
			 * do not even know the redshift of infall. Otherwise, we compute the model prediction
			 * at the infall redshift.
			 */
			if (snap_infall < 0)
			{
				hd->acc_rate_200m_dyn = modelDiemer20Gamma(hd->nu200m, snap->z);
				hd->status_acc_rate = ACCRATE_STATUS_SUB_MODEL_NOW;

				debugHaloMoria(hd->id,
						"Setting MAR of sub from model at current time (nu %.2f, z %.2f), status = %d.",
						hd->nu200m, snap->z, hd->status_acc_rate);
			} else
			{
				z_past = moria_cfg->snap_z[snap_infall];
				R200m = sd->halo_R200m[hd->sparta_idx][snap_infall];
				M200m_past = soRtoM(R200m, moria_cfg->snap_rho_200m[snap_infall]);
				nu200m_past = peakHeight(&(moria_cfg->cosmo), z_past, M200m_past);
				hd->acc_rate_200m_dyn = modelDiemer20Gamma(nu200m_past, z_past);
				hd->status_acc_rate = ACCRATE_STATUS_SUB_MODEL;

				debugHaloMoria(hd->id,
						"Setting MAR of sub from model at infall (nu %.2f, z %.2f), status = %d.",
						nu200m_past, z_past, hd->status_acc_rate);
			}
		}
	}

	if (hd->status_acc_rate < 0)
		error(__FFL__, "Found undefined status at the end of MAR function. Internal error.\n");

	debugHaloMoria(hd->id, "Set MAR = %.2f, status %d.", hd->acc_rate_200m_dyn,
			hd->status_acc_rate);
}

/*
 * Find halo ID in sorted array, check that it really corresponds to the right ID.
 */
void findSpartaIndex(SpartaData *sd, MoriaSorter *sorters, HaloData *hd, int snap_idx)
{
	MoriaSorter temp_sorter, *p;

	if (hd->sparta_idx == INVALID_IDX)
	{
		temp_sorter.id = hd->id;
		p = (MoriaSorter*) bsearch(&temp_sorter, sorters, sd->n_halos, sizeof(MoriaSorter),
				&compareIndexSorters);
		if (p != NULL)
		{
			hd->sparta_idx = p->idx;
			if (hd->id != sd->halo_ids[hd->sparta_idx][snap_idx])
				error(__FFL__, "Found ID mismatch (%ld, %ld).\n", hd->id,
						sd->halo_ids[hd->sparta_idx][snap_idx]);

			debugHaloMoria(hd->id, "Found in SPARTA data, index %d.", hd->sparta_idx);
		} else
		{
			debugHaloMoria(hd->id, "Did not find in SPARTA data.");
		}
	} else
	{
		debugHaloMoria(hd->id, "SPARTA index already set in halo, index %d.", hd->sparta_idx);
	}
}

/*
 * If we are including ghost halos, some fields taken from the halo catalog need to be corrected,
 * namely the PID and mmp. If they are set incorrectly, they can mess up the tree.
 *
 * Similarly, we look for halos that have ended in SPARTA.
 *
 * This function cannot be called during the final snapshot as it relies on a future snapshot to
 * check whether a halo is alive.
 */
void resolveStatusConflicts(MoriaConfig *moria_cfg, SpartaData *sd, HaloData *hd, int snap_idx)
{
	int_least8_t next_status;

	if (hd->sparta_idx == INVALID_IDX)
		return;

	next_status = sd->sparta_status[hd->sparta_idx][snap_idx + 1];

	debugHaloMoria(hd->id, "Checking for conflicts; mmp %d, next status %d.", hd->mmp, next_status);

	if (hd->mmp)
	{
		/*
		 * This halo is not ending in the catalog, we check whether it ended in SPARTA and
		 * change the mmp/desc_id fields accordingly.
		 */
		if (next_status == HALO_STATUS_NONE)
		{
			debugHaloMoria(hd->id,
					"Halo continues in catalog (desc_id %ld) but ends in SPARTA; ending in MORIA.",
					hd->desc_id);
			debugHaloMoria(hd->desc_id,
					"Progenitor %ld continues in catalog but ends in SPARTA; ending in MORIA.",
					hd->id);
			hd->mmp = 0;
			hd->desc_id = INVALID_ID;
		}
	} else
	{
		/*
		 * This halo is ending in the catalog, we check whether it was converted into a ghost.
		 */
		if ((moria_cfg->include_ghosts) && (isGhostStatus(next_status)))
		{
			hd->mmp = 1;
			hd->desc_id = sd->halo_ids[hd->sparta_idx][snap_idx + 1];

			debugHaloMoria(hd->id,
					"Halo ends in catalog but continues as ghost in SPARTA; set descendant to %ld.",
					hd->desc_id);
		}
	}
}
