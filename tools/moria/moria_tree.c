/*************************************************************************************************
 *
 * This unit implements MORIA's merger tree functionality.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "moria_tree.h"
#include "../../src/utils.h"
#include "../../src/memory.h"
#include "../../src/halos/halo.h"
#include "../../src/halos/halo_ghosts.h"

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct LinkedListElement LinkedListElement_;

typedef struct LinkedListElement
{
	TreeHalo *me;
	LinkedListElement_ *up;
	LinkedListElement_ *dn;
	LinkedListElement_ *root;
	LinkedListElement_ *tail;
} LinkedListElement;

typedef struct TreeComparisonElement
{
	float order_q;
	int merge_snap;
	LinkedListElement *lle;
} TreeComparisonElement;

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void updateTreeHalo(MoriaConfig *moria_cfg, TreeHalo *th, HaloData *hd, SpartaData *sd,
		int snap_idx);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

TreeSet* initializeTreeSet()
{
	TreeSet *ts;

	ts = (TreeSet*) memAlloc(__FFL__, MID_MORIA_TREE, sizeof(TreeSet));

	ts->n_alloc = 0;
	ts->n = 0;
	ts->n_out = 0;
	ts->th = NULL;
	ts->halo_tree_idxs = NULL;
	ts->tree_out_idxs = NULL;

	return ts;
}

void initializeTreeHalo(TreeHalo *th)
{
	th->do_output = 0;
	th->is_alive = 0;
	th->id = INVALID_ID;
	th->desc_id = INVALID_ID;
	th->sparta_idx = INVALID_IDX;
	th->merge_idx = INVALID_IDX;
	th->merge_snap = INVALID_IDX;
	th->merge_order_q = INVALID_R;
}

void increaseTreeSet(MoriaConfig *moria_cfg, TreeSet *ts, int n_target)
{
	int i, n_new;

	if (ts->n_alloc == 0)
		n_new = n_target * moria_cfg->n_snaps;
	else
		n_new = n_target * 2.0;

	if (ts->th == NULL)
		ts->th = (TreeHalo*) memAlloc(__FFL__, MID_MORIA_TREE, sizeof(TreeHalo) * n_new);
	else
		ts->th = (TreeHalo*) memRealloc(__FFL__, MID_MORIA_TREE, ts->th, sizeof(TreeHalo) * n_new,
				sizeof(TreeHalo) * ts->n_alloc);

	for (i = ts->n_alloc; i < n_new; i++)
		initializeTreeHalo(&(ts->th[i]));

	ts->n_alloc = n_new;
}

void freeTreeSet(TreeSet *ts)
{
	if (ts->n_alloc > 0)
		memFree(__FFL__, MID_MORIA_TREE, ts->th, sizeof(TreeHalo) * ts->n_alloc);
	if (ts->tree_out_idxs != NULL)
		memFree(__FFL__, MID_MORIA_TREE, ts->tree_out_idxs, sizeof(int) * ts->n);
	memFree(__FFL__, MID_MORIA_TREE, ts, sizeof(TreeSet));
}

/*
 * This is the main function in the tree logic. At each snapshot, we go through the tree elements
 * that have previously been created and find the corresponding halos at this snapshot. For new
 * halos, we create new tree elements.
 */
void assignHalosToTree(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		TreeSet *ts)
{
	int i, h_idx, n_found, n_new;
	TreeHalo *th;
	MoriaSorter *sorters, temp_sorter, *p;

	/*
	 * Create an array of size n_halos where we store the tree index for each halo. This will be
	 * written to file in the end.
	 */
	ts->halo_tree_idxs = (int*) memAlloc(__FFL__, MID_MORIA_TREE, sizeof(int) * hs->n_tot);
	for (i = 0; i < hs->n_tot; i++)
		ts->halo_tree_idxs[i] = INVALID_IDX;

	/*
	 * Sort halos by ID
	 */
	sorters = (MoriaSorter*) memAlloc(__FFL__, MID_MORIA_TREE, sizeof(MoriaSorter) * hs->n_tot);
	for (i = 0; i < hs->n_tot; i++)
	{
		sorters[i].id = hs->hd[i].id;
		sorters[i].idx = i;
	}
	qsort(sorters, hs->n_tot, sizeof(MoriaSorter), &compareIndexSorters);

	/*
	 * Go through halos in tree. If they are alive and have a descendant, it should be in the halo
	 * set.
	 */
	n_found = 0;
	for (i = 0; i < ts->n; i++)
	{
		th = &(ts->th[i]);

		if (!th->is_alive)
			continue;

		if (th->desc_id == INVALID_ID)
			error(__FFL__,
					"Found tree index %d alive but without descendant ID (current ID %ld).\n", i,
					th->id);

		temp_sorter.id = th->desc_id;
		p = (MoriaSorter*) bsearch(&temp_sorter, sorters, hs->n_tot, sizeof(MoriaSorter),
				&compareIndexSorters);

		if (p == NULL)
			error(__FFL__,
					"Could not find descendant halo ID %ld in halo set (tree idx %d, ID %ld).\n",
					ts->th[i].desc_id, i, th->id);

		/*
		 * Check that this halo doesn't already have a tree index; that would be an error.
		 */
		if (ts->halo_tree_idxs[p->idx] != INVALID_IDX)
			error(__FFL__, "Tree halo %d found descendant ID %ld, but already has tree index %d.\n",
					i, th->desc_id, ts->halo_tree_idxs[p->idx]);

		/*
		 * Update the tree data and store the tree index for this halo
		 */
		h_idx = p->idx;
		ts->halo_tree_idxs[h_idx] = i;
		updateTreeHalo(moria_cfg, th, &(hs->hd[h_idx]), sd, snap->snap_idx);
		n_found++;
	}

	/*
	 * Add new halos to tree. First, we make sure the tree array is large enough.
	 */
	n_new = hs->n_tot - n_found;
	if (ts->n + n_new > ts->n_alloc)
		increaseTreeSet(moria_cfg, ts, ts->n + n_new);

	for (i = 0; i < hs->n_tot; i++)
	{
		if (ts->halo_tree_idxs[i] != INVALID_IDX)
			continue;

		ts->halo_tree_idxs[i] = ts->n;
		updateTreeHalo(moria_cfg, &(ts->th[ts->n]), &(hs->hd[i]), sd, snap->snap_idx);
		ts->n++;

#if MORIA_CAREFUL
		if (ts->n >= ts->n_alloc)
			error(__FFL__, "Exceeded array allocation.\n");
#endif
	}

#if MORIA_CAREFUL
	for (i = 0; i < hs->n_tot; i++)
	{
		if (ts->halo_tree_idxs[i] == INVALID_IDX)
			error(__FFL__, "Found halo without tree assignment.\n");
	}
#endif

	memFree(__FFL__, MID_MORIA_TREE, sorters, sizeof(MoriaSorter) * hs->n_tot);
}

/*
 * Update information in the tree set with fresh halo data. If the halo is new (as in, not yet
 * alive), we assign it the new halo's sparta_idx. This number should stay constant over time.
 *
 * We only need the SpartaData to check on a particular case where the sparta index in the tree
 * and the current halo disagree. This can happen if the halo lives on in the catalog but was
 * ended in SPARTA; even though we have fixed the mmp and desc_id in the snapshot where the halo
 * was ended, there can be a snapshot where the halo is not represented in SPARTA. Thus, the
 * initial tree element does not have a SPARTA index, which leads to a conflict once the halo is
 * recovered in SPARTA. We check for this case by making sure that the halo really did not exist
 * in SPARTA in the previous snapshot.
 */
void updateTreeHalo(MoriaConfig *moria_cfg, TreeHalo *th, HaloData *hd, SpartaData *sd,
		int snap_idx)
{
	int halo_ended_in_sparta;

	halo_ended_in_sparta = 0;
	if (th->is_alive)
	{
		if (hd->sparta_idx != th->sparta_idx)
		{
			/*
			 * If the tree was alive and the halo and tree SPARTA indices disagree, this indicates
			 * a problem unless the halo ended in SPARTA due to an error. In that case, we double
			 * check (using the current SPARTA index) that the halo did indeed not exist in SPARTA
			 * at the previous snapshot. If that is the case, we set the correct SPARTA index in
			 * the tree so that the problem should not appear any more.
			 */
			if (th->sparta_idx == -1)
			{
				if (sd->sparta_status[hd->sparta_idx][snap_idx - 1] == HALO_STATUS_NONE)
				{
					th->sparta_idx = hd->sparta_idx;
					debugHaloMoria(th->id,
							"Tree element: found conflicting SPARTA indices, halo was not alive in SPARTA previously; set to tree index %d.",
							th->sparta_idx);
					debugHaloMoria(hd->id,
							"Tree element: found conflicting SPARTA indices, halo was not alive in SPARTA previously; set to tree index %d.",
							th->sparta_idx);
				} else
				{
					error(__FFL__,
							"Found disagreement between current and tree SPARTA idx (current %d, tree %d, current ID %ld, tree ID %ld).\n",
							hd->sparta_idx, th->sparta_idx, hd->id, th->id);
				}
			} else if (hd->sparta_idx == -1)
			{
				error(__FFL__,
						"Found disagreeing current and tree SPARTA index (%d and %d) for halo ID %ld, desc_id %ld.\n",
						hd->sparta_idx, th->sparta_idx, hd->id, hd->desc_id);
			} else
			{
				error(__FFL__,
						"SPARTA indices of halo and tree halo do not agree (%d, %d), halo ID %ld.\n",
						hd->sparta_idx, th->sparta_idx, hd->id);
			}
		}
	} else
	{
		/*
		 * The tree index wasn't alive, meaning that this is the first snapshot; we assign the
		 * halo's SPARTA index to the tree. Note that this index can be -1 if the halo was not
		 * found in the SPARTA data.
		 */
		th->sparta_idx = hd->sparta_idx;
	}

	/*
	 * Decide if the halo will be alive in the next snapshot. If it has ended in SPARTA due to an
	 * error, we end it regardless of the catalog.
	 */
	if (halo_ended_in_sparta)
	{
		th->is_alive = 0;
	} else if (hd->mmp)
	{
		th->is_alive = 1;
	} else
	{
		th->is_alive = 0;
	}

	debugHaloMoria(th->id,
			"Tree element: found as previous halo in tree, set alive %d, sparta_idx %d.",
			th->is_alive, th->sparta_idx);

	/*
	 * Update properties in tree. We output a tree if it is output at ANY snapshot in the catalogs.
	 * The order quantity is the maximum of the reference mass (typically M200m_all) along a tree
	 * history.
	 */
	th->id = hd->id;
	th->do_output = (th->do_output || hd->do_output);
	th->merge_order_q = fmax(th->merge_order_q, hd->M_ref);

	if (th->is_alive)
	{
		th->desc_id = hd->desc_id;
	} else
	{
		/*
		 * If the halo is dead, we want the descendant ID to be void; however, we still need it to
		 * identify the tree element the halo merges into, so we temporarily set it to that ID
		 * (which is different for ghosts).
		 */
		if (isGhostID(th->id))
		{
			th->desc_id = hd->cat_upid;
		} else
		{
			th->desc_id = hd->desc_id;
		}
	}

	debugHaloMoria(th->id, "Tree element: set desc_id %ld, alive %d, do_output %d, sparta_idx %d.",
			th->desc_id, th->is_alive, th->do_output, th->sparta_idx);
	debugHaloMoria(th->desc_id, "Tree element: found as desc_id in ID %ld, alive %d, do_output %d.",
			th->id, th->is_alive, th->do_output);
}

void findTreeMergers(MoriaConfig *moria_cfg, SnapshotData *snap, HaloSet *hs, TreeSet *ts)
{
	int i, j, h_idx, i_first, i_last, found, merge_idx;
	TreeHalo *th, *th_merge, *th_merge2;
	MoriaSorter *sorters, temp_sorter, *p;

	/*
	 * We will need to look for halos by ID / desc_id for both ghosts and normal halos.
	 */
	sorters = (MoriaSorter*) memAlloc(__FFL__, MID_MORIA_TREE, sizeof(MoriaSorter) * ts->n);

	/*
	 * First, we tackle non-ghost halos; here, the final desc_id is the same as the desc_id of the
	 * tree they are merging into. Thus, we create sorters that are sorted by desc_id.
	 */
	for (i = 0; i < ts->n; i++)
	{
		sorters[i].id = ts->th[i].desc_id;
		sorters[i].idx = i;
		debugHaloMoria(ts->th[i].desc_id,
				"Finding tree mergers: added as descendant ID in sorter element %d.", i);
	}
	qsort(sorters, ts->n, sizeof(MoriaSorter), &compareIndexSorters);

	for (i = 0; i < ts->n; i++)
	{
		th = &(ts->th[i]);

		if (th->is_alive)
			continue;
		if (th->desc_id == INVALID_ID)
			continue;
		if (isGhostID(th->id))
			continue;

		/*
		 * Note that the correct halo is the one that is alive, will be alive in the next
		 * snapshot, and is the mmp; there could be arbitrarily many other halos with the same
		 * desc_id. Note that the bsearch function does not necessarily return the first
		 * element, so we need to search before and after it.
		 */
		debugHaloMoria(th->id,
				"Considering for mergers because not alive, not ghost, has desc_id %ld. Searching merge target.",
				th->desc_id);
		temp_sorter.id = th->desc_id;
		p = (MoriaSorter*) bsearch(&temp_sorter, sorters, ts->n, sizeof(MoriaSorter),
				&compareIndexSorters);
		if (p == NULL)
		{
			error(__FFL__, "Could not find tree any entries for descendant ID %ld (halo ID %ld).\n",
					th->desc_id, th->id);
		}

		// Find the first element
		i_first = (p - sorters);
		i_last = i_first;
		while ((sorters[i_first].id == th->desc_id) && (i_first > 0))
			i_first--;
		if (sorters[i_first].id != th->desc_id)
			i_first++;

		// Find the last element
		while ((sorters[i_last].id == th->desc_id) && (i_last < ts->n - 1))
			i_last++;
		if (sorters[i_last].id != th->desc_id)
			i_last--;

#if MORIA_CAREFUL
		if ((i_last < i_first) && (!ts->th[p->idx].is_alive))
		{
			error(__FFL__,
					"Found entries for descendant ID %ld, but all entries dead (halo ID %ld).\n",
					th->desc_id, th->id);
		}
#endif

		/*
		 * Now run from first to last and check for an element that has the same desc_id and is
		 * alive. If there are more than one, that is an error.
		 */
		found = 0;
		for (j = i_first; j <= i_last; j++)
		{
			h_idx = sorters[j].idx;
			if (ts->th[h_idx].is_alive)
			{
				if (found)
				{
					error(__FFL__,
							"Found more than one alive tree element with the same desc_id %ld.\n",
							th->desc_id);
				}
				th->merge_idx = h_idx;
				th->merge_snap = snap->snap_idx;
				th->desc_id = INVALID_ID;
				found = 1;

				debugHaloMoria(th->id, "Set merge target to halo ID %ld, idx %d, merge snap %d.",
						ts->th[h_idx].id, th->merge_idx, th->merge_snap);

				debugHaloMoria(ts->th[h_idx].id,
						"Set halo ID %ld to merge into this halo, idx %d, merge snap %d; currently is_alive %d, desc_id %ld.",
						th->id, th->merge_idx, th->merge_snap, ts->th[h_idx].is_alive,
						ts->th[h_idx].desc_id);
			}
		}
	}

	/*
	 * Now for ghosts; this is a separate routine because it diverges from the one for normal halos
	 * above in some key ways:
	 *
	 * - we need to look up tree elements by the ghost's parent ID (rather than descendant ID). Here,
	 *   the desc_id of the ghost has been set to its parent ID, which is the current ID of the tree
	 *   the ghost is merging into.
	 * - We expect to only find one halo by ID, as opposed to multiple halos by descendant ID.
	 * - It is possible that the halo the ghost is merging into is merging away itself at the same
	 *   snapshot, in which case we need to merge into its merge target. This is not the case for
	 *   normal halos where there has to be exactly one live descendant.
	 */
	if (moria_cfg->do_ghosts)
	{
		for (i = 0; i < ts->n; i++)
		{
			sorters[i].id = ts->th[i].id;
			sorters[i].idx = i;
			debugHaloMoria(ts->th[i].id, "Finding tree mergers: added ID in sorter element %d.", i);
		}
		qsort(sorters, ts->n, sizeof(MoriaSorter), &compareIndexSorters);

		for (i = 0; i < ts->n; i++)
		{
			th = &(ts->th[i]);

			if (th->is_alive)
				continue;
			if (th->desc_id == INVALID_ID)
				continue;
			if (!isGhostID(th->id))
				continue;

			debugHaloMoria(th->id,
					"Considering ghost for mergers because not alive, has parend ID %ld. Searching parent.",
					th->desc_id);
			temp_sorter.id = th->desc_id;

			p = (MoriaSorter*) bsearch(&temp_sorter, sorters, ts->n, sizeof(MoriaSorter),
					&compareIndexSorters);
			if (p == NULL)
			{
				error(__FFL__,
						"Could not find tree entry for ghost parent ID %ld (ghost ID %ld).\n",
						th->desc_id, th->id);
			}

			/*
			 * In most cases, the parent ID is alive and we merge into it. However, if it
			 * happens to merge itself at this snapshot, we need to "follow" it. The merge
			 * target must have been set, however, since all normal halos were treated before
			 * all the ghosts. Since the merge targets for halos are uniquely defined by the
			 * catalog, we do not need to iterate here.
			 */
			merge_idx = p->idx;
			th_merge = &(ts->th[merge_idx]);
			if (!th_merge->is_alive)
			{
				merge_idx = th_merge->merge_idx;
				th_merge2 = &(ts->th[merge_idx]);

				if (!th_merge2->is_alive)
				{
					error(__FFL__,
							"Trying to set merge target; found dead parent %ld, merge idx %d, but that merge target ID %ld is also dead.\n",
							th_merge->id, p->idx, th_merge2->id);
				}
				th_merge = th_merge2;
			}

			th->merge_idx = merge_idx;
			th->merge_snap = snap->snap_idx;
			th->desc_id = INVALID_ID;

			debugHaloMoria(th->id, "Set merge target to halo ID %ld, idx %d, merge snap %d.",
					th_merge->id, th->merge_idx, th->merge_snap);

			debugHaloMoria(th_merge->id,
					"Set halo ID %ld to merge into this halo, idx %d, merge snap %d; currently is_alive %d, desc_id %ld.",
					th->id, i, th->merge_snap, th_merge->is_alive, th_merge->desc_id);
		}
	}

	/*
	 * Now check for any halos that are not alive but have a desc_id; they should have been fixed
	 */
#if MORIA_CAREFUL
	for (i = 0; i < ts->n; i++)
	{
		th = &(ts->th[i]);
		if ((!th->is_alive) && (th->desc_id != INVALID_ID))
			error(__FFL__,
					"Found tree element that is dead but has a descendant ID %ld; internal error (ID %ld).\n",
					th->desc_id, th->id);
	}
#endif

	memFree(__FFL__, MID_MORIA_TREE, sorters, sizeof(MoriaSorter) * ts->n);
}

/*
 * Finally, if we are at the last snap and need to resolve some issues with the merge targets in
 * the tree.
 *
 * First, there are subhalos that are alive but should still get a merge index into their host halos
 * for sorting. For root halos, i.e. hosts alive at the final snap, we set a merge_snap even one
 * snapshot further for sorting.
 *
 * This process must be iterative because there could be subhalos of subhalos, in which case
 * we must first find the parent of the sub-parent and so on. In practice, the iteration should
 * converge quickly.
 *
 * Finally, we need to consider merge targets that are not being output. For the purposes of
 * ordering and giving the user merge target indices, only merge targets that are being output
 * should be considered.
 */
void findTreeMergersFinal(MoriaConfig *moria_cfg, HaloSet *hs, TreeSet *ts)
{
	int i, iter, n_deferred, h_idx, t_idx, n_snaps, snap_root;
	TreeHalo *th = NULL, *th_merge = NULL;
	HaloData *hd = NULL, *hd_merge = NULL;
	MoriaSorter *h_sorters = NULL, temp_sorter, *p_sort = NULL;

	/*
	 * Shortcuts
	 */
	n_snaps = moria_cfg->n_snaps;
	snap_root = n_snaps + 1;

	/*
	 * Create sorter so we can look up tree elements by ID.
	 */
	h_sorters = (MoriaSorter*) memAlloc(__FFL__, MID_MORIA_TREE, sizeof(MoriaSorter) * hs->n_tot);
	for (i = 0; i < hs->n_tot; i++)
	{
		h_sorters[i].id = hs->hd[i].id;
		h_sorters[i].idx = i;
		debugHaloMoria(hs->hd[i].id, "Added ID in sorter element %d.", i);
	}
	qsort(h_sorters, hs->n_tot, sizeof(MoriaSorter), &compareIndexSorters);

	/*
	 * Check that all halos that are not mmp by the end of the simulation have been given merge
	 * targets. We will only consider mmp halos in this function.
	 */
#if MORIA_CAREFUL
	for (i = 0; i < hs->n_tot; i++)
	{
		hd = &(hs->hd[i]);
		t_idx = ts->halo_tree_idxs[i];

		if ((t_idx < 0) || (t_idx >= ts->n))
			error(__FFL__, "Invalid tree idx, %d (max %d), halo index %d, halo ID %ld.\n", t_idx,
					ts->n, i, hd->id);

		th = &(ts->th[t_idx]);

		if (!hd->mmp && (th->merge_idx == INVALID_IDX))
		{
			error(__FFL__, "Found halo %ld which is not mmp, but has invalid merge index.\n",
					hd->id);
		}

		if (hd->mmp && (hd->cat_upid == INVALID_ID) && (th->merge_idx != INVALID_IDX))
		{
			error(__FFL__, "Found host mmp halo %ld at final snapshot, but merge idx is %d.\n",
					hd->id, th->merge_idx);
		}
	}
#endif

	/*
	 * Part 1: We iterate to find merge targets for subhalos at the final snapshot. At this point,
	 * we do not worry about whether the merge targets are actually being output or not.
	 */
	iter = 0;
	while (1)
	{
		n_deferred = 0;
		for (i = 0; i < hs->n_tot; i++)
		{
			hd = &(hs->hd[i]);
			th = &(ts->th[ts->halo_tree_idxs[i]]);

			/*
			 * We only consider mmp halos that are subhalos. If we have already set this index, no
			 * need to repeat it.
			 */
			if (th->merge_idx != INVALID_IDX)
				continue;
			if (!hd->mmp)
				continue;
			if (hd->cat_upid == INVALID_ID)
				continue;

			debugHaloMoria(hd->id,
					"Considering as subhalo for final merge update, iteration %d, mmp %d, merge index %d; searching parent ID %ld.",
					iter, hd->mmp, th->merge_idx, hd->cat_upid);

			/*
			 * Let's look for the host halo
			 */
			temp_sorter.id = hd->cat_upid;
			p_sort = (MoriaSorter*) bsearch(&temp_sorter, h_sorters, hs->n_tot, sizeof(MoriaSorter),
					&compareIndexSorters);
			if (p_sort == NULL)
				error(__FFL__, "Could not find host halo ID %ld.\n", hd->cat_upid);

			h_idx = p_sort->idx;
			t_idx = ts->halo_tree_idxs[h_idx];
			hd_merge = &(hs->hd[h_idx]);
			th_merge = &(ts->th[t_idx]);

			/*
			 * Check for the case that means we have to iterate: if the merge target is a
			 * subhalo and has not yet been sorted out.
			 */
			if ((hd_merge->cat_upid != INVALID_ID) && (th_merge->merge_idx == INVALID_IDX))
			{
				debugHaloMoria(hd->id,
						"Cannot set the merge idx in iteration %d because parent halo %ld is a subhalo without a merge index itself.",
						iter, hd->cat_upid);
				n_deferred++;
				continue;
			}

			if (th_merge->merge_idx == INVALID_IDX)
			{
				/*
				 * Case 1: the merge target is a root, we set the target for the subhalo to
				 * the root and the snap to n_snaps.
				 */
				th->merge_idx = t_idx;
				th->merge_snap = n_snaps;

				debugHaloMoria(th->id,
						"Parent is root, setting merge idx to parent idx %d, merge snap %d, merge do_output %d.",
						th->merge_idx, th->merge_snap, th_merge->do_output);
			} else
			{
#if MORIA_CAREFUL
				/*
				 * Case 2: the merge target is not a root. So we set our target to this halo's target.
				 *
				 * If we are to merge into this halo, it cannot have merged at a previous snapshot.
				 */
				if (th_merge->merge_snap < n_snaps - 1)
					error(__FFL__,
							"At last snapshot, found halo %ld merging into %ld, but that has already merged at snapshot %d.\n",
							th->id, th_merge->id, th_merge->merge_snap);

				if (th_merge->merge_idx == i)
					error(__FFL__,
							"Found merge halo idx %d, its merge idx %d, same as halo idx (ID %ld).\n",
							th->merge_idx, th_merge->merge_idx, th->id);
#endif
				th->merge_idx = th_merge->merge_idx;
				th->merge_snap = th_merge->merge_snap;

				debugHaloMoria(th->id,
						"Parent is not root, setting merge idx to parent's merge idx %d, merge snap %d.",
						th->merge_idx, th->merge_snap);
				debugHaloMoria(th_merge->id,
						"Added this halo as merge target for halo ID %ld since its parent is not root. Setting merge idx to parent's merge idx %d, merge snap %d.",
						th->id, th->merge_idx, th->merge_snap);
			}
		}

		iter++;
		if (n_deferred == 0)
			break;
		if (iter > 10)
			error(__FFL__, "Could not assign all merge indices after 10 iterations.\n");
	}

	output(0, "Took %d iterations to assign merge indices.\n", iter);

	/*
	 * Set a merge_snap beyond the simulation for root halos
	 */
	for (i = 0; i < ts->n; i++)
	{
		th = &(ts->th[i]);
		if (th->merge_idx == INVALID_IDX)
		{
#if MORIA_CAREFUL
			if (th->merge_snap != INVALID_IDX)
				error(__FFL__,
						"Found tree element with no merge index but merge snapshot (ID %ld).\n",
						th->id);
#endif
			th->merge_snap = snap_root;
			debugHaloMoria(th->id, "Root halo, setting merge snap to %d.", th->merge_snap);
		}
	}

	/*
	 * Test that all tree elements have been given a merge_idx or that they are tree roots,
	 * that is, hosts that are alive.
	 */
#if MORIA_CAREFUL
	for (i = 0; i < hs->n_tot; i++)
	{
		hd = &(hs->hd[i]);
		th = &(ts->th[ts->halo_tree_idxs[i]]);

		if (th->merge_order_q < 0.0)
		{
			error(__FFL__, "Found invalid order quantity %.2e in tree element with ID %ld.\n",
					th->merge_order_q, th->id);
		}
		if (th->merge_idx == INVALID_IDX)
		{
			if ((hd->cat_upid != INVALID_ID) || (!th->is_alive))
				error(__FFL__,
						"Found root tree element for halo %ld, but halo has upid %ld, alive %d.\n",
						hd->id, hd->cat_upid, th->is_alive);
		} else
		{
			th_merge = &(ts->th[th->merge_idx]);

			/*
			 * Halos cannot have a merge snapshot greater than the halo they are merging into,
			 * that makes no sense.
			 */
			if (th_merge->merge_snap <= th->merge_snap)
				error(__FFL__,
						"Found merge snap %d in target halo %ld (for halo %ld, merge snap %d).\n",
						th_merge->merge_snap, th_merge->id, th->id, th->merge_snap);

		}
	}
#endif

	/*
	 * Part 2: Fix cases where the merge target is not being output. This would lead to an invalid
	 * index in the output files, and to an undefined sorting order.
	 */
	for (i = 0; i < ts->n; i++)
	{
		th = &(ts->th[i]);
		if (th->merge_idx == INVALID_IDX)
			continue;

		t_idx = th->merge_idx;
		th_merge = &(ts->th[t_idx]);
		iter = 0;
		while (!th_merge->do_output)
		{
			debugHaloMoria(th->id,
					"Merge target idx %d is not output, updating merge idx (iteration %d).", t_idx,
					iter);
			if (th_merge->merge_idx != INVALID_IDX)
			{
				t_idx = th_merge->merge_idx;
				th_merge = &(ts->th[t_idx]);
			} else
			{
				t_idx = INVALID_IDX;
				th_merge = NULL;
			}
			iter++;
			if (t_idx == INVALID_IDX)
				break;
		}

		/*
		 * Iter 0 means the above loop was never executed, meaning that the first merge target is
		 * being output; nothing to fix here.
		 */
		if (iter == 0)
			continue;

		if (t_idx == INVALID_IDX)
		{
			th->merge_idx = INVALID_IDX;
			th->merge_snap = snap_root;
		} else
		{
			th->merge_idx = t_idx;
#if MORIA_CAREFUL
			if (th_merge->merge_snap <= th->merge_snap)
				error(__FFL__,
						"Found merge snap %d in target halo %ld (for halo %ld, merge snap %d).\n",
						th_merge->merge_snap, th_merge->id, th->id, th->merge_snap);
#endif
		}

		debugHaloMoria(th->id, "Updated to merge idx %d, merge snap %d.", th->merge_idx,
				th->merge_snap);
	}

	memFree(__FFL__, MID_MORIA_TREE, h_sorters, sizeof(MoriaSorter) * hs->n_tot);
}

/*
 * Returns -1 if a goes before b, 0 if there is no preference, and 1 if b goes before a. The
 * decision priority is:
 *
 * root element:    if one of the elements is a root (merge_idx = INVALID_IDX) and the other one
 *                  is not, the root gets preference.
 * merge time:      if both merged, the later one gets preference
 * order quantity:  if still undecided, use the order quantity in descending order (larger first)
 */
int compareTreeElements(const void *a, const void *b)
{
	const TreeComparisonElement *da = (const TreeComparisonElement*) a;
	const TreeComparisonElement *db = (const TreeComparisonElement*) b;

	if ((da->merge_snap == INVALID_IDX) && (db->merge_snap != INVALID_IDX))
		return -1;
	else if ((da->merge_snap != INVALID_IDX) && (db->merge_snap == INVALID_IDX))
		return 1;
	else if (da->merge_snap != db->merge_snap)
		return (da->merge_snap < db->merge_snap) - (da->merge_snap > db->merge_snap);
	else
		return (da->order_q < db->order_q) - (da->order_q > db->order_q);
}

void insertRootElement(LinkedListElement *lle, LinkedListElement *lle_above)
{
	lle_above->dn = lle;
	lle->up = lle_above;
	lle->root = lle;
	lle->tail = lle;
}

void insertElement(LinkedListElement *lle, LinkedListElement *lle_above)
{
	LinkedListElement *tmp;

	tmp = lle_above->dn;
	lle_above->dn = lle;
	lle->dn = tmp;
	lle->up = lle_above;
	lle->root = lle_above->root;
	lle->tail = NULL;
	lle->root->tail = lle;
}

void sortTree(MoriaConfig *moria_cfg, TreeSet *ts)
{
	int i, j, n_out, n_snaps, snap_root, tree_idx, *new_idxs = NULL, merge_idx;
	TreeComparisonElement *comp = NULL;
	LinkedListElement *list = NULL, *lle = NULL, *lle_above = NULL, *lle_merge = NULL, head, tail;
	TreeHalo *th = NULL;

	/*
	 * Reset all indices, count total number of tree halos that we are outputting.
	 * Also create an index array that maps the old indices into the new.
	 */
	n_out = 0;
	n_snaps = moria_cfg->n_snaps;
	snap_root = n_snaps + 1;
	new_idxs = (int*) memAlloc(__FFL__, MID_MORIA_TREE, sizeof(int) * ts->n);
	for (i = 0; i < ts->n; i++)
	{
		th = &(ts->th[i]);
		if (th->do_output)
		{
			new_idxs[i] = n_out;
			n_out++;
		} else
		{
			new_idxs[i] = INVALID_IDX;
		}
	}

	/*
	 * Create a linked list for all relevant tree elements, set the me pointer and reset the up/dn
	 * pointers. Also create a list of the merger conditions for each halo, which can be sorted.
	 */
	comp = (TreeComparisonElement*) memAlloc(__FFL__, MID_MORIA_TREE,
			sizeof(TreeComparisonElement) * n_out);
	list = (LinkedListElement*) memAlloc(__FFL__, MID_MORIA_TREE,
			sizeof(LinkedListElement) * n_out);

	j = 0;
	for (i = 0; i < ts->n; i++)
	{
		th = &(ts->th[i]);
		if (th->do_output)
		{
			list[j].me = th;
			list[j].up = NULL;
			list[j].dn = NULL;
			list[j].root = NULL;
			list[j].tail = NULL;

			comp[j].lle = &(list[j]);
			comp[j].order_q = th->merge_order_q;
			comp[j].merge_snap = th->merge_snap;

			debugHaloMoria(th->id,
					"Created tree sorter element, merge snap %d, order q %.2e, sparta_idx %d.",
					comp[j].order_q, comp[j].merge_snap, th->sparta_idx);

			j++;
		}
	}
#if MORIA_CAREFUL
	if (j != n_out)
		error(__FFL__, "Count mismatch.\n");
#endif

	qsort(comp, n_out, sizeof(TreeComparisonElement), &compareTreeElements);

	/*
	 * Start a linked list with just head and tail
	 */
	head.me = NULL;
	head.up = NULL;
	head.dn = &tail;
	head.root = NULL;
	head.tail = &head;

	tail.me = NULL;
	tail.up = &head;
	tail.dn = NULL;
	tail.root = NULL;
	tail.tail = &tail;

	/*
	 * Now start inserting elements, beginning with the root halos (merge index -1)
	 */
	i = 0;
	while (comp[i].merge_snap == snap_root)
	{
		lle = comp[i].lle;
		if (i == 0)
			lle_above = &head;
		else
			lle_above = comp[i - 1].lle;
		insertRootElement(lle, lle_above);

		debugHaloMoria(lle->me->id, "Inserted as root element at position %d.", i);

		i++;
	}

	/*
	 * Now continue with halos that do merge into another halo, i.e., merge index > -1
	 */
	while (i < n_out)
	{
		lle = comp[i].lle;

#if MORIA_CAREFUL
		if (lle->me == NULL)
			error(__FFL__, "Me element not set.\n");
#endif

		merge_idx = lle->me->merge_idx;

#if MORIA_CAREFUL
		if ((merge_idx < 0) || (merge_idx >= ts->n))
			error(__FFL__, "Invalid merge idx, %d.\n", merge_idx);
#endif

		/*
		 * Convert to an index into the array of halos that are being output.
		 */
		merge_idx = new_idxs[merge_idx];

#if MORIA_CAREFUL
		if (merge_idx == INVALID_IDX)
			error(__FFL__,
					"Trying to access tree element that is not being written out (looking for merge idx %d, found in tree ID %ld).\n",
					lle->me->merge_idx, lle->me->id);
		if (merge_idx >= n_out)
			error(__FFL__, "Invalid merge idx %d, must be between 0 and %d.\n", merge_idx, n_out);
#endif

		lle_merge = &(list[merge_idx]);

#if MORIA_CAREFUL
		if (lle_merge->root == NULL)
		{
			int my_idx, my_idx_new;

			my_idx = (lle->me - ts->th);
			my_idx_new = new_idxs[my_idx];

			error(__FFL__,
					"Found NULL root for merge target idx %d (all) / %d (output); probably has not been inserted yet (idx %d (all), %d (output), merge snap %d, order q %.2e, ID %ld, target merge snap %d, target ID %ld, target order q %.2e).\n",
					lle->me->merge_idx, merge_idx, my_idx, my_idx_new, lle->me->merge_snap,
					lle->me->merge_order_q, lle->me->id, lle_merge->me->merge_snap,
					lle_merge->me->merge_order_q, lle_merge->me->id);
		}
#endif

		/*
		 * Add element at the tail of the tree into which it merges, which is stored in the root
		 * element.
		 */
		lle_above = lle_merge->root->tail;

#if MORIA_CAREFUL
		if (lle_above == NULL)
			error(__FFL__, "Found NULL tail.\n");
#endif

		insertElement(lle, lle_above);

		debugHaloMoria(lle->me->id,
				"Inserted as non-root element (index %d, merge_idx %d, merge ID %8ld, sparta_idx %d).",
				i, merge_idx, lle_merge->me->id, lle->me->sparta_idx);

		i++;
	}

	/*
	 * Walk through list and count up the indices
	 */
	ts->tree_out_idxs = (int*) memAlloc(__FFL__, MID_MORIA_TREE, sizeof(int) * ts->n);
	for (i = 0; i < ts->n; i++)
		ts->tree_out_idxs[i] = INVALID_IDX;
	lle = &head;
	i = 0;
	while (lle->dn != NULL)
	{
		lle = lle->dn;
#if MORIA_CAREFUL
		if (lle->me == NULL)
			error(__FFL__, "Found null me element.\n");
#endif
		tree_idx = (lle->me - ts->th);
#if MORIA_CAREFUL
		if ((tree_idx < 0) || (tree_idx >= ts->n))
			error(__FFL__, "Invalid tree idx %d, must be between 0 and %d.\n", tree_idx, ts->n);
		if (ts->tree_out_idxs[tree_idx] != INVALID_IDX)
			error(__FFL__,
					"Trying to set output index %d for tree idx %d, but already set to %d.\n", i,
					tree_idx, ts->tree_out_idxs[tree_idx]);
#endif
		ts->tree_out_idxs[tree_idx] = i;

		i++;
	}

#if MORIA_CAREFUL
	if (i != n_out)
		error(__FFL__, "Expected to set %d output indices, but only set %d.\n", n_out, i);
#endif

	/*
	 * The merge_idxs still point to the old tree indices before sorting; we need to now adjust
	 * them to the new indices.
	 *
	 * Note that the merge index is allowed to be -1, so we need to just keep it -1 in such cases.
	 */
	for (i = 0; i < ts->n; i++)
	{
		th = &(ts->th[i]);

		if (!th->do_output)
			continue;
		if (th->merge_idx == INVALID_IDX)
			continue;

		th->merge_idx = ts->tree_out_idxs[th->merge_idx];

#if MORIA_CAREFUL
		if (ts->tree_out_idxs[i] == INVALID_IDX)
			error(__FFL__, "Output index not set for idx %d.\n", i);
		if (th->merge_idx == INVALID_IDX)
			error(__FFL__, "Found invalid final merge index for halo %ld.\n", th->id);
#endif
	}

	memFree(__FFL__, MID_MORIA_TREE, new_idxs, sizeof(int) * ts->n);
	memFree(__FFL__, MID_MORIA_TREE, list, sizeof(LinkedListElement) * n_out);
	memFree(__FFL__, MID_MORIA_TREE, comp, sizeof(TreeComparisonElement) * n_out);

	ts->n_out = n_out;
}
