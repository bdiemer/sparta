/*************************************************************************************************
 *
 * Global constants, structures, and routines.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>

#include "moria_global.h"
#include "moria_config.h"
#include "../../src/memory.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

#define ENUM(x, y) #y,
const char *MORIA_UNIT_NAMES[MORIA_N_UNITS] =
	{MORIA_UNITS};
#undef ENUM

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void printDebugHaloMoria(HaloID id, const char *format, ...)
{
	if (id != MORIA_DEBUG_HALO)
		return;

	char msg[400];

	va_list args;
	va_start(args, format);
	vsprintf(msg, format, args);
	va_end(args);

	output(0, "*** Halo %ld: %s\n", MORIA_DEBUG_HALO, msg);
}

int compareIndexSorters(const void *a, const void *b)
{
	if (((MoriaSorter*) a)->id > ((MoriaSorter*) b)->id)
		return 1;
	else if (((MoriaSorter*) a)->id < ((MoriaSorter*) b)->id)
		return -1;
	else
		return 0;
}

int unitsFromString(char *str)
{
	int i, result;

	result = -1;

	for (i = 0; i < MORIA_N_UNITS; i++)
	{
		if (strcmp(str, MORIA_UNIT_NAMES[i]) == 0)
		{
			result = i;
			break;
		}
	}
	if (result == -1)
	{
		for (i = 0; i < MORIA_N_UNITS; i++)
			output(0, "%s\n", MORIA_UNIT_NAMES[i]);
		error(__FFL__, "Invalid unit identifier, %s. Valid unit descriptors are listed above.\n",
				str);
	}

	return result;
}

void unitsToString(int u, char *str)
{
	sprintf(str, "%s", MORIA_UNIT_NAMES[u]);
}

int isSpartaRspDefinition(HaloDefinition *def)
{
	return ((def->type == HDEF_TYPE_SPLASHBACK) && (def->source == HDEF_SOURCE_SPARTA));
}

int isSpartaHpsDefinition(HaloDefinition *def)
{
	return (((def->type == HDEF_TYPE_SO) || (def->type == HDEF_TYPE_ORBITING)) && (def->source == HDEF_SOURCE_SPARTA));
}

int defHasStatus(HaloDefinition *def)
{
	return isSpartaHpsDefinition(def);
}
