/*************************************************************************************************
 *
 * This unit merges the catalog data with SPARTA HaloProps analysis data.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_MERGE_HPS_H_
#define _MORIA_MERGE_HPS_H_

#include "moria_global.h"
#include "moria_config.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void mergeAnalysisHaloProps(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		int halo_idx);

#endif
