/*************************************************************************************************
 *
 * I/O routines for moria.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_IO_SPARTA_H_
#define _MORIA_IO_SPARTA_H_

#include "moria_global.h"
#include "moria_config.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

SpartaData *readSpartaConfig(MoriaConfig *moria_cfg);
void readSpartaData(MoriaConfig *moria_cfg, SpartaData *sd);
void freeSpartaDataMain(MoriaConfig *moria_cfg, SpartaData *sd);
void freeSpartaDataConfig(MoriaConfig *moria_cfg, SpartaData *sd);

#endif
