/*************************************************************************************************
 *
 * This unit determines subhalo relations given a set of halo positions and radii.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_HOSTSUB_H_
#define _MORIA_HOSTSUB_H_

#include "moria_global.h"
#include "moria_config.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void computeSubhaloRelations(MoriaConfig *moria_cfg, SnapshotData *snap, HaloSet *hs);

#endif
