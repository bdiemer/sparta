/*************************************************************************************************
 *
 * The moria_cfg routines for moria.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "moria_global.h"
#include "moria_config.h"
#include "../models/models_rsp.h"
#include "../../src/config.h"
#include "../../src/memory.h"
#include "../../src/utils.h"
#include "../../src/global_types.h"
#include "../../src/halos/halo_definitions.h"
#include "moria_halo.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

#define CONFIG_LINE_LENGTH 2000
const char *OUTDIR_RESTART = "restart";
const char *OUTDIR_TREETMP = "tree_tmp";

const char *MORIA_DEFAULT_OUTPUT_DIR_ORIGINAL = "moria_original";
const char *MORIA_DEFAULT_OUTPUT_DIR_HDF5 = "moria_catalogs";
const char *MORIA_DEFAULT_OUTPUT_DIR_TMP = ".";
const char *MORIA_DEFAULT_OUTPUT_FILENAME_TREE = "moria_tree.hdf5";

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void printMoriaConfigList(const char *param, char *list, int n, int list_str_len);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void setMoriaConfigDefaults(MoriaConfig *moria_cfg)
{
	// Input
	sprintf(moria_cfg->sparta_file, "%s", "");
	sprintf(moria_cfg->catalog_dir, "%s", "");
	initHaloDefinition(&(moria_cfg->main_def_catalog));
	moria_cfg->main_def_catalog.quantity = MORIA_DEFAULT_MAIN_DEF_CAT_QUANTITY;
	moria_cfg->main_def_catalog.type = MORIA_DEFAULT_MAIN_DEF_CAT_TYPE;
	moria_cfg->main_def_catalog.subtype = MORIA_DEFAULT_MAIN_DEF_CAT_SUBTYPE;
	moria_cfg->main_def_catalog.overdensity = MORIA_DEFAULT_MAIN_DEF_CAT_OVERDENSITY;
	moria_cfg->main_def_catalog.ptl_select = MORIA_DEFAULT_MAIN_DEF_CAT_PTLSEL;
	moria_cfg->rockstar_strict_so = -1;
	moria_cfg->rockstar_bound_props = -1;
	moria_cfg->cosmo = getCosmology(COSMOLOGY_NONE);
	moria_cfg->cosmo.flat = MORIA_DEFAULT_COSMO_FLAT;
	moria_cfg->cosmo.Omega_r = MORIA_DEFAULT_COSMO_OMEGA_R;
	moria_cfg->cosmo.w = MORIA_DEFAULT_COSMO_W;
	moria_cfg->cosmo.T_CMB = MORIA_DEFAULT_COSMO_T_CMB;
	moria_cfg->cosmo.Omega_b = -1.0;
	moria_cfg->cosmo.sigma8 = -1.0;
	moria_cfg->cosmo.ns = -1.0;
	moria_cfg->cosmo.ps_type = MORIA_DEFAULT_COSMO_PS_TYPE;

	// Log
	moria_cfg->log_level = MORIA_DEFAULT_LOG_LEVEL;
	moria_cfg->log_level_memory = MORIA_DEFAULT_LOG_LEVEL_MEMORY;
	moria_cfg->log_level_timing = MORIA_DEFAULT_LOG_LEVEL_TIMING;
	moria_cfg->log_flush = MORIA_DEFAULT_LOG_FLUSH;
	moria_cfg->log_substatus_diff = MORIA_DEFAULT_LOG_SUBSTATUS_DIFF;

	// Error levels
	moria_cfg->err_level_more_snaps_than_cat = MORIA_DEFAULT_ERR_LEVEL_MORE_SNAPS_THAN_CAT;
	moria_cfg->err_level_snap_outside_range = MORIA_DEFAULT_ERR_LEVEL_SNAP_OUTSIDE_RANGE;
	moria_cfg->err_level_main_def_not_200m = MORIA_DEFAULT_ERR_LEVEL_MAIN_DEF_NOT_200M;
	moria_cfg->err_level_acc_rate_undefined = MORIA_DEFAULT_ERR_LEVEL_ACC_RATE_UNDEFINED;
	moria_cfg->err_level_cat_field_changed = MORIA_DEFAULT_ERR_LEVEL_CAT_FIELD_CHANGED;

	// Algorithm
	initHaloDefinition(&(moria_cfg->cut_def));
	moria_cfg->cut_def.quantity = MORIA_DEFAULT_CUT_DEF_QUANTITY;
	moria_cfg->cut_def.type = MORIA_DEFAULT_CUT_DEF_TYPE;
	moria_cfg->cut_def.time = MORIA_DEFUALT_CUT_DEF_TIME;
	moria_cfg->cut_threshold_in_units = MORIA_DEFAULT_CUT_THRESHOLD;
	moria_cfg->cut_units = MORIA_DEFAULT_CUT_UNITS;
	initHaloDefinition(&(moria_cfg->order_def));
	moria_cfg->order_def.quantity = MORIA_DEFAULT_ORDER_DEF_QUANTITY;
	moria_cfg->order_def.type = MORIA_DEFAULT_ORDER_DEF_TYPE;
	moria_cfg->hostsub_assign_subparent_id = MORIA_DEFAULT_HOSTSUB_ASSIGN_SUBPARENT_ID;
	moria_cfg->hostsub_restrict_to_output = MORIA_DEFAULT_HOSTSUB_RESTRICT_TO_OUTPUT;
	moria_cfg->include_ghosts = MORIA_DEFAULT_INCLUDE_GHOSTS;
	moria_cfg->adjust_phantom_xv = MORIA_DEFAULT_ADJUST_PHANTOM_XV;
	moria_cfg->max_mass_ratio_sparta = MORIA_DEFAULT_MAX_MASS_RATIO_SPARTA;
	moria_cfg->max_rsp_model_diff_factor = MORIA_DEFAULT_MAX_RSP_MODEL_DIFF_FACTOR;
	moria_cfg->max_rsp_model_diff_sigma = MORIA_DEFAULT_MAX_RSP_MODEL_DIFF_SIGMA;

	// Output
	sprintf(moria_cfg->output_dir_original, "%s", MORIA_DEFAULT_OUTPUT_DIR_ORIGINAL);
	sprintf(moria_cfg->output_dir_hdf5, "%s", MORIA_DEFAULT_OUTPUT_DIR_HDF5);
	sprintf(moria_cfg->output_dir_tmp, "%s", MORIA_DEFAULT_OUTPUT_DIR_TMP);
	sprintf(moria_cfg->output_filename_tree, "%s", MORIA_DEFAULT_OUTPUT_FILENAME_TREE);
	moria_cfg->output_restart_files = MORIA_DEFAULT_OUTPUT_RESTART_FILES;
	moria_cfg->output_restart_every = MORIA_DEFAULT_OUTPUT_RESTART_EVERY;
	moria_cfg->output_original = MORIA_DEFAULT_OUTPUT_ORIGINAL;
	moria_cfg->output_hdf5 = MORIA_DEFAULT_OUTPUT_HDF5;
	moria_cfg->output_tree = MORIA_DEFAULT_OUTPUT_TREE;
	moria_cfg->output_compression_level = -1;
	moria_cfg->n_a = 1;
	moria_cfg->output_a[0] = -1;
	moria_cfg->n_cat_fields = 0;
	moria_cfg->n_defs_rm = 0;

	// Changeable variables
	moria_cfg->warnings_field_name_done = 0;
}

void readMoriaConfigFile(char *filename, MoriaConfig *moria_cfg)
{
	int tmp_int;
	char line[CONFIG_LINE_LENGTH], *key, *value;
	FILE *f;

	f = fopen(filename, "r");
	if (f == NULL)
		error(__FFL__, "moria_cfg file %s could not be opened.\n", filename);

	while (fgets(line, CONFIG_LINE_LENGTH, f) != NULL)
	{
		if (lineToKeyValue(line, &key, &value) > 0)
			continue;

		/*
		 * Input
		 */
		if (strcmp(key, "sparta_file") == 0)
		{
			strcpy(moria_cfg->sparta_file, value);
		} else if (strcmp(key, "catalog_dir") == 0)
		{
			strcpy(moria_cfg->catalog_dir, value);
		} else if (strcmp(key, "main_def_catalog") == 0)
		{
			moria_cfg->main_def_catalog = stringToHaloDefinition(value, 0);
		} else if (strcmp(key, "rockstar_strict_so") == 0)
		{
			moria_cfg->rockstar_strict_so = atoi(value);
		} else if (strcmp(key, "rockstar_bound_props") == 0)
		{
			moria_cfg->rockstar_bound_props = atoi(value);
		} else if (strcmp(key, "cosmo_omega_r") == 0)
		{
			moria_cfg->cosmo.Omega_r = atof(value);
		} else if (strcmp(key, "cosmo_w") == 0)
		{
			moria_cfg->cosmo.w = atof(value);
		} else if (strcmp(key, "cosmo_t_cmb") == 0)
		{
			moria_cfg->cosmo.T_CMB = atof(value);
		} else if (strcmp(key, "cosmo_omega_b") == 0)
		{
			moria_cfg->cosmo.Omega_b = atof(value);
		} else if (strcmp(key, "cosmo_sigma8") == 0)
		{
			moria_cfg->cosmo.sigma8 = atof(value);
		} else if (strcmp(key, "cosmo_ns") == 0)
		{
			moria_cfg->cosmo.ns = atof(value);
		} else if (strcmp(key, "cosmo_flat") == 0)
		{
			moria_cfg->cosmo.flat = atoi(value);
		} else if (strcmp(key, "cosmo_self_similar") == 0)
		{
			tmp_int = atoi(value);
			if (tmp_int)
				moria_cfg->cosmo.ps_type = POWERSPECTRUM_TYPE_POWERLAW;
			else
				moria_cfg->cosmo.ps_type = MORIA_DEFAULT_COSMO_PS_TYPE;
		}
		/*
		 * Console output
		 */
		else if (strcmp(key, "log_level") == 0)
		{
			moria_cfg->log_level = atoi(value);
		} else if (strcmp(key, "log_level_memory") == 0)
		{
			moria_cfg->log_level_memory = atoi(value);
		} else if (strcmp(key, "log_level_timing") == 0)
		{
			moria_cfg->log_level_timing = atoi(value);
		} else if (strcmp(key, "log_flush") == 0)
		{
			moria_cfg->log_flush = atoi(value);
		} else if (strcmp(key, "log_substatus_diff") == 0)
		{
			moria_cfg->log_substatus_diff = atoi(value);
		}
		/*
		 * Error levels
		 */
		else if (strcmp(key, "err_level_more_snaps_than_cat") == 0)
		{
			moria_cfg->err_level_more_snaps_than_cat = atoi(value);
		} else if (strcmp(key, "err_level_snap_outside_range") == 0)
		{
			moria_cfg->err_level_snap_outside_range = atoi(value);
		} else if (strcmp(key, "err_level_main_def_not_200m") == 0)
		{
			moria_cfg->err_level_main_def_not_200m = atoi(value);
		} else if (strcmp(key, "err_level_acc_rate_undefined") == 0)
		{
			moria_cfg->err_level_acc_rate_undefined = atoi(value);
		} else if (strcmp(key, "err_level_cat_field_changed") == 0)
		{
			moria_cfg->err_level_cat_field_changed = atoi(value);
		}
		/*
		 * Algorithm
		 */
		else if (strcmp(key, "cut_def") == 0)
		{
			moria_cfg->cut_def = stringToHaloDefinition(value, 0);
		} else if (strcmp(key, "cut_threshold") == 0)
		{
			moria_cfg->cut_threshold_in_units = atof(value);
		} else if (strcmp(key, "cut_units") == 0)
		{
			moria_cfg->cut_units = unitsFromString(value);
		} else if (strcmp(key, "order_def") == 0)
		{
			moria_cfg->order_def = stringToHaloDefinition(value, 0);
		} else if (strcmp(key, "hostsub_assign_subparent_id") == 0)
		{
			moria_cfg->hostsub_assign_subparent_id = atoi(value);
		} else if (strcmp(key, "hostsub_restrict_to_output") == 0)
		{
			moria_cfg->hostsub_restrict_to_output = atoi(value);
		} else if (strcmp(key, "include_ghosts") == 0)
		{
			moria_cfg->include_ghosts = atoi(value);
		} else if (strcmp(key, "adjust_phantom_xv") == 0)
		{
			moria_cfg->adjust_phantom_xv = atoi(value);
		} else if (strcmp(key, "max_mass_ratio_sparta") == 0)
		{
			moria_cfg->max_mass_ratio_sparta = atof(value);
		} else if (strcmp(key, "max_rsp_model_diff_factor") == 0)
		{
			moria_cfg->max_rsp_model_diff_factor = atof(value);
		} else if (strcmp(key, "max_rsp_model_diff_sigma") == 0)
		{
			moria_cfg->max_rsp_model_diff_sigma = atof(value);
		}
		/*
		 * Output
		 */
		else if (strcmp(key, "output_dir_original") == 0)
		{
			strcpy(moria_cfg->output_dir_original, value);
		} else if (strcmp(key, "output_dir_hdf5") == 0)
		{
			strcpy(moria_cfg->output_dir_hdf5, value);
		} else if (strcmp(key, "output_dir_tmp") == 0)
		{
			strcpy(moria_cfg->output_dir_tmp, value);
		} else if (strcmp(key, "output_filename_tree") == 0)
		{
			strcpy(moria_cfg->output_filename_tree, value);
		} else if (strcmp(key, "output_restart_files") == 0)
		{
			moria_cfg->output_restart_files = atoi(value);
		} else if (strcmp(key, "output_restart_every") == 0)
		{
			moria_cfg->output_restart_every = atoi(value);
		} else if (strcmp(key, "output_original") == 0)
		{
			moria_cfg->output_original = atoi(value);
		} else if (strcmp(key, "output_hdf5") == 0)
		{
			moria_cfg->output_hdf5 = atoi(value);
		} else if (strcmp(key, "output_tree") == 0)
		{
			moria_cfg->output_tree = atoi(value);
		} else if (strcmp(key, "output_compression_level") == 0)
		{
			moria_cfg->output_compression_level = atoi(value);
		} else if (strcmp(key, "output_a") == 0)
		{
			moria_cfg->n_a = stringToListFloat(value, moria_cfg->output_a, MORIA_MAX_N_A, 0);
		} else if (strcmp(key, "output_cat_fields") == 0)
		{
			moria_cfg->n_cat_fields = stringToListString(value,
					(char*) moria_cfg->output_cat_fields[0],
					MORIA_MAX_CAT_FIELDS, 0, CAT_FIELD_LENGTH);
		} else if (strcmp(key, "output_rm_defs") == 0)
		{
			moria_cfg->n_defs_rm = stringToListString(value,
					(char*) moria_cfg->output_rm_defs_str[0], MORIA_MAX_DEFS, 0,
					DEFAULT_HALO_DEF_STRLEN);
		} else if (strcmp(key, "output_sub_defs") == 0)
		{
			moria_cfg->n_defs_sub = stringToListString(value,
					(char*) moria_cfg->output_sub_defs_str[0], MORIA_MAX_DEFS, 0,
					DEFAULT_HALO_DEF_STRLEN);
		} else
		{
			error(__FFL__, "Key '%s' is not a valid moria_cfg parameter.\n", key);
		}
	}

	fclose(f);
}

/*
 * Some parameters are formally stored in the moria config, but need to be transferred to the
 * global SPARTA config. This needs to be done, for example, before any memory is reserved.
 *
 * Similarly, we need to write back a few things loaded from the Sparta file.
 */
void setSpartaConfig(MoriaConfig *moria_cfg)
{
	config.log_level = moria_cfg->log_level;
	config.log_level_memory = moria_cfg->log_level_memory;
	config.log_level_timing = moria_cfg->log_level_timing;
	config.log_flush = moria_cfg->log_flush;

	config.box_size = moria_cfg->box_size;
	config.cat_type = moria_cfg->cat_type;
	if (moria_cfg->output_compression_level < 0)
		moria_cfg->output_compression_level = moria_cfg->sparta_output_compression_level;
	config.output_compression_level = moria_cfg->output_compression_level;
}

/*
 * This function converts the input strings for halo definitions into structures and compiles a
 * "master list" of quantities that need to be loaded. We also make sure that the source of all
 * quantities is defined: for example, SO definitions can be loaded from SPARTA or from the
 * catalog -- the user must specify which to avoid confusion.
 */
void processMoriaConfig(MoriaConfig *moria_cfg, SpartaData *sd)
{
	int i, j, k, found;
	int_least8_t a_remove_mask[MORIA_MAX_N_A];
	char tmp_str[DEFAULT_HALO_DEF_STRLEN];
	HaloDefinition def_200m, def_vir;

	/*
	 * Routines relevant for restart
	 */
	processMoriaConfigRestart(moria_cfg);

	/*
	 * Check input options
	 */
	if (moria_cfg->cat_type == CAT_TYPE_ROCKSTAR)
	{
		if (moria_cfg->rockstar_strict_so < 0)
			error(__FFL__,
					"Config parameter rockstar_strict_so must be set for Rockstar catalog input.\n");
		if (moria_cfg->rockstar_bound_props < 0)
			error(__FFL__,
					"Config parameter rockstar_bound_props must be set for Rockstar catalog input.\n");
	}

	/*
	 * Check that cosmology is set. The value of ns depends on the power spectrum type: in LCDM
	 * cosmologies, it must be a positive number. In power-law cosmologies, it must be between
	 * -3 and 0. No other types of power spectrum are currently allowed.
	 */
	if (moria_cfg->cosmo.Omega_b < 0.0)
		error(__FFL__, "moria_cfg: Parameter cosmo_omega_b must be set.\n");
	if (moria_cfg->cosmo.sigma8 < 0.0)
		error(__FFL__, "moria_cfg: Parameter cosmo_sigma8 must be set.\n");
	if (moria_cfg->cosmo.flat < 0)
		error(__FFL__, "moria_cfg: Parameter cosmo_flat must be set.\n");
	if (moria_cfg->cosmo.h <= 0.0)
		error(__FFL__, "moria_cfg: Hubble parameter h must be greater than zero.\n");
	if (moria_cfg->cosmo.Omega_m <= 0.0)
		error(__FFL__, "moria_cfg: Omega_m must be greater than zero.\n");

	if (moria_cfg->cosmo.ps_type == POWERSPECTRUM_TYPE_EH98)
	{
		if (moria_cfg->cosmo.ns < 0.0)
			error(__FFL__,
					"moria_cfg: Parameter cosmo_ns must be set and non-negative, found %.3f.\n",
					moria_cfg->cosmo.ns);
	} else if (moria_cfg->cosmo.ps_type == POWERSPECTRUM_TYPE_POWERLAW)
	{
		if ((moria_cfg->cosmo.ns <= -3.0) || (moria_cfg->cosmo.ns >= 0.0))
			error(__FFL__,
					"For self-similar (power-law) cosmologies, ns must be between -3 and 0 (found %.3f).\n",
					moria_cfg->cosmo.ns);
	} else
	{
		error(__FFL__,
				"Found unexpected power spectrum type %d, must be %d (LCDM) or %d (power-law).\n",
				moria_cfg->cosmo.ps_type, POWERSPECTRUM_TYPE_EH98, POWERSPECTRUM_TYPE_POWERLAW);
	}

	/*
	 * Add some cosmological info that was not loaded from the SPARTA config
	 */
	def_200m = getHaloDefinitionSO(HDEF_SUBTYPE_SO_MATTER, 200.0);
	def_vir = getHaloDefinitionSO(HDEF_SUBTYPE_SO_VIRIAL, 0.0);

	moria_cfg->snap_rho_200m = memAlloc(__FFL__, MID_MORIA_CONFIG,
			moria_cfg->n_snaps * sizeof(float));
	moria_cfg->snap_rho_vir = memAlloc(__FFL__, MID_MORIA_CONFIG,
			moria_cfg->n_snaps * sizeof(float));
	moria_cfg->snap_rho_catmain = memAlloc(__FFL__, MID_MORIA_CONFIG,
			moria_cfg->n_snaps * sizeof(float));

	for (i = 0; i < moria_cfg->n_snaps; i++)
	{
		moria_cfg->snap_rho_200m[i] = densityThreshold(&(moria_cfg->cosmo), moria_cfg->snap_z[i],
				&def_200m);
		moria_cfg->snap_rho_vir[i] = densityThreshold(&(moria_cfg->cosmo), moria_cfg->snap_z[i],
				&def_vir);
		moria_cfg->snap_rho_catmain[i] = densityThreshold(&(moria_cfg->cosmo), moria_cfg->snap_z[i],
				&(moria_cfg->main_def_catalog));
	}

	/*
	 * Determine whether we need to load certain ghost-related quantities from SPARTA
	 */
	moria_cfg->load_all_halos = moria_cfg->output_tree;
	moria_cfg->do_ghosts = (moria_cfg->include_ghosts && moria_cfg->sparta_has_ghosts);
	moria_cfg->load_sparta_pid = moria_cfg->do_ghosts;
	moria_cfg->load_sparta_xv = (moria_cfg->do_ghosts
			|| (moria_cfg->sparta_has_ghosts && moria_cfg->adjust_phantom_xv));

	/*
	 * If the user selected to output all snaps, create the corresponding array. Otherwise, check
	 * that the selected snaps are within range.
	 */
	if (moria_cfg->n_a == 0)
		error(__FFL__, "Need at least one output redshift.\n");
	if (moria_cfg->output_a[0] == -1.0)
	{
		if (moria_cfg->n_a != 1)
			error(__FFL__,
					"If the chosen output redshift is -1 (all), cannot have any other redshifts.\n");
	} else
	{
		for (i = 0; i < moria_cfg->n_a; i++)
		{
			a_remove_mask[i] = ((moria_cfg->output_a[i] < moria_cfg->snap_a[0])
					|| (moria_cfg->output_a[i] > moria_cfg->snap_a[moria_cfg->n_snaps - 1]));
			if (a_remove_mask[i])
			{
				warningOrError(__FFL__, moria_cfg->err_level_snap_outside_range,
						"Output scale factor %.4f is too small or large for range in SPARTA file (%.4f .. %.4f) and will be ignored.\n",
						moria_cfg->output_a[i], moria_cfg->snap_a[0],
						moria_cfg->snap_a[moria_cfg->n_snaps - 1]);
			}
		}
		j = 0;
		for (i = 0; i < moria_cfg->n_a; i++)
		{
			if (!a_remove_mask[i])
			{
				moria_cfg->output_a[j] = moria_cfg->output_a[i];
				j++;
			}
		}
		moria_cfg->n_a = j;
	}

	/*
	 * If the user did not manually set the catalog dir, assume that it is the same as it was
	 * were in the SPARTA run.
	 */
	if (strlen(moria_cfg->catalog_dir) < 1)
		strcpy(moria_cfg->catalog_dir, sd->cat_path);
	strcpy(config.cat_path, moria_cfg->catalog_dir);
	sprintf(moria_cfg->output_dir_treetmp, "%s/%s", moria_cfg->output_dir_tmp, OUTDIR_TREETMP);

	/*
	 * Work out quantities that have been given in certain units
	 */
	switch (moria_cfg->cut_units)
	{
	case MORIA_UNITS_NONE:
		moria_cfg->cut_threshold = moria_cfg->cut_threshold_in_units;
		break;
	case MORIA_UNITS_PARTICLES:
		moria_cfg->cut_threshold = moria_cfg->cut_threshold_in_units * moria_cfg->particle_mass;
		break;
	case MORIA_UNITS_PARTICLES_VMAX:
		/*
		 * Use an empirical conversion formula from mass to vmax, Klypin et al. 2011, equation 8
		 */
		moria_cfg->cut_threshold = 2.8E-2
				* pow(moria_cfg->cut_threshold_in_units * moria_cfg->particle_mass, 0.316);
		break;
	default:
		error(__FFL__, "Unknown unit identifier, %d.\n", moria_cfg->cut_units);
		break;
	}

	/*
	 * Convert strings to halo definitions
	 */
	for (i = 0; i < moria_cfg->n_defs_rm; i++)
		moria_cfg->output_rm_defs[i] = stringToHaloDefinition(moria_cfg->output_rm_defs_str[i], 0);

	for (i = 0; i < moria_cfg->n_defs_sub; i++)
	{
		moria_cfg->output_sub_defs[i] = stringToHaloDefinition(moria_cfg->output_sub_defs_str[i],
				0);

		if (moria_cfg->output_sub_defs[i].quantity != HDEF_Q_RADIUS)
			error(__FFL__,
					"Subhalo relation definition %s does not denote radius (must start with R).\n",
					moria_cfg->output_sub_defs_str[i]);
	}

	/*
	 * Make sure we have enough room for all the quantities to be stored
	 */
	if ((moria_cfg->n_defs_rm + moria_cfg->n_defs_sub + 2) > MORIA_MAX_DEFS_ALL)
		error(__FFL__,
				"Not enough room (%d) to store all definitions (%d); increase MORIA_MAX_DEFS.\n",
				MORIA_MAX_DEFS_ALL, moria_cfg->n_defs_rm + moria_cfg->n_defs_sub + 2);

	/*
	 * Compile list of all quantities to be loaded. First, add the output quantities chosen by the
	 * user, either for direct output or for computing subhalo relations.
	 */
	j = 0;
	for (i = 0; i < moria_cfg->n_defs_rm; i++)
	{
		moria_cfg->all_defs[j] = moria_cfg->output_rm_defs[i];
		moria_cfg->output_rm_defs[i].idx = j;
		j++;
	}
	for (i = 0; i < moria_cfg->n_defs_sub; i++)
	{
		found = 0;
		for (k = 0; k < j; k++)
		{
			if (compareHaloDefinitions(moria_cfg->all_defs[k], moria_cfg->output_sub_defs[i])
					== HDEF_DIFF_NONE)
			{
				moria_cfg->output_sub_defs[i].idx = k;
				found = 1;
				break;
			}
		}
		if (!found)
		{
			moria_cfg->all_defs[j] = moria_cfg->output_sub_defs[i];
			moria_cfg->output_sub_defs[i].idx = j;
			j++;
		}
	}

	/*
	 * Now find the cut and order definitions in the already listed ones. If it's not there, add
	 * it.
	 */
	moria_cfg->cut_def.idx = -1;
	for (k = 0; k < j; k++)
	{
		if (compareHaloDefinitions(moria_cfg->all_defs[k], moria_cfg->cut_def) == HDEF_DIFF_NONE)
		{
			moria_cfg->cut_def.idx = k;
			break;
		}
	}
	if (moria_cfg->cut_def.idx == -1)
	{
		moria_cfg->all_defs[j] = moria_cfg->cut_def;
		moria_cfg->cut_def.idx = j;
		j++;
	}

	moria_cfg->order_def.idx = -1;
	for (k = 0; k < j; k++)
	{
		if (compareHaloDefinitions(moria_cfg->all_defs[k], moria_cfg->order_def) == HDEF_DIFF_NONE)
		{
			moria_cfg->order_def.idx = k;
			break;
		}
	}
	if (moria_cfg->order_def.idx == -1)
	{
		moria_cfg->all_defs[j] = moria_cfg->order_def;
		moria_cfg->order_def.idx = j;
		j++;
	}

	/*
	 * Find the catalog definition, or add it to the array. Here, we need to first make sure that
	 * it does refer to the catalog. If the definition is not M200m_all, we should issue a warning:
	 * SPARTA intrinsically assumes it is, so anything else is an approximation.
	 */
	if ((moria_cfg->main_def_catalog.source != HDEF_SOURCE_ANY)
			&& (moria_cfg->main_def_catalog.source != HDEF_SOURCE_CATALOG))
	{
		error(__FFL__,
				"Found source other than catalog in the main mass definition for the catalog.\n");
	} else
	{
		moria_cfg->main_def_catalog.source = HDEF_SOURCE_CATALOG;
	}

	if ((moria_cfg->main_def_catalog.type != HDEF_TYPE_SO)
			|| (moria_cfg->main_def_catalog.subtype != HDEF_SUBTYPE_SO_MATTER)
			|| (moria_cfg->main_def_catalog.overdensity != 200.0))
	{
		warningOrError(__FFL__, moria_cfg->err_level_main_def_not_200m,
				"The main catalog definition is not 200m. This leads to approximations.\n");
	}

	moria_cfg->main_def_catalog.idx = -1;
	for (k = 0; k < j; k++)
	{
		if (compareHaloDefinitions(moria_cfg->all_defs[k], moria_cfg->main_def_catalog)
				== HDEF_DIFF_NONE)
		{
			moria_cfg->main_def_catalog.idx = k;
			break;
		}
	}
	if (moria_cfg->main_def_catalog.idx == -1)
	{
		moria_cfg->all_defs[j] = moria_cfg->main_def_catalog;
		moria_cfg->main_def_catalog.idx = j;
		j++;
	}

	/*
	 * Set the counter for all definitions.
	 */
	moria_cfg->n_defs_all = j;

	/*
	 * Check for missing information; in particular, set default sources for Rsp (SPARTA) and
	 * Vmax (catalog). For SO definitions, the source could be ambiguous and we throw an error if
	 * the user has not set it. For ORB definitions, the source will always be SPARTA, but since
	 * they are handled through the HPS analysis, we require the user to set the SPARTA flag so
	 * that the naming comes out consistent.
	 */
	moria_cfg->n_defs_catalog = 0;
	moria_cfg->n_defs_sparta = 0;
	for (j = 0; j < moria_cfg->n_defs_all; j++)
	{
		if (moria_cfg->all_defs[j].source == HDEF_SOURCE_ANY)
		{
			if (moria_cfg->all_defs[j].type == HDEF_TYPE_SPLASHBACK)
			{
				moria_cfg->all_defs[j].source = HDEF_SOURCE_SPARTA;
			} else if (moria_cfg->all_defs[j].type == HDEF_TYPE_VMAX)
			{
				moria_cfg->all_defs[j].source = HDEF_SOURCE_CATALOG;
			} else
			{
				haloDefinitionToString(moria_cfg->all_defs[j], tmp_str);
				error(__FFL__,
						"In definition %s, could not determine source (Sparta or catalog), must be set by user.\n",
						tmp_str);
			}
		} else if ((moria_cfg->all_defs[j].source != HDEF_SOURCE_CATALOG)
				&& (moria_cfg->all_defs[j].source != HDEF_SOURCE_SPARTA))
		{
			error(__FFL__,
					"Found unrecognized source code %d in definition %s, expected catalog or Sparta.\n",
					moria_cfg->all_defs[j].source, tmp_str);
		}

		if (moria_cfg->all_defs[j].source == HDEF_SOURCE_CATALOG)
		{
			moria_cfg->n_defs_catalog++;
		} else if (moria_cfg->all_defs[j].source == HDEF_SOURCE_SPARTA)
		{
			moria_cfg->n_defs_sparta++;
		}

		haloDefinitionToString(moria_cfg->all_defs[j], moria_cfg->all_defs_str[j]);
	}

	/*
	 * Write source information back to cut def and order def. Otherwise, checks on those sources
	 * may come out ANY if, in fact, a source was set above.
	 */
	moria_cfg->order_def.source = moria_cfg->all_defs[moria_cfg->order_def.idx].source;
	moria_cfg->cut_def.source = moria_cfg->all_defs[moria_cfg->cut_def.idx].source;

	/*
	 * For SPARTA definitions, check that they are actually available. Splashback definitions
	 * should come from the Rsp analysis, SO definitions from the haloprops analysis. If a
	 * definition is neither, we need to throw an error because we won't know where to get it.
	 */
	for (j = 0; j < moria_cfg->n_defs_all; j++)
	{
		if (moria_cfg->all_defs[j].source == HDEF_SOURCE_SPARTA)
		{
			found = 0;
			if (moria_cfg->all_defs[j].type == HDEF_TYPE_SPLASHBACK)
			{
				for (i = 0; i < sd->anl_rsp.n_defs; i++)
				{
					/*
					 * For splashback definitions, the requested and provided defs need to be an
					 * exact match, there is no conversion MORIA can perform.
					 */
					if (compareHaloDefinitions(sd->anl_rsp.defs[i], moria_cfg->all_defs[j])
							== HDEF_DIFF_NONE)
					{
						found = 1;
						break;
					}
				}
				if (!found)
					error(__FFL__, "Could not find halo definition %s in SPARTA Rsp analysis.\n",
							moria_cfg->all_defs_str[j]);
			} else if (isSpartaHpsDefinition(&(moria_cfg->all_defs[j])))
			{
				for (i = 0; i < sd->anl_hps.n_defs; i++)
				{
					/*
					 * For SO definitions, MORIA can convert radii and masses. Thus, we can tolerate
					 * a difference in quantity between the definitions.
					 */
					if (compareHaloDefinitions(sd->anl_hps.defs[i], moria_cfg->all_defs[j])
							>= HDEF_DIFF_QUANTITY)
					{
						found = 1;
						break;
					}
				}
				if (!found)
					error(__FFL__,
							"Could not find halo definition %s in SPARTA haloprops analysis.\n",
							moria_cfg->all_defs_str[j]);
			} else
			{
				error(__FFL__,
						"Halo definition %s is supposed to be loaded from SPARTA, but is neither splashback nor SO.\n",
						moria_cfg->all_defs_str[j]);
			}
		}
	}
}

/*
 * Config processing that needs to happen even for a restart
 */
void processMoriaConfigRestart(MoriaConfig *moria_cfg)
{
	sprintf(moria_cfg->output_dir_restart, "%s/%s", moria_cfg->output_dir_tmp, OUTDIR_RESTART);
}

void printMoriaConfigList(const char *param, char *list, int n, int list_str_len)
{
	char tmpstr[1000], tmpstr2[100];
	int i, l, l2;

	const int max_len = 140;

	sprintf(tmpstr, "    %-21s                %s", param, list);
	l = strlen(tmpstr);
	for (i = 1; i < n; i++)
	{
		sprintf(tmpstr2, ", %s", list + i * list_str_len);
		l2 = strlen(tmpstr2);
		if (l + l2 > max_len)
		{
			sprintf(tmpstr, "%s,\n", tmpstr);
			output(0, tmpstr);
			sprintf(tmpstr, "%s", "                                           ");
			sprintf(tmpstr2, "%s", list + i * list_str_len);
			l = strlen(tmpstr);
		}
		l += l2;
		sprintf(tmpstr, "%s%s", tmpstr, tmpstr2);
	}
	sprintf(tmpstr, "%s\n", tmpstr);
	output(0, tmpstr);
}

void printMoriaConfig(MoriaConfig *moria_cfg, SpartaData *sd)
{
	int i;
	char a_str[200], cut_q_str[DEFAULT_HALO_DEF_STRLEN], order_q_str[DEFAULT_HALO_DEF_STRLEN],
			main_def_cat_q_str[DEFAULT_HALO_DEF_STRLEN], cut_unit_str[20];

	if (moria_cfg->output_a[0] < 0.0)
	{
		sprintf(a_str, "%s", "all");
	} else
	{
		sprintf(a_str, "%.4f", moria_cfg->output_a[0]);
		for (i = 1; i < moria_cfg->n_a; i++)
			sprintf(a_str, "%s, %.4f", a_str, moria_cfg->output_a[i]);
	}

	haloDefinitionToString(moria_cfg->cut_def, cut_q_str);
	haloDefinitionToString(moria_cfg->order_def, order_q_str);
	haloDefinitionToString(moria_cfg->main_def_catalog, main_def_cat_q_str);
	unitsToString(moria_cfg->cut_units, cut_unit_str);

	output(0, "RUN-TIME CONFIGURATION\n");
	printLine(0);

	output(0, "Input\n");
	output(0, "    sparta_file                          %s\n", moria_cfg->sparta_file);
	output(0, "    catalog_dir                          %s\n", moria_cfg->catalog_dir);
	output(0, "    catalog type                         %d\n", moria_cfg->cat_type);
	output(0, "    main_def_catalog                     %s\n", main_def_cat_q_str);
	if (moria_cfg->cat_type == CAT_TYPE_ROCKSTAR)
	{
		output(0, "    rockstar_strict_so                   %d\n", moria_cfg->rockstar_strict_so);
		output(0, "    rockstar_bound_props                 %d\n", moria_cfg->rockstar_bound_props);
	}
	output(0, "Cosmology\n");
	output(0, "    flat                                 %d\n", moria_cfg->cosmo.flat);
	output(0, "    Omega_m                              %.4f\n", moria_cfg->cosmo.Omega_m);
	output(0, "    Omega_L                              %.4f\n", moria_cfg->cosmo.Omega_L);
	output(0, "    omega_b                              %.4f\n", moria_cfg->cosmo.Omega_b);
	output(0, "    h                                    %.4f\n", moria_cfg->cosmo.h);
	output(0, "    sigma8                               %.4f\n", moria_cfg->cosmo.sigma8);
	output(0, "    ns                                   %.4f\n", moria_cfg->cosmo.ns);
	output(0, "    power spectrum type                  %d\n", moria_cfg->cosmo.ps_type);

	output(0, "From SPARTA\n");
	output(0, "    output_min_n200m                     %d\n", moria_cfg->output_min_n200m);
	output(0, "    has_ghosts                           %d\n", moria_cfg->sparta_has_ghosts);
	output(0, "    particle_mass                        %.4e\n", moria_cfg->particle_mass);
	output(0, "    box_size                             %.4f\n", moria_cfg->box_size);
	output(0, "    n_snap                               %d\n", moria_cfg->n_snaps);
	output(0, "    snap_a                               %.4f .. %.4f\n", moria_cfg->snap_a[0],
			moria_cfg->snap_a[moria_cfg->n_snaps - 1]);
	output(0, "    n_halos                              %d\n", sd->n_halos);
	if (sd->anl_rsp.anl_exists)
		output(0, "    n_anl_rsp                            %d (%.1f%% of halos)\n",
				sd->anl_rsp.n_anl, 100.0 * sd->anl_rsp.n_anl / sd->n_halos);
	if (sd->anl_hps.anl_exists)
		output(0, "    n_anl_hps                            %d (%.1f%% of halos)\n",
				sd->anl_hps.n_anl, 100.0 * sd->anl_hps.n_anl / sd->n_halos);

	output(0, "Algorithm\n");
	output(0, "    cut_def                              %s\n", cut_q_str);
	output(0, "    cut_threshold                        %.3e\n", moria_cfg->cut_threshold_in_units);
	output(0, "    cut_units                            %s\n", cut_unit_str);
	output(0, "    cut_threshold (abs)                  %.3e\n", moria_cfg->cut_threshold);
	output(0, "    order_def                            %s\n", order_q_str);
	output(0, "    hostsub_assign_subparent_id          %d\n",
			moria_cfg->hostsub_assign_subparent_id);
	output(0, "    hostsub_restrict_to_output           %d\n",
			moria_cfg->hostsub_restrict_to_output);
	output(0, "    include_ghosts                       %d\n", moria_cfg->include_ghosts);
	output(0, "    adjust_phantom_xv                    %d\n", moria_cfg->adjust_phantom_xv);
	output(0, "    max_mass_ratio_sparta                %.4f\n", moria_cfg->max_mass_ratio_sparta);
	output(0, "    max_rsp_model_diff_factor            %.4f\n",
			moria_cfg->max_rsp_model_diff_factor);
	output(0, "    max_rsp_model_diff_sigma             %.4f\n",
			moria_cfg->max_rsp_model_diff_sigma);

	output(0, "Console output\n");
	output(0, "    log_level                            %d\n", moria_cfg->log_level);
	output(0, "    log_level_memory                     %d\n", moria_cfg->log_level_memory);
	output(0, "    log_level_timing                     %d\n", moria_cfg->log_level_timing);
	output(0, "    log_flush                            %d\n", moria_cfg->log_flush);
	output(0, "    log_substatus_diff                   %d\n", moria_cfg->log_substatus_diff);

	output(0, "Error levels\n");
	output(0, "    err_level_more_snaps_than_cat        %d\n",
			moria_cfg->err_level_more_snaps_than_cat);
	output(0, "    err_level_snap_outside_range         %d\n",
			moria_cfg->err_level_snap_outside_range);
	output(0, "    err_level_main_def_not_200m          %d\n",
			moria_cfg->err_level_main_def_not_200m);
	output(0, "    err_level_acc_rate_undefined         %d\n",
			moria_cfg->err_level_acc_rate_undefined);
	output(0, "    err_level_cat_field_changed          %d\n",
			moria_cfg->err_level_cat_field_changed);

	output(0, "File output\n");
	output(0, "    output_dir_original                  %s\n", moria_cfg->output_dir_original);
	output(0, "    output_dir_hdf5                      %s\n", moria_cfg->output_dir_hdf5);
	output(0, "    output_dir_restart                   %s\n", moria_cfg->output_dir_restart);
	output(0, "    output_dir_treetmp                   %s\n", moria_cfg->output_dir_treetmp);
	output(0, "    output_filename_tree                 %s\n", moria_cfg->output_filename_tree);
	output(0, "    output_restart_files                 %d\n", moria_cfg->output_restart_files);
	output(0, "    output_restart_every                 %d\n", moria_cfg->output_restart_every);
	output(0, "    output_original                      %d\n", moria_cfg->output_original);
	output(0, "    output_hdf5                          %d\n", moria_cfg->output_hdf5);
	output(0, "    output_tree                          %d\n", moria_cfg->output_tree);
	output(0, "    output_compression_level             %d\n", moria_cfg->output_compression_level);
	output(0, "    output_a                             %s\n", a_str);

	printMoriaConfigList("output_rm_defs", (char*) moria_cfg->output_rm_defs_str,
			moria_cfg->n_defs_rm,
			DEFAULT_HALO_DEF_STRLEN);
	printMoriaConfigList("output_sub_defs", (char*) moria_cfg->output_sub_defs_str,
			moria_cfg->n_defs_sub,
			DEFAULT_HALO_DEF_STRLEN);
	printMoriaConfigList("all_defs", (char*) moria_cfg->all_defs_str, moria_cfg->n_defs_all,
	DEFAULT_HALO_DEF_STRLEN);

	printMoriaConfigList("output_cat_fields", (char*) moria_cfg->output_cat_fields,
			moria_cfg->n_cat_fields,
			CAT_FIELD_LENGTH);
}

void freeMoriaConfig(MoriaConfig *moria_cfg)
{
	memFree(__FFL__, MID_MORIA_CONFIG, moria_cfg->snap_a, moria_cfg->n_snaps * sizeof(float));
	memFree(__FFL__, MID_MORIA_CONFIG, moria_cfg->snap_z, moria_cfg->n_snaps * sizeof(float));
	memFree(__FFL__, MID_MORIA_CONFIG, moria_cfg->snap_t, moria_cfg->n_snaps * sizeof(float));
	memFree(__FFL__, MID_MORIA_CONFIG, moria_cfg->snap_t_dyn, moria_cfg->n_snaps * sizeof(float));
	memFree(__FFL__, MID_MORIA_CONFIG, moria_cfg->snap_rho_200m,
			moria_cfg->n_snaps * sizeof(float));
	memFree(__FFL__, MID_MORIA_CONFIG, moria_cfg->snap_rho_vir, moria_cfg->n_snaps * sizeof(float));
	memFree(__FFL__, MID_MORIA_CONFIG, moria_cfg->snap_rho_catmain,
			moria_cfg->n_snaps * sizeof(float));
}
