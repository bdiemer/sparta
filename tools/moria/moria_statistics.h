/*************************************************************************************************
 *
 * Functions to output various statistics about moria and the halo population.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_STATISTICS_H_
#define _MORIA_STATISTICS_H_

#include "moria_global.h"
#include "moria_config.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Timers
 */
#define MORIA_TIMERS0 \
ENUM2(MORIA_T0_INIT, Loading SPARTA data)\
ENUM2(MORIA_T0_SNAPS, Snapshots)\
ENUM2(MORIA_T0_TREE, Merger tree)\
ENUM2(MORIA_T0_FINALIZE, Finalize)

#define MORIA_TIMERS1 \
ENUM2(MORIA_T1_INITFINAL, Initialize and Finalize)\
ENUM2(MORIA_T1_SORT, Sort IDs)\
ENUM2(MORIA_T1_READCAT, Read halo catalog)\
ENUM2(MORIA_T1_SPARTAMATCH, Match to SPARTA file)\
ENUM2(MORIA_T1_GHOSTS, Add ghost halos)\
ENUM2(MORIA_T1_COMBINE, Combine halo data)\
ENUM2(MORIA_T1_HOSTSUB, Subhalo relations)\
ENUM2(MORIA_T1_HALOSTATS, Halo statistics)\
ENUM2(MORIA_T1_TREE, Merger tree logic)\
ENUM2(MORIA_T1_READCAT2, Read catalog data)\
ENUM2(MORIA_T1_OUTCATALOG, Write catalog)\
ENUM2(MORIA_T1_OUTHDF5, Write HDF5)\
ENUM2(MORIA_T1_RESTART, Write restart files)

#define ENUM2(x, y) x,

enum
{
	MORIA_TIMERS0/*space*/MORIA_T0_N
};

enum
{
	MORIA_TIMERS1/*space*/MORIA_T1_N
};

#undef ENUM2

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct
{
	clock_t timers_0[MORIA_T0_N];
	clock_t timers_1[MORIA_T1_N];
	clock_t timers_1_cum[MORIA_T1_N];
} MoriaTimers;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initTimers(MoriaTimers *timers);
void resetTimersSnapshot(MoriaTimers *timers);

void printHaloStatistics(MoriaConfig *moria_cfg, SpartaData *sd, HaloSet *hs);
void printMoriaTimingStatistics(MoriaConfig *moria_cfg, MoriaTimers *timers);
void printMoriaTimingStatisticsFinal(MoriaConfig *moria_cfg, MoriaTimers *timers);

#endif
