/*************************************************************************************************
 *
 * Routines related to the halo set.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MORIA_HALO_H_
#define _MORIA_HALO_H_

#include "moria_global.h"
#include "moria_config.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

HaloSet* initializeHaloSet(int n_defs, int n_halos);
void initializeHaloData(HaloData *hd);
void freeHaloSet(MoriaConfig *moria_cfg, HaloSet *hs);

int countGhostHalos(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap);
void addGhostHalos(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		int n_ghosts);

#endif
