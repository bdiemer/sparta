/*************************************************************************************************
 *
 * The MORIA tool writes SPARTA output back into catalog files.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>

#include "moria_global.h"
#include "moria_config.h"
#include "moria_io.h"
#include "moria_io_sparta.h"
#include "moria_run.h"
#include "moria_statistics.h"
#include "../../src/statistics.h"
#include "../../src/memory.h"

/*************************************************************************************************
 * MAIN
 *************************************************************************************************/

int main(int argc, char **argv)
{
	int restart, cfg_file_idx;
	MoriaConfig moria_cfg;
	MoriaTimers timers;
	SpartaData *sd = NULL;

	/*
	 * Initialize the timers, print welcome message
	 */
	initTimers(&timers);
	timers.timers_0[MORIA_T0_INIT] = -clock();

	printLine(0);
	output(0, "Welcome to MORIA.\n");
	printLine(0);

	/*
	 * Check input parameters. There can be 1 (argc = 2), which must be the config file, or 2,
	 * where the first is a restart flag (argc = 3).
	 */
	restart = 0;
	cfg_file_idx = -1;
	if (argc < 2)
	{
		error(__FFL__,
				"[Main] Missing config file parameter. Usage: moria [-restart] <config file>.\n");
	} else if (argc == 2)
	{
		restart = 0;
		cfg_file_idx = 1;
	} else if (argc == 3)
	{
		if (!(strcmp(argv[1], "-restart") == 0))
			error(__FFL__,
					"[Main] Found unknown parameter '%s'. Usage: moria [-restart] <config file>.\n",
					argv[1]);
		restart = 1;
		cfg_file_idx = 2;
	} else
	{
		error(__FFL__, "[Main] Too many parameters. Usage: moria [-restart] <config file>.\n");
	}

	/*
	 * Initialize some global SPARTA variables in the absence of MPI
	 */
	proc = 0;
	n_proc = 1;
	is_main_proc = 1;
	comm = COMM_NO_MPI;

	/*
	 * We are tracking memory in MORIA just as in SPARTA, which means we need to initialize the
	 * counters.
	 */
	resetMemoryStatistics();

	/*
	 * We set the default config in SPARTA; this is undesirable in the sense that fields should
	 * either not be used or overwritten in MORIA, but there are some parameters that immediately
	 * cause problems if not set to reasonable values, such as the memory log level.
	 *
	 * We then read the moria_cfg. This involves multiple stages, because certain values from
	 * the config and input files need to be written back to the SPARTA config at various stages.
	 *
	 * Step 1: Read config from moria config file and apply to the global Sparta config
	 * structure. We need to do this even if restarting because we need the output path where we
	 * can find the restart files.
	 */
	setDefaultConfig();
	setMoriaConfigDefaults(&moria_cfg);
	readMoriaConfigFile(argv[cfg_file_idx], &moria_cfg);

	if (restart)
	{
		processMoriaConfigRestart(&moria_cfg);
	} else
	{
		/*
		 * Step 2: Read the Sparta config, set the Sparta array properties from both the moria
		 * config and the Sparta file.
		 */
		sd = readSpartaConfig(&moria_cfg);
		setSpartaConfig(&moria_cfg);

		/*
		 * Step 3: Process the config, for example, halo definitions
		 */
		processMoriaConfig(&moria_cfg, sd);

		/*
		 * Step 4: Print config
		 */
		printMoriaConfig(&moria_cfg, sd);
		printLine(0);

		/*
		 * Prep work; if we are restarting, the output directories (and files) really ought to have
		 * been created already.
		 */
		createOutputDirectories(&moria_cfg);
		readSpartaData(&moria_cfg, sd);
	}

	/*
	 * Do the actual work
	 */
	moriaProcessSimulation(&moria_cfg, &timers, &sd, restart);

	/*
	 * Free data, output final information
	 */
	freeCosmology(&(moria_cfg.cosmo));
	freeMoriaConfig(&moria_cfg);
	timers.timers_0[MORIA_T0_FINALIZE] += clock();

	printMoriaTimingStatisticsFinal(&moria_cfg, &timers);
	printLine(0);
	config.log_level_memory = 2;
	printMemoryStatistics(0, 1);
	printLine(0);
	output(0, "Finished.\n");
	printLine(0);

	return EXIT_SUCCESS;
}
