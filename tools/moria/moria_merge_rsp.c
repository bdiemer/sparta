/*************************************************************************************************
 *
 * This unit merges the catalog data with SPARTA splashback data.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "moria_merge_rsp.h"
#include "../models/models_rsp.h"
#include "../../src/halos/halo.h"
#include "../../src/halos/halo_so.h"
#include "../../src/analyses/analysis_rsp.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Tolerances for the algorithm when determining quantities at slightly different times. Each
 * number multiplies the current dynamical time:
 *
 * TDYN_INTERP:         The maximum distance in time an Rsp/Msp value can have to be accepted
 *                      when the current Rsp/Msp is not available.
 */
#define TOLERANCE_TDYN_INTERP 1.0

#define RET_EPOCH_NO_RESULT 0
#define RET_EPOCH_RESULT_INVALID 1
#define RET_EPOCH_VALID 2

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int getPastOrFutureQsp(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloData *hd,
		int anl_idx, int anl_s_idx, float Gamma, float *qq_out);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Combine the catalog and Rsp analysis data.
 */
void mergeAnalysisRsp(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloSet *hs,
		int halo_idx)
{
	int i, j, k, anl_idx, anl_snap_idx, snap_idx_k, found_past, found_future, ret;
	float Gamma, tdyn_current, t_past, t_future, q200m_now, qq_interp,
			qq_past[moria_cfg->n_defs_all], qq_future[moria_cfg->n_defs_all];
	HaloDefinition *def;
	HaloData *hd;

	/*
	 * Shortcut to the HaloData record for this halo. We can't pass that directly because we also
	 * need the defs / upid_defs fields from the haloset.
	 */
	hd = &(hs->hd[halo_idx]);

	/*
	 * Find the correct snapshot index in this analysis. If this snapshot does not exist, throw an
	 * error.
	 */
	anl_snap_idx = sd->anl_rsp.snap_idxs_in_anl[snap->snap_idx];
	if (anl_snap_idx < 0)
		error(__FFL__, "Could not find Rsp analysis at snapshot %d, redshift %.2f.\n",
				snap->snap_idx, moria_cfg->snap_z[snap->snap_idx]);

	/*
	 * First check: if the halo does not exist, we just set the status accordingly.
	 */
	if (hd->sparta_idx != -1)
	{
		anl_idx = sd->anl_rsp.anl_idx[hd->sparta_idx];

		if (anl_idx == -1)
		{
			hd->status_sparta_rsp = ANL_RSP_STATUS_NOT_FOUND;
		} else
		{
			hd->status_sparta_rsp = sd->anl_rsp.status[anl_idx][anl_snap_idx];
		}
	} else
	{
		anl_idx = -1;
		hd->status_sparta_rsp = ANL_RSP_STATUS_HALO_NOT_SAVED;
	}

	/*
	 * Sanity check that the status has one of the expected values.
	 */
	if ((hd->status_sparta_rsp <= ANL_RSP_STATUS_UNDEFINED)
			|| (hd->status_sparta_rsp > ANL_RSP_STATUS_INSUFFICIENT_WEIGHT))
	{
		if (hd->sparta_idx != -1)
		{
			for (i = 0; i < moria_cfg->n_snaps; i++)
			{
				output(0, "Snap %3d a %.4f sparta_status status %2d rsp status %d\n", i,
						moria_cfg->snap_a[i], sd->sparta_status[hd->sparta_idx][i],
						sd->anl_rsp.status[hd->sparta_idx][anl_snap_idx]);
			}
		}

		error(__FFL__,
				"Found invalid Rsp status %d in halo ID %ld, sparta_idx %d, anl_idx %d. See info above.\n",
				hd->status_sparta_rsp, hd->id, hd->sparta_idx, anl_idx);
	}

	/*
	 * Reset all definitions and the status.
	 */
	for (j = 0; j < moria_cfg->n_defs_all; j++)
	{
		def = &(moria_cfg->all_defs[j]);
		if (!isSpartaRspDefinition(def))
			continue;
		hs->hd_defs[j][halo_idx] = -1.0;
	}
	hd->status_moria_rsp = MORIA_STATUS_UNDEFINED;

	/*
	 * Preference 1: if qsp was output by SPARTA, we take that value of course
	 */
	if (hd->status_sparta_rsp == ANL_RSP_STATUS_SUCCESS)
	{
		for (j = 0; j < moria_cfg->n_defs_all; j++)
		{
			def = &(moria_cfg->all_defs[j]);
			if (!isSpartaRspDefinition(def))
				continue;

			hs->hd_defs[j][halo_idx] =
					sd->anl_rsp.datasets[def->idx].q_float_2[anl_idx][anl_snap_idx];

			debugHaloMoria(hd->id, "Def %2d %-15s from SPARTA %.2e", j, moria_cfg->all_defs_str[j],
					hs->hd_defs[j][halo_idx]);

#if MORIA_CAREFUL
			if (isnan(hs->hd_defs[j][halo_idx]))
				error(__FFL__, "Found NaN in SPARTA values for definition %s, halo ID %ld.\n",
						moria_cfg->all_defs_str[j], hd->id);

			if (isinf(hs->hd_defs[j][halo_idx]))
				error(__FFL__, "Found inf in SPARTA values for definition %s, halo ID %ld.\n",
						moria_cfg->all_defs_str[j], hd->id);

			if (hs->hd_defs[j][halo_idx] <= 0.0)
				error(__FFL__,
						"Found negative or zero in SPARTA values for definition %s, halo ID %ld.\n",
						moria_cfg->all_defs_str[j], hd->id);
#endif
		}

		hd->status_moria_rsp = MORIA_STATUS_SPARTA_SUCCESS;
		return;
	}

	/*
	 * We may want to check against the model predictions or end up using them, so we need to
	 * set a mass accretion rate valid for the models.
	 */
	Gamma = hd->acc_rate_200m_dyn;
	Gamma = fmaxf(Gamma, RSPMODEL_DIEMER20_ACCRATE_MIN);
	Gamma = fminf(Gamma, RSPMODEL_DIEMER20_ACCRATE_MAX);

	/*
	 * Preference 2: interpolate or extrapolate from past and/or future values. We go to the
	 * closest snap in the past and future and keep qsp/q200m or interpolate if we find both.
	 *
	 * However, we allow only values from hosts, since subhalos can have very funny mass values
	 * even for M200m. Furthermore, we use R_ref / M_ref which have been checked against crazy
	 * ratios.
	 */
	if (anl_idx >= 0)
	{
		tdyn_current = moria_cfg->snap_t_dyn[snap->snap_idx];

		/*
		 * Try to find a valid snapshot in the past; if one is found within some fraction of
		 * the dynamical time, we test whether it is valid. If there is no result (because the
		 * halo was not a host or there is no Rsp analysis, we continue searching. If there was
		 * a result but it was deemed invalid, we stop because going further away in time to find
		 * valid results would be a strange choice, given that we know that there is a strange
		 * result closer in time.
		 */
		found_past = 0;
		t_past = -1.0;
		k = anl_snap_idx - 1;
		while (k > 0)
		{
			snap_idx_k = sd->anl_rsp.snap_idxs[k];
			t_past = moria_cfg->snap_t[snap_idx_k];
			if ((snap->t - t_past) > TOLERANCE_TDYN_INTERP * tdyn_current)
				break;
			ret = getPastOrFutureQsp(moria_cfg, sd, snap, hd, anl_idx, k, Gamma, qq_past);
			if (ret == RET_EPOCH_VALID)
				found_past = 1;
			else if (ret == RET_EPOCH_RESULT_INVALID)
				break;
			if (found_past)
				break;
			k--;
		}

		/*
		 * Look for a valid future result, the same way we did for past results.
		 */
		found_future = 0;
		t_future = -1.0;
		k = anl_snap_idx + 1;
		while (k < sd->anl_rsp.n_snaps)
		{
			snap_idx_k = sd->anl_rsp.snap_idxs[k];
			t_future = moria_cfg->snap_t[snap_idx_k];
			if ((t_future - snap->t) > TOLERANCE_TDYN_INTERP * tdyn_current)
				break;
			ret = getPastOrFutureQsp(moria_cfg, sd, snap, hd, anl_idx, k, Gamma, qq_future);
			if (ret == RET_EPOCH_VALID)
				found_future = 1;
			else if (ret == RET_EPOCH_RESULT_INVALID)
				break;
			if (found_future)
				break;
			k++;
		}

		/*
		 * Now loop over mass definitions, if we found either future or past data. Otherwise, there
		 * is nothing to do and we continue
		 */
		q200m_now = -1.0;
		if (found_past || found_future)
		{
			for (j = 0; j < moria_cfg->n_defs_all; j++)
			{
				def = &(moria_cfg->all_defs[j]);
				if (!isSpartaRspDefinition(def))
					continue;

				if (def->quantity == HDEF_Q_RADIUS)
					q200m_now = hd->R_ref;
				else if (def->quantity == HDEF_Q_MASS)
					q200m_now = hd->M_ref;
				else
					error(__FFL__, "Unexpected quantity, %d.\n", def->quantity);

				if (found_past && (!found_future))
				{
					hs->hd_defs[j][halo_idx] = qq_past[j] * q200m_now;
					debugHaloMoria(hd->id,
							"Def %2d %-15s from past value q/q200m %.2f, time difference %.2f tdyn.",
							j, moria_cfg->all_defs_str[j], qq_past[j],
							(snap->t - t_past) / tdyn_current);

				} else if ((!found_past) && found_future)
				{
					hs->hd_defs[j][halo_idx] = qq_future[j] * q200m_now;
					debugHaloMoria(hd->id,
							"Def %2d %-15s from future value q/q200m %.2f, time difference %.2f tdyn.",
							j, moria_cfg->all_defs_str[j], qq_future[j],
							(t_future - snap->t) / tdyn_current);

				} else if (found_past && found_future)
				{
					qq_interp = qq_past[j]
							+ (snap->t - t_past) * (qq_future[j] - qq_past[j])
									/ (t_future - t_past);
					hs->hd_defs[j][halo_idx] = qq_interp * q200m_now;
					debugHaloMoria(hd->id, "Def %2d %-15s from adjacent value at snap %d, new %.2e",
							j, moria_cfg->all_defs_str[j], qq_past[j], qq_future[j],
							(snap->t - t_past) / tdyn_current, (t_future - snap->t) / tdyn_current,
							qq_interp);
				} else
				{
					error(__FFL__, "Internal error.\n");
				}

				/*
				 * Throw errors if the results are not right
				 */
#if MORIA_CAREFUL
				if (isnan(hs->hd_defs[j][halo_idx]))
					error(__FFL__,
							"Inter/extrapolation returned NaN for definition %s, halo ID %ld.\n",
							moria_cfg->all_defs_str[j], hd->id);

				if (isinf(hs->hd_defs[j][halo_idx]))
					error(__FFL__,
							"Inter/extrapolation returned inf for definition %s, halo ID %ld.\n",
							moria_cfg->all_defs_str[j], hd->id);

				if (hs->hd_defs[j][halo_idx] <= 0.0)
					error(__FFL__,
							"Inter/extrapolation returned negative or zero value for definition %s, halo ID %ld.\n",
							moria_cfg->all_defs_str[j], hd->id);
#endif
			}

			/*
			 * Depending on whether we found past and/or future results, either accept one or
			 * interpolate.
			 */
			if (found_past && (!found_future))
			{
				hd->status_moria_rsp = MORIA_RSP_STATUS_ESTIMATED_PAST;
			} else if ((!found_past) && found_future)
			{
				hd->status_moria_rsp = MORIA_RSP_STATUS_ESTIMATED_FUTURE;

			} else if (found_past && found_future)
			{
				hd->status_moria_rsp = MORIA_RSP_STATUS_ESTIMATED_INTERPOLATED;
			} else
			{
				error(__FFL__, "Internal error.\n");
			}

			return;
		}
	}

	/*
	 * Preference 3: output the model prediction.
	 */
	for (j = 0; j < moria_cfg->n_defs_all; j++)
	{
		def = &(moria_cfg->all_defs[j]);
		if (!isSpartaRspDefinition(def))
			continue;

		q200m_now = 0.0;
		if (def->quantity == HDEF_Q_RADIUS)
		{
			q200m_now = hd->R_ref;
		} else if (def->quantity == HDEF_Q_MASS)
		{
			q200m_now = hd->M_ref;
		} else
		{
			error(__FFL__, "Unexpected quantity, %d.\n", def->quantity);
		}

		hs->hd_defs[j][halo_idx] = q200m_now
				* modelDiemer20Splashback(def, Gamma, hd->nu200m, snap->Om);

		debugHaloMoria(hd->id,
				"Def %2d, rsp_status %d, Rsp from model, RM %.2e, acc rate %.2e, acc_rate_model %.2e, nu %.2f, Om %.2f",
				j, hd->status_sparta_rsp, hs->hd_defs[j][halo_idx], hd->acc_rate_200m_dyn, Gamma,
				hd->nu200m, snap->Om);

		/*
		 * Throw errors if the results are not right
		 */
#if MORIA_CAREFUL
		if (isnan(hs->hd_defs[j][halo_idx]))
			error(__FFL__, "Rsp/Msp model returned NaN for definition %s, halo ID %ld.\n",
					moria_cfg->all_defs_str[j], hd->id);

		if (isinf(hs->hd_defs[j][halo_idx]))
			error(__FFL__, "Rsp/Msp model returned inf for definition %s, halo ID %ld.\n",
					moria_cfg->all_defs_str[j], hd->id);

		if (hs->hd_defs[j][halo_idx] <= 0.0)
			error(__FFL__,
					"Rsp/Msp model returned negative or zero value for definition %s, halo ID %ld.\n",
					moria_cfg->all_defs_str[j], hd->id);
#endif
	}

	hd->status_moria_rsp = MORIA_RSP_STATUS_ESTIMATED_MODEL;
}

/*
 * Check whether the splashback quantities at a past or future snapshot are reliable for
 * extrapolation. If so, the ratios are returned in the qq_out array.
 */
int getPastOrFutureQsp(MoriaConfig *moria_cfg, SpartaData *sd, SnapshotData *snap, HaloData *hd,
		int anl_idx, int anl_s_idx, float Gamma, float *qq_out)
{
	int snap_idx_k;

	snap_idx_k = sd->anl_rsp.snap_idxs[anl_s_idx];

	/*
	 * If the halo was not a host, we do not want to use the splashback data, they are probably
	 * unreliable.
	 */
	if (sd->sparta_status[hd->sparta_idx][snap_idx_k] != HALO_STATUS_HOST)
		return RET_EPOCH_NO_RESULT;

	/*
	 * If there are no splashback data, we can't return any.
	 */
	if (sd->anl_rsp.status[anl_idx][anl_s_idx] != ANL_RSP_STATUS_SUCCESS)
		return RET_EPOCH_NO_RESULT;

	/*
	 * If the user has set a limit on how many sigma the ratio can be away from the splashback
	 * model, we check all definitions against the model. If one exceeds the threshold, we do not
	 * use this epoch.
	 */
	int j, ret;
	float qq_model, qq_model_scatter, R200m, q200m, model_diff, log_model_factor;
	HaloDefinition *def, def_err;

	ret = RET_EPOCH_VALID;
	q200m = 0.0;
	log_model_factor = log10(moria_cfg->max_rsp_model_diff_factor);

	for (j = 0; j < moria_cfg->n_defs_all; j++)
	{
		def = &(moria_cfg->all_defs[j]);

		if (isSpartaRspDefinition(def))
		{
			/*
			 * Compute the SPARTA values and model prediction for q/q
			 */
			R200m = sd->halo_R200m[hd->sparta_idx][snap_idx_k];
			if (def->quantity == HDEF_Q_RADIUS)
				q200m = R200m;
			else if (def->quantity == HDEF_Q_MASS)
				q200m = soRtoM(R200m, moria_cfg->snap_rho_200m[snap_idx_k]);
			else
				error(__FFL__, "Unexpected quantity, %d.\n", def->quantity);

			qq_out[j] = sd->anl_rsp.datasets[def->idx].q_float_2[anl_idx][anl_s_idx] / q200m;

			/*
			 * If desired, we check against the model. If the qq from SPARTA is more than a
			 * certain number of sigma away from the prediction, we reject this epoch altogether.
			 */
			memcpy(&def_err, def, sizeof(HaloDefinition));
			def_err.is_error = HDEF_ERR_1SIGMA;

			qq_model = modelDiemer20Splashback(def, Gamma, hd->nu200m, snap->Om);
			qq_model_scatter = modelDiemer20Scatter(&def_err, Gamma, hd->nu200m);

			model_diff = fabs(log10(qq_out[j] / qq_model));

			if ((model_diff > log_model_factor)
					&& (model_diff > moria_cfg->max_rsp_model_diff_sigma * qq_model_scatter))
			{
				ret = RET_EPOCH_RESULT_INVALID;
				break;
			}
		} else
		{
			qq_out[j] = -1.0;
		}
	}

	return ret;
}
