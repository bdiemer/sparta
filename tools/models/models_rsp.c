/*************************************************************************************************
 *
 * This unit implements fitting functions for Rsp, Msp, and Delta_sp, as well as scatter.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "models_rsp.h"
#include "../../src/global.h"

/*************************************************************************************************
 * FORWARD DEFINITIONS
 *************************************************************************************************/

float modelDiemer20RM(HaloDefinition *def, float Gamma, float nu200m, float Om);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void modelDiemer20CheckGamma(float Gamma)
{
	if (Gamma < RSPMODEL_DIEMER20_ACCRATE_MIN)
		error(__FFL__,
				"The value of Gamma (%.2e) is smaller than the allowed range (%.1f .. %.1f).\n",
				Gamma, RSPMODEL_DIEMER20_ACCRATE_MIN, RSPMODEL_DIEMER20_ACCRATE_MAX);
	if (Gamma > RSPMODEL_DIEMER20_ACCRATE_MAX)
		error(__FFL__,
				"The value of Gamma (%.2e) is larger than the allowed range (%.1f .. %.1f).\n",
				Gamma, RSPMODEL_DIEMER20_ACCRATE_MIN, RSPMODEL_DIEMER20_ACCRATE_MAX);
}

/*
 * Wrapper function around the R/M and scatter functions.
 */
float modelDiemer20Splashback(HaloDefinition *def, float Gamma, float nu200m, float Om)
{
	if (def->type != HDEF_TYPE_SPLASHBACK)
	{
		error(__FFL__,
				"Can only output Diemer+17 model for splashback definitions, found type %d.\n",
				def->type);
	}

	if (def->is_error == HDEF_ERR_NO)
	{
		return modelDiemer20RM(def, Gamma, nu200m, Om);
	} else if (def->is_error == HDEF_ERR_1SIGMA)
	{
		return modelDiemer20Scatter(def, Gamma, nu200m);
	} else
	{
		error(__FFL__, "Unknown error type %d, expected NO or 1SIGMA.\n", def->is_error);
	}

	return 0.0;
}

float modelDiemer20RM(HaloDefinition *def, float Gamma, float nu200m, float Om)
{
	float a0, b0, b_om, b_nu, c0, c_om, c_nu, c_om2, c_nu2, a0_p, b0_p, b_om_p, b_om_p2, b_nu_p,
			c_om_p, c_om_p2, c_om2_p, c_om2_p2, c_nu_p, c_nu2_p, p, A0, B0, B_om, B_nu, C0, C_om,
			C_om2, C_nu, C_nu2, A, B, C, ret;

	if (def->is_error != HDEF_ERR_NO)
		error(__FFL__, "Cannot evaluate errors in R/M function.\n");

	a0 = 0.0;
	b0 = 0.0;
	b_om = 0.0;
	b_nu = 0.0;
	c0 = 0.0;
	c_om = 0.0;
	c_nu = 0.0;
	c_om2 = 0.0;
	c_nu2 = 0.0;
	a0_p = 0.0;
	b0_p = 0.0;
	b_om_p = 0.0;
	b_om_p2 = 0.0;
	b_nu_p = 0.0;
	c_om_p = 0.0;
	c_om_p2 = 0.0;
	c_om2_p = 0.0;
	c_om2_p2 = 0.0;
	c_nu_p = 0.0;
	c_nu2_p = 0.0;

	p = 0.0;

	if (def->subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN)
	{
		if (def->quantity == HDEF_Q_RADIUS)
		{
			a0 = 0.659745;
			b0 = 0.556171;
			b_om = 0.114053;
			b_nu = 0.069775;
			c0 = -0.850819;
			c_om = 18.446356;
			c_nu = -0.333245;
			c_om2 = -10.059591;
			c_nu2 = 0.047432;
		} else if (def->quantity == HDEF_Q_MASS)
		{
			a0 = 0.696229;
			b0 = 0.373627;
			b_om = 0.300490;
			b_nu = 0.000000;
			c0 = 3.344518;
			c_om = 1.371785;
			c_nu = -0.082529;
			c_om2 = 0.000000;
			c_nu2 = 0.000000;
		} else
		{
			error(__FFL__, "Unknown quantity, %d.", def->quantity);
		}
	} else if (def->subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE)
	{
		if ((def->percentile < RSPDEF_MIN_PERC) || (def->percentile > RSPDEF_MAX_PERC))
		{
			error(__FFL__, "Found percentile %.1f, not valid. Must be between %.1f and %.1f.\n",
					def->percentile,
					RSPDEF_MIN_PERC, RSPDEF_MAX_PERC);
		}
		p = def->percentile * 0.01;

		if (def->quantity == HDEF_Q_RADIUS)
		{
			a0 = 0.307107;
			b0 = 0.250844;
			b_om = 0.152731;
			b_nu = 0.195627;
			c0 = -1.221448;
			c_om = 17.537448;
			c_nu = 0.000000;
			c_om2 = -10.315817;
			c_nu2 = -0.018938;
			a0_p = 0.642779;
			b0_p = 0.507395;
			b_om_p = 0.000000;
			b_om_p2 = 0.000000;
			b_nu_p = -0.212788;
			c_om_p = 0.002358;
			c_om_p2 = 9.711469;
			c_om2_p = -0.000550;
			c_om2_p2 = 10.762591;
			c_nu_p = -0.473487;
			c_nu2_p = 0.094019;
		} else if (def->quantity == HDEF_Q_MASS)
		{
			a0 = 0.287428;
			b0 = 0.661551;
			b_om = 0.132142;
			b_nu = 0.000000;
			c0 = 4.591265;
			c_om = 3.092819;
			c_nu = -0.115458;
			c_om2 = 0.000000;
			c_nu2 = 0.000000;
			a0_p = 0.822820;
			b0_p = -0.656698;
			b_om_p = 0.003182;
			b_om_p2 = 4.953590;
			b_nu_p = 0.288168;
			c_om_p = -0.660871;
			c_om_p2 = -1.105000;
			c_om2_p = 0.000000;
			c_om2_p2 = 0.000000;
			c_nu_p = -0.376065;
			c_nu2_p = 0.078376;
		} else
		{
			error(__FFL__, "Unknown quantity, %d.", def->quantity);
		}
	} else
	{
		error(__FFL__, "Unknown type, %d.", def->type);
	}

	modelDiemer20CheckGamma(Gamma);

	A0 = a0 + p * a0_p;
	B0 = b0 + p * b0_p;
	B_om = b_om + b_om_p * exp(p * b_om_p2);
	B_nu = b_nu + p * b_nu_p;
	C0 = c0;
	C_om = c_om + c_om_p * exp(p * c_om_p2);
	C_om2 = c_om2 + c_om2_p * exp(p * c_om2_p2);
	C_nu = c_nu + p * c_nu_p;
	C_nu2 = c_nu2 + p * c_nu2_p;

	A = A0;
	B = (B0 + B_om * Om) * (1.0 + B_nu * nu200m);
	C = (C0 + C_om * Om + C_om2 * Om * Om) * (1.0 + C_nu * nu200m + C_nu2 * nu200m * nu200m);
	C = fmax(C, 1E-4);

	ret = A + B * exp(-Gamma / C);

	return ret;
}

/*
 * The scatter model is not valid at some extreme peak heights etc. For simplicity, the
 * model never returns a scatter of less than 0.02 dex, the lowest scatter measured in any halo
 * sample.
 */
float modelDiemer20Scatter(HaloDefinition *def, float Gamma, float nu200m)
{
	float sigma_0, sigma_Gamma, sigma_nu, sigma_p, p, ret;

	if (def->is_error != HDEF_ERR_1SIGMA)
		error(__FFL__, "Can only evaluate 1-sigma scatter, found error code %d.\n", def->is_error);

	sigma_0 = 0.0;
	sigma_Gamma = 0.0;
	sigma_nu = 0.0;
	sigma_p = 0.0;
	p = 0.0;

	if (def->subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN)
	{
		if (def->quantity == HDEF_Q_RADIUS)
		{
			sigma_0 = 0.050135;
			sigma_Gamma = 0.003548;
			sigma_nu = -0.010766;
		} else if (def->quantity == HDEF_Q_MASS)
		{
			sigma_0 = 0.045588;
			sigma_Gamma = 0.001679;
			sigma_nu = -0.007945;
		} else
		{
			error(__FFL__, "Unknown quantity, %d.", def->quantity);
		}
	} else if (def->subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE)
	{
		if ((def->percentile < RSPDEF_MIN_PERC) || (def->percentile > RSPDEF_MAX_PERC))
		{
			error(__FFL__, "Found percentile %.1f, not valid. Must be between %.1f and %.1f.\n",
					def->percentile,
					RSPDEF_MIN_PERC, RSPDEF_MAX_PERC);
		}
		p = def->percentile * 0.01;

		if (def->quantity == HDEF_Q_RADIUS)
		{
			sigma_0 = 0.041872;
			sigma_Gamma = 0.004279;
			sigma_nu = -0.014068;
			sigma_p = 0.023470;
		} else if (def->quantity == HDEF_Q_MASS)
		{
			sigma_0 = 0.022368;
			sigma_Gamma = 0.000916;
			sigma_nu = -0.009101;
			sigma_p = 0.045386;
		} else
		{
			error(__FFL__, "Unknown quantity, %d.", def->quantity);
		}
	} else
	{
		error(__FFL__, "Unknown type, %d.", def->type);
	}

	modelDiemer20CheckGamma(Gamma);
	ret = sigma_0 + sigma_Gamma * Gamma + sigma_nu * nu200m + sigma_p * p;
	ret = fmaxf(ret, 0.02);

	return ret;
}

float modelDiemer20Gamma(float nu200m, float z)
{
	float a0 = 1.172092;
	float a1 = 0.325463;
	float b0 = -0.256528;
	float b1 = 0.093206;
	float b2 = -0.057122;
	float b3 = 0.004207;

	float A, B, Gamma;

	A = a0 + a1 * z;
	B = b0 + b1 * z + b2 * z * z + b3 * z * z * z;
	Gamma = A * nu200m + B * pow(nu200m, 1.5);

	return Gamma;
}
