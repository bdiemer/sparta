/*************************************************************************************************
 *
 * This unit implements fitting functions for Rsp, Msp, and Delta_sp, as well as scatter.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MODELS_RSP_H_
#define _MODELS_RSP_H_

#include "../../src/global.h"
#include "../../src/halos/halo_definitions.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Percentile definitions for the Diemer et al. 2020 model.
 *
 * The model was calibrated up to the 90th percentile.
 */
#define RSPDEF_MIN_PERC 50.0
#define RSPDEF_MAX_PERC 90.0

/*
 * The allowed range of mass accretion rates for the Diemer et al. 2020 model. If a mass
 * accretion rate outside of this range is passed, an error is thrown. For higher mass accretion
 * rates, they can safely be set to the maximum because the model predictions are flat at very
 * high Gamma. Negative mass accretion rates typically indicate disrupting halos, where the
 * predictions are hard to make sense of.
 */
#define RSPMODEL_DIEMER20_ACCRATE_MIN 0.0
#define RSPMODEL_DIEMER20_ACCRATE_MAX 12.0

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

float modelDiemer20Splashback(HaloDefinition *def, float Gamma, float nu200m, float Om);
float modelDiemer20Scatter(HaloDefinition *def, float Gamma, float nu200m);
float modelDiemer20Gamma(float nu200m, float z);

#endif
