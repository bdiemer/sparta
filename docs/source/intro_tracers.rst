*******
Tracers
*******

A dynamical tracer, hereafter simply "tracer", is defined as an object that follows some orbit 
with respect to a halo center (a "trajectory"). 
Tracers can be part of multiple halos at the same time because each halo keeps a separate, 
dynamically allocated array of tracers of each type. Currently, two tracers are implemented in
SPARTA, particles and subhalos, which carry the following names and abbreviations throughout the
code and output:

+--------------------------+-------------------+-------------------+ 
| Tracer type              | Long name         | Abbreviation      |
+==========================+===================+===================+
| Particles                | ``particles``     | ``ptl``           |
+--------------------------+-------------------+-------------------+ 
| Subhalos                 | ``subhalos``      | ``sho``           |
+--------------------------+-------------------+-------------------+ 

For example, a directory for tracer particles might be called ``tcr_ptl`` or ``tcr_sho``.

================================
Particle tracers in host halos
================================

The goal of SPARTA is to consider as complete as possible a set of particles in a halo. The full
particle tracer logic is somewhat complicated and depends on whether a halo is considered a host
or subhalo. Here, we discuss the basic logic of particle tracers as shown in the schematics below.

|

.. image:: ../images/sparta_tracers.jpeg
    :scale: 50 %
    :align: center

|

Particle tracers are created whenever a particle first comes within r\ :sub:`create` of a halo's 
center (typically about 2 R\ :sub:`200m`), and SPARTA follows its trajectory from that point onward 
(see :doc:`run_config` for how to set r\ :sub:`create`). Due to the enormous number of particles in 
a simulation, storing the full trajectories of all tracers in memory is impossible. Thus, SPARTA 
keeps only a certain number of snapshots depending on the analyses to be performed. The particle 
properties that are stored are adjusted depending on the chosen output variables (for example, 
radius and radial velocity rather than the full three-dimensional position, unless the latter is 
necessary for a particular purpose). At each snapshot, all active tracer trajectories are analyzed 
for certain events, including first infall
into the halo (crossing R\ :sub:`200m`, hereafter "infall"), reaching the apocenter of its first 
orbit (hereafter "splashback"), and so on (see :doc:`intro_rs_al`). A particle tracer's life can 
end for a number of reasons:

* if all analysess of its trajectory have finished (e.g., when the search for both an infall 
  and a splashback event has succeeded or failed)
* if it strays too far from the halo to be considered a dynamical tracer of the halo potential 
  (r > r\ :sub:`delete`, typically about 3 R\ :sub:`200m`)
* if the halo becomes a subhalo for more than one snapshot (though that depends, see section
  on subhalos below; halos that are only a subhalo for one snapshot are considered not to have
  truly become a subhalo, at least not yet)
* if the halo itself ceases to exist according to the catalog. 

If any of those occur, the tracer object is deleted from memory, and only its results remain. 
If the deleted tracer had entered within R\ :sub:`200m` of the halo, we add its ID to a list of 
tracers to be ignored in the future. Such a list is kept by each halo for each tracer type and 
checked before creating new tracers to avoid accidentally treating a returning tracer as 
infalling for the first time. This logic leads to many, many possible scenarios for the life of
a particle tracer. Some of the most common are sketched in the figure above:

1. A tracer enters into the creation radius and is created, but then strays far from the halo and is
   deleted again. No results have been recorded, and the tracer is not added to the ignore list as
   it is not deemed to ever have entered the halo.
2. A tracer enters into the halo, all chosen results (say, infall and splashback) are completed. The
   tracer is deleted at that point and added to the ignore list.
3. A tracer enters the halo, leaves, is put on the ignore list, but returns. Depending on the 
   results chosen, the tracer may be recreated (e.g., to count its orbits). However, other results
   such as splashback will be turned off because the tracer is not on a first orbit.
4. A particle enters as part of a second halo (yellow). It exists as a tracer in both halos after it
   enters the creation radius. When the second halo becomes a subhalo (according to the catalog), we
   determine the particles that truly belong to this subhalo (see section below), identify their 
   counterparts in the  host halo, and tag them as having originated from a subhalo. This is 
   important for certain analyses where we might not want to include tracer particles from massive
   subhalos.

Note that we cannot set r\ :sub:`delete` to arbitrarily high values because particle tracers often 
dominate SPARTA's memory consumption, and because a large search radius would force each process 
to consider a large fraction of the simulation box.

================================
Particle tracers in subhalos
================================

The situation in subhalos is fundamentally different from host halos. The motion of many of a 
subhalo's particles about the subhalo center will make no sense dynamically, as the particle does 
executes a trajectory in the combined potential of host and sub. Similarly, if seen from the 
perspective of the host halo, tracers will also not follow normal orbits. Overall, the interaction
between host and sub can lead to vastly disparate outcomes, some of which can be very misleading 
when interpreted as normal host halo particles.

|

.. image:: ../images/sparta_tracers_sub.jpeg
    :scale: 50 %
    :align: center

The schematic above shows three typical cases of what can happen to particles that enter a host as
part of a subhalo, although there are numerous other strange possibilties. In particular:

1. A subhalo particle happens to be close to pericenter (i.e., close to the host's center) during
   first orbit, and is quickly stripped from the subhalo.
2. A subhalo particle is retained by the subhalo and roughly follows it's orbit (shows as a yellow
   line).
3. A subhalo particle is relatively weakly bound to the subhalo, is stripped from it, and executes
   a relatively normal orbit about the host halo.

Because of these possibilities, we tag host tracer particles as having originated from a subhalo
at infall (see next section for how this decision is made). 
The particle tracers that are identified as subhalo particles are kept within the subhalo, or
created if they do not yet exist for some reason. All other particle tracers are deleted. While a 
halo is a subhalo, no new tracers can be added; the idea is that many host particles will 
accidentally stray into the subhalo, but that a subhalo can only lose mass to its host. We track
the subhalo tracer particles until they reach a deletion radius set by the user (which is 
similar to the deletion radius around host halos, but refers to the radius of the subhalo when it
entered the host). If the subhalo becomes a host halo again (i.e., if it is a "backsplash halo"), 
we begin adding new tracers once again. 

=============================================
Which particles belong to a subhalo?
=============================================

As mentioned above, we need to decide which particles belong to a subhalo in order to track them.
Membership in a subhalo is also important for other reasons. For example, subhalos suffer from 
dynamical friction, meaning their orbit is not the same as a particle's orbit with the same initial
conditions (this is important for the :doc:`run_al_rsp`). Thus, we wish to tag tracer particles in 
host halos with a sub-to-host mass ratio (SMR) if they fell in as part of a subhalo.

.. image:: ../images/sparta_tracers_subtagging.jpeg
    :scale: 50 %
    :align: center

Either way, how can we decide which particles are in a subhalo? To understand why this is a really
tricky question, consider the particle orbits shown in the schematic above. Here, the yellow 
subhalo falls into the red host halo. At the point where it is first considered a subhalo, its 
center has just penetrated into the radius of the host, by definition. 

The particle orbits demonstrate
a few common cases, though there are many, many more possibilities. Particle (1) truly belongs to
the subhalo: it fell into the sub long time ago and then into the host as part of the subhalo.
For particle (2), however, the situation is much less clear: it fell into the subhalo before 
falling into the host halo, but perhaps that happened by chance - it was not part of the subhalo
for most of its life. Particle (3) clearly does not belong to the subhalo, but that is not obvious
because it happens to fall into the host at a similar time and then into the subhalo. Finally,
particle (4) fell into the host long time ago and has already orbited, but nevertheless ends up
within the radius of the subhalo at infall. 

To distinguish these possibilities in a phsyically meaningful way, SPARTA offers a number of 
algorithms which can be combined as chosen by the user at compile-time 
(see :doc:`run_compile`), including:

* All particles that have been part of the halo for a particular time in units of the dynamical
  time (see :doc:`intro_conventions_mar`). Here, SPARTA would measure the time between the infall
  into the subhalo (blue dots) and the time when the subhalo falls into the host. If this time is
  longer than a certain number of dynamical times (see :doc:`run_config`), the particle is deemed 
  to be part of the subhalo. This algorithm works well for most particles, but might erroneously
  tag particles in subhalos with tangential (or other strange) orbits, where the subhalo might be
  close to the host, add host particles, and take a long time to actually be considered a subhalo.
  In that case, the particles swept up by the subhalo might be tagged.
* Particles that joined the subhalo a certain distance from the host halo. Here, we would compare 
  the distance of the infall into the subhalo (blue dots) from the host halo at that time. If the
  infall happened a number of host halo radii away from the host, we conclude that the particle
  was, indeed, part of the subhalo before it became part of the host. This algorithm is perhaps the
  most reliable, but has the slight disadvantage that it demands position information in all
  :doc:`run_rs_ifl`, i.e., that SPARTA needs to record extra information about particles falling 
  into halos.
* Gravitationally bound particles. We include the particles within a certain fraction of the
  subhalo radius at infall, and require that their gravitational potential (only from the 
  included particles!) is greater than their kinetic energy relative to the subhalo center by some
  factor. The included radius matters greatly; if it is too large, a lot of host material can be
  included, resulting in host particles being "bound" to the subhalo. Conversely, if the radius is
  too small, there may not be enough gravitational potential to bind almost any particles.

Once we have determined subhalo membership, we tag the host's tracer particles with the sub-to-host
mass ratio. However, we require that the host particle fell in less than some factor times the
dynamical time ago in order to avoid tagging particles that somehow were part of both sub and host,
but that had really lived in the host for a long time.

================================
Subhalo tracers 
================================

Subhalo tracers are treated in much the same way as particles, except that their positions and 
velocities are determined by the halo finder. Subhalo tracers are created whenever a new subhalo 
is added to a host, but are not deleted if the subhalo leaves the host. Instead, the trajectory 
of the (former) subhalo is traced until the halo merges away or the simulation ends. Thus, subhalo 
tracers are allowed to stray far away from their previous host, which poses no performance problem 
as it does not change the simulation volume for which particles have to be loaded (the halo catalog 
is loaded in its entirety anyway). 

Subhalos should not be seen as unbiased dynamical tracers such as particles, chiefly because they
suffer from dynamical friction. This is one of the main reasons why it is important to tag 
subhalo particles: their orbits have a tendency to move closer to the host halo center. This effect
becomes stronger with sub-to-host mass ratio. Thus, most analyses implemented in SPARTA do not rely
on results from subhalo tracers.

When a subhalo becomes a ghost, we stop tracking it as a subhalo tracer. The reason for this 
behavior is mostly technical (ghost's center and velocity are computed after the subhalo tracers 
may be analyzed, meaning they may be undefined at that time). In principle, the tracking of ghost
subhalo tracers could be implemented though.
