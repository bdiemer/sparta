***************************************************************************************************
Splashback results
***************************************************************************************************

The splashback result routines measure the time and location of a tracer's first apocenter after
infall into a halo. The main point of the splashback results is that they can be used in the 
:doc:`run_al_rsp` to determine a halo's slashback radius and mass.

.. rubric:: Algorithm

The splashback measurement can be somewhat tricky because we can only keep a small
fraction of a trajectory in memory, and because the trajectories may be noisy. For simplicity, we
ignore all angular information and consider only the tracer's radius and radial velocity. We 
consider the four last snapshots in time. 

Before we can look for apocenter, we need to establish pericenter, i.e., the first passage of the
tracer through the host halo. This event is much clearer in the radial velocity than in radius, as
the radial velocity must switch from negative to positive. We look for this upwards crossing of 
zero velocity and require that it is robust in the sense that the first two time bins have negative
velocity and the second two positive velocity. If the trajectory is ambiguous, for example if v
becomes positive for only one snapshot, we record the number of such invalid switches. If the 
number exceeds one, we abort the trajectory as it is likely under-sampled in time (that is, the
orbital time is shorter than the snapshot spacing). When we find a pericener, we interpolate the
radial trajectory in time to find the radius of closest approach.

Once we have found a pericenter, we begin looking for an apocenter. Again, we search for a zero-
crossing in the radial velocity, but this time from positive to negative. We again require 
the first two bins to have positive v and the second two negative, otherwise we abort. We then
linearly interpolate the trajectory to find the time and radius of the apocenter.

Finally, we compute the mass enclosed within the splashback event by interpolating the halo's mass
profile in time and radius. This algorithm identifies a splashback event for about 90% of the 
particle trajectories, about 85% of subhalo trajectories (see 
`Diemer 2017 <https://ui.adsabs.harvard.edu//#abs/2017ApJS..231....5D/abstract>`_ for details).

.. rubric:: Compile-time parameters

+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| Parameter                                          | Explanation                                                                                              |
+====================================================+==========================================================================================================+ 
| ``OUTPUT_RESULT_SPLASHBACK``                       | Write splashback results to output file                                                                  |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_SPLASHBACK_MSP``                   | Save the splashback mass as well as the radius in output file                                            |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_SPLASHBACK_RRM``                   | Save the pericenter radius (closest approach to halo center in units of R200m) of the tracer             |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_SPLASHBACK_POS``                   | Save the 3-dimensional coordinates (rather than just radius) of the splashback event in output file      |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 

.. rubric:: Run-time parameters

This result does not add any config parameters.

.. rubric:: Output fields

+--------------------------+----------+----------------------------+-----------------------------------+------------------------------------------------------------------------------------+ 
| Field                    | Type     | Dimensions                 | Exists if                         | Explanation                                                                        |
+==========================+==========+============================+===================================+====================================================================================+ 
| ``halo_first``           | int64    | ``n_halos``                | Always                            | The index of the first result for each halo (or -1 if none exists for a halo).     |
+--------------------------+----------+----------------------------+-----------------------------------+------------------------------------------------------------------------------------+ 
| ``halo_n``               | int32    | ``n_halos``                | Always                            | The number of results of this type for each halo (can be 0).                       |
+--------------------------+----------+----------------------------+-----------------------------------+------------------------------------------------------------------------------------+ 
| ``tracer_id``            | int64    | ``n_rs_splashback``        | Always                            | The ID of the tracer to which this result refers.                                  |
+--------------------------+----------+----------------------------+-----------------------------------+------------------------------------------------------------------------------------+ 
| ``tsp``                  | float    | ``n_rs_splashback``        | Always                            | The time of splashback in Gyr since the Big Bang                                   |
+--------------------------+----------+----------------------------+-----------------------------------+------------------------------------------------------------------------------------+ 
| ``rsp``                  | float    | ``n_rs_splashback``        | Always                            | The apocenter (splashback) radius of the tracer in physical kpc/h                  |
+--------------------------+----------+----------------------------+-----------------------------------+------------------------------------------------------------------------------------+ 
| ``msp``                  | float    | ``n_rs_splashback``        | ``OUTPUT_RESULT_SPLASHBACK_MSP``  | The mass enclosed within the apocenter radius in Msun/h                            |
+--------------------------+----------+----------------------------+-----------------------------------+------------------------------------------------------------------------------------+ 
| ``rrm``                  | float    | ``n_rs_splashback``        | ``OUTPUT_RESULT_SPLASHBACK_RRM``  | rmin/R200(tmin), the radius at pericenter in units of R200m                        |
+--------------------------+----------+----------------------------+-----------------------------------+------------------------------------------------------------------------------------+ 
| ``theta``                | float    | ``n_rs_splashback``        | ``OUTPUT_RESULT_SPLASHBACK_POS``  | First angular coordinate of splashback event (see below)                           |
+--------------------------+----------+----------------------------+-----------------------------------+------------------------------------------------------------------------------------+ 
| ``phi``                  | float    | ``n_rs_splashback``        | ``OUTPUT_RESULT_SPLASHBACK_POS``  | Second angular coordinate of splashback event (see below)                          |
+--------------------------+----------+----------------------------+-----------------------------------+------------------------------------------------------------------------------------+ 

The polar coordinates ``theta`` and ``phi`` are given in the convention where

.. math::
    x = r \times sin(\theta) cos(\phi) \\
    y = r \times sin(\theta) sin(\phi) \\
    z = r \times cos(\theta)
    