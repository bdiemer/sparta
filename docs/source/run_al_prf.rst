***************************************************************************************************
Density profile analysis
***************************************************************************************************

The density profile analysis outputs the mass enclosed within a set of radial bins at a 
user-defined number of redshifts. One special feature of SPARTA's profile analysis is that it uses
:doc:`run_rs_oct` to separate the profiles of particles on a first infall trajectory and those that 
are orbiting in the halo (the so-called 1-halo term). 

.. rubric:: Algorithm

The basic algorithm is simple: we create a set of logarithmically spaced radial bins between the limits
chosen by the user and compute the mass profile in these bins. However, things get more complicated
when we split the particle population by their dynamics. Here, we search for :doc:`run_rs_oct` 
which should exist for all particle tracers. If the pericenter count is at least one, the particle is
part of the 1-halo term, the material orbiting inside the halo. In some cases, the orbit count is a
lower limit because the particle was already in the halo when the halo was first detected by the
halo finder. We include such particles in the 1-halo term, as they are extremely likely to have 
already had at least one pericenter.

Density profiles are computed in the same way for subhalos and even ghosts. Here, R200m is 
typically smaller than the all-particle R200m because it is measured from subhalo tracers only. 
Thus, the profile may not actually reach a density of 200m anywhere. The orbit counter keeps 
running for subhalo tracers, meaning that the 1-halo term has the same meaning in principle. 
However, in subhalos (and especially in ghosts) we expect the tracer trajectories to be messy and
the orbit counting to be unreliable.

In ghost halos, the 1-halo term of the ghost is also computed from the particles' orbit counts, 
but as the tracers have typically orbited at least once, this 1-halo term is almost identical to a
profile of the ghost tracers. Since the orbit counter is not increased any more in ghosts, this
profile should be interpreted with care.

.. rubric:: Compile-time parameters

+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| Parameter                                          | Explanation                                                                                              |
+====================================================+==========================================================================================================+ 
| ``OUTPUT_ANALYSIS_PROFILES``                       | Write profile analyses to output file                                                                    |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_ANALYSIS_PROFILES_ALL``                   | Write the density profiles including all particles around a halo                                         |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_ANALYSIS_PROFILES_1HALO``                 | Write the density profiles of the 1-halo term, that is, only including particles that have orbited       |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``ANALYSIS_PROFILES_MAX_SNAPS``                    | The maximum number of snapshots that can be requested by the user                                        |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``ANALYSIS_PROFILES_N_BINS``                       | The number of radial bins in the profiles                                                                |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 

.. rubric:: Run-time parameters

+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| Parameter                        | Type    | Default     | Explanation                                                                            |
+==================================+=========+=============+========================================================================================+ 
| ``anl_prf_redshifts``            | [float] | -1          | Redshifts where profiles are output; list of floats, or -1 for all                     |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_prf_rmin``                 | float   | 0.01        | Minimum radius of bins in units of R200m                                               |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_prf_rmax``                 | float   | 3.0         | Maximum radius of bins in units of R200m                                               |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_prf_do_subs``              | bool    | ``TRUE``    | Output profiles for subhalos (they will be influenced by the host)                     |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_prf_do_ghosts``            | bool    | ``TRUE``    | Output profiles for ghost halos (they will be influenced by the host)                  |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 

Note that the number of profile redshifts must be smaller or equal than the ``ANALYSIS_PROFILES_MAX_SNAPS``
define. It is recommended to give a list of redshifts rather than outputting profiles at all
snapshots because the latter can lead to large output and memory consumption.

.. note::
    Profiles of the 1-halo term at the final snapshot of a simulation are slightly biased low 
    because pericenter events are recorded one snapshot after they occur. SPARTA then corrects
    profiles at the previous snapshot with those events, but that is not possible if the profile
    is output at the final snapshot. Thus, it is recommended to output profiles at the 
    second-to-last snapshot instead.

.. rubric:: Output fields

Here, ``n_prf_snp`` is the number of snapshots at which profiles are output, determined by the 
``anl_prf_redshifts`` parameter listed above. ``n_bins`` is the number of radial bins which is 
given by the ``ANALYSIS_PROFILES_N_BINS`` define.

+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| Field                    | Type     | Dimensions                                        | Exists if                           | Explanation                                                                        |
+==========================+==========+===================================================+=====================================+====================================================================================+ 
| ``halo_first``           | int64    | ``n_halos``                                       | Always                              | The index of the first analysis for each halo (or -1 if none exists for a halo).   |
+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``halo_n``               | int32    | ``n_halos``                                       | Always                              | The number of analyses of this type for each halo (can be 0).                      |
+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``halo_id``              | int64    | ``n_al_profiles``                                 | Always                              | The (original, first-snapshot) halo ID to which this analysis refers.              |
+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``status``               | int8     | ``n_al_profiles`` * ``n_prf_snp``                 | Always                              | A status field that indicates if the profile analysis was successful (see below)   |
+--------------------------+----------+----------------------------+------------------------------------------------------------+------------------------------------------------------------------------------------+ 
| ``M_all``                | float    | ``n_al_profiles`` * ``n_prf_snp`` * ``n_bins``    | ``OUTPUT_ANALYSIS_PROFILES_ALL``    | The total density profile (including all particles, mass enclosed within r bins).  |
+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``M_1halo``              | float    | ``n_al_profiles`` * ``n_prf_snp`` * ``n_bins``    | ``OUTPUT_ANALYSIS_PROFILES_1HALO``  | The mass of the 1-halo term (ptls. that have undergone at least one pericenter)    |
+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 

The ``status`` field can take on the following values:

+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| Value | Parameter                             | Explanation                                                                                              |
+=======+=======================================+==========================================================================================================+ 
| 0     | ``ANL_PRF_STATUS_UNDEFINED``          | Placeholder, should never occur in output file                                                           |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 1     | ``ANL_PRF_STATUS_SUCCESS``            | The analysis succeeded, all output values can be used                                                    |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 2     | ``ANL_PRF_STATUS_HALO_NOT_VALID``     | Halo could not be analyzed at this snapshot, e.g. because too young                                      |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 5     | ``ANL_PRF_STATUS_SEARCH_RADIUS``      | The search radius had to be reduced, the profile was not computed                                        |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 6     | ``ANL_PRF_STATUS_CATALOG_RADIUS``     | R200m_all could not be computed, the radius was taken from catalog; the profile was not computed         |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 

The ``ANL_PRF_STATUS_SEARCH_RADIUS`` error reflects the rare case where SPARTA underestimates a 
halo radius, leading to too small a particle box. The particles can then not be loaded after the 
fact as that would mean re-doing a large fraction of SPARTA's work on the snapshot. As particles 
were almost certainly missed, we do not compute a profile in this case.

The ``ANL_PRF_STATUS_CATALOG_RADIUS`` error means that R200_all was not computed. This is a rare
case where the halo is next to a (typically much larger) halo that dominates its density field. 
While the bound-only mass from the catalog may be small, R200m_all would include the entirety of
the larger halo. In this case, SPARTA may fall back to the catalog radius (see the 
``halo_max_radius_ratio_cat`` configuration parameter).
