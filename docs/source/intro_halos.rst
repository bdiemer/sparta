******************
Halos & Subhalos
******************

Halos serve as the top-level organizational unit for work and memory in SPARTA. Each halo is 
uniquely assigned to one process, and halos can be exchanged between processes. A halo object 
contains dynamical lists of :doc:`intro_tracers` for each tracer type, as well as 
:doc:`intro_rs_al`.

------------------------------
Halo and subhalo tracking
------------------------------

SPARTA is not a halo finder, it takes its list of halos from an existing halo catalog (the most 
well-tested halo finder for this purpose is Peter Behroozi's 
`ROCKSTAR <https://bitbucket.org/gfcstanford/rockstar/src/master/>`_ halo finder and his 
`consistent-trees <https://bitbucket.org/pbehroozi/consistent-trees/src/master/>`_ merger trees 
code). Based on those catalogs, SPARTA keeps track of all halos in a simulation.

At each snapshot, SPARTA connects halos to their descendants by matching their unique IDs, saving 
certain halo properties, and updating others. SPARTA explicitly tracks the relation between host 
and subhalos as defined by the merger trees (or catalogs). Internally, SPARTA assigns each subhalo
to its top-level host, meaning that if there are subhalos within subhalos, their parent ID is that 
of the highest-level host. The schematic below demonstrates some of the most common host-sub 
relations and their evolution.

|

.. image:: ../images/sparta_halos.jpeg
    :scale: 50 %
    :align: center

Red circles represent host halos, yellow circles subhalos. SPARTA would see this structure from top 
to bottom, going forward in time. At the end of each snapshot, each halo has a certain status which 
is written into the output file (see below). 

The halo radii computed by SPARTA (e.g., splashback or spherical overdensity radii) can be used to
reassign subhalos using MORIA (see :doc:`analysis_moria`), but internally, SPARTA treats a halo as
a subhalo if it is so defined in the halo catalog. Thus, the definition and algorithm used for 
subhalo assignment in the halo catalog matters a great deal.

------------------------------
Ghosts
------------------------------

The vast majority of halos end in one of two ways: because the simuation ends, or because they 
merge into another, larger halo. Some of this merging is numerical: subhalos tend to be
artificially disrupted in N-body simulations, and there is little we can do about that after the 
fact. However, the life of subhalos may also be cut short because they cannot be identified by the
halo finder any more as they are mixed with their host's particles. Halo finders such as Rockstar
combat this issue using techniques such as phase-space FOF, but even those algorithms may fail to
follow some small subhalos.

In SPARTA, we have access to the full particle information, and can track subhalos using the member
particles they had at infall (see :doc:`intro_tracers` for details on how those particles are 
selected and tracked). We call such halos "ghosts" as they are the remnants of a halo that does not
exist in the halo finder catalogs any longer. If the user chooses to output ghosts, SPARTA follows
them and outputs them much like normal halos. A few common scenarios are shown in this schematic:

|

.. image:: ../images/sparta_halos_ghosts.jpeg
    :scale: 50 %
    :align: center

The dashed yellow circles denote ghosts. As they are not present in the original halo catalog, they 
are assigned an ID that reflects the ghost's original ID and the snapshot number (white/gray 
labels). The host-subhalo relations are a little more complicated than for normal halos, including
the following scenarios:

* When a subhalo is converted into a ghost, the latter maintains the same host halo.
* Unlike normal halos, ghosts can be subhalos of other subhalos. The main reason is that the halo
  finder is not keeping track of the ghosts for us. Thus, if a ghost's host is temporarily a 
  subhalo but than becomes a host again, we would lose that "true" connection if we were to assign
  it to the top-level host.
* When the host halo of a ghost merges into another halo and disappears from the catalog, we 
  transfer the ghost to that new host.
* When the host halo of a ghost becomes a ghost itself, we transfer the first ghost to the new
  ghost's subhalo, thus avoiding ghost-ghost relations.

From the particles we are tracking in a ghost, we compute its center as the location of the 
particle with the highest binding energy given all tracked particles, and its velocity as the 
average velocity with R\ :sub:`200m` (see below for details on how the radius is computed).

There are, fundamentally, two ways for a ghost to end: it either reaches the last snapshot like 
other halos, or it dissolves to the point where it cannot be identified any longer. The user can 
set parameters for these criteria (see :doc:`run_config`), namely:

* a minimum number of particles at which we stop tracking the ghost. 
* a minimum distance from the host center and a maximum time for which this distance is tolerated.
  If the ghost is close to the host center for longer, we consider it to have merged with the host.
  We also consider the positional uncertainty on the ghost center in this calculation.

The final status (see below) indicates exactly what happened to the ghost.

------------------------------
Halo status fields
------------------------------

The following values are possible:

+--------+------------------------+------------------------------------------------------------------+
| Value  | Meaning                | Explanation                                                      |
+========+========================+==================================================================+
| ``-2`` | ``NOT_FOUND``          | Halo not found in SPARTA file (use in tools such as MORIA)       |
+--------+------------------------+------------------------------------------------------------------+
| ``-1`` | ``NONE``               | Halo did not exist at this snapshot                              |
+--------+------------------------+------------------------------------------------------------------+
| ``10`` | ``HOST``               | Host halo                                                        |
+--------+------------------------+------------------------------------------------------------------+
| ``20`` | ``SUB``                | Subhalo                                                          |
+--------+------------------------+------------------------------------------------------------------+
| ``21`` | ``BECOMING_SUB``       | Subhalo; became a subhalo in this snap                           |
+--------+------------------------+------------------------------------------------------------------+
| ``22`` | ``BECOMING_HOST``      | Subhalo; will become a host in the next snap                     |
+--------+------------------------+------------------------------------------------------------------+
| ``23`` | ``BECOMING_SUB_HOST``  | Subhalo; became sub this snap and will be host in the next       |
+--------+------------------------+------------------------------------------------------------------+
| ``24`` | ``SWITCHED_HOST``      | Subhalo; switched host                                           |
+--------+------------------------+------------------------------------------------------------------+
| ``30`` | ``GHOST_HOST``         | Ghost host halo                                                  |
+--------+------------------------+------------------------------------------------------------------+
| ``31`` | ``GHOST_SUB``          | Ghost subhalo                                                    |
+--------+------------------------+------------------------------------------------------------------+
| ``32`` | ``GHOST_SWITCHING``    | Ghost subhalo, changing host                                     |
+--------+------------------------+------------------------------------------------------------------+

When the trajectory of a halo ends, it is assigned a ``final_status`` value. Some examples are shown 
as gray boxes in the schematic above. The values can be:

+--------+------------------------+--------------------------------------------------------------------------+
| Status | Meaning                | Explanation                                                              |
+========+========================+==========================================================================+
| ``50`` | ``MERGED``             | The halo merged with another, larger halo                                |
+--------+------------------------+--------------------------------------------------------------------------+
| ``51`` | ``GHOST_CENTER``       | The ghost's center coincided with the host's, meaning it merged          |
+--------+------------------------+--------------------------------------------------------------------------+
| ``52`` | ``GHOST_TOO_SMALL``    | The ghost was identified, but below the minimum number of particles      |
+--------+------------------------+--------------------------------------------------------------------------+
| ``53`` | ``GHOST_NOT_FOUND``    | The tracer particles were not found in the search volume; should be rare |
+--------+------------------------+--------------------------------------------------------------------------+
| ``54`` | ``GHOST_POSITION``     | The ghost's position could not clearly be identified from its tracers    |
+--------+------------------------+--------------------------------------------------------------------------+
| ``60`` | ``LAST_SNAP``          | The halo ended at the last snapshot of the simulation                    |
+--------+------------------------+--------------------------------------------------------------------------+
| ``70`` | ``NOT_FOUND``          | A descendant was not found in the catalog (should not happen)            |
+--------+------------------------+--------------------------------------------------------------------------+
| ``71`` | ``JUMP``               | The descendant in the catalog jumped unphysically (should not happen)    |
+--------+------------------------+--------------------------------------------------------------------------+
| ``72`` | ``HOST_ENDED``         | This halo was a sub of a host that ended                                 |
+--------+------------------------+--------------------------------------------------------------------------+
| ``73`` | ``SEARCH_RADIUS``      | Failed to find a valid particle distribution (should not happen)         |
+--------+------------------------+--------------------------------------------------------------------------+

If a halo catalog is constructed in a self-consistent manner, only ``MERGED`` and ``LAST_SNAP`` 
should occur, plus the ghost statuses if ghosts are on.

We note that SPARTA does not have to be started at the first snapshot, of a simulation but its 
intended mode of operation is to follow entire halo histories.

------------------------------
Internal halo masses and radii
------------------------------

For many purposes, SPARTA must internally rely on some definition of the halo radius, even if the
purpose of a SPARTA run is to determine other definitions of the halo boundary. The chosen 
definition for this internal boundary is R\ :sub:`200m`, but the exact details of how it is
computed differ for different types of halos. The chosen value of R\ :sub:`200m` is written to
the output file for all halos and times.

For host (isolated) halos, the halo catalog may already specify R\ :sub:`200m`, but SPARTA computes 
it directly by constructing the mass profile around a halo from the particle distribution. If the 
initial search radius is too small to find R\ :sub:`200m`, it is increased iteratively until a 
solution has been found. Here, R\ :sub:`200m` are computed from the full particle distribution, 
including bound and unbound particles, in order to avoid the ambiguities inherent in any 
unbinding procedure. For host halos, the difference between the bound and full mass profiles is 
generally small. 

For subhalos, however, the difference can be large because the density around a subhalo may never 
reach sufficiently low values, meaning that material from its host is included in the spherical 
overdensity R\ :sub:`200m` which leads to an unphysically high radius and mass. There are two 
solutions to this issue: we can either use an estimate of a bound-only R\ :sub:`200m` from the 
halo catalog, or we can compute it from only subhalo particles that we are tracking over time
(see :doc:`intro_tracers` for details on how this is done). For many operations, including
determining the particle search radius around subhalos and the radius out to which we are tracking
particles, we use the maximum of the tracer-based and catalog-based radii to be on the safe side,
but the tracer-based radius is output. 

If we are not tracking particles, we use the radius at infall for subhalos, that is, the last
measured R\ :sub:`200m` at the snapshot before the halo became a subhalo. This radius is less
arbitrary than halofinder-specific bound-only definitions. We note that we keep the radius constant
rather than the mass; this corresponds to a decreasing mass as the mean density of the universe
drops over cosmic time. If we kept the mass constant, the physical halo radius would increase
with time, which is unlikely to be a good representation of the physical evolution of subhalos.
However, if we are not tracking subhalo particles, we do not do much with subhalos at all and the
exact definition does not matter much.

For ghosts, we stay consistent with the subhalo definition described above and always use the 
tracer mass, that is R\ :sub:`200m` measured from those particles that we are tracking. The radius
is measured using the tracked-particle profile around the center, which is the location of the
particle with the highest binding energy. In summary, the internal R\ :sub:`200m` is computed in 
the following ways:

* Host halos: directly measured from the particle distribution, including all particles.
* Subhalos that are subs for only one snapshot: treated like host halos.
* Subhalos, if tracking subhalo particles is off: radius at infall, meaning the radius at the last
  snapshot when the halo was a host.
* Subhalos, if tracking subhalo particles is on: determined from tracer particles. If there are no 
  tracers, we use the catalog radius.
* Ghosts: determined from tracer particles. For subhalo phantoms, the same routine can optionally
  be applied.

We emphasize that the purpose of this internal R\ :sub:`200m` calculation is not to provide a
"perfect" mass definition for halos, but rather to provide a consistent basis for the algorithms in
SPARTA. There are many other ways to calculate various mass definitions, see e.g. the 
:doc:`run_al_rsp` and :doc:`run_al_hps` modules.

