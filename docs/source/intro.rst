************
Introduction
************

The following pages give an introduction to the functionality of SPARTA. It is highly recommended 
to read this section before attempting to use SPARTA, and, within this section, it is recommended 
to read the :doc:`intro_general` page before diving into the specifics.

.. rubric:: Contents

.. toctree::
    :maxdepth: 2

    intro_general
    intro_halos
    intro_tracers
    intro_rs_al
    intro_framework
    intro_conventions
    intro_acknowlegments
    