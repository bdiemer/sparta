------------------------------
Unit system
------------------------------

There are essentially two sets of internal units used in SPARTA. Halo and particle positions 
within the context of the simulation box are expressed in comoving units, whereas positions
within halos and radii are expressed in physical units. In some rare cases radii are expressed in
comoving units and should be marked as such (\*_com).

Velocities referring to the simulation box are expressed in their native km/s units, whereas
velocities within halos are in units that combine the physical length units with the time
units of Gyr, making the unit system internally consistent. All cosmic times measure time since
the Big Bang as opposed to lookback-time.

.. table::
   :widths: auto

   ====================================================== ===============
   Physical quantity                                      Units
   ====================================================== ===============
   Masses                                                 :math:`M_{\odot} / h`
   Comoving coordinates, referring to position in box     :math:`{\rm Mpc} / h` (comoving)
   Coordinates within halo, e.g. radii                    :math:`{\rm kpc} / h` (physical)
   Velocities in simulation box                           :math:`{\rm km} / s` (peculiar)
   Velocities within halos                                :math:`{\rm kpc} / h / {\rm Gyr}`
   Times (either relative or since Big Bang)              :math:`{\rm Gyr}`
   ====================================================== ===============
