***************************************************************************************************
Compiling and Running MORIA
***************************************************************************************************

One reason why MORIA is a separate tool is that it is much easier and faster to run, with no need
for parallelization.

.. rubric:: Compiling MORIA

See :doc:`run_compile` for details on how to compile SPARTA. By involing either ``make all`` or 
``make moria``, the MORIA code is compiled in addition to the main SPARTA executable.

.. rubric:: Running MORIA

MORIA is not parallelized and simply executed by calling the executable with the config file as 
a single parameter::

    /path/to/sparta/build/moria <config_file>

.. rubric:: Console Output

.. highlight:: none

Initially, MORIA outputs its configuration similar to the SPARTA output (see 
:doc:`analysis_console`). The first step of MORIA is to initialize all necessary data fields, 
most notably to load the SPARTA file. This step can take a few minutes and will produce output
such as::

    Reading SPARTA file /home/data/sparta/sparta.hdf5...
        Reading group halos...
        Found 1115393 halos and 91 snapshots.
        Reading group anl_rsp...
        Reading group anl_hps...

As with SPARTA, the amount of output is governed by config parameters (see 
:doc:`analysis_moria_config`). At each snapshot, MORIA outputs a few basic status lines and more
detailed output if desired. It begins with::

    Snap  90/91, index 89/90, a 0.9700
    Found 954880 halos in catalog, 257106 (26.9%) pass M200m_peak_cat cut, out of those 73.3% hosts and 26.7% subs.
    
In this case, we are at the second-to-last snapshot of the simulation. We have imposed a cut in 
M200m_peak_cat, which only about 27% of all halos pass. For those, we get much more accurate 
statistics on the available data::
    
    Sparta status:                  N/A      Host       Sub
      Fraction:                    0.0%     73.3%     26.7%
    Mass acc. rate status:           OK rdcd-tdyn     model    sub-OK  sub-rdcd sub-model sb-mdl-nw
      Fraction (all):             71.7%      1.5%      0.1%     23.1%      3.0%      0.3%      0.3%
      Fraction (hosts):           97.8%      2.0%      0.2%      0.0%      0.0%      0.0%      0.0%
      Fraction (subs):             0.0%      0.0%      0.0%     86.4%     11.4%      1.1%      1.1%
      
A SPARTA status was found for all halos, out of which about three quarters are hosts. The meaning
of the mass accretion rate status fields is explained in :doc:`analysis_moria_output`. If we are
adding a splashback analysis, we get the following lines::

    Analysis rsp status:    OK   not_vld    no_anl  insf_evt  insf_wgt |  gs-model   gs-past   gs-ftre  gs-intrp
      Fraction (all):    51.4%      0.0%      0.5%     23.0%     25.0% |     33.2%     66.8%      0.0%      0.0%
      Fraction (hosts):  68.5%      0.0%      0.2%      6.2%     25.2% |     16.4%     83.6%      0.0%      0.0%
      Fraction (subs):    4.4%      0.0%      1.6%     69.4%     24.6% |     48.5%     51.5%      0.0%      0.0%
      
They tell us how many halos had successful splashback radius measurements, and reasons for those
where they were not available. The right part tells us how MORIA fixed the missing measurements,
see :doc:`analysis_moria_output` for the meaning of the column headers. If we are adding a 
halo properties analysis, we get lines like these::   

    Analysis hps status:             OK    no_anl  so_small  so_large
      R200m_all_spa   (all):      89.9%      0.0%      0.0%     10.1%
      R200m_all_spa   (hosts):   100.0%      0.0%      0.0%      0.0%
      R200m_all_spa   (subs):     62.1%      0.0%      0.0%     37.9%
      M200m_all_spa   (all):      89.9%      0.0%      0.0%     10.1%
      M200m_all_spa   (hosts):   100.0%      0.0%      0.0%      0.0%
      M200m_all_spa   (subs):     62.1%      0.0%      0.0%     37.9%
      R200m_tcr_spa   (all):      99.4%      0.0%      0.6%      0.0%
      R200m_tcr_spa   (hosts):   100.0%      0.0%      0.0%      0.0%
      R200m_tcr_spa   (subs):     97.8%      0.0%      2.2%      0.0%
      M200m_tcr_spa   (all):      99.4%      0.0%      0.6%      0.0%
      M200m_tcr_spa   (hosts):   100.0%      0.0%      0.0%      0.0%
      M200m_tcr_spa   (subs):     97.8%      0.0%      2.2%      0.0%

Again, for each definition that was output we learn how many halos did not have valid 
measurements. For host halos, virtually all halos should have valid SO masses but for subhalos
there can be many issues. Finally, if we are computing host-subhalo relations, we get lines like
these::
      
    Definition        subs changed       host->host      host->sub      sub->host   sub->sub,same  sub->sub,diff
      Rsp-apr-mn     29.4%    4.7%  179879 ( 70.0%)  8564 (  3.3%)  1715 (  0.7%)  65070 ( 25.3%)  1878 (  0.7%)
      Rsp-apr-p50    28.7%    4.5%  181074 ( 70.4%)  7369 (  2.9%)  2346 (  0.9%)  64448 ( 25.1%)  1869 (  0.7%)
      Rsp-apr-p70    32.3%    6.9%  173602 ( 67.5%) 14841 (  5.8%)   500 (  0.2%)  65857 ( 25.6%)  2306 (  0.9%)
      Rsp-apr-p75    33.3%    7.8%  171282 ( 66.6%) 17161 (  6.7%)   288 (  0.1%)  65823 ( 25.6%)  2552 (  1.0%)
      Rsp-apr-p80    34.3%    8.9%  168639 ( 65.6%) 19804 (  7.7%)   154 (  0.1%)  65625 ( 25.5%)  2884 (  1.1%)
      Rsp-apr-p85    35.5%   10.2%  165652 ( 64.4%) 22791 (  8.9%)    65 (  0.0%)  65323 ( 25.4%)  3275 (  1.3%)
      Rsp-apr-p90    37.0%   11.8%  161948 ( 63.0%) 26495 ( 10.3%)    22 (  0.0%)  64810 ( 25.2%)  3831 (  1.5%)
      R200m_bnd_cat  26.7%    0.0%  188443 ( 73.3%)     0 (  0.0%)     0 (  0.0%)  68662 ( 26.7%)     1 (  0.0%)
      Rvir_bnd_cat   21.9%    5.4%  188443 ( 73.3%)     0 (  0.0%) 12275 (  4.8%)  54879 ( 21.3%)  1509 (  0.6%)
      R200c_bnd_cat  15.8%   11.7%  188443 ( 73.3%)     0 (  0.0%) 28034 ( 10.9%)  38474 ( 15.0%)  2155 (  0.8%)
      R500c_bnd_cat   8.8%   18.6%  188443 ( 73.3%)     0 (  0.0%) 46124 ( 17.9%)  20764 (  8.1%)  1775 (  0.7%)
      R200m_all_spa  28.5%    2.0%  183865 ( 71.5%)  4578 (  1.8%)     1 (  0.0%)  68222 ( 26.5%)   440 (  0.2%)
      Rvir_all_spa   23.8%    5.5%  187407 ( 72.9%)  1036 (  0.4%)  8420 (  3.3%)  55459 ( 21.6%)  4784 (  1.9%)
      R200c_all_spa  17.6%   11.6%  188432 ( 73.3%)    11 (  0.0%) 23548 (  9.2%)  38729 ( 15.1%)  6386 (  2.5%)
      R500c_all_spa   9.9%   18.6%  188442 ( 73.3%)     1 (  0.0%) 43097 ( 16.8%)  20846 (  8.1%)  4720 (  1.8%)
      R200m_tcr_spa  27.2%    0.6%  187076 ( 72.8%)  1367 (  0.5%)    51 (  0.0%)  68415 ( 26.6%)   197 (  0.1%)
      Rvir_tcr_spa   22.1%    5.2%  188414 ( 73.3%)    29 (  0.0%) 11811 (  4.6%)  55337 ( 21.5%)  1515 (  0.6%)
      R200c_tcr_spa  15.9%   11.7%  188436 ( 73.3%)     7 (  0.0%) 27812 ( 10.8%)  38668 ( 15.0%)  2183 (  0.8%)
      R500c_tcr_spa   8.8%   18.6%  188443 ( 73.3%)     0 (  0.0%) 45973 ( 17.9%)  20842 (  8.1%)  1848 (  0.7%)

Each line tells us how the host-sub relations change in a given definition compared to the original
catalog definition. In this case, the original was ``R200m_bnd_cat`` and we see that no halos changed
for that definition (except or one parent ID, which can be the result of numerical inaccuracies).
Larger radii will lead to more subhalos, and vice versa. Finally, MORIA can output memory statistics
very much like the ones in SPARTA::

    [Main] [SN  89] Memory: Total current 15.0 GB (15377.7 MB per proc), peak 15.8 GB (16195.9 MB per proc)
    [Main] [SN  89]         Overall peak was 16982618461, 16195.9 MB on proc 0

The total current refers to the memory that is persistent between snapshots, which is typically
the vast majority. We also get a listing of the memory-consuming fields::

    [Main] [SN  89]         Memory ID             Description        At overall peak:   Byte       MB Fraction
    [Main] [SN  89]         MORIA_SPARTA          MORIA: SPARTA data             15992510914  15251.6   94.2 %
    [Main] [SN  89]         MORIA_CAT             MORIA: Catalog data              265141488    252.9    1.6 %
    [Main] [SN  89]         MORIA_HALOS           MORIA: Halo data                 572346095    545.8    3.4 %

Note that the consumption at the overall memory peak was almost entirely the SPARTA data, 15GB
in this case. Thus, a MORIA job must have enough memory for the SPARTA data available but not
much more. If there is an insufficient-memory crash, it will likely occur at the beginning of a 
run. At the end of the run, we get a timing summary::

    Timing (min)                    Time       Percentage
    ------------------------------------------------------------
    Loading SPARTA data             1.69        1.4
    Snapshots                     106.52       86.8
       Initialize and Finalize         0.07        0.1
       Sort IDs                        0.20        0.2
       Read halo catalog              16.23       13.2
       Match to SPARTA file            0.27        0.2
       Add ghost halos                 0.05        0.0
       Combine halo data               4.65        3.8
       Subhalo relations              27.25       22.2
       Halo statistics                 0.24        0.2
       Merger tree logic              12.37       10.1
       Read catalog data              15.07       12.3
       Write catalog                  25.21       20.5
       Write HDF5                      2.56        2.1
       Write restart files             2.34        1.9
    Merger tree                    14.57       11.9
    Finalize                        0.00        0.0
    ------------------------------------------------------------
    Total                         122.78      100.

The time consumption tends to be dominated by I/O operations and computing the subhalo relations.