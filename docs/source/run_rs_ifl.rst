**************
Infall results
**************

As the name suggests, infall results record the time and circumstances of the first infall of a 
tracer into a halo. These events can contain more information such as the radial and tangential 
velocities at infall, as well as whether a tracer was part of a subhalo.
Specifically, infall results are created at the time when the tracer crosses R\ :sub:`200m` of a 
host halo. However, an infall result is also written in cases where the true infall cannot be 
determined.

.. rubric:: Particle tracers

In the ideal (and most common) case, SPARTA begins tracking particles well outside of 
R\ :sub:`200m` (as determined by the tracer creation radius). The code then follows the particle's
trajectory and determines the exact infall time t\ :sub:`ifl` by linear interpolation. 
There are, however, a number of reasons why the moment of crossing R\ :sub:`200m` cannot be 
determined:

* the particle is already in the halo when it is born, that is, when it first appears in the halo
  catalog. This inevitably happens to the first particles the halo finder detects as a new halo.
* when a halo becomes a subhalo, we determine a set of particles that truly belong to the
  subhalo (see :doc:`intro_halos`) and create tracers for those particles. In some cases, those
  particles might not have crossed R\ :sub:`200m` and might thus not have an infall result
  associated with them.

In those cases, we still save an infall event, but with the flag ``born_in_halo``, which 
indicates that the time of infall was not actually recorded and simply reflects the time when
the tracer object was created. Thus, every particle tracer should either already have an
infall result, or we should be looking for one because the tracer has not yet entered 
R\ :sub:`200m`. As a consequence, infall events are numerous and can occupy a significant amount
of memory or output space. They should only be written to file if necessary.

.. rubric:: Subhalo tracers

Infall events for subhalos are recorded whenever they cross R\ :sub:`200m`. As the halo merger 
trees are based on R\ :sub:`200m` as computed only from bound host halo particles, some subhalos 
may already lie within the R\ :sub:`200m` of all particles when they first become subhalos. 
However, infall events can be constructed from their saved trajectories in most cases. One 
exception occurs when halos are newly created as subhalos: in this case, the subhalo never 
technically crossed into its host, and no infall result is recorded.

.. rubric:: Compile-time parameters

+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| Parameter                                          | Explanation                                                                                              |
+====================================================+==========================================================================================================+ 
| ``OUTPUT_RESULT_INFALL``                           | Write infall results to output file                                                                      |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_INFALL_TIME``                      | Save the time of infall in output file                                                                   |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_INFALL_BORNINHALO``                | Save the born-in-halo flag in output file                                                                |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_INFALL_SMR``                       | Save the subhalo-to-host halo mass ratio in output file                                                  |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_INFALL_VRV200``                    | Save vr/v200m to the output file                                                                         |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_INFALL_VTV200``                    | Save vt/v200m to the output file                                                                         |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_INFALL_X``                         | Save the 3D position at infall to the output file                                                        |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 

.. rubric:: Run-time parameters

This result does not add any config parameters.

.. rubric:: Output fields

+--------------------------+----------+----------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| Field                    | Type     | Dimensions                 | Exists if                           | Explanation                                                                        |
+==========================+==========+============================+=====================================+====================================================================================+ 
| ``halo_first``           | int64    | ``n_halos``                | Always                              | The index of the first result for each halo (or -1 if none exists for a halo).     |
+--------------------------+----------+----------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``halo_n``               | int32    | ``n_halos``                | Always                              | The number of results of this type for each halo (can be 0).                       |
+--------------------------+----------+----------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``tracer_id``            | int64    | ``n_rs_infall``            | Always                              | The ID of the tracer to which this result refers.                                  |
+--------------------------+----------+----------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``t_infall``             | float    | ``n_rs_infall``            | ``OUTPUT_RESULT_INFALL_TIME``       | The time of infall in Gyr since the Big Bang; not reliable when born_in_halo       |
+--------------------------+----------+----------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``born_in_halo``         | float    | ``n_rs_infall``            | ``OUTPUT_RESULT_INFALL_BORNINHALO`` | If True, infall of tracer was not tracked, tracer was created inside (sub-)halo    |
+--------------------------+----------+----------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``sub_mass_ratio``       | float    | ``n_rs_infall``            | ``OUTPUT_RESULT_INFALL_SMR``        | SubMassRatio, >0 for subhalos or particles that fell in with a subhalo             |
+--------------------------+----------+----------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``vrv200``               | float    | ``n_rs_infall``            | ``OUTPUT_RESULT_INFALL_VRV200``     | The radial velocity at infall divided by v200m.                                    |
+--------------------------+----------+----------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``vtv200``               | float    | ``n_rs_infall``            | ``OUTPUT_RESULT_INFALL_VTV200``     | The tangential velocity at infall divided by v200m.                                |
+--------------------------+----------+----------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``x``                    | float    | ``n_rs_infall`` * 3        | ``OUTPUT_RESULT_INFALL_X``          | The position at infall relative to the halo center (in physical kpc/h)             |
+--------------------------+----------+----------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
