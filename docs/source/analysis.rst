***************************************************************************************************
Analyzing SPARTA output
***************************************************************************************************

While SPARTA is running, it produces :doc:`analysis_console` that one should at least partially 
understand before using the outputs of SPARTA.

A SPARTA run produces a single HDF5 output file. The contents of this file 
are documented in :doc:`analysis_hdf5`, and depend on the chosen :doc:`intro_rs_al` as well as the 
compile-time output settings (see :doc:`run_compile` as well as the documentation about the 
different sub-modules).

The HDF5 file can be read using any HDF5 utility, but in practice matching the different datasets 
can be tedious. Thus, SPARTA comes with a much more convenient :doc:`analysis_python` that reads 
a SPARTA file and translates the selected data into python dictionaries. This utility is the 
recommended way to read SPARTA files.

However, in many cases, the desired output is not the SPARTA file itself but an augmented halo
catalog that can be created with the MORIA extension (see :doc:`analysis_moria`).

Regardless of how the SPARTA and/or MORIA files are read and analyzed, they use the common set
of abbreviations for tracers, results, and analyses:

+-------------------+--------------------------+-------------------+-------------------+ 
| Type              | Incarnation              | Long name         | Abbreviation      |
+===================+==========================+===================+===================+
| Tracer            |                          | ``tracer``        | ``tcr``           |
+-------------------+--------------------------+-------------------+-------------------+ 
| Tracer            | Particles                | ``particles``     | ``ptl``           |
+-------------------+--------------------------+-------------------+-------------------+ 
| Tracer            | Subhalos                 | ``subhalos``      | ``sho``           |
+-------------------+--------------------------+-------------------+-------------------+ 
| Result            |                          | ``result``        | ``res``           |
+-------------------+--------------------------+-------------------+-------------------+ 
| Result            | Infall                   | ``infall``        | ``ifl``           |
+-------------------+--------------------------+-------------------+-------------------+ 
| Result            | Splashsback              | ``splashback``    | ``sbk``           |
+-------------------+--------------------------+-------------------+-------------------+ 
| Result            | Trajectory               | ``trajectory``    | ``tjy``           |
+-------------------+--------------------------+-------------------+-------------------+ 
| Result            | OrbitCount               | ``orbitcount``    | ``oct``           |
+-------------------+--------------------------+-------------------+-------------------+ 
| Analysis          |                          | ``analysis``      | ``anl``           |
+-------------------+--------------------------+-------------------+-------------------+ 
| Analysis          | SplashbackRadius         | ``rsp``           | ``rsp``           |
+-------------------+--------------------------+-------------------+-------------------+ 
| Analysis          | DensityProfiles          | ``profiles``      | ``prf``           |
+-------------------+--------------------------+-------------------+-------------------+ 
| Analysis          | HaloProperties           | ``haloprops``     | ``hps``           |
+-------------------+--------------------------+-------------------+-------------------+ 

.. rubric:: Contents

.. toctree::
    :maxdepth: 2

    analysis_console
    analysis_hdf5
    analysis_python
    analysis_moria
    analysis_moria_output
    