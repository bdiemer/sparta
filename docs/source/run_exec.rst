***************************************************************************************************
Running the SPARTA executable
***************************************************************************************************

Before attempting to run SPARTA, please follow the steps in :doc:`run_compile` to create a build
directory and build the code. Let's assume the path to your build directory is ``/path/to/build``.

SPARTA can be run from anywhere but must be run as an MPI program, i.e. by executing::

    mpirun -np <np> /path/to/build/sparta <config_file>
    
The number of processes, ``np``, can be 1. If a run has crashed, SPARTA can be restarted from a 
set of restart files (see :doc:`intro_framework`). To do so, we add a ``-restart`` parameter::

    mpirun -np <np> /path/to/build/sparta -restart <config_file>

Note that the config file must be the same as before. The config cannot be changed as it is read
from the restart files. However, the code needs to read the output path from the original config
file to know where to find the restart files. The number of processes cannot be changed during a
restart.
