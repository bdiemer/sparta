***************************************************************************************************
Adding a run-time configuration parameter
***************************************************************************************************

The SPARTA run-time configuration contains any user-defined parameter that does not need to be set 
at compile-time. If this parameter is specific to a result or analysis, much of its treatment is 
confined to the respective code files, leading to a different treatment as described below.

=========================================================================
Adding a parameter for a result or analysis
=========================================================================

Follow these steps to add a general configuration parameter that is specific to a result or an
analysis:

* ``src/global.h``

  - Define the new parameter in the ``ConfigData`` struct.

* ``src/config.c``

  - In ``readConfigFile()``, add an ``else if`` switch looking for the new parameter. Make sure to 
    use the correct conversion from string to the format, e.g. ``atof`` or ``atoi`` etc. The 
    identifier string used in the config file should ideally be the same as the name of your 
    variable within the ``ConfigData`` struct to avoid confusion.

* ``src/results/result_xxx.c`` or ``src/analyses/analysis_xxx.c``

  - There will be a function ``initConfigResultXxx`` or ``initConfigAnalysisXxx``, and within this
    function a number of steps are treated.
  - Under ``CONFIG_INIT_STEP_DEFAULT``, set the default values of your configuration parameters.
    Any parameter should have a default unless you want to force the user to set the parameter 
    manually, in which case you must check that they did so (see below); otherwise the parameter
    will be undefined.
  - Under ``CONFIG_INIT_STEP_CHECK``, check that the user's input makes sense (e.g., no negative
    numbers). This step can be omitted, but should be as strict as possible/necessary. This step
    happens directly after the config file is read.
  - Under ``CONFIG_INIT_STEP_CONFIG``, you can analyze the given config parameters in detail. The
    difference to ``CONFIG_INIT_STEP_CHECK`` is that, by now, SPARTA has analyzed the rest of the
    config and the simulation, including cosmology, the number of snapshots and so on.
  - Under ``CONFIG_INIT_STEP_PRINT``, add a line where you print the value of the new config
    parameter to the console. Please pay attention to the format (int/float/string, and if float, 
    a sensible number of significant digits).
  - There should be a function called ``outputConfigResultXxx`` or ``outputConfigAnalysisXxx``, 
    and if not, you should create it and link it to the properties of your result or analysis
    (see :doc:`developers_result` or :doc:`developers_analysis`). In this function, add a command
    to output your parameter, which will look something like::
    
        hdf5WriteAttributeFloat(grp, "anl_xxx_my_param", config.anl_xxx_my_param);
  
    See the HDF5 unit in ``src/io/io_hdf5.c`` for details on the different output functions. Note
    that your parameter will automatically be written into the correct place in the output file.

* ``docs/source/rs_xxx.rst`` or ``docs/source/al_xxx.rst``

  - Add the new parameter to the table that lists config parameters or create such a table. Specify 
    its default value and add a brief explanation.

* ``config/sparta_complete.cfg`` and ``config/sparta.cfg``

  - Add the new parameter and its default value to the complete, default config file. This file 
    makes it easier for the user to have a starting point and adjust parameters to their needs.
    If you think your parameter is important enough to be frequently changed by the user, also
    add it to ``sparta.cfg``.

=========================================================================
Adding a general parameter
=========================================================================

Follow these steps to add a general configuration parameter that is not specific to a result or an
analysis. Note that you should only need to do this if you are working on the SPARTA framework 
itself! Any parameters related to plugins, i.e., results and analyses, should be contained within 
those units.

* ``src/global.h``

  - Define the new parameter in the ``ConfigData`` struct.
  - Define a default for the parameter in the appropriate section near the top of the file. Most 
    parameters should have defaults; if no default is set, the parameter could end up being undefined 
    unless a check for invalid settings is added (see below).

* ``src/config.c``

  - In ``setDefaultConfig()``, set the parameter to the default value.
  - In ``readConfigFile()``, add an ``else if`` switch looking for the new parameter. Make sure to 
    use the correct conversion from string to the format, e.g. ``atof`` or ``atoi`` etc. The 
    identifier string used in the config file should ideally be the same as the name of your 
    variable within the ``ConfigData`` struct to avoid confusion.
  - In ``checkConfig()``, if necessary add an ``assert`` to check for nonsense values of the new 
    parameter. This helps to avoid a lot of silly problems later!
  - In ``printConfig()``, add the new parameter to the printed output. Please pay attention to the 
    format (int/float/string, and if float, a sensible number of significant digits).

* ``src/io/io_output.c``

  - In ``writeConfig()``, add a command to output your parameter, which will look something like::
    
        hdf5WriteAttributeFloat(grp, "my_param", config.my_param);
  
    See the HDF5 unit in ``src/io/io_hdf5.c`` for details on the different output functions.

* ``docs/source/run_config.rst``

  - Add the new parameter to the table, specify its default value, and add a brief explanation.

* ``config/sparta.cfg``

  - Add the new parameter and its default value to this complete, default config file. This file 
    makes it easier for the user to have a starting point and adjust parameters to their needs.
    If you think your parameter is important enough to be frequently changed by the user, also
    add it to ``sparta.cfg``.

You may notice that all of the routines above as well as the documentation and defaults follow the 
same order, please make sure you stick to this order.
  