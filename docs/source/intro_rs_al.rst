***************************************************************************************************
Results & Analyses
***************************************************************************************************

The previous pages about :doc:`intro_halos` and :doc:`intro_tracers` described mechanisms that are 
fundamental to the SPARTA framework. In contrast, this page describes two types of components that 
are meant to be flexible, meaning that

* they can easily be turned on or off by the user with compiler switches
* they represent plug-in like structures that can easily be added to the code
* they provide abstracted routines and data structures that allow SPARTA to be agnostic as to what 
  exactly they do.
  
In particular, tracer results are modules and the resulting units of information that refer to an 
individual tracer. Halo analyses are modules and resulting units of information that refer to 
individual halos. We will discuss the currently implemented instantiations of these types of modules 
in :doc:`run_rs` and :doc:`run_al`.

|

.. image:: ../images/intro_rs_al.jpeg
    :scale: 50 %
    :align: center

|

Here, we briefly discuss their general logic and properties. The schematic above illustrates the
main principles based on a few of the module implementations. The gray shaded area represents the 
halo object in memory and code, whereas the circle represents the physical halo in the simulation.

------------------------------
Tracer results
------------------------------

We use the terms "event", "tracer result", and simply "result" interchangeably to mean pieces of 
information derived from the trajectory of a tracer, as well as the routines that perform those 
computations. Results are structured as plug-ins, making it easy to add a result to SPARTA (for 
details, see :doc:`developers`). Results are turned on and off by compiler switches. 

In the schematic above, two results are turned on, they are represented as blue spheres and red
squares, respectively (they happen at infall and splashback, but that does not matter here). 
The result routines scan each tracer's trajectory at each snapshot. If an event is detected, 
that result is saved into the result arrays of the respective tracer types (particles and 
subhalos).

In principle, a tracer can create any number of results, though many events (such as infall 
and splashback), occur at most once for each tracer in a given halo. Results are stored in 
separate arrays but carry the ID of their generating tracer so that they can be reconnected 
to each other later. For example, some results are continuously modified.

------------------------------
Halo analyses
------------------------------

Analyses are pieces of code that is executed on a per-halo basis. The analysis chooses when to
act. For example, the profile analysis may be called at user-defined redshifts, whereas the 
splashback radius (Rsp) analysis runs only once at the end of a halo's life.

Analyses have access to the full information in the halo: the particles within the search radius,
the tracer objects, and the stored results. The user can choose which elements are saved to disk
at the end of a run. Storing all results might lead to an excessive file size, and the user might 
choose to only save the results of an analysis instead. This choice would not influence the way
the results are computed internally.
