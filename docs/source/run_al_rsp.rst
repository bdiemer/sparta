**************************
Splashback radius analysis
**************************

The splashback analysis computes the splashback radius and mass of a halo over time based on the 
:doc:`run_rs_sbk` of individual particles.

.. rubric:: Algorithm

The splashback analysis is run at the end of a halo's life, when all splashback events have been 
collected. First, we exclude particles that belonged to subhalos greater than some maximum 
sub-to-host mass ratio (the ``anl_rsp_max_smr`` parameter) because they are, on average, 
influenced by the dynamical friction 
that large subhalos experience. We use the particles' :doc:`run_rs_ifl` to determine whether they
were part of a large subhalo.

After removing such particles, the radial distribution of splashback events tends to be well fit 
by a Gaussian profile with a tail toward high radii. We smooth the contribution from each splashback 
event in time using a Gaussian with a width of ``anl_rsp_sigma_tdyn`` dynamical times. We now have,
for each snapshot, a set of weighted particle splashback events. We calculate splashback properties
for this time if the accumlated weight is at least ``anl_rsp_min_weight``. Possible definitions 
include the mean of the distribution or precentiles. We note that, especially for higher percentiles, the 
statistical uncertainty due to the limited number of particle splashback events can become 
significant. To estimate such errors, we run a few hundred bootstrap samples (if requested by the
user).

Finally, we need to correct for two biases that occur at the end of the simulation (typically at 
z = 0). First, the number of splashback events in the final time bin (between the second-to-last 
and last snapshots) is drastically lower than in the previous time bins. Thus, we ignore any 
splashback events that have occurred after the time of the second-to-last snapshot as they are likely 
biased in some nontrivial way. Second, the distribution of splashback events considered in the final 
snapshots becomes asymmetric due to the smoothing discussed above: the Gaussian filter is sensitive 
to events at earlier times, but there are no events at later times. This asymmetry can lead to a 
significant and systematic bias because the splashback radius is, in most cases, increasing with time. 
We correct for this asymmetry by linearly extrapolating the past evolution of the splashback radius
into the future.

We find the splashback mass by considering the distribution of enclosed masses of the particle
splashback events. The algorithm is described in detail in 
`Diemer 2017 <https://ui.adsabs.harvard.edu//#abs/2017ApJS..231....5D/abstract>`_.

.. rubric:: Splashback definitions

The splashback analysis can compute a number of different definitions of the splashback radius and
mass. These can be set using the ``anl_rsp_defs`` parameter. Of course, only definitions that can
actually be computed by the Rsp analysis can be chosen. Those include:

* ``Rsp-apr-mn``: mean of the apocenter distribution
* ``Rsp-apr-p50``: median of the apocenter distribution
* ``Rsp-apr-p<n>``: any percentile of the apocenter distribution
* ``Msp-***``: the corresponding masses
* ``***_err``: the one-sigma uncertainty on any of the above quantities. For percentiles, this 
  leads to bootstrapping and thus a significant performance penalty.
  
For details on the string format and possible definitions, see :doc:`intro_conventions_halodef`.

.. rubric:: Compile-time parameters

+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| Parameter                                          | Explanation                                                                                              |
+====================================================+==========================================================================================================+ 
| ``OUTPUT_ANALYSIS_RSP``                            | Write Rsp analyses to output file                                                                        |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``ANALYSIS_RSP_MAX_SNAPS``                         | The max. num. of snapshots for which analysis can be saved; must be at least num. of requested redshifts |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``ANALYSIS_RSP_MAX_DEFINITIONS``                   | The maximum number of splashback definitions (radii, masses etc) that can be requested by the user       |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 

If memory is an issue, the ``ANALYSIS_RSP_MAX_SNAPS`` and ``ANALYSIS_RSP_MAX_DEFINITIONS`` 
parameters should be adjusted close to the values they must have to accommodate a given simulation
and user preferences.

.. rubric:: Run-time parameters

+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| Parameter                        | Type    | Default     | Explanation                                                                            |
+==================================+=========+=============+========================================================================================+ 
| ``anl_rsp_redshifts``            | list    | -1          | A list of redshifts where splashback properties should be computed, or -1 (all)        |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_rsp_defs``                 | list    | None        | A list of splashback definitions to be computed (see above)                            |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_rsp_min_rrm``              | float   | 0.0         | Minimum r / R200m of splashback events to use in Rsp analysis                          |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_rsp_max_rrm``              | float   | 1.0         | Maximum r / R200m of splashback events to use in Rsp analysis                          |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_rsp_min_smr``              | float   | -2.0        | Minimum sub-to-host ratio to use in Rsp analysis (for subhalo particles)               |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_rsp_max_smr``              | float   | 0.01        | Maximum sub-to-host ratio to use in Rsp analysis (for subhalo particles)               |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_rsp_demand_infall_rs``     | bool    | ``FALSE``   | Only use splashback results if they have a corresponding infall result                 |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_rsp_sigma_tdyn``           | float   | 0.2         | The width in time over which events are smoothed, in units of the dynamical time       |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_rsp_min_weight``           | float   | 10.0        | The minimum total weight in a bin for Rsp to be estimated                              |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_rsp_n_bootstrap``          | int     | 200         | The number of bootstrap samples used to estimate the statistical uncertainty           |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_rsp_do_correction``        | bool    | ``TRUE``    | Correct the final snapshots for the asymmetry in events (recommended)                  |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 

Note that choosing redshifts using the ``anl_rsp_redshifts`` parameter saves memory and disk space, 
but can lead to issues when creating halo catalogs with MORIA. If in doubt, it is probably best to
output the analysis for all snapshots.

.. rubric:: Output fields

+--------------------------+----------+----------------------------+------------------------------------------+------------------------------------------------------------------------------------+ 
| Field                    | Type     | Dimensions                 | Exists if                                | Explanation                                                                        |
+==========================+==========+============================+==========================================+====================================================================================+ 
| ``halo_first``           | int64    | ``n_halos``                | Always                                   | The index of the first analysis for each halo (or -1 if none exists for a halo).   |
+--------------------------+----------+----------------------------+------------------------------------------+------------------------------------------------------------------------------------+ 
| ``halo_n``               | int32    | ``n_halos``                | Always                                   | The number of analyses of this type for each halo (can be 0).                      |
+--------------------------+----------+----------------------------+------------------------------------------+------------------------------------------------------------------------------------+ 
| ``halo_id``              | int64    | ``n_al_rsp``               | Always                                   | The (original, first-snapshot) halo ID to which this analysis refers.              |
+--------------------------+----------+----------------------------+------------------------------------------+------------------------------------------------------------------------------------+ 
| ``status``               | int8     | ``n_al_rsp`` * ``n_snaps`` | Always                                   | A status field that indicates whether the Rsp analysis was successful (see below)  |
+--------------------------+----------+----------------------------+------------------------------------------+------------------------------------------------------------------------------------+ 
| ``<halo definition>``    | float    | ``n_al_rsp`` * ``n_snaps`` | Always                                   | One field for each halo definition (see above)                                     |
+--------------------------+----------+----------------------------+------------------------------------------+------------------------------------------------------------------------------------+ 

Here, ``n_snaps`` is, of course, the number of redshifts chosen by the user of the number of
snapshots in the simulation if ``anl_rsp_redshifts`` is -1. The status field can take on the
following values:

+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| Value | Parameter                             | Explanation                                                                                              |
+=======+=======================================+==========================================================================================================+ 
| 0     | ``ANL_RSP_STATUS_UNDEFINED``          | Placeholder, should never occur in output file                                                           |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 1     | ``ANL_RSP_STATUS_SUCCESS``            | The analysis succeeded, all output values can be used                                                    |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 2     | ``ANL_RSP_STATUS_HALO_NOT_VALID``     | Halo could not be analyzed at this snapshot, e.g. because too young                                      |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 3     | ``ANL_RSP_STATUS_HALO_NOT_SAVED``     | Halo was not saved to the SPARTA output file at all (used in MORIA)                                      |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 4     | ``ANL_RSP_STATUS_NOT_FOUND``          | Analysis not found for this halo (used in MORIA)                                                         |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 5     | ``ANL_RSP_STATUS_INSUFFICIENT_EVENTS``| There were not enough particle splashback events                                                         |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 6     | ``ANL_RSP_STATUS_INSUFFICIENT_WEIGHT``| There were enough particle splashback events, but their weight was too low                               |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 

