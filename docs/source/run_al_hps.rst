***************************************************************************************************
Halo properties analysis
***************************************************************************************************

The halo properties analysis is designed to compute halo properties that depend on the particle
distribution, on tracer properties, and possibly on more advanced data such as pericenter counts.

.. rubric:: Algorithm for SO masses and radii

The analysis computes SO masses by creating a sorted array of particle radii and comparing the 
overdensity enclosed within each particle's radius to one or multiple density thresholds. Moreover,
the particle distribution may be a sub-selection of all particles. Thus, there are a number of 
fundamentally different SO masses:

* ``all``: All particles. These masses correspond to a strict overdensity criterion for both host
  and subhalos. For subhalos, strict SO masses may not be very sensible as they likely include a
  large contribution from the host mass. In such cases, the mass at infall or peak is often used 
  instead of the instantaneous subhalo mass. For all-particle masses, we do not tolerate cases
  where the density never decreases to the threshold; for such halos, we set an error code.
* ``bnd``: Gravitationally bound particles. Here, the distribution of particles is taken inside a 
  certain radius determined through config parameters (see below). Then, that distribution is 
  subjected to (possibly iterative) unbinding, where a particle is considered unbound if its 
  kinetic energy with respect to the halo center is larger than some factor times its gravitational 
  binding energy (computed approximately by using a tree potential). Note that the result is 
  strongly dependent on the radius within which particles are considered: if that radius is very 
  large, e.g., multiple times R200m, then almost all particles will be bound. If the radius is 
  small, no particles may be bound, in which case a halo mass and radius of zero are output. 
  Generally, one should not expect that the bound masses match bound masses from a halo finder. 
  For example, Rockstar uses a friends-of-friends group as the initial halo membership, which 
  cannot be reproduced with any SO definition. For bound masses, we tolerate SO masses where the
  density never decreases to the threshold because the bound particle distribution can be very
  compact. In that case, multiple SO definitions can agree on the same mass (but different radii).
* ``tcr``: Tracer masses. This definition applies only to subhalos when sub-particle tracking is
  on (see :doc:`intro_tracers`). At subhalo infall, particles are tagged if they are deemed to 
  physically belong to the subhalo. The tracer mass is then defined as an SO mass only for those
  particles, while no new particles are added to the subhalo (because they are thought to likely
  belong to the host). For host halos, the all-particle mass is substituted for completeness.
  As for bound masses, we tolerate cases where the density never decreases to the threshold (which
  is a fairly common case for tracer masses).
* ``orb``: Orbiting particles, namely particles that have undergone at least one pericenter as
  determined by their :doc:`run_rs_oct` tracer results. This definition applies for both hosts 
  and subhalos. 

Thus, valid definitions include ``R200m_all``, ``R500c_all``, ``M500c_all``, ``M200c_tcr``, 
``M200c_bnd``, ``R200m_orb`` or any other combination of those elements.

.. rubric:: Radii and masses from percentiles of orbiting particles

Unlike SO definitions, these experimental definitions are based on the distribution of orbiting 
particles but do not rely on an overdensity threshold. There are two fundamental types of 
definition:

* ``Morb-all``: The total mass of all particles that have ever (!) had a pericenter in this halo.
  These particles can be at arbitrary distances at the current time. 
* ``Morb-p<percentile>`` and ``Rorb-p<percentile>``: these definitions pick out the radius where a
  particular percentile of the orbiting particles is reached. Since we need the radii of the 
  orbiting particles, we cannot include particles at arbitrary distances. Instead, only particles
  within ``anl_hps_r_max_orb_host`` or ``anl_hps_r_max_orb_sub`` times R200m of the halo are 
  considered. If those radii are large enough (e.g., at least 3R200m), the vast majority of 
  orbiting particles is typically included. This can be checked by comparing ``Morb-p99`` to
  ``Morb-all``, for example. The radius is linearly interpolated between neighboring particles.
  By construction, the percentile masses are trivially related to each other, but the radii depend
  on the radial profile of orbiting particles.
  
Note that all orbiting definitions depend critically on the algorithm to determine pericenters.
Thus, they are to be seen as experimental.
  
.. rubric:: Compile-time parameters

+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| Parameter                                          | Explanation                                                                                              |
+====================================================+==========================================================================================================+ 
| ``OUTPUT_ANALYSIS_HALOPROPS``                      | Write halo properties analyses to output file                                                            |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_ANALYSIS_HALOPROPS_RM``                   | Compute spherical overdensity radii and masses for definitions selected by the user                      |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_ANALYSIS_HALOPROPS_ORBITING``             | Compute SO radii and masses that include only orbiting particles (triggers OCT results)                  |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``ANALYSIS_HALOPROPS_MAX_SNAPS``                   | The maximum number of snapshots that can be requested by the user                                        |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``ANALYSIS_HALOPROPS_MAX_DEFINITIONS``             | The maximum number of definitions that can be requested by the user                                      |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 

If memory is an issue, the ``ANALYSIS_HALOPROPS_MAX_SNAPS`` and ``ANALYSIS_HALOPROPS_MAX_DEFINITIONS`` 
parameters should be adjusted close to the values they must have to accommodate a given simulation
and user preferences.

.. rubric:: Run-time parameters

+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| Parameter                        | Type    | Default     | Explanation                                                                            |
+==================================+=========+=============+========================================================================================+ 
| ``anl_hps_redshifts``            | [float] | -1          | Redshifts where halo properties are output; list of floats, or -1 for all              |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_hps_defs``                 | list    | None        | Spherical overdensity definitions to be computed                                       |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_hps_r_max_so_host``        | float   | 2.0         | Maximum radius within which particles are considered for hosts, in units of R200m      |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_hps_r_max_sub``            | float   | 2.0         | Maximum radius within which particles are considered for subs, in units of R200m       |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_hps_r_max_orb_host``       | float   | 3.0         | Maximum radius within which orbiting ptls. are considered for hosts, in units of R200m |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_hps_r_max_orb_sub``        | float   | 3.0         | Maximum radius within which orbiting ptls. are considered for subs, in units of R200m  |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_hps_r_unbinding_host``     | float   | 1.0         | Radius within which host particles are considered for unbinding, in units of R200m     |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_hps_r_unbinding_sub``      | float   | 1.0         | Rad. within which subhalo ptl are considered for unbinding, in units of R200m at infall|
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| ``anl_hps_iterative_unbinding``  | bool    | ``FALSE``   | Unbind once or iteratively, taking new halo membership into account                    |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 

The convention for choosing mass and radius definitions is described in 
:doc:`intro_conventions_halodef`.

Note that choosing redshifts using the ``anl_hps_redshifts`` parameter saves memory and disk space, 
but can lead to issues when creating halo catalogs with MORIA. If in doubt, it is probably best to
output the analysis for all snapshots.

The ``anl_hps_r_max_so_host`` and ``anl_hps_r_max_sub`` parameters give the maximum radius to 
which all-particle SO radii are allowed. These radii do contribute to the particle search radius,
that is, we guarantee that all particles are available within those radii. Thus, if they are set
to a value larger than other radii (such as tracer radii), they may slow down the code. 

However, for hosts, the ``anl_hps_r_max_so_host`` parameter can safely be left at a value near 
1 as long as no definition with a larger radius than R200m is chosen. If a larger radius is 
chosen, e.g., 180m, then this radius can be slightly larger than R200m at high redshift (note that
Rvir approaches R178m at high redshift). Importantly, making ``anl_hps_r_max_so_host`` or 
``anl_hps_r_max_sub`` larger does not affect run time if there are other, larger radii already 
forcing the code to look for particles further out. Conversely, we do not use any particles 
beyond those radii even if they are available, for example for density profiles, in order to 
prevent any dependence on extraneous factors.

If the threshold cannot be reached within the search radius, i.e., if the density is higher than
the threshold at all considered radii, the mass/radius cannot be computed and the status field
will indicate that (see below). When the search radius is near 200m, some small (typically less
than 100 particle) halos may not be assigned a valid 200m by this analysis due to numerical 
reasons. If you care about such halos, please increase ``anl_hps_r_max_so_host``.

The corresponding parameter for subhalos, ``anl_hps_r_max_sub``, sets the search radius in units
of R200m at infall. Note that SO radii are generally not well-defined for subhalos due to the
contribution of mass from the host. Thus, it is not recommended to increase ``anl_hps_r_max_sub``
to large values only to get SO radii that are more or less meaningless anyway, unless that is
the specific purpose of the calculation. We do not output radii/masses if the threshold was not
reached as they are physically meaningless and will totally depend on ``anl_hps_r_max_sub``.

For ``tcr`` masses, however, we do allow radii/masses where the outer threshold was not reached,
as we consider their particle set to be finite (unlike the overall particle distribution). Thus, 
their total mass is well-defined even if they never formally reach a threshold. 

If ``anl_hps_iterative_unbinding`` is on, the unbinding procedure (for ``_bnd`` masses) is 
performed iteratively, meaning that the potential is recomputed without particles that have 
previously been unbound. This procedure should, in principle, lead to more accurate results, but 
can also be very time-consuming. In practice the results are not different enough to warrant the
computational cost. See also the ``potential_err_tol`` parameter in the general configuration 
(:doc:`run_config`).

.. rubric:: Output fields

+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| Field                    | Type     | Dimensions                                        | Exists if                           | Explanation                                                                        |
+==========================+==========+===================================================+=====================================+====================================================================================+ 
| ``halo_first``           | int64    | ``n_halos``                                       | Always                              | The index of the first analysis for each halo (or -1 if none exists for a halo).   |
+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``halo_n``               | int32    | ``n_halos``                                       | Always                              | The number of analyses of this type for each halo (can be 0).                      |
+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``halo_id``              | int63    | ``n_al_haloprops``                                | Always                              | The (original, first-snapshot) halo ID to which this analysis refers.              |
+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``<halo defn>``          | float    | ``n_al_haloprops`` * ``n_snaps``                  | Always                              | One field for each halo definition (see above)                                     |
+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 
| ``status_<halo defn>``   | int8     | ``n_al_haloprops`` * ``n_snaps``                  | Always                              | Status for each definition and redshift                                            |
+--------------------------+----------+---------------------------------------------------+-------------------------------------+------------------------------------------------------------------------------------+ 

Here, ``n_snaps`` is, of course, the number of redshifts chosen by the user of the number of
snapshots in the simulation if ``anl_rsp_redshifts`` is -1. The status field can take on the
following values:

+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| Value | Parameter                             | Explanation                                                                                              |
+=======+=======================================+==========================================================================================================+ 
| 0     | ``ANL_HPS_STATUS_UNDEFINED``          | Placeholder, should never occur in output file                                                           |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 1     | ``ANL_HPS_STATUS_SUCCESS``            | The analysis succeeded, all output values can be used                                                    |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 2     | ``ANL_HPS_STATUS_HALO_NOT_VALID``     | Halo could not be analyzed at this snapshot, e.g. because it didn't exist                                |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 3     | ``ANL_HPS_STATUS_HALO_NOT_SAVED``     | Halo was not saved to the SPARTA output file at all (used in MORIA)                                      |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 4     | ``ANL_HPS_STATUS_NOT_FOUND``          | Analysis not found for this halo (used in MORIA)                                                         |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 5     | ``ANL_HPS_STATUS_SO_TOO_SMALL``       | The density was lower than the SO threshold everywhere within the search radius                          |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 6     | ``ANL_HPS_STATUS_SO_TOO_LARGE``       | The density was higher than the SO threshold everywhere within the search radius                         |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| 7     | ``ANL_HPS_STATUS_ORB_ZERO``           | There were no orbiting particles                                                                         |
+-------+---------------------------------------+----------------------------------------------------------------------------------------------------------+ 

The ``ANL_HPS_STATUS_SO_TOO_LARGE`` status occurs frequently for subhalos, where an SO 
boundary can often not be obtained because the mass profile is dominated by the host halo.
