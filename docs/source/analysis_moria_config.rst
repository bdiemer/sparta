***************************************************************************************************
Configuring MORIA
***************************************************************************************************

The following is a complete list of MORIA's configuration parameters, with some explanations 
below. See :doc:`run_config` for the syntax of the config file. Parameters with default ``NONE`` 
must be set by the user or the code will throw an error.

+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| Parameter                        | Type    | Default       | Explanation                                                                            |
+==================================+=========+===============+========================================================================================+ 
| *Input*                                                                                                                                             |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``sparta_file``                  | string  | ``NONE``      | The path to the SPARTA output file                                                     |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``catalog_dir``                  | string  | <from sparta> | The path to the catalog dir (by default taken from SPARTA config)                      |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``main_def_catalog``             | string  | M200m_bnd     | The fiducial (host/sub) halo mass/radius definition of the input halo catalog          |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``rockstar_strict_so``           | bool    | ``NONE``      | If Rockstar catalog, was it run with the STRICT_SO_MASSES option turned on?            |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``rockstar_bound_props``         | bool    | ``NONE``      | If Rockstar catalog, was it run with the BOUND_PROPS option turned on?                 |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``cosmo_flat``                   | int     | 1             | Cosmology: flat?                                                                       |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``cosmo_omega_b``                | float   | ``NONE``      | Cosmology: baryon density of the universe                                              |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``cosmo_sigma8``                 | float   | ``NONE``      | Cosmology: normalization of power spectrum                                             |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``cosmo_ns``                     | float   | ``NONE``      | Cosmology: slope of the primordial power spectrum                                      |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``cosmo_omega_r``                | float   | 0.0           | Cosmology: energy density of radiation in the universe                                 |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``cosmo_w``                      | float   | -1.0          | Cosmology: dark energy equation of state parameter                                     |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``cosmo_t_cmb``                  | float   | 2.725         | Cosmology: CMB temperature at z = 0 (in Kelvin)                                        |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``cosmo_self_similar``           | bool    | ``FALSE``     | Cosmology: if True, use power-law power spectrum P ~ k^ns, with -3 < ns < 0            |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| *Console output*                                                                                                                                    |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``log_level``                    | int     | 1             | Same meaning as in SPARTA config file                                                  |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``log_level_memory``             | int     | 2             | Same meaning as in SPARTA config file                                                  |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``log_level_timing``             | int     | 1             | Same meaning as in SPARTA config file                                                  |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``log_substatus_diff``           | bool    | ``FALSE``     | Print detailed output about differences in subhalo assignment compared to catalog      |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| *Error levels*                                                                                                                                      |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``err_level_more_snaps_than_cat``| int     | 1             | Ignore/warning/error when more snapshots that catalog files are found                  |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``err_level_snap_outside_range`` | int     | 1             | Ignore/warning/error when a requested snap is outside the redshift range of the sim.   |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``err_level_main_def_not_200m``  | int     | 1             | Ignore/warning/error when the main catalog definition is not R/M200m                   |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``err_level_acc_rate_undefined`` | int     | 1             | Ignore/warning/error when the accretion rate at this snapshot cannot be det. at all    |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``err_level_cat_field_changed``  | int     | 1             | Ignore/warning/error when catalog field name changed in HDF5 file (for compatibility)  |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| *Algorithm*                                                                                                                                         |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``cut_def``                      | string  | Vmax_peak_cat | The halo mass/radius definition that determines the threshold for output               |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``cut_threshold``                | int     | 1000          | The threshold value in units of ``cut_units``                                          |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``cut_units``                    | string  | particles     | Can be ``none``, ``particles``, ``particles_vmax`` depending on ``cut_def``            |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``order_def``                    | string  | Vmax          | The halo mass/radius definition used to order halos during subhalo assignment          |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``hostsub_assign_subparent_id``  | bool    | ``FALSE``     | Give subhalos of subhalos the ID of that sub (``TRUE``) or of its host (``FALSE``)?    |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``hostsub_restrict_to_output``   | bool    | ``FALSE``     | Only consider halos written to file (that pass the cut) for host-sub relations         |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``include_ghosts``               | bool    | ``TRUE``      | Include ghost halos in the MORIA catalogs and tree                                     |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``adjust_phantom_xv``            | bool    | ``TRUE``      | Set the position and velocity to those measured by SPARTA, if available                |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``max_mass_ratio_sparta``        | float   | 2.0           | When SPARTA mass over catalog mass is greater, use the catalog mass for calculations   |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``max_rsp_model_diff_factor``    | float   | 2.0           | If interpolated/extrapolated Rsp/Msp fractionally differ more from model, do not use   |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``max_rsp_model_diff_sigma``     | float   | 5.0           | If interpolated/extrapolated Rsp/Msp differ more than x sigma from model, do not use   |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| *Output*                                                                                                                                            |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_dir_original``          | string  | moria_original| Output directory for files in original format (filename is same as input)              |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_dir_hdf5``              | string  | moria_catalogs| Output directory for catalog files in hdf5 format (filename is auto-generated)         |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_dir_tmp``               | string  | .             | Output directory for temporary files (restart and temporary tree files)                |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_filename_tree``         | string  |moria_tree.hdf5| Filename for tree output file (can contain a file path that will be created)           |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_restart_files``         | bool    | ``TRUE``      | Write restart files from which the code can restart if it crashes                      |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_restart_every``         | int     | 10            | Number of snapshots between restart files                                              |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_original``              | bool    | ``TRUE``      | Whether catalogs are output in the format of the input halo catalog                    |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_hdf5``                  | bool    | ``TRUE``      | Whether catalogs are output in MORIA's native HDF5 format                              |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_tree``                  | bool    | ``FALSE``     | Whether a tree file is output                                                          |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_compression_level``     | int     | <from sparta> | Compression level for MORIA output files; by default the level set in SPARTA           |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_a``                     | list    | -1 (all)      | A scale factor or list of scale factors or -1 for all snapshots                        |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_rm_defs``               | list    | ``NONE``      | A list of mass/radius definitions to output (see below)                                |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_sub_defs``              | list    | ``NONE``      | A list of mass/radius definitions for which to compute subhalo assignments             |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 
| ``output_cat_fields``            | list    | ``NONE``      | A list of fields from the input catalog that are copied into the output catalog        |
+----------------------------------+---------+---------------+----------------------------------------------------------------------------------------+ 

.. rubric:: What are strict-SO and bound-props?

The ``rockstar_strict_so`` parameter may seem a little esoterical, but is important and must be set
by the user if Rockstar catalogs are used. This parameter, called ``STRICT_SO_MASSES`` in the
Rockstar config, determines if Rockstar includes all particles in its SO calculation or only those 
bound to the FOF group. This setting is not apparent from the Rockstar output file, meaning that 
MORIA cannot know the true meaning of the definitions without this information.

Similarly, the ``BOUND_PROPS`` parameter in Rockstar determines what is output for certain halo
properties, including the "main" mass definition in the catalog. By default, ``BOUND_PROPS`` is
on, meaning that the main definition is bound-only even if ``STRICT_SO_MASSES`` makes the 
alternative mass definition include all particles.

Depending on these switches, requesting certain catalog definitions may lead to failure. For 
example, if ``STRICT_SO_MASSES = 1`` and ``BOUND_PROPS = 0``, the outputs include all 
particles and we cannot get any ``*_bnd`` masses. Similarly, if ``STRICT_SO_MASSES = 0``, we cannot
get any ``*_all`` definitions (from the catalog, we can from SPARTA).

.. rubric:: Why so many cosmological parameters?

MORIA needs to compute certain power-spectrum related quantities such as peak height, which is why
it needs more cosmological information than can be extracted from some simulation code outputs
(e.g., Gadget2).

If ``cosmo_self_similar`` is turned on, the cosmology changes from a standard LCDM cosmology to a
self-similar universe with a power-law spectrum. In this case, we assume that ``ns`` is the slope
of that spectrum, P(k) ~ k^ns, with ns between -3 and 0. The matter density should typically be 
the critical density, ``Omega_m = 1``, but this is set in SPARTA and not in the MORIA 
configuration.

.. rubric:: Mass/radius definitions

All halo mass/radius definitions are given in the standard :doc:`intro_conventions_halodef`
format. Of course, the definition(s) requested must make sense in the respective context. In 
particular:

* The definitions chosen for ``cut_def`` and ``order_def`` must be catalog definitions, i.e., they
  must exist in the input halo catalog and for all halos.
* The ``main_def_catalog`` should, if at all possible, be a definition as close to M200m_all as
  possible. This field is used if (in some very rare cases) SPARTA failed to compute an internal
  M200m_all for the halo, or (more likely) if the halo does not exist in SPARTA.
* The definitions chosen for ``output_rm_defs`` and ``output_sub_defs`` can be definitions from 
  either the input catalog or SPARTA, and must carry the ``_spa`` or ``_cat`` identifiers if they
  can be computed by both. For example, SO definitions must be listed as ``M200c_all_cat`` and
  ``M200c_all_spa`` because they can appear in both, whereas ``Rsp-apr-mn`` can only be computed
  by SPARTA and is thus unambiguous.
* For Rockstar catalogs, mass definitions are usually given as masses but not radii. MORIA 
  automatically converts radii and masses into one another if necessary. For example, it is OK
  to demand ``R500c_all_cat`` if ``M500c`` is part of the catalog and ``STRICT_SO`` was on.
* The same goes for SO definitions from the SPARTA halo properties analysis. Here, the user can
  freely demand radii and masses as long as either one of the two is present in the SPARTA file.

.. rubric:: Output threshold

The threshold for inclusion in the output catalog is important: only halos that are well-defined in
the input catalog and that were output by SPARTA should be included. For example, if ``output_min_n200m``
is set to 500 in SPARTA (see :doc:`run_config`), it does not make sense to set 
``cut_def`` to ``M200m_all``, ``cut_units`` to particles, and ``cut_threshold`` to 200: in that 
case, we would be including halos down to 200 particles even though they are not included in the 
SPARTA file.

The ``particles_vmax`` unit choice converts the given number of particles, that is, a minimum halo 
mass, into a circular velocity using the empirical fitting function of fitting function of
`Klypin et al. 2011 <https://ui.adsabs.harvard.edu/abs/2011ApJ...740..102K/abstract>`_,

.. math::

    v_{\rm max} \approx 2.8 \times 10^{-2} M_{\rm vir}^{0.316}

We note that this formula is only used to convert a particle limit into a :math:`v_{\rm max}` 
limit, which is then compared to :math:`v_{\rm max}` as measured by the halo finder for each 
individual halo. Thus, a limit of 100 particles means the :math:`v_{\rm max}` that, on 
average, corresponds to a virial mass of 100 times the simulation's particle mass.

.. rubric:: Host-sub relations

MORIA assigns parent IDs by sorting the halos by ``order_def`` and the looking for subhalos within
halos, starting with the first (largest) halo. By default, subhalos do not have subhalos themselves.
However, there is a case where a subhalo is also the subhalo of a subhalo within the same host.
In this case, we can assign either the host or the larger subhalo as parent. The latter is only
done if ``hostsub_assign_subparent_id`` is turned on.

Another subtlety arises because the order definition is not necessarily the same as the cut
definition, meaning that some halos might have subhalos but not be included in the output. By
default, we still assign such halos as parents, especially because they may be output in the tree.
This choice means that the results of running MORIA on a single snapshot are the same as creating
a tree where we run on all snapshots and always consider all halos, whether they make the cut
at a given snapshot or not. However, the user can change this behavior by turning on 
``hostsub_restrict_to_output``, which means that only halos in the catalog file at this redshift
will be considered as hosts. Note that this can lead to small deviations in the host-sub 
assignment even if the same definition as in the original halo catalog is used.

.. rubric:: Tree output

If ``output_tree`` is set, the list of scale factors in ``output_a`` is ignored because we need to
run over all snapshots to construct the tree.

.. rubric:: Compression level

The ``output_compression_level`` parameter has the same meaning as the corresponding parameter in
the SPARTA config. Compared to no compression (0), a value of 1 provides almost all the benefit at
very modest computational expense. In practice, raising the compression level does not lead to much
smaller MORIA output files.

.. rubric:: Example config file

The following file contains all currently supported MORIA parameters and their default values
wherever applicable. The file can be found at ``/config/moria.cfg``:

.. rubric:: Rsp/Msp interpolation and extrapolation

MORIA increases the completeness of the splashback values in the catalogs by interpolating missing
values between valid snapshots, or even extrapolating from past and/or future snapshots. For a 
snapshot to be considered, it must be within one dynamical time, the halo must have been a host, 
and a valid SPARTA value for all splashback definitions must have been computed. To avoid extreme
values, we also check Rsp/R200m or Msp/M200m from that snapshot against the fitting function. If 
the values are far off, they may not be wrong -- but we may not want to extrapolate them since the
halo may have been in a unique situation, such as a merger event, mass loss etc. Thus, we accept a
value only if it either differs less than ``max_rsp_model_diff_factor`` from the model (is either
larger or smaller by that factor) or (!) if it is less than ``max_rsp_model_diff_sigma`` off the
model. The sigma value is compared to the model scatter from the same fitting function. However, 
this model scatter is formally small (as low as 5%) depending on mass and accretion rate. Thus, 
we also introduce the absolute threshold. 

.. highlight:: none

.. include:: ../../config/moria.cfg
   :literal:
