*************************************************************************************************** 
Results 
***************************************************************************************************

The following pages describe the tracer results currently implemented in SPARTA. For a general 
introduction, see :doc:`intro_tracers` and :doc:`intro_rs_al`. 

.. rubric:: Basics

Results are modules that are applied to tracers at each snapshot. They can look for particular
events in the trajectory of a tracer. The following modules are implemented:

+--------------------------+-------------------+-------------------+ 
| Result                   | Long name         | Abbreviation      |
+==========================+===================+===================+
| Infall into the halo     | ``infall``        | ``ifl``           |
+--------------------------+-------------------+-------------------+ 
| First apocenter          | ``splashback``    | ``sbk``           |
+--------------------------+-------------------+-------------------+ 
| Trajectory               | ``trajectory``    | ``tjy``           |
+--------------------------+-------------------+-------------------+ 
| Orbit counter            | ``orbitcount``    | ``oct``           |
+--------------------------+-------------------+-------------------+ 

These modules are documented in detail (see bottom of this page). However, they all follow a 
general logic of how they operate on different types of tracers in different kinds of halos. For
each tracer type (e.g., particles or subhalos), there are a number of possible scenarios:

+--------------------------+------------------------------------------------------------------------------------------------------------------+ 
| Situation                | Explanation                                                                                                      |
+==========================+==================================================================================================================+
| ``OUT``                  | Tracer is created outside halo, should be the case for particles as long as the search radius is sufficient      |
+--------------------------+------------------------------------------------------------------------------------------------------------------+ 
| ``IN``                   | Tracer is created inside R200m, meaning its infall / initial orbit(s) were not tracked                           |
+--------------------------+------------------------------------------------------------------------------------------------------------------+ 
| ``IN_NEW``               | Tracer existed in a newly created halo; of course, each halo has such particles when it is first identified      |
+--------------------------+------------------------------------------------------------------------------------------------------------------+ 
| ``IN_REBORN``            | Inside a backsplash halo that was a subhalo and is a host halo again (new tracers are not added in subhalos)     |
+--------------------------+------------------------------------------------------------------------------------------------------------------+ 
| ``SUBTRACK_NEW``         | Newly created tracer to follow a particle that was tagged as belonging to a subhalo (but was not tracked before) |
+--------------------------+------------------------------------------------------------------------------------------------------------------+ 
| ``SUBTRACK_CONTD``       | Tracer existed and will persist as halo becomes subhalo                                                          |
+--------------------------+------------------------------------------------------------------------------------------------------------------+ 
| ``GHOST``                | Tracer in halo that has become a ghost                                                                           |
+--------------------------+------------------------------------------------------------------------------------------------------------------+ 
| ``HAS_RES``              | Tracer is newly created but a previously existing result was found (e.g. because tracer was temporarily deleted) |
+--------------------------+------------------------------------------------------------------------------------------------------------------+ 
| ``TCR_RECREATED``        | Tracer had been deleted and has now been recreated (e.g., because a particle strayed far from the halo)          |
+--------------------------+------------------------------------------------------------------------------------------------------------------+ 

Note that subhalos cannot host their own subhalos in SPARTA, meaning that ``SUBTRACK_NEW``, 
``SUBTRACK_CONTD``, and ``GHOST`` cannot occur for subhalo tracers. Similarly, ``TCR_RECREATED``
cannot occur because subhalo tracers are never deleted unless the halo stops to exist in the halo
catalog.

For each of the possible situations, each result has an "instruction" in SPARTA, meaning a status
to which the tracer is set if the situation occurs. The following tables show these instructions
for both particle and subhalo tracers:

.. image:: ../images/sparta_results_instr_ptl.jpeg
    :scale: 50 %
    :align: center

.. image:: ../images/sparta_results_instr_sho.jpeg
    :scale: 50 %
    :align: center

|

The infall, orbit count, and trajectory results are greedy in that they attempt to run whenever 
possible, with some exception for ghosts where we expect the trajectories to become very 
unreliable. The splashback results are restricted to host halos as subhalos and ghosts are not
expected to have well-behaved trajectories and apocenters. 

.. rubric:: Documentation of analysis modules

Each of the following pages documents the compile-time settings, run-time settings, and output
for each type of result.

.. toctree::
    :maxdepth: 1

    run_rs_ifl
    run_rs_sbk
    run_rs_tjy
    run_rs_oct
    