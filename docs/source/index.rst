***************************************************************************************************
SPARTA Documentation
***************************************************************************************************

SPARTA is an acronym for **S**\ ubhalo and \ **PAR**\ ticle **T**\ rajectory **A**\ nalysis. The 
purpose of SPARTA is to provide a framework for large-scale, dynamical analyses of simulations 
of structure formation. Here, "dynamical" means that SPARTA considers not only one snapshot but 
rather an entire simulation across cosmic time. As SPARTA is based on a number of somewhat abstract 
concepts and numerical objects, it is highly recommended that you read at least some of the 
:doc:`intro` pages before attempting to run SPARTA.

.. rubric:: Citing SPARTA

If you use SPARTA for a publication, please cite the two code papers 
(`Diemer 2017 <https://ui.adsabs.harvard.edu/abs/2017ApJS..231....5D/abstract>`_ and 
`Diemer 2020a <https://ui.adsabs.harvard.edu/abs/2020arXiv200709149D/abstract>`_). The halo density
profile module is described in `Diemer 2022 <https://arxiv.org/abs/2112.03921>`_.
If you would like to develop SPARTA, please feel free to get in touch! 

Copyright (c) 2015-2022 Benedikt Diemer (diemer@umd.edu) / License: GNU GPLv3

.. rubric:: Contents

.. toctree::
    :maxdepth: 2

    versions
    intro
    run
    analysis
    developers

.. rubric:: Search

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
