***************************************************************************************************
Analyses
***************************************************************************************************

The following pages describe the halo-wide analyses currently implemented in SPARTA. For a general 
introduction, see :doc:`intro_rs_al`. The following modules are implemented:

+--------------------------+-------------------+-------------------+ 
| Type of analysis         | Long name         | Abbreviation      |
+==========================+===================+===================+
| Splashback radius        | ``rsp``           | ``rsp``           |
+--------------------------+-------------------+-------------------+ 
| Density profiles         | ``profiles``      | ``prf``           |
+--------------------------+-------------------+-------------------+ 
| Halo properties          | ``haloprops``     | ``hps``           |
+--------------------------+-------------------+-------------------+ 

Each of the following pages documents the compile-time settings, run-time settings, and output
for each type of analysis.

.. toctree::
    :maxdepth: 1

    run_al_rsp
    run_al_prf
    run_al_hps
    