***************************************************************************************************
Orbit count results
***************************************************************************************************

The orbit counting result counts pericenters. This information can be used to separate particles
into a 1-halo term (particles that have undergone at least one pericenter) and material on a first
infall trajectory.

.. rubric:: Algorithm

As a tracer is created, we set its orbit counter to zero. If the tracer is born outside the halo, 
we know that this count is reliable. If it is born inside or under other dubious circumstances, 
we set the ``n_is_lower_limit`` flag to indicate that there could have been pericenters before the
count was started. 

Similar to the :doc:`run_rs_sbk`, we look for a change in radial velocity from negative to positive.
Each time such a switch occurs, we increase the orbit counter. Unlike other results, the orbit
count result would never end, i.e., a  tracer can always accumulate more pericenters. This means 
that tracers are being kept alive even if all their other results have been recorded. To avoid 
unnecessary tracers that cost memory and computation, the user can set a maximum number of 
pericenters, ``res_oct_max_norbit``. 

The orbit count result allows us to tell which tracers are on a first infall into the halo (zero
pericenters), which are on their first orbit, or have had multiple orbits. This information can 
then be used for example in the :doc:`run_al_prf`.

.. rubric:: Compile-time parameters

+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| Parameter                                          | Explanation                                                                                              |
+====================================================+==========================================================================================================+ 
| ``OUTPUT_RESULT_ORBITCOUNT``                       | Write orbit count results to output file                                                                 |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_ORBITCOUNT_OCT``                   | Save the orbit count to the output file (the result makes little sense without this number)              |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_ORBITCOUNT_LOWER_LIMIT``           | Save a flag indicating whether the orbit count is a lower limit (recommended)                            |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_ORBITCOUNT_LAST_SNAP``             | Save the snapshot of infall/where lower limit was set/where last pericenter was detected                 |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 

.. rubric:: Run-time parameters

+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 
| Parameter                        | Type    | Default     | Explanation                                                                            |
+==================================+=========+=============+========================================================================================+ 
| ``res_oct_max_norbit``           | int     | 3           | When a tracer has completed this many orbits, stop counting                            |
+----------------------------------+---------+-------------+----------------------------------------------------------------------------------------+ 

.. rubric:: Output fields

+--------------------------+----------+----------------------------+----------------------------------------+------------------------------------------------------------------------------------+ 
| Field                    | Type     | Dimensions                 | Exists if                              | Explanation                                                                        |
+==========================+==========+============================+========================================+====================================================================================+ 
| ``halo_first``           | int64    | ``n_halos``                | Always                                 | The index of the first result for each halo (or -1 if none exists for a halo).     |
+--------------------------+----------+----------------------------+----------------------------------------+------------------------------------------------------------------------------------+ 
| ``halo_n``               | int32    | ``n_halos``                | Always                                 | The number of results of this type for each halo (can be 0).                       |
+--------------------------+----------+----------------------------+----------------------------------------+------------------------------------------------------------------------------------+ 
| ``tracer_id``            | int64    | ``n_rs_orbitcount``        | Always                                 | The ID of the tracer to which this result refers.                                  |
+--------------------------+----------+----------------------------+----------------------------------------+------------------------------------------------------------------------------------+ 
| ``n_pericenter``         | int16    | ``n_rs_orbitcount``        | OUTPUT_RESULT_ORBITCOUNT_OCT           | The number of orbits (completed pericenters).                                      |
+--------------------------+----------+----------------------------+----------------------------------------+------------------------------------------------------------------------------------+ 
| ``n_is_lower_limit``     | int8     | ``n_rs_orbitcount``        | OUTPUT_RESULT_ORBITCOUNT_LOWER_LIMIT   | Whether ``n_pericenter`` is a lower limit (a tracer's origin was not tracked).     |
+--------------------------+----------+----------------------------+----------------------------------------+------------------------------------------------------------------------------------+ 
| ``last_pericenter_snap`` | int16    | ``n_rs_orbitcount``        | OUTPUT_RESULT_ORBITCOUNT_LAST_SNAP     | If no orbit counts recorded, the snapshot of infall; if no orbit counts but a lower|
|                          |          |                            |                                        | limit, the snapshot where the lower limit was set; if the orbit count is greater   |
|                          |          |                            |                                        | than zero, the last snapshot where a pericenter was detected.                      |
+--------------------------+----------+----------------------------+----------------------------------------+------------------------------------------------------------------------------------+ 
