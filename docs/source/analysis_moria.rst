***************************************************************************************************
Creating halo catalogs with MORIA
***************************************************************************************************

MORIA is an extension of SPARTA that post-processes output files to create user-defined halo 
catalogs. The name refers to sacred, publicly owned olive trees in ancient Greece, hinting at 
MORIA's dealing with merger trees.

.. rubric:: Basics

SPARTA stores its results in a single HDF5 file on a per-halo basis, where halos are defined as in
the original, input halo catalog. For many applications, however, we wish to create an altered halo 
catalog, for example, one containing splashback radii and masses. The purpose of MORIA is to 
create such catalogs in post-processing, by combining the input catalogs with SPARTA output:

|

.. image:: ../images/sparta_schematic.jpeg
    :scale: 50 %
    :align: center

|

There are a number of reasons why MORIA is a separate tool and why it is run in post-processing,
most importantly:
    
* SPARTA can take hours to run on many cores, whereas MORIA typically runs quickly and on one
  process only.
* The output from SPARTA can be used for many different purposes, and be used in many different
  halo catalogs. MORIA allows the user to easily create such catalogs without re-running SPARTA.
* MORIA is entirely configured at run-time, meaning it does not need to be re-compiled for 
  different operations in the way SPARTA does.

MORIA is, however, written in pure C in order to deal with very large catalog files. Some of the
most important features are:

* MORIA works snapshot by snapshot, the user can choose which redshifts halo catalogs should be
  created for.
* MORIA tries to improve the completeness of catalogs, for example, by approximating SPARTA 
  results such as splashback radii where they could not be computed by SPARTA.
* An important part of creating the catalogs is to compute host-sub relations for a variety of
  user-defined halo radius definitions, including spherical overdensity and splashback radii. 
* MORIA can output both a native HDF5 catalog format as well as the format of the input halo
  finder.
* MORIA can output a HDF5-based tree format where all halos and snapshots are combined into one 
  file.

.. rubric:: How are host-subhalo relations computed?

One of the main tasks of MORIA is to compute host-subhalo relations for a number of definitions
requested by the user. The following schematic highlights a few situations:

.. image:: ../images/moria_hostsub.jpeg
    :scale: 50 %
    :align: center

Each assignment is output in a ``parent_id_<def>`` field. A parent ID (PID) of ``-1`` indicates 
that the halo is a host. If a halo is the sub of a subhalo but outside other, larger halos, it 
is still counted as a subhalo (halo 5 above). In this case, we can assign it the ID of its direct 
parent (the default behavior) or the ID of the host's host (if ``hostsub_assign_subparent_id`` is
set). If a halo is a sub of a subhalo but inside the host's host, it is always assigned the primary
host's ID (1 in the case of halo 3 above). In Rockstar, these options are distinguished by the 
``id`` and ``upid`` fields, in MORIA we always assign the equivalent of ``upid``.

The figure also illustrates a case where the smaller halo (according to the radius definition in
question) is host to a larger subhalo (IDs 8 and 9). The reason for this assignment is that
the smaller halo has the larger mass in the definition used to order the halos (``order_def``), 
in this example :math:`V_{\rm max}`.

To establish host-sub relations, each halo must have some estimate of a radius for each definition, 
which causes trouble in cases where no such radius is available. MORIA solves such cases as 
follows:

* For splashback radii, a radius is always computed, reconstructed, or guessed (see above).
* For SO radii from the catalog, the radius can be zero, for example in cases where the density
  never reaches a particular threshold. We keep the zero radius, meaning that such halos are 
  deemed too small to host subhalos.
* For halos where the ``SO_TOO_SMALL`` status was set by SPARTA, we similarly set a radius to
  zero as the real radius is too small to be resolved.
* For halos where the ``SO_TOO_LARGE`` status was set by SPARTA, the situation is more 
  complicated. Most of those halos are subhalos where the profile included the host halo, and thus
  never went down to the desired density threshold. This leaves us with no real information about
  the halo's "true" size. We thus again set the radius to zero, as the subhalo is likely close
  to the host's center anyway. 
* Depending on the chosen cut threshold for halos, some halos may not be found in the SPARTA 
  file. This indicates a poor matching of the lower mass cutoff in SPARTA and MORIA, since the
  MORIA cutoff should ideally be more conservative. However, when setting a threshold that is
  not simply as mass such as Vmax, some halos may not have had sufficient mass to be output by
  SPARTA even though they make a reasonably conservative Vmax cut. Again, we set their radius to
  zero; this is not physical, and the user is encouraged to always check that the vast majority of
  halos in a MORIA output catalog should have been found by SPARTA 
  (see :doc:`analysis_moria_exec`).

In practice, as long as not too many halos are missing in SPARTA, the impact on the overall 
statistics of the host-sub assignments should be small. 

One final note: the host-sub assignments in MORIA use all halos in the catalog, regardless of the
mass (or other) cut the user has imposed. Thus, it is possible that some halos are subhalos of
halos that are not written to the final catalog (or tree) files. This situation can be avoided if
the ordering and cut quantities are the same (see :doc:`analysis_moria_config`).

.. rubric:: Contents

.. toctree::
    :maxdepth: 1

    analysis_moria_config
    analysis_moria_exec
    