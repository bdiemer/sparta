***************************************************************************************************
Compile-time settings
***************************************************************************************************

SPARTA relies on a significant number of compile-time parameters that are necessary to properly
size arrays and reduce memory consumption. The only file that the user should need to modify for 
this purpose is ``sparta.h`` (in the build directory created above, NOT in the file that is part 
of the SPARTA repository). The relevant sections in this file should be self-documenting. All 
compile switches are written to the SPARTA output file (see :doc:`analysis_hdf5`).

The sample file below has all tracers, results, and analyses set to zero, providing a minimal 
example where SPARTA will only read the halo catalog and put out a file with minimal content.
Running in this mode can be a good first check on the consistency of the halo catalog, snapshot
files and so on.

.. highlight:: c

.. include:: 
    ../../build/sparta.h 
    :literal:

.. rubric:: Compile-time settings for MORIA

The compile system for MORIA works just as the SPARTA one, but MORIA needs barely any compile-time
input from the user:
    
.. include:: 
    ../../build/moria.h 
    :literal:

.. rubric:: Compile-time settings for test suite

The test suite is provided for developers. Different tests can be activated in its header file:

.. include:: 
    ../../build/tests.h 
    :literal:
   