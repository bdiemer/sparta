***************************************************************************************************
Python analysis package
***************************************************************************************************

All output from SPARTA and MORIA is written to user-friendly HDF5 files that can be read and 
analyzed with any HDF5 software (such as the HDF5 viewer) or programming interface (including 
python, C, C++, Fortran, and many others). 

However, the raw output from SPARTA can still be fairly complex depending on the operations 
performed, and reading it without introducing errors can be tricky. This python module provides
high-level functions to load SPARTA files as well as the much simpler MORIA catalogs and trees.
It also provides certain utilities that help with the interpretation of the files, for example
halo statuses.

Note that this module does not interact with SPARTA at run-time, it is intended as a post-
processing tool for SPARTA files.

.. rubric:: Contents

.. toctree::
    :maxdepth: 1

    analysis_python_sparta
    analysis_python_moria
    analysis_python_utils
