------------------------------
Acknowlegments
------------------------------

While SPARTA was more or less developed from scratch, a few code components were adapted from other
codes and modified for use in SPARTA. Such adaptions are highlighted by comments in the respective
code files, but for completeness, the developer would like to acknowlege the following 
contributions:

* The tree search algorithm and the gravitational potential tree-based calculation were adapted 
  from Peter Behroozi's ROCKSTAR code (see 
  `repository <https://bitbucket.org/gfcstanford/rockstar/src/master/>`_ and 
  `code paper <https://ui.adsabs.harvard.edu/abs/2013ApJ...762..109B/abstract>`_).
* The Peano-Hilbert ordering in the domain decomposition module was adapted from the ART code by
  Andrey Kravtsov and Douglas Rudd (see, e.g., 
  `Kravtsov et al. 1997 <https://ui.adsabs.harvard.edu/abs/1997ApJS..111...73K/abstract>`_ and 
  `Rudd et al. 2008 <https://ui.adsabs.harvard.edu/abs/2008ApJ...672...19R/abstract>`_).
* Some routines in the cosmology module were adapted from the 
  `CosmoCalc code <https://github.com/beckermr/cosmocalc>`_ by Matt Becker

Finally, Benedikt Diemer would like to acknowlege the generous support of an ITC fellowship and
an Einstein fellowship at the Harvard-Smithsonian Center for Astrophysics and at the University
of Maryland. The majority of the code development was completed during those years and would not
have been possible without long-term funding.
