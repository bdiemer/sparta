***************************************************************************************************
Compiling SPARTA
***************************************************************************************************

As SPARTA is written in pure C, it should build on any Linux-like system. However, before building 
the system, a number of compiler flags need to be set as described below.

.. rubric:: Dependencies

SPARTA was designed to have as few dependencies as possible, but the following libraries are 
inevitable:

* MPI
* gsl 2.0 (earlier versions are missing 2D interpolation functions)
* HDF5 1.8 (version 1.6.x will not compile due to interface differences; 1.10 is preferred as it 
  contains numerous bug fixes)    

Please make sure these libraries are installed and in the include/library paths so the compiler
can find them.

.. rubric:: Compiling SPARTA

The idea of SPARTA's compile system is that there is a central header file, ``sparta.h``, that 
contains all user-defined compile-time input. To avoid changing this file for each build (and 
accidentally adding these changes to the repository), the user should create one (or many) build 
directories in a location of their choice. Then, follow these steps:

* create a new build directory
* copy ``build/sparta.h`` and ``build/Makefile`` to the build directory
* also copy ``build/moria.h`` and ``build/tests.h`` if you want to compile MORIA and the test
  suite
* set the ``SPARTA_DIR`` variable in ``Makefile`` to the relative or absolute path where the
  SPARTA code is located
* set PLATFORM to one of the platforms listed in the ``build/main.make`` file, and/or set
  any machine-specific settings (such as ``CFLAGS`` or ``LD_FLAGS``) in ``Makefile``

At this point, ``Makefile`` could look like this::

    SPARTA_DIR := /my/home/dir/code/sparta
    PLATFORM := osx
    CFLAGS += -I$(PWD)
    include $(SPARTA_DIR)/build/main.make

Now simply execute ``make`` in your build directory. If you also want to compile MORIA and 
the test suite, use ``make all`` instead.

.. rubric:: Troubleshooting: GSL

If GSL is not found, make sure that the directory where the gsl include files are is included in 
your path via the ``CFLAGS``. When compiling, you should see it explicitly listed, e.g.::

    mpicc -I/home/you/build -Wall -O2 -I/home/you/gsl/include -c /home/you/sparta/src/geometry.c -o /home/you/sparta/src/geometry.o

or something similar. The same goes for the library path, where the ``lib`` directory needs to be
added. For a typical GSL installation, the ``include`` and ``lib`` directories should be in the 
same top directory. In some cases, the compilation works but when trying to run SPARTA you get::

    error while loading shared libraries: libgsl.so.25: cannot open shared object file: No such file or directory

This means that the code was compiled with a dynamic link to GSL, and that the operating system
can now not find the GSL library. You probably need to add it to your library path in the command
line (ideally at login, e.g. in ``.bashrc``::

    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/gsl/lib
    
.. rubric:: Troubleshooting: hdf5

The hdf5 library can cause various errors on various systems. Besides adding the paths as described
for GSL above, one error can be that the linker cannot find the zlib library, which can be fixed
with::

    LD_FLAGS += -lz

.. rubric:: Troubleshooting: Linker errors

Sometimes, after compile options are changed in the header files such as ``sparta.h``, the linker
throws errors that look like missing include statements. In such cases, try executing 
``make clean`` and recompiling; the make tool sometimes does not take all the necessary 
dependencies into account.
