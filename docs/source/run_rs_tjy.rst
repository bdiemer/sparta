***************************************************************************************************
Trajectory results
***************************************************************************************************

Trajectory results are simply a vehicle to output full trajectories of tracers (even though they are
never stored in memory over the full cosmic time). The user can choose whether to output only
radial information or also full 3D positions and velocities. As trajectory information quickly
becomes very large, this result should typically only be switched on for one or a few halos (see
:doc:`run_config` for how to set such instructions).

.. rubric:: Compile-time parameters

+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| Parameter                                          | Explanation                                                                                              |
+====================================================+==========================================================================================================+ 
| ``OUTPUT_RESULT_TRAJECTORY``                       | Write trajectory results to output file                                                                  |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_TRAJECTORY_R``                     | Save the tracer's radius along the trajectory                                                            |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_TRAJECTORY_VR``                    | Save the tracer's radial velocity along the trajectory                                                   |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_TRAJECTORY_VT``                    | Save the tracer's tangential velocity along the trajectory                                               |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_TRAJECTORY_X``                     | Save the tracer's full 3-dimensional coordinates along the trajectory                                    |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 
| ``OUTPUT_RESULT_TRAJECTORY_V``                     | Save the tracer's full 3-dimensional velocity along the trajectory                                       |
+----------------------------------------------------+----------------------------------------------------------------------------------------------------------+ 

.. rubric:: Run-time parameters

This result does not add any config parameters.

.. rubric:: Output fields

+--------------------------+----------+----------------------------+------------------------------------+------------------------------------------------------------------------------------+ 
| Field                    | Type     | Dimensions                 | Exists if                          | Explanation                                                                        |
+==========================+==========+============================+====================================+====================================================================================+ 
| ``halo_first``           | int64    | ``n_halos``                | Always                             | The index of the first result for each halo (or -1 if none exists for a halo).     |
+--------------------------+----------+----------------------------+------------------------------------+------------------------------------------------------------------------------------+ 
| ``halo_n``               | int32    | ``n_halos``                | Always                             | The number of results of this type for each halo (can be 0).                       |
+--------------------------+----------+----------------------------+------------------------------------+------------------------------------------------------------------------------------+ 
| ``tracer_id``            | int64    | ``n_rs_trajectory``        | Always                             | The ID of the tracer to which this result refers.                                  |
+--------------------------+----------+----------------------------+------------------------------------+------------------------------------------------------------------------------------+ 
| ``first_snap``           | int16    | ``n_rs_trajectory``        | Always                             | The first snapshot where the trajectory was recorded.                              |
+--------------------------+----------+----------------------------+------------------------------------+------------------------------------------------------------------------------------+ 
| ``last_snap``            | int16    | ``n_rs_trajectory``        | Always                             | The last snapshot where the trajectory was recorded.                               |
+--------------------------+----------+----------------------------+------------------------------------+------------------------------------------------------------------------------------+ 
| ``r``                    | float    | ``n_rs_trajectory`` * 3    | ``OUTPUT_RESULT_TRAJECTORY_R``     | The radius of the tracer as a function of time.                                    |
+--------------------------+----------+----------------------------+------------------------------------+------------------------------------------------------------------------------------+ 
| ``vr``                   | float    | ``n_rs_trajectory`` * 3    | ``OUTPUT_RESULT_TRAJECTORY_VR``    | The radial velocity as a function of time.                                         |
+--------------------------+----------+----------------------------+------------------------------------+------------------------------------------------------------------------------------+ 
| ``vt``                   | float    | ``n_rs_trajectory`` * 3    | ``OUTPUT_RESULT_TRAJECTORY_VT``    | The tangential velocity as a function of time.                                     |
+--------------------------+----------+----------------------------+------------------------------------+------------------------------------------------------------------------------------+ 
| ``x``                    | float    | ``n_rs_trajectory`` * 3    | ``OUTPUT_RESULT_TRAJECTORY_X``     | The relative coordinates as a function of time; ``r`` is implicit in this field.   |
+--------------------------+----------+----------------------------+------------------------------------+------------------------------------------------------------------------------------+ 
| ``v``                    | float    | ``n_rs_trajectory`` * 3    | ``OUTPUT_RESULT_TRAJECTORY_V``     | The relative velocity as a function of time; ``vr`` is implicit in this field.     |
+--------------------------+----------+----------------------------+------------------------------------+------------------------------------------------------------------------------------+ 
