***************************************************************************************************
For developers
***************************************************************************************************

SPARTA was designed as a code framework and is intended to be expanded, using a modular 
architecture. This section gives both general guidelines for coding within the SPARTA framework
and manuals for how to extend it to perform new operations.

The developer documentation is incomplete at this point, please don't be disappointed if there are
many use cases missing. The following pages give a very basic introduction to some of the most
important aspects of coding within the SPARTA framework.

.. rubric:: Contents

.. toctree::
    :maxdepth: 2
    
    developers_guidelines
    developers_config
    developers_result
    developers_analysis
