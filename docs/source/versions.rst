***************************************************************************************************
Version History
***************************************************************************************************

See below for a listing of the most important code and interface changes in SPARTA. As of version
1.0, each new version will correspond to a code push to the public repository unless otherwise
noted.

.. rubric:: Version 1.1.1 (released 03/01/2022)

* Replaced time.clock() calls in loading function because this call is now deprecated. Replaced 
  with process_time(), which measures only the active time of the process (excluding sleep) and
  thus gives a more accurate reflection of the actual computation time taken.
* Replaced occurrences of np.bool / np.float / np.int with bool, float, int.
* Minor updates to documentation.

.. rubric:: Version 1.1.0 (released 12/06/2021)

This is the major version released to go with `Diemer 2022 <https://arxiv.org/abs/2112.03921>`_,
a paper on halo density profiles split into orbiting and infalling components.

* The OrbitCounter result was added. This result counts the number of pericenters a tracer has
  completed.
* The profiles analysis was added. This analysis produces radial density (or other) profiles that
  may or may not rely on dynamical properties of particles such as the number of orbits completed.
* Based on the pericenter count, the "orbiting" mass and radius definitions were added to the 
  HaloProps module. These definitions include only particles that have undergone at least one
  pericenter.
* MORIA offers a new output structure and more user control over where the different output files
  end up.
* Fixed a number of small bugs in MORIA.
* Fixed a bug that prevented ghost positions at the final snapshot from being output.

.. rubric:: Version 1.0.0 (released 06/27/2020)

The first publicly released version of SPARTA, as described in 
`Diemer 2020a <https://ui.adsabs.harvard.edu/abs/2020arXiv200709149D/abstract>`_. This release 
represents a major update including the following features:

* The MORIA tool was added to create catalogs and subhalo assignments (see :doc:`analysis_moria`).
* Reading Gadget3 files (HDF5 format)
* Restart capabilities in both SPARTA and MORIA
* Ghost halos (tracking subhalos that were lost by the halo finder)
* The halo properties analysis was added for the computation of general, non-splashback halo 
  properties such as spherical overdensity masses and radii.

The following improvements were made to the code framework:

* Designed a new compiling system where the user defines what should be output and the code
  automatically determines a large number of DO_* switches that control operations, cleanly 
  separating what needs to be done from what needs to be written to the output file.
* Changed the logic of tracers in subhalos. Instead of deleting all tracers, a set of particle
  tracers is now kept for those particles deemed to belong to the subhalo at infall. These 
  tracers can then be used to determine the properties of the subhalo even in the presence of
  host halo material.
* Based on the same logic, the tagging of subhalo particles in host halos was re-worked 
  completely. The user now has a choice of multiple tagging schemes, and can choose to tag based
  on the distance at subhalo infall which seems most robust.
* Changed internal handling of mass and radius definitions as well as config input from the user
  to new, uniquely defined format (see :doc:`intro_conventions_halodef`).
* The console output was significantly improved.

There are too many small bug fixes to enumerate in this version. The most significant fixes are:

* The Hubble drag term in the radial velocity calculation of tracers had an erroneous factor of
  h, meaning that radial velocities were underestimated. This led to splashback events happening
  slightly too early in the old version. As halos generally grow, that led to an underestimation 
  of splashback radii by a few percent.
* The halo status field contained some spurious changes of subhalo status.

.. rubric:: Version 0.9.0 (unreleased)

This was the first (unreleased) version of SPARTA used in
`Diemer 2017 <https://ui.adsabs.harvard.edu/abs/2017ApJS..231....5D/abstract>`_ and 
`Diemer et al. 2017 <https://ui.adsabs.harvard.edu/abs/2017ApJ...843..140D/abstract>`_. The chief
purpose of this version was to compute splashback radii and masses. This version contains the 
general code framework as well as infall, splashback, and trajectory results and the Rsp analysis.
Only Rockstar and LGadget2 data could be read.
