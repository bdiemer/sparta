***************************************************************************************************
The SPARTA HDF5 output format
***************************************************************************************************

The main output format for SPARTA is a single HDF5 file, which is placed in the 
output directory. The top level of the HDF5 hierarchy always contains three groups, as well as one 
further group for each tracer type. The content of these structures is explained below.

===================================================================================================
The config group
===================================================================================================

The purpose of the config group is to provide a central storage location for all configuration 
parameter as well as derived outputs that are not datasets. All config parameters are stored as
HDF5 attributes.

The main group ``/config`` contains all user-defined parameters, including their default values if
they were not specified by the user. The sub-groups contain the config parameters of specific
:doc:`run_rs` or :doc:`run_al`. Those groups exist only if the respective parts of the code 
were turned on, otherwise the corresponding config parameters are ignored.

===================================================================================================
The simulation group
===================================================================================================

The simulation group contains information derived by SPARTA that is specific to the simulation the
code was run on, including cosmology, snapshots, and the characteristics of the simulation box.

+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| Field                    | Type     | Dimensions                 | Exists if                 | Explanation                                                                        |
+==========================+==========+============================+===========================+====================================================================================+ 
| ``n_snaps``              | int      | 1                          | Always                    | The number of snapshots processed                                                  |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``n_files_per_snap``     | int      | 1                          | Always                    | The number of files into which snapshot data are split                             |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``n_particles``          | int      | 1                          | Always                    | The total number of particles in the simulation                                    |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``box_size``             | float    | 1                          | Always                    | The box size in comoving Mpc/h                                                     |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``particle_mass``        | float    | 1                          | Always                    | The particle mass in Msun/h                                                        |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``Omega_m``              | float    | 1                          | Always                    | Omega\_m,0                                                                         |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``Omega_L``              | float    | 1                          | Always                    | Omega\_Lambda,0                                                                    |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``h``                    | float    | 1                          | Always                    | The Hubble constant in units of 100 km/s/Mpc                                       |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``snap_a``               | float    | ``n_snaps``                | Always                    | The scale factor of each snap                                                      |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``snap_z``               | float    | ``n_snaps``                | Always                    | The redshift of each snap                                                          |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``snap_t``               | float    | ``n_snaps``                | Always                    | The time of each snap in Gyr                                                       |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``snap_tdyn``            | float    | ``n_snaps``                | Always                    | The dynamical (crossing) time at the snapshot time in Gyr                          |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 

===================================================================================================
The halos group
===================================================================================================

The halos group contains the following datasets for all halos that were output (generally because
they reached a certain minimum number of particles at some point):

+--------------------------+----------+--------------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| Field                    | Type     | Dimensions                     | Exists if                 | Explanation                                                                        |
+==========================+==========+================================+===========================+====================================================================================+ 
| ``first_snap``           | int16    | ``n_halos``                    | Always                    | The snapshot index where the halo was created                                      |
+--------------------------+----------+--------------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``last_snap``            | int16    | ``n_halos``                    | Always                    | The snapshot index where the halo ended                                            |
+--------------------------+----------+--------------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``status``               | int8     | ``n_halos`` * ``n_snaps``      | Always                    | The halo's status at each snap (see below)                                         |
+--------------------------+----------+--------------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``status_final``         | int8     | ``n_halos``                    | Always                    | The halo's final status (see below)                                                |
+--------------------------+----------+--------------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``id``                   | int64    | ``n_halos`` * ``n_snaps``      | Always                    | The halo ID at each snap                                                           |
+--------------------------+----------+--------------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``parent_id``            | int64    | ``n_halos`` * ``n_snaps`` * 3  | ``OUTPUT_HALO_PARENT_ID`` | The halo's host ID at each snap, or -1 if it was a host itself                     |
+--------------------------+----------+--------------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``R200m``                | float    | ``n_halos`` * ``n_snaps``      | Always                    | The halo radius R200m (in physical kpc/h) at each snap                             |
+--------------------------+----------+--------------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``position``             | float    | ``n_halos`` * ``n_snaps`` * 3  | ``OUTPUT_HALO_X``         | The position in the box (in comoving Mpc/h)                                        |
+--------------------------+----------+--------------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``velocity``             | float    | ``n_halos`` * ``n_snaps`` * 3  | ``OUTPUT_HALO_V``         | The halo's perculiar velocity in physical km/s                                     |
+--------------------------+----------+--------------------------------+---------------------------+------------------------------------------------------------------------------------+ 

Please see the :doc:`intro_halos` page for the meaning of the status and status_final numerical
codes.

===================================================================================================
The tracer, result, and analysis groups
===================================================================================================

Each tracer group contains one subgroup per result type. This subgroup will be present if the 
result type was activated in the compile-time settings, even if it was turned off for tracers of 
this type and thus contains zero results. Each result subgroup contains at least the following 
fields, plus other fields listed below:

+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| Field                    | Type     | Dimensions                 | Exists if                 | Explanation                                                                        |
+==========================+==========+============================+===========================+====================================================================================+ 
| ``tracer_id``            | int64    | ``n_rs_*``                 | In result groups          | The unique ID of the tracer from which this result was derived                     |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``halo_id``              | int64    | ``n_al_*``                 | In analysis groups        | The unique ID of the halo which this analysis refers to                            |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``halo_first``           | int64    | ``n_halos``                | Always                    | The index of the first result or analysis of this type for this halo               |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 
| ``halo_n``               | int32    | ``n_halos``                | Always                    | The number of results or analyses of this type for this halo                       |
+--------------------------+----------+----------------------------+---------------------------+------------------------------------------------------------------------------------+ 

The ``halo_first`` and ``halo_n`` fields, combined with the IDs, represent an indexing system that
allows us to connect halos, results, and analyses to each other. In practice, this process can get
complicated. For this reason, it is recommended to use the :doc:`analysis_python` to load data from
SPARTA files. This module can perform the most common types of matching automatically.

See the documentation of the individual :doc:`run_rs` and :doc:`run_al` for details about the 
datasets that they write to the output file.
