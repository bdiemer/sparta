from setuptools import setup

setup(name = 'sparta',
	version = '1.0.0',
	description = 'Analysis package for the SPARTA code',
	url = '',
	author = 'Benedikt Diemer',
	author_email = 'diemer@umd.edu',
	license = 'MIT',
	requires = ['numpy', 'hdf5'],
	packages = ['sparta'],
	zip_safe = False)
