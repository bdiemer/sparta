###################################################################################################
#
# moria.py                  (c) Benedikt Diemer
#     				    	    diemer@umd.edu
#
###################################################################################################

"""
This file contains routines for the analysis of MORIA catalogs and merger trees.

---------------------------------------------------------------------------------------------------
Code examples
---------------------------------------------------------------------------------------------------

The main purpose of this module is to make it easier to load data from MORIA catalog or tree files,
although the data format is straightforward and can also be directly loaded from the HDF5 files
(see :doc:`analysis_moria_output` for details). Unlike in the SPARTA module where the entire file 
contents are loaded into one dictionary, there are two functions to load data or the configuration 
from a MORIA file.

.. rubric:: Basic file loading

The idea of the :func:`load` function is that the user can pass a series of fields and get back a
structured array. For example, we can load a particular definition of the halo radius and the halo
IDs::

	from sparta import moria
	
	fn_catalog = '/Users/you/some/dir/moria_catalog.hdf5'
	data = moria.load(fn_catalog, field_list = ['id', 'R200c_bnd_cat'], log_level = 1)
	
	>>> Loading 2 fields from MORIA catalog with 8899 halos.

The data array now has a shape of the number of halos and each field can be called using its string
identifier::

	print(data.shape)
	>>> (8899,)
	
	print(data['id'])
	>>> [1477557 1477562 1477564 ... 1470345 1482355 1474986]
	
	print(data['R200c_bnd_cat'])
	>>> [1029.2485     46.760006   75.87765  ...   46.040173   38.205635 46.040173]

We can also apply the function to tree files to get all snapshots. If we want to load a whole group
of fields, we can use regular expressions::

	fn_tree = '/Users/you/some/dir/moria_tree.hdf5'
	d = moria.load(fn_tree, field_list = ['vmax', 'x', 'R500c*', 'Acc_Rate_1*Tdyn'], log_level = 2)

	>>> Attempting to load the following fields from MORIA file:
	>>>     vmax                                regex: False
	>>>     x                                   regex: False
	>>>     R500c*                              regex: True
	>>>     Acc_Rate_1*Tdyn                     regex: False
	>>> Loading 11 fields from MORIA tree with 7593 halos and 96 snapshots.
	>>>     Acc_Rate_1*Tdyn                     shape: (96, 7593)
	>>>     R500c_all_spa                       shape: (96, 7593)
	>>>     R500c_bnd_cat                       shape: (96, 7593)
	>>>     R500c_tcr_spa                       shape: (96, 7593)
	>>>     parent_id_R500c_all_spa             shape: (96, 7593)
	>>>     parent_id_R500c_bnd_cat             shape: (96, 7593)
	>>>     parent_id_R500c_tcr_spa             shape: (96, 7593)
	>>>     status_moria_hps_R500c_all_spa      shape: (96, 7593)
	>>>     status_moria_hps_R500c_tcr_spa      shape: (96, 7593)
	>>>     vmax                                shape: (96, 7593)
	>>>     x                                   shape: (96, 7593, 3)

Note that ``Acc_Rate_1*Tdyn`` exists in the file and was thus not interpreted as a regular 
expression. In contrast, ``R500c*`` does not exist and was thus interpreted as a wildcard, 
meaning all fields related to R500c are loaded. Note that the structure of the returned array 
matches that in the MORIA file::

	print(d.shape)
	>>> (96, 7593)

The dimensions of individual fields (e.g., ``x``) are handled within the structured array, making
it easy to make cuts, for example selecting all halos at the final snapshot::

	halo_data = d[-1, :]
	
or selecting all epochs of the first halo::

	halo_history = d[0, :]

.. rubric:: Tree loading and halo selection

The arrays we have loaded from MORIA tree files above contain zeros where halos were not valid. 
These halos can easily be excluded if we are loading only one snapshot::

	data = moria.load(fn_tree, field_list = ['id'], a = 0.5, log_level = 1)
	>>> Loading 1 fields from MORIA tree at snapshot 64, a = 0.503, loading 13161/15004 halos.
	
	print(data.shape)
	>>> (13161,)

Here, we have loaded the IDs of all halos that were alive at a = 0.5. The load function has 
automatically selected the correct snapshot and taken out halos that were not alive. However, 
this is not the same output we would get in the corresponding catalog file, because the tree
includes all halos that were in a catalog file at any redshift. If we want only those halos that
made the mass cut at the given redshift, we use the ``apply_cut`` parameter::

	data = moria.load(fn_tree, field_list = ['id'], a = 0.5, apply_cut = True)
	>>> Loading 1 fields from MORIA tree at snapshot 64, a = 0.503, loading 8267/15004 halos.

	print(data.shape)	
	>>> (8267,)

Now, the function has loaded fewer halos, as some halos were alive but not above the cut imposed
on the MORIA run.

.. rubric:: Manual loading

As we have seen, it is easy to use the :func:`load` function to load a specific snapshot from 
trees, but sometimes it may be desirable to perform this operation manually. This is relatively 
easy because the tree files contains a mask field. If reading the file directly, we load the mask 
and fields separately. For example, if we wanted to load R200c for all halos that were alive in the 
final snapshot, the code cool look like this::

	f = h5py.File(fn_tree, 'r')
	mask = np.array(f['mask_alive'][-1, :]).astype(bool)
	R200c = np.array(f['R200c_tcr_spa'][-1, :])[mask]

Note the conversion to a boolean type; in the MORIA files, the masks are stored as 8-bit integers 
for compatibility with the rest of SPARTA. If we are using the :func:`load` function, this 
conversion is automatically taken care of::
	
	data = moria.load(fn_tree, field_list = ['mask_alive', 'R200c_tcr_spa'])
	mask = data['mask_alive'][-1, :]
	R200c = data['R200c_tcr_spa'][-1, mask]

The R200c array now contains the radii of all halos that were alive in the final snapshot, but we
do not know if those halos passed the mass (or other) cut that was imposed on the MORIA catalogs.
To allow the user to impose the same cut, the ``mask_cut`` field is one only if the halo was 
output to the catalog at that snapshot, i.e., if it passed the cut. For example, the following code
finds the halo radius for all halos that survived the cut at z = 1::

	a = 0.5
	data = moria.load(fn_tree, field_list = ['mask_cut', 'R200c_tcr_spa'])
	cfg = moria.loadConfig(fn_tree)
	snap_a = cfg['simulation']['snap_a']
	snap_idx = np.argmin(np.abs(snap_a - a))
	mask = data['mask_cut'][snap_idx, :]
	R200c = data['R200c_tcr_spa'][snap_idx, mask]

Here, the :func:`loadConfig` function loads the rest of the contents of a MORIA file, that is, all 
configuration, simulation, and (if it is a catalog) snapshot properties.

.. rubric:: Creating labels

The :func:`getLatexLabel` function can be used to produce latex-formatted labels for many common
MORIA quantities (though not all, since the user can add arbitrary fields). For fields for which 
there is no obvious mathematical symbol, a latex-formatted string is returned. For example::

	moria.getLatexLabel('acc_rate_200m_dyn')
	>>> $\Gamma_{\\rm dyn}$

	moria.getLatexLabel('parent_id_R200c_all_spa')
	>>> $\mathrm{parent\_id\_R200c\_all\_spa}$

---------------------------------------------------------------------------------------------------
Function documentation
---------------------------------------------------------------------------------------------------

.. autosummary:: 
	load
	loadConfig
	getLatexLabel
"""

import numpy as np
import h5py
import re
import sys
import collections.abc

from sparta import halodef

###################################################################################################
# STATUS CONSTANTS
###################################################################################################

# Similar to SPARTA itself, MORIA also returns a status for certain fields. Regardless of the 
# exact halo analysis, the following codes are defined:

# STATUS_HALO_DOES_NOT_EXIST        This halo was not present in the SPARTA file at all
#
# STATUS_ANL_DOES_NOT_EXIST         This analysis was not present in the SPARTA file or was not
#                                   considered for another reason. Thus, no value from SPARTA was 
#                                   found for the output in question.
#
# STATUS_UNDEFINED                  Internal status, should never occur in output file
# 
# STATUS_SPARTA_SUCCESS             The analysis was present in SPARTA, and its output was used
#                                   for the value in question. This should ideally be the status
#                                   for the vast majority of halos and outputs.

MORIA_STATUS_HALO_DOES_NOT_EXIST = -2
MORIA_STATUS_ANL_DOES_NOT_EXIST = -1
MORIA_STATUS_UNDEFINED = 0
MORIA_STATUS_SPARTA_SUCCESS = 1

# For the splashback radius analysis, MORIA also uses the following codes to indicate how a value
# was generated if a SPARTA value did not exist. This happens when any of the ANL_RSP error codes
# list above occurred, but MORIA had to guess some value in order to proceed:
# 
# RSP_STATUS_ESTIMATED_MODEL        The Gamma-Rsp/Msp model of Diemer et al. 2017 was used to 
#                                   estimate a value based on the mass and mass acc. rate of the 
#                                   halo.
#                                    
# RSP_STATUS_ESTIMATED_PAST         The most recent past value for Rsp/R200m or Msp/M200m was used.
#
# RSP_STATUS_ESTIMATED_FUTURE       The next valid future value for Rsp/R200m or Msp/M200m was used.
# 
# RSP_STATUS_ESTIMATED_INTERPOLATED Both past and future values were used to interpolate Rsp/R200m 
#                                   or Msp/M200m in time.

MORIA_RSP_STATUS_ESTIMATED_MODEL = 2
MORIA_RSP_STATUS_ESTIMATED_PAST = 3
MORIA_RSP_STATUS_ESTIMATED_FUTURE = 4
MORIA_RSP_STATUS_ESTIMATED_INTERPOLATED = 5

# For the halo properties analysis, MORIA also uses the following codes to indicate how a value
# was generated if a SPARTA value did not exist. 
# 
# HPS_STATUS_SO_TOO_SMALL           This definition could not be computed because the SO threshold 
#                                   was not reached at the center. This commonly happens to poorly
#                                   resolved halos with few particles, e.g., at the beginning of a
#                                   simulation.
#
# HPS_STATUS_SO_TOO_LARGE           This definition could not be computed because the SO threshold
#                                   was exceeded even at the largest radii considered. This often
#                                   happens with subhalos due to the contribution from their host, 
#                                   but should be very rare for host halos.
#
# HPS_STATUS_ORB_ZERO               This definition could not be computed because there were no 
#                                   orbiting particles.

MORIA_HPS_STATUS_SO_TOO_SMALL = 2
MORIA_HPS_STATUS_SO_TOO_LARGE = 3
MORIA_HPS_STATUS_ORB_ZERO = 4

# MORIA outputs mass accretion rates by comparing masses across time. There can be complications
# depending on the history of the halo:

# ACCRATE_STATUS_UNDEFINED              Should not occur for valid halos
#
# ACCRATE_STATUS_SUCCESS                Accretion rate was successfully computed
#
# ACCRATE_STATUS_REDUCED_INTERVAL       Acc. rate was computed, but the interval had to be cut down
#                                       to less than a dynamical time
#
# ACCRATE_STATUS_MODEL                  No real estimate was possible, the value given is predicted
#                                       by the fitting function of Diemer et al. 2017 (which has
#                                       significant scatter). This value is not accurate and should
#                                       not be used if possible.
#
# ACCRATE_STATUS_SUB_SUCCESS            This is a subhalo, acc. rate refers to the time of infall
#                                       where it was successfully computed
#
# ACCRATE_STATUS_SUB_REDUCED_INTERVAL   This is a subhalo, acc. rate refers to the time of infall
#                                       where it was computed with a reduced interval
#
# ACCRATE_STATUS_SUB_MODEL              This is a subhalo, acc. rate refers to the time of infall
#                                       where it was estimated using the fitting function 
#
# ACCRATE_STATUS_SUB_MODEL_NOW          This is a subhalo, but its infall time is not known and
#                                       the acc. rate was computed at the current time using the
#                                       fitting function

MORIA_ACCRATE_STATUS_UNDEFINED = 0
MORIA_ACCRATE_STATUS_SUCCESS = 1
MORIA_ACCRATE_STATUS_REDUCED_INTERVAL = 2
MORIA_ACCRATE_STATUS_MODEL = 3
MORIA_ACCRATE_STATUS_SUB_SUCCESS = 4
MORIA_ACCRATE_STATUS_SUB_REDUCED_INTERVAL = 5
MORIA_ACCRATE_STATUS_SUB_MODEL = 6
MORIA_ACCRATE_STATUS_SUB_MODEL_NOW = 7

###################################################################################################
# LABELS
###################################################################################################

# List of latex-formatted labels for certain quantities that can appear in MORIA catalogs
moria_labels = {}

moria_labels['id']                               = r'$\mathrm{ID}$'
moria_labels['num_prog']                         = r'$N_{\rm prog}$'

moria_labels['Mpeak_Scale']                      = r'$a_{\rm peak}$'
moria_labels['Acc_Scale']                        = r'$a_{\rm acc}$'
moria_labels['First_Acc_Scale']                  = r'$a_{\rm first-acc}$'
moria_labels['scale_of_last_MM']                 = r'$a_{\rm last-mm}$'
moria_labels['Halfmass_Scale']                   = r'$a_{1/2}$'
moria_labels['Time_to_future_merger']            = r'$\Delta t_{\rm merge}$'

moria_labels['nu200m_internal']                  = r'$\nu_{\rm 200m}$'

moria_labels['Vacc']                             = r'$V_{\rm acc}$'
moria_labels['Macc']                             = r'$M_{\rm acc}$'
moria_labels['Vpeak']                            = r'$V_{\rm peak}$'
moria_labels['Mpeak']                            = r'$M_{\rm peak}$'
moria_labels['Vmax@Mpeak']                       = r'$V_{\rm max,Mpeak}$'
moria_labels['First_Acc_Mvir']                   = r'$M_{\rm vir,first-acc}$'
moria_labels['First_Acc_Vmax']                   = r'$V_{\rm max,first-acc}$'
moria_labels['Halfmass_Radius']                  = r'$r_{\rm half}$'
moria_labels['rs']                               = r'$r_{\rm s}$'
moria_labels['Rs_Klypin']                        = r'$r_{\rm s,vmax}$'

moria_labels['acc_rate_200m_dyn']                = r'$\Gamma_{\rm dyn}$'
moria_labels['Acc_Rate_1*Tdyn']                  = r'$\Gamma_{\rm RS,Mvir}^{\rm tdyn/2}$'
moria_labels['Acc_Rate_100Myr']                  = r'$\Gamma_{\rm RS,Mvir}^{\rm 100Myr}$'
moria_labels['Acc_Rate_2*Tdyn']                  = r'$\Gamma_{\rm RS,Mvir}^{\rm tdyn}$'
moria_labels['Acc_Rate_Inst']                    = r'$\Gamma_{\rm RS,Mvir}^{\rm inst}$'
moria_labels['Acc_Rate_Mpeak']                   = r'$\Gamma_{\rm RS,Mpeak}$'
moria_labels['Acc_Log_Vmax_1*Tdyn']              = r'$\Gamma_{\rm RS,vmax}^{\rm tdyn/2}$'
moria_labels['Acc_Log_Vmax_Inst']                = r'$\Gamma_{\rm RS,vmax}^{\rm inst}$'
moria_labels['Log_(VmaxVmax_max(Tdyn;Tmpeak))']  = r'$\Gamma_{\rm RS,vmax}^{\rm peak}$'

moria_labels['A[x]']                             = r'$A_{\rm x}$'
moria_labels['A[x](500c)']                       = r'$A_{\rm x,500c}$'
moria_labels['A[y]']                             = r'$A_{\rm y}$'
moria_labels['A[y](500c)']                       = r'$A_{\rm y,500c}$'
moria_labels['A[z]']                             = r'$A_{\rm z}$'
moria_labels['A[z](500c)']                       = r'$A_{\rm z,500c}$'
moria_labels['Jx']                               = r'$J_{\rm x}$'
moria_labels['Jy']                               = r'$J_{\rm y}$'
moria_labels['Jz']                               = r'$J_{\rm x}$'
moria_labels['Spin']                             = r'$\lambda_{\rm peebles}$'
moria_labels['Spin_Bullock']                     = r'$\lambda_{\rm bullock}$'
moria_labels['Tidal_Force']                      = r'$F_{\rm tidal}$'
moria_labels['Tidal_Force_Tdyn']                 = r'$F_{\rm tidal,tdyn}$'
moria_labels['T|U|']                             = r'$T/U$'
moria_labels['vrms']                             = r'$V_{\rm rms}$'
moria_labels['Voff']                             = r'$\Delta x_{\rm off}$'
moria_labels['Xoff']                             = r'$\Delta v_{\rm off}$'
moria_labels['b_to_a']                           = r'$b/a$'
moria_labels['b_to_a(500c)']                     = r'$(b/a)_{\rm 500c}$'
moria_labels['c_to_a']                           = r'$c/a$'
moria_labels['c_to_a(500c)']                     = r'$(c/a)_{\rm 500c}$'
moria_labels['v']                                = r'$v$'
moria_labels['x']                                = r'$x$'

###################################################################################################

def load(filename, field_list, 
		# Options that select a snapshot from tree files
		snap_idx = None, a = None, z = None, 
		a_tolerance = 0.05, missing_snap_action = 'abort', return_snap_idx = False,
		# Other options
		apply_cut = False, apply_cut_alive = True, 
		use_h5py_masking = True, fail_on_missing = True, log_level = 0):
	"""
	Load specific fields or kinds of fields from a MORIA catalog or merger tree file.
	
	The MORIA file format is simple: it contains one dataset per field, and the dimensions are 
	either the number of halos (for catalog files) or the number of snapshots by the number of 
	halos (for tree files). It is perfectly fine to directly load these fields using the h5py
	library. This function, however, makes it easier to generate a coherent data structure, namely
	a structured array where the fields carry the same string identifiers as in the file. The 
	dimensions of the array are automatically adjusted to the overall dimensions (halos, snapshots)
	and to the dimensions of the individual fields (e.g., 3 for a position).
	
	Moreover, this function allows the user to not only request specific fields but also to look 
	for fields using regular expressions. If a requested fields contains any special characters, 
	we interpret it as a regular expression. Note that passing invalid expressions will likely lead
	to errors. We except fields that exactly match one of the fields from the file, as some catalog
	fields may contain special characters. For example, if the user requests the field 
	"my*field" and this field exists in the file, we only load this field but not "my_other_field".
	By default, the function throws an exception if a regular field (i.e., not a special 
	expression) could not be found in the file, but this behavior can be turned off.
	
	Finally, if a tree file is being loaded, the user can select a particular snapshot, redshift,
	or scale factor to load. A mask is automatically applied so that only halos that were alive at
	that snapshot are loaded, or even only halos that make the catalog mass cut. If the latter 
	behavior is invoked, the results are exactly the same as loading the same fields from the 
	catalog file at the redshift in question (except for the ordering of the halos).

	Parameters
	-----------------------------------------------------------------------------------------------
	filename: str
		The path to the MORIA catalog or tree file.
	field_list: array_like
		A list of strings that denote either a field in the file or a regular expression.
	snap_idx: int
		If loading from a tree file: the index of the snapshot to be loaded. If ``None``, all 
		snapshots are loaded. Can be replaced by ``a`` or ``z``, but only one of the three can
		be set.
	a: float
		If loading from a tree file: scale factor to be loaded. See ``snap_idx`` above.
	z: float
		If loading from a tree file: redshift to be loaded. See ``snap_idx`` above.
	a_tolerance: float
		If loading from a tree file: tolerance for difference in requested scale factor (whether
		requested as ``a`` or ``z``) and the closest one found in the tree file. If
		the difference is greater than this tolerance, the ``missing_snap_action`` parameter 
		decides what action is taken.
	missing_snap_action: str
		 If loading from a tree file: action to be taken when the requested redshift or snapshot
		 cannot be found. Can be ``abort`` in which case ``None`` is returned, ``warning`` in which
		 case the closest snapshot is returned but a warning is printed (not recommended!), or 
		 ``error`` in which case the function aborts with an error message. If the snapshot was
		 requested via a ``snap_idx``, there is no way to return a closest match; thus, the 
		 ``warning`` option translates to ``error`` in that case.
	return_snap_idx: bool
		If loading from a tree file: return the snapshot index that was selected for a given 
		redshift or scale factor. This is a non-trivial output because the redshift given by the
		user may not match the snapshot redshifts exactly.
	apply_cut: bool
		If loading a snapshot from a tree file (see ``snap_idx`` above), this parameter determines
		whether the function returns all halos that were alive at the snapshot in question (if
		``False``) or only those halos that were output in the corresponding catalog, i.e., those
		that made the cut(s) specified in the MORIA run (if ``True``).
	apply_cut_alive: bool
		If loading a snapshot from a tree file (see ``snap_idx`` above), this parameter determines
		whether the function returns all halos that were alive at the snapshot in question, or also
		halos that were not alive (which will appear as zeros in the array). The latter behavior is
		rarely needed, but can be useful when matching arrays from different load calls. If 
		``True``, the returned array contains exactly the same number of halos as the MORIA tree 
		file. If ``apply_cut`` is ``True``, this parameter has no effect because all halos that are
		in the catalog are automatically also alive.
	use_h5py_masking: bool
		This parameter does not influence the output, but may influence the speed of the function.
		When loading a tree snapshot, we need to mask out other snapshots. This masking can be 
		performed by the h5py library itself or on a numpy array. The latter may be slightly 
		faster, but means that the entire field must be loaded from the file. The masking of 
		active halos is always performed in numpy as it is much faster this way.
	fail_on_missing: bool
		If ``True``, we stop if a requested field cannot be found in the file.
	log_level: int
		If zero, no output is generated. One is the default level of output, greater numbers lead
		to very detailed output.

	Returns
	-----------------------------------------------------------------------------------------------
	d: array_like
		A structured array containing the requested fields.
	"""
	
	# If any of these characters appear in a requested field, we assume the string represents a 
	# regular expression.
	special_chars = ['.', '^', '$', '*', '+', '?', '{', '}', '[', ']', '(', ')', ':', '#', '<', '>', '|']
	
	# Certain fields are int8 types in the HDF5 file but really should be boolean in the output.
	bool_fields = ['mask_alive', 'mask_cut']

	# Open file
	file = h5py.File(filename, 'r')
	is_tree = ('mask_alive' in file.keys())

	if not is_tree and return_snap_idx:
		raise Exception('Cannot return snapshot index when not loading from tree file, but return_snap_idx is True.')
	
	if return_snap_idx:
		return_tuple = (None, None)
	else:
		return_tuple = None
	
	# Check whether the user has requested a particular snapshot
	if is_tree and ((snap_idx is not None) or (a is not None) or (z is not None)):
		n_constraints = 0
		if snap_idx is not None:
			n_constraints += 1
		if a is not None:
			n_constraints += 1
		if z is not None:
			n_constraints += 1
		if n_constraints > 1:
			raise Exception('Found more than one constraint on redshift (snapshot, a, z); can at most have one.')
	
		snap_a = np.array(file['simulation'].attrs['snap_a'])
		n_snaps = len(snap_a)
		if snap_idx is not None:
			if isinstance(snap_idx, collections.abc.Iterable):
				raise Exception('Received list for snap_idx parameter, but must be None or number.')
			if (snap_idx >= n_snaps) or (snap_idx < 0):
				if missing_snap_action == 'abort':
					return return_tuple
				elif missing_snap_action in ['warning', 'error']: 
					raise Exception('Found snap_idx %d but only %d snapshots in file.' % (snap_idx, n_snaps))
				else:
					raise Exception('Unknown missing_snap_action parameter, %s.' % (missing_snap_action))
		else:
			if a is None:
				a_target = 1.0 / (1.0 + z)
			else:
				a_target = a
			snap_idx = np.argmin(np.abs(snap_a - a_target))
			if np.abs(snap_a[snap_idx] - a_target) > a_tolerance:
				if missing_snap_action == 'abort':
					return return_tuple
				elif missing_snap_action == 'warning':
					print('WARNING: Closest snapshot to target a = %.3f, was %.3f.' \
						% (a_target, snap_a[snap_idx]), file = sys.stderr)
				elif missing_snap_action == 'error': 
					raise Exception('In MORIA tree file, closest snapshot to target a = %.3f was %.3f (difference greater than tolerance %.3f).' \
								% (a_target, snap_a[snap_idx], a_tolerance))
				else:
					raise Exception('Unknown missing_snap_action parameter, %s.' % (missing_snap_action))
		a_load = snap_a[snap_idx]
		load_tree_snap = True
	else:
		load_tree_snap = False
		if return_snap_idx:
			raise Exception('Cannot return snapshot index when no redshift is selected, but return_snap_idx is True.')

	# Compile list of available fields
	fields = list(file.keys())
	fields.remove('config')
	fields.remove('simulation')
	if 'snapshot' in fields:
		fields.remove('snapshot')

	# Prepare the user's field list. If a field is just a string, it needs to match exactly. If it
	# contains special characters, we interpret it as a regular expression. There are, however, a 
	# few fields that contain special characters in halo catalogs; if the requested field matches
	# one exactly, we assume that is what the user meant.
	n_req = len(field_list)
	is_regex = np.zeros((n_req), bool)
	for i in range(n_req):
		if field_list[i] in fields:
			continue
		for s in special_chars:
			if s in field_list[i]:
				is_regex[i] = True
				break

	if log_level > 1:
		print('Attempting to load the following fields from MORIA file:')
		for i in range(n_req):
			print('    %-35s regex: %s' % (field_list[i], str(is_regex[i])))

	# Match with the fields or expressions given by the user in an additive and inclusive fashion,
	# meaning we'd rather load too many than too few fields
	fields_load = []
	for i in range(len(fields)):
		load_field = False
		j = 0
		for j in range(n_req):
			if is_regex[j]:
				load_field = re.search(field_list[j], fields[i])
			else:
				load_field = (field_list[j] == fields[i])
			if load_field:
				break
		if load_field:
			fields_load.append(fields[i])

	# If desired, check for errors
	if fail_on_missing:
		for i in range(n_req):
			if (not is_regex[i]) and (not field_list[i] in fields_load):
				raise Exception('Could not find field %s in MORIA file %s.' % (field_list[i], filename))

	# First, we get the overall dimensions of the data in the file. If it is a catalog, that is just 
	# the number of halos. If it is a tree, that is the number of snapshots by the number of halos.
	shape_default = file['status_sparta'].shape
	ndim_default = len(shape_default)
	if not ndim_default in [1, 2]:
		raise Exception('Found unexpected dimensionality %d of datasets in MORIA file.' % ndim_default)
	
	# If we are loading a snapshot from a tree, load the mask, count the loaded halos
	if load_tree_snap:
		if apply_cut:
			mask_tree = np.array(file['mask_cut'][snap_idx, :]).astype(bool)
		elif apply_cut_alive:
			mask_tree = np.array(file['mask_alive'][snap_idx, :]).astype(bool)
		else:
			mask_tree = np.ones((shape_default[1]), bool)
		n_halos_tree = np.count_nonzero(mask_tree)

	# Output one line about what we are doing
	if log_level > 0:
		if is_tree:
			if load_tree_snap:
				print('Loading %d fields from MORIA tree at snapshot %d, a = %.3f, loading %d/%d halos.' \
					% (len(fields_load), snap_idx, a_load, n_halos_tree, shape_default[1]))
			else:
				print('Loading %d fields from MORIA tree with %d halos and %d snapshots.' \
					% (len(fields_load), shape_default[1], shape_default[0]))
		else:
			print('Loading %d fields from MORIA catalog with %d halos.' % (len(fields_load), shape_default[0]))
	
	# Now get the dimensions of each field and put them into a structured array. The explicit 
	# string conversions ensure python 2.x compatibility.
	dtypes = []
	for f in fields_load:
		dtype = file[f].dtype
		if f in bool_fields:
			dtype = bool
		shape = file[f].shape
		if len(shape) > ndim_default:
			shape_tuple = tuple(shape[ndim_default:])
			dtypes.append((str(f), dtype, shape_tuple))
		else:
			dtypes.append((str(f), dtype))
		if log_level > 1:
			print('    %-35s shape: %s' % (f, str(shape)))
	
	# Create the structured array. If we are not loading a tree snap, the shape is just the same 
	# shape as in the file. If we are, the arrays are reduced to one dimension of n_halos.
	if load_tree_snap:
		shape_array = (n_halos_tree, )
	else:
		shape_array = shape_default
	d = np.zeros(shape_array, dtype = dtypes)
		
	# Load the fields from the HDF5 file. If we are loading a tree snap, we apply both the
	# snapshot index and the mask.
	for f in fields_load:
		
		if use_h5py_masking:
			# If we are using the masking function of the h5py library, the mask goes inside the
			# array conversion, meaning that the library makes the cut. Even though that seems like
			# it should be more effective, it is often slower.
			if load_tree_snap:
				if f in bool_fields:
					d[f] = np.array(file[f][snap_idx])[mask_tree].astype(bool)
				else:
					d[f] = np.array(file[f][snap_idx])[mask_tree]
			else:
				if f in bool_fields:
					d[f] = np.array(file[f]).astype(bool)
				else:
					d[f] = np.array(file[f])		

		else:
			# Simply load the entire field into a numpy array and then perform masking operations
			# and conversions on this array.
			tmp_ar = np.array(file[f])
			if load_tree_snap:
				tmp_ar = tmp_ar[snap_idx, mask_tree]
			if f in bool_fields:
				tmp_ar =  tmp_ar.astype(bool)
			d[f] = tmp_ar
	
	file.close()

	if return_snap_idx:
		return_tuple = (d, snap_idx)
	else:
		return_tuple = d
	
	return return_tuple

###################################################################################################

def loadConfig(filename):
	"""
	Load all configuration and simulation parameters from a MORIA file into a dictionary.
	
	The function also returns a listing of all field names in the file.

	Parameters
	-----------------------------------------------------------------------------------------------
	filename: str
		The path to the MORIA catalog or tree file.

	Returns
	-----------------------------------------------------------------------------------------------
	dic: dictionary
		A dictionary containing the same group structure and parameters as the MORIA file.
	"""
	
	dic = {}
	groups = ['config', 'simulation', 'snapshot']
	
	file = h5py.File(filename, 'r')
	
	# Copy group attributes into dictionary
	for g in groups:
		if g in file.keys():
			dic_group = {}
			for f in file[g].attrs:
				dic_group[f] = file[g].attrs[f]
			dic[g] = dic_group
	
	# Create list of all fields
	fields = list(file.keys())
	for g in groups:
		if g in fields:
			fields.remove(g)
	dic['field_list'] = fields
		
	file.close()
	
	return dic

###################################################################################################

def getLatexLabel(q, add_dollars = True, **kwargs):
	"""
	Convert the name of a MORIA quantity to a latex-formatted label.
	
	Since MORIA files can contain user-defined quantities, there is no way to define a function
	that works for all possible quantities. This function encodes all halo radii, masses, peak
	heights and so on as well as the quantities listed in the MORIA files for the Erebos 
	simulations. If no matching label is found, the function attempts to convert the string to a
	textual latex label (which can contain underscores but not spaces).
	
	Parameters
	-----------------------------------------------------------------------------------------------
	q: str
		The name of a quantity as listed in a MORIA hdf5 file. 
	add_dollars: bool
		Whether or not to add $ signs at the beginning and end of the string.
	kwargs: kwargs
		Keyword args that are passed to the :func:`sparta.halodef.HaloDefinition.toLabel` function
		(in addition to ``add_dollars``).

	Returns
	-----------------------------------------------------------------------------------------------
	label: str
		A label in latex format.
	"""
		
	label = None
	if q in moria_labels:
		label = moria_labels[q]
	elif (q[0] in ['R', 'r', 'M', 'm', 'V', 'v']) or (q.startswith('nu')):
		try:
			hdef = halodef.strToDef(q)
			label = hdef.toLabel(add_dollars = add_dollars, **kwargs)
		except:
			pass
		
	if label is None:
		label = q
		label = label.replace('_', '\_')
		if add_dollars:
			label = r'$\mathrm{%s}$' % (label)
		else:
			label = r'\mathrm{%s}' % (label)
	
	return label

###################################################################################################
