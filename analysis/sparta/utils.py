###################################################################################################
#
# utils.py                  (c) Benedikt Diemer
#     				    	    diemer@umd.edu
#
###################################################################################################

"""
---------------------------------------------------------------------------------------------------
Utilities
---------------------------------------------------------------------------------------------------

This unit contains a number of useful constants and functions for analyzing SPARTA output:

.. autosummary:: 
	isArray
	findNearestIndexSorted
	findNearestIndices
	findRedshiftIndex
	findIndices
	findIndicesMultiD
"""

import numpy as np
import h5py

###################################################################################################
# ASTROPHYSICS CONSTANTS
###################################################################################################

RHO_CRIT_0_KPC3 = 2.774848e+02
"""The critical density of the universe at z = 0 in units of :math:`M_{\odot} h^2 / kpc^3`."""

###################################################################################################
# FUNCTIONS
###################################################################################################

def isArray(var):
	"""
	Tests whether a variable var is iterable or not.

	Parameters
	---------------------------
	var: array_like
		Variable to be tested.
	
	Returns
	-------
	is_array: boolean
		Whether var is a numpy array or not.
	"""
	
	try:
		dummy = iter(var)
	except TypeError:
		is_array = False
	else:
		is_array = True
		
	return is_array

###################################################################################################

def findNearestIndexSorted(d, value):
	"""
	Find the index of a value in an array, where the array is sorted ascendingly.

	Parameters
	---------------------------
	d: array_like
		Array of values.
	value: float
		The value to search.
	
	Returns
	-------
	idx: int
		The index where the array value is closest.
	"""
	
	idx = np.searchsorted(d, value, side = 'left')
	if idx == len(d) or np.fabs(value - d[idx - 1]) < np.fabs(value - d[idx]):
		return idx - 1
	else:
		return idx

###################################################################################################

def findNearestIndices(d, values):
	"""
	Find the indices of a number of values in an array, where the array is sorted ascendingly.

	Parameters
	---------------------------
	d: array_like
		Array of values.
	values: array_like
		An array of values to search.
	
	Returns
	-------
	idx: array_like
		The indices where the array values are closest to values. Has the same dimensions as 
		values.
	"""
	
	is_array = isArray(values)
	v = values
	if not is_array:
		v = np.array([v])

	ret = np.zeros((len(v)), int)
	for i in range(len(v)):		
		ret[i] = findNearestIndexSorted(d, v[i])
	
	if not is_array:
		ret = ret[0]
		
	return ret

###################################################################################################

def findRedshiftIndex(fn, a = None, z = None):
	"""
	Returns the closest index in a SPARTA file to a given redshift of scale factor.
	
	Note that this function does not check whether the given redshift was within the range of 
	redshifts in the SPARTA file.

	Parameters
	---------------------------
	a: float
		Scale factor (either a or z need to be passed).
	z: float
		Redshift (either a or z need to be passed).
	
	Returns
	-------
	idx: int
		Index of closest redshift bin in SPARTA file.
	a: float
		The scale factor at the chosen snapshot.
	z: float
		The redshift at the chosen snapshot.
	"""
		
	if a is None:
		if z is None:
			raise Exception('Either a or z must be given.')
		a = 1.0 / (z + 1.0)
	else:
		if z is not None:
			raise Exception('Only a or z can be given.')
	
	f = h5py.File(fn, 'r')
	a_snap = findNearestIndexSorted(f['simulation'].attrs['snap_a'], a)
	a_use = f['simulation'].attrs['snap_a'][a_snap]
	z_use = 1.0 / a_use - 1.0
	f.close()
	
	return a_snap, a_use, z_use

###################################################################################################

def findIndices(set_all, set_find, algorithm = 'auto'):
	"""
	Find the indices of one set of integers in another. 
	
	This function is useful for finding the indices of a set of IDs in another large array of IDs.
	For example, the load() function does not return halos in the requested order if halo_ids are
	passed to the function. This function makes it easy to obtain the original ordering.
	
	Two algorithms are implemented: the standard np.where() function which searches each ID 
	separately, and an algorithm based on np.searchsorted(). The latter is faster when set_find is
	large (greater than 100). By default, the best algorithm is chosen.

	Parameters
	---------------------------
	set_all: array_like
		A set of unique integer numbers (does not need to be sorted). 
	set_find: array_like
		The numbers to be found in set_all (does not need to be sorted, and must be smaller or 
		equal in size to set_all).
	algorithm: str
		If ``auto``, the function automatically chooses the best algorithm. If ``individual``, the
		np.where() function is used. If ``array``, the np.searchsorted() function is used.
		
	Returns
	-------
	idx_find: array_like
		Array of indices of the same size as set_find, pointing to set_all.
	"""

	n_find = len(set_find)
	mask = np.in1d(set_all, set_find)
	if np.count_nonzero(mask) != n_find:
		msg = "At least one ID could not be found in set, or an ID was duplicated (looking for %d IDs, found %d matches)." \
			% (n_find, np.count_nonzero(mask))
		raise Exception(msg)	

	if algorithm == 'auto':
		use_individual = (n_find < 100)
	elif algorithm == 'individual':
		use_individual = True
	elif algorithm == 'array':
		use_individual = False

	if use_individual:
		idx_find = np.zeros((n_find), int)
		for i in range(n_find):
			idx_find[i] = np.where(set_find[i] == set_all)[0][0]
	else:
		matches = set_all[mask]
		sorter = np.argsort(matches)
		matches = matches[sorter]
		mask_idx = (np.where(mask)[0])[sorter]
		matches = np.searchsorted(matches, set_find)
		idx_find = mask_idx[matches]

	return idx_find

###################################################################################################

def findIndicesMultiD(set_all, set_find, algorithm = 'auto'):
	"""
	Find the indices of one set of integers in another, higher-dimensional array.
	
	Like findIndices, but allows set_all to have higher dimensionality. Only the index in the first
	dimension is returned. This is useful for index arrays that have a time axis to them, where we
	do not care at what time an ID is found.

	Parameters
	---------------------------
	set_all: array_like
		A set of unique integer numbers (does not need to be sorted). 
	set_find: array_like
		The numbers to be found in set_all (does not need to be sorted, and must be smaller or 
		equal in size to set_all).
	algorithm: str
		If ``auto``, the function automatically chooses the best algorithm. If ``individual``, the
		np.where() function is used. If ``array``, the np.searchsorted() function is used.
		
	Returns
	-------
	idx_find: array_like
		Array of indices of the same size as set_find, pointing to set_all.
	"""
	
	shape = set_all.shape
	set_all = set_all.flatten()
	idx = findIndices(set_all, set_find, algorithm = algorithm)
	idx = np.unravel_index(idx, shape)[0]
	
	return idx
	
###################################################################################################
