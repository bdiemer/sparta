###################################################################################################
#
# halodef.py                (c) Benedikt Diemer
#     				    	    diemer@umd.edu
#
###################################################################################################

"""
This unit handles definitions of halo extent, boundary, and mass. Most importantly, it converts
a uniquely defined string-based format for definitions to a machine-readable format, and vice 
versa. Halo definitions are encapsulated by an object with the following main properties:

* ``quantity``: Indicates the actual quantity described, e.g., radius, mass, peak height, 
  of circular velocity. Perhaps counter-intuitively, this field can be unknown, in which case a
  general type of definition is described.
* ``type``: The main type of definition, e.g. friends-of-friends, overdensity, or splashback.
* ``subtype``: A refinement of type. For example, there are SO defintions wrt critical or mean
  density, and many types of splashback definitions.

The string format is then generated as ``<quantity><type><subtype>_<other-properties>``. The user
can generate a :class:`HaloDefinition` object from a string using :func:`strToDef` and convert
the object back using :func:`HaloDefinition.toStr`.

There are also a number of less important, optional descriptors for which a default is set so that
they do not need to be explicitly spelled out in the string format. This unit determines which 
definitions can be requested in SPARTA or MORIA configuration files, as long as they can be 
computed by the code.

The implementation exactly shadows the C implementation contained in SPARTA, so that the 
numerical values of all settings are the same in both languages:

+-------------------+--------------------------------------------------+------------+---------------------------------------------------+ 
| Property          | Value                                            | String     | Explanation                                       |
+===================+==================================================+============+===================================================+
| Quantity          | ``HDEF_Q_UNKNOWN``                               | Default    | Could describe any, general type                  |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_Q_RADIUS``                                | R          | Halo radius                                       |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_Q_MASS``                                  | M          | Halo mass                                         |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_Q_PEAKHEIGHT``                            | nu         | Peak height                                       |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_Q_VCIRC``                                 | V          | Cirular velocity                                  |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
| Type              | ``HDEF_TYPE_SO``                                 | m/c/vir    | Spherical overdensity                             |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_TYPE_SPLASHBACK``                         | sp         | Splashback                                        |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_TYPE_FOF``                                | fof        | Friends-of-friends                                |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_TYPE_VMAX``                               | max        | Maximum circular velocity                         |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_TYPE_ORBITING``                           | orb        | Orbiting particles                                |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
| Subtype           | ``HDEF_SUBTYPE_SO_MATTER``                       | <int>m     | Matter-based SO (e.g., 200m)                      |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SUBTYPE_SO_CRITICAL``                     | <int>c     | Critical-based SO (e.g., 200c)                    |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SUBTYPE_SO_VIRIAL``                       | vir        | Virial-based SO (varying overdensity)             |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SUBTYPE_SP_SLOPE_PROFILE``                | slp-prf    | Radius where slope is steepest                    |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SUBTYPE_SP_SLOPE_MEDIAN``                 | slp-med    | Steepest slope radius in median of angular bins   |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SUBTYPE_SP_SLOPE_SHELL``                  | slp-shl    | Non-isotropic steepest slope (e.g. SHELLFISH)     |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN``        | apr-mn     | Mean of first apocenter radii of halo particles   |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE``  | apr-p<int> | Percentile of first apocenter radii of ptl.       |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SUBTYPE_SP_APOCENTER_SHELL_MEAN``         | aps-mn     | Non-isotropic apocenter mean                      |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SUBTYPE_SP_APOCENTER_SHELL_PERCENTILE``   | aps-p<int> | Non-isotropic apocenter percentile                |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SUBTYPE_ORB_ALL``                         | all        | All orbiting particles that ever entered the halo |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SUBTYPE_ORB_PERCENTILE``                  | p<int>     | Percentile of orbiting ptl (within some radius)   |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
| Time              | ``HDEF_TIME_NOW``                                | Default    | Current state of halo                             |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_TIME_ACC``                                | acc        | State of subhalo at last accretion                |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_TIME_PEAK``                               | peak       | State of halo at peak of this property            |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
| Particle selection| ``HDEF_PTLSEL_DEFAULT``                          | Default    | Not specified                                     |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_PTLSEL_ALL``                              | all        | Definition includes all halo particles            |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_PTLSEL_BOUND``                            | bnd        | Definition includes only grav. bound particles    |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_PTLSEL_TRACER``                           | tcr        | Definition includes only traced ptl (for subhalos)|
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_PTLSEL_ORBITING``                         | orb        | Definition includes only orbiting particles       |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
| Source            | ``HDEF_SOURCE_ANY``                              | Default    | Unknown source, unspecified                       |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SOURCE_CATALOG``                          | cat        | Taken from halo catalog                           |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SOURCE_SPARTA``                           | spa        | Computed by SPARTA                                |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_SOURCE_SHELLFISH``                        | sfish      | Computed by SHELLFISH                             |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
| Value or error    | ``HDEF_ERR_NO``                                  | Default    | This is the indicated quantity                    |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_ERR_1SIGMA``                              | err        | This is the 1-sigma uncertainty on the quantity   |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
|                   | ``HDEF_ERR_2SIGMA``                              | err2s      | This is the 2-sigma uncertainty on the quantity   |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+
| Other flags       | ``HDEF_FLAG_INTERNAL``                           | internal   | Flag for internally used values                   |
+-------------------+--------------------------------------------------+------------+---------------------------------------------------+

Depending on the choices, a number of numerical values can become active, in particular:

* ``overdensity``: if type is ``HDEF_TYPE_SO`` and subtype is ``HDEF_SUBTYPE_SO_MATTER`` or
  ``HDEF_SUBTYPE_SO_CRITICAL``.
* ``linking_length``: if type is ``HDEF_TYPE_FOF``.
* ``percentile``: if type is ``HDEF_TYPE_SPLASHBACK`` and subtype is 
  ``HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE`` or 
  ``HDEF_SUBTYPE_SP_APOCENTER_SHELL_PERCENTILE``.

---------------------------------------------------------------------------------------------------
Examples
---------------------------------------------------------------------------------------------------

Valid examples of mass definitions would include:

* ``200m``, ``vir``
* ``R200m``, ``M200m``, ``nu200m``
* ``Vmax``
* ``Mfof-b0.2``, ``Rfof-b0.15``
* ``Rsp-slp-prf``, ``Msp-slp-prf``
* ``Rsp-apr-p75``

Not strictly correct, but also accepted when converting from a string are different capitalizations
of the quantity (but only the quantity), e.g.:

* ``r200m``, ``m200m``, ``mfof-b0.2``
* ``NU200m``, ``Nu200m``
* ``vmax``

While those strings will be converted into a halo definition without an error, converting the 
definition back to a string will result in the standard capitalization. Thus, it is recommended to
stick with ``R``, ``M`` and so on whenever possible.

All additional properties are separated by underscores and can be ordered at will. Defaults are
used if no value is specified. Valid definitions thus include:

* ``R200m``
* ``R200m_peak``, ``R200m_acc``
* ``R200m_all``, ``R200m_bnd``
* ``R200m_cat``, ``R200m_spa``
* ``R200m_err``, ``R200m_err2s``

The best way to test whether a definition is valid is to try converting it with :func:`strToDef`,
which will throw an error if the string is not a correct definition.

---------------------------------------------------------------------------------------------------
Module reference
---------------------------------------------------------------------------------------------------
"""

###################################################################################################
# DEFINITION OF HALODEF FORMAT
###################################################################################################

HDEF_Q_UNKNOWN = 0
HDEF_Q_RADIUS = 1
HDEF_Q_MASS = 2
HDEF_Q_PEAKHEIGHT = 3
HDEF_Q_VCIRC = 4
HDEF_Q_N = 5

HDEF_TYPE_INVALID = 0
HDEF_TYPE_SO = 1
HDEF_TYPE_SPLASHBACK = 2
HDEF_TYPE_FOF = 3
HDEF_TYPE_VMAX = 4
HDEF_TYPE_ORBITING = 5
HDEF_TYPE_N = 6

HDEF_SUBTYPE_SO_MATTER = 0
HDEF_SUBTYPE_SO_CRITICAL = 1
HDEF_SUBTYPE_SO_VIRIAL = 2
HDEF_SUBTYPE_SO_N = 3

HDEF_SUBTYPE_SP_SLOPE_PROFILE = 0
HDEF_SUBTYPE_SP_SLOPE_MEDIAN = 1
HDEF_SUBTYPE_SP_SLOPE_SHELL = 2
HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN = 3
HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE = 4
HDEF_SUBTYPE_SP_APOCENTER_SHELL_MEAN = 5
HDEF_SUBTYPE_SP_APOCENTER_SHELL_PERCENTILE = 6
HDEF_SUBTYPE_SP_N = 7

HDEF_SUBTYPE_FOF_N = 1
HDEF_SUBTYPE_VMAX_N = 1

HDEF_SUBTYPE_ORB_ALL = 0
HDEF_SUBTYPE_ORB_PERCENTILE = 1
HDEF_SUBTYPE_ORB_N = 2

HDEF_TIME_NOW = 0
HDEF_TIME_ACC = 1
HDEF_TIME_PEAK = 2
HDEF_TIME_N = 3

HDEF_PTLSEL_DEFAULT = 0
HDEF_PTLSEL_ALL = 1
HDEF_PTLSEL_BOUND = 2
HDEF_PTLSEL_TRACER = 3
HDEF_PTLSEL_ORBITING = 4
HDEF_PTLSEL_N = 5

HDEF_SOURCE_ANY = 0
HDEF_SOURCE_CATALOG = 1
HDEF_SOURCE_SPARTA = 2
HDEF_SOURCE_SHELLFISH = 3
HDEF_SOURCE_N = 4

HDEF_ERR_NO = 0
HDEF_ERR_1SIGMA = 1
HDEF_ERR_2SIGMA = 2
HDEF_ERR_N = 3

HDEF_FLAG_INTERNAL = 1

###################################################################################################

class HaloDefinition():
	"""
	Object that encapsulates a halo definition.
	"""

	def __init__(self):
		
		self.quantity = HDEF_Q_UNKNOWN
		self.type = HDEF_TYPE_INVALID
		self.subtype = HDEF_TYPE_INVALID
		self.time = HDEF_TIME_NOW
		self.ptl_select = HDEF_PTLSEL_DEFAULT
		self.source = HDEF_SOURCE_ANY
		self.is_error = HDEF_ERR_NO
		self.overdensity = 0.0
		self.percentile = 0.0
		self.linking_length = 0.0
		self.idx = -1
		
		self.flag_internal = False
		
		return

	# ---------------------------------------------------------------------------------------------
	
	def toStr(self):
		"""
		Convert halo definition to string.
		
		Returns
		-------------------------------------------------------------------------------------------
		defstr: str
			A string that uniquely describes the halo definition, and that can be converted back
			into the same definition.
		"""
		
		defstr = ''
		
		if self.quantity == HDEF_Q_UNKNOWN:
			defstr += ''
		elif self.quantity == HDEF_Q_RADIUS:
			defstr += 'R'
		elif self.quantity == HDEF_Q_MASS:
			defstr += 'M'
		elif self.quantity == HDEF_Q_PEAKHEIGHT:
			defstr += 'nu'
		elif self.quantity == HDEF_Q_VCIRC:
			defstr += 'V'
		else:
			raise Exception('Invalid halo quantity %d.' % self.quantity)

		if self.type == HDEF_TYPE_SO:
			
			if self.subtype == HDEF_SUBTYPE_SO_MATTER:
				defstr += '%.0fm' % self.overdensity
			elif self.subtype == HDEF_SUBTYPE_SO_CRITICAL:
				defstr += '%.0fc' % self.overdensity
			elif self.subtype == HDEF_SUBTYPE_SO_VIRIAL:
				defstr += 'vir'
			else:
				raise Exception('Invalid SO subtype %d.' % self.subtype)
		
		elif self.type == HDEF_TYPE_SPLASHBACK:
			
			if self.subtype == HDEF_SUBTYPE_SP_SLOPE_PROFILE:
				defstr += 'sp-slp-prf'
			elif self.subtype == HDEF_SUBTYPE_SP_SLOPE_MEDIAN:
				defstr += 'sp-slp-med'
			elif self.subtype == HDEF_SUBTYPE_SP_SLOPE_SHELL:
				defstr += 'sp-slp-shl'
			elif self.subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN:
				defstr += 'sp-apr-mn'
			elif self.subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE:
				defstr += 'sp-apr-p%.0f' % self.percentile
			elif self.subtype == HDEF_SUBTYPE_SP_APOCENTER_SHELL_MEAN:
				defstr += 'sp-aps-mn'
			elif self.subtype == HDEF_SUBTYPE_SP_APOCENTER_SHELL_PERCENTILE:
				defstr += 'sp-aps-p%.0f' % self.percentile
			else:
				raise Exception('Invalid splashback subtype %d.' % self.subtype)

		elif self.type == HDEF_TYPE_FOF:
			
			defstr += 'fof-b%.2f' % self.linking_length

		elif self.type == HDEF_TYPE_VMAX:
			
			defstr += 'max'

		elif self.type == HDEF_TYPE_ORBITING:

			if self.subtype == HDEF_SUBTYPE_ORB_ALL:
				defstr += 'orb-all'
			elif self.subtype == HDEF_SUBTYPE_ORB_PERCENTILE:
				defstr += 'orb-p%.0f' % self.percentile
			else:
				raise Exception('Invalid orbiting subtype %d.' % self.subtype)

		else:
			raise Exception('Invalid type %d.' % self.type)

		if self.time == HDEF_TIME_NOW:
			pass
		elif self.time == HDEF_TIME_ACC:
			defstr += '_acc'
		elif self.time == HDEF_TIME_PEAK:
			defstr += '_peak'
		else:
			raise Exception('Invalid halo definition time identifier %d.' % self.time)

		if self.ptl_select == HDEF_PTLSEL_DEFAULT:
			pass
		elif self.ptl_select == HDEF_PTLSEL_ALL:
			defstr += '_all'
		elif self.ptl_select == HDEF_PTLSEL_BOUND:
			defstr += '_bnd'
		elif self.ptl_select == HDEF_PTLSEL_TRACER:
			defstr += '_tcr'
		elif self.ptl_select == HDEF_PTLSEL_ORBITING:
			defstr += '_orb'
		else:
			raise Exception('Invalid halo bound/all identifier %d.' % self.ptl_select)

		if self.is_error == HDEF_ERR_NO:
			pass
		elif self.is_error == HDEF_ERR_1SIGMA:
			defstr += '_err'
		elif self.is_error == HDEF_ERR_2SIGMA:
			defstr += '_err2s'
		else:
			raise Exception('Invalid error state identifier %d.' % self.is_error)

		if self.source == HDEF_SOURCE_ANY:
			pass
		elif self.source == HDEF_SOURCE_CATALOG:
			defstr += '_cat'
		elif self.source == HDEF_SOURCE_SPARTA:
			defstr += '_spa'
		elif self.source == HDEF_SOURCE_SHELLFISH:
			defstr += '_sfish'
		else:
			raise Exception('Invalid source identifier %d.' % self.source)

		if self.flag_internal:
			defstr += '_internal'

		return defstr

	# ---------------------------------------------------------------------------------------------
	
	def toLabel(self, add_dollars = True, add_units = False,
			suppress_source = False, suppress_time = False, suppress_bound = False, suppress_flags = False):
		"""
		Convert halo definition to a latex label.

		Parameters
		-------------------------------------------------------------------------------------------
		add_dollars: bool
			Whether or not to add $ signs at the beginning and end of the string.
		add_units: bool
			Add units in brackets behind the label.
		suppress_source: bool
			Do not output source identifiers (e.g., ``_cat``).
		suppress_time: bool
			Do not output time identifiers (e.g., ``_peak`` or ``_acc``).
		suppress_bound: bool
			Do not output all-particle and bound-only identifiers (e.g., ``_all`` and ``_bnd``).
		suppress_flags: bool
			Do not output additional flag info (e.g., ``_internal``).
			
		Returns
		-------------------------------------------------------------------------------------------
		label: str
			A string that can be fed to latex, e.g. for figure labels.
		"""

		label = ''
		
		do_subscript = True
		if self.quantity == HDEF_Q_UNKNOWN:
			do_subscript = False
		elif self.quantity == HDEF_Q_RADIUS:
			label += r'R'
		elif self.quantity == HDEF_Q_MASS:
			label += r'M'
		elif self.quantity == HDEF_Q_PEAKHEIGHT:
			label += r'\nu'
		elif self.quantity == HDEF_Q_VCIRC:
			label += r'V'
		else:
			raise Exception('Invalid halo quantity %d.' % self.quantity)

		substr = ''
		if self.type == HDEF_TYPE_SO:
			
			if self.subtype == HDEF_SUBTYPE_SO_MATTER:
				substr += r'%.0fm' % self.overdensity
			elif self.subtype == HDEF_SUBTYPE_SO_CRITICAL:
				substr += r'%.0fc' % self.overdensity
			elif self.subtype == HDEF_SUBTYPE_SO_VIRIAL:
				substr += r'vir'
			else:
				raise Exception('Invalid SO subtype %d.' % self.subtype)
		
		elif self.type == HDEF_TYPE_SPLASHBACK:
			
			if self.subtype == HDEF_SUBTYPE_SP_SLOPE_PROFILE:
				substr += r'sp\mbox{-}slp\mbox{-}prf'
			elif self.subtype == HDEF_SUBTYPE_SP_SLOPE_MEDIAN:
				substr += r'sp\mbox{-}slp\mbox{-}med'
			elif self.subtype == HDEF_SUBTYPE_SP_SLOPE_SHELL:
				substr += r'sp\mbox{-}slp\mbox{-}shl'
			elif self.subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN:
				substr += r'sp,mn'
			elif self.subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE:
				substr += r'sp,%.0f\%%' % self.percentile
			elif self.subtype == HDEF_SUBTYPE_SP_APOCENTER_SHELL_MEAN:
				substr += r'sp\mbox{-}aps\mbox{-}mn'
			elif self.subtype == HDEF_SUBTYPE_SP_APOCENTER_SHELL_PERCENTILE:
				substr += r'sp\mbox{-}aps,%.0f\%%' % self.percentile
			else:
				raise Exception('Invalid splashback subtype %d.' % self.subtype)

		elif self.type == HDEF_TYPE_FOF:
			
			substr += r'fof-b%.2f' % self.linking_length

		elif self.type == HDEF_TYPE_VMAX:
			
			substr += r'max'

		elif self.type == HDEF_TYPE_ORBITING:
			
			if self.subtype == HDEF_SUBTYPE_ORB_ALL:
				substr += r'orb,all'
			elif self.subtype == HDEF_SUBTYPE_ORB_PERCENTILE:
				substr += r'orb,%.0f\%%' % self.percentile
			else:
				raise Exception('Invalid orbiting subtype %d.' % self.subtype)

		else:
			raise Exception('Invalid type %d.' % self.type)

		if suppress_time or self.time == HDEF_TIME_NOW:
			pass
		elif self.time == HDEF_TIME_ACC:
			substr += r',acc'
		elif self.time == HDEF_TIME_PEAK:
			substr += r',peak'
		else:
			raise Exception('Invalid halo definition time identifier %d.' % self.time)

		if suppress_bound or self.ptl_select == HDEF_PTLSEL_DEFAULT:
			pass
		elif self.ptl_select == HDEF_PTLSEL_ALL:
			substr += r',all'
		elif self.ptl_select == HDEF_PTLSEL_BOUND:
			substr += r',bnd'
		elif self.ptl_select == HDEF_PTLSEL_TRACER:
			substr += r',tcr'
		elif self.ptl_select == HDEF_PTLSEL_ORBITING:
			substr += r',orb'
		else:
			raise Exception('Invalid halo bound/all identifier %d.' % self.ptl_select)

		if self.is_error == HDEF_ERR_NO:
			pass
		elif self.is_error == HDEF_ERR_1SIGMA:
			substr += r',err'
		elif self.is_error == HDEF_ERR_2SIGMA:
			substr += r',err2s'
		else:
			raise Exception('Invalid error state identifier %d.' % self.is_error)

		if suppress_source or self.source == HDEF_SOURCE_ANY:
			pass
		elif self.source == HDEF_SOURCE_CATALOG:
			substr += r',cat'
		elif self.source == HDEF_SOURCE_SPARTA:
			substr += r',spa'
		elif self.source == HDEF_SOURCE_SHELLFISH:
			substr += r',sfish'
		else:
			raise Exception('Invalid source identifier %d.' % self.source)

		if not suppress_flags:
			if self.flag_internal:
				substr += r',internal'

		label = label.replace(' ', '\ ')
		label = label.replace('_', '\_')
		
		if substr != '':
			if do_subscript:
				label += r'_{\rm %s}' % substr
			else:
				label = r'{\rm %s}' % substr
		
		if add_units:
			if self.quantity == HDEF_Q_UNKNOWN:
				pass
			elif self.quantity == HDEF_Q_RADIUS:
				label += r'\ (h^{-1}{\rm kpc})'
			elif self.quantity == HDEF_Q_MASS:
				label += r'\ (h^{-1}M_{\odot})'
			elif self.quantity == HDEF_Q_PEAKHEIGHT:
				pass
			elif self.quantity == HDEF_Q_VCIRC:
				label += r'\ ({\rm km}/s)'
			else:
				raise Exception('Invalid halo quantity %d.' % self.quantity)
		
		if add_dollars:
			label = r'$%s$' % label

		return label

	# ---------------------------------------------------------------------------------------------

	def overdensityStr(self):
		"""
		Convert an SO halo definition to a short overdensity identifier, such as "200m" or "500c".
		
		This function fails if the definition is not an SO definition, and ignores any additional
		properties of the definition.
			
		Returns
		-------------------------------------------------------------------------------------------
		so_str: str
			A string that identifies the overdensity of an SO definition.
		"""
		
		if self.type != HDEF_TYPE_SO:
			raise Exception('Cannot get SO overdensity string from non-SO definition of type %d.' % self.type)
			
		so_str = ''
	
		if self.subtype == HDEF_SUBTYPE_SO_MATTER:
			so_str += '%.0fm' % self.overdensity
		elif self.subtype == HDEF_SUBTYPE_SO_CRITICAL:
			so_str += '%.0fc' % self.overdensity
		elif self.subtype == HDEF_SUBTYPE_SO_VIRIAL:
			so_str += 'vir'
		else:
			raise Exception('Invalid SO subtype %d.' % self.subtype)
	
		return so_str

###################################################################################################
# FUNCTIONS
###################################################################################################

def strToDef(defstr):
	"""
	Convert a halo definition string to a halo definition object.
	
	Parameters
	-----------------------------------------------------------------------------------------------
	defstr: str
		A string that uniquely describes the halo definition in the correct format.
	
	Returns
	-----------------------------------------------------------------------------------------------
	hdef: HaloDefinition
		A halo definition object that can be converted back into the same string.
	"""

	if len(defstr) == 0:
		raise Exception('Cannot generate mass definition object from empty string.')

	hdef = HaloDefinition()
	
	w = defstr.split('_')
	str1 = w[0]
	i = 0
	
	# Dissect the first part, the main identifier. We do allow variations in capitalization here, 
	# although only one version is "correct" in the sense that it is output when a definition is
	# converted to a string. We do need to be careful not to convert identifiers such as "vir" to
	# V and so on. 
	if str1.startswith('R') or str1.startswith('r'):
		hdef.quantity = HDEF_Q_RADIUS
		i += 1
	elif str1.startswith('M') or (str1.startswith('m') and (not str1.startswith('max'))):
		hdef.quantity = HDEF_Q_MASS
		i += 1
	elif str1.startswith('NU') or str1.startswith('Nu') or str1.startswith('nu'):
		hdef.quantity = HDEF_Q_PEAKHEIGHT
		i += 2
	elif str1.startswith('V') or (str1.startswith('v') and (not str1.startswith('vir'))):
		hdef.quantity = HDEF_Q_VCIRC
		i += 1
	else:
		hdef.quantity = HDEF_Q_UNKNOWN

	str1 = str1[i:]
	num_str = None

	if str1.startswith('vir'):
		hdef.type = HDEF_TYPE_SO
		hdef.subtype = HDEF_SUBTYPE_SO_VIRIAL
	elif str1.startswith('max'):
		hdef.type = HDEF_TYPE_VMAX
	elif str1.startswith('sp-slp-prf'):
		hdef.type = HDEF_TYPE_SPLASHBACK
		hdef.subtype = HDEF_SUBTYPE_SP_SLOPE_PROFILE
	elif str1.startswith('sp-slp-med'):
		hdef.type = HDEF_TYPE_SPLASHBACK
		hdef.subtype = HDEF_SUBTYPE_SP_SLOPE_MEDIAN
	elif str1.startswith('sp-slp-shl'):
		hdef.type = HDEF_TYPE_SPLASHBACK
		hdef.subtype = HDEF_SUBTYPE_SP_SLOPE_SHELL
	elif str1.startswith('sp-apr-mn'):
		hdef.type = HDEF_TYPE_SPLASHBACK
		hdef.subtype = HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN
	elif str1.startswith('sp-apr-p'):
		hdef.type = HDEF_TYPE_SPLASHBACK
		hdef.subtype = HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE
		num_str = str1[8:]
	elif str1.startswith('sp-aps-mn'):
		hdef.type = HDEF_TYPE_SPLASHBACK
		hdef.subtype = HDEF_SUBTYPE_SP_APOCENTER_SHELL_MEAN
	elif str1.startswith('sp-aps-p'):
		hdef.type = HDEF_TYPE_SPLASHBACK
		hdef.subtype = HDEF_SUBTYPE_SP_APOCENTER_SHELL_PERCENTILE
		num_str = str1[8:]
	elif str1.startswith('fof-b'):
		hdef.type = HDEF_TYPE_FOF
		num_str = str1[5:]
	elif str1.startswith('orb-all'):
		hdef.type = HDEF_TYPE_ORBITING
		hdef.subtype = HDEF_SUBTYPE_ORB_ALL
	elif str1.startswith('orb-p'):
		hdef.type = HDEF_TYPE_ORBITING
		hdef.subtype = HDEF_SUBTYPE_ORB_PERCENTILE
		num_str = str1[5:]
	elif str1.endswith('m'):
		hdef.type = HDEF_TYPE_SO
		hdef.subtype = HDEF_SUBTYPE_SO_MATTER
		num_str = str1[:-1]
	elif str1.endswith('c'):
		hdef.type = HDEF_TYPE_SO
		hdef.subtype = HDEF_SUBTYPE_SO_CRITICAL
		num_str = str1[:-1]
	else:
		raise Exception('Did not understand identifier %s in definition %s.' % (str1, defstr))
	
	if num_str is not None:
		num = float(num_str)
		if hdef.type == HDEF_TYPE_SO:
			hdef.overdensity = num
		elif hdef.type == HDEF_TYPE_SPLASHBACK:
			hdef.percentile = num
		elif hdef.type == HDEF_TYPE_FOF:
			hdef.linking_length = num
		elif hdef.type == HDEF_TYPE_ORBITING:
			hdef.percentile = num
		else:
			raise Exception('Found number string but cannot use for type %d, definition %s.' \
						% (hdef.type, defstr))
	
	# Other parts
	for i in range(1, len(w)):
		if w[i] == 'all':
			hdef.ptl_select = HDEF_PTLSEL_ALL
		elif w[i] == 'bnd':
			hdef.ptl_select = HDEF_PTLSEL_BOUND
		elif w[i] == 'tcr':
			hdef.ptl_select = HDEF_PTLSEL_TRACER
		elif w[i] == 'orb':
			hdef.ptl_select = HDEF_PTLSEL_ORBITING
		elif w[i] == 'acc':
			hdef.time = HDEF_TIME_ACC
		elif w[i] == 'peak':
			hdef.time = HDEF_TIME_PEAK
		elif w[i] == 'cat':
			hdef.source = HDEF_SOURCE_CATALOG
		elif w[i] == 'spa':
			hdef.source = HDEF_SOURCE_SPARTA
		elif w[i] == 'sfish':
			hdef.source = HDEF_SOURCE_SHELLFISH
		elif w[i] == 'err':
			hdef.is_error = HDEF_ERR_1SIGMA
		elif w[i] == 'err2s':
			hdef.is_error = HDEF_ERR_2SIGMA
		elif w[i] == 'internal':
			hdef.flag_internal = True
		else:
			raise Exception('Unknown extension %s in definition %s.' % (w[i], defstr))

	return hdef

###################################################################################################
