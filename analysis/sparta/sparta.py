###################################################################################################
#
# sparta.py                 (c) Benedikt Diemer
#     				    	    diemer@umd.edu
#
###################################################################################################

"""
This file contains some of the main routines for the python analysis package for SPARTA.

---------------------------------------------------------------------------------------------------
Code examples
---------------------------------------------------------------------------------------------------

The main purpose of this module is to translate SPARTA output from its HDF5 format into a python
structure of dictionaries and structured arrays. In this section, we explore some of the
functionality of the :func:`load` function through code examples. First, we need to import the
sparta module. We also set a default filename variable::
	
	from sparta import sparta
	
	filename = '/Users/you/some/dir/sparta.hdf5'

===================================================================================================
Basic structure
===================================================================================================

We can now attempt to execute the load function. It always returns a dictionary::

	dic = sparta.load(filename)
	
	>>> sparta.load: Loading file /Users/you/some/dir/sparta.hdf5.
	>>> sparta.load: Loading 25181 halos from SPARTA file...

	print(dic.keys())

	>>> dict_keys(['anl_prf', 'tcr_ptl', 'halos', 'config', 'simulation'])

If we execute the load function without any parameters besides the filename, the function loads
all data from the SPARTA file. For large files, this can take a long time or even exceed the 
available memory! In the case above, the file contained a total of 25181 halos. Here, the term 
"halo" means a branch in a merger tree, i.e., the history of a halo over time. This history begins
when the halo is first detected by the halo finder and ends when the halo disappears, generally
through merging into another, larger halo or because the simulation ends. Besides the halo data, 
the file seems to contain particle tracer information (the ``tcr_ptl`` sub-dictionary) as well as
a density profile analysis (``anl_prf``). See the :doc:`intro` for an introduction to these
abbreviations.

In addition, the dictionary always contains the ``config`` and ``simulation`` sub-dictionaries
which have the same content as the eponymous groups in the HDF5 file (see :doc:`analysis_hdf5`). 
Let's explore their content a little::

	for k in sorted(dic['config'].keys()):
		x = dic['config'][k]
		if not isinstance(x, dict):
			print('%-30s  %-40s' % (k, str(x)))

	>>> cat_halo_jump_tol_box           0.03                                    
	>>> cat_halo_jump_tol_phys          10.0                                    
	>>> ...
	>>> snap_path                       b'/Users/you/snapdir_%04d/snapshot_%04d.%d'
	>>> snap_sim_type                   0                                       
	>>> tcr_ptl_create_radius           2.0                                     
	>>> tcr_ptl_delete_radius           3.0            	

The dictionary contains all user-defined configuration parameters (see :doc:`run_config`). Note
that we did not display the entries that are themselves dictionaries. Those contain the config
parameters for particular :doc:`run_rs` or :doc:`run_al`::

	for k in sorted(dic['config'].keys()):
		x = dic['config'][k]
		if isinstance(x, dict):
			print(k)

	>>> anl_prf
	>>> res_oct
	
For example, for the orbit counting results that are contained in this example file::

	for k in sorted(dic['config']['res_oct'].keys()):
		print('%-30s  %-40s' % (k, str(dic['config']['res_oct'][k])))

	>>> res_oct_max_norbit              3 

Similarly, the ``simulation`` sub-dictionary contains information about the simulation that SPARTA
was run on::
	
	for k in sorted(dic['simulation'].keys()):
		print('%-20s  %-40s' % (k, str(dic['simulation'][k])))

	>>> Omega_L               0.73                                    
	>>> Omega_m               0.27                                    
	>>> box_size              62.5                                    
	>>> ...

===================================================================================================
Loading specific halos
===================================================================================================

.. warning::
	The order of the returned halo data is the order in which the halos appear in the
	SPARTA file, not the order in which they are requested (if the ``halo_ids`` parameter is used).
	
	This ordering also means that the order may vary between two otherwise identical SPARTA runs
	because the output ordering is non-deterministic. When comparing two files, one must always 
	match the halo IDs.

---------------------------------------------------------------------------------------------------
Function documentation
---------------------------------------------------------------------------------------------------

.. autosummary:: 
	load
	findHalos
	matchAnalyses
	haloIsHost
	haloIsSub
	haloIsSubPermanently
	haloIsGhost
"""

import time
import numpy as np
import h5py

from . import utils

###################################################################################################
# SPARTA-RELATED CONSTANTS
###################################################################################################

# The status history of each halo is recorded in a SPARTA output file as a series of integers for
# each snapshot of the simulation. For all snapshots where the halo existed, this number takes on
# one of the following values:
#
# HALO_STATUS_NOT_FOUND            The halo was not found in a SPARTA file (e.g., by MORIA)
# HALO_STATUS_NONE                 Undefined status; this should not happen where there is output
# HALO_STATUS_HOST                 The halo was a host (with tracers, results etc)
# HALO_STATUS_SUB                  The halo was a sub (and thus not recording tracers, results...)
# HALO_STATUS_BECOMING_SUB         The halo became a subhalo in this snapshot
# HALO_STATUS_BECOMING_HOST        The halo will become a host halo (but is a sub in this snapshot!)
# HALO_STATUS_BECOMING_SUB_HOST    The halo became a sub and will be a host in the next snapshot
# HALO_STATUS_SWITCHED_HOST        The halo remained a subhalo, but switched host halos
# HALO_STATUS_GHOST_HOST           The halo is being tracked through its particles and is a host.
# HALO_STATUS_GHOST_SUB            The ghost is a subhalo of another (non-ghost) halo.
# HALO_STATUS_GHOST_SWITCHING      The subhalo ghost is switching from one host to another.
#
# Note that all statuses other than 11 and 30 (12, 16, 17, 18, 20, 31, 32) indicate subhalos.

HALO_STATUS_NOT_FOUND = -2
HALO_STATUS_NONE = -1
HALO_STATUS_HOST = 10
HALO_STATUS_SUB = 20
HALO_STATUS_BECOMING_SUB = 21
HALO_STATUS_BECOMING_HOST = 22
HALO_STATUS_BECOMING_SUB_HOST = 23
HALO_STATUS_SWITCHED_HOST = 24
HALO_STATUS_GHOST_HOST = 30
HALO_STATUS_GHOST_SUB = 31
HALO_STATUS_GHOST_SWITCHING = 32

# A halo can fundamentally end for three reasons: it merged into another halo, it reached the end
# of the simulation, or an error occurred. Some of these scenarios are further distinguished. For
# example, ghosts have specific codes for how they disappeared, but the following statuses
# all indicate that the halo merged away:
#
# MERGED            The halo's main branch ended, i.e. it merged with another, larger halo.
# GHOST_CENTER      A ghost was found to have merged with its host as its center was within a
#                   small fraction of the host's radius for a significant fraction of a dynamical
#                   time.
# GHOST_TOO_SMALL   The ghost was found and its center determined, but its mass was below the
#                   limiting number of particles.
# GHOST_NOT_FOUND   Not enough of the ghost's particles could be found at all in the new
#                   snapshot (this should be rare, because it means that the particles moved far
#                   from where they were expected to be)
# GHOST_POSITION    The position and/or velocity of the ghost could not be reliably determined.
#
# There is only one status for reaching the last snapshot, regardless of whether it refers to a
# halo or ghost:
#
# LAST_SNAP         The halo reached the last snap that was evaluated.
#
# Finally, there are a number of error codes that should occur never or rarely:
#
# NOT_FOUND         The halo could not be found in the catalog. This should never (or very rarely
#                   occur, otherwise there is something wrong with the merger tree code).
# JUMP              The halo jumped a physically impossible distance. This indicates a problem in
#                   the merger tree code.
# SEARCH_RADIUS     No physically sensible particle distribution could be found. This can happen
#                   with very small or interpolated ('phantom') halos.

HALO_ENDED_MERGED = 50
HALO_ENDED_GHOST_CENTER = 51
HALO_ENDED_GHOST_TOO_SMALL = 52
HALO_ENDED_GHOST_NOT_FOUND = 53
HALO_ENDED_GHOST_POSITION = 54
HALO_ENDED_LAST_SNAP = 60
HALO_ENDED_NOT_FOUND = 70
HALO_ENDED_JUMP = 71
HALO_ENDED_HOST_ENDED = 72
HALO_ENDED_SEARCH_RADIUS = 73

###################################################################################################

# The Rsp analysis can succeed or fail due to a number of reasons. The status field is set
# accordingly:
#
# ANL_RSP_STATUS_UNDEFINED            This code should be used as a placeholder, but never be written
#                                     into the output file; that would indicate an internal error.
#
# ANL_RSP_STATUS_SUCCESS              The analysis succeeded, all output values can be used.
#
# ANL_RSP_STATUS_HALO_NOT_VALID       The halo did not exist at a particular snapshot, or could not
#                                     be analyzed for some other reason (e.g., it had not lived long
#                                     enough). The exact meaning of this field depends on the
#                                     specific analysis.
#
# ANL_RSP_STATUS_HALO_NOT_SAVED       The halo was not saved to the SPARTA output file at all. This
#                                     error code is not used within SPARTA, but can be used when
#                                     analyzing the output file (e.g., by MORIA).
#
# ANL_RSP_STATUS_NOT_FOUND            The halo was written to the SPARTA output file, but does not
#                                     have the analysis in question associated with it. This error
#                                     code is not used within SPARTA, but can be used when analyzing
#                                     the output file (e.g., by MORIA).
#
# ANL_RSP_STATUS_INSUFFICIENT_EVENTS  There were too few splashback events in the maximal time range
#                                     considered.
#
# ANL_RSP_STATUS_INSUFFICIENT_WEIGHT  There were enough splashback events, but their total statistical
#                                     weight in this time bin was too low.

ANL_RSP_STATUS_UNDEFINED = 0
ANL_RSP_STATUS_SUCCESS = 1
ANL_RSP_STATUS_HALO_NOT_VALID = 2
ANL_RSP_STATUS_HALO_NOT_SAVED = 3
ANL_RSP_STATUS_NOT_FOUND = 4
ANL_RSP_STATUS_INSUFFICIENT_EVENTS = 5
ANL_RSP_STATUS_INSUFFICIENT_WEIGHT = 6

###################################################################################################

# The HaloProperties analysis returns the same statuses as the Rsp analysis above, except for the 
# final two:
#
# ANL_HPS_STATUS_SO_TOO_SMALL         The overdensity never reached the threshold, even at small
#                                     radii. The mass/radius could not be computed.
#
# ANL_HPS_STATUS_SO_TOO_LARGE         The overdensity never fell below the threshold, even at large
#                                     radii. The mass/radius could not be computed.
#
# ANL_HPS_STATUS_ORB_ZERO             A percentile of orbiting particles could not be computed 
#                                     because there were no orbiting particles.

ANL_HPS_STATUS_UNDEFINED = 0
ANL_HPS_STATUS_SUCCESS = 1
ANL_HPS_STATUS_HALO_NOT_VALID = 2
ANL_HPS_STATUS_HALO_NOT_SAVED = 3
ANL_HPS_STATUS_NOT_FOUND = 4
ANL_HPS_STATUS_SO_TOO_SMALL = 5
ANL_HPS_STATUS_SO_TOO_LARGE = 6
ANL_HPS_STATUS_ORB_ZERO = 7

###################################################################################################

# The profiles analysis returns the same statuses as the Rsp analysis above, except for the 
# final one:
#
# ANL_PRF_STATUS_SEARCH_RADIUS        The search radius had to be reduced because it was not 
#                                     entirely contained in the region for which particles had been
#                                     loaded. Thus, the profile could not be computed. This error 
#                                     reflects the rare case where SPARTA underestimates a halo 
#                                     radius, leading to too small a particle box.
#
# ANL_PRF_STATUS_CATALOG_RADIUS       R200m_all could not be computed for this halo, and the
#                                     catalog radius was used instead. In this case, we do not
#                                     compute profiles as they would be normalized by a different
#                                     radius definition.

ANL_PRF_STATUS_UNDEFINED = 0
ANL_PRF_STATUS_SUCCESS = 1
ANL_PRF_STATUS_HALO_NOT_VALID = 2
ANL_PRF_STATUS_SEARCH_RADIUS = 5
ANL_PRF_STATUS_CATALOG_RADIUS = 6

###################################################################################################

# Similar to SPARTA itself, MORIA also returns a status for certain fields. Regardless of the 
# exact halo analysis, the following codes are defined:

# STATUS_HALO_DOES_NOT_EXIST        This halo was not present in the SPARTA file at all
#
# STATUS_ANL_DOES_NOT_EXIST         This analysis was not present in the SPARTA file or was not
#                                   considered for another reason. Thus, no value from SPARTA was 
#                                   found for the output in question.
#
# STATUS_UNDEFINED                  Internal status, should never occur in output file
# 
# STATUS_SPARTA_SUCCESS             The analysis was present in SPARTA, and its output was used
#                                   for the value in question. This should ideally be the status
#                                   for the vast majority of halos and outputs.

MORIA_STATUS_HALO_DOES_NOT_EXIST = -2
MORIA_STATUS_ANL_DOES_NOT_EXIST = -1
MORIA_STATUS_UNDEFINED = 0
MORIA_STATUS_SPARTA_SUCCESS = 1

###################################################################################################
# OTHER CONSTANTS
###################################################################################################

# N_MAX_HDF5_MASKING is the number of elements to load from an HDF5 array where we switch from HDF5
# masking to numpy masking. HDF5 can get very slow when loading a significant number, but not all, 
# elements of an array. Loading the entire array scales with the total number of elements, 
# loading a masked array scales with the total number of loaded elements in HDF5, but the load
# times differ by about a factor of 1000. Thus, as a rough rule of thumb, we should load the 
# entire array if there are more than 1000 items to load, unless we are loading the entire 
# array.

N_MAX_HDF5_MASKING = 1000

###################################################################################################

def load(filename = None, hdf5_file = None, 
				halo_ids = None, halo_mask = None,
				load_halo_data = True, analyses = None, tracers = None, results = None,
				anl_match = None, anl_pad_unmatched = True,
				res_match = None, res_pad_unmatched = True,
				log_level = 1):
	"""
	Load the contents of a SPARTA HDF5 results file.

	Parameters
	-----------------------------------------------------------------------------------------------
	filename: str
		The path to the sparta file. Either this field of hdf5_file must not be None.
	hdf5_file: HDF5 file object
		Sometimes multiple load operations need to be performed, in which case the user may
		prefer not to keep opening and closing the HDF5 file. If a valid file object is passed,
		that file object is used and the filename parameter is ignored.
	halo_ids: array_like
		If this field is None, the results for all halos are loaded. If it contains the catalog 
		IDs of one or multiple halos (at any snapshot!), only the results for those halos will be 
		loaded. The order of the returned halos may not be the same as the order of this input 
		list!
	halo_mask: array_like
		If this field is None, the results for all halos are loaded (unless a selection is made
		with the halo_ids parameter instead). If not None, the parameter must be a numpy array
		with n_halos entries, where True means a halo is loaded. Such an array can, for example,
		be generated with the findIDs() function, and speeds up loading because the IDs do not 
		have to be searched in the halo ID array.
	load_halo_data: bool
		If True, the properties of halos (such as the histories of their ID, radius, and status)
		will be loaded, otherwise they will be omitted.
	analyses: array_like
		If None, all analyses are loaded. Otherwise a list of analysis names to load, with the 
		names corresponding to the abbreviated analysis names used in the sparta results file (e.g. 
		"rsp", see the introduction section of the documentation).
	tracers: array_like
		If None, all tracers are loaded. Otherwise a list of tracer names to load, with the names
		corresponding to the abbreviated tracer names used in the sparta results file (e.g. "ptl"
		or "sho", see the introduction section of the documentation).
	results: array_like
		If None, all tracer results are loaded. Otherwise a list of result names to load, with 
		the names corresponding to the abbreviated result names used in the sparta results file 
		(e.g. "ifl" or "sbk", see the introduction section of the documentation).
	anl_match: array_like
		If None, no matching is performed. Otherwise, the parameter can be the name of one analysis
		(e.g. 'rsp') or a list of analyses (e.g. ['rsp']) for which matching is performed. This 
		means that the halo and analysis arrays have the same dimension, i.e. that each halo is 
		assigned exactly one analysis of each of the given types. If there are more than one of an
		analysis, the first one is returned. The anl_pad_unmatched parameter determines what happens 
		when halos do not have an analysis.
	anl_pad_unmatched: bool	
		If we are matching analyses (see above), we can either discard halos that do not have the
		analyses in question (``anl_pad_unmatched = False``), or we can pad the analysis arrays
		with empty elements (``anl_pad_unmatched = True``). Such padded elements can easily be 
		identified by their ID field which is ``halo_id = -1``.
	res_match: array_like
		If None, no matching is performed. Otherwise, the parameter must be a list of results
		with at least two elements, e.g. ``res_match = ['ifl', 'sbk']``. In that case, the 
		listed results are matched by their tracer ID. Since all result arrays are sorted by 
		tracer ID, the matched arrays will naturally be in the same order. The res_pad_unmatched
		parameter determines what happens to results that do not have a counterpart. If a tracer 
		does not have one of the matched results, no matching is performed and a warning is output.
	res_pad_unmatched: bool
		If matching results (see above), we can either discard (``res_pad_unmatched = False``) all 
		unmatched results (i.e. results from tracers that do not have all of the matched types), 
		or we can keep them (``res_pad_unmatched = True``). In the latter case, in order to maintain the 
		synched array ordering of the result arrays, we need to insert void records where results 
		are missing. The void records can easily be identified by their ``tracer_id = -1`` value.
	log_level: int
		If zero, no output is generated. One is the default level of output, greater numbers lead
		to very detailed output including timing information.
		
	Returns
	-----------------------------------------------------------------------------------------------
	dic: dictionary
		A dictionary containing essentially the same file structure as the HDF5 file, depending
		on the options chosen.
	"""
	
	# ---------------------------------------------------------------------------------------------

	def get_tcr_res_name(tracer, result):
		return tracer + '_' + result

	# ---------------------------------------------------------------------------------------------
	
	def get_anl_name(al):
		return 'anl_' + al

	# ---------------------------------------------------------------------------------------------

	# We first load the first-pointer and n for each halo for this group. Then we compile
	# the slice for the group arrays. If all halos are selected, this slice is trivial and
	# should not be manually compiled from the individual halos. Furthermore, if only one
	# halo has been selected, it is faster to use slice notation than index notation, so 
	# we treat this case separately.

	def setPointerList(grp, n_load_halos, dset_name, use_numpy_masking):
		
		dset_name_first = dset_name + '_first'
		dset_name_n = dset_name + '_n'
		n_tot = grp.attrs['n']
		
		if use_numpy_masking:
			halo_data[dset_name_first] = np.array(grp['halo_first'])[slc_halos, ...]
			halo_data[dset_name_n] = np.array(grp['halo_n'])[slc_halos, ...]
		else:
			halo_data[dset_name_first] = np.array(grp['halo_first'][slc_halos, ...])
			halo_data[dset_name_n] = np.array(grp['halo_n'][slc_halos, ...])
		
		# If all halos are loaded, we'll need to load all items in this group. If one halo is 
		# loaded, we need to be careful: there may or may not be item(s) for this halo in which 
		# case the first element must be -1. The same goes when multiple halos are being loaded.
		if n_load_halos == n_halos:
			slc = slice(0, n_tot)
			n_load = n_tot
		elif n_load_halos == 1:
			n_load = halo_data[dset_name_n][0]
			slc = slice(halo_data[dset_name_first][0], halo_data[dset_name_first][0] + halo_data[dset_name_n][0])
			if n_load > 0:
				halo_data[dset_name_first][0] = 0
			else:
				halo_data[dset_name_first][0] = -1
		else:
			slc = np.zeros((n_tot), bool)
			first = halo_data[dset_name_first]
			n = halo_data[dset_name_n]

			# If there is at most one analysis per halo (which is often the case), that makes our 
			# task easier. Note that we are assuming that the analyses are ordered in the same way
			# as the halo array, meaning that the "first" array is monotonically increasing.
			if np.max(n) <= 1:
				slc = np.zeros((n_tot), bool)
				# Careful here: first contains elements that are -1 and would set the last element 
				# in slc to True, which may be erroneous.
				slc[first[first >= 0]] = True
				n_load = np.count_nonzero(slc)
				mask_has_anl = (n > 0)
				halo_data[dset_name_first][np.logical_not(mask_has_anl)] = -1
				halo_data[dset_name_first][mask_has_anl] = np.arange(n_load)
			else:
				counter = 0
				for i in range(n_load_halos):
					slc[first[i]:first[i] + n[i]] = True
					if n[i] == 0:
						halo_data[dset_name_first][i] = -1
					else:
						halo_data[dset_name_first][i] = counter
						counter += n[i]
				n_load = counter

		return n_load, slc
	
	# ---------------------------------------------------------------------------------------------
	
	# Given an HDF5 group and its datasets, create numpy dtypes to mimic this data, as well as an
	# empty record that fits this data structure in case data need to be padded.
	
	def getDataLayout(grp):
		
		field_names = []
		dtypes = []
		empty_record = []
		for f in grp.keys():
			if f in ['halo_first', 'halo_n']:
				continue
			field_names.append(f)
			dtype = grp[f].dtype
			shape = grp[f].shape
			
			# The explicit string conversions ensure python 2.x compatibility
			if len(shape) > 1:
				shape_tuple = tuple(shape[1:])
				dtypes.append((str(f), dtype, shape_tuple))
			else:
				dtypes.append((str(f), dtype))
			
			if f == 'tracer_id' or f == 'halo_id':
				empty_record.append(-1)
			elif str.startswith(dtype.name, 'int'):
				if len(shape) > 1:
					empty_record.append(np.zeros(shape_tuple, int))
				else:
					empty_record.append(0)
			elif str.startswith(dtype.name, 'float'):
				if len(shape) > 1:
					empty_record.append(np.zeros(shape_tuple, float))
				else:
					empty_record.append(0.0)
			else:
				raise Exception('Could not set initializer for data type %s.' % dtype.name)
		
		empty_record = tuple(empty_record)
		
		return field_names, dtypes, empty_record

	# ---------------------------------------------------------------------------------------------
	# Open file, load general properties
	# ---------------------------------------------------------------------------------------------

	# If the user has passed an HDF5 file object we do not need to load a new one
	t = time.process_time()
	timer_init = -t
	timer_total = -t
		
	if hdf5_file is None:
		if filename is None:
			raise Exception('Need to pass either filename or hdf5_file to load routine.')
		if log_level >= 1:
			print('sparta.load: Loading file %s.' % (filename))
		file = h5py.File(filename, 'r')
	else:
		if log_level >= 1:
			print('sparta.load: Loading from previously opened file object.')
		file = hdf5_file

	# Create output dictionary, load general simulation and config attributes
	if log_level >= 3:
		print('sparta.load: Creating output dictionary.')
	dic = {}
	dic_cfg = {}
	dic_sim = {}
	grp_cfg = file['config']
	grp_sim = file['simulation']
	grp_halos = file['halos']
	for f in grp_cfg.attrs:
		dic_cfg[f] = grp_cfg.attrs[f]
	for f in grp_cfg.keys():
		dic_cfg[f] = {}
		for ff in grp_cfg[f].attrs:
			dic_cfg[f][ff] = grp_cfg[f].attrs[ff]
	for f in grp_sim.attrs:
		dic_sim[f] = grp_sim.attrs[f]
	dic['config'] = dic_cfg
	dic['simulation'] = dic_sim
	
	#n_snaps = grp_sim.attrs['n_snaps']
	n_halos = grp_halos.attrs['n_halos']
	
	# Before we can create the halo structure array, we need to make a list of all the analyses,
	# tracers and results we need to keep track of. Also, we check whether the user has supplied 
	# lists of those objects to load, or whether we wish to load all of them
	if log_level >= 3:
		print('sparta.load: Creating lists of objects to load.')
	if analyses is None:
		analyses = []
		for f in file.keys():
			if f.startswith('anl_'):
				analyses.append(f.replace('anl_', ''))
	n_analyses = len(analyses)
	
	if tracers is None:
		tracers = []
		for f in file.keys():
			if f.startswith('tcr_'):
				tracers.append(f.replace('tcr_', ''))
	n_tracers = len(tracers)
	
	if results is None:
		results = []
		for tracer in tracers:
			for f in file['tcr_' + tracer].keys():
				if f.startswith('res_'):
					f_str = f.replace('res_', '')
					if not f_str in results:
						results.append(f_str)
	n_results = len(results)
	
	result_mask = np.zeros((n_tracers, n_results), bool)
	tt_rs_str = []
	for tt in range(n_tracers):
		for rs in range(n_results):
			result_mask[tt, rs] = ('res_' + results[rs] in file['tcr_' + tracers[tt]].keys())
			if (result_mask[tt, rs]):
				tt_rs_str.append(get_tcr_res_name(tracers[tt], results[rs]))
	n_tt_rs = len(tt_rs_str)
	
	t = time.process_time()
	timer_init += t
	timer_halo_idx = -t

	# ---------------------------------------------------------------------------------------------
	# Determine halo mask, load halo data
	# ---------------------------------------------------------------------------------------------
	
	# Now find the indices of the halos to be loaded. The optimal way to do this depends on how 
	# many halos we are loading. If all, we create a slice object. If only specific halos, we 
	# create a list of array indices. Note that creating a slice object with one halo is not faster
	# than the index variant, so we do not distinguish this case. Using a boolean mask leads to 
	# errors in the hdf5 library down the road.
	if log_level >= 3:
		print('sparta.load: Loading halo data.')
	if halo_ids is not None and halo_mask is not None:
		raise Exception('Both halo_ids and halo_mask were passed, but only one of those parameters can be used.')
	
	use_numpy_masking = None
	if halo_ids is None and halo_mask is None:
		slc_halos = slice(0, n_halos)
		n_load_halos = n_halos
		use_numpy_masking = False
		
	elif halo_ids is not None:
		if not utils.isArray(halo_ids):
			halo_ids = [halo_ids]
		n_load_halos = len(halo_ids)
		ds_halo_id = grp_halos['id']
		slc_halos = np.zeros((n_halos), bool)
		slc_halos = utils.findIndicesMultiD(ds_halo_id[:], halo_ids, algorithm = 'auto')
		
	elif halo_mask is not None:
		slc_halos = np.where(halo_mask)
		n_load_halos = np.count_nonzero(halo_mask)
		use_numpy_masking = True
		
	# Determine whether we should let HDF5 mask out the halos or do it in numpy. The need for this
	# decision is that HDF5 can get very slow when loading a significant number, but not all, 
	# elements of an array.
	if use_numpy_masking is None:
		use_numpy_masking = (n_load_halos < n_halos) and (n_load_halos > N_MAX_HDF5_MASKING)
	
	t = time.process_time()
	timer_halo_idx += t
	timer_halo_load = -t

	if log_level >= 1:
		if n_load_halos == n_halos:	
			load_str = 'entire array'
		else:
			if use_numpy_masking:
				load_str = 'using numpy masking'
			else:
				load_str = 'using HDF5 masking'
		print('sparta.load: Loading %d/%d halos from SPARTA file (%s)...' % (n_load_halos, n_halos, load_str))

	# Load halo data. We do a little bit of reshuffling from the original SPARTA output file here:
	# we add the *_first and *_n arrays from each analysis and tracer result to the halo array, 
	# since they have a dimension of n_halos. They will be loaded later though. If the user wants 
	# to load the halo data, we also load all other fields in the halos group.
	if load_halo_data:
		_, dtypes, _ = getDataLayout(grp_halos)
	else:
		dtypes = []
	
	for i in range(n_analyses):
		dtypes.append((str('anl_' + analyses[i] + '_first'), 'i8'))
		dtypes.append((str('anl_' + analyses[i] + '_n'), 'i8'))
	for i in range(n_tt_rs):
		dtypes.append((str(tt_rs_str[i] + '_first'), 'i8'))
		dtypes.append((str(tt_rs_str[i] + '_n'), 'i8'))

	halo_data = np.zeros((n_load_halos), dtype = dtypes)
	if load_halo_data:
		for f in list(grp_halos.keys()):
			if use_numpy_masking:
				halo_data[f] = np.array(grp_halos[f])[slc_halos, ...]
			else:
				halo_data[f] = np.array(grp_halos[f][slc_halos, ...])

	t = time.process_time()
	timer_halo_load += t

	# ---------------------------------------------------------------------------------------------
	# Load and match analyses
	# ---------------------------------------------------------------------------------------------

	# Note that in this case, it appears to be faster to let h5py do the slicing on the loaded
	# analyses rather than loading the entire array and slicing if afterwards.
	timer_anl_load = -t
	if log_level >= 3:
		print('sparta.load: Loading analyses.')
	anl_dtypes = []
	anl_empty_record = []
	for al in range(n_analyses):
		al_name = get_anl_name(analyses[al])
		grp_al = file[al_name]
		n_load_al, slc_al = setPointerList(grp_al, n_load_halos, al_name, use_numpy_masking)
		field_names, dtypes, empty_record = getDataLayout(grp_al)
		anl_dtypes.append(dtypes)
		anl_empty_record.append(empty_record)
		al_data = np.zeros((n_load_al), dtype = dtypes)
		for f in field_names:
			if use_numpy_masking:
				al_data[f] = np.array(grp_al[f])[slc_al, ...]
			else:
				al_data[f] = np.array(grp_al[f][slc_al, ...])
		dic[al_name] = al_data
	t = time.process_time()
	timer_anl_load += t
	
	# Match analyses
	timer_anl_match = -t
	if log_level >= 3:
		print('sparta.load: Matching analyses.')
	if anl_match is not None:
		if not isinstance(anl_match, (list, tuple)):
			anl_match = [anl_match]
		n_anl_match = len(anl_match)
		
		if anl_pad_unmatched:
			
			# If we're padding the analysis arrays, each array can be treated separately: they are
			# simply adjusted to have the same dimension as the halo array. If a halo has at least
			# one analysis of the given type, we use that, otherwise an empty record. Thus, each
			# halo ends up having one analysis at the same index as the halo.
			for i in range(n_anl_match):
				anl_name = 'anl_' + anl_match[i]
				anl_first = anl_name + '_first'
				anl_n = anl_name + '_n'
				anl_new = np.zeros((n_load_halos), dtype = anl_dtypes[i])
				anl_mask = (halo_data[anl_first] > -1)
				anl_new[anl_mask] = dic[anl_name][halo_data[anl_first][anl_mask]]
				anl_mask = np.logical_not(anl_mask)
				anl_new[anl_mask][:] = anl_empty_record[i]
				dic[anl_name] = anl_new
				halo_data[anl_first][:] = np.arange(0, n_load_halos)
				halo_data[anl_n][:] = 1
			
		else:
			
			# If we are not padding, things get a little more complicated because the size of the 
			# loaded halo array changes to only include halos that have all analyses that are 
			# matched.
			
			#n_load_halos = len(halo_data)
			#if log_level >= 1:
			#	print('sparta.load: Loading %d halos from SPARTA file...' % (n_load_halos))
			raise NotImplementedError('Unpadded analysis loading is not yet implemented.')
	
	t = time.process_time()
	timer_anl_match += t
	
	# ---------------------------------------------------------------------------------------------
	# Load and match tracers and results
	# ---------------------------------------------------------------------------------------------

	timer_tcr = -t
	if log_level >= 3:
		print('sparta.load: Loading tracers and tracer results.')
	timer_res = 0.0
	timer_res_load_halos = 0.0
	timer_res_mask = 0.0
	timer_res_load = 0.0
	timer_res_match = 0.0
	tt_rs_counter = 0
	for tt in range(len(tracers)):
		tcr_name = 'tcr_' + tracers[tt]
		if log_level >= 3:
			print('sparta.load: Loading tracer %s.' % tcr_name)
		if not tcr_name in file:
			continue
		grp_tracer = file[tcr_name]
		dic_tcr = {}

		t = time.process_time()
		timer_tcr += t
		timer_res -= t
		
		result_field_names = []
		result_dtypes = []
		result_empty_record = []
	
		for rs in range(n_results):

			t = time.process_time()
			timer_res += t
			timer_res_load_halos -= t
		
			if not result_mask[tt, rs]:
				continue
			rs_name = 'res_' + results[rs]
			if log_level >= 3:
				print('sparta.load: Loading result %s.' % rs_name)
			tt_rs_name = tt_rs_str[tt_rs_counter]
			grp_rs = grp_tracer[rs_name]

			t = time.process_time()
			timer_res_load_halos += t
			timer_res_mask -= t

			# We now compile a slice of results to load, and the shape of the structured result 
			# array. Note that when actually loading the arrays, there are two choices: we can
			# load the entire array and slice it in numpy, or let h5py do the slicing.
			n_load_rs, slc_rs = setPointerList(grp_rs, n_load_halos, tt_rs_name, use_numpy_masking)
			field_names, dtypes, empty_record = getDataLayout(grp_rs)
			rs_data = np.zeros((n_load_rs), dtype = dtypes)
			result_field_names.append(field_names)
			result_dtypes.append(dtypes)
			result_empty_record.append(tuple(empty_record))			

			t = time.process_time()
			timer_res_mask += t
			timer_res_load -= t
	
			for f in field_names:
				if use_numpy_masking:
					rs_data[f] = np.array(grp_rs[f])[slc_rs]
				else:
					rs_data[f] = np.array(grp_rs[f][slc_rs])
			dic_tcr[rs_name] = rs_data
			tt_rs_counter += 1

			t = time.process_time()
			timer_res_load += t
			timer_res -= t
		
		t = time.process_time()
		timer_res += t
		timer_res_match -= t

		# Check whether we need to perform a result matching for this tracer. This happens if the
		# user has passed a list of results to be matched, and if all of those results are present
		# in the current tracer.
		do_res_matching = False
		if res_match is not None:
			if not isinstance(res_match, (list, tuple)):
				raise Exception('The res_match parameter must be a list of result names.')
			n_res_match = len(res_match)
			if n_res_match < 2:
				raise Exception('The res_match list must have at least two items.')
			
			match_rs_idx = np.zeros((n_res_match), int)
			match_arrays = []
			match_id_arrays = []
			match_tt_rs_first = []
			match_tt_rs_n = []
			do_res_matching = True
			for i in range(n_res_match):
				if (not res_match[i] in results):
					do_res_matching = False
					print('WARNING: The result %s was not found and cannot be matched.' % res_match[i])
					break
				match_rs_idx[i] = results.index(res_match[i])
				if not result_mask[tt, match_rs_idx[i]]:
					do_res_matching = False
					print('WARNING: The result %s was not loaded for tracer %s, cannot res_match.' \
						% (res_match[i], tracers[tt]))
					break
				match_arrays.append(dic_tcr['res_' + res_match[i]])
				match_id_arrays.append(dic_tcr['res_' + res_match[i]]['tracer_id'])
				match_tt_rs_first.append(get_tcr_res_name(tracers[tt], results[match_rs_idx[i]]) + '_first')
				match_tt_rs_n.append(get_tcr_res_name(tracers[tt], results[match_rs_idx[i]]) + '_n')

		# Match the loaded results. Note that the same tracer ID can appear in multiple halos, so
		# this has to be done on a per-halo basis. Depending on res_pad_unmatched, we may be performing 
		# two quite different operations: if we are discarding unmatched results, the result arrays 
		# will shrink, whereas they will grow if we're padding the arrays.
		if do_res_matching:
			n_halos_print = int(n_load_halos / 10)
			n_halos_print = max(n_halos_print, 10)
			
			if res_pad_unmatched:

				# Run through the halos first to compute the size of the array we need. This is slow
				# but hard to avoid. Then we do a second run where we actually rearrange the data,
				# padding the arrays with empty records where necessary.		
				rs_data_new = []
				counter = 0
				n_new = 0
				for j in range(n_res_match):
					n_new += len(match_arrays[j])
				for j in range(n_res_match):
					rs_data_new.append(np.zeros((n_new), dtype = match_arrays[j].dtype))			
				for i in range(n_load_halos):
					if log_level >= 1 and i % n_halos_print == 0 and i != 0:
						print('sparta.load: Matching result IDs in tracer %s, halo %d/%d' \
							% (tracers[tt], i + 1, n_load_halos))
					tids = np.zeros((0), np.int64)
					for j in range(n_res_match):
						oldf = halo_data[match_tt_rs_first[j]][i]
						oldl = oldf + halo_data[match_tt_rs_n[j]][i]
						tids = np.concatenate((tids, match_id_arrays[j][oldf:oldl]), axis = 0)
					all_tids = np.unique(tids)
					n_new_halo = len(all_tids)
					all_tids.sort()
					newf = counter
					newl = newf + n_new_halo
					for j in range(n_res_match):
						oldf = halo_data[match_tt_rs_first[j]][i]
						oldl = oldf + halo_data[match_tt_rs_n[j]][i]
						mask = np.in1d(all_tids, match_id_arrays[j][oldf:oldl], assume_unique = True)
						(rs_data_new[j][newf:newl])[mask] = match_arrays[j][oldf:oldl]
						unmask = np.logical_not(mask)
						(rs_data_new[j][newf:newl])[unmask] = result_empty_record[match_rs_idx[j]]
					halo_data[match_tt_rs_first[j]][i] = newf
					halo_data[match_tt_rs_n[j]][i] = n_new_halo
					counter += n_new_halo
				for j in range(n_res_match):
					dic_tcr['res_' + res_match[j]] = rs_data_new[j][0:counter]
					
			else:

				counter = 0				
				for i in range(n_load_halos):
					if log_level >= 1 and i % n_halos_print == 0:
						print('sparta.load: Matching result IDs in tracer %s, halo %d/%d' \
							% (tracers[tt], i + 1, n_load_halos))
					tids_all = []
					n_tids = []
					for j in range(n_res_match):
						first = halo_data[match_tt_rs_first[j]][i]
						last = first + halo_data[match_tt_rs_n[j]][i]
						tids = np.array(match_id_arrays[j][first:last])
						tids_all.append(tids)
						n_tids.append(len(tids))
					for j in range(n_res_match):
						mask = np.ones((n_tids[j]), bool)
						for k in range(n_res_match):
							if j != k:
								mask = (mask & np.in1d(tids_all[j], tids_all[k]))
						n_new = np.count_nonzero(mask)
						first = halo_data[match_tt_rs_first[j]][i]
						last = first + halo_data[match_tt_rs_n[j]][i]
						dic_tcr['res_' + res_match[j]][counter:counter + n_new] = (match_arrays[j][first:last])[mask]
						halo_data[match_tt_rs_first[j]][i] = counter
						halo_data[match_tt_rs_n[j]][i] = n_new
					counter += n_new
				for j in range(n_res_match):
					dic_tcr['res_' + res_match[j]] = dic_tcr['res_' + res_match[j]][:counter]
					
		dic[tcr_name] = dic_tcr
	
		t = time.process_time()
		timer_res_match += t
		timer_tcr -= t

	t = time.process_time()
	timer_tcr += t

	# ---------------------------------------------------------------------------------------------
	# Close file, output timings
	# ---------------------------------------------------------------------------------------------

	timer_finalize = -t
	if log_level >= 3:
		print('sparta.load: Finalizing.')

	# Halo data is manipulated by the matching operations, thus we only add it to the dictionary 
	# at this point.
	dic['halos'] = halo_data
	
	if hdf5_file is None:
		file.close()
	
	t = time.process_time()
	timer_finalize += t
	timer_total += t

	if log_level >= 2:
		print('Timing information (sleep time excluded):')
		print('--------------------------------------------------')
		print('Preparation               %6.2f (%4.1f%%)' % (timer_init, timer_init / timer_total * 100.0))
		print('Finding halo IDs          %6.2f (%4.1f%%)' % (timer_halo_idx, timer_halo_idx / timer_total * 100.0))
		print('Loading halo data         %6.2f (%4.1f%%)' % (timer_halo_load, timer_halo_load / timer_total * 100.0))
		print('Loading tracers           %6.2f (%4.1f%%)' % (timer_tcr, timer_tcr / timer_total * 100.0))
		print('Loading analyses          %6.2f (%4.1f%%)' % (timer_anl_load, timer_anl_load / timer_total * 100.0))
		print('Matching analyses         %6.2f (%4.1f%%)' % (timer_anl_match, timer_anl_match / timer_total * 100.0))
		print('Result loop               %6.2f (%4.1f%%)' % (timer_res, timer_res / timer_total * 100.0))
		print('Loading result pointers   %6.2f (%4.1f%%)' % (timer_res_load_halos, timer_res_load_halos / timer_total * 100.0))
		print('Creating result masks     %6.2f (%4.1f%%)' % (timer_res_mask, timer_res_mask / timer_total * 100.0))
		print('Loading results           %6.2f (%4.1f%%)' % (timer_res_load, timer_res_load / timer_total * 100.0))
		print('Matching result IDs       %6.2f (%4.1f%%)' % (timer_res_match, timer_res_match / timer_total * 100.0))
		print('Finalizing                %6.2f (%4.1f%%)' % (timer_finalize, timer_finalize / timer_total * 100.0))
		print('--------------------------------------------------')
		print('Total                     %6.2f' % (timer_total))

	return dic

###################################################################################################

def findHalos(filename = None, hdf5_file = None, cuts = [], log_level = 1):
	"""
	Find halos in a SPARTA file according to certain criteria, output them as an ID list.
	
	The result of this function can be used as input to other functions such as load(). By default,
	the function tries to find the quantity passed in each cut in the structured halo array 
	in the SPARTA file. Some quantities are automatically generated, namely M200m (from R200m)
	and N200m (from M200m). Each cut dictionary must contain the following entries:
	
	* q: the identifier of the quantity to be cut on, e.g. R200m
	* min: the minimum value of this quantity
	* max: the maximum value of this quantity
	* possible parameters:
	
	  * a / t / z / snap: the time where the cut is considered
	  * a_max / t_max / z_max / snap_max: if passed, make a cut between t and t_max 
	 
	A special case is a cut on the halo status, that is, whether a halo was a host or subhalo or
	ghost. In that case, the keyword ``include`` must be in the dictionary, and contain a list of
	statuses to exclude which can be ``hosts``, ``subs``, or ``ghosts``.
	
	Parameters
	-----------------------------------------------------------------------------------------------
	filename: str
		The path to the sparta file. Either this field of hdf5_file must not be None.
	hdf5_file: HDF5 file object
		Sometimes multiple load operations need to be performed, in which case the user may
		prefer not to keep opening and closing the HDF5 file. If a valid file object is passed,
		that file object is used and the filename parameter is ignored.
	cuts: array_like
		A list of dictionaries, where each entry corresponds to a cut. 
	log_level: int
		Output level
		
	Returns
	-----------------------------------------------------------------------------------------------
	ids: array_like
		A list of halo IDs.
	mask: array_like
		A boolean array of dimension n_halos which can be used to speed up the load() function.
	"""
	
	if hdf5_file is None:
		file = h5py.File(filename, 'r')
	else:
		file = hdf5_file

	grp_halos = file['halos']
	grp_sim = file['simulation']
	n_halos = grp_halos.attrs['n_halos']
	n_snaps = grp_sim.attrs['n_snaps']
	
	# By default, all halos are returned
	mask = np.ones((n_halos), bool)
	loaded_q = {}
	
	for c in cuts:
		
		q = c['q']
		
		if q == 'status':
			qmin = None
			qmax = None
			if not 'include' in c:
				raise Exception('Expected "include" entry in cut dictionary for status.')
			if log_level >= 1:
				print('sparta.findIDs: Cutting on status, including %s.' % (str(c['include'])))
		else:
			qmin = c['min']
			qmax = c['max']
			if log_level >= 1:
				print('sparta.findIDs: Cutting on quantity %s [%.2e .. %.2e]' % (q, qmin, qmax))
		
		snap = None
		if 'snap' in c:
			snap = c['snap']
		elif 't' in c:
			snap = utils.findNearestIndexSorted(grp_sim.attrs['snap_t'], c['t'])
		elif 'a' in c:
			snap = utils.findNearestIndexSorted(grp_sim.attrs['snap_a'], c['a'])
		elif 'z' in c:
			a = 1.0 / (c['z'] + 1.0)
			snap = utils.findNearestIndexSorted(grp_sim.attrs['snap_a'], a)

		snapmax = None
		if 'snapmax' in c:
			snapmax = c['snapmax']
		elif 'tmax' in c:
			snapmax = utils.findNearestIndexSorted(grp_sim.attrs['snap_t'], c['tmax'])
		elif 'amax' in c:
			snapmax = utils.findNearestIndexSorted(grp_sim.attrs['snap_a'], c['amax'])
		elif 'zmax' in c:
			a = 1.0 / (c['zmax'] + 1.0)
			snapmax = utils.findNearestIndexSorted(grp_sim.attrs['snap_a'], a)
		
		snapmask = slice(0, n_snaps)
		if snap is not None and snapmax is not None:
			snapmask = slice(snap, snapmax)
		elif snap is not None:
			snapmask = slice(snap, snap + 1)
		
		# Decide what quantity needs to be loaded for this cut
		if q in ['M200m', 'N200m']:
			q_load = 'R200m'
		else:
			q_load = q
		
		# Now load from file or temporary memory
		if q_load in loaded_q:
			qd = loaded_q[q_load]
		elif q_load in grp_halos.keys():
			qd = np.array(grp_halos[q_load])
			loaded_q[q_load] = qd
		else:
			msg = 'Unknown cut quantity, %s.' % (q)
			raise Exception(msg)
		
		# Process cuts. If there this is not a special cut, we simply apply a min/max mask
		if q == 'status':
			if snap is None:
				raise Exception('Can only perform cut on halo status at one snapshot, need snapshot/z/a/t.')
			if snapmax is not None:
				raise Exception('Can only perform cut on halo status at one snapshot, not on a range.')
			status = qd[:, snap]
			include = c['include']
			mask_status = np.zeros_like(status, bool)
			for inc in include:
				if inc == 'hosts':
					mask_status = mask_status | haloIsHost(status)
				elif inc == 'subs':
					mask_status = mask_status | (haloIsSub(status) & np.logical_not(haloIsGhost(status)))
				elif inc == 'ghosts':
					mask_status = mask_status | (haloIsGhost(status))
				else:
					raise Exception('Unknwon halo status, %s; can be hosts, subs, or ghosts.' % inc)
			mask = mask & mask_status
			
		elif q in ['M200m', 'N200m']:
			R200m = qd[:, snapmask]
			rho_200m = 200.0 * utils.RHO_CRIT_0_KPC3 * (1.0 + grp_sim.attrs['snap_z'][snapmask])**3 \
				* grp_sim.attrs['Omega_m']
			M200m = R200m**3 * 4.0 / 3.0 * np.pi * rho_200m
			M200m_max = np.max(M200m, axis = 1)
			if q == 'N200m':
				mp = grp_sim.attrs['particle_mass']
				qmin *= mp
				qmax *= mp
			mask = mask & (M200m_max >= qmin) & (M200m_max <= qmax)
		
		else:
			mask = mask & (qd >= qmin) & (qd <= qmax)

	if np.count_nonzero(mask) == 0:
		print('WARNING: Returning empty ID list.')
		return np.array([])
	ids = np.array(grp_halos['id'])[mask, :]
	ids = np.max(ids, axis = 1)
	
	if log_level >= 1:
		print('sparta.findIDs: Returning list of %d IDs.' % (len(ids)))

	return ids, mask

###################################################################################################

def matchAnalyses(anl1, anl2):
	"""
	Find matches between the halo IDs of two sets of analyses and return the matched arrays.
	
	The order of halos in SPARTA arrays is, essentially, random because it depends on the processes
	to which halos are assigned, and thus the computing architecture. When comparing the results of
	two SPARTA runs, we must match the halo IDs. This function returns matched arrays of analyses,
	their size may or may not be equal to the size of the input arrays depending on whether all
	analyses have matches or not.
	
	Parameters
	-----------------------------------------------------------------------------------------------
	anl1: structured array
		A set of halo analyses as returned by the :func:`load` function. For example, if ``dic``
		was returned by the :func:`load` function, the profiles analysis (if it exists) can be
		found in ``dic['anl_prf']`` which is a structured array that can serve as input to this
		function.
	anl2: structured array
		See above, for a second SPARTA file.
		
	Returns
	-----------------------------------------------------------------------------------------------
	anl1_matched: structured array
		The analyses in ``anl1`` that have matches in ``anl2``.
	anl2_matched: structured array
		The analyses in ``anl2`` that have matches in ``anl1``, in the same order as the 
		``anl1_matched`` returned.
	"""

	anl1_cut = anl1[anl1['halo_id'] > 0]
	anl2_cut = anl2[anl2['halo_id'] > 0]
	
	anl1_sorted = anl1_cut[np.argsort(anl1_cut['halo_id'])]
	anl2_sorted = anl2_cut[np.argsort(anl2_cut['halo_id'])]

	matches = np.in1d(anl2_sorted['halo_id'], anl1_sorted['halo_id'])
	anl2_matched = anl2_sorted[matches]
	matches = np.in1d(anl1_sorted['halo_id'], anl2_matched['halo_id'])
	anl1_matched = anl1_sorted[matches]
			
	return anl1_matched, anl2_matched

###################################################################################################

def haloIsHost(status):
	"""
	Decide whether a halo was a host given a SPARTA status.
	
	This function refers to the ``status`` field output in the ``halos`` group in SPARTA output
	file, or the ``sparta_status`` field in MORIA output files. The ``final_status`` fields take 
	on different meanings and cannot be evaluated with this function.
	
	Parameters
	-----------------------------------------------------------------------------------------------
	status: array_like
		One integer or a numpy array of integers indicating a SPARTA status..
		
	Returns
	-----------------------------------------------------------------------------------------------
	is_host: array_like
		Boolean number or array with the same dimensions as ``status``, True if the status 
		indicates that a halo was a host.
	"""
	
	return ((status == HALO_STATUS_HOST) | (status == HALO_STATUS_GHOST_HOST))

###################################################################################################

def haloIsSub(status):
	"""
	Decide whether a halo was a subhalo given a SPARTA status.
	
	This function refers to the ``status`` field output in the ``halos`` group in SPARTA output
	file, or the ``sparta_status`` field in MORIA output files. The ``final_status`` fields take 
	on different meanings and cannot be evaluated with this function.
	
	Parameters
	-----------------------------------------------------------------------------------------------
	status: array_like
		One integer or a numpy array of integers indicating a SPARTA status..
		
	Returns
	-----------------------------------------------------------------------------------------------
	is_sub: array_like
		Boolean number or array with the same dimensions as ``status``, True if the status 
		indicates that a halo was a subhalo.
	"""
	
	return ((status == HALO_STATUS_SUB) 
			| (status == HALO_STATUS_BECOMING_SUB)
			| (status == HALO_STATUS_BECOMING_SUB_HOST)
			| (status == HALO_STATUS_BECOMING_HOST)
			| (status == HALO_STATUS_SWITCHED_HOST)
			| (status == HALO_STATUS_GHOST_SUB)
			| (status == HALO_STATUS_GHOST_SWITCHING))

###################################################################################################

def haloIsSubPermanently(status):
	"""
	Decide whether a halo was a subhalo for more than one snapshot given a SPARTA status.
	
	The distinction whether a halo is a subhalo for one or multiple snapshots may seem 
	insignificant, but SPARTA treats fly-through events where a halo is a sub for only one snapshot
	somewhat differently.
	
	This function refers to the ``status`` field output in the ``halos`` group in SPARTA output
	file, or the ``sparta_status`` field in MORIA output files. The ``final_status`` fields take 
	on different meanings and cannot be evaluated with this function.
	
	Parameters
	-----------------------------------------------------------------------------------------------
	status: array_like
		One integer or a numpy array of integers indicating a SPARTA status..
		
	Returns
	-----------------------------------------------------------------------------------------------
	is_sub_permanently: array_like
		Boolean number or array with the same dimensions as ``status``, True if the status 
		indicates that a halo was a subhalo for more than one snapshot.
	"""
	
	return ((status == HALO_STATUS_SUB) 
			| (status == HALO_STATUS_BECOMING_SUB)
			| (status == HALO_STATUS_BECOMING_HOST)
			| (status == HALO_STATUS_SWITCHED_HOST)
			| (status == HALO_STATUS_GHOST_SUB)
			| (status == HALO_STATUS_GHOST_SWITCHING))

###################################################################################################

def haloIsGhost(status):
	"""
	Decide whether a halo was a ghost given a SPARTA status.
	
	This function refers to the ``status`` field output in the ``halos`` group in SPARTA output
	file, or the ``sparta_status`` field in MORIA output files. The ``final_status`` fields take 
	on different meanings and cannot be evaluated with this function.
	
	Parameters
	-----------------------------------------------------------------------------------------------
	status: array_like
		One integer or a numpy array of integers indicating a SPARTA status..
		
	Returns
	-----------------------------------------------------------------------------------------------
	is_sub: array_like
		Boolean number or array with the same dimensions as ``status``, True if the status 
		indicates that a halo was a ghost.
	"""
	
	return ((status == HALO_STATUS_GHOST_HOST) 
			| (status == HALO_STATUS_GHOST_SUB)
			| (status == HALO_STATUS_GHOST_SWITCHING))

###################################################################################################
