# SPARTA (Subhalo and PARticle Trajectory Analysis)

Copyright (c) 2015-2022 Benedikt Diemer (diemer@umd.edu)

License: GNU GPLv3

Please see the [Online Documentation](https://bdiemer.bitbucket.io/sparta/) for details.
