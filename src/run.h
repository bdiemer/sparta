/*************************************************************************************************
 *
 * This unit implements the main work flow of sparta, namely a loop over all snapshots. The steps
 * that do not demand communication between processes are separated into the processSnapshot()
 * function.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _RUN_H_
#define _RUN_H_

#include "statistics.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void processSimulation(GlobalStatistics *gs, int restart);

#endif
