/*************************************************************************************************
 *
 * This unit implements the config system for sparta, namely reading the config file, checking
 * the halo catalog and snapshot directories, and computing cosmological quantities.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "global.h"
#include "halos/halo_definitions.h"

/*************************************************************************************************
 * CONFIG GLOBAL CONSTANTS
 *************************************************************************************************/

#define ERR_LEVEL_IGNORE 0
#define ERR_LEVEL_WARNING 1
#define ERR_LEVEL_ERROR 2

/*
 * This enum lists steps taken when initializing the config of results or analyses.
 */
enum
{
	CONFIG_INIT_STEP_DEFAULT,
	CONFIG_INIT_STEP_CHECK,
	CONFIG_INIT_STEP_CONFIG,
	CONFIG_INIT_STEP_PRINT
};

/*************************************************************************************************
 * CONFIG DEFAULTS
 *************************************************************************************************/

// Input (Halo catalogs)
#define DEFAULT_CAT_PATH ""
#define DEFAULT_CAT_TOLERANCE_A_SNAP 0.0001
#define DEFAULT_CAT_RELIABLE_N200M 20
#define DEFAULT_CAT_HALO_JUMP_TOL_PHYS 10.0
#define DEFAULT_CAT_HALO_JUMP_TOL_BOX 0.03

// Input (snapshots)
#define DEFAULT_SNAP_PATH ""
#define DEFAULT_SNAP_MAX_READ_PROCS 64
#define DEFAULT_SNAP_READ_ON_MAIN 1
#define DEFAULT_SNAP_MAX_WAITING_MESSAGES 64

// Input (simulation properties)
#define DEFAULT_SIM_FORCE_RES -1.0
#define DEFAULT_SIM_FORCE_RES_COMOVING 1

// Output
#define DEFAULT_OUTPUT_PATH "output"
#define DEFAULT_OUTPUT_FILE "sparta.hdf5"
#define DEFAULT_OUTPUT_COMPRESSION_LEVEL 1
#define DEFAULT_OUTPUT_RESTART_FILES 0
#define DEFAULT_OUTPUT_RESTART_EVERY 20
#define DEFAULT_OUTPUT_MIN_N200M 100

// Logging
#define DEFAULT_LOG_LEVEL 1
#define DEFAULT_LOG_LEVEL_MEMORY 2
#define DEFAULT_LOG_LEVEL_TIMING 0
#define DEFAULT_LOG_FLUSH 0

// Error levels
#define DEFAULT_ERR_LEVEL_CANNOT_FORK ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_FILE_COPY_FAILED ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_CAT_SNAP_MISMATCH ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_SKIPPING_SNAP ERR_LEVEL_IGNORE
#define DEFAULT_ERR_LEVEL_ROCKSTAR_FILENAME ERR_LEVEL_IGNORE
#define DEFAULT_ERR_LEVEL_INVALID_DOMAIN_BOX ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_DOMAIN_MINOR ERR_LEVEL_IGNORE
#define DEFAULT_ERR_LEVEL_HALO_REQ_NOT_FOUND ERR_LEVEL_ERROR
#define DEFAULT_ERR_LEVEL_DESC_NOT_FOUND ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_FOUND_UNEXPECTED ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_HALO_JUMP ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_MISSING_HALO_PTL ERR_LEVEL_IGNORE
#define DEFAULT_ERR_LEVEL_CAT_RADIUS_DIFF ERR_LEVEL_IGNORE
#define DEFAULT_ERR_LEVEL_ZERO_PTL ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_ZERO_PTL_PHANTOM ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_RADIUS_OUTSIDE_BOX ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_RADIUS_NOT_FOUND ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_RADIUS_BOUND_DIFF ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_SEARCH_RADIUS ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_EXC_HOST_ENDED ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_EXC_HALO_NOT_FOUND ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_MAX_EXCHANGE_SIZE ERR_LEVEL_WARNING
#define DEFAULT_ERR_LEVEL_LARGE_HALO ERR_LEVEL_WARNING

// Instructions
#define DEFAULT_LAST_SNAP (-1)
#define DEFAULT_LAST_SCALE_FACTOR (-1.0)

// Domain & Exchange
#define DEFAULT_LB_DO_LOAD_BALANCING 1
#define DEFAULT_LB_ADJUST_MIN_SNAP 5
#define DEFAULT_LB_SLABS_ADJUST_FACTOR 0.2
#define DEFAULT_LB_SLABS_ADJUST_MAX 0.01
#define DEFAULT_LB_SLABS_DRIFT_TOLERANCE 0.005
#define DEFAULT_EXC_MAX_MESSAGE_SIZE 500.0

// Other
#define DEFAULT_RANDOM_SEED 0
#define DEFAULT_TREE_POINTS_PER_LEAF 100
#define DEFAULT_POTENTIAL_POINTS_PER_LEAF 10
#define DEFAULT_POTENTIAL_ERR_TOL 0.1
#define DEFAULT_MEMORY_ALLOCATION_FACTOR 1.5
#define DEFAULT_MEMORY_DEALLOC_SAFETY_FACTOR 0.8
#define DEFAULT_MEMORY_MAX_HALO_SIZE 2000.0

// Halos
#define DEFAULT_HALO_MIN_RADIUS_MASS_PROFILE 0.01
#define DEFAULT_HALO_MAX_RADIUS_RATIO_CAT 10.0
#define DEFAULT_HALO_CORRECT_PHANTOMS 1
#define DEFAULT_GHOST_MIN_N200M 10
#define DEFAULT_GHOST_MIN_HOST_DISTANCE 0.05
#define DEFAULT_GHOST_MAX_HOST_CENTER_TIME 0.5

// Tracer Particles
#define DEFAULT_TCR_PTL_CREATE_RADIUS 2.0
#define DEFAULT_TCR_PTL_DELETE_RADIUS 3.0
#define DEFAULT_TCR_PTL_SUBTAG_RADIUS 2.0
#define DEFAULT_TCR_PTL_SUBTAG_INCLUSIVE 1
#define DEFAULT_TCR_PTL_SUBTAG_IFL_AGE_MIN 0.5
#define DEFAULT_TCR_PTL_SUBTAG_IFL_DIST_MIN 2.0
#define DEFAULT_TCR_PTL_SUBTAG_BOUND_RADIUS 0.5
#define DEFAULT_TCR_PTL_SUBTAG_BOUND_RATIO 1.0
#define DEFAULT_TCR_PTL_SUBTAG_HOST_MAX_AGE 0.5

/*************************************************************************************************
 * CONFIG KEYS, FIELDS ETC
 *************************************************************************************************/

/*
 * Prefixes for config parameters
 */
#define CONFIG_PREFIX_CATFIELDS "catkey_"
#define CONFIG_PREFIX_MINRS "output_minrs_"
#define CONFIG_PREFIX_DOALL "doall_"
#define CONFIG_PREFIX_DOHALO "dohalo_"

/*
 * Catalog fields. The need for those fields is given by the information demanded by the
 * HaloCatalogData struct. One should not need more than MAX_N_CATALOG_FIELDS to extract that from
 * a catalog.
 */
#define MAX_N_CATALOG_FIELDS 25
#define CAT_FIELD_LENGTH 40

/*
 * Halo catalog types
 */
#define CONFIG_CAT_TYPES \
ENUM(CAT_TYPE_ROCKSTAR, rockstar)\
ENUM(CAT_TYPE_SUBFIND, sublink)

#define ENUM(x, y) x,
enum
{
	CONFIG_CAT_TYPES/*space*/N_CONFIG_CAT_TYPES
};
#undef ENUM

/*
 * Simulation (snapshot) types
 */
#define CONFIG_SIM_TYPES \
ENUM(SIM_TYPE_LGADGET, lgadget)\
ENUM(SIM_TYPE_GADGET3, gadget3)\
ENUM(SIM_TYPE_AREPO, arepo)

#define ENUM(x, y) x,
enum
{
	CONFIG_SIM_TYPES/*space*/N_CONFIG_SIM_TYPES
};
#undef ENUM

/*
 * The maximum number of halo IDs which can be specified to save trajectories.
 */
#define MAX_HALO_INSTRUCTIONS 100

/*************************************************************************************************
 * CONFIG DATA STRUCTURES
 *************************************************************************************************/

/*
 * An instruction to do or not do a certain analysis on a certain halo.
 */
typedef struct
{
	long int id;
	int_least8_t tt;
	int_least8_t rs;
	int_least8_t do_tt_rs;
} HaloInstruction;

/*
 * Global settings struct. By definition, this struct contains only values that are computed
 * centrally on the main process at the very beginning of a program run, propagated to all other
 * processes, and NEVER changed thereafter.
 */
typedef struct
{
	/*
	 * --------------------------------------------------------------------------------------------
	 * User-defined config parameters
	 * --------------------------------------------------------------------------------------------
	 */

	// Input (Halo catalogs)
	char cat_path[300];
	int cat_type;
	int cat_fields_n;
	char cat_fields[MAX_N_CATALOG_FIELDS][CAT_FIELD_LENGTH];
	float cat_tolerance_a_snap;
	int cat_reliable_n200m;
	float cat_halo_jump_tol_phys;
	float cat_halo_jump_tol_box;

	// Input (Snapshots)
	char snap_path[300];
	int snap_sim_type;
	int snap_max_read_procs;
	int snap_read_on_main;
	int snap_max_waiting_messages;

	// Input (Simulation properties)
	float sim_force_res;
	int sim_force_res_comoving;

	// File output
	char output_path[300];
	char output_file[100];
	int output_compression_level;
	int output_restart_files;
	int output_restart_every;
	int output_min_n200m;
	int output_min_results[NTT][NRS];

	// Console output
	int log_level;
	int log_level_memory;
	int log_level_timing;
	int log_flush;

	// Error levels
	int err_level_cannot_fork;
	int err_level_file_copy_failed;
	int err_level_cat_snap_mismatch;
	int err_level_skipping_snap;
	int err_level_rockstar_filename;
	int err_level_invalid_domain_box;
	int err_level_domain_minor;
	int err_level_halo_req_not_found;
	int err_level_desc_not_found;
	int err_level_found_unexpected;
	int err_level_halo_jump;
	int err_level_missing_halo_ptl;
	int err_level_cat_radius_diff;
	int err_level_zero_ptl;
	int err_level_zero_ptl_phantom;
	int err_level_radius_outside_box;
	int err_level_radius_not_found;
	int err_level_radius_bound_diff;
	int err_level_search_radius;
	int err_level_exc_host_ended;
	int err_level_exc_halo_not_found;
	int err_level_max_exchange_size;
	int err_level_large_halo;

	// Instructions
	int last_snap;
	float last_scale_factor;
	int all_instructions[NTT][NRS];
	int n_halo_instructions;
	HaloInstruction halo_instructions[MAX_HALO_INSTRUCTIONS];

	// Domain & Exchange
	int lb_do_load_balancing;
	int lb_adjust_min_snap;
	float lb_slabs_adjust_factor;
	float lb_slabs_adjust_max;
	float lb_slabs_drift_tolerance;
	float exc_max_message_size;

	// Other
	int random_seed;
	int tree_points_per_leaf;
	int potential_points_per_leaf;
	float potential_err_tol;
	float memory_allocation_factor;
	float memory_dealloc_safety_factor;
	float memory_max_halo_size;

	// Halos
	float halo_min_radius_mass_profile;
	float halo_max_radius_ratio_cat;
	int halo_correct_phantoms;
	int ghost_min_n200m;
	float ghost_min_host_distance;
	float ghost_max_host_center_time;

	// Tracer Particles
	float tcr_ptl_create_radius;
	float tcr_ptl_delete_radius;
	float tcr_ptl_subtag_radius;
	int tcr_ptl_subtag_inclusive;
	float tcr_ptl_subtag_ifl_age_min;
	float tcr_ptl_subtag_ifl_dist_min;
	float tcr_ptl_subtag_bound_radius;
	float tcr_ptl_subtag_bound_ratio;
	float tcr_ptl_subtag_host_max_age;

	// Result oct
	int res_oct_max_norbit;

	// Analysis rsp
	float anl_rsp_redshifts[ANALYSIS_RSP_MAX_SNAPS];
	int anl_rsp_n_redshifts;
	char anl_rsp_defs_str[ANALYSIS_RSP_MAX_DEFINITIONS][DEFAULT_HALO_DEF_STRLEN];
	int anl_rsp_n_defs;
	float anl_rsp_min_rrm;
	float anl_rsp_max_rrm;
	float anl_rsp_min_smr;
	float anl_rsp_max_smr;
	int anl_rsp_demand_infall_rs;
	float anl_rsp_sigma_tdyn;
	float anl_rsp_min_weight;
	int anl_rsp_n_bootstrap;
	int anl_rsp_do_correction;

	// Analysis prf
	float anl_prf_redshifts[ANALYSIS_PROFILES_MAX_SNAPS];
	int anl_prf_n_redshifts;
	float anl_prf_rmin;
	float anl_prf_rmax;
	int anl_prf_do_subs;
	int anl_prf_do_ghosts;

	// Analysis hps
	float anl_hps_redshifts[ANALYSIS_HALOPROPS_MAX_SNAPS];
	int anl_hps_n_redshifts;
	char anl_hps_defs_str[ANALYSIS_HALOPROPS_MAX_DEFINITIONS][DEFAULT_HALO_DEF_STRLEN];
	int anl_hps_n_defs;
	float anl_hps_r_max_so_host;
	float anl_hps_r_max_so_sub;
	float anl_hps_r_max_orb_host;
	float anl_hps_r_max_orb_sub;
	float anl_hps_r_unbinding_host;
	float anl_hps_r_unbinding_sub;
	int anl_hps_iterative_unbinding;

	/*
	 * --------------------------------------------------------------------------------------------
	 * Derived config parameters
	 * --------------------------------------------------------------------------------------------
	 */
	float jump_threshold;
	float reliable_mass_R200m;
	float min_save_M200m;
	int mightRecreateTracers;
	float memory_dealloc_factor;
	float memory_alloc_fac_inv;

	/*
	 * Derived from catalogs
	 */
	int cat_n_chunks;
	int n_snaps;
	int snap_id[MAX_SNAPS];
	float snap_a[MAX_SNAPS];
	float snap_z[MAX_SNAPS];
	float snap_t[MAX_SNAPS];
	float snap_delta_t[MAX_SNAPS - 1];
	float snap_t_dyn[MAX_SNAPS];

	/*
	 * Derived from snapshot
	 */
	int n_files_per_snap;
	int n_particles;
	float box_size;
	float particle_mass;
	float Omega_m;
	float Omega_L;
	float h;

	/*
	 * Cosmology and related parameters. The v_conversion factor converts velocities in km/s to the
	 * internal SPARTA units of kpc/h/Gyr. The v200m conversion factor is used to convert radius to
	 * the circular velocity, v200m = f * R200m. This factor depends on redshift.
	 */
	Cosmology cosmo;
	float v_conversion;
	float snap_rho_200m[MAX_SNAPS];
	float snap_v200m_factor[MAX_SNAPS];
	float snap_aH[MAX_SNAPS];
	float snap_phys_conv[MAX_SNAPS];
	float snap_com_conv[MAX_SNAPS];

	/*
	 * The mass bins are the upper edges of the radial bins in units
	 * of R200m at whichever time they refer to, and are saved in both linear and log space in
	 * order to save computation time later. The log mass bins are a double array to accommodate
	 * GSL.
	 */
#if DO_MASS_PROFILE
	float r_bins_lin[N_MBINS];
	double r_bins_log[N_MBINS];
#endif

	/*
	 * Derived parameters for particular results or analyses
	 */
#if DO_ANALYSIS_RSP
	float anl_rsp_redshifts_actual[ANALYSIS_RSP_MAX_SNAPS];
	int anl_rsp_snaps[ANALYSIS_RSP_MAX_SNAPS];
	int anl_rsp_snap_idx[MAX_SNAPS];
	HaloDefinition anl_rsp_defs[ANALYSIS_RSP_MAX_DEFINITIONS];
	int anl_rsp_percentiles[ANALYSIS_RSP_MAX_DEFINITIONS];
	int anl_rsp_n_percentiles;
	int anl_rsp_do_bootstrap;
#endif

#if DO_ANALYSIS_PROFILES
	double anl_prf_r_bins_lin[ANALYSIS_PROFILES_N_BINS];
	double anl_prf_r_bins_log[ANALYSIS_PROFILES_N_BINS];
	float anl_prf_redshifts_actual[ANALYSIS_PROFILES_MAX_SNAPS];
	int anl_prf_snaps[ANALYSIS_PROFILES_MAX_SNAPS];
	int anl_prf_snap_idx[MAX_SNAPS];
#endif

#if DO_ANALYSIS_HALOPROPS
	float anl_hps_redshifts_actual[ANALYSIS_HALOPROPS_MAX_SNAPS];
	int anl_hps_snaps[ANALYSIS_HALOPROPS_MAX_SNAPS];
	int anl_hps_snap_idx[MAX_SNAPS];
	HaloDefinition anl_hps_defs[ANALYSIS_HALOPROPS_MAX_DEFINITIONS];
	int anl_hps_do_any_so_all;
	int anl_hps_do_any_so_bnd;
	int anl_hps_do_any_so_tcr;
	int anl_hps_do_any_so_orb;
	int anl_hps_do_m_orb_all;
#endif

	/*
	 * --------------------------------------------------------------------------------------------
	 * Convenient snapshot-based variables to avoid repetitive calculations
	 * --------------------------------------------------------------------------------------------
	 */
	float this_snap_force_res;
	float this_snap_a;
	float this_snap_z;
	float this_snap_t;
	float this_snap_rho_200m;
	float this_snap_phys_conv;
	float this_snap_com_conv;
} ConfigData;

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

extern ConfigData config;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void setDefaultConfig();
void processConfig(char *config_file, int file_only);
void printConfig();
void printCompiledConfig();

int lineToKeyValue(char *line, char **key, char **value);
void setSnapshotVariables(int snap_idx);
void getFilePathSnapChunk(const char *path, int snap_dir_idx, int chunk_idx, char *str);

#endif
