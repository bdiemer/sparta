/*************************************************************************************************
 *
 * This unit implements a generic rectangular sub-box of a simulation box which may span
 * across the periodic boundaries.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <mpi.h>

#include "geometry.h"
#include "config.h"
#include "memory.h"
#include "lb/domain.h"
#include "io/io_snaps.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * When creating boxes around the particles in snapshot files, sometimes weird solutions crop up,
 * including periodic boxes of length 0. When a box is below this fraction of the overall box
 * size, we are careful and accept the non-periodic solution instead.
 */
#define SNAP_BOX_PERIODIC_MIN 0.001

#define BOX_SMALL 1E-8
#define BOX_LARGE 1E8

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

#if DO_READ_PARTICLES
Box *snap_boxes;
#endif

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void computeSnapBox(char *filename, Box *box);
void computeSnapBoxes();
void readSnapBoxes();

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * The main function computing and reading the snap boxes. Note the importance of the barrier after
 * writing the files: without it, the first process might try to read a file that has not been
 * finished yet.
 */
#if DO_READ_PARTICLES
void getSnapBoxes()
{
	size_t size_snap_boxes;

	computeSnapBoxes();
	MPI_Barrier(MPI_COMM_WORLD);

	if (is_main_proc)
	{
		output(1, "[Main] Reading snapshot boxes...\n");
		readSnapBoxes();
		output(5, "[Main] Done reading snapshot boxes.\n");
		printLine(0);
	}

	size_snap_boxes = sizeof(Box) * config.n_snaps * config.n_files_per_snap;
	MPI_Bcast(snap_boxes, size_snap_boxes, MPI_BYTE, MAIN_PROC, MPI_COMM_WORLD);
}

/*
 * Each proc picks its own number first, and then increases this number by n_proc at every step.
 * We try to read the file first, if that fails we assume the file doesn't exist.
 */
void computeSnapBoxes()
{
	int i, j;
	char snap_file[300], box_file[300];
	FILE *f_box;
	Box box;

	i = proc;
	while (i < config.n_snaps)
	{
		getSnapBoxFilename(i, box_file);
		f_box = fopen(box_file, "r");
		if (f_box == NULL)
		{
			output(1, "[%4d] [BB %4d] Computing snapshot box, file %s.\n", proc, i, box_file);
			f_box = fopen(box_file, "w");
			if (f_box == NULL)
				error(__FFL__, "Could not write to file %s.\n", box_file);

			for (j = 0; j < config.n_files_per_snap; j++)
			{
				getSnapshotFilename(i, j, snap_file);
				output(4, "[%4d] [BB %4d] Calculating box %d/%d, snap file %s.\n", proc, i, j + 1,
						config.n_files_per_snap, snap_file);
				computeSnapBox(snap_file, &box);
				output(4, "[%4d] [BB %4d] Writing to file %s.\n", proc, i, box_file);
				if (fprintf(f_box,
						"%4d %1d %1d %1d %1d %12.7f %12.7f %12.7f %12.7f %12.7f %12.7f\n", j,
						box.ignore, box.periodic[0], box.periodic[1], box.periodic[2], box.min[0],
						box.max[0], box.min[1], box.max[1], box.min[2], box.max[2]) < 0)
				{
					error(__FFL__, "Could not write line to file %s.\n", box_file);
				}
			}
		}
		fclose(f_box);
		output(5, "[%4d] [BB %4d] Done with snapshot boxes, closed file %s.\n", proc, i, box_file);
		i += n_proc;
	}
}

/*
 * Compute a box around the particles in a snapshot file. In this case, we do NOT care about
 * domain information, i.e. what volume this proc covers.
 */
void computeSnapBox(char *filename, Box *box)
{
	Particle *particles;
	int n_particles, i, d;
	float L_box_np, L_box_p, min_np, max_np;

	resetBox(box);
	output(4, "[%4d] Reading particles from file %s.\n", proc, filename);
	readParticles(filename, &particles, &n_particles, 1);
	output(4, "[%4d] Read %d particles from file %s.\n", proc, n_particles, filename);

	for (d = 0; d < 3; d++)
	{
		/*
		 * Run 1: assume box is non-periodic in this dimension
		 */
		for (i = 0; i < n_particles; i++)
		{
			if (particles[i].x[d] < (*box).min[d])
				(*box).min[d] = particles[i].x[d];
			if (particles[i].x[d] > (*box).max[d])
				(*box).max[d] = particles[i].x[d];
		}

		/*
		 * Check for periodicity: if the size of the box is larger than half the
		 * simulation box, it presumably crosses a boundary.
		 */
		L_box_np = (*box).max[d] - (*box).min[d];
		if (L_box_np > config.box_size * 0.5)
		{
			min_np = (*box).min[d];
			max_np = (*box).max[d];
			(*box).min[d] = BOX_LARGE;
			(*box).max[d] = BOX_SMALL;

			for (i = 0; i < n_particles; i++)
			{
				if (particles[i].x[d] >= config.box_size * 0.5)
				{
					if (particles[i].x[d] < (*box).min[d])
						(*box).min[d] = particles[i].x[d];
				} else
				{
					if (particles[i].x[d] > (*box).max[d])
						(*box).max[d] = particles[i].x[d];
				}
			}

			/*
			 * If the box is still larger than half the sim box, we have a problem since
			 * there is no unique solution. We can only accept the less bad of the two solutions.
			 * If the periodic box is smaller than half the box size, we accept the periodic
			 * solution.
			 *
			 * Note that the warnings below can be frequent, for example if there is only one
			 * core which needs to handle the whole box.
			 */
			L_box_p = (*box).max[d] - (*box).min[d] + config.box_size;
			if (L_box_p > config.box_size * 0.5)
			{
				if (L_box_p < config.box_size * SNAP_BOX_PERIODIC_MIN)
				{
					warningOrError(__FFL__, config.err_level_domain_minor,
							"Could not find unique box in dimension %d (non-periodic minmax [%.2e, %.2e], periodic minmax [%.2e, %.2e]). Too small, accepting non-periodic solution.\n",
							d, min_np, max_np, (*box).min[d], (*box).max[d]);
					(*box).min[d] = min_np;
					(*box).max[d] = max_np;
					(*box).periodic[d] = 0;
				} else if (L_box_p > L_box_np)
				{
					warningOrError(__FFL__, config.err_level_domain_minor,
							"Could not find unique box in dimension %d (non-periodic minmax = [%.2e, %.2e], periodic minmax = [%.2e, %.2e]). Accepting non-periodic solution.\n",
							d, min_np, max_np, (*box).min[d], (*box).max[d]);
					(*box).min[d] = min_np;
					(*box).max[d] = max_np;
					(*box).periodic[d] = 0;
				} else
				{
					warningOrError(__FFL__, config.err_level_domain_minor,
							"Could not find unique box in dimension %d (non-periodic minmax = [%.2e, %.2e], periodic minmax = [%.2e, %.2e]). Accepting periodic solution.\n",
							d, min_np, max_np, (*box).min[d], (*box).max[d]);
					(*box).periodic[d] = 1;
				}
			} else
			{
				(*box).periodic[d] = 1;
			}
		}

#if CAREFUL
		assert((*box).min[d] >= 0.0);
		assert((*box).max[d] >= 0.0);
		assert((*box).min[d] <= config.box_size);
		assert((*box).max[d] <= config.box_size);
		if ((*box).periodic[d])
		{
			if ((*box).max[d] > (*box).min[d])
				error(__FFL__, "Dimension %d, found periodic box with min %.5f, max %.5f.\n", d,
						(*box).min[d], (*box).max[d]);
		} else
		{
			if ((*box).min[d] > (*box).max[d])
				error(__FFL__, "Dimension %d, found non-periodic box with min %.5f, max %.5f.\n", d,
						(*box).min[d], (*box).max[d]);
		}
#endif
	}

	memFree(__FFL__, MID_PARTICLES, particles, n_particles * sizeof(Particle));
}

void readSnapBoxes()
{
	int i, j, idx;
	char box_file[300];
	FILE *f_box;

	for (i = 0; i < config.n_snaps; i++)
	{
		getSnapBoxFilename(i, box_file);
		f_box = fopen(box_file, "r");
		if (f_box == NULL)
			error(__FFL__, "[Main] Could not open file %s.\n", box_file);

		for (j = 0; j < config.n_files_per_snap; j++)
		{
			idx = i * config.n_files_per_snap + j;
			if (fscanf(f_box, "%d %d %d %d %d %f %f %f %f %f %f\n", &j, &(snap_boxes[idx].ignore),
					&(snap_boxes[idx].periodic[0]), &(snap_boxes[idx].periodic[1]),
					&(snap_boxes[idx].periodic[2]), &(snap_boxes[idx].min[0]),
					&(snap_boxes[idx].max[0]), &(snap_boxes[idx].min[1]), &(snap_boxes[idx].max[1]),
					&(snap_boxes[idx].min[2]), &(snap_boxes[idx].max[2])) != 11)
			{
				error(__FFL__, "Failed to read snapshot box file %s.\n", box_file);
			}
		}

		fclose(f_box);
	}
}

void getSnapBoxFilename(int snap_idx, char *str)
{
	int snap_id = config.snap_id[snap_idx];
	sprintf(str, "%s/snapboxes/box_%04d.txt", config.output_path, snap_id);
}

/*
 * Note that there is a special case where a snapshot box is set to ignore. In that case, this
 * function always returns no match.
 */
void intersectingSnapFiles(int snap_idx, Box *box, int **file_intersect, int *n_intersect)
{
	int i;
	Box *snap_box;

	*n_intersect = 0;
	*file_intersect = (int*) memAlloc(__FFL__, MID_GEOMETRY, sizeof(int) * config.n_files_per_snap);
	for (i = 0; i < config.n_files_per_snap; i++)
	{
		snap_box = &(snap_boxes[snap_idx * config.n_files_per_snap + i]);
		(*file_intersect)[i] = (!(snap_box->ignore) && !(box->ignore)
				&& boxesIntersect(box, snap_box));

		if (snap_box->ignore)
			output(4, "[%4d] Ignoring snapshot %d, file %d due to corruption.\n", proc, snap_idx,
					i);

		/*
		 * If we are checking for particles around a certain position, we need to consider all files
		 * on the main proc where this check is performed.
		 */
#if CHECK_POSITION
		if (is_main_proc)
		(*file_intersect)[i] = 1;
#endif

		if ((*file_intersect)[i])
			(*n_intersect)++;
	}
}
#endif

void resetBox(Box *box)
{
	int d;

	box->ignore = 0;
	for (d = 0; d < 3; d++)
	{
		(*box).periodic[d] = 0;
		(*box).min[d] = BOX_LARGE;
		(*box).max[d] = BOX_SMALL;
	}
}

void setEmptyBox(Box *box)
{
	int d;

	box->ignore = 1;
	for (d = 0; d < 3; d++)
	{
		(*box).periodic[d] = 0;
		(*box).min[d] = 0.0;
		(*box).max[d] = 0.0;
	}
}

float boxVolume(Box *box)
{
	int d;
	float V, L;

	V = 1.0;
	for (d = 0; d < 3; d++)
	{
		L = box->max[d] - box->min[d];
		if (box->periodic[d])
			L += config.box_size;
		V *= L;
	}

	return V;
}

int inBox(float x[3], Box *box)
{
	int d, in_box, in_dim;

	in_box = 1;
	for (d = 0; d < 3; ++d)
	{
		if (box->periodic[d])
		{
			in_dim = ((x[d] >= box->min[d]) || (x[d] <= box->max[d]));
		} else
		{
			in_dim = ((x[d] >= box->min[d]) && (x[d] <= box->max[d]));
		}
		if (!in_dim)
		{
			in_box = 0;
			break;
		}
	}

	return in_box;
}

int sphereInBox(float x[3], float r, Box *box)
{
	int i, d, in_box, in_dim;
	float xx;

	in_box = 1;
	for (d = 0; d < 3; ++d)
	{
		for (i = 0; i < 2; i++)
		{
			if (i == 0)
			{
				xx = x[d] - r;
			} else
			{
				xx = x[d] + r;
			}
			xx = correctPeriodic(xx, config.box_size);

			if (box->periodic[d])
			{
				in_dim = ((xx >= box->min[d]) || (xx <= box->max[d]));
			} else
			{
				in_dim = ((xx >= box->min[d]) && (xx <= box->max[d]));
			}

			if (!in_dim)
			{
				in_box = 0;
				break;
			}
		}
		if (!in_box)
			break;
	}

	return in_box;
}

int boxesIntersect(Box *box1, Box *box2)
{
	int d, intersect, p1, p2;
	float min1, min2, max1, max2;

	intersect = 0;
	for (d = 0; d < 3; ++d)
	{
		min1 = box1->min[d];
		min2 = box2->min[d];
		max1 = box1->max[d];
		max2 = box2->max[d];
		p1 = box1->periodic[d];
		p2 = box2->periodic[d];

		if (!p1 && !p2)
		{
			// no periodic boxes: either the min or max of a box have to lie in the other box
			if (!((min1 <= min2 && min2 <= max1) || (min1 <= max2 && max2 <= max1)
					|| (min2 <= min1 && min1 <= max2) || (min2 <= max1 && max1 <= max2)))
				break;
		} else if (!p1 && p2)
		{
			// box 2 is periodic
			if (!((min2 <= max1) || (min1 <= max2)))
				break;
		} else if (p1 && !p2)
		{
			// box 1 is periodic
			if (!((min1 <= max2) || (min2 <= max1)))
				break;
		} else if (p1 && p2)
		{
			// both boxes are periodic
			max1 += config.box_size;
			max2 += config.box_size;
			if (!((min1 <= min2 && min2 <= max1) || (min1 <= max2 && max2 <= max1)
					|| (min2 <= min1 && min1 <= max2) || (min2 <= max1 && max1 <= max2)))
				break;
		} else
		{
			error(__FFL__, "Internal error.\n");
		}

		/*
		 * If we have gotten to this point, all dimensions intersect
		 */
		if (d == 2)
			intersect = 1;
	}

	return intersect;
}

/*
 * Correct periodic boundaries, assuming that the position is within [-L, 2L].
 */
float correctPeriodic(float x, float L)
{
	float xnew = x;

	if (xnew > L)
		xnew -= L;
	if (xnew < 0.0)
		xnew += L;

#if CAREFUL
	assert(xnew >= 0.0);
	assert(xnew <= L);
#endif

	return xnew;
}

/*
 * Compute a vector distance from x1 to x2 including periodic boundaries. Note that this is not
 * the same as correcting a periodic position: here, the resulting vector can be negative.
 */
void periodicDistanceVector(float *x1, float *x2, float *vec_out)
{
	int d;

	for (d = 0; d < 3; d++)
	{
		vec_out[d] = x2[d] - x1[d];
		if (fabs(vec_out[d]) > 0.5 * config.box_size)
		{
			if (vec_out[d] > 0.0)
				vec_out[d] -= config.box_size;
			else
				vec_out[d] += config.box_size;
		}
	}
}

/*
 * Returns the shortest possible distance between two points, assuming that they might have crossed
 * periodic boundaries.
 */
float periodicDistance(float *x1, float *x2)
{
	int d;
	float dx, dist;

	dist = 0.0;
	for (d = 0; d < 3; d++)
	{
		dx = x1[d] - x2[d];
		if (fabs(dx) > config.box_size * 0.5)
			dx = config.box_size - fabs(dx);
		dist += dx * dx;
	}
	dist = sqrt(dist);

	return dist;
}

/*
 * A non-periodic distance calculation for arbitrary vectors.
 */
float distance(float *x1, float *x2)
{
	int d;
	float dx, dist;

	dist = 0.0;
	for (d = 0; d < 3; d++)
	{
		dx = x1[d] - x2[d];
		dist += dx * dx;
	}
	dist = sqrt(dist);

	return dist;
}

/*
 * Normalize a vector to unity.
 */
void unitVector(float *x_in, float *x_out)
{
	int d;
	float l;

	l = 0.0;
	for (d = 0; d < 3; d++)
		l += x_in[d] * x_in[d];
	l = 1.0 / sqrt(l);
	for (d = 0; d < 3; d++)
		x_out[d] = l * x_in[d];
}

float dotProduct(float *x, float *y)
{
	int d;
	float dp;

	dp = 0.0;
	for (d = 0; d < 3; d++)
		dp += x[d] * y[d];

	return dp;
}
