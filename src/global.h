/*************************************************************************************************
 *
 * This unit implements the system of compiler flags used in SPARTA, as well as constants and
 * functions such as output, errors and warnings.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdint.h>
#include <mpi.h>

#include <sparta.h>

/*************************************************************************************************
 * USEFUL MACROS
 *************************************************************************************************/

/*
 * Using these macros, one can print out a pre-processor variable using a pragma, e.g.
 * #pragma message(PRINT_MACRO(__MYVAR__))
 */
#define STR(x) #x
#define PRINT_VALUE(x) STR(x)
#define PRINT_MACRO(var) #var "=" PRINT_VALUE(var)

/*
 * For all error messages in SPARTA, the file, function or line must be given. This can easily be
 * done with this macro.
 */
#define __FFL__ __FILE__,__FUNCTION__,__LINE__

/*************************************************************************************************
 * SET DO-SWITCHES THAT DEPEND ON CHOSEN TRACERS, RESULTS, ANALYSES
 *************************************************************************************************/

/*
 * Analyses
 */
#define DO_ANALYSIS_RSP (OUTPUT_ANALYSIS_RSP)
#define DO_ANALYSIS_PROFILES (OUTPUT_ANALYSIS_PROFILES)
#define DO_ANALYSIS_HALOPROPS (OUTPUT_ANALYSIS_HALOPROPS)

/*
 * Analysis fields
 */
#define DO_ANALYSIS_PROFILES_ALL (DO_ANALYSIS_PROFILES && OUTPUT_ANALYSIS_PROFILES_ALL)
#define DO_ANALYSIS_PROFILES_1HALO (DO_ANALYSIS_PROFILES && OUTPUT_ANALYSIS_PROFILES_1HALO)

#define DO_ANALYSIS_HALOPROPS_RM (DO_ANALYSIS_HALOPROPS && OUTPUT_ANALYSIS_HALOPROPS_RM)
#define DO_ANALYSIS_HALOPROPS_ORBITING (DO_ANALYSIS_HALOPROPS && OUTPUT_ANALYSIS_HALOPROPS_ORBITING)

/*
 * Results. Note that the splashback analysis needs infall results to weed out splashbacks
 * from subhalo particles. The same goes for any analysis that uses subhalo tracking. We implement
 * those conditions in a second run below.
 */
#define DO_RESULT_ORBITCOUNT (OUTPUT_RESULT_ORBITCOUNT || (DO_ANALYSIS_PROFILES && DO_ANALYSIS_PROFILES_1HALO) || DO_ANALYSIS_HALOPROPS_ORBITING)
#define DO_RESULT_INFALL_ (OUTPUT_RESULT_INFALL || DO_ANALYSIS_RSP)
#define DO_RESULT_SPLASHBACK (OUTPUT_RESULT_SPLASHBACK || DO_ANALYSIS_RSP)
#define DO_RESULT_TRAJECTORY (OUTPUT_RESULT_TRAJECTORY)

/*
 * Result fields.
 */
#define DO_RESULT_INFALL_SMR_ ((DO_RESULT_INFALL_ && OUTPUT_RESULT_INFALL_SMR) || DO_ANALYSIS_RSP)
#define DO_RESULT_INFALL_VRV200 (DO_RESULT_INFALL_ && OUTPUT_RESULT_INFALL_VRV200)
#define DO_RESULT_INFALL_VTV200 (DO_RESULT_INFALL_ && OUTPUT_RESULT_INFALL_VTV200)
#define DO_RESULT_INFALL_X_ (DO_RESULT_INFALL_ && OUTPUT_RESULT_INFALL_X)

#define DO_RESULT_SPLASHBACK_MSP ((DO_RESULT_SPLASHBACK && OUTPUT_RESULT_SPLASHBACK_MSP) || DO_ANALYSIS_RSP)
#define DO_RESULT_SPLASHBACK_RRM ((DO_RESULT_SPLASHBACK && OUTPUT_RESULT_SPLASHBACK_RRM) || DO_ANALYSIS_RSP)
#define DO_RESULT_SPLASHBACK_POS (DO_RESULT_SPLASHBACK && OUTPUT_RESULT_SPLASHBACK_POS)

#define DO_RESULT_TRAJECTORY_R (DO_RESULT_TRAJECTORY && OUTPUT_RESULT_TRAJECTORY_R)
#define DO_RESULT_TRAJECTORY_VR (DO_RESULT_TRAJECTORY && OUTPUT_RESULT_TRAJECTORY_VR)
#define DO_RESULT_TRAJECTORY_VT (DO_RESULT_TRAJECTORY && OUTPUT_RESULT_TRAJECTORY_VT)
#define DO_RESULT_TRAJECTORY_X (DO_RESULT_TRAJECTORY && OUTPUT_RESULT_TRAJECTORY_X)
#define DO_RESULT_TRAJECTORY_V (DO_RESULT_TRAJECTORY && OUTPUT_RESULT_TRAJECTORY_V)

/*
 * Ghosts, subhalos, tracer actions
 *
 * DO_SUBSUBS                  If on, allow subhalos to have subhalos themselves. This has some
 *                             profound implications for sub-pointers, exchange etc.
 * DO_SUBHALO_TRACKING         If on, do not delete all tracers from subhalos but instead keep
 *                             (some of) them alive while the halo is a subhalo. This increases
 *                             the number of tracers, but allows following the mass in the subhalo.
 * DO_SUBHALO_TAGGING          If on, identify the particles in subhalos and add a tag to the
 *                             corresponding tracers in the subhalo's host.
 */
#define DO_GHOSTS OUTPUT_GHOSTS
#define DO_SUBSUBS DO_GHOSTS

#define DO_TRACER_PARTICLES_ (OUTPUT_TRACER_PARTICLES || DO_ANALYSIS_RSP || DO_ANALYSIS_PROFILES || (DO_ANALYSIS_HALOPROPS && (DO_ANALYSIS_HALOPROPS_RM || OUTPUT_ANALYSIS_HALOPROPS_ORBITING)) || DO_GHOSTS)
#define DO_TRACER_SUBHALOS (OUTPUT_TRACER_SUBHALOS)
#define DO_SUBHALO_TRACKING (DO_ANALYSIS_HALOPROPS_RM || DO_GHOSTS)
#define DO_SUBHALO_TAGGING ((DO_RESULT_INFALL_SMR_ || DO_SUBHALO_TRACKING) && DO_TRACER_PARTICLES_)

#define DO_SUBHALO_TAGGING_IFL_AGE (DO_SUBHALO_TAGGING && SUBTAG_METHOD_IFL_AGE)
#define DO_SUBHALO_TAGGING_IFL_DISTANCE (DO_SUBHALO_TAGGING && SUBTAG_METHOD_IFL_DISTANCE)
#define DO_SUBHALO_TAGGING_BOUND (DO_SUBHALO_TAGGING && SUBTAG_METHOD_BOUND)

/*
 * One-loop dependencies. If we are using infall events for subhalo tagging, then we need infall
 * events.
 */
#define DO_TRACER_PARTICLES (DO_TRACER_PARTICLES_ || DO_SUBHALO_TAGGING || DO_SUBHALO_TRACKING)
#define DO_RESULT_INFALL (DO_RESULT_INFALL_ || DO_SUBHALO_TAGGING)
#define DO_RESULT_INFALL_SMR (DO_RESULT_INFALL_SMR_ || DO_SUBHALO_TAGGING)
#define DO_RESULT_INFALL_X (DO_RESULT_INFALL_X_ || DO_SUBHALO_TAGGING_IFL_DISTANCE)

/*
 * Tracer fields
 *
 * Compute switches for routines that may or may not need to be executed, or memory fields that
 * may or may not exist.
 *
 * DO_TRACER_POS               In addition to tracer radius, compute theta and phi (i.e., the
 *                             tracer's position in 3D). Note that this is stored for STCL_TCR
 *                             snapshots.
 * DO_TRACER_INITIAL_ANGLE     Keep the initial angular coordinates when the tracer was first
 *                             tracked. This angle depends on the radius where tracers are created.
 * DO_TRACER_VT                Compute the tracer's tangential velocity.
 * DO_TRACER_X / DO_TRACER_V   Keep the current position/velocity vectors in a tracer (only for the
 *                             current snapshot).
 * DO_TRACER_SMR               Compute the sub-to-host mass ratio of tracers that fell in with a
 *                             subhalo. Needed for infall events.
 */
#define DO_TRACER_POS (DO_RESULT_SPLASHBACK_POS || DO_RESULT_INFALL_X || DO_RESULT_ORBITCOUNT)
#define DO_TRACER_INITIAL_ANGLE (DO_RESULT_ORBITCOUNT)
#define DO_TRACER_VT (DO_RESULT_TRAJECTORY_VT || DO_RESULT_INFALL_VTV200)
#define DO_TRACER_X (DO_RESULT_TRAJECTORY_X)
#define DO_TRACER_V (DO_RESULT_TRAJECTORY_V)
#define DO_TRACER_SMR (DO_RESULT_INFALL_SMR)

/*
 * DO_MASS_PROFILE             Keep an array of mass vs radius for the last STCL_TCR snapshots.
 *                             This is rather heavy-weight, only turn on if needed.
 * DO_READ_PARTICLES           Read particles from snapshot files. If this is off, only catalog
 *                             files are read (a rare use case).
 */
#define DO_MASS_PROFILE (DO_TRACER_PARTICLES || (DO_TRACER_SUBHALOS && DO_RESULT_SPLASHBACK_MSP))
#define DO_READ_PARTICLES (DO_TRACER_PARTICLES || DO_MASS_PROFILE || DO_ANALYSIS_PROFILES || DO_ANALYSIS_HALOPROPS)

/*
 * Summary fields that indicate whether any of a given species is active.
 */
#define DO_TRACER_ANY (DO_TRACER_PARTICLES || DO_TRACER_SUBHALOS)
#define DO_RESULT_ANY (DO_RESULT_INFALL || DO_RESULT_SPLASHBACK || DO_RESULT_TRAJECTORY || DO_RESULT_ORBITCOUNT)
#define DO_ANALYSIS_ANY (DO_ANALYSIS_RSP || DO_ANALYSIS_PROFILES || DO_ANALYSIS_HALOPROPS)

/*************************************************************************************************
 * SET SWITCHES THAT DEPEND ON CHOSEN TRACERS, RESULTS, ANALYSES
 *************************************************************************************************/

#define DOMAIN_DECOMP_SLABS (DOMAIN_DECOMPOSITION == DOMAIN_DECOMPOSITION_SLABS)
#define DOMAIN_DECOMP_SFC (DOMAIN_DECOMPOSITION == DOMAIN_DECOMPOSITION_SFC)

/*
 * Stencils, meaning the number of snapshots kept in memory. These stencils should never be less
 * than one because that causes certain routines such as interpolators to crash.
 *
 * The tracer stencil is of huge importance for the memory consumption because there can be many
 * tracers and because a number of variables are kept for STCL_TCR snapshots (e.g., radius,
 * velocity etc).
 */
#define STCL_TCR 2

#if DO_RESULT_ORBITCOUNT
#undef STCL_TCR
#define STCL_TCR 3
#endif

#if DO_RESULT_SPLASHBACK
#undef STCL_TCR
#define STCL_TCR 4
#endif

/*
 * The halo position does not need to be kept in memory for too long, unless we are using it to tag
 * subhalo particles; in that case, we need the full history. If we are outputting it, we also need
 * to preseve the full history.
 */
#define STCL_HALO_X 2

#if (DO_SUBHALO_TAGGING_IFL_DISTANCE || OUTPUT_HALO_X)
#undef STCL_HALO_X
#define STCL_HALO_X MAX_SNAPS
#endif

/*
 * The velocity is not used for anything except possibly output
 */
#define STCL_HALO_V 2

#if OUTPUT_HALO_V
#undef STCL_HALO_V
#define STCL_HALO_V MAX_SNAPS
#endif

/*************************************************************************************************
 * ARRAY INDICES
 *************************************************************************************************/

/*
 * Compute the number and indices of tracers used.
 */
enum
{
#if DO_TRACER_PARTICLES
	PTL,
#endif
#if DO_TRACER_SUBHALOS
	SHO,
#endif
	NTT
};

/*
 * Compute the number and indices of result types used.
 */
enum
{
#if DO_RESULT_INFALL
	IFL,
#endif
#if DO_RESULT_SPLASHBACK
	SBK,
#endif
#if DO_RESULT_TRAJECTORY
	TJY,
#endif
#if DO_RESULT_ORBITCOUNT
	OCT,
#endif
	NRS
};

/*
 * Compute the number and indices of analyses used.
 */
enum
{
#if DO_ANALYSIS_RSP
	RSP,
#endif
#if DO_ANALYSIS_PROFILES
	PRF,
#endif
#if DO_ANALYSIS_HALOPROPS
	HPS,
#endif
	NAL
};

/*************************************************************************************************
 * ARRAY SIZES
 *************************************************************************************************/

/*
 * The total number of dynamic arrays in a halo
 */
#define N_HALO_DA (NTT * (2 + NRS) + NAL)

/*
 * The maximum dimensionality of an output dataset. This number needs to be defined globally
 * because output fields can be set by various modules.
 */
#define N_DSET_DIM_MAX 3

/*************************************************************************************************
 * DEBUG SWITCHES
 *************************************************************************************************/

/*
 * Halo and tracer debug output can be turned on if necessary. Note that any unit that uses the
 * debug functions needs to include the header file where the function is defined to avoid
 * compiler warnings, that is, halos/halo.h and/or tracers/tracers.h.
 */
#if DEBUG_HALO
#define debugHalo(args...) printDebugHalo(args)
#define debugHaloCatalogData(args...) printDebugHaloCatalogData(args)
#else
#define debugHalo(args...) {}
#define debugHaloCatalogData(args...) {}
#endif

#if DEBUG_TRACER
#define debugTracer(args...) printDebugTracer(args)
#define debugTracerByID(args...) printDebugTracerByID(args)
#else
#define debugTracer(args...) {}
#define debugTracerByID(args...) {}
#endif

/*************************************************************************************************
 * INTERNAL CONSTANTS
 *************************************************************************************************/

/*
 * MPI constants.
 *
 * The MPI communicator is initially set to this value which indicates that MPI has not been
 * initialized. Functions can be made safe for applications that do not use MPI (such as smaller
 * tools) by checking the communicator.
 */
#define MAIN_PROC 0
#define COMM_NO_MPI 0

/*
 * Values set to indicate invalid fields. A few notes on individual values:
 *
 * INVALID_X:     Could be -1 since comoving coordinates in the box cannot be negative. However,
 *                the symbol "x" is also used for coordinates within a halo which can be negative.
 *                Thus, we choose a very large negative value.
 * INVALID_ACCRATE: The mass accretion rate can be negative.
 */
#define INVALID_NEGATIVE -1E20
#define INVALID_INDICATOR -1E19

#define INVALID_IDX -1
#define INVALID_ID ((long int) -1)
#define INVALID_PROC -1
#define INVALID_R -1.0
#define INVALID_M -1.0
#define INVALID_NU -1.0
#define INVALID_X INVALID_NEGATIVE
#define INVALID_V INVALID_NEGATIVE
#define INVALID_T -1.0
#define INVALID_ANGLE INVALID_NEGATIVE
#define INVALID_SMR -1.0
#define INVALID_ACCRATE INVALID_NEGATIVE

#define LARGE_ID LONG_MAX
#define LOG10_NO_MASS (-10.0)

/*
 * The ghost ID offsets are defined here rather than in the ghosts unit as they are also used in
 * other contexts. The ID of a ghost is always larger than GHOST_ID_OFFSET, and encodes the
 * current snapshot as a multiple of GHOST_ID_SNAP_OFFSET.
 */
#define GHOST_ID_OFFSET 1E16
#define GHOST_ID_SNAP_OFFSET 1E12

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct
{
	MPI_Datatype box;
	MPI_Datatype particle;
	MPI_Datatype halo;
	MPI_Datatype sub;
	MPI_Datatype tcr;
	MPI_Datatype iid;
	MPI_Datatype rs[NRS];
	MPI_Datatype al[NAL];
} MPITypes;

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

/*
 * MPI variables; these variables are used very often and are thus global. They are initialized
 * even after a restart.
 */
extern MPI_Comm comm;
extern int proc;
extern int n_proc;
extern int is_main_proc;
extern MPITypes mpiTypes;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void output(int level, const char *format, ...);
void printLine(int level);

void warning(const char *format, ...);
void error(const char *file, const char *func, int line, const char *format, ...);
void warningOrError(const char *file, const char *func, int line, int error_level,
		const char *format, ...);

void abortCode();
void delay(int milliseconds);

void allocateGlobalMemory();
void freeGlobalMemory();

#endif
