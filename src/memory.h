/*************************************************************************************************
 *
 * This module contains the memory management system for SPARTA.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _MEMORY_H_
#define _MEMORY_H_

#include <stdint.h>

/*************************************************************************************************
 * MEMORY FIELDS
 *************************************************************************************************/

/*
 * Each memory allocation in SPARTA can be tracked. The IDs below correspond to each memory field.
 *
 * In some rare cases, memory is allocated or deallocated by system functions and we cannot trace
 * it. Such allocations are filed under the MID_IGNORE memory ID.
 *
 * The MID_DA blocks correspond to dynamic arrays which account for the bulk of the memory usage.
 * Those blocks, as well as a few others, persist across snapshots, but all blocks after
 * MID_MUSTBEZERO are expected to be zero after a snapshot.
 */
#define MID_NAMES \
ENUM(MID_IGNORE, Ignored memory fields)\
ENUM(MID_DA_HALO, Halo objects)\
ENUM(MID_DA_CD, Catalog data)\
ENUM(MID_DA_TREQ, Catalog request data)\
ENUM(MID_DA_TRACER, Tracer objects)\
ENUM(MID_DA_RESIFL, Infall result objects)\
ENUM(MID_DA_RESSBK, Splashback result objects)\
ENUM(MID_DA_RESTJY, Trajectory result objects)\
ENUM(MID_DA_RESOCT, Orbitcount result objects)\
ENUM(MID_DA_ALRSP, Rsp analysis  objects)\
ENUM(MID_DA_ALPRF, Profile analysis objects)\
ENUM(MID_DA_ALHPS, Haloprops analysis objects)\
ENUM(MID_DA_INT, Integer lists)\
ENUM(MID_DA_SUBPOINTER, Subhalo lists)\
ENUM(MID_DA_ID, Ignored ID lists)\
ENUM(MID_SFCINDEXLOOKUP, SFC index lookup)\
ENUM(MID_BOXES, Snapshot file boundaries)\
ENUM(MID_OUTPUTFILES, Output file objects)\
ENUM(MID_MORIA_CONFIG, MORIA: Config)\
ENUM(MID_MORIA_SPARTA, MORIA: SPARTA data)\
ENUM(MID_MORIA_TREE, MORIA: Merger tree logic)\
ENUM(MID_MUSTBEZERO, Begin of intra-snap blocks)\
ENUM(MID_HDF5BUFFER, HDF5 read/write buffer)\
ENUM(MID_CATREADING, Halo catalog read/exchange)\
ENUM(MID_SNAPREADBUF, Snapshot read buffer)\
ENUM(MID_PARTICLES, Raw particle data)\
ENUM(MID_PARTICLEMESSAGES, Particle exchange packets)\
ENUM(MID_OUTPUTBUF, Output buffers)\
ENUM(MID_EXCHANGEBUF, Exchange buffers)\
ENUM(MID_TREE, Tree calculations)\
ENUM(MID_TREEPOT, Tree potential)\
ENUM(MID_GEOMETRY, Boundaries and geometry)\
ENUM(MID_DOMAINDECOMP, Domain decomposition)\
ENUM(MID_HALOLOGIC, Halo/subhalo handling)\
ENUM(MID_GHOSTS, Ghost properties)\
ENUM(MID_TRACERHANDLING, Tracer handling)\
ENUM(MID_TCRPTL, Tracer: particles)\
ENUM(MID_RESSBK, Result: splashback)\
ENUM(MID_RESTJY, Result: trajectory)\
ENUM(MID_ANLRSP, Analysis: Rsp)\
ENUM(MID_ALPRF, Analysis: profiles)\
ENUM(MID_ANLHPS, Analysis: Halo properties)\
ENUM(MID_MORIA_TMP, MORIA: General temp memory)\
ENUM(MID_MORIA_CAT, MORIA: Catalog data)\
ENUM(MID_MORIA_HALOS, MORIA: Halo data)\
ENUM(MID_MORIA_IO, MORIA: I/O buffers)

#define ENUM(x, y) x,
enum
{
	MID_NAMES/*space*/MID_N
};
#undef ENUM

/*************************************************************************************************
 * OTHER CONSTANTS
 *************************************************************************************************/

#define MEGABYTE (1.0 / (1024.0 * 1024.0))

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

/*
 * This struct records three numbers for each memory field: its current usage, its peak usage over
 * the entire program run, and its usage at the time when the overall peak reached maximum.
 */
typedef struct
{
	long int cur_tot;
	long int peak_tot;
	long int current[MID_N];
	long int peak[MID_N];
	long int at_tot_peak[MID_N];
} MemoryStatistics;

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

extern MemoryStatistics mem_stats;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void* memAlloc(const char *file, const char *func, int line, int mem_id, size_t size);
void* memRealloc(const char *file, const char *func, int line, int mem_id, void *ptr,
		size_t new_size, size_t old_size);
void memFree(const char *file, const char *func, int line, int mem_id, void *mem, size_t old_size);

void resetMemoryStatistics();

#endif
