/*************************************************************************************************
 *
 * This unit collects, analyzes and prints statistics about a sparta run. Each process keeps
 * local and particle statistics, only the main process keeps the global statistics object.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _STATISTICS_H_
#define _STATISTICS_H_

#include "global_types.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

#define MEMORY_MIN_PRINT_FRAC 0.01
#define TIMING_MIN_PRINT_FRAC 0.01

/*
 * Level 0: Global timers that only need to be measured on one core and sum to the total run time
 * of the program.
 */
#define TIMERS0 \
ENUM2(T0_INIT, Initialization)\
ENUM2(T0_SNAPS, Snapshots)\
ENUM2(T0_FINALIZE, Finalize)

/*
 * Level 1: Timers that differ between cores, including some that are only executed on the main
 * proc such as reading the halo catalog. However, because of communication calls, these timers
 * are mostly synched, except for the PROCESSSNAP/IDLE timers which quantify the independent work
 * every proc is doing.
 */
#define TIMERS1 \
ENUM2(T1_STATISTICS, Statistics)\
ENUM2(T1_CONNECTREQUESTS, Connection requests)\
ENUM2(T1_READCATS, Reading catalog (main))\
ENUM2(T1_CONNECT, Connections)\
ENUM2(T1_LB, Load balancing)\
ENUM2(T1_EXCHANGE, Exchange)\
ENUM2(T1_HALOBOX, Halo box)\
ENUM2(T1_READSNAPS, Reading snapshots)\
ENUM2(T1_PROCESS, Process snap (indep.))\
ENUM2(T1_IDLE, Idle)\
ENUM2(T1_SAVE, Write results)\
ENUM2(T1_DELETEHALOS, Delete ended halos)\
ENUM2(T1_RESTARTFILES, Write restart files)

/*
 * Level 2: Subdivisions within the independent work of each proc. These timers should sum to the
 * time in T1_PROCESSSNAP, which is why they are in their own category, rather than part of
 * level 1.
 */
#define TIMERS2 \
ENUM2(T2_BUILDTREE, Building tree)\
ENUM2(T2_SORT, Sorting)\
ENUM2(T2_TAGSUBS, Tagging subhalos)\
ENUM2(T2_HALOLOOP, Halo loop)\
ENUM2(T2_TREESEARCH, Tree searches)\
ENUM2(T2_GHOSTPROPS, Ghost properties)\
ENUM2(T2_MASSPROFILE, Mass profile)\
ENUM2(T2_PTL_PRERESULTS, Particle preparation)\
ENUM2(T2_SHO_PRERESULTS, Subhalo preparation)\
ENUM2(T2_TCR_RESULTS, Tracer results)\
ENUM2(T2_TCR_MARKENDING, Mark ending tracers)\
ENUM2(T2_PTL_POSTRESULTS, Particle post-processing)\
ENUM2(T2_SHO_POSTRESULTS, Subhalo post-processing)\
ENUM2(T2_TCR_IGNLIST, Tracer ignore lists)\
ENUM2(T2_TCR_DELETE, Delete tracers)\
ENUM2(T2_FINDENDEDHALOS, Find ended halos)\
ENUM2(T2_ANALYSES, Analyses)

/*
 * Level 3/4: Timers for the tracer result and halo analysis routines, one for each tracer/result
 * type and analysis type. No enums are necessary for these timers.
 */
#define T3_N (NTT * NRS)
#define T4_N (NAL)

#define ENUM2(x, y) x,

enum
{
	TIMERS0/*space*/T0_N
};

enum
{
	TIMERS1/*space*/T1_N
};

enum
{
	TIMERS2/*space*/T2_N
};

#undef ENUM2

/*
 * Non-timing statistics we also wish to collect.
 */
enum
{
	SNPSTAT_VRATIO, SNPSTAT_FILESREAD, SNPSTAT_NDELETEDHALOS, SNPSTAT_N
};

enum
{
	RSSTAT_ANALYZED, RSSTAT_NEW_RES, RSSTAT_FAILED, RSSTAT_N
};

/*
 * Numbers to keep track of for tracers. The number of processed tracers must equal the following
 * fields (new tracer, was ignored, was tracked or not tracked for some reason), but only for
 * particle tracers. For subhalos, all active subhalos are processed, so these fields do not make
 * much sense.
 *
 * For all tracers, the number of deleted tracers must equal the possible reasons for deletion
 * (left the halo, couldn't be connected, halo became sub, or all analyses were completed).
 *
 * Not all fields have exactly the same meaning for each tracer, for example "unconnected" for
 * particles means that the particle wasn't within the sphere around the halo that was considered,
 * whereas for subhalos it means that the subhalo merged away.
 *
 * The SUB_TAG fields refer to the special case where tracers are tagged as belonging to a subhalo
 * at infall. The number of such tags is counted in SUB_TAG. If the tracers are created during this
 * process, those creations are counted in SUB_TAG_CREATED. Finally, if previously existing results
 * are tagged, those tags are counted in SUB_TAG_RESULTS.
 */
enum
{
	// Processed tracers
	TCRSTAT_PROCESSED,
	TCRSTAT_NEW,
	TCRSTAT_RECREATED,
	TCRSTAT_IGNORED,
	TCRSTAT_TRACKED,
	TCRSTAT_NOT_TRACKED,
	// Deleted tracers
	TCRSTAT_DELETED,
	TCRSTAT_HALO_BECAME_SUB,
	TCRSTAT_HALO_BECAME_GHOST,
	TCRSTAT_LEFT_HALO,
	TCRSTAT_UNCONNECTED,
	TCRSTAT_ALL_RS_DONE,
	// Other counters
	TCRSTAT_SUB_TAG,
	TCRSTAT_SUB_TAG_IGNORED,
	TCRSTAT_SUB_TAG_CREATED,
	TCRSTAT_SUB_TAG_RESULT,
	// Length of this enum
	TCRSTAT_N
};

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct
{
	long int n;
	long int n_alloc;
	int n_exc;
	long int n_saved;
	long int n_saved_cum;
	float mem;
} DynArrayStatistics;

typedef struct
{
	DynArrayStatistics da;
	long int n[RSSTAT_N];
} ResultStatistics;

typedef struct
{
	DynArrayStatistics tcr;
	DynArrayStatistics iid;
	long int n[TCRSTAT_N];
	long int n_in_hosts;
	long int n_in_subs;
	long int n_in_ghosts;
	ResultStatistics rs[NRS];
} TracerStatistics;

typedef struct
{
	DynArrayStatistics da;
} AnalysisStatistics;

/*
 * The timers and fields in these local statistics are written per snapshot, i.e. they are NOT
 * cumulative and are reset at the beginning of each snap.
 */
typedef struct
{
	double timers_1[T1_N];
	double timers_2[T2_N];
	double timers_3[T3_N];
	double timers_4[T4_N];
	double snpstats[SNPSTAT_N];
	TracerStatistics tt[NTT];
	AnalysisStatistics al[NAL];
} LocalStatistics;

/*
 * Statistics about the current snapshot, as well as some cumulative timing statistics. This struct
 * is only kept on the main proc. All memory fields are in units of MB.
 *
 * The timing statistics at the levels that differ between procs, as well as the non-timing
 * statistics, also contain cumulative, min, max, and mean fields. Some of these fields may not
 * make sense for the non-timing statistics, but they are computed anyway.
 */
typedef struct
{
	DynArrayStatistics halos;

	long int n_halos_connected;
	long int n_halos_new;
	long int n_halos_host;
	long int n_halos_sub;
	long int n_halos_ghost;
	int n_exd_subs;

	float mem_total;

	TracerStatistics tt[NTT];
	AnalysisStatistics al[NAL];
	long int tcrstat_cum[NTT][TCRSTAT_N];
	long int rsstat_cum[NTT][NRS][RSSTAT_N];

	int max_proc;

	double timers_0_cum[T0_N];

	double timers_1_cum[T1_N];
	double timers_1_min[T1_N];
	double timers_1_max[T1_N];
	double timers_1_mean[T1_N];
	double timers_1_maxproc[T1_N];

	double timers_2_cum[T2_N];
	double timers_2_min[T2_N];
	double timers_2_max[T2_N];
	double timers_2_mean[T2_N];
	double timers_2_maxproc[T2_N];

	double timers_3_cum[T3_N];
	double timers_3_min[T3_N];
	double timers_3_max[T3_N];
	double timers_3_mean[T3_N];
	double timers_3_maxproc[T3_N];

	double timers_4_cum[T4_N];
	double timers_4_min[T4_N];
	double timers_4_max[T4_N];
	double timers_4_mean[T4_N];
	double timers_4_maxproc[T4_N];

	double procstats_cum[SNPSTAT_N];
	double procstats_min[SNPSTAT_N];
	double procstats_max[SNPSTAT_N];
	double procstats_mean[SNPSTAT_N];
	double procstats_maxproc[SNPSTAT_N];
} GlobalStatistics;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initGlobalStatistics(GlobalStatistics *gs);
void initLocalStatistics(LocalStatistics *ls);

void collectHaloStatistics(DynArray *halos, GlobalStatistics *gs);
void collectTracerStatistics(DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs);
void collectTimingStatistics(LocalStatistics *ls, GlobalStatistics *gs);
void collectAllStatistics(DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs);

void printSnapStatistics(int snap_idx, GlobalStatistics *gs, int log_level);
void printMemoryStatistics(int snap_idx, int final);
void printMemoryStatisticsProcess(DynArray *halos, int snap_idx, char *after_step,
		int print_halo_stats);
void printTimingStatisticsFinal(GlobalStatistics *gs);
void printHaloWarnings(int snap_idx, DynArray *halos);

#endif
