/*************************************************************************************************
 *
 * This unit implements the config system for sparta, namely reading the config file, checking
 * the halo catalog and snapshot directories, and computing cosmological quantities.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <mpi.h>
#include <sys/stat.h>

#include "config.h"
#include "constants.h"
#include "global.h"
#include "utils.h"
#include "tree.h"
#include "halos/halo_definitions.h"
#include "io/io_snaps.h"
#include "io/io_halocat.h"
#include "results/results.h"
#include "analyses/analyses.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

#define CONFIG_LINE_LENGTH 10000

#define ENUM(x, y) #y,
const char *CAT_TYPE_NAMES[N_CONFIG_CAT_TYPES] =
	{CONFIG_CAT_TYPES};
#undef ENUM

#define ENUM(x, y) #y,
const char *SIM_TYPE_NAMES[N_CONFIG_SIM_TYPES] =
	{CONFIG_SIM_TYPES};
#undef ENUM

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

ConfigData config;

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void readConfigFile(char *filename);
void checkConfigUser();
void analyzeSnaplist();
void analyzeSnapshotHeader();
void setCosmology();
void deriveParameters();
void createDirectories();
void checkConfigDerived();

/*
 * Utility functions
 */
void str_tt_rs(char *key, int *tt, int *rs);
int str_catfield(char *key);

/*
 * Functions for specific parameters
 */
void setMemoryFactors();

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * The procedure for setting the global config. Note that the analyses often use more complicated
 * config procedures, so they have been separated out into each individual analysis.
 */
void processConfig(char *config_file, int file_only)
{
	if (is_main_proc)
	{
		if (file_only)
		{
			setDefaultConfig();
			readConfigFile(config_file);
		} else
		{
			setDefaultConfig();
			initConfigResults(CONFIG_INIT_STEP_DEFAULT);
			initConfigAnalyses(CONFIG_INIT_STEP_DEFAULT);
			readConfigFile(config_file);
			checkConfigUser();
			initConfigResults(CONFIG_INIT_STEP_CHECK);
			initConfigAnalyses(CONFIG_INIT_STEP_CHECK);
			analyzeSnaplist();
			analyzeSnapshotHeader();
			setCosmology();
			deriveParameters();
			initConfigResults(CONFIG_INIT_STEP_CONFIG);
			initConfigAnalyses(CONFIG_INIT_STEP_CONFIG);
			createDirectories();
			checkConfigDerived();
		}
	}

	MPI_Bcast(&config, sizeof(ConfigData), MPI_BYTE, MAIN_PROC, MPI_COMM_WORLD);
}

void setDefaultConfig()
{
	int i, tt, rs;

	// Input (Halo catalogs)
	strcpy(config.cat_path, DEFAULT_CAT_PATH);
	config.cat_type = -1;
	config.cat_n_chunks = -1;
	config.cat_tolerance_a_snap = DEFAULT_CAT_TOLERANCE_A_SNAP;
	config.cat_reliable_n200m = DEFAULT_CAT_RELIABLE_N200M;
	config.cat_halo_jump_tol_phys = DEFAULT_CAT_HALO_JUMP_TOL_PHYS;
	config.cat_halo_jump_tol_box = DEFAULT_CAT_HALO_JUMP_TOL_BOX;

	// Input (snapshots)
	strcpy(config.snap_path, DEFAULT_SNAP_PATH);
	config.snap_sim_type = -1;
	config.snap_max_read_procs = DEFAULT_SNAP_MAX_READ_PROCS;
	config.snap_read_on_main = DEFAULT_SNAP_READ_ON_MAIN;
	config.snap_max_waiting_messages = DEFAULT_SNAP_MAX_WAITING_MESSAGES;

	// Input (simulation properties)
	config.sim_force_res = DEFAULT_SIM_FORCE_RES;
	config.sim_force_res_comoving = DEFAULT_SIM_FORCE_RES_COMOVING;

	// Output
	strcpy(config.output_path, DEFAULT_OUTPUT_PATH);
	strcpy(config.output_file, DEFAULT_OUTPUT_FILE);
	config.output_compression_level = DEFAULT_OUTPUT_COMPRESSION_LEVEL;
	config.output_restart_files = DEFAULT_OUTPUT_RESTART_FILES;
	config.output_restart_every = DEFAULT_OUTPUT_RESTART_EVERY;
	config.output_min_n200m = DEFAULT_OUTPUT_MIN_N200M;
	for (tt = 0; tt < NTT; tt++)
		for (rs = 0; rs < NRS; rs++)
			config.output_min_results[tt][rs] = 0;

	// Logging
	config.log_level = DEFAULT_LOG_LEVEL;
	config.log_level_memory = DEFAULT_LOG_LEVEL_MEMORY;
	config.log_level_timing = DEFAULT_LOG_LEVEL_TIMING;
	config.log_flush = DEFAULT_LOG_FLUSH;

	// Error levels
	config.err_level_cannot_fork = DEFAULT_ERR_LEVEL_CANNOT_FORK;
	config.err_level_file_copy_failed = DEFAULT_ERR_LEVEL_FILE_COPY_FAILED;
	config.err_level_cat_snap_mismatch = DEFAULT_ERR_LEVEL_CAT_SNAP_MISMATCH;
	config.err_level_skipping_snap = DEFAULT_ERR_LEVEL_SKIPPING_SNAP;
	config.err_level_rockstar_filename = DEFAULT_ERR_LEVEL_ROCKSTAR_FILENAME;
	config.err_level_invalid_domain_box = DEFAULT_ERR_LEVEL_INVALID_DOMAIN_BOX;
	config.err_level_domain_minor = DEFAULT_ERR_LEVEL_DOMAIN_MINOR;
	config.err_level_halo_req_not_found = DEFAULT_ERR_LEVEL_HALO_REQ_NOT_FOUND;
	config.err_level_desc_not_found = DEFAULT_ERR_LEVEL_DESC_NOT_FOUND;
	config.err_level_found_unexpected = DEFAULT_ERR_LEVEL_FOUND_UNEXPECTED;
	config.err_level_halo_jump = DEFAULT_ERR_LEVEL_HALO_JUMP;
	config.err_level_missing_halo_ptl = DEFAULT_ERR_LEVEL_MISSING_HALO_PTL;
	config.err_level_cat_radius_diff = DEFAULT_ERR_LEVEL_CAT_RADIUS_DIFF;
	config.err_level_zero_ptl = DEFAULT_ERR_LEVEL_ZERO_PTL;
	config.err_level_zero_ptl_phantom = DEFAULT_ERR_LEVEL_ZERO_PTL_PHANTOM;
	config.err_level_radius_outside_box = DEFAULT_ERR_LEVEL_RADIUS_OUTSIDE_BOX;
	config.err_level_radius_not_found = DEFAULT_ERR_LEVEL_RADIUS_NOT_FOUND;
	config.err_level_radius_bound_diff = DEFAULT_ERR_LEVEL_RADIUS_BOUND_DIFF;
	config.err_level_search_radius = DEFAULT_ERR_LEVEL_SEARCH_RADIUS;
	config.err_level_exc_host_ended = DEFAULT_ERR_LEVEL_EXC_HOST_ENDED;
	config.err_level_exc_halo_not_found = DEFAULT_ERR_LEVEL_EXC_HALO_NOT_FOUND;
	config.err_level_max_exchange_size = DEFAULT_ERR_LEVEL_MAX_EXCHANGE_SIZE;
	config.err_level_large_halo = DEFAULT_ERR_LEVEL_LARGE_HALO;

	// Instructions
	config.last_snap = DEFAULT_LAST_SNAP;
	config.last_scale_factor = DEFAULT_LAST_SCALE_FACTOR;
	for (tt = 0; tt < NTT; tt++)
		for (rs = 0; rs < NRS; rs++)
			config.all_instructions[tt][rs] = 1;
	config.n_halo_instructions = 0;
	for (i = 0; i < MAX_HALO_INSTRUCTIONS; i++)
	{
		config.halo_instructions[i].id = INVALID_ID;
		config.halo_instructions[i].tt = -1;
		config.halo_instructions[i].rs = -1;
	}

	// Domain & Exchange
	config.lb_do_load_balancing = DEFAULT_LB_DO_LOAD_BALANCING;
	config.lb_adjust_min_snap = DEFAULT_LB_ADJUST_MIN_SNAP;
	config.lb_slabs_adjust_factor = DEFAULT_LB_SLABS_ADJUST_FACTOR;
	config.lb_slabs_adjust_max = DEFAULT_LB_SLABS_ADJUST_MAX;
	config.lb_slabs_drift_tolerance = DEFAULT_LB_SLABS_DRIFT_TOLERANCE;
	config.exc_max_message_size = DEFAULT_EXC_MAX_MESSAGE_SIZE;

	// Halos
	config.halo_min_radius_mass_profile = DEFAULT_HALO_MIN_RADIUS_MASS_PROFILE;
	config.halo_max_radius_ratio_cat = DEFAULT_HALO_MAX_RADIUS_RATIO_CAT;
	config.halo_correct_phantoms = DEFAULT_HALO_CORRECT_PHANTOMS;
	config.ghost_min_n200m = DEFAULT_GHOST_MIN_N200M;
	config.ghost_min_host_distance = DEFAULT_GHOST_MIN_HOST_DISTANCE;
	config.ghost_max_host_center_time = DEFAULT_GHOST_MAX_HOST_CENTER_TIME;

	// Other
	config.random_seed = DEFAULT_RANDOM_SEED;
	config.tree_points_per_leaf = DEFAULT_TREE_POINTS_PER_LEAF;
	config.potential_points_per_leaf = DEFAULT_POTENTIAL_POINTS_PER_LEAF;
	config.potential_err_tol = DEFAULT_POTENTIAL_ERR_TOL;
	config.memory_allocation_factor = DEFAULT_MEMORY_ALLOCATION_FACTOR;
	config.memory_dealloc_safety_factor = DEFAULT_MEMORY_DEALLOC_SAFETY_FACTOR;
	config.memory_max_halo_size = DEFAULT_MEMORY_MAX_HALO_SIZE;

	// Tracer Particles
	config.tcr_ptl_create_radius = DEFAULT_TCR_PTL_CREATE_RADIUS;
	config.tcr_ptl_delete_radius = DEFAULT_TCR_PTL_DELETE_RADIUS;
	config.tcr_ptl_subtag_radius = DEFAULT_TCR_PTL_SUBTAG_RADIUS;
	config.tcr_ptl_subtag_inclusive = DEFAULT_TCR_PTL_SUBTAG_INCLUSIVE;
	config.tcr_ptl_subtag_ifl_age_min = DEFAULT_TCR_PTL_SUBTAG_IFL_AGE_MIN;
	config.tcr_ptl_subtag_ifl_dist_min = DEFAULT_TCR_PTL_SUBTAG_IFL_DIST_MIN;
	config.tcr_ptl_subtag_bound_radius = DEFAULT_TCR_PTL_SUBTAG_BOUND_RADIUS;
	config.tcr_ptl_subtag_bound_ratio = DEFAULT_TCR_PTL_SUBTAG_BOUND_RATIO;
	config.tcr_ptl_subtag_host_max_age = DEFAULT_TCR_PTL_SUBTAG_HOST_MAX_AGE;

	/*
	 * The memory parameters must be set even in the default config, since they may be used by
	 * external tools such as MORIA.
	 */
	setMemoryFactors();
}

/*
 * Convert a line to a key-value pair. Note that the line is altered, whereas the key and value are
 * merely pointers into the line.
 */
int lineToKeyValue(char *line, char **key, char **value)
{
	char *p;

	// Convert tabs to spaces
	p = line;
	while (*p != '\0')
	{
		if ((*p == '\n') || (*p == '\t'))
			*p = ' ';
		p++;
	}
	p = line;

	// Ignore whitespace
	while (*p == ' ')
		p++;

	// Continue if the line is blank or a comment
	if ((*p == '\0') || (*p == '#'))
		return 1;

	// Found keyword
	*key = p;
	while (*p != ' ')
		p++;
	*p = '\0';
	p++;

	// Skip whitespace once more
	while (*p == ' ')
		p++;

	// Read value. Note that we do not stop on whitespace
	*value = p;
	while ((*p != '#') && (*p != '\0'))
		p++;

	// Now remove trailing white spaces from value
	p--;
	while (*p == ' ')
		p--;
	p++;
	*p = '\0';

	return 0;
}

void readConfigFile(char *filename)
{
	int i, tt, rs, n_halo_ids, n;
	long int halo_ids[MAX_HALO_INSTRUCTIONS];
	char line[CONFIG_LINE_LENGTH], *key, *value;
	FILE *f;

	tt = 0;
	rs = 0;

	f = fopen(filename, "r");
	if (f == NULL)
		error(__FFL__, "[Main] Config file %s could not be opened.\n", filename);

	while (fgets(line, CONFIG_LINE_LENGTH, f) != NULL)
	{
		if (lineToKeyValue(line, &key, &value) > 0)
			continue;

		/*
		 * Input (Halo catalogs). When the cat_type key is found, we can know how many catalog
		 * fields there are and what their default values are.
		 */
		if (strcmp(key, "cat_path") == 0)
		{
			strcpy(config.cat_path, value);
		} else if (strcmp(key, "cat_type") == 0)
		{
			for (i = 0; i < N_CONFIG_CAT_TYPES; i++)
			{
				if (strcmp(value, CAT_TYPE_NAMES[i]) == 0)
					config.cat_type = i;
			}
			if (config.cat_type < 0)
			{
				output(0, "Valid halo catalog types:\n");
				for (i = 0; i < N_CONFIG_CAT_TYPES; i++)
					output(0, "%s\n", CAT_TYPE_NAMES[i]);
				error(__FFL__, "[Main] Unknown halo catalog type, %s.\n", value);
			}

			char **cat_field_defaults;

			config.cat_fields_n = getNCatFields();
			cat_field_defaults = getCatFieldDefaults();
			for (i = 0; i < config.cat_fields_n; i++)
				strcpy(config.cat_fields[i], cat_field_defaults[i]);
		} else if (stringStartsWith(key, CONFIG_PREFIX_CATFIELDS))
		{
			strcpy(config.cat_fields[str_catfield(key)], value);
		} else if (strcmp(key, "cat_tolerance_a_snap") == 0)
		{
			config.cat_tolerance_a_snap = atof(value);
		} else if (strcmp(key, "cat_reliable_n200m") == 0)
		{
			config.cat_reliable_n200m = atoi(value);
		} else if (strcmp(key, "cat_halo_jump_tol_phys") == 0)
		{
			config.cat_halo_jump_tol_phys = atof(value);
		} else if (strcmp(key, "cat_halo_jump_tol_box") == 0)
		{
			config.cat_halo_jump_tol_box = atof(value);
		}
		/*
		 * Input (Snapshots)
		 */
		else if (strcmp(key, "snap_path") == 0)
		{
			strcpy(config.snap_path, value);
		} else if (strcmp(key, "snap_sim_type") == 0)
		{
			for (i = 0; i < N_CONFIG_SIM_TYPES; i++)
			{
				if (strcmp(value, SIM_TYPE_NAMES[i]) == 0)
					config.snap_sim_type = i;
			}
			if (config.snap_sim_type < 0)
			{
				output(0, "Valid simulation types:\n");
				for (i = 0; i < N_CONFIG_SIM_TYPES; i++)
					output(0, "%s\n", SIM_TYPE_NAMES[i]);
				error(__FFL__, "[Main] Unknown simulation type, %s.\n", value);
			}
		} else if (strcmp(key, "snap_max_read_procs") == 0)
		{
			config.snap_max_read_procs = atoi(value);
		} else if (strcmp(key, "snap_read_on_main") == 0)
		{
			config.snap_read_on_main = atoi(value);
		} else if (strcmp(key, "snap_max_waiting_messages") == 0)
		{
			config.snap_max_waiting_messages = atoi(value);
		}
		/*
		 * Simulation properties
		 */
		else if (strcmp(key, "sim_force_res") == 0)
		{
			config.sim_force_res = atof(value);
		} else if (strcmp(key, "sim_force_res_comoving") == 0)
		{
			config.sim_force_res_comoving = atoi(value);
		}
		/*
		 * Output parameters
		 */
		else if (strcmp(key, "output_path") == 0)
		{
			strcpy(config.output_path, value);
		} else if (strcmp(key, "output_file") == 0)
		{
			strcpy(config.output_file, value);
		} else if (strcmp(key, "output_compression_level") == 0)
		{
			config.output_compression_level = atoi(value);
		} else if (strcmp(key, "output_restart_files") == 0)
		{
			config.output_restart_files = atoi(value);
		} else if (strcmp(key, "output_restart_every") == 0)
		{
			config.output_restart_every = atoi(value);
		} else if (strcmp(key, "output_min_n200m") == 0)
		{
			config.output_min_n200m = atoi(value);
		} else if (stringStartsWith(key, CONFIG_PREFIX_MINRS))
		{
			str_tt_rs(key, &tt, &rs);
			config.output_min_results[tt][rs] = atoi(value);
		}
		/*
		 * Logging parameters
		 */
		else if (strcmp(key, "log_level") == 0)
		{
			config.log_level = atoi(value);
		} else if (strcmp(key, "log_level_memory") == 0)
		{
			config.log_level_memory = atoi(value);
		} else if (strcmp(key, "log_level_timing") == 0)
		{
			config.log_level_timing = atoi(value);
		} else if (strcmp(key, "log_flush") == 0)
		{
			config.log_flush = atoi(value);
		}
		/*
		 * Error levels
		 */
		else if (strcmp(key, "err_level_cannot_fork") == 0)
		{
			config.err_level_cannot_fork = atoi(value);
		} else if (strcmp(key, "err_level_file_copy_failed") == 0)
		{
			config.err_level_file_copy_failed = atoi(value);
		} else if (strcmp(key, "err_level_cat_snap_mismatch") == 0)
		{
			config.err_level_cat_snap_mismatch = atoi(value);
		} else if (strcmp(key, "err_level_skipping_snap") == 0)
		{
			config.err_level_skipping_snap = atoi(value);
		} else if (strcmp(key, "err_level_rockstar_filename") == 0)
		{
			config.err_level_rockstar_filename = atoi(value);
		} else if (strcmp(key, "err_level_invalid_domain_box") == 0)
		{
			config.err_level_invalid_domain_box = atoi(value);
		} else if (strcmp(key, "err_level_domain_minor") == 0)
		{
			config.err_level_domain_minor = atoi(value);
		} else if (strcmp(key, "err_level_halo_req_not_found") == 0)
		{
			config.err_level_halo_req_not_found = atoi(value);
		} else if (strcmp(key, "err_level_desc_not_found") == 0)
		{
			config.err_level_desc_not_found = atoi(value);
		} else if (strcmp(key, "err_level_found_unexpected") == 0)
		{
			config.err_level_found_unexpected = atoi(value);
		} else if (strcmp(key, "err_level_halo_jump") == 0)
		{
			config.err_level_halo_jump = atoi(value);
		} else if (strcmp(key, "err_level_missing_halo_ptl") == 0)
		{
			config.err_level_missing_halo_ptl = atoi(value);
		} else if (strcmp(key, "err_level_cat_radius_diff") == 0)
		{
			config.err_level_cat_radius_diff = atoi(value);
		} else if (strcmp(key, "err_level_zero_ptl") == 0)
		{
			config.err_level_zero_ptl = atoi(value);
		} else if (strcmp(key, "err_level_zero_ptl_phantom") == 0)
		{
			config.err_level_zero_ptl_phantom = atoi(value);
		} else if (strcmp(key, "err_level_radius_outside_box") == 0)
		{
			config.err_level_radius_outside_box = atoi(value);
		} else if (strcmp(key, "err_level_radius_not_found") == 0)
		{
			config.err_level_radius_not_found = atoi(value);
		} else if (strcmp(key, "err_level_radius_bound_diff") == 0)
		{
			config.err_level_radius_bound_diff = atoi(value);
		} else if (strcmp(key, "err_level_search_radius") == 0)
		{
			config.err_level_search_radius = atoi(value);
		} else if (strcmp(key, "err_level_exc_host_ended") == 0)
		{
			config.err_level_exc_host_ended = atoi(value);
		} else if (strcmp(key, "err_level_exc_halo_not_found") == 0)
		{
			config.err_level_exc_halo_not_found = atoi(value);
		} else if (strcmp(key, "err_level_max_exchange_size") == 0)
		{
			config.err_level_max_exchange_size = atoi(value);
		} else if (strcmp(key, "err_level_large_halo") == 0)
		{
			config.err_level_large_halo = atoi(value);
		}
		/*
		 * Instructions
		 */
		else if (strcmp(key, "last_snap") == 0)
		{
			config.last_snap = atoi(value);
		} else if (strcmp(key, "last_scale_factor") == 0)
		{
			config.last_scale_factor = atof(value);
		} else if (stringStartsWith(key, CONFIG_PREFIX_DOALL))
		{
			str_tt_rs(key, &tt, &rs);
			config.all_instructions[tt][rs] = atoi(value);
		} else if (stringStartsWith(key, CONFIG_PREFIX_DOHALO))
		{
			str_tt_rs(key, &tt, &rs);
			n_halo_ids = stringToListLong(value, halo_ids, MAX_HALO_INSTRUCTIONS, 0);
			if (n_halo_ids < 2)
				error(__FFL__,
						"Found halo instruction '%s', but only one number; first number must be 0 or 1, then halo IDs.\n",
						key);
			n = config.n_halo_instructions;
			for (i = 0; i < n_halo_ids - 1; i++)
			{
				if ((halo_ids[0] != 0) && (halo_ids[0] != 1))
					error(__FFL__,
							"Found halo instruction '%s', first number is %ld but must be 0 or 1.\n",
							key, halo_ids[0]);

				config.halo_instructions[n].id = halo_ids[i + 1];
				config.halo_instructions[n].tt = tt;
				config.halo_instructions[n].rs = rs;
				config.halo_instructions[n].do_tt_rs = halo_ids[0];
				n++;
			}
			config.n_halo_instructions = n;
		}
		/*
		 * Domain & Exchange
		 */
		else if (strcmp(key, "lb_do_load_balancing") == 0)
		{
			config.lb_do_load_balancing = atof(value);
		} else if (strcmp(key, "lb_adjust_min_snap") == 0)
		{
			config.lb_adjust_min_snap = atoi(value);
		} else if (strcmp(key, "lb_slabs_adjust_factor") == 0)
		{
			config.lb_slabs_adjust_factor = atof(value);
		} else if (strcmp(key, "lb_slabs_adjust_max") == 0)
		{
			config.lb_slabs_adjust_max = atof(value);
		} else if (strcmp(key, "lb_slabs_drift_tolerance") == 0)
		{
			config.lb_slabs_drift_tolerance = atof(value);
		} else if (strcmp(key, "exc_max_message_size") == 0)
		{
			config.exc_max_message_size = atof(value);
		}
		/*
		 * Other
		 */
		else if (strcmp(key, "random_seed") == 0)
		{
			config.random_seed = atoi(value);
		} else if (strcmp(key, "tree_points_per_leaf") == 0)
		{
			config.tree_points_per_leaf = atoi(value);
		} else if (strcmp(key, "potential_points_per_leaf") == 0)
		{
			config.potential_points_per_leaf = atoi(value);
		} else if (strcmp(key, "potential_err_tol") == 0)
		{
			config.potential_err_tol = atof(value);
		} else if (strcmp(key, "memory_allocation_factor") == 0)
		{
			config.memory_allocation_factor = atof(value);
		} else if (strcmp(key, "memory_dealloc_safety_factor") == 0)
		{
			config.memory_dealloc_safety_factor = atof(value);
		} else if (strcmp(key, "memory_max_halo_size") == 0)
		{
			config.memory_max_halo_size = atof(value);
		}
		/*
		 * Halos
		 */
		else if (strcmp(key, "halo_min_radius_mass_profile") == 0)
		{
			config.halo_min_radius_mass_profile = atof(value);
		} else if (strcmp(key, "halo_max_radius_ratio_cat") == 0)
		{
			config.halo_max_radius_ratio_cat = atof(value);
		} else if (strcmp(key, "halo_correct_phantoms") == 0)
		{
			config.halo_correct_phantoms = atoi(value);
		} else if (strcmp(key, "ghost_min_n200m") == 0)
		{
			config.ghost_min_n200m = atoi(value);
		} else if (strcmp(key, "ghost_min_host_distance") == 0)
		{
			config.ghost_min_host_distance = atof(value);
		} else if (strcmp(key, "ghost_max_host_center_time") == 0)
		{
			config.ghost_max_host_center_time = atof(value);
		}
		/*
		 * Tracer Particles
		 */
		else if (strcmp(key, "tcr_ptl_create_radius") == 0)
		{
			config.tcr_ptl_create_radius = atof(value);
		} else if (strcmp(key, "tcr_ptl_delete_radius") == 0)
		{
			config.tcr_ptl_delete_radius = atof(value);
		} else if (strcmp(key, "tcr_ptl_subtag_radius") == 0)
		{
			config.tcr_ptl_subtag_radius = atof(value);
		} else if (strcmp(key, "tcr_ptl_subtag_inclusive") == 0)
		{
			config.tcr_ptl_subtag_inclusive = atoi(value);
		} else if (strcmp(key, "tcr_ptl_subtag_ifl_age_min") == 0)
		{
			config.tcr_ptl_subtag_ifl_age_min = atof(value);
		} else if (strcmp(key, "tcr_ptl_subtag_ifl_dist_min") == 0)
		{
			config.tcr_ptl_subtag_ifl_dist_min = atof(value);
		} else if (strcmp(key, "tcr_ptl_subtag_bound_radius") == 0)
		{
			config.tcr_ptl_subtag_bound_radius = atof(value);
		} else if (strcmp(key, "tcr_ptl_subtag_bound_ratio") == 0)
		{
			config.tcr_ptl_subtag_bound_ratio = atof(value);
		} else if (strcmp(key, "tcr_ptl_subtag_host_max_age") == 0)
		{
			config.tcr_ptl_subtag_host_max_age = atof(value);
		}
		/*
		 * Result orbit count
		 */
		else if (strcmp(key, "res_oct_max_norbit") == 0)
		{
			config.res_oct_max_norbit = atoi(value);
		}
		/*
		 * Analysis Rsp
		 */
		else if (strcmp(key, "anl_rsp_redshifts") == 0)
		{
			config.anl_rsp_n_redshifts = stringToListFloat(value, config.anl_rsp_redshifts,
			ANALYSIS_RSP_MAX_SNAPS, 0);
		} else if (strcmp(key, "anl_rsp_defs") == 0)
		{
			config.anl_rsp_n_defs = stringToListString(value, (char*) config.anl_rsp_defs_str[0],
			ANALYSIS_RSP_MAX_DEFINITIONS, 0, DEFAULT_HALO_DEF_STRLEN);
		} else if (strcmp(key, "anl_rsp_min_rrm") == 0)
		{
			config.anl_rsp_min_rrm = atof(value);
		} else if (strcmp(key, "anl_rsp_max_rrm") == 0)
		{
			config.anl_rsp_max_rrm = atof(value);
		} else if (strcmp(key, "anl_rsp_min_smr") == 0)
		{
			config.anl_rsp_min_smr = atof(value);
		} else if (strcmp(key, "anl_rsp_max_smr") == 0)
		{
			config.anl_rsp_max_smr = atof(value);
		} else if (strcmp(key, "anl_rsp_demand_infall_rs") == 0)
		{
			config.anl_rsp_demand_infall_rs = atoi(value);
		} else if (strcmp(key, "anl_rsp_sigma_tdyn") == 0)
		{
			config.anl_rsp_sigma_tdyn = atof(value);
		} else if (strcmp(key, "anl_rsp_min_weight") == 0)
		{
			config.anl_rsp_min_weight = atof(value);
		} else if (strcmp(key, "anl_rsp_n_bootstrap") == 0)
		{
			config.anl_rsp_n_bootstrap = atoi(value);
		} else if (strcmp(key, "anl_rsp_do_correction") == 0)
		{
			config.anl_rsp_do_correction = atoi(value);
		}
		/*
		 * Analysis Profiles
		 */
		else if (strcmp(key, "anl_prf_rmin") == 0)
		{
			config.anl_prf_rmin = atof(value);
		} else if (strcmp(key, "anl_prf_rmax") == 0)
		{
			config.anl_prf_rmax = atof(value);
		} else if (strcmp(key, "anl_prf_do_subs") == 0)
		{
			config.anl_prf_do_subs = atof(value);
		} else if (strcmp(key, "anl_prf_do_ghosts") == 0)
		{
			config.anl_prf_do_ghosts = atof(value);
		} else if (strcmp(key, "anl_prf_redshifts") == 0)
		{
			config.anl_prf_n_redshifts = stringToListFloat(value, config.anl_prf_redshifts,
			ANALYSIS_PROFILES_MAX_SNAPS, 0);
		}
		/*
		 * Analysis Haloprops
		 */
		else if (strcmp(key, "anl_hps_redshifts") == 0)
		{
			config.anl_hps_n_redshifts = stringToListFloat(value, config.anl_hps_redshifts,
			ANALYSIS_HALOPROPS_MAX_SNAPS, 0);
		} else if (strcmp(key, "anl_hps_defs") == 0)
		{
			config.anl_hps_n_defs = stringToListString(value, (char*) config.anl_hps_defs_str[0],
			ANALYSIS_HALOPROPS_MAX_DEFINITIONS, 0, DEFAULT_HALO_DEF_STRLEN);
		} else if (strcmp(key, "anl_hps_r_max_so_host") == 0)
		{
			config.anl_hps_r_max_so_host = atof(value);
		} else if (strcmp(key, "anl_hps_r_max_so_sub") == 0)
		{
			config.anl_hps_r_max_so_sub = atof(value);
		} else if (strcmp(key, "anl_hps_r_max_orb_host") == 0)
		{
			config.anl_hps_r_max_orb_host = atof(value);
		} else if (strcmp(key, "anl_hps_r_max_orb_sub") == 0)
		{
			config.anl_hps_r_max_orb_sub = atof(value);
		} else if (strcmp(key, "anl_hps_r_unbinding_host") == 0)
		{
			config.anl_hps_r_unbinding_host = atof(value);
		} else if (strcmp(key, "anl_hps_r_unbinding_sub") == 0)
		{
			config.anl_hps_r_unbinding_sub = atof(value);
		} else if (strcmp(key, "anl_hps_iterative_unbinding") == 0)
		{
			config.anl_hps_iterative_unbinding = atoi(value);
		}
		/*
		 * Error
		 */
		else
		{
			error(__FFL__, "[Main] Key '%s' is not a valid config parameter.\n", key);
		}
	}

	fclose(f);
}

/*
 * Check parameters that the user has to give. If those are wrong, we should not attempt to do
 * anything else.
 */
void checkConfigUser()
{
	// Input (Catalogs)
	assert(strlen(config.cat_path) > 0);
	assert(config.cat_type >= 0);
	assert(config.cat_reliable_n200m > 0);

	// Input (Snapshots)
	assert(strlen(config.snap_path) > 0);
	assert(config.snap_sim_type >= 0);
	assert(config.snap_max_read_procs > 0);
	assertBool(config.snap_read_on_main);
	assert(config.snap_max_waiting_messages > 0);

	// Output
	assert(strlen(config.output_path) > 0);
	assert(strlen(config.output_file) > 0);
	assert((config.output_compression_level >= 0) && (config.output_compression_level <= 9));
	assertBool(config.output_restart_files);
	assert(config.output_restart_every >= 1);

	// Logging
	assert(config.log_level >= 0);
	assert(config.log_level_memory >= 0);
	assert(config.log_level_timing >= 0);
	assertBool(config.log_flush);

	// Error level
	assert(
			(config.err_level_cannot_fork >= ERR_LEVEL_IGNORE) && (config.err_level_cannot_fork <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_file_copy_failed >= ERR_LEVEL_IGNORE) && (config.err_level_file_copy_failed <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_cat_snap_mismatch >= ERR_LEVEL_IGNORE) && (config.err_level_cat_snap_mismatch <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_skipping_snap >= ERR_LEVEL_IGNORE) && (config.err_level_skipping_snap <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_rockstar_filename >= ERR_LEVEL_IGNORE) && (config.err_level_rockstar_filename <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_invalid_domain_box >= ERR_LEVEL_IGNORE) && (config.err_level_invalid_domain_box <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_domain_minor >= ERR_LEVEL_IGNORE) && (config.err_level_domain_minor <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_halo_req_not_found >= ERR_LEVEL_IGNORE) && (config.err_level_halo_req_not_found <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_desc_not_found >= ERR_LEVEL_IGNORE) && (config.err_level_desc_not_found <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_found_unexpected >= ERR_LEVEL_IGNORE) && (config.err_level_found_unexpected <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_halo_jump >= ERR_LEVEL_IGNORE) && (config.err_level_halo_jump <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_missing_halo_ptl >= ERR_LEVEL_IGNORE) && (config.err_level_missing_halo_ptl <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_cat_radius_diff >= ERR_LEVEL_IGNORE) && (config.err_level_cat_radius_diff <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_zero_ptl >= ERR_LEVEL_IGNORE) && (config.err_level_zero_ptl <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_zero_ptl_phantom >= ERR_LEVEL_IGNORE) && (config.err_level_zero_ptl_phantom <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_radius_outside_box >= ERR_LEVEL_IGNORE) && (config.err_level_radius_outside_box <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_radius_not_found >= ERR_LEVEL_IGNORE) && (config.err_level_radius_not_found <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_radius_bound_diff >= ERR_LEVEL_IGNORE) && (config.err_level_radius_bound_diff <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_search_radius >= ERR_LEVEL_IGNORE) && (config.err_level_search_radius <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_exc_host_ended >= ERR_LEVEL_IGNORE) && (config.err_level_exc_host_ended <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_exc_halo_not_found >= ERR_LEVEL_IGNORE) && (config.err_level_exc_halo_not_found <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_max_exchange_size >= ERR_LEVEL_IGNORE) && (config.err_level_max_exchange_size <= ERR_LEVEL_ERROR));
	assert(
			(config.err_level_large_halo >= ERR_LEVEL_IGNORE) && (config.err_level_large_halo <= ERR_LEVEL_ERROR));

	// Domain & Exchange
	assertBool(config.lb_do_load_balancing);
	assert(config.lb_adjust_min_snap >= -1);
	assert((config.lb_slabs_adjust_factor > 0.0) && (config.lb_slabs_adjust_factor <= 1.0));
	assert((config.lb_slabs_adjust_max > 0.0) && (config.lb_slabs_adjust_max <= 1.0));
	assert((config.lb_slabs_drift_tolerance > 0.0) && (config.lb_slabs_drift_tolerance <= 1.0));
	assert(config.exc_max_message_size > 0.0);

	// Other
	assert(config.random_seed >= 0);
	assert(config.tree_points_per_leaf > 1);
	assert(config.potential_points_per_leaf > 1);
	assert(config.potential_err_tol > 0.0);
	assert(config.memory_allocation_factor > 1.0);
	assert(
			(config.memory_dealloc_safety_factor > 0.0)
					&& (config.memory_dealloc_safety_factor < 1.0));
	assert(config.memory_max_halo_size > 0.0);

	// Tracer Particles
	assert(config.tcr_ptl_create_radius >= 0.0);
	assert(config.tcr_ptl_delete_radius >= 0.0);
	assert(config.tcr_ptl_delete_radius > config.tcr_ptl_create_radius);
}

/*
 * Create the list of snapshots over which SPARTA must run. This is achieved in two steps: we first
 * compile a complete list of available snapshots, and then find matching halo catalogs. In
 * principle, not all snapshots and/or halo catalogs must be used.
 */
void analyzeSnaplist()
{
	int n_snaps_tot, snapshot_id[MAX_SNAPS];
	float snapshot_a[MAX_SNAPS];

	n_snaps_tot = getSnapshotListing(snapshot_id, snapshot_a);
	if (n_snaps_tot == 0)
		error(__FFL__,
				"Found zero snapshots. Please check the snapshot path and filename format.\n");

	matchCatalogsToSnapshots(n_snaps_tot, snapshot_id, snapshot_a);
	if (config.n_snaps == 0)
		error(__FFL__, "Found zero snapshots.\n");
}

/*
 * Compute cosmological quantities, namely the physical time of the snapshots. Some cosmological
 * parameters are set to generic values since they do not influence this computation.
 */
void setCosmology()
{
	int i;
	float age_0, z;
	HaloDefinition def_200m;

	config.cosmo.flat = 1;
	config.cosmo.h = config.h;
	config.cosmo.Omega_m = config.Omega_m;
	config.cosmo.Omega_b = 0.0;
	config.cosmo.Omega_r = 0.0;
	config.cosmo.Omega_L = config.Omega_L;
	config.cosmo.w = -1;
	config.cosmo.T_CMB = 2.725;
	config.cosmo.sigma8 = 0.8;
	config.cosmo.ns = 1.0;

	age_0 = lookbackTime(&(config.cosmo), 100000.0);

	for (i = 0; i < config.n_snaps; i++)
	{
		z = config.snap_z[i];

		/*
		 * Physical parameters that depend on redshift. Note that aH does NOT have config.h in it
		 * because it is used to multiply distances in comoving Mpc/h, so the h's cancel.
		 */
		def_200m = getHaloDefinitionSO(HDEF_SUBTYPE_SO_MATTER, 200.0);
		config.snap_rho_200m[i] = densityThreshold(&(config.cosmo), z, &def_200m);
		config.snap_phys_conv[i] = 1000.0 * config.snap_a[i];
		config.snap_com_conv[i] = 1.0 / config.snap_phys_conv[i];
		config.snap_v200m_factor[i] = sqrt(
		AST_G_Gyr * 4.0 * MATH_pi / 3.0 * config.snap_rho_200m[i]);
		config.snap_aH[i] = config.snap_a[i] * 100.0 * Ez(&(config.cosmo), z);
		config.snap_t[i] = age_0 - lookbackTime(&(config.cosmo), z);
		config.snap_t_dyn[i] = pow(2, 1.5) * hubbleTime(&(config.cosmo), z)
				/ sqrt(config.snap_rho_200m[i] / rhoCrit(&(config.cosmo), z));
	}

	for (i = 0; i < config.n_snaps - 1; i++)
		config.snap_delta_t[i] = config.snap_t[i + 1] - config.snap_t[i];

	/*
	 * Converts velocities in km/s to the internal units of kpc/h/Gyr.
	 */
	config.v_conversion = config.h / AST_kpc * 1E14 * AST_year;
}

void deriveParameters()
{
	int rs;

	setMemoryFactors();

	config.jump_threshold = fminf(config.cat_halo_jump_tol_phys,
			config.box_size * config.cat_halo_jump_tol_box);
	config.reliable_mass_R200m = config.particle_mass * config.cat_reliable_n200m;
	config.min_save_M200m = config.particle_mass * config.output_min_n200m;

	/*
	 * Compute whether any active results might want to re-created ignored tracers. If not, that
	 * function never needs to be executed which will save time.
	 */
	config.mightRecreateTracers = 0;
	for (rs = 0; rs < NRS; rs++)
	{
		if (rsprops[rs].checkRecreateIgnoredTracer != NULL)
			config.mightRecreateTracers = 1;
	}

	// Bins for interpolated mass profile
#if DO_MASS_PROFILE
	int i_mp;
	double log_min, log_bin_width;

	log_min = log10(config.halo_min_radius_mass_profile);
	log_bin_width = (log10(config.tcr_ptl_delete_radius) - log_min) / (double) (N_MBINS - 1);
	for (i_mp = 0; i_mp < N_MBINS; i_mp++)
	{
		config.r_bins_log[i_mp] = log_min + i_mp * log_bin_width;
		config.r_bins_lin[i_mp] = pow(10.0, (float) config.r_bins_log[i_mp]);
	}
#endif
}

void createDirectories()
{
	char tmpstr[300];

	createPath(config.output_path, 0);
	sprintf(tmpstr, "%s/snapboxes", config.output_path);
	createPath(tmpstr, 0);

	if (config.output_restart_files)
	{
		sprintf(tmpstr, "%s/restart", config.output_path);
		createPath(tmpstr, 0);
	}
}

void checkConfigDerived()
{
	int tt, rs;

	// Input (Catalogs)
	assert(config.cat_halo_jump_tol_phys > 0.0);
	assert(config.cat_halo_jump_tol_box > 0.0);

	// File output
	assert(config.output_min_n200m >= 0);
	for (tt = 0; tt < NTT; tt++)
		for (rs = 0; rs < NRS; rs++)
			assertBool(config.output_min_results[tt][rs]);

	// Instructions
	assert(config.last_snap >= -1);
	for (tt = 0; tt < NTT; tt++)
		for (rs = 0; rs < NRS; rs++)
			assertBool(config.all_instructions[tt][rs]);

	// Derived and hard-coded
	assert(config.box_size > 0.0);
	assert(config.reliable_mass_R200m > 0.0);
	assert(config.n_files_per_snap > 0);

	// Halos
	assert(config.halo_min_radius_mass_profile >= 0.0);
	assertBool(config.halo_correct_phantoms);
	assert(config.ghost_min_n200m > 0);
	assert(config.ghost_min_host_distance > 0.0);
	assert(config.ghost_max_host_center_time > 0.0);
}

void printConfig()
{
	int i, tt, rs;
	HaloInstruction *hi;
	char **cat_field_names;

	cat_field_names = getCatFieldNames();

	output(0, "[Main] RUN-TIME CONFIGURATION\n");
	printLine(0);

	output(0, "[Main] Input (halo catalogs)\n");
	output(0, "[Main]     catalog_path                  %s\n", config.cat_path);
	output(0, "[Main]     cat_type                      %d\n", config.cat_type);
	output(0, "[Main]     cat_n_chunks                  %d\n", config.cat_n_chunks);
	for (i = 0; i < config.cat_fields_n; i++)
		output(0, "[Main]     catalog field %-15s %s\n", cat_field_names[i], config.cat_fields[i]);
	output(0, "[Main]     cat_tolerance_a_snap          %.3f\n", config.cat_tolerance_a_snap);
	output(0, "[Main]     cat_reliable_n200m            %d\n", config.cat_reliable_n200m);
	output(0, "[Main]     cat_halo_jump_tol_phys        %.3f\n", config.cat_halo_jump_tol_phys);
	output(0, "[Main]     cat_halo_jump_tol_box         %.3f\n", config.cat_halo_jump_tol_box);

	output(0, "[Main] Input (snapshots)\n");
	output(0, "[Main]     snapshot_path                 %s\n", config.snap_path);
	output(0, "[Main]     snap_sim_type                 %d\n", config.snap_sim_type);
	output(0, "[Main]     snap_max_read_procs           %d\n", config.snap_max_read_procs);
	output(0, "[Main]     snap_read_on_main             %d\n", config.snap_read_on_main);
	output(0, "[Main]     snap_max_waiting_messages     %d\n", config.snap_max_waiting_messages);

	output(0, "[Main] Input (simulation)\n");
	output(0, "[Main]     sim_force_res                 %.3f\n", config.sim_force_res);
	output(0, "[Main]     sim_force_res_comoving        %d\n", config.sim_force_res_comoving);

	output(0, "[Main] File output\n");
	output(0, "[Main]     output_path                   %s\n", config.output_path);
	output(0, "[Main]     output_file                   %s\n", config.output_file);
	output(0, "[Main]     output_compression_level      %d\n", config.output_compression_level);
	output(0, "[Main]     output_restart_files          %d\n", config.output_restart_files);
	output(0, "[Main]     output_restart_every          %d\n", config.output_restart_every);
	output(0, "[Main]     output_min_n200m              %d\n", config.output_min_n200m);
	for (tt = 0; tt < NTT; tt++)
		for (rs = 0; rs < NRS; rs++)
			output(0, "[Main]     output_min_results_%s_%s    %d\n", ttprops[tt].name_short,
					rsprops[rs].name_short, config.output_min_results[tt][rs]);

	output(0, "[Main] Logging output\n");
	output(0, "[Main]     log_level                     %d\n", config.log_level);
	output(0, "[Main]     log_level_memory              %d\n", config.log_level_memory);
	output(0, "[Main]     log_level_timing              %d\n", config.log_level_timing);
	output(0, "[Main]     log_flush                     %d\n", config.log_flush);

	output(0, "[Main] Error levels\n");
	output(0, "[Main]     err_level_cannot_fork         %d\n", config.err_level_cannot_fork);
	output(0, "[Main]     err_level_file_copy_failed    %d\n", config.err_level_file_copy_failed);
	output(0, "[Main]     err_level_cat_snap_mismatch   %d\n", config.err_level_cat_snap_mismatch);
	output(0, "[Main]     err_level_skipping_snap       %d\n", config.err_level_skipping_snap);
	output(0, "[Main]     err_level_rockstar_filename   %d\n", config.err_level_rockstar_filename);
	output(0, "[Main]     err_level_invalid_domain_box  %d\n", config.err_level_invalid_domain_box);
	output(0, "[Main]     err_level_domain_minor        %d\n", config.err_level_domain_minor);
	output(0, "[Main]     err_level_halo_req_not_found  %d\n", config.err_level_halo_req_not_found);
	output(0, "[Main]     err_level_desc_not_found      %d\n", config.err_level_desc_not_found);
	output(0, "[Main]     err_level_found_unexpected    %d\n", config.err_level_found_unexpected);
	output(0, "[Main]     err_level_halo_jump           %d\n", config.err_level_halo_jump);
	output(0, "[Main]     err_level_missing_halo_ptl    %d\n", config.err_level_missing_halo_ptl);
	output(0, "[Main]     err_level_cat_radius_diff     %d\n", config.err_level_cat_radius_diff);
	output(0, "[Main]     err_level_zero_ptl            %d\n", config.err_level_zero_ptl);
	output(0, "[Main]     err_level_zero_ptl_phantom    %d\n", config.err_level_zero_ptl_phantom);
	output(0, "[Main]     err_level_radius_outside_box  %d\n", config.err_level_radius_outside_box);
	output(0, "[Main]     err_level_radius_not_found    %d\n", config.err_level_radius_not_found);
	output(0, "[Main]     err_level_radius_bound_diff   %d\n", config.err_level_radius_bound_diff);
	output(0, "[Main]     err_level_search_radius       %d\n", config.err_level_search_radius);
	output(0, "[Main]     err_level_exc_host_ended      %d\n", config.err_level_exc_host_ended);
	output(0, "[Main]     err_level_exc_halo_not_found  %d\n", config.err_level_exc_halo_not_found);
	output(0, "[Main]     err_level_max_exchange_size   %d\n", config.err_level_max_exchange_size);
	output(0, "[Main]     err_level_large_halo          %d\n", config.err_level_large_halo);

	output(0, "[Main] Instructions\n");
	output(0, "[Main]     last_snap                     %d\n", config.last_snap);
	output(0, "[Main]     last_scale_factor             %.3f\n", config.last_scale_factor);
	for (tt = 0; tt < NTT; tt++)
		for (rs = 0; rs < NRS; rs++)
			output(0, "[Main]     doall_%s_%s                 %d\n", ttprops[tt].name_short,
					rsprops[rs].name_short, config.all_instructions[tt][rs]);
	for (i = 0; i < config.n_halo_instructions; i++)
	{
		hi = &(config.halo_instructions[i]);
		if (hi->rs == -1)
		{
			output(0, "[Main]     dohalo_%3s                    %d, ID %ld\n",
					ttprops[hi->tt].name_short, hi->do_tt_rs, hi->id);
		} else
		{
			output(0, "[Main]     dohalo_%3s_%3s                %d, ID %ld\n",
					ttprops[hi->tt].name_short, rsprops[hi->rs].name_short, hi->do_tt_rs, hi->id);
		}
	}

	output(0, "[Main] Domain and exchange\n");
	output(0, "[Main]     lb_do_load_balancing          %d\n", config.lb_do_load_balancing);
	output(0, "[Main]     lb_adjust_min_snap            %d\n", config.lb_adjust_min_snap);
	output(0, "[Main]     lb_slabs_adjust_factor        %.3f\n", config.lb_slabs_adjust_factor);
	output(0, "[Main]     lb_slabs_adjust_max           %.3f\n", config.lb_slabs_adjust_max);
	output(0, "[Main]     lb_slabs_drift_tolerance      %.3f\n", config.lb_slabs_drift_tolerance);
	output(0, "[Main]     exc_max_message_size          %.1f MB\n", config.exc_max_message_size);

	output(0, "[Main] Other parameters\n");
	output(0, "[Main]     random_seed                   %d\n", config.random_seed);
	output(0, "[Main]     tree_points_per_leaf          %d\n", config.tree_points_per_leaf);
	output(0, "[Main]     potential_points_per_leaf     %d\n", config.potential_points_per_leaf);
	output(0, "[Main]     potential_err_tol             %.5f\n", config.potential_err_tol);
	output(0, "[Main]     memory_allocation_factor      %.2f\n", config.memory_allocation_factor);
	output(0, "[Main]     memory_dealloc_safety_factor  %.2f\n",
			config.memory_dealloc_safety_factor);
	output(0, "[Main]     memory_max_halo_size          %.2f MB\n", config.memory_max_halo_size);

	output(0, "[Main] Halos\n");
	output(0, "[Main]     halo_min_radius_mass_profile  %.3f\n",
			config.halo_min_radius_mass_profile);
	output(0, "[Main]     halo_max_radius_ratio_cat     %.2f\n", config.halo_max_radius_ratio_cat);
	output(0, "[Main]     halo_correct_phantoms         %d\n", config.halo_correct_phantoms);
#if DO_GHOSTS
	output(0, "[Main]     ghost_min_n200m               %d\n", config.ghost_min_n200m);
	output(0, "[Main]     ghost_min_host_distance       %.3f\n", config.ghost_min_host_distance);
	output(0, "[Main]     ghost_max_host_center_time    %.3f\n", config.ghost_max_host_center_time);
#endif

#if DO_TRACER_ANY
	output(0, "[Main] Tracer particles\n");
	output(0, "[Main]     tcr_ptl_create_radius         %.3f\n", config.tcr_ptl_create_radius);
	output(0, "[Main]     tcr_ptl_delete_radius         %.3f\n", config.tcr_ptl_delete_radius);
	output(0, "[Main]     tcr_ptl_subtag_radius         %.3f\n", config.tcr_ptl_subtag_radius);
	output(0, "[Main]     tcr_ptl_subtag_inclusive      %d\n", config.tcr_ptl_subtag_inclusive);
	output(0, "[Main]     tcr_ptl_subtag_ifl_age_min    %.3f\n", config.tcr_ptl_subtag_ifl_age_min);
	output(0, "[Main]     tcr_ptl_subtag_ifl_dist_min   %.3f\n",
			config.tcr_ptl_subtag_ifl_dist_min);
	output(0, "[Main]     tcr_ptl_subtag_bound_radius   %.3f\n",
			config.tcr_ptl_subtag_bound_radius);
	output(0, "[Main]     tcr_ptl_subtag_bound_ratio    %.3f\n", config.tcr_ptl_subtag_bound_ratio);
	output(0, "[Main]     tcr_ptl_subtag_host_max_age   %.3f\n",
			config.tcr_ptl_subtag_host_max_age);
#endif

	initConfigResults(CONFIG_INIT_STEP_PRINT);
	initConfigAnalyses(CONFIG_INIT_STEP_PRINT);

	output(0, "[Main] Derived parameters:\n");
	output(0, "[Main]     jump_threshold                %.3f\n", config.jump_threshold);
	output(0, "[Main]     reliable_mass_R200m           %.3e\n", config.reliable_mass_R200m);
	output(0, "[Main]     min_save_M200m                %.3e\n", config.min_save_M200m);
	output(0, "[Main]     memory_dealloc_factor         %.3f\n", config.memory_dealloc_factor);

	output(0, "[Main] Taken from snapshots and catalogs:\n");
	output(0, "[Main]     n_snaps                       %d\n", config.n_snaps);
	output(0, "[Main]     lowest a                      %.4f\n", config.snap_a[0]);
	output(0, "[Main]     highest a                     %.4f\n", config.snap_a[config.n_snaps - 1]);
	output(0, "[Main]     box_size                      %.4f\n", config.box_size);
	output(0, "[Main]     n_files_per_snap              %d\n", config.n_files_per_snap);
	output(0, "[Main]     n_particles                   %d\n", config.n_particles);
	output(0, "[Main]     particle_mass                 %.3e\n", config.particle_mass);
	output(0, "[Main]     min_save_M200m                %.3e\n", config.min_save_M200m);
	output(0, "[Main]     Omega_m                       %.3f\n", config.Omega_m);
	output(0, "[Main]     Omega_L                       %.3f\n", config.Omega_L);
	output(0, "[Main]     h                             %.3f\n", config.h);

	printLine(0);
}

void printCompiledConfig()
{
#define PRINT_SETTING(x) output(0, "[Main]     %-45s %6d\n", #x, x);

	output(0, "[Main] COMPILER SETTINGS\n");
	printLine(0);

	output(0, "[Main] User-defined output settings\n");

	PRINT_SETTING(OUTPUT_GHOSTS)
	PRINT_SETTING(OUTPUT_HALO_X)
	PRINT_SETTING(OUTPUT_HALO_V)
	PRINT_SETTING(OUTPUT_HALO_PARENT_ID)

	PRINT_SETTING(OUTPUT_ANALYSIS_RSP)
	PRINT_SETTING(OUTPUT_ANALYSIS_PROFILES)
	PRINT_SETTING(OUTPUT_ANALYSIS_PROFILES_ALL)
	PRINT_SETTING(OUTPUT_ANALYSIS_PROFILES_1HALO)
	PRINT_SETTING(OUTPUT_ANALYSIS_HALOPROPS)
	PRINT_SETTING(OUTPUT_ANALYSIS_HALOPROPS_RM)

	PRINT_SETTING(OUTPUT_TRACER_PARTICLES)
	PRINT_SETTING(OUTPUT_TRACER_SUBHALOS)

	PRINT_SETTING(OUTPUT_RESULT_INFALL)
	PRINT_SETTING(OUTPUT_RESULT_INFALL_TIME)
	PRINT_SETTING(OUTPUT_RESULT_INFALL_BORNINHALO)
	PRINT_SETTING(OUTPUT_RESULT_INFALL_SMR)
	PRINT_SETTING(OUTPUT_RESULT_INFALL_VRV200)
	PRINT_SETTING(OUTPUT_RESULT_INFALL_VTV200)
	PRINT_SETTING(OUTPUT_RESULT_INFALL_X)

	PRINT_SETTING(OUTPUT_RESULT_SPLASHBACK)
	PRINT_SETTING(OUTPUT_RESULT_SPLASHBACK_MSP)
	PRINT_SETTING(OUTPUT_RESULT_SPLASHBACK_RRM)
	PRINT_SETTING(OUTPUT_RESULT_SPLASHBACK_POS)

	PRINT_SETTING(OUTPUT_RESULT_TRAJECTORY)
	PRINT_SETTING(OUTPUT_RESULT_TRAJECTORY_R)
	PRINT_SETTING(OUTPUT_RESULT_TRAJECTORY_VR)
	PRINT_SETTING(OUTPUT_RESULT_TRAJECTORY_VT)
	PRINT_SETTING(OUTPUT_RESULT_TRAJECTORY_X)
	PRINT_SETTING(OUTPUT_RESULT_TRAJECTORY_V)

	PRINT_SETTING(OUTPUT_RESULT_ORBITCOUNT)
	PRINT_SETTING(OUTPUT_RESULT_ORBITCOUNT_OCT)
	PRINT_SETTING(OUTPUT_RESULT_ORBITCOUNT_LOWER_LIMIT)

	output(0, "[Main] Other user-defined settings\n");

	PRINT_SETTING(ANALYSIS_RSP_MAX_SNAPS)
	PRINT_SETTING(ANALYSIS_RSP_MAX_DEFINITIONS)

	PRINT_SETTING(ANALYSIS_PROFILES_MAX_SNAPS)
	PRINT_SETTING(ANALYSIS_PROFILES_N_BINS)

	PRINT_SETTING(ANALYSIS_HALOPROPS_MAX_SNAPS)
	PRINT_SETTING(ANALYSIS_HALOPROPS_MAX_DEFINITIONS)

	PRINT_SETTING(SUBTAG_METHOD_IFL_AGE)
	PRINT_SETTING(SUBTAG_METHOD_IFL_DISTANCE)
	PRINT_SETTING(SUBTAG_METHOD_BOUND)

	PRINT_SETTING(CAREFUL)
	PRINT_SETTING(PARANOID)
	PRINT_SETTING(MAX_SNAPS)
	PRINT_SETTING(MAX_PROCS)
	PRINT_SETTING(N_MBINS)
	PRINT_SETTING(DOMAIN_DECOMPOSITION)
	PRINT_SETTING(DOMAIN_SFC_INDEX_TABLE)

	output(0, "[Main] Actions to be performed\n");

	PRINT_SETTING(DO_ANALYSIS_ANY)

	PRINT_SETTING(DO_ANALYSIS_RSP)

	PRINT_SETTING(DO_ANALYSIS_PROFILES)
	PRINT_SETTING(DO_ANALYSIS_PROFILES_ALL)
	PRINT_SETTING(DO_ANALYSIS_PROFILES_1HALO)

	PRINT_SETTING(DO_ANALYSIS_HALOPROPS)
	PRINT_SETTING(DO_ANALYSIS_HALOPROPS_RM)
	PRINT_SETTING(DO_ANALYSIS_HALOPROPS_ORBITING)

	PRINT_SETTING(DO_RESULT_ANY)

	PRINT_SETTING(DO_RESULT_INFALL)
	PRINT_SETTING(DO_RESULT_INFALL_SMR)
	PRINT_SETTING(DO_RESULT_INFALL_VRV200)
	PRINT_SETTING(DO_RESULT_INFALL_VTV200)
	PRINT_SETTING(DO_RESULT_INFALL_X)

	PRINT_SETTING(DO_RESULT_SPLASHBACK)
	PRINT_SETTING(DO_RESULT_SPLASHBACK_MSP)
	PRINT_SETTING(DO_RESULT_SPLASHBACK_RRM)
	PRINT_SETTING(DO_RESULT_SPLASHBACK_POS)

	PRINT_SETTING(DO_RESULT_TRAJECTORY)
	PRINT_SETTING(DO_RESULT_TRAJECTORY_R)
	PRINT_SETTING(DO_RESULT_TRAJECTORY_VR)
	PRINT_SETTING(DO_RESULT_TRAJECTORY_VT)
	PRINT_SETTING(DO_RESULT_TRAJECTORY_X)
	PRINT_SETTING(DO_RESULT_TRAJECTORY_V)

	PRINT_SETTING(DO_RESULT_ORBITCOUNT)

	PRINT_SETTING(DO_TRACER_ANY)
	PRINT_SETTING(DO_TRACER_PARTICLES)
	PRINT_SETTING(DO_TRACER_SUBHALOS)

	PRINT_SETTING(DO_TRACER_POS)
	PRINT_SETTING(DO_TRACER_INITIAL_ANGLE)
	PRINT_SETTING(DO_TRACER_VT)
	PRINT_SETTING(DO_TRACER_X)
	PRINT_SETTING(DO_TRACER_V)
	PRINT_SETTING(DO_TRACER_SMR)

	PRINT_SETTING(DO_MASS_PROFILE)
	PRINT_SETTING(DO_READ_PARTICLES)

	PRINT_SETTING(DO_GHOSTS)
	PRINT_SETTING(DO_SUBSUBS)
	PRINT_SETTING(DO_SUBHALO_TRACKING)
	PRINT_SETTING(DO_SUBHALO_TAGGING)
	PRINT_SETTING(DO_SUBHALO_TAGGING_IFL_AGE)
	PRINT_SETTING(DO_SUBHALO_TAGGING_IFL_DISTANCE)
	PRINT_SETTING(DO_SUBHALO_TAGGING_BOUND)

	output(0, "[Main] Other derived settings\n");

	PRINT_SETTING(DOMAIN_DECOMP_SLABS)
	PRINT_SETTING(DOMAIN_DECOMP_SFC)

	PRINT_SETTING(STCL_TCR)
	PRINT_SETTING(STCL_HALO_X)
	PRINT_SETTING(STCL_HALO_V)

#undef PRINT_SETTING

	printLine(0);
}

/*************************************************************************************************
 * PUBLIC UTILITY FUNCTIONS
 *************************************************************************************************/

/*
 * Set global convenience variables, mostly to avoid repetitive calculations and indexing.
 */
void setSnapshotVariables(int snap_idx)
{
	config.this_snap_a = config.snap_a[snap_idx];
	config.this_snap_z = config.snap_z[snap_idx];
	config.this_snap_t = config.snap_t[snap_idx];
	config.this_snap_rho_200m = config.snap_rho_200m[snap_idx];
	config.this_snap_phys_conv = config.snap_phys_conv[snap_idx];
	config.this_snap_com_conv = config.snap_com_conv[snap_idx];

	/*
	 * Set force resolution. This parameter may or may not be set. Note that for resolution is in
	 * physical kpc/h, so we need to convert from a comoving value if sim_force_res_comoving.
	 */
	if (config.sim_force_res < 0.0)
	{
		config.this_snap_force_res = -1.0;
	} else
	{
		if (config.sim_force_res_comoving)
			config.this_snap_force_res = config.sim_force_res * config.this_snap_a;
		else
			config.this_snap_force_res = config.sim_force_res;
	}
}

/*
 * Replace the keywords <snap> and <chunk> in a path.
 */
void getFilePathSnapChunk(const char *path, int snap_dir_idx, int chunk_idx, char *str)
{
	int i, j, k;
	char tmpstr[100], tmpstr2[100];

	i = 0;
	j = 0;
	while (i < strlen(path))
	{
		if (path[i] != '<')
		{
			str[j] = path[i];
			i++;
			j++;

		} else if (strncmp(path + i, "<snap:", 6) == 0)
		{
			i += 6;
			k = i + 1;
			while ((k < strlen(path)) && (path[k] != '>'))
			{
				if (path[k] == '<')
					error(__FFL__,
							"Found '<snap:' in snapshot path, and then opening '<' before closing '>' (at position %d).\n",
							k);
				k++;
			}
			if (k == strlen(path))
				error(__FFL__, "Found '<snap:' in snapshot path, but no closing '>'.\n");
			strncpy(tmpstr, path + i, k - i);
			tmpstr[k - i] = '\0';
			sprintf(tmpstr2, tmpstr, snap_dir_idx);
			sprintf(str + j, tmpstr2, strlen(tmpstr2));
			j += strlen(tmpstr2);
			i += (k - i + 1);

		} else if (strncmp(path + i, "<chunk:", 7) == 0)
		{
			i += 7;
			k = i + 1;
			while ((k < strlen(path)) && (path[k] != '>'))
			{
				if (path[k] == '<')
					error(__FFL__,
							"Found '<chunk:' in snapshot path, and then opening '<' before closing '>' (at position %d).\n",
							k);
				k++;
			}
			if (k == strlen(path))
				error(__FFL__, "Found '<chunk:' in snapshot path, but no closing '>'.\n");
			strncpy(tmpstr, path + i, k - i);
			tmpstr[k - i] = '\0';
			sprintf(tmpstr2, tmpstr, chunk_idx);
			sprintf(str + j, tmpstr2, strlen(tmpstr2));
			j += strlen(tmpstr2);
			i += (k - i + 1);
		}
	}

	str[j] = '\0';
}

/*************************************************************************************************
 * PRIVATE UTILITY FUNCTIONS
 *************************************************************************************************/

void str_tt_rs(char *key, int *tt, int *rs)
{
	int itt, irs;
	char key_cpy[CONFIG_LINE_LENGTH], *cp, *keyp;
	const char *delimeter = "_";

	*tt = -1;
	*rs = -1;
	strcpy(key_cpy, key);
	keyp = key_cpy;

	cp = strsep(&keyp, delimeter);
	cp = strsep(&keyp, delimeter);
	for (itt = 0; itt < NTT; itt++)
	{
		if (strcmp(cp, ttprops[itt].name_short) == 0)
			*tt = itt;
	}
	if (*tt == -1)
		error(__FFL__, "In config key %s, the tracer type '%s' could not be found.\n", key, cp);
	cp = strsep(&keyp, delimeter);
	if (cp == NULL)
		return;
	for (irs = 0; irs < NRS; irs++)
	{
		if (strcmp(cp, rsprops[irs].name_short) == 0)
			*rs = irs;
	}
	if (*rs == -1)
		error(__FFL__, "In config key %s, the result type '%s' could not be found.\n", key, cp);
}

int str_catfield(char *key)
{
	int i;
	char key_cpy[CONFIG_LINE_LENGTH], *cp, *keyp;
	const char *delimeter = "_";
	char **cat_field_names;

	cat_field_names = getCatFieldNames();

	strcpy(key_cpy, key);
	keyp = key_cpy;

	cp = strsep(&keyp, delimeter);
	cp = strsep(&keyp, " ");

	for (i = 0; i < config.cat_fields_n; i++)
		if (strcmp(cp, cat_field_names[i]) == 0)
			return i;

	error(__FFL__, "In config key %s, the catalog field '%s' could not be found.\n", key, cp);

	return EXIT_FAILURE;
}

void setMemoryFactors()
{
	config.memory_alloc_fac_inv = 1.0 / config.memory_allocation_factor;
	config.memory_dealloc_factor = config.memory_alloc_fac_inv
			* config.memory_dealloc_safety_factor;
}
