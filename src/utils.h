/*************************************************************************************************
 *
 * This unit implements various utility functions.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>
#include <sys/stat.h>

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

extern const mode_t DEFAULT_DIR_PERMISSIONS;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

int compareTracers(const void *a, const void *b);
int compareHalos(const void *a, const void *b);
int compareSubhalos(const void *a, const void *b);
int compareHaloRequests(const void *a, const void *b);
int compareTracerRequests(const void *a, const void *b);
int compareResults(const void *a, const void *b);
int compareHaloCatalogDataByProc(const void *a, const void *b);
int compareHaloCatalogDataByID(const void *a, const void *b);
int compareIntegersAscending(const void *a, const void *b);
int compareIntegersDescending(const void *a, const void *b);
int compareLongsAscending(const void *a, const void *b);
int compareLongsDescending(const void *a, const void *b);
int compareFloatsAscending(const void *a, const void *b);
int compareFloatsDescending(const void *a, const void *b);

int imin(int a, int b);
int imax(int a, int b);
int closestInArray(float *a, int n, float x);
void linearIndices(float *indices, int N, float min, float max);
void linearIndicesDouble(double *indices, int N, double min, double max);
void logIndices(float *indices, int N, float min, float max);
void logIndicesDouble(double *indices, int N, double min, double max);
void randomIntegers(int min, int max, int n, int *out);

int stringStartsWith(const char *str, const char *start);
int stringEndsWith(const char *str, const char *end);
int numberInString(char *str, int number_start, int number_end);
int stringToListInt(char *str, int *values, int n_max, int allow_duplicates);
int stringToListLong(char *str, long int *values, int n_max, int allow_duplicates);
int stringToListFloat(char *str, float *values, int n_max, int allow_duplicates);
int stringToListString(char *str, char *values, int n_max, int allow_duplicates, int str_len);

void createPathPermissions(char *path, mode_t permissions, int is_file_path);
void createPath(char *path, int is_file_path);

void assertBool(int b);

#endif
