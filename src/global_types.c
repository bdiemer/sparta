/*************************************************************************************************
 *
 * This unit implements the data structures used throughout SPARTA.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stddef.h>

#include "global_types.h"
#include "config.h"
#include "memory.h"
#include "utils.h"
#include "geometry.h"
#include "halos/halo.h"
#include "halos/halo_subs.h"
#include "analyses/analyses.h"
#include "analyses/analysis_rsp.h"
#include "analyses/analysis_profiles.h"
#include "analyses/analysis_haloprops.h"
#include "results/result_infall.h"
#include "results/result_splashback.h"
#include "results/result_trajectory.h"
#include "results/result_orbitcount.h"
#include "results/results.h"
#include "tracers/tracers.h"

/*************************************************************************************************
 * GLOBAL CONSTANTS
 *************************************************************************************************/

/*
 * This set of properties sets the logic for which results are pursued for which tracers,
 * depending on the circumstances under which they are born. There are a number of possible
 * circumstances:
 *
 * INSTR_OUT             Born outside the halo (should be the most typical case for particles).
 *                       Note that this status is not a 100% guarantee that the tracer has never
 *                       been inside the halo. For example, this instruction includes cases where
 *                       the tracer had been part of the halo before it became a subhalo and is now
 *                       part of a re-born halo.
 * INSTR_IN              Born inside the halo (i.e. its infall was not tracked; typical for
 *                       subhalo tracers)
 * INSTR_IN_NEW          Inside a halo that was just born; we cannot tell when the particle
 *                       actually joined the halo
 * INSTR_IN_REBORN       Inside a halo that was just re-born (i.e. became host after being sub)
 * INSTR_SUBTRACK_NEW    Created when the halo became a subhalo to keep track of sub particles
 * INSTR_SUBTRACK_CONTD  Changed to a subhalo track when halo became a subhalo
 * INSTR_GHOST           Halo became a ghost
 * INSTR_HAS_RES         A previous result of this type has been found. Some results may only ever
 *                       want to create one result, and will abort in this case.
 * INSTR_TCR_RECREATED   The tracer has been recreated, i.e., it had previously existed but was
 *                       deleted.
 *
 * If the instruction for INSTR_HAS_RES is RS_STATUS_SUCCESS, as in, if there already is a result,
 * this result is final, then that is the instruction given. If RS_STATUS_SEEKING is given, the
 * previously determined instruction (based on whether inside/outside halo etc) is used. This means
 * that finding an old result can, for example, lead to the instruction specified for INSTR_IN_NEW,
 * even if the result was meant to be finished. The logic for each of the event types is:
 *
 * Infall:       Seeking if born inside or outside halo, success if previous found. Tracers in
 *               subhalos are also considered because they do not necessarily have an infall event
 *               yet when they are determined to belong to the subhalo. Thus, we maintain the
 *               current status.
 * Splashback:   Seeking whether born inside or outside halo, but not if in born / re-born halo
 *               since the particle might have orbited already. Not considered in subhalos. If a
 *               tracer is recreated, do not attempt splashback.
 * Trajectory:   Always seeking, even in subhalos and recreated tracers.
 * OrbitCount:   Always seeking, even in subhalos and recreated tracers. Note that when a subhalo
 *               is born with INSTR_IN, we set a SEEKING status. This occurs because the actual
 *               crossing cannot be tracked and does not mean that the subhalo has previously
 *               orbited.
 */
const TracerTypeProperties ttprops[NTT] =
	{
#if DO_TRACER_PARTICLES
				{
				// Long name
						"particles",
						// Short name
						"ptl",
						// Do output for this tracer at all?
						OUTPUT_TRACER_PARTICLES,
						// Do output for which results?
							{
#if DO_RESULT_INFALL
									(OUTPUT_TRACER_PARTICLES && OUTPUT_RESULT_INFALL),
#endif
#if DO_RESULT_SPLASHBACK
									(OUTPUT_TRACER_PARTICLES && OUTPUT_RESULT_SPLASHBACK),
#endif
#if DO_RESULT_TRAJECTORY
									(OUTPUT_TRACER_PARTICLES && OUTPUT_RESULT_TRAJECTORY),
#endif
#if DO_RESULT_ORBITCOUNT
									(OUTPUT_TRACER_PARTICLES && OUTPUT_RESULT_ORBITCOUNT),
#endif
						},
						{
#if DO_RESULT_INFALL
									{
									// Instructions
									// INSTR_OUT
											RS_STATUS_SEEKING,
											// INSTR_IN
											RS_STATUS_SEEKING,
											// INSTR_IN_NEW
											RS_STATUS_SEEKING,
											// INSTR_IN_REBORN
											RS_STATUS_SEEKING,
											// INSTR_SUBTRACK_NEW
											RS_STATUS_SEEKING,
											// INSTR_SUBTRACK_CONTD
											RS_STATUS_MAINTAIN,
											// INSTR_GHOST
											RS_STATUS_NOT_SEEKING,
											// INSTR_HAS_RES
											RS_STATUS_SUCCESS,
											// INSTR_TCR_RECREATED
											RS_STATUS_SEEKING},
#endif
#if DO_RESULT_SPLASHBACK
									{
									// Instructions
									// INSTR_OUT
											RS_STATUS_SEEKING,
											// INSTR_IN
											RS_STATUS_SEEKING,
											// INSTR_IN_NEW
											RS_STATUS_NOT_SEEKING,
											// INSTR_IN_REBORN
											RS_STATUS_NOT_SEEKING,
											// INSTR_SUBTRACK_NEW
											RS_STATUS_NOT_SEEKING,
											// INSTR_SUBTRACK_CONTD
											RS_STATUS_NOT_SEEKING,
											// INSTR_GHOST
											RS_STATUS_NOT_SEEKING,
											// INSTR_HAS_RES
											RS_STATUS_SUCCESS,
											// INSTR_TCR_RECREATED,
											RS_STATUS_NOT_SEEKING},
#endif
#if DO_RESULT_TRAJECTORY
									{
									// Instructions
									// INSTR_OUT
											RS_STATUS_SEEKING,
											// INSTR_IN
											RS_STATUS_SEEKING,
											// INSTR_IN_NEW
											RS_STATUS_SEEKING,
											// INSTR_IN_REBORN
											RS_STATUS_SEEKING,
											// INSTR_SUBTRACK_NEW
											RS_STATUS_SEEKING,
											// INSTR_SUBTRACK_CONTD
											RS_STATUS_MAINTAIN,
											// INSTR_GHOST
											RS_STATUS_MAINTAIN,
											// INSTR_HAS_RES
											RS_STATUS_SEEKING,
											// INSTR_TCR_RECREATED,
											RS_STATUS_SEEKING},
#endif
#if DO_RESULT_ORBITCOUNT
									{
									// Instructions
									// INSTR_OUT
											OCT_STATUS_SEEKING,
											// INSTR_IN
											OCT_STATUS_BORN_IN_HALO,
											// INSTR_IN_NEW
											OCT_STATUS_BORN_IN_HALO,
											// INSTR_IN_REBORN
											OCT_STATUS_BORN_IN_HALO,
											// INSTR_SUBTRACK_NEW
											OCT_STATUS_BORN_IN_HALO,
											// INSTR_SUBTRACK_CONTD
											OCT_STATUS_MAINTAIN,
											// INSTR_GHOST
											OCT_STATUS_NOT_SEEKING,
											// INSTR_HAS_RES
											OCT_STATUS_SEEKING,
											// INSTR_TCR_RECREATED,
											OCT_STATUS_MAINTAIN},
#endif
						}},
#endif
#if DO_TRACER_SUBHALOS
				{
				// Long name
						"subhalos",
						// Short name
						"sho",
						// Do output for this tracer at all?
						OUTPUT_TRACER_SUBHALOS,
						// Do output for which results?
							{
#if DO_RESULT_INFALL
									(OUTPUT_TRACER_SUBHALOS && OUTPUT_RESULT_INFALL),
#endif
#if DO_RESULT_SPLASHBACK
									(OUTPUT_TRACER_SUBHALOS && OUTPUT_RESULT_SPLASHBACK),
#endif
#if DO_RESULT_TRAJECTORY
									(OUTPUT_TRACER_SUBHALOS && OUTPUT_RESULT_TRAJECTORY),
#endif
#if DO_RESULT_ORBITCOUNT
									(OUTPUT_TRACER_SUBHALOS && OUTPUT_RESULT_ORBITCOUNT),
#endif
						},
						{
#if DO_RESULT_INFALL
									{
									// Instructions
									// INSTR_OUT
											RS_STATUS_SEEKING,
											// INSTR_IN
											RS_STATUS_SEEKING,
											// INSTR_IN_NEW
											RS_STATUS_SEEKING,
											// INSTR_IN_REBORN
											RS_STATUS_SEEKING,
											// INSTR_SUBTRACK_NEW
											RS_STATUS_NONE,
											// INSTR_SUBTRACK_CONTD
											RS_STATUS_NONE,
											// INSTR_GHOST
											RS_STATUS_NONE,
											// INSTR_HAS_RES
											RS_STATUS_SUCCESS,
											// INSTR_TCR_RECREATED
											RS_STATUS_NONE},
#endif
#if DO_RESULT_SPLASHBACK
									{
									// Instructions
									// INSTR_OUT
											RS_STATUS_SEEKING,
											// INSTR_IN
											RS_STATUS_SEEKING,
											// INSTR_IN_NEW
											RS_STATUS_NOT_SEEKING,
											// INSTR_IN_REBORN
											RS_STATUS_NOT_SEEKING,
											// INSTR_SUBTRACK_NEW
											RS_STATUS_NONE,
											// INSTR_SUBTRACK_CONTD
											RS_STATUS_NONE,
											// INSTR_GHOST
											RS_STATUS_NONE,
											// INSTR_HAS_RES
											RS_STATUS_SUCCESS,
											// INSTR_TCR_RECREATED
											RS_STATUS_NONE},
#endif
#if DO_RESULT_TRAJECTORY
									{
									// Instructions
									// INSTR_OUT
											RS_STATUS_SEEKING,
											// INSTR_IN
											RS_STATUS_SEEKING,
											// INSTR_IN_NEW
											RS_STATUS_SEEKING,
											// INSTR_IN_REBORN
											RS_STATUS_SEEKING,
											// INSTR_SUBTRACK_NEW
											RS_STATUS_NONE,
											// INSTR_SUBTRACK_CONTD
											RS_STATUS_NONE,
											// INSTR_GHOST
											RS_STATUS_NONE,
											// INSTR_HAS_RES
											RS_STATUS_SEEKING,
											// INSTR_TCR_RECREATED
											RS_STATUS_NONE},
#endif
#if DO_RESULT_ORBITCOUNT
									{
									// Instructions
									// INSTR_OUT
											OCT_STATUS_SEEKING,
											// INSTR_IN
											OCT_STATUS_SEEKING,
											// INSTR_IN_NEW
											OCT_STATUS_BORN_IN_HALO,
											// INSTR_IN_REBORN
											OCT_STATUS_BORN_IN_HALO,
											// INSTR_SUBTRACK_NEW
											OCT_STATUS_NONE,
											// INSTR_SUBTRACK_CONTD
											OCT_STATUS_NONE,
											// INSTR_GHOST
											RS_STATUS_NONE,
											// INSTR_HAS_RES
											OCT_STATUS_SEEKING,
											// INSTR_TCR_RECREATED
											RS_STATUS_NONE},
#endif
						}},
#endif
		};

/*
 * Properties of tracer results (not split by tracer type as above). Note that there are two function
 * hooks: one to run the tracer result function, and one to determine whether a previously ignored
 * tracer should become active again. This second function can be NULL.
 */
const ResultTypeProperties rsprops[NRS] =
	{
#if DO_RESULT_INFALL
				{
				// Long name
						"infall",
						// Short name
						"ifl",
						// Type of DynArray
						DA_TYPE_RSIFL,
						// Size of element
						sizeof(ResultInfall),
						// initConfig()
						NULL,
						// runResult()
						&runResultInfall,
						// checkRecreateIgnoredTracer()
						NULL,
						// outputFields()
						&outputFieldsResultInfall,
						// outputConfig()
						NULL},
#endif
#if DO_RESULT_SPLASHBACK
				{
				// Long name
						"splashback",
						// Short name
						"sbk",
						// Type of DynArray
						DA_TYPE_RSSBK,
						// Size of element
						sizeof(ResultSplashback),
						// initConfig()
						NULL,
						// runResult()
						&runResultSplashback,
						// checkRecreateIgnoredTracer()
						NULL,
						// outputFields()
						&outputFieldsResultSplashback,
						// outputConfig()
						NULL},
#endif
#if DO_RESULT_TRAJECTORY
				{
				// Long name
						"trajectory",
						// Short name
						"tjy",
						// Type of DynArray
						DA_TYPE_RSTJY,
						// Size of element
						sizeof(ResultTrajectory),
						// initConfig()
						NULL,
						// runResult()
						&runResultTrajectory,
						// checkRecreateIgnoredTracer()
						NULL,
						// outputFields()
						&outputFieldsResultTrajectory,
						// outputConfig()
						NULL},
#endif
#if DO_RESULT_ORBITCOUNT
				{
				// Long name
						"orbitcount",
						// Short name
						"oct",
						// Type of DynArray
						DA_TYPE_RSOCT,
						// Size of element
						sizeof(ResultOrbitCount),
						// initConfig()
						&initConfigResultOrbitCount,
						// runResult()
						&runResultOrbitCount,
						// checkRecreateIgnoredTracer()
						&checkRecreateIgnoredTracerOrbitCount,
						// outputFields()
						&outputFieldsResultOrbitCount,
						// outputConfig()
						&outputConfigResultOrbitCount},
#endif
		};

/*
 * Properties of analyses. The first function hook is for the initialization function, there are
 * two function hooks to run the analysis, one while the halo is being analyzed and one afterwards.
 * Those hooks can be NULL.
 */
const AnalysisTypeProperties alprops[NAL] =
	{
#if DO_ANALYSIS_RSP
				{
				// Long name
						"rsp",
						// Short name
						"rsp",
						// Type of DynArray
						DA_TYPE_ALRSP,
						// Size of element
						sizeof(AnalysisRsp),
						// Do output?
						OUTPUT_ANALYSIS_RSP,
						// initConfig()
						&initConfigAnalysisRsp,
						// runAnalysis1()
						NULL,
						// runAnalysis2()
						&runAnalysisRsp,
						// outputFields()
						&outputFieldsAnalysisRsp,
						// outputConfig()
						&outputConfigAnalysisRsp,
						// searchRadius()
						NULL, },
#endif
#if DO_ANALYSIS_PROFILES
				{
				// Long name
						"profiles",
						// Short name
						"prf",
						// Type of DynArray
						DA_TYPE_ALPRF,
						// Size of element
						sizeof(AnalysisProfiles),
						// Do output?
						OUTPUT_ANALYSIS_PROFILES,
						// initConfig()
						&initConfigAnalysisProfiles,
						// runAnalysis1()
						&runAnalysisProfiles,
						// runAnalysis2()
						NULL,
						// outputFields()
						&outputFieldsAnalysisProfiles,
						// outputConfig()
						&outputConfigAnalysisProfiles,
						// searchRadius()
						&searchRadiusAnalsisProfiles, },
#endif
#if DO_ANALYSIS_HALOPROPS
				{
				// Long name
						"haloprops",
						// Short name
						"hps",
						// Type of DynArray
						DA_TYPE_ALHPS,
						// Size of element
						sizeof(AnalysisHaloProps),
						// Do output?
						OUTPUT_ANALYSIS_HALOPROPS,
						// initConfig()
						&initConfigAnalysisHaloprops,
						// runAnalysis1()
						&runAnalysisHaloProps,
						// runAnalysis2()
						NULL,
						// outputFields()
						&outputFieldsAnalysisHaloProps,
						// outputConfig()
						&outputConfigAnalysisHaloProps,
						// searchRadius()
						&searchRadiusAnalysisHaloProps, },
#endif
		};

/*************************************************************************************************
 * INTERNAL CONSTANTS
 *************************************************************************************************/

/*
 * The properties of a type of DynArray
 */
typedef struct DynArrayTypeProperties
{
	int initial_alloc;
	size_t size;
	int memory_id;
} DynArrayTypeProperties;

/*
 * For each type of dynamic array, we define an initial size (which can be overwritten if using the
 * initDynArrayN() function). Note that the following list must follow the same order as the enum in
 * global_types.h.
 */
DynArrayTypeProperties da_props[DA_N_TYPES] =
	{
		{1000, sizeof(int), MID_DA_INT},
		{1000, sizeof(Halo), MID_DA_HALO},
		{1, sizeof(SubPointer), MID_DA_SUBPOINTER},
		{10, sizeof(ID), MID_DA_ID},
		{1000, sizeof(HaloCatalogData), MID_DA_CD},
		{1000, sizeof(TracerRequest), MID_DA_TREQ},
		{10, sizeof(Tracer), MID_DA_TRACER},
		{10, sizeof(ResultInfall), MID_DA_RESIFL},
		{10, sizeof(ResultSplashback), MID_DA_RESSBK},
		{10, sizeof(ResultTrajectory), MID_DA_RESTJY},
		{10, sizeof(ResultOrbitCount), MID_DA_RESOCT},
		{1, sizeof(AnalysisRsp), MID_DA_ALRSP},
		{1, sizeof(AnalysisProfiles), MID_DA_ALPRF},
		{1, sizeof(AnalysisHaloProps), MID_DA_ALHPS}};

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void* addToDynArray(const char *file, const char *func, int line, DynArray *da);

/*************************************************************************************************
 * FUNCTIONS - GENERAL TYPES AND ND ARRAYS
 *************************************************************************************************/

/*
 * Return the size a data type in memory.
 */
size_t datatypeSize(int dtype)
{
	switch (dtype)
	{
	case DTYPE_INT8:
		return sizeof(int_least8_t);
	case DTYPE_INT16:
		return sizeof(short int);
	case DTYPE_INT32:
		return sizeof(int);
	case DTYPE_INT64:
		return sizeof(long int);
	case DTYPE_FLOAT:
		return sizeof(float);
	case DTYPE_DOUBLE:
		return sizeof(double);
	case DTYPE_STRING:
		return sizeof(char);
	default:
		error(__FFL__, "Unknown data type, %d.\n", dtype);
	}

	return DTYPE_INVALID;
}

void initializeNDArray(NDArray *nda)
{
	int i;

	nda->n_dim = 0;
	nda->dtype = DTYPE_INVALID;
	nda->allocated = 0;
	for (i = 0; i < NDARRAY_MAX_DIM; i++)
		nda->n[i] = 0;
	nda->q_bytes = NULL;
	nda->q_bytes_2 = NULL;
}

size_t sizeOfNDArray(NDArray *nda)
{
	int i;
	size_t s;

	s = datatypeSize(nda->dtype);
	for (i = 0; i < nda->n_dim; i++)
		s *= nda->n[i];

	return s;
}

/*
 * Note that this function allows 0 for the dimensionality, in which case no memory is
 * allocated.
 */
void allocateNDArray(const char *file, const char *func, int line, int mem_id, NDArray *nda)
{
	int i, j;
	size_t s, s_alloc;

	if (nda->allocated)
		error(file, func, line, "Found NDArray that is already allocated.\n");

	if (nda->n[0] < 0)
		error(file, func, line, "Found negative dimension %d in NDArray.\n", nda->n[0]);

	if (nda->n[0] == 0)
		return;

	if (nda->n_dim > NDARRAY_MAX_DIM)
		error(__FFL__, "ND Array cannot have dimensionsality greater than %d, found %d.\n",
		NDARRAY_MAX_DIM, nda->n_dim);

	s = datatypeSize(nda->dtype);
	s_alloc = s;
	for (i = 0; i < nda->n_dim; i++)
		s_alloc *= nda->n[i];

	/*
	 * Always allocate the q_bytes pointer (and its union) to the actual, contiguous memory
	 * allocation. If there are higher dimensions, we add pointer arrays for convenience.
	 */
	nda->q_bytes = (char*) memAlloc(file, func, line, mem_id, s_alloc);
	memset(nda->q_bytes, 0, s_alloc);

	if (nda->n_dim == 2)
	{
		nda->q_bytes_2 = (char**) memAlloc(file, func, line, mem_id, nda->n[0] * sizeof(char*));
		for (i = 0; i < nda->n[0]; i++)
			nda->q_bytes_2[i] = nda->q_bytes + i * s * nda->n[1];

	} else if (nda->n_dim == 3)
	{
		nda->q_bytes_3 = (char***) memAlloc(file, func, line, mem_id, nda->n[0] * sizeof(char**));
		for (i = 0; i < nda->n[0]; i++)
		{
			nda->q_bytes_3[i] = (char**) memAlloc(file, func, line, mem_id,
					nda->n[1] * sizeof(char*));
			for (j = 0; j < nda->n[1]; j++)
			{
				nda->q_bytes_3[i][j] = nda->q_bytes + i * s * nda->n[1] * nda->n[2]
						+ j * s * nda->n[2];
			}
		}
	}

	nda->allocated = 1;
}

void freeNDArray(const char *file, const char *func, int line, int mem_id, NDArray *nda)
{
	if (!nda->allocated)
		return;

	if (nda->n_dim > NDARRAY_MAX_DIM)
		error(__FFL__, "ND Array cannot have dimensionsality greater than %d, found %d.\n",
		NDARRAY_MAX_DIM, nda->n_dim);

	int i;
	size_t s, s_alloc;

	s = datatypeSize(nda->dtype);
	s_alloc = s;
	for (i = 0; i < nda->n_dim; i++)
		s_alloc *= nda->n[i];

	/*
	 * Always free the actually allocated memory. If we have higher dimensions, also free the
	 * pointer arrays.
	 */
	memFree(file, func, line, mem_id, nda->q_void, s_alloc);

	if (nda->n_dim == 2)
	{
		memFree(file, func, line, mem_id, nda->q_void_2, nda->n[0] * sizeof(char*));

	} else if (nda->n_dim == 3)
	{
		int i;

		for (i = 0; i < nda->n[0]; i++)
			memFree(file, func, line, mem_id, nda->q_void_3[i], nda->n[1] * sizeof(char*));
		memFree(file, func, line, mem_id, nda->q_void_3, nda->n[0] * sizeof(char**));
	}

	nda->allocated = 0;
	for (i = 0; i < NDARRAY_MAX_DIM; i++)
		nda->n[i] = 0;
	nda->q_bytes = NULL;
	nda->q_bytes_2 = NULL;
}

/*************************************************************************************************
 * FUNCTIONS - OUTPUT
 *************************************************************************************************/

void printTypeSizes()
{
	output(0, "[Main] MEMORY SIZES\n");
	printLine(0);

	output(0, "[Main]     sizeof(HaloCatalogEntry)      %7d bytes\n", sizeof(HaloCatalogData));
	output(0, "[Main]     sizeof(Halo)                  %7d bytes\n", sizeof(Halo));
	output(0, "[Main]     sizeof(SubPointer)            %7d bytes\n", sizeof(SubPointer));
#if DO_READ_PARTICLES
	output(0, "[Main]     sizeof(Particle)              %7d bytes\n", sizeof(Particle));
	output(0, "[Main]     sizeof(TreeNode)              %7d bytes\n", sizeof(TreeNode));
#endif
#if DO_TRACER_ANY
	output(0, "[Main]     sizeof(TracerType)            %7d bytes\n", sizeof(TracerType));
	output(0, "[Main]     sizeof(Tracer)                %7d bytes\n", sizeof(Tracer));
#endif

#if DO_RESULT_INFALL
	output(0, "[Main]     sizeof(ResultInfall)          %7d bytes\n", sizeof(ResultInfall));
#endif
#if DO_RESULT_SPLASHBACK
	output(0, "[Main]     sizeof(ResultSplashback)      %7d bytes\n", sizeof(ResultSplashback));
#endif
#if DO_RESULT_TRAJECTORY
	output(0, "[Main]     sizeof(ResultTrajectory)      %7d bytes\n", sizeof(ResultTrajectory));
#endif
#if DO_RESULT_ORBITCOUNT
	output(0, "[Main]     sizeof(ResultOrbitCount)      %7d bytes\n", sizeof(ResultOrbitCount));
#endif

#if DO_ANALYSIS_RSP
	output(0, "[Main]     sizeof(AnalysisRsp)           %7d bytes\n", sizeof(AnalysisRsp));
#endif
#if DO_ANALYSIS_PROFILES
	output(0, "[Main]     sizeof(AnalysisProfiles)      %7d bytes\n", sizeof(AnalysisProfiles));
#endif
#if DO_ANALYSIS_HALOPROPS
	output(0, "[Main]     sizeof(AnalysisHaloProps)     %7d bytes\n", sizeof(AnalysisHaloProps));
#endif

	printLine(0);
}

/*************************************************************************************************
 * FUNCTIONS - MPI TYPE CREATION
 *************************************************************************************************/

void initMPITypes()
{
	int rs, al;

	/*
	 * Type for box
	 */
	int type_box_counts[4] =
		{1, 3, 3, 3};
	MPI_Aint type_box_offsets[4] =
		{offsetof(Box, ignore), offsetof(Box, periodic), offsetof(Box, min), offsetof(Box, max)};
	const MPI_Datatype type_box_types[4] =
		{MPI_INT, MPI_INT, MPI_FLOAT, MPI_FLOAT};
	MPI_Type_create_struct(4, type_box_counts, type_box_offsets, type_box_types, &mpiTypes.box);
	MPI_Type_commit(&mpiTypes.box);

	/*
	 * Type for particle
	 */
	int type_particle_counts[3] =
		{1, 3, 3};
	MPI_Aint type_particle_offsets[3] =
		{offsetof(Particle, id), offsetof(Particle, x), offsetof(Particle, v)};
	const MPI_Datatype type_particle_types[3] =
		{MPI_LONG, MPI_FLOAT, MPI_FLOAT};
	MPI_Type_create_struct(3, type_particle_counts, type_particle_offsets, type_particle_types,
			&mpiTypes.particle);
	MPI_Type_commit(&mpiTypes.particle);

	/*
	 * Other types which are really just byte arrays
	 */
	MPI_Aint gen_offsets[1] =
		{0};
	const MPI_Datatype gen_types[1] =
		{MPI_BYTE};
	int gen_counts[1] =
		{0};

	// Halo
	gen_counts[0] = sizeof(Halo);
	MPI_Type_create_struct(1, gen_counts, gen_offsets, gen_types, &mpiTypes.halo);
	MPI_Type_commit(&mpiTypes.halo);

	// Sub
	gen_counts[0] = sizeof(SubPointer);
	MPI_Type_create_struct(1, gen_counts, gen_offsets, gen_types, &mpiTypes.sub);
	MPI_Type_commit(&mpiTypes.sub);

	// Tracer
	gen_counts[0] = sizeof(Tracer);
	MPI_Type_create_struct(1, gen_counts, gen_offsets, gen_types, &mpiTypes.tcr);
	MPI_Type_commit(&mpiTypes.tcr);

	// IID
	gen_counts[0] = sizeof(ID);
	MPI_Type_create_struct(1, gen_counts, gen_offsets, gen_types, &mpiTypes.iid);
	MPI_Type_commit(&mpiTypes.iid);

	// Results
	for (rs = 0; rs < NRS; rs++)
	{
		gen_counts[0] = rsprops[rs].size;
		MPI_Type_create_struct(1, gen_counts, gen_offsets, gen_types,
				(MPI_Datatype*) &mpiTypes.rs[rs]);
		MPI_Type_commit(&mpiTypes.rs[rs]);
	}

	// Analyses
	for (al = 0; al < NAL; al++)
	{
		gen_counts[0] = alprops[al].size;
		MPI_Type_create_struct(1, gen_counts, gen_offsets, gen_types,
				(MPI_Datatype*) &mpiTypes.al[al]);
		MPI_Type_commit(&mpiTypes.al[al]);
	}
}

/*************************************************************************************************
 * FUNCTIONS - DYNAMIC ARRAYS
 *************************************************************************************************/

void initDynArray(const char *file, const char *func, int line, DynArray *da, int type)
{
	initDynArrayN(file, func, line, da, type, 0);
}

/*
 * Initialize a dynamic array with a given number of elements. This can be useful when we know
 * roughly how many elements the dyn array should contain. If N is 0, we use the standard
 * initial allocation for the given type.
 *
 * Note that we cannot simply check for data being NULL in this function to ascertain whether
 * the DynArray is already allocated, as this function is used on uninitialized instances of
 * DynArray that have random values.
 */
void initDynArrayN(const char *file, const char *func, int line, DynArray *da, int type, int N)
{
	da->n = 0;
	da->size = da_props[type].size;
	da->type = type;
	if (N > 0)
	{
		da->n_alloc = imax(N, da_props[type].initial_alloc);
		da->data = memAlloc(file, func, line, da_props[type].memory_id,
				(size_t) da->size * da->n_alloc);
	} else
	{
		da->n_alloc = 0;
		da->data = NULL;
	}
}

/*
 * For an already existing and initialized DynArray, this function re-allocates the memory. This
 * is necessary, for example, when sending halos between procs: the DynArray is intact, but the
 * data memory has been lost.
 */
void resetDynArray(const char *file, const char *func, int line, DynArray *da)
{
	if (da->n_alloc == 0)
	{
		da->data = NULL;
	} else
	{
		da->data = memAlloc(file, func, line, da_props[da->type].memory_id,
				(size_t) da->size * da->n_alloc);
	}
}

/*
 * Check whether a DynArray is initialized, i.e., whether any memory is allocated. This check
 * should be performed before initializing an array because otherwise a memory leak could occur.
 */
int dynArrayWasFreed(DynArray *da)
{
	return (da->data == NULL);
}

/*
 * Add an element to a dynamic array. If necessary, increase the memory allocation. Here, we
 * multiply with the allocation factor, but also check that the new number has increased by at
 * least the one element we want to add; for very small allocation factors, that is not
 * guaranteed.
 */
void* addToDynArray(const char *file, const char *func, int line, DynArray *da)
{
	void *new;

	da->n++;
	if (da->n > da->n_alloc)
	{
		int n_alloc_old;

		n_alloc_old = da->n_alloc;
		if (n_alloc_old == 0)
		{
			da->n_alloc = da_props[da->type].initial_alloc;
			da->data = memAlloc(file, func, line, da_props[da->type].memory_id,
					(size_t) da->size * da->n_alloc);
		} else
		{
			da->n_alloc = imax((int) (config.memory_allocation_factor * da->n_alloc), da->n);
			da->data = memRealloc(file, func, line, da_props[da->type].memory_id, da->data,
					(size_t) da->size * da->n_alloc, (size_t) da->size * n_alloc_old);
		}
	}

	new = (void*) (((char*) da->data) + (size_t) da->size * (da->n - 1));

	return new;
}

/*
 * Delete a single item from a dynamic array.
 */
void deleteDynArrayItem(const char *file, const char *func, int line, DynArray *da, int idx)
{
	memmove(&(da->bytes[(size_t ) da->size * idx]), &(da->bytes[(size_t ) da->size * (idx + 1)]),
			(size_t ) da->size * (da->n - idx - 1));
	da->n--;
	checkDynArray(__FFL__, da);
}

/*
 * Delete multiple items from a dynamic array (all those where mask is true).
 *
 * Note that the function moves the items that are not being deleted, not the ones that are.
 */
void deleteDynArrayItems(const char *file, const char *func, int line, DynArray *da,
		int_least8_t *mask)
{
	int i, j;

	j = 0;
	for (i = 0; i < da->n; i++)
	{
		if (!mask[i])
		{
			memmove(&(da->bytes[(size_t ) da->size * j]), &(da->bytes[(size_t ) da->size * i]),
					(size_t ) da->size);
			j++;
		}
	}
	da->n = j;
	checkDynArray(__FFL__, da);
}

/*
 * After a dynamic array has been modified manually, one can call this function to make sure
 * the array can react to deleted elements. There is no "right" way to resize the array, in any
 * case we are making some sort of prediction as to its future development. We consider three
 * criteria:
 *
 * - The array should be reduced by at least 1 / memory_allocation_factor
 * - If n has fallen even more so that it is below that number, we further reduce it to
 *   n_new * memory_allocation_factor
 * - We never down-size to smaller than the initial allocation.
 */
void checkDynArray(const char *file, const char *func, int line, DynArray *da)
{
	if (da->n < (float) da->n_alloc * config.memory_dealloc_factor)
	{
		int new_size, old_size;

		new_size = (int) (da->n_alloc * config.memory_alloc_fac_inv);
		new_size = imin(new_size, (int) (da->n * config.memory_allocation_factor));
		new_size = imax(new_size, da_props[da->type].initial_alloc);

		if (new_size < da->n_alloc)
		{
#if PARANOID
			if (new_size < da->n)
				error(__FFL__, "Trying to down-size dynamic array, but new size too small.\n");
#endif
			old_size = da->n_alloc;
			da->n_alloc = new_size;
			da->data = memRealloc(file, func, line, da_props[da->type].memory_id, da->data,
					(size_t) da->size * new_size, (size_t) da->size * old_size);
		}
	}
}

/*
 * In some cases, a DynArray will not be added to for some time but we need to keep its contents.
 * Thus, we want to compress it to use only the necessary memory, meaning that we resize the array
 * to the number of actually occupied elements.
 *
 * On some compilers, reallocating to a size of zero throws an error, so we treat that case
 * separately.
 */
void compressDynArray(const char *file, const char *func, int line, DynArray *da)
{
	if (da->n_alloc <= da->n)
		return;

	if (da->n == 0)
	{
		freeDynArray(file, func, line, da);
	} else
	{
		da->data = memRealloc(file, func, line, da_props[da->type].memory_id, da->data,
				(size_t) da->size * da->n, (size_t) da->size * da->n_alloc);
		da->n_alloc = da->n;
	}
}

void freeDynArray(const char *file, const char *func, int line, DynArray *da)
{
	memFree(file, func, line, da_props[da->type].memory_id, da->data,
			(size_t) da->size * da->n_alloc);
	da->n = 0;
	da->n_alloc = 0;
	da->data = NULL;
}

int* addInt(const char *file, const char *func, int line, DynArray *da)
{
	int *p;

	p = (int*) addToDynArray(file, func, line, da);
	*p = 0;

	return p;
}

Halo* addHalo(const char *file, const char *func, int line, DynArray *da, int init_dyn_arrays)
{
	Halo *p;

	p = (Halo*) addToDynArray(file, func, line, da);
	initializeHalo(p);
	if (init_dyn_arrays)
		initHaloDynArrays(p);

	return p;
}

SubPointer* addSubPointer(const char *file, const char *func, int line, DynArray *da)
{
	SubPointer *p;

	p = (SubPointer*) addToDynArray(file, func, line, da);
	initSubPointer(p);

	return p;
}

ID* addId(const char *file, const char *func, int line, DynArray *da)
{
	ID *p;

	p = (ID*) addToDynArray(file, func, line, da);
	initId(p);

	return p;
}

Tracer* addTracer(const char *file, const char *func, int line, DynArray *da)
{
	Tracer *p;

	p = (Tracer*) addToDynArray(file, func, line, da);
	initTracer(p);

	return p;
}

HaloCatalogData* addHaloCatalogData(const char *file, const char *func, int line, DynArray *da)
{
	HaloCatalogData *p;

	p = (HaloCatalogData*) addToDynArray(file, func, line, da);
	initHaloCatalogData(p);

	return p;
}

TracerRequest* addTracerRequest(const char *file, const char *func, int line, DynArray *da)
{
	TracerRequest *p;

	p = (TracerRequest*) addToDynArray(file, func, line, da);
	initTracerRequest(p);

	return p;
}

void initId(ID *id)
{
	*id = LARGE_ID;
}

#if DO_RESULT_INFALL
ResultInfall* addResultInfall(const char *file, const char *func, int line, DynArray *da)
{
	ResultInfall *p;

	p = (ResultInfall*) addToDynArray(file, func, line, da);
	initResultInfall(p);

	return p;
}
#endif

#if DO_RESULT_SPLASHBACK
ResultSplashback* addResultSplashback(const char *file, const char *func, int line, DynArray *da)
{
	ResultSplashback *p;

	p = (ResultSplashback*) addToDynArray(file, func, line, da);
	initResultSplashback(p);

	return p;
}
#endif

#if DO_RESULT_TRAJECTORY
ResultTrajectory* addResultTrajectory(const char *file, const char *func, int line, DynArray *da)
{
	ResultTrajectory *p;

	p = (ResultTrajectory*) addToDynArray(file, func, line, da);
	initResultTrajectory(p);

	return p;
}
#endif

#if DO_RESULT_ORBITCOUNT
ResultOrbitCount* addResultOrbitCount(const char *file, const char *func, int line, DynArray *da)
{
	ResultOrbitCount *p;

	p = (ResultOrbitCount*) addToDynArray(file, func, line, da);
	initResultOrbitCount(p);

	return p;
}
#endif

#if DO_ANALYSIS_RSP
AnalysisRsp* addAnalysisRsp(const char *file, const char *func, int line, DynArray *da)
{
	AnalysisRsp *p;

	p = (AnalysisRsp*) addToDynArray(file, func, line, da);
	initAnalysisRsp(p);

	return p;
}
#endif

#if DO_ANALYSIS_PROFILES
AnalysisProfiles* addAnalysisProfiles(const char *file, const char *func, int line, DynArray *da)
{
	AnalysisProfiles *p;

	p = (AnalysisProfiles*) addToDynArray(file, func, line, da);
	initAnalysisProfiles(p);

	return p;
}
#endif

#if DO_ANALYSIS_HALOPROPS
AnalysisHaloProps* addAnalysisHaloProps(const char *file, const char *func, int line, DynArray *da)
{
	AnalysisHaloProps *p;

	p = (AnalysisHaloProps*) addToDynArray(file, func, line, da);
	initAnalysisHaloProps(p);

	return p;
}
#endif
