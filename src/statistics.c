/*************************************************************************************************
 *
 * This unit collects, analyzes and prints statistics about a sparta run. Each process keeps
 * local and particle statistics, only the main process keeps the global statistics object.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stddef.h>

#include "statistics.h"
#include "config.h"
#include "memory.h"
#include "halos/halo.h"
#include "tracers/tracers.h"

/*************************************************************************************************
 * INTERNAL CONSTANTS
 *************************************************************************************************/

#define ENUM(x, y) #x,
const char *mid_names[MID_N] =
	{MID_NAMES};
#undef ENUM

#define ENUM(x, y) #y,
const char *mid_desc[MID_N] =
	{MID_NAMES};
#undef ENUM

#define ENUM2(x, y) #y,
const char *T0_NAMES[T0_N] =
	{TIMERS0};
const char *T1_NAMES[T1_N] =
	{TIMERS1};
const char *T2_NAMES[T2_N] =
	{TIMERS2};
#undef ENUM2

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void printTimingStatistics(int snap_idx, GlobalStatistics *gs);
void accumulateTracerStatistics(GlobalStatistics *gs);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initDynArrayStatistics(DynArrayStatistics *ds)
{
	ds->n = 0;
	ds->n_alloc = 0;
	ds->n_exc = 0;
	ds->n_saved = 0;
	ds->n_saved_cum = 0;
	ds->mem = 0.0;
}

void initResultStatistics(ResultStatistics *rs)
{
	int i;

	initDynArrayStatistics(&(rs->da));
	for (i = 0; i < RSSTAT_N; i++)
		rs->n[i] = 0;
}

void initTracerStatistics(TracerStatistics *ts)
{
	int i, rs;

	initDynArrayStatistics(&(ts->tcr));
	initDynArrayStatistics(&(ts->iid));
	for (i = 0; i < TCRSTAT_N; i++)
		ts->n[i] = 0;
	for (rs = 0; rs < NRS; rs++)
		initResultStatistics(&(ts->rs[rs]));
}

void initAnalysisStatistics(AnalysisStatistics *als)
{
	initDynArrayStatistics(&(als->da));
}

void initLocalStatistics(LocalStatistics *ls)
{
	int i, tt, al;

	for (i = 0; i < T1_N; i++)
		ls->timers_1[i] = 0.0;
	for (i = 0; i < T2_N; i++)
		ls->timers_2[i] = 0.0;
	for (i = 0; i < T3_N; i++)
		ls->timers_3[i] = 0.0;
	for (i = 0; i < T4_N; i++)
		ls->timers_4[i] = 0.0;
	for (i = 0; i < SNPSTAT_N; i++)
		ls->snpstats[i] = 0.0;
	for (tt = 0; tt < NTT; tt++)
		initTracerStatistics(&(ls->tt[tt]));
	for (al = 0; al < NAL; al++)
		initAnalysisStatistics(&(ls->al[al]));
}

void initGlobalStatistics(GlobalStatistics *gs)
{
	int i, tt, rs, al;

	initDynArrayStatistics(&(gs->halos));

	gs->n_halos_connected = 0;
	gs->n_halos_new = 0;
	gs->n_halos_host = 0;
	gs->n_halos_sub = 0;
	gs->n_halos_ghost = 0;
	gs->n_exd_subs = 0;
	gs->mem_total = 0.0;

	for (tt = 0; tt < NTT; tt++)
	{
		initTracerStatistics(&(gs->tt[tt]));
		for (i = 0; i < TCRSTAT_N; i++)
			gs->tcrstat_cum[tt][i] = 0;
		for (rs = 0; rs < NRS; rs++)
			for (i = 0; i < RSSTAT_N; i++)
				gs->rsstat_cum[tt][rs][i] = 0;
	}
	for (al = 0; al < NAL; al++)
		initAnalysisStatistics(&(gs->al[al]));

	for (i = 0; i < T0_N; i++)
		gs->timers_0_cum[i] = 0.0;
	for (i = 0; i < T1_N; i++)
		gs->timers_1_cum[i] = 0.0;
	for (i = 0; i < T2_N; i++)
		gs->timers_2_cum[i] = 0.0;
	for (i = 0; i < T3_N; i++)
		gs->timers_3_cum[i] = 0.0;
	for (i = 0; i < T4_N; i++)
		gs->timers_4_cum[i] = 0.0;
	for (i = 0; i < SNPSTAT_N; i++)
		gs->procstats_cum[i] = 0.0;
}

/*
 * Collect statistics about the halos and subhalos that are being tracked.
 */
void collectHaloStatistics(DynArray *halos, GlobalStatistics *gs)
{
	int i;
	long int n_halos_host, n_halos_sub, n_halos_ghost, n_temp;
	Halo *h;
	float da_mem;

	da_mem = (float) ((long int) halos->n_alloc * halos->size) * MEGABYTE;

	n_temp = halos->n;
	MPI_Reduce(&n_temp, &(gs->halos.n), 1, MPI_LONG, MPI_SUM, MAIN_PROC, comm);
	n_temp = halos->n_alloc;
	MPI_Reduce(&n_temp, &(gs->halos.n_alloc), 1, MPI_LONG, MPI_SUM, MAIN_PROC, comm);
	MPI_Reduce(&da_mem, &(gs->halos.mem), 1, MPI_FLOAT, MPI_SUM, MAIN_PROC, comm);

	n_halos_host = 0;
	n_halos_sub = 0;
	n_halos_ghost = 0;

	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);

		if (isHostOrConnecting(h))
			n_halos_host++;
		if (isSubOrConnecting(h))
			n_halos_sub++;
		if (isGhost(h))
			n_halos_ghost++;
	}

	MPI_Reduce(&n_halos_host, &(gs->n_halos_host), 1, MPI_LONG, MPI_SUM, MAIN_PROC, comm);
	MPI_Reduce(&n_halos_sub, &(gs->n_halos_sub), 1, MPI_LONG, MPI_SUM, MAIN_PROC, comm);
	MPI_Reduce(&n_halos_ghost, &(gs->n_halos_ghost), 1, MPI_LONG, MPI_SUM, MAIN_PROC, comm);
}

/*
 * Collect statistics about the tracers. In this case, we need to first iterate over all local
 * halos and count the tracers and their results.
 */
void collectTracerStatistics(DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs)
{
	int i, tt, rs, al;
	Halo *h;
	TracerStatistics *ts;
	AnalysisStatistics *als;

	/*
	 * Note that we initialize the cumulative counter fields in the local stats here, but NOT the
	 * n[] array.
	 */
	for (tt = 0; tt < NTT; tt++)
	{
		ts = &(ls->tt[tt]);

		initDynArrayStatistics(&(ts->tcr));
		initDynArrayStatistics(&(ts->iid));
		for (rs = 0; rs < NRS; rs++)
			initDynArrayStatistics(&(ts->rs[rs].da));
		ts->n_in_hosts = 0;
		ts->n_in_subs = 0;
		ts->n_in_ghosts = 0;
	}
	for (al = 0; al < NAL; al++)
		initDynArrayStatistics(&(ls->al[al].da));

	/*
	 * Go through halos, add to counters
	 */
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);

		for (tt = 0; tt < NTT; tt++)
		{
			ts = &(ls->tt[tt]);
			ts->tcr.n += h->tt[tt].tcr.n;
			ts->tcr.n_alloc += h->tt[tt].tcr.n_alloc;
			ts->iid.n += h->tt[tt].iid.n;
			ts->iid.n_alloc += h->tt[tt].iid.n_alloc;
			for (rs = 0; rs < NRS; rs++)
			{
				ts->rs[rs].da.n += h->tt[tt].rs[rs].n;
				ts->rs[rs].da.n_alloc += h->tt[tt].rs[rs].n_alloc;
			}

			if (isGhost(h))
				ts->n_in_ghosts += h->tt[tt].tcr.n;
			else if (isSubPermanently(h))
				ts->n_in_subs += h->tt[tt].tcr.n;
			else
				ts->n_in_hosts += h->tt[tt].tcr.n;
		}
		for (al = 0; al < NAL; al++)
		{
			als = &(ls->al[al]);
			als->da.n += h->al[al].n;
			als->da.n_alloc += h->al[al].n_alloc;
		}
	}

	for (tt = 0; tt < NTT; tt++)
	{
		MPI_Reduce(&(ls->tt[tt].tcr.n), &(gs->tt[tt].tcr.n), 1, MPI_LONG, MPI_SUM, MAIN_PROC, comm);
		MPI_Reduce(&(ls->tt[tt].tcr.n_alloc), &(gs->tt[tt].tcr.n_alloc), 1, MPI_LONG, MPI_SUM,
		MAIN_PROC, comm);
		MPI_Reduce(&(ls->tt[tt].iid.n), &(gs->tt[tt].iid.n), 1, MPI_LONG, MPI_SUM, MAIN_PROC, comm);
		MPI_Reduce(&(ls->tt[tt].iid.n_alloc), &(gs->tt[tt].iid.n_alloc), 1, MPI_LONG, MPI_SUM,
		MAIN_PROC, comm);
		MPI_Reduce(&(ls->tt[tt].n), &(gs->tt[tt].n), TCRSTAT_N, MPI_LONG, MPI_SUM, MAIN_PROC, comm);

		MPI_Reduce(&(ls->tt[tt].n_in_hosts), &(gs->tt[tt].n_in_hosts), 1, MPI_LONG, MPI_SUM,
		MAIN_PROC, comm);
		MPI_Reduce(&(ls->tt[tt].n_in_subs), &(gs->tt[tt].n_in_subs), 1, MPI_LONG, MPI_SUM,
		MAIN_PROC, comm);
		MPI_Reduce(&(ls->tt[tt].n_in_ghosts), &(gs->tt[tt].n_in_ghosts), 1, MPI_LONG, MPI_SUM,
		MAIN_PROC, comm);

		for (rs = 0; rs < NRS; rs++)
		{
			MPI_Reduce(&(ls->tt[tt].rs[rs].da.n), &(gs->tt[tt].rs[rs].da.n), 1, MPI_LONG, MPI_SUM,
			MAIN_PROC, comm);
			MPI_Reduce(&(ls->tt[tt].rs[rs].da.n_alloc), &(gs->tt[tt].rs[rs].da.n_alloc), 1,
					MPI_LONG, MPI_SUM, MAIN_PROC, comm);
			MPI_Reduce(&(ls->tt[tt].rs[rs].n), &(gs->tt[tt].rs[rs].n), RSSTAT_N, MPI_LONG, MPI_SUM,
			MAIN_PROC, comm);
		}
		for (al = 0; al < NAL; al++)
		{
			MPI_Reduce(&(ls->al[al].da.n), &(gs->al[al].da.n), 1, MPI_LONG, MPI_SUM, MAIN_PROC,
					comm);
			MPI_Reduce(&(ls->al[al].da.n_alloc), &(gs->al[al].da.n_alloc), 1, MPI_LONG, MPI_SUM,
			MAIN_PROC, comm);
		}
	}

	if (is_main_proc)
	{
		gs->mem_total = gs->halos.mem;
		for (tt = 0; tt < NTT; tt++)
		{
			gs->tt[tt].tcr.mem = (float) ((long int) gs->tt[tt].tcr.n_alloc * sizeof(Tracer))
					* MEGABYTE;
			gs->mem_total += gs->tt[tt].tcr.mem;
			gs->tt[tt].iid.mem =
					(float) ((long int) gs->tt[tt].iid.n_alloc * sizeof(ID)) * MEGABYTE;
			gs->mem_total += gs->tt[tt].iid.mem;
			for (rs = 0; rs < NRS; rs++)
			{
				gs->tt[tt].rs[rs].da.mem = (float) ((long int) gs->tt[tt].rs[rs].da.n_alloc
						* rsprops[rs].size) * MEGABYTE;
				gs->mem_total += gs->tt[tt].rs[rs].da.mem;
			}
		}
		for (al = 0; al < NAL; al++)
		{
			gs->al[al].da.mem = (float) ((long int) gs->al[al].da.n_alloc * alprops[al].size)
					* MEGABYTE;
			gs->mem_total += gs->al[al].da.mem;
		}

		/*
		 * Quick check on the overall tracer statistics
		 */
#if CAREFUL
		for (tt = 0; tt < NTT; tt++)
		{
			if (gs->tt[tt].n_in_hosts + gs->tt[tt].n_in_subs + gs->tt[tt].n_in_ghosts
					!= gs->tt[tt].tcr.n)
				error(__FFL__,
						"Mismatch in tracer statistics: Found %d + %d + %d = %d in hosts/subs/ghosts, but % total.\n",
						gs->tt[tt].n_in_hosts, gs->tt[tt].n_in_subs, gs->tt[tt].n_in_ghosts,
						gs->tt[tt].n_in_hosts + gs->tt[tt].n_in_subs + gs->tt[tt].n_in_ghosts,
						gs->tt[tt].tcr.n);
		}
#endif
	}
}

/*
 * The ordering in the cmmm arrays is cumulative, min, max, mean. We handle the computation of the
 * timers in all relevant levels and procs in one generalized loop.
 */
void collectTimingStatistics(LocalStatistics *ls, GlobalStatistics *gs)
{
	int i, j, jj, N;
	double x, *buf_t1 = NULL, *buf_t2 = NULL, *buf_t3 = NULL, *buf_t4 = NULL, *buf_stat = NULL,
			*buf_proc = NULL, *buf_cum = NULL, *buf_min = NULL, *buf_max = NULL, *buf_mean =
			NULL, *buf_maxproc = NULL, proc_t, max_t;

	if (is_main_proc)
	{
		buf_t1 = (double*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(double) * T1_N * n_proc);
		buf_t2 = (double*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(double) * T2_N * n_proc);
		buf_t3 = (double*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(double) * T3_N * n_proc);
		buf_t4 = (double*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(double) * T4_N * n_proc);
		buf_stat = (double*) memAlloc(__FFL__, MID_EXCHANGEBUF,
				sizeof(double) * SNPSTAT_N * n_proc);
	}

	MPI_Gather(ls->timers_1, T1_N, MPI_DOUBLE, buf_t1, T1_N, MPI_DOUBLE, MAIN_PROC, comm);
	MPI_Gather(ls->timers_2, T2_N, MPI_DOUBLE, buf_t2, T2_N, MPI_DOUBLE, MAIN_PROC, comm);
	MPI_Gather(ls->timers_3, T3_N, MPI_DOUBLE, buf_t3, T3_N, MPI_DOUBLE, MAIN_PROC, comm);
	MPI_Gather(ls->timers_4, T4_N, MPI_DOUBLE, buf_t4, T4_N, MPI_DOUBLE, MAIN_PROC, comm);
	MPI_Gather(ls->snpstats, SNPSTAT_N, MPI_DOUBLE, buf_stat, SNPSTAT_N, MPI_DOUBLE, MAIN_PROC,
			comm);

	if (is_main_proc)
	{
		/*
		 * Find the proc that had the maximum time consumption and save its values in the
		 * maxproc fields.
		 */
		gs->max_proc = -1;
		max_t = 0.0;
		for (i = 0; i < n_proc; i++)
		{
			proc_t = buf_t1[i * T1_N + T1_PROCESS];
			if (proc_t > max_t)
			{
				gs->max_proc = i;
				max_t = proc_t;
			}
		}

		/*
		 * Now go through all fields and compute the mean, min/max, and save the values from the
		 * max_proc.
		 */
		for (jj = 0; jj < T1_N + T2_N + T3_N + T4_N + SNPSTAT_N; jj++)
		{
			if (jj < T1_N)
			{
				j = jj;
				N = T1_N;
				buf_proc = buf_t1;
				buf_cum = gs->timers_1_cum;
				buf_min = gs->timers_1_min;
				buf_max = gs->timers_1_max;
				buf_mean = gs->timers_1_mean;
				buf_maxproc = gs->timers_1_maxproc;
			} else if (jj < T1_N + T2_N)
			{
				j = jj - T1_N;
				N = T2_N;
				buf_proc = buf_t2;
				buf_cum = gs->timers_2_cum;
				buf_min = gs->timers_2_min;
				buf_max = gs->timers_2_max;
				buf_mean = gs->timers_2_mean;
				buf_maxproc = gs->timers_2_maxproc;
			} else if (jj < T1_N + T2_N + T3_N)
			{
				j = jj - T1_N - T2_N;
				N = T3_N;
				buf_proc = buf_t3;
				buf_cum = gs->timers_3_cum;
				buf_min = gs->timers_3_min;
				buf_max = gs->timers_3_max;
				buf_mean = gs->timers_3_mean;
				buf_maxproc = gs->timers_3_maxproc;
			} else if (jj < T1_N + T2_N + T3_N + T4_N)
			{
				j = jj - T1_N - T2_N - T3_N;
				N = T4_N;
				buf_proc = buf_t4;
				buf_cum = gs->timers_4_cum;
				buf_min = gs->timers_4_min;
				buf_max = gs->timers_4_max;
				buf_mean = gs->timers_4_mean;
				buf_maxproc = gs->timers_4_maxproc;
			} else
			{
				j = jj - T1_N - T2_N - T3_N - T4_N;
				N = SNPSTAT_N;
				buf_proc = buf_stat;
				buf_cum = gs->procstats_cum;
				buf_min = gs->procstats_min;
				buf_max = gs->procstats_max;
				buf_mean = gs->procstats_mean;
				buf_maxproc = gs->procstats_maxproc;
			}

			buf_min[j] = 1E20;
			buf_max[j] = -1;
			buf_mean[j] = 0.0;

			for (i = 0; i < n_proc; i++)
			{
				x = buf_proc[i * N + j];
				buf_mean[j] += x;
				if (x < buf_min[j])
					buf_min[j] = x;
				if (x > buf_max[j])
					buf_max[j] = x;
			}

			buf_mean[j] /= n_proc;
			buf_cum[j] += buf_mean[j];

			buf_maxproc[j] = buf_proc[gs->max_proc * N + j];
		}

		memFree(__FFL__, MID_EXCHANGEBUF, buf_t1, sizeof(double) * T1_N * n_proc);
		memFree(__FFL__, MID_EXCHANGEBUF, buf_t2, sizeof(double) * T2_N * n_proc);
		memFree(__FFL__, MID_EXCHANGEBUF, buf_t3, sizeof(double) * T3_N * n_proc);
		memFree(__FFL__, MID_EXCHANGEBUF, buf_t4, sizeof(double) * T4_N * n_proc);
		memFree(__FFL__, MID_EXCHANGEBUF, buf_stat, sizeof(double) * SNPSTAT_N * n_proc);
	}
}

void accumulateTracerStatistics(GlobalStatistics *gs)
{
	int i, tt, rs;

	for (tt = 0; tt < NTT; tt++)
	{
		for (i = 0; i < TCRSTAT_N; i++)
			gs->tcrstat_cum[tt][i] += gs->tt[tt].n[i];
		for (rs = 0; rs < NRS; rs++)
			for (i = 0; i < RSSTAT_N; i++)
				gs->rsstat_cum[tt][rs][i] += gs->tt[tt].rs[rs].n[i];
	}
}

/*
 * This function should only be executed once for each snapshot, as it adds cumulative statistics.
 */
void collectAllStatistics(DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs)
{
	collectHaloStatistics(halos, gs);
	collectTracerStatistics(halos, ls, gs);
	collectTimingStatistics(ls, gs);
	if (is_main_proc)
		accumulateTracerStatistics(gs);
}

void printSnapStatistics(int snap_idx, GlobalStatistics *gs, int log_level)
{
	int i, tt, rs, al;
	long int n_deleted_halos;
	float fac, fac_mem, mem_tcr, mem_rs[NRS], ratio;
	double t_snap;
	char str[1000], str2[1000];

	/*
	 * Print statistics about the halos tracked.
	 */
	n_deleted_halos = (long int) (gs->procstats_mean[SNPSTAT_NDELETEDHALOS] * n_proc);
	fac = 100.0 / (float) gs->halos.n;
	output(log_level,
			"[Main] [SN %3d] Halos (alive):  %ld total, %ld connected, %ld new, %ld hosts (%.1f %%), %ld subs (%.1f %%), %ld ghosts (%.1f %%)\n",
			snap_idx, gs->halos.n, gs->n_halos_connected, gs->n_halos_new, gs->n_halos_host,
			(float) gs->n_halos_host * fac, gs->n_halos_sub, (float) gs->n_halos_sub * fac,
			gs->n_halos_ghost, (float) gs->n_halos_ghost * fac);
	output(log_level,
			"[Main] [SN %3d] Halos (ended):  %ld total, %ld discarded, %ld saved (%ld cumulative)\n",
			snap_idx, n_deleted_halos, n_deleted_halos - gs->halos.n_saved, gs->halos.n_saved,
			gs->halos.n_saved_cum);

	/*
	 * Print statistics about tracers
	 */
	for (tt = 0; tt < NTT; tt++)
	{
		if (ttParticles(tt))
		{
			fac = 100.0 / (float) config.n_particles;
			output(log_level,
					"[Main] [SN %3d] Tracer %s:     Active %ld (%.1f %%), ignoring %ld (%.1f %%), processed %ld (%.1f %%), deleted %ld (%.1f %%)\n",
					snap_idx, ttprops[tt].name_short, gs->tt[tt].tcr.n,
					(float) gs->tt[tt].tcr.n * fac, gs->tt[tt].iid.n,
					(float) gs->tt[tt].iid.n * fac, gs->tt[tt].n[TCRSTAT_PROCESSED],
					(float) gs->tt[tt].n[TCRSTAT_PROCESSED] * fac, gs->tt[tt].n[TCRSTAT_DELETED],
					gs->tt[tt].n[TCRSTAT_DELETED] * fac);

			fac = 100.0 / (float) gs->tt[tt].tcr.n;
			output(log_level,
					"[Main] [SN %3d]                 Of active: %d in hosts (%.1f %%), %d in subs (%.1f %%), %d in ghosts (%.1f %%)\n",
					snap_idx, gs->tt[tt].n_in_hosts, (float) gs->tt[tt].n_in_hosts * fac,
					gs->tt[tt].n_in_subs, (float) gs->tt[tt].n_in_subs * fac,
					gs->tt[tt].n_in_ghosts, (float) gs->tt[tt].n_in_ghosts * fac);

			fac = 100.0 / (float) gs->tt[tt].n[TCRSTAT_PROCESSED];
			output(log_level,
					"[Main] [SN %3d]                 Of proc'd: new %ld (%.1f %%), rec'd %ld (%.1f %%), ign %ld (%.1f %%), tr %ld (%.1f %%), n-tr %ld (%.1f %%)\n",
					snap_idx, gs->tt[tt].n[TCRSTAT_NEW], (float) gs->tt[tt].n[TCRSTAT_NEW] * fac,
					gs->tt[tt].n[TCRSTAT_RECREATED], (float) gs->tt[tt].n[TCRSTAT_RECREATED] * fac,
					gs->tt[tt].n[TCRSTAT_IGNORED], (float) gs->tt[tt].n[TCRSTAT_IGNORED] * fac,
					gs->tt[tt].n[TCRSTAT_TRACKED], (float) gs->tt[tt].n[TCRSTAT_TRACKED] * fac,
					gs->tt[tt].n[TCRSTAT_NOT_TRACKED],
					(float) gs->tt[tt].n[TCRSTAT_NOT_TRACKED] * fac);
		} else
		{
			output(log_level, "[Main] [SN %3d] Tracer %s:     Active %ld, new %ld, deleted %ld\n",
					snap_idx, ttprops[tt].name_short, gs->tt[tt].tcr.n,
					(float) gs->tt[tt].tcr.n * fac, gs->tt[tt].n[TCRSTAT_NEW],
					gs->tt[tt].n[TCRSTAT_DELETED]);
		}

		fac = 100.0 / (float) gs->tt[tt].n[TCRSTAT_DELETED];
		output(log_level,
				"[Main] [SN %3d]                 Of del'td: host-sub %ld (%.1f %%), escpd %ld (%.1f %%), unconn %ld (%.1f %%), all done %ld (%.1f %%)\n",
				snap_idx, gs->tt[tt].n[TCRSTAT_HALO_BECAME_SUB],
				(float) gs->tt[tt].n[TCRSTAT_HALO_BECAME_SUB] * fac,
				gs->tt[tt].n[TCRSTAT_LEFT_HALO], (float) gs->tt[tt].n[TCRSTAT_LEFT_HALO] * fac,
				gs->tt[tt].n[TCRSTAT_UNCONNECTED], (float) gs->tt[tt].n[TCRSTAT_UNCONNECTED] * fac,
				gs->tt[tt].n[TCRSTAT_ALL_RS_DONE], (float) gs->tt[tt].n[TCRSTAT_ALL_RS_DONE] * fac);

#if DO_SUBHALO_TAGGING
		if (ttParticles(tt))
			output(log_level,
					"[Main] [SN %3d]                 Subhalo tags: %ld, ignored %ld, created %ld, result %ld\n",
					snap_idx, gs->tt[tt].n[TCRSTAT_SUB_TAG], gs->tt[tt].n[TCRSTAT_SUB_TAG_IGNORED],
					gs->tt[tt].n[TCRSTAT_SUB_TAG_CREATED], gs->tt[tt].n[TCRSTAT_SUB_TAG_RESULT]);
#endif
	}

	/*
	 * Print statistics about results (active and saved) for all tracers in a generalized fashion.
	 * In particular, we are outputting the following fields:
	 *
	 * current    Number of results currently kept in memory
	 * sn-new     Results that were successfully created during this snapshot
	 * cm-new     Cumulative sum of all results that have been successfully created
	 * sn-fld     Number of results that failed during this snapshot
	 * cm-fld     Cumulative number of all failed results
	 * sn-svd     Saved during this snapshot
	 * cm-svd     Saved overall
	 * fld-f      Fraction of total events that failed (fld / (new + fld))
	 * svd-f      Fraction of successful events that was saved
	 */
	if ((NTT > 0) && (NRS > 0))
		output(log_level,
				"[Main] [SN %3d] Results:        tcr  res%12s%12s%12s%12s%12s%12s%12s%8s%8s\n",
				snap_idx, "current", "sn-new", "cm-new", "sn-fld", "cm-fld", "sn-svd", "cm-svd",
				"fld-f", "svd-f");
	for (tt = 0; tt < NTT; tt++)
	{
		for (rs = 0; rs < NRS; rs++)
		{
			if (gs->rsstat_cum[tt][rs][RSSTAT_NEW_RES] == 0)
			{
				sprintf(str, "%s", "---");
				sprintf(str2, "%s", "---");
			} else
			{
				sprintf(str, "%.3f",
						(double ) gs->rsstat_cum[tt][rs][RSSTAT_FAILED]
								/ (double ) (gs->rsstat_cum[tt][rs][RSSTAT_NEW_RES]
										+ gs->rsstat_cum[tt][rs][RSSTAT_FAILED]));
				sprintf(str2, "%.3f",
						(double ) gs->tt[tt].rs[rs].da.n_saved_cum
								/ (double ) gs->rsstat_cum[tt][rs][RSSTAT_NEW_RES]);
			}

			output(log_level,
					"[Main] [SN %3d]                 %s  %s  %10ld  %10ld  %10ld  %10ld  %10ld  %10ld  %10ld  %6s  %6s\n",
					snap_idx, ttprops[tt].name_short, rsprops[rs].name_short,
					gs->tt[tt].rs[rs].da.n, gs->tt[tt].rs[rs].n[RSSTAT_NEW_RES],
					gs->rsstat_cum[tt][rs][RSSTAT_NEW_RES], gs->tt[tt].rs[rs].n[RSSTAT_FAILED],
					gs->rsstat_cum[tt][rs][RSSTAT_FAILED], gs->tt[tt].rs[rs].da.n_saved,
					gs->tt[tt].rs[rs].da.n_saved_cum, str, str2);
		}
	}

	/*
	 * Print general performance statistics about exchanges, efficiency etc
	 */
	output(log_level,
			"[Main] [SN %3d] Efficiency:     Volume ratio %.2f [%.2f..%.2f], snap files read %.0f [%d..%d]\n",
			snap_idx, gs->procstats_mean[SNPSTAT_VRATIO], gs->procstats_min[SNPSTAT_VRATIO],
			gs->procstats_max[SNPSTAT_VRATIO], gs->procstats_mean[SNPSTAT_FILESREAD],
			(int) gs->procstats_min[SNPSTAT_FILESREAD], (int) gs->procstats_max[SNPSTAT_FILESREAD]);

	sprintf(str, "[Main] [SN %3d]                 Exchanged %d halos, %d sub pointers", snap_idx,
			gs->halos.n_exc, gs->n_exd_subs);
	for (tt = 0; tt < NTT; tt++)
		sprintf(str, "%s, %d %s", str, gs->tt[tt].tcr.n_exc, ttprops[tt].name_long);
	output(log_level, "%s\n", str);

	/*
	 * Print rough timing statistics
	 */
	t_snap = 0.0;
	for (i = 0; i < T1_N; i++)
		t_snap += gs->timers_1_mean[i];
	output(log_level,
			"[Main] [SN %3d] Timing:         Total %.1f, balance %.2f (indep. %.1f, idle %.1f)\n",
			snap_idx, t_snap, gs->timers_1_max[T1_PROCESS] / gs->timers_1_min[T1_PROCESS],
			gs->timers_1_mean[T1_PROCESS], gs->timers_1_mean[T1_IDLE]);

	/*
	 * Print detailed timing statistics if desired
	 */
	if (config.log_level_timing > 0)
		printTimingStatistics(snap_idx, gs);

	/*
	 * Output memory statistics
	 */
	if (NTT > 0)
		sprintf(str, ", tracers");
	else
		sprintf(str, "%s", "");
	sprintf(str2, "Results");
	fac = 1.0 / (float) n_proc;
	fac_mem = 100.0 / gs->mem_total;
	for (rs = 0; rs < NRS; rs++)
		mem_rs[rs] = 0.0;
	for (tt = 0; tt < NTT; tt++)
	{
		mem_tcr = (float) gs->tt[tt].tcr.n_alloc * sizeof(Tracer) * MEGABYTE;
		sprintf(str, "%s %s %.0f (%.1f %%)", str, ttprops[tt].name_short, mem_tcr,
				mem_tcr * fac_mem);
		for (rs = 0; rs < NRS; rs++)
			mem_rs[rs] += (float) gs->tt[tt].rs[rs].da.n_alloc * rsprops[rs].size * MEGABYTE;
	}
	for (rs = 0; rs < NRS; rs++)
		sprintf(str2, "%s %s %.0f (%.1f %%)", str2, rsprops[rs].name_short, mem_rs[rs],
				mem_rs[rs] * fac_mem);
	output(log_level,
			"[Main] [SN %3d] Pers. memory:   Total %.0f MB (%.0f per proc), halos %.0f (%.1f %%)%s\n",
			snap_idx, gs->mem_total, gs->mem_total * fac, gs->halos.mem, gs->halos.mem * fac_mem,
			str);
	if (NRS > 0)
		output(log_level, "[Main] [SN %3d]                 %s\n", snap_idx, str2);

	/*
	 * Output stats about efficiency of DynArray allocation
	 */
	for (tt = 0; tt < NTT; tt++)
	{
		if (tt == 0)
			sprintf(str, "%s", "Memory eff.:");
		else
			sprintf(str, "%s", "");

		// The tracer itself
		sprintf(str2, "Tracer %s", ttprops[tt].name_short);
		if (gs->tt[tt].tcr.n_alloc == 0)
		{
			sprintf(str2, "%s   ---", str2);
		} else
		{
			ratio = (float) (gs->tt[tt].tcr.n) / (float) (gs->tt[tt].tcr.n_alloc);
			sprintf(str2, "%s %.3f", str2, ratio);
		}

		// Ignore list
		sprintf(str2, "%s, ignore list", str2);
		if (gs->tt[tt].iid.n_alloc == 0)
		{
			sprintf(str2, "%s   ---", str2);
		} else
		{
			ratio = (float) (gs->tt[tt].iid.n) / (float) (gs->tt[tt].iid.n_alloc);
			sprintf(str2, "%s %.3f", str2, ratio);
		}

		// Tracer results
		sprintf(str2, "%s, results:", str2);

		for (rs = 0; rs < NRS; rs++)
		{
			sprintf(str2, "%s %3s", str2, rsprops[rs].name_short);

			if (gs->tt[tt].rs[rs].da.n_alloc == 0)
			{
				sprintf(str2, "%s   ---", str2);
			} else
			{
				ratio = (float) (gs->tt[tt].rs[rs].da.n) / (float) (gs->tt[tt].rs[rs].da.n_alloc);
				sprintf(str2, "%s %.3f", str2, ratio);
			}
		}
		output(log_level, "[Main] [SN %3d] %-15s %s\n", snap_idx, str, str2);
	}

	sprintf(str2, "%s", "Analyses:");
	for (al = 0; al < NAL; al++)
	{
		sprintf(str2, "%s %s", str2, alprops[al].name_short);
		if (gs->al[al].da.n_alloc == 0)
		{
			sprintf(str2, "%s   ---", str2);
		} else
		{
			ratio = (float) (gs->al[al].da.n) / (float) (gs->al[al].da.n_alloc);
			sprintf(str2, "%s %.3f", str2, ratio);
		}
	}
	output(log_level, "[Main] [SN %3d]                 %s\n", snap_idx, str2);

}

void printTimingStatistics(int snap_idx, GlobalStatistics *gs)
{
	int j, k, l;
#if DO_RESULT_ANY
	int rs, tt;
#endif
	double pc_1_fac, t1_total, perc;
	char str[3000];

	//printLine(0);

	t1_total = 0.0;
	for (j = 0; j < T1_N; j++)
		t1_total += gs->timers_1_maxproc[j];
	pc_1_fac = 100.0 / t1_total;

	output(0, "[Main] [SN %3d]                 On max. proc (%03d)         Time (s)          %%\n",
			snap_idx, gs->max_proc);
	output(0,
			"[Main] [SN %3d]                 ------------------------------------------------------------\n",
			snap_idx);

// Level 1
	for (j = 0; j < T1_N; j++)
	{
		perc = gs->timers_1_maxproc[j] * pc_1_fac;
		if ((perc > TIMING_MIN_PRINT_FRAC * 100.0) || (config.log_level_timing > 1))
			output(0, "[Main] [SN %3d]                 %-25s     %5.1f      %5.1f\n", snap_idx,
					T1_NAMES[j], gs->timers_1_maxproc[j], perc);

		// Level 2
		if (j == T1_PROCESS)
		{
			for (k = 0; k < T2_N; k++)
			{
				perc = gs->timers_2_maxproc[k] * pc_1_fac;
				if ((perc > TIMING_MIN_PRINT_FRAC * 100.0) || (config.log_level_timing > 1))
					output(0, "[Main] [SN %3d]                   %-25s     %5.1f      %5.1f\n",
							snap_idx, T2_NAMES[k], gs->timers_2_maxproc[k], perc);

				// Level 3
				if (k == T2_TCR_RESULTS)
				{
					for (l = 0; l < T3_N; l++)
					{
						perc = gs->timers_3_maxproc[l] * pc_1_fac;
#if DO_RESULT_ANY
						tt = l / NRS;
						rs = l % NRS;
						sprintf(str, "Tracer %s result %s", ttprops[tt].name_short,
								rsprops[rs].name_short);
#endif
						if ((perc > TIMING_MIN_PRINT_FRAC * 100.0) || (config.log_level_timing > 1))
							output(0,
									"[Main] [SN %3d]                     %-25s     %5.1f      %5.1f\n",
									snap_idx, str, gs->timers_3_maxproc[l], perc);
					}
				}

				// Level 4
				if (k == T2_ANALYSES)
				{
					for (l = 0; l < T4_N; l++)
					{
						perc = gs->timers_4_maxproc[l] * pc_1_fac;
						sprintf(str, "Analysis %s", alprops[l].name_short);
						if ((perc > TIMING_MIN_PRINT_FRAC * 100.0) || (config.log_level_timing > 1))
							output(0,
									"[Main] [SN %3d]                     %-25s     %5.1f      %5.1f\n",
									snap_idx, str, gs->timers_4_maxproc[l], perc);
					}
				}
			}
		}
	}
	output(0,
			"[Main] [SN %3d]                 ------------------------------------------------------------\n",
			snap_idx);
	output(0, "[Main] [SN %3d]                 %-25s     %5.1f      %5.1f\n", snap_idx,
			"Timing total", t1_total, 100.0);
}

void printTimingStatisticsFinal(GlobalStatistics *gs)
{
	int i, j, k, l;
#if DO_RESULT_ANY
	int rs, tt;
#endif
	double pc_0_fac, pc_1_fac, pc_2_fac, pc_3_fac, pc_4_fac, t0_total, t1_total, t2_total, t3_total,
			t4_total, u, snap_fac;
	char str[400];

	/*
	 * Conversion factors to percent at each level
	 */
	t0_total = 0.0;
	t1_total = 0.0;
	t2_total = 0.0;
	t3_total = 0.0;
	t4_total = 0.0;
	for (i = 0; i < T0_N; i++)
		t0_total += gs->timers_0_cum[i];
	for (i = 0; i < T1_N; i++)
		t1_total += gs->timers_1_cum[i];
	for (i = 0; i < T2_N; i++)
		t2_total += gs->timers_2_cum[i];
	for (i = 0; i < T3_N; i++)
		t3_total += gs->timers_3_cum[i];
	for (i = 0; i < T4_N; i++)
		t4_total += gs->timers_4_cum[i];
	pc_0_fac = 100.0 / t0_total;
	pc_1_fac = 100.0 / t1_total;
	pc_2_fac = 100.0 / t2_total;
	pc_3_fac = 100.0 / t3_total;
	pc_4_fac = 100.0 / t4_total;

	/*
	 * Units: seconds, minutes, hours
	 */
	if (t0_total >= 60000.0)
	{
		u = 1.0 / 3600.0;
		output(0, "[Main] All times are in hours.\n");
	} else if (t0_total >= 1000.0)
	{
		u = 1.0 / 60.0;
		output(0, "[Main] All times are in minutes.\n");
	} else
	{
		u = 1.0;
		output(0, "[Main] All times are in seconds.\n");
	}
	snap_fac = u / (double) config.n_snaps;

	output(0,
			"[Main] Task                               Time   Per snap    Level %%    Total %%\n");
	output(0,
			"[Main] --------------------------------------------------------------------------\n");

// Level 0
	for (i = 0; i < T0_N; i++)
	{
		output(0, "[Main] %-25s     %5.1f                            %5.1f\n", T0_NAMES[i],
				gs->timers_0_cum[i] * u, gs->timers_0_cum[i] * pc_0_fac);

		// Level 1
		if (i == T0_SNAPS)
		{
			for (j = 0; j < T1_N; j++)
			{
				output(0, "[Main]   %-25s     %5.1f      %5.1f      %5.1f      %5.1f\n",
						T1_NAMES[j], gs->timers_1_cum[j] * u, gs->timers_1_cum[j] * snap_fac,
						gs->timers_1_cum[j] * pc_1_fac, gs->timers_1_cum[j] * pc_0_fac);

				// Level 2
				if (j == T1_PROCESS)
				{
					for (k = 0; k < T2_N; k++)
					{
						output(0, "[Main]     %-25s     %5.1f      %5.1f      %5.1f      %5.1f\n",
								T2_NAMES[k], gs->timers_2_cum[k] * u,
								gs->timers_2_cum[k] * snap_fac, gs->timers_2_cum[k] * pc_2_fac,
								gs->timers_2_cum[k] * pc_0_fac);

						// Level 3
						if ((k == T2_TCR_RESULTS) && (NRS > 0))
						{
							for (l = 0; l < T3_N; l++)
							{
#if DO_RESULT_ANY
								tt = l / NRS;
								rs = l % NRS;
								sprintf(str, "Tracer %s result %s", ttprops[tt].name_short,
										rsprops[rs].name_short);
#endif
								output(0,
										"[Main]       %-25s     %5.1f      %5.1f      %5.1f      %5.1f\n",
										str, gs->timers_3_cum[l] * u,
										gs->timers_3_cum[l] * snap_fac,
										gs->timers_3_cum[l] * pc_3_fac,
										gs->timers_3_cum[l] * pc_0_fac);
							}
						}

						// Level 4
						if (k == T2_ANALYSES)
						{
							for (l = 0; l < T4_N; l++)
							{
								sprintf(str, "Analysis %s", alprops[l].name_short);
								output(0,
										"[Main]       %-25s     %5.1f      %5.1f      %5.1f      %5.1f\n",
										str, gs->timers_4_cum[l] * u,
										gs->timers_4_cum[l] * snap_fac,
										gs->timers_4_cum[l] * pc_4_fac,
										gs->timers_4_cum[l] * pc_0_fac);
							}
						}
					}
				}
			}
		}
	}
	output(0,
			"[Main] --------------------------------------------------------------------------\n");
	output(0, "[Main] %-25s     %5.1f                            %5.1f\n", "Total", t0_total * u,
			100.0);
}

/*
 * Note that this function is non-MPI safe so that it can be used by tools that run on one core.
 */
void printMemoryStatistics(int snap_idx, int final)
{
	/*
	 * Nothing to do if we are not tracking memory
	 */
	if (config.log_level_memory == 0)
		return;

	int i, j, peak_proc, min_mid, found_current;
	float mid_peak_flt, mid_at_tot_peak_flt, use_tot, cur_allproc, peak_allproc;
	long int mid_peak, mid_at_tot_peak, overall_peak, *buf_cur_tot, *buf_peak_tot, *buf_current,
			*buf_peak, *buf_at_tot_peak;

	/*
	 * Gather memory statistics from all procs
	 */
	if (comm != COMM_NO_MPI)
	{
		buf_cur_tot = NULL;
		buf_peak_tot = NULL;
		buf_current = NULL;
		buf_peak = NULL;
		buf_at_tot_peak = NULL;
		if (is_main_proc)
		{
			buf_cur_tot = (long int*) memAlloc(__FFL__, MID_IGNORE, sizeof(long int) * n_proc);
			buf_peak_tot = (long int*) memAlloc(__FFL__, MID_IGNORE, sizeof(long int) * n_proc);
			buf_current = (long int*) memAlloc(__FFL__, MID_IGNORE,
					sizeof(long int) * MID_N * n_proc);
			buf_peak = (long int*) memAlloc(__FFL__, MID_IGNORE, sizeof(long int) * MID_N * n_proc);
			buf_at_tot_peak = (long int*) memAlloc(__FFL__, MID_IGNORE,
					sizeof(long int) * MID_N * n_proc);
		}

		MPI_Gather(&(mem_stats.cur_tot), 1, MPI_LONG, buf_cur_tot, 1, MPI_LONG, MAIN_PROC, comm);
		MPI_Gather(&(mem_stats.peak_tot), 1, MPI_LONG, buf_peak_tot, 1, MPI_LONG, MAIN_PROC, comm);
		MPI_Gather(mem_stats.current, MID_N, MPI_LONG, buf_current, MID_N, MPI_LONG, MAIN_PROC,
				comm);
		MPI_Gather(mem_stats.peak, MID_N, MPI_LONG, buf_peak, MID_N, MPI_LONG, MAIN_PROC, comm);
		MPI_Gather(mem_stats.at_tot_peak, MID_N, MPI_LONG, buf_at_tot_peak, MID_N, MPI_LONG,
		MAIN_PROC, comm);
	} else
	{
		buf_cur_tot = &(mem_stats.cur_tot);
		buf_peak_tot = &(mem_stats.peak_tot);
		buf_current = mem_stats.current;
		buf_peak = mem_stats.peak;
		buf_at_tot_peak = mem_stats.at_tot_peak;
	}

	if (is_main_proc)
	{
		/*
		 * Print total and peak consumption
		 */
		if (config.log_level_memory > 1)
		{
			cur_allproc = 0.0;
			peak_allproc = 0.0;
			for (i = 0; i < n_proc; i++)
			{
				cur_allproc += buf_cur_tot[i];
				peak_allproc += buf_peak_tot[i];
			}
			cur_allproc /= (1024.0 * 1024.0);
			peak_allproc /= (1024.0 * 1024.0);
			if (final)
			{
				output(0, "[Main] Memory: Peak total consumption was %.1f GB (%.1f MB per proc).\n",
						peak_allproc / 1024.0, peak_allproc / n_proc);
			} else
			{
				output(0,
						"[Main] [SN %3d] Memory:         Total current %.1f GB (%.1f MB per proc), peak %.1f GB (%.1f MB per proc)\n",
						snap_idx, cur_allproc / 1024.0, cur_allproc / n_proc, peak_allproc / 1024.0,
						peak_allproc / n_proc);
			}
		}

		/*
		 * If detailed, print memory on each proc
		 */
		if (config.log_level_memory > 2)
		{
			for (i = 0; i < n_proc; i++)
				if (final)
				{
					output(0, "[Main] On proc %3d:   %11ld (%7.1f MB)\n", i, buf_peak_tot[i],
							(float) buf_peak_tot[i] / 1024.0 / 1024.0);
				} else
				{
					output(0,
							"[Main] [SN %3d]                 On proc %3d:  %11ld (%7.1f MB) peak %11ld (%7.1f MB)\n",
							snap_idx, i, buf_cur_tot[i], (float) buf_cur_tot[i] / 1024.0 / 1024.0,
							buf_peak_tot[i], (float) buf_peak_tot[i] / 1024.0 / 1024.0);
				}
		}

		/*
		 * Look for memory leaks
		 */
		if (final)
			min_mid = MID_IGNORE;
		else
			min_mid = MID_MUSTBEZERO;

		found_current = 0;
		for (i = 0; i < n_proc; i++)
		{
			for (j = min_mid; j < MID_N; j++)
			{
				if (buf_current[i * MID_N + j] != 0)
				{
					output(0,
							"[%4d] [SN %3d]                 MemID %3d %-20s Current use %11ld peak %11ld\n",
							i, snap_idx, j, mid_names[j], buf_current[i * MID_N + j],
							buf_peak[i * MID_N + j]);
					found_current = 1;
				}
			}
		}
		if (found_current)
			warning("[Main] [SN %3d] Found at least one memory leak (details above).\n", snap_idx);

		/*
		 * Output the proc where the peak consumption happened
		 */
		if (config.log_level_memory > 1)
		{
			peak_proc = -1;
			overall_peak = 0;
			for (i = 0; i < n_proc; i++)
			{
				if (buf_peak_tot[i] > overall_peak)
				{
					overall_peak = buf_peak_tot[i];
					peak_proc = i;
				}
			}
			use_tot = (float) buf_peak_tot[peak_proc];
			if (final)
			{
				output(0,
						"[Main] Overall peak was %ld, %.1f MB on proc %d. Memory consuming fields:\n",
						overall_peak, use_tot / 1024.0 / 1024.0, peak_proc);
			} else
			{
				output(0,
						"[Main] [SN %3d]                 Overall peak was %ld, %.1f MB on proc %d\n",
						snap_idx, overall_peak, use_tot / 1024.0 / 1024.0, peak_proc);
			}

			/*
			 * Output the biggest consumers of memory at the overall peak usage of any proc
			 */
			if (final)
			{
				output(0,
						"[Main] -------------------------------------------------------------------------------------------------------------------------\n");
				output(0,
						"[Main] | Memory ID                | Description                    |  Highest consumption    |          At overall peak        |\n");
				output(0,
						"[Main] |                          |                                |        Byte         MB  |        Byte        MB  Fraction |\n");
				output(0,
						"[Main] -------------------------------------------------------------------------------------------------------------------------\n");
			} else
			{
				output(0,
						"[Main] [SN %3d]                 Memory ID             Description        At overall peak:   Byte       MB Fraction\n",
						snap_idx);
			}
			for (j = MID_IGNORE + 1; j < MID_N; j++)
			{
				mid_peak = buf_peak[peak_proc * MID_N + j];
				mid_peak_flt = (float) mid_peak;
				mid_at_tot_peak = buf_at_tot_peak[peak_proc * MID_N + j];
				mid_at_tot_peak_flt = (float) mid_at_tot_peak;
				if (final)
				{
					if ((mid_peak > 0) || (mid_at_tot_peak > 0) || (config.log_level_memory > 2))
						output(0,
								"[Main] | %-24s | %-30s | %11ld    %7.1f  | %11ld   %7.1f   %5.1f %% |\n",
								mid_names[j] + 4, mid_desc[j], mid_peak,
								mid_peak_flt / 1024.0 / 1024.0, mid_at_tot_peak,
								mid_at_tot_peak_flt / 1024.0 / 1024.0,
								mid_at_tot_peak_flt / use_tot * 100.0);
				} else if ((mid_at_tot_peak_flt / use_tot > MEMORY_MIN_PRINT_FRAC)
						|| ((config.log_level_memory > 2) && (mid_at_tot_peak > 0))
						|| (config.log_level_memory > 3))
				{
					output(0,
							"[Main] [SN %3d]                 %-20s  %-30s %11ld  %7.1f  %5.1f %%\n",
							snap_idx, mid_names[j] + 4, mid_desc[j], mid_at_tot_peak,
							mid_at_tot_peak_flt / 1024.0 / 1024.0,
							mid_at_tot_peak_flt / use_tot * 100.0);
				}
			}
		}

		if (comm != COMM_NO_MPI)
		{
			memFree(__FFL__, MID_IGNORE, buf_cur_tot, sizeof(long int) * n_proc);
			memFree(__FFL__, MID_IGNORE, buf_peak_tot, sizeof(long int) * n_proc);
			memFree(__FFL__, MID_IGNORE, buf_current, sizeof(long int) * MID_N * n_proc);
			memFree(__FFL__, MID_IGNORE, buf_peak, sizeof(long int) * MID_N * n_proc);
			memFree(__FFL__, MID_IGNORE, buf_at_tot_peak, sizeof(long int) * MID_N * n_proc);
		}
	}
}

/*
 * This function is more for debugging than detailed memory output, and just lists the total
 * memory consumption on each process and for each type of halo.
 */
void printMemoryStatisticsProcess(DynArray *halos, int snap_idx, char *after_step,
		int print_halo_stats)
{
	if (print_halo_stats)
	{
		int i, n_hst, n_sub, n_gho, n_otr;
		double mem, mem_tot_hst, mem_tot_sub, mem_tot_gho, mem_tot_otr;
		Halo *h;

		n_hst = 0;
		n_sub = 0;
		n_gho = 0;
		n_otr = 0;
		mem_tot_hst = 0.0;
		mem_tot_sub = 0.0;
		mem_tot_gho = 0.0;
		mem_tot_otr = 0.0;
		for (i = 0; i < halos->n; i++)
		{
			h = &(halos->h[i]);
			mem = MEGABYTE * haloMemory(h);
			if (isGhost(h))
			{
				n_gho++;
				mem_tot_gho += mem;
			} else
			{
				if (isHost(h))
				{
					n_hst++;
					mem_tot_hst += mem;
				} else if (isSub(h))
				{
					n_sub++;
					mem_tot_sub += mem;
				} else
				{
					n_otr++;
					mem_tot_otr += mem;
				}
			}
		}
		output(2,
				"[%4d] [SN %3d] After %s, using %7.1f MB memory (%7d hosts, %7d subs, %7d ghosts, %7d other, using %7.1f / %7.1f / %7.1f / %7.1f MB).\n",
				proc, snap_idx, after_step,
				MEGABYTE * mem_stats.cur_tot, n_hst, n_sub, n_gho, n_otr, mem_tot_hst, mem_tot_sub,
				mem_tot_gho, mem_tot_otr);
	} else
	{
		output(2, "[%4d] [SN %3d] After %s, using %7.1f MB memory.\n", proc, snap_idx, after_step,
		MEGABYTE * mem_stats.cur_tot);
	}
}

/*
 * Helper function for the halo warning function. We measure the total memory in a particular
 * array, either for a host, all its subs, or all its ghosts.
 */
void getDynArrayMemory(DynArray *halos, Halo *h, DynArray *da, int halo_type, int *n_used,
		int *n_allc, float *mem_used, float *mem_alloc)
{
	int n_used_tmp, n_allc_tmp, i, offset, sub_idx, is_ghost;
	DynArray *da_sub;
	Halo *sub;
#if DO_SUBSUBS
	int k, is_ghost_subsub;
	Halo *subsub;
#endif

	if (halo_type > 0)
	{
		n_used_tmp = 0;
		n_allc_tmp = 0;
		offset = (char*) da - (char*) h;

		for (i = 0; i < h->subs.n; i++)
		{
			sub_idx = h->subs.sp[i].idx;
#if CAREFUL
			if ((sub_idx < 0) || (sub_idx > halos->n))
				error(__FFL__, "Invalid sub idx %d, max %d.\n", sub_idx, halos->n);
#endif
			sub = &(halos->h[sub_idx]);
			is_ghost = isGhost(sub);

			/*
			 * Check whether this halo is relevant. If we are looking for subs and it is a ghost,
			 * we can abort. If we are looking for ghosts and it is a sub, it could still have
			 * ghost subhalos.
			 */
			if ((halo_type == 1) && is_ghost)
				continue;

			if (((halo_type == 1) && !is_ghost) || ((halo_type == 2) && is_ghost))
			{
				da_sub = (DynArray*) ((char*) sub + offset);
				n_used_tmp += da_sub->n;
				n_allc_tmp += da_sub->n_alloc;
#if CAREFUL
				if ((da_sub->n < 0) || (da_sub->n_alloc < 0))
					error(__FFL__,
							"Found negative number of objects in DynArray (%d used, %d alloc), halo ID %ld, offset %d, sub_idx %d/%d, sub ID %ld.\n",
							da_sub->n, da_sub->n_alloc, h->cd.id, offset, sub_idx, halos->n,
							sub->cd.id);
#endif
			}

			if ((halo_type == 2) && !is_ghost)
			{
#if DO_SUBSUBS
				for (k = 0; k < sub->subs.n; k++)
				{
					sub_idx = sub->subs.sp[k].idx;
#if CAREFUL
					if ((sub_idx < 0) || (sub_idx > halos->n))
						error(__FFL__, "Invalid sub idx %d, max %d.\n", sub_idx, halos->n);
#endif
					subsub = &(halos->h[sub_idx]);
					is_ghost_subsub = isGhost(subsub);

					/*
					 * At this point, we accept either subs or ghosts to be future-proof.
					 */
					if (((halo_type == 1) && !is_ghost_subsub)
							|| ((halo_type == 2) && is_ghost_subsub))
					{
						da_sub = (DynArray*) ((char*) subsub + offset);
						n_used_tmp += da_sub->n;
						n_allc_tmp += da_sub->n_alloc;
#if CAREFUL
						if ((da_sub->n < 0) || (da_sub->n_alloc < 0))
							error(__FFL__,
									"Found negative number of objects in DynArray (%d used, %d alloc), halo ID %ld, offset %d, subsub_idx %d/%d, subsub ID %ld.\n",
									da_sub->n, da_sub->n_alloc, h->cd.id, offset, sub_idx, halos->n,
									sub->cd.id);
#endif
					}
				}
#endif
			}
		}
	} else
	{
		n_used_tmp = da->n;
		n_allc_tmp = da->n_alloc;
	}

	*n_used = n_used_tmp;
	*n_allc = n_allc_tmp;
	*mem_used = MEGABYTE * n_used_tmp * da->size;
	*mem_alloc = MEGABYTE * n_allc_tmp * da->size;
}

float printDynArrayMemory(DynArray *halos, Halo *h, DynArray *da, char *head_str, double mem)
{
	int n_used_hst, n_allc_hst, n_used_sub, n_allc_sub, n_used_gho, n_allc_gho;
	float s_used_hst, s_allc_hst, s_used_sub, s_allc_sub, s_used_gho, s_allc_gho;

	getDynArrayMemory(halos, h, da, 0, &n_used_hst, &n_allc_hst, &s_used_hst, &s_allc_hst);
	getDynArrayMemory(halos, h, da, 1, &n_used_sub, &n_allc_sub, &s_used_sub, &s_allc_sub);
	getDynArrayMemory(halos, h, da, 2, &n_used_gho, &n_allc_gho, &s_used_gho, &s_allc_gho);
	output(0,
			"%s%8d objects (%7.1f MB), %9d alloc (%7.1f MB, %4.1f%%)  %9d in subs (%7.1f MB), %9d alloc (%7.1f MB, %4.1f%%)  %9d in ghosts (%7.1f MB), %9d alloc (%7.1f MB, %4.1f%%)\n",
			head_str, n_used_hst, s_used_hst, n_allc_hst, s_allc_hst, 100.0 * s_allc_hst / mem,
			n_used_sub, s_used_sub, n_allc_sub, s_allc_sub, 100.0 * s_allc_sub / mem, n_used_gho,
			s_used_gho, n_allc_gho, s_allc_gho, 100.0 * s_allc_gho / mem);

	return s_allc_hst + s_allc_sub + s_allc_gho;
}

/*
 * After the exchange step, we know all halos on this process. This is a good time to warn the user
 * about extreme halos, e.g., those that consume a particularly large amount of memory and might
 * crash the process.
 */
void printHaloWarnings(int snap_idx, DynArray *halos)
{
	if (config.err_level_large_halo == ERR_LEVEL_IGNORE)
		return;

#if DO_READ_PARTICLES
	int i, j, rs, tt, al, sub_idx, n_subs, n_ghosts, found_at_least_one;
	double mem;
	float M200m, N200m;
	Halo *h, *sub;
	char head_str[200];
#if DO_SUBSUBS
	int k;
	Halo *subsub;
#endif

	found_at_least_one = 0;
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);

		/*
		 * Subhalos need to be with their host, so we'll count them into the host
		 */
		if (!isHost(h))
			continue;

		/*
		 * Compute the total memory in this halo, see if we need to output it.
		 */
		mem = haloMemory(h);
		n_subs = 0;
		n_ghosts = 0;
		for (j = 0; j < h->subs.n; j++)
		{
			sub_idx = h->subs.sp[j].idx;
#if CAREFUL
			if ((sub_idx < 0) || (sub_idx > halos->n))
				error(__FFL__, "Invalid sub idx %d, max %d.\n", sub_idx, halos->n);
#endif
			sub = &(halos->h[sub_idx]);
			mem += haloMemory(sub);
			if (isGhost(sub))
				n_ghosts++;
			else
				n_subs++;

#if DO_SUBSUBS
			for (k = 0; k < sub->subs.n; k++)
			{
				sub_idx = sub->subs.sp[k].idx;
				subsub = &(halos->h[sub_idx]);
				mem += haloMemory(subsub);
				if (isGhost(subsub))
					n_ghosts++;
				else
					n_subs++;
			}
#endif
		}
		mem *= MEGABYTE;
		if (mem < config.memory_max_halo_size)
			continue;

		/*
		 * Print general information about the host
		 */
		found_at_least_one = 1;
		M200m = haloM200m(h, snap_idx);
		N200m = M200m / config.particle_mass;
		output(0,
				"[%4d] Warning: Halo ID %ld: total mem size %.1f MB (status %2d, R200m %.1f, M200m %.1e, N200m %.1e, %d subhalos, %d ghosts).\n",
				proc, h->cd.id, mem, h->status, haloR200m(h, snap_idx), M200m, N200m, n_subs,
				n_ghosts);

		/*
		 * We measure memory twice, first for the host, then for the sum of subhalos, but then
		 * print it into one line
		 */
		float mem_total = 0.0;
		for (tt = 0; tt < NTT; tt++)
		{
			sprintf(head_str, "       Tracer %s:    ", ttprops[tt].name_short);
			mem_total += printDynArrayMemory(halos, h, &(h->tt[tt].tcr), head_str, mem);

			sprintf(head_str, "          Ignore:     ");
			mem_total += printDynArrayMemory(halos, h, &(h->tt[tt].iid), head_str, mem);

			for (rs = 0; rs < NRS; rs++)
			{
				sprintf(head_str, "          Result %s: ", rsprops[rs].name_short);
				mem_total += printDynArrayMemory(halos, h, &(h->tt[tt].rs[rs]), head_str, mem);
			}
		}
		for (al = 0; al < NAL; al++)
		{
			sprintf(head_str, "       Analysis %s:  ", alprops[al].name_short);
			mem_total += printDynArrayMemory(halos, h, &(h->al[al]), head_str, mem);
		}

		/*
		 * Compare the total memory we counted in DynArrays and the total memory for the halo. They
		 * were measured indepdendently but should agree pretty exactly, minus the size of the
		 * actual halo objects.
		 */
		if (mem_total / mem < 0.9)
			warning(
					"Found incompatible memory totals %.2f and %.2f MB, ratio %.3f, for halo ID %ld.\n",
					mem, mem_total, mem_total / mem, h->cd.id);
	}

	if (found_at_least_one && (config.err_level_large_halo == ERR_LEVEL_ERROR))
		error(__FFL__, "Found at least one halo whose memory exceeds max_halo_size. Aborting.\n");
#endif
}
