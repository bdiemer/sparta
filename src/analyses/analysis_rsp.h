/*************************************************************************************************
 *
 * This unit implements the splashback radius analysis.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _ANALYSIS_RSP_H_
#define _ANALYSIS_RSP_H_

#include "analyses.h"
#include "../global_types.h"
#include "../io/io_hdf5.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * In addition to the default analysis status codes, there are a number of reasons for why the
 * Rsp analysis can fail in particular time bins:
 *
 * RSP_ERR_INSUFFICIENT_EVENTS   There were too few splashback events in the maximal time range
 *                               considered.
 *
 * RSP_ERR_INSUFFICIENT_WEIGHT   There were enough splashback events, but their total statistical
 *                               weight in this time bin was too low.
 *
 * Note that these constants must be outside the #if protected block because they are used in
 * analysis software.
 */
#define ANL_RSP_STATUS_UNDEFINED ANALYSIS_STATUS_UNDEFINED
#define ANL_RSP_STATUS_SUCCESS ANALYSIS_STATUS_SUCCESS
#define ANL_RSP_STATUS_HALO_NOT_VALID ANALYSIS_STATUS_HALO_NOT_VALID
#define ANL_RSP_STATUS_HALO_NOT_SAVED ANALYSIS_STATUS_HALO_NOT_SAVED
#define ANL_RSP_STATUS_NOT_FOUND ANALYSIS_STATUS_NOT_FOUND
#define ANL_RSP_STATUS_INSUFFICIENT_EVENTS (ANALYSIS_STATUS_OTHER)
#define ANL_RSP_STATUS_INSUFFICIENT_WEIGHT (ANALYSIS_STATUS_OTHER + 1)

#if DO_ANALYSIS_RSP

/*
 * Default config parameters. DEFAULT_ANL_RSP_MIN_SMR must be lower than the invalid SMR, which is
 * -1.0.
 */
#define DEFAULT_ANL_RSP_MIN_RRM 0.0
#define DEFAULT_ANL_RSP_MAX_RRM 1.0
#define DEFAULT_ANL_RSP_MIN_SMR -2.0
#define DEFAULT_ANL_RSP_MAX_SMR 0.01
#define DEFAULT_ANL_RSP_DEMAND_INFALL_RS 0
#define DEFAULT_ANL_RSP_SIGMA_TDYN 0.2
#define DEFAULT_ANL_RSP_MIN_WEIGHT 10.0
#define DEFAULT_ANL_RSP_N_BOOTSTRAP 200
#define DEFAULT_ANL_RSP_DO_CORRECTION 1

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initConfigAnalysisRsp(int step);
void initAnalysisRsp(AnalysisRsp *al_rsp);
void runAnalysisRsp(int snap_idx, Halo *halo, void *ls_vp);
int outputFieldsAnalysisRsp(OutputField *outfields, int max_n_fields);
void outputConfigAnalysisRsp(hid_t grp);

#endif

#endif
