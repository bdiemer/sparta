/*************************************************************************************************
 *
 * This unit implements an analysis for halo properties such as SO radii.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <time.h>

#include "analysis_haloprops.h"
#include "analyses.h"
#include "../config.h"
#include "../constants.h"
#include "../memory.h"
#include "../tree.h"
#include "../utils.h"
#include "../halos/halo.h"
#include "../halos/halo_definitions.h"
#include "../halos/halo_so.h"
#include "../tree_potential.h"
#include "../tracers/tracers.h"

#if DO_ANALYSIS_HALOPROPS

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

#define TEST_DIRECT_POTENTIAL 0
#define DEBUG_UNBINDING 0

#define TEST_ORIGINAL_R200M 0
#define TEST_CATALOG_R200M 0

/*
 * Initial guesses for the radii within which we look for SO definitions. For hosts, the initial
 * guess of R200m should be accurate and include most other definitions (though Rvir -> R180m at
 * high redshift, which is a little larger). For subs, the situation is much more complicated
 * and often SO radii cannot be found because they would include the entire host halo.
 */
#define INITIAL_PTL_R_SO_HOSTS 1.2
#define INITIAL_PTL_R_SO_SUBS 2.0

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct ParticlePotentialData
{
	float x[3];
	float radius;
	float pe;
	float ke;
	float pe_ke_ratio;
	int_least8_t bound;
} ParticlePotentialData;

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void doSphericalOverdensity(int snap_idx, int hps_idx, Halo *halo, TreeResults *tr_res,
		AnalysisHaloProps *al_hps);
void doOrbiting(int snap_idx, int hps_idx, Halo *halo, TreeResults *tr_res,
		AnalysisHaloProps *al_hps);

void performUnbinding(ParticlePotentialData *pdata, int n_ptl_included);
void computeSphericalOverdensities(int snap_idx, int hps_idx, Halo *halo, float *radii, int n_ptl,
		AnalysisHaloProps *al_hps, int_least8_t *do_def, int accept_total_mass);
int compareParticlesRadius(const void *a, const void *b);
int compareParticlesBoundness(const void *a, const void *b);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initConfigAnalysisHaloprops(int step)
{
	int i, idx, i_ap;

	switch (step)
	{
	case CONFIG_INIT_STEP_DEFAULT:
		for (i = 0; i < ANALYSIS_HALOPROPS_MAX_SNAPS; i++)
		{
			config.anl_hps_redshifts_actual[i] = -1.0;
			config.anl_hps_snaps[i] = -1;
		}
		for (i = 0; i < MAX_SNAPS; i++)
			config.anl_hps_snap_idx[i] = -1;

		config.anl_hps_r_max_so_host = DEFAULT_ANL_HPS_R_MAX_SO_HOST;
		config.anl_hps_r_max_so_sub = DEFAULT_ANL_HPS_R_MAX_SO_SUB;
		config.anl_hps_r_max_orb_host = DEFAULT_ANL_HPS_R_MAX_ORB_HOST;
		config.anl_hps_r_max_orb_sub = DEFAULT_ANL_HPS_R_MAX_ORB_SUB;
		config.anl_hps_r_unbinding_host = DEFAULT_ANL_HPS_R_UNBINDING_HOST;
		config.anl_hps_r_unbinding_sub = DEFAULT_ANL_HPS_R_UNBINDING_SUB;
		config.anl_hps_iterative_unbinding = DEFAULT_ANL_HPS_ITERATIVE_UNBINDING;
		break;

	case CONFIG_INIT_STEP_CHECK:
		assert(config.anl_hps_r_max_so_host > 0.0);
		assert(config.anl_hps_r_max_so_sub > 0.0);
		assert(config.anl_hps_r_max_orb_host > 0.0);
		assert(config.anl_hps_r_max_orb_sub > 0.0);
		assert(config.anl_hps_r_unbinding_host > 0.0);
		assert(config.anl_hps_r_unbinding_sub > 0.0);
		assertBool(config.anl_hps_iterative_unbinding);
		if (config.anl_hps_n_defs <= 0)
			error(__FFL__,
					"HaloProperties analysis needs at least one output definition (set with the anl_hps_defs parameter).\n");
		if (config.anl_hps_n_redshifts <= 0)
			error(__FFL__,
					"HaloProperties analysis needs at least one output redshift (set with the anl_hps_redshifts parameter).\n");
		break;

	case CONFIG_INIT_STEP_CONFIG:
		/*
		 * Redshifts
		 */
		qsort(config.anl_hps_redshifts, config.anl_hps_n_redshifts, sizeof(float),
				&compareFloatsDescending);
		if (config.anl_hps_redshifts[0] < -0.9999)
		{
			config.anl_hps_n_redshifts = config.n_snaps;
			if (config.anl_hps_n_redshifts > ANALYSIS_HALOPROPS_MAX_SNAPS)
				error(__FFL__,
						"Trying to save halo properties for %d snapshots but ANALYSIS_HALOPROPS_MAX_SNAPS = %d.\n",
						config.anl_hps_n_redshifts, ANALYSIS_HALOPROPS_MAX_SNAPS);
			for (i_ap = 0; i_ap < config.n_snaps; i_ap++)
			{
				config.anl_hps_redshifts_actual[i_ap] = config.snap_z[i_ap];
				config.anl_hps_snaps[i_ap] = i_ap;
				config.anl_hps_snap_idx[i_ap] = i_ap;
			}
		} else
		{
			for (i_ap = 0; i_ap < config.anl_hps_n_redshifts; i_ap++)
			{
				idx = closestInArray(config.snap_z, config.n_snaps, config.anl_hps_redshifts[i_ap]);
				config.anl_hps_redshifts_actual[i_ap] = config.snap_z[idx];
				config.anl_hps_snaps[i_ap] = idx;
				config.anl_hps_snap_idx[idx] = i_ap;
				if (fabs(
						config.anl_hps_redshifts[i_ap]
								- config.anl_hps_redshifts_actual[i_ap]) > ANL_REDSHIFT_TOLERANCE)
					error(__FFL__,
							"Redshift requested for halo properties analysis z = %.4f could not be found, closest match z = %.4f.\n",
							config.anl_hps_redshifts[i_ap], config.anl_hps_redshifts_actual[i_ap]);
			}
		}

		/*
		 * Mass definitions. If any of the definitions require bound-only particles, we set the
		 * anl_hps_do_unbinding parameter.
		 */
		config.anl_hps_do_any_so_all = 0;
		config.anl_hps_do_any_so_bnd = 0;
		config.anl_hps_do_any_so_tcr = 0;
		config.anl_hps_do_any_so_orb = 0;
		config.anl_hps_do_m_orb_all = 0;

		for (i = 0; i < config.anl_hps_n_defs; i++)
		{
			config.anl_hps_defs[i] = stringToHaloDefinition(config.anl_hps_defs_str[i], 0);

			/*
			 * We currently compute only masses and radii regardless of definition.
			 */
			if ((config.anl_hps_defs[i].quantity != HDEF_Q_RADIUS)
					&& (config.anl_hps_defs[i].quantity != HDEF_Q_MASS))
				error(__FFL__, "Halo definition %s does not denote radius or mass.\n",
						config.anl_hps_defs_str[i]);

			/*
			 * We record flag that indicate whether the broad categories of definitions (all, bnd,
			 * tcr) are active at all. Note that a tracer definition turns on all as well, because
			 * host halos have no tcr definition, which is thus replaced by all.
			 */
			if (config.anl_hps_defs[i].type == HDEF_TYPE_SO)
			{

				if (config.anl_hps_defs[i].ptl_select == HDEF_PTLSEL_ALL)
				{
					config.anl_hps_do_any_so_all = 1;
				} else if (config.anl_hps_defs[i].ptl_select == HDEF_PTLSEL_BOUND)
				{
					config.anl_hps_do_any_so_bnd = 1;
				} else if (config.anl_hps_defs[i].ptl_select == HDEF_PTLSEL_TRACER)
				{
					config.anl_hps_do_any_so_all = 1;
					config.anl_hps_do_any_so_tcr = 1;
				} else if (config.anl_hps_defs[i].ptl_select == HDEF_PTLSEL_ORBITING)
				{
					config.anl_hps_do_any_so_orb = 1;
				} else
				{
					error(__FFL__,
							"Halo definition %s does not specify all-particles or bound-only or tracer (all/bnd/tcr/orb).\n",
							config.anl_hps_defs_str[i]);
				}
			} else if (config.anl_hps_defs[i].type == HDEF_TYPE_ORBITING)
			{
				/*
				 * For orbiting definitions, there are really two very different types: the all
				 * orbiting mass type does not need particle radii, whereas the other definitions
				 * are computed similarly to SO definitions.
				 */
				if (config.anl_hps_defs[i].subtype == HDEF_SUBTYPE_ORB_ALL)
				{
					if (config.anl_hps_defs[i].quantity != HDEF_Q_MASS)
						error(__FFL__,
								"Halo definition %s does not denote mass; other quantities are not available for orbiting definition.\n",
								config.anl_hps_defs_str[i]);
					config.anl_hps_do_m_orb_all = 1;
				} else if (config.anl_hps_defs[i].subtype == HDEF_SUBTYPE_ORB_PERCENTILE)
				{
					config.anl_hps_do_any_so_orb = 1;
				} else
				{
					error(__FFL__, "Halo definition %s has an invalid sub-type %d.\n",
							config.anl_hps_defs_str[i], config.anl_hps_defs[i].subtype);
				}

			} else
			{
				error(__FFL__,
						"Found definition %s, but only SO and ORBITING definitions are valid for halo properties analysis.\n",
						config.anl_hps_defs_str[i]);
			}

			/*
			 * This case should never happen because DO_SUBHALO_TRACKING should be automatically on
			 * if this analysis is on, but it does not hurt to check.
			 */
#if (!DO_SUBHALO_TRACKING)
			if (config.anl_hps_defs[i].ptl_select == HDEF_PTLSEL_TRACER)
			error(__FFL__, "Found tcr-based halo definition, but subhalo tracking is off.\n");
#endif
		}
		break;

	case CONFIG_INIT_STEP_PRINT:
		output(0, "[Main] Analysis: Halo properties\n");

		if (config.anl_hps_redshifts[0] <= -0.99999)
		{
			output(0, "[Main]     anl_hps_redshifts             -1 (all)\n");
		} else
		{
			char anl_hps_str[500];

			sprintf(anl_hps_str, "%.2f", config.anl_hps_redshifts[0]);
			for (i = 1; i < config.anl_hps_n_redshifts; i++)
				sprintf(anl_hps_str, "%s, %.2f", anl_hps_str, config.anl_hps_redshifts[i]);
			output(0, "[Main]     anl_hps_redshifts (user)      %s\n", anl_hps_str);

			sprintf(anl_hps_str, "%.2f", config.anl_hps_redshifts[0]);
			for (i = 1; i < config.anl_hps_n_redshifts; i++)
				sprintf(anl_hps_str, "%s, %.2f", anl_hps_str, config.anl_hps_redshifts_actual[i]);
			output(0, "[Main]     anl_hps_redshifts (actual)    %s\n", anl_hps_str);
		}

		char anl_hps_defs_str[500];

		sprintf(anl_hps_defs_str, "%s", config.anl_hps_defs_str[0]);
		for (i = 1; i < config.anl_hps_n_defs; i++)
		{
			sprintf(anl_hps_defs_str, "%s, %s", anl_hps_defs_str, config.anl_hps_defs_str[i]);
			if (strlen(anl_hps_defs_str) > 450)
				error(__FFL__, "String for hps definitions is not long enough, increase length.\n");
		}
		output(0, "[Main]     anl_hps_defs                  %s\n", anl_hps_defs_str);

		output(0, "[Main]     anl_hps_r_max_so_host         %.3f\n", config.anl_hps_r_max_so_host);
		output(0, "[Main]     anl_hps_r_max_so_sub          %.3f\n", config.anl_hps_r_max_so_sub);
		output(0, "[Main]     anl_hps_r_max_orb_host        %.3f\n", config.anl_hps_r_max_orb_host);
		output(0, "[Main]     anl_hps_r_max_orb_sub         %.3f\n", config.anl_hps_r_max_orb_sub);
		output(0, "[Main]     anl_hps_r_unbinding_host      %.3f\n",
				config.anl_hps_r_unbinding_host);
		output(0, "[Main]     anl_hps_r_unbinding_sub       %.3f\n",
				config.anl_hps_r_unbinding_sub);
		output(0, "[Main]     anl_hps_iterative_unbinding   %d\n",
				config.anl_hps_iterative_unbinding);
		break;
	}
}

void initAnalysisHaloProps(AnalysisHaloProps *al_hps)
{
	al_hps->halo_id = INVALID_ID;

#if DO_ANALYSIS_HALOPROPS_RM
	int i, j;

	for (i = 0; i < ANALYSIS_HALOPROPS_MAX_DEFINITIONS; i++)
	{
		for (j = 0; j < ANALYSIS_HALOPROPS_MAX_SNAPS; j++)
		{
			al_hps->defs[i][j] = INVALID_R;
			al_hps->status[i][j] = ANL_HPS_STATUS_UNDEFINED;
		}
	}
#endif
}

/*
 * For the SO search radius, we use the strict definition of host halo. Even if a halo is a sub
 * only temporarily, it lives within a larger halo and might be susceptible to being influenced
 * by that halo's density field.
 */
float searchRadiusAnalysisHaloProps(int snap_idx, Halo *halo)
{
	float r_search_fac;

	r_search_fac = 0.0;

	if (config.anl_hps_snap_idx[snap_idx] >= 0)
	{
		if (config.anl_hps_do_any_so_all)
		{
			if (isHost(halo))
				r_search_fac = fmax(r_search_fac, config.anl_hps_r_max_so_host);
			else
				r_search_fac = fmax(r_search_fac, config.anl_hps_r_max_so_sub);
		}

		if (config.anl_hps_do_any_so_bnd)
		{
			if (isHost(halo))
				r_search_fac = fmax(r_search_fac, config.anl_hps_r_unbinding_host);
			else
				r_search_fac = fmax(r_search_fac, config.anl_hps_r_unbinding_sub);
		}

		if (config.anl_hps_do_any_so_orb)
		{
			if (isHost(halo))
				r_search_fac = fmax(r_search_fac, config.anl_hps_r_max_orb_host);
			else
				r_search_fac = fmax(r_search_fac, config.anl_hps_r_max_orb_sub);
		}
	}

	return r_search_fac;
}

void runAnalysisHaloProps(int snap_idx, Halo *halo, void *ls_vp, void *tree_vp, void *tr_res_vp)
{
	/*
	 * Check whether we should be executing this function at all.
	 */
	int hps_idx;

	hps_idx = config.anl_hps_snap_idx[snap_idx];
	if (hps_idx < 0)
		return;

	/*
	 * Start the actual routine. First, check the tree results pointer
	 */
	int i, j;
	AnalysisHaloProps *al_hps = NULL;
	TreeResults *tr_res;

	tr_res = (TreeResults*) tr_res_vp;
#if CAREFUL
	if (tr_res == NULL)
		error(__FFL__,
				"Found NULL for tree finding results, this should not happen in haloprops routine (halo ID %ld, needs_all_particles %d).\n",
				halo->cd.id, halo->needs_all_ptl);
#endif

	/*
	 * Find halo properties analysis object, or create it
	 */
	if (halo->al[HPS].n == 0)
	{
		al_hps = addAnalysisHaloProps(__FFL__, &(halo->al[HPS]));
		al_hps->halo_id = haloOriginalID(halo);
		for (i = 0; i < config.anl_hps_n_defs; i++)
			for (j = 0; j < config.anl_hps_n_redshifts; j++)
				al_hps->status[i][j] = ANL_HPS_STATUS_HALO_NOT_VALID;
	} else if (halo->al[HPS].n > 1)
	{
		error(__FFL__, "Found %d AnalysisHaloProps objects in halo ID %ld.\n", halo->al[HPS].n,
				halo->cd.id);
	} else
	{
		al_hps = &(halo->al[HPS].al_hps[0]);
	}

	/*
	 * Perform the actual computations in this module. We start with the spherical overdensity
	 * related calculations. This function also takes care of orbit-based definitions.
	 */
#if (DO_ANALYSIS_HALOPROPS_RM || OUTPUT_ANALYSIS_HALOPROPS_ORBITING)
	doSphericalOverdensity(snap_idx, hps_idx, halo, tr_res, al_hps);
#endif

	/*
	 * Compute definitions that rely on an orbiting/infalling distinction using OCT results.
	 */
#if OUTPUT_ANALYSIS_HALOPROPS_ORBITING
	doOrbiting(snap_idx, hps_idx, halo, tr_res, al_hps);
#endif
}

#if OUTPUT_ANALYSIS_HALOPROPS_ORBITING

/*
 * Orbit-related definitions.
 */
void doOrbiting(int snap_idx, int hps_idx, Halo *halo, TreeResults *tr_res,
		AnalysisHaloProps *al_hps)
{
	if (!config.anl_hps_do_m_orb_all)
		return;

	int i, j, n_orbiting;
	ResultOrbitCount *res_oct;

	for (i = 0; i < config.anl_hps_n_defs; i++)
	{
		if ((config.anl_hps_defs[i].type != HDEF_TYPE_ORBITING)
				|| (config.anl_hps_defs[i].subtype != HDEF_SUBTYPE_ORB_ALL))
			continue;

		n_orbiting = 0;
		for (j = 0; j < halo->tt[PTL].rs[OCT].n; j++)
		{
			res_oct = &(halo->tt[PTL].rs[OCT].rs_oct[j]);
			if (res_oct->n_is_lower_limit || (res_oct->n_pericenter > 0))
				n_orbiting += 1;
		}

		al_hps->defs[i][hps_idx] = config.particle_mass * n_orbiting;
		al_hps->status[i][hps_idx] = ANL_HPS_STATUS_SUCCESS;
	}
}

#endif

#if DO_ANALYSIS_HALOPROPS_RM

/*
 * Compute spherical overdensity definitions. We compile a list of particles to be included (which
 * depends on whether the halo is a host or sub), and then compute the SO defs.
 */
void doSphericalOverdensity(int snap_idx, int hps_idx, Halo *halo, TreeResults *tr_res,
		AnalysisHaloProps *al_hps)
{
	int n_ptl_all, n_ptl_include, n_ptl_bound, i, j, k, d, is_host;
	int_least8_t *include_ptl, *do_def;
	float r_phys, v_phys, R200m_spa, *radii_all, *radii, unbinding_radius;
	ParticlePotentialData *pdata;
	Particle *particle;

	/*
	 * Hosts and subs will lead to some fundamental distinctions.
	 */
	is_host = isHost(halo);
	R200m_spa = haloR200m(halo, snap_idx);
	n_ptl_all = tr_res->num_points;
	include_ptl = (int_least8_t*) memAlloc(__FFL__, MID_ANLHPS, n_ptl_all * sizeof(int_least8_t));
	do_def = (int_least8_t*) memAlloc(__FFL__, MID_ANLHPS,
			config.anl_hps_n_defs * sizeof(int_least8_t));

	/*
	 * Compute the physical radii of all particles; we will need this data regardless of exactly
	 * which definitions are chosen.
	 */
	radii_all = (float*) memAlloc(__FFL__, MID_ANLHPS, n_ptl_all * sizeof(float));
	for (i = 0; i < n_ptl_all; i++)
		radii_all[i] = tracerRadius(snap_idx, halo->cd.x, tr_res->points[i]->x);

	/*
	 * Part I: All-particle definitions. Here, we simply take all particles within the maximum
	 * radius.
	 */
	if (config.anl_hps_do_any_so_all)
	{
		int iter, stop_iter, all_defs_found;
		float max_search_radius, include_radius;

		/*
		 * Find all particles included in the calculation. We want to start with a reasonable
		 * guess for the maximum radius rather than the (possibly very large) total search radius.
		 * However, we need do this iteratively because in some cases we can run into the SO_LARGE
		 * error, meaning the density is still above the threshold at the outermost radius.
		 *
		 * We always respect the maximum set by the user for the SO radii. Even if the real
		 * search radius is larger, this might depend on other factors (e.g., which analyses are
		 * active at this snapshot). Using those particles would then make the results of the SO
		 * calculation dependent on unrelated factors, which is highly undesirable.
		 */
		if (is_host)
		{
			include_radius = R200m_spa * INITIAL_PTL_R_SO_HOSTS;
			max_search_radius = R200m_spa * config.anl_hps_r_max_so_host;
		} else
		{
			include_radius = R200m_spa * INITIAL_PTL_R_SO_SUBS;
			max_search_radius = R200m_spa * config.anl_hps_r_max_so_sub;
		}
		include_radius = fmin(include_radius, max_search_radius);

		iter = 0;
		stop_iter = 0;
		while (!stop_iter)
		{
			/*
			 * We follow an aggressive strategy for enlarging the search radius: if there was an
			 * SO_LARGE issue, we immediately go to the maximum search radius, ensuring that the
			 * loop can only ever be executed twice.
			 */
			if (iter > 0)
			{
				include_radius = max_search_radius;
				stop_iter = 1;
			}

			n_ptl_include = 0;
			for (i = 0; i < n_ptl_all; i++)
			{
				include_ptl[i] = (radii_all[i] <= include_radius);
				if (include_ptl[i])
					n_ptl_include++;
			}

			// Create radius array, sort
			radii = (float*) memAlloc(__FFL__, MID_ANLHPS, n_ptl_include * sizeof(float));
			j = 0;
			for (i = 0; i < n_ptl_all; i++)
			{
				if (include_ptl[i])
				{
					radii[j] = radii_all[i];
					j++;
				}
			}
			qsort(radii, n_ptl_include, sizeof(float), &compareFloatsAscending);

			/*
			 * Compute SO for relevant definitions, that is, all-defs or tcr-defs if host. Also
			 * check that we have not yet done this definition.
			 */
			for (i = 0; i < config.anl_hps_n_defs; i++)
			{
				do_def[i] = (config.anl_hps_defs[i].type == HDEF_TYPE_SO);
				do_def[i] = (do_def[i]
						&& ((config.anl_hps_defs[i].ptl_select == HDEF_PTLSEL_ALL)
								|| ((config.anl_hps_defs[i].ptl_select == HDEF_PTLSEL_TRACER)
										&& is_host)));
				do_def[i] = (do_def[i] && (al_hps->status[i][hps_idx] != ANL_HPS_STATUS_SUCCESS)
						&& (al_hps->status[i][hps_idx] != ANL_HPS_STATUS_SO_TOO_SMALL));
			}
			computeSphericalOverdensities(snap_idx, hps_idx, halo, radii, n_ptl_include, al_hps,
					do_def, 0);

			// Free memory
			memFree(__FFL__, MID_ANLHPS, radii, n_ptl_include * sizeof(float));

			/*
			 * Check whether there are any definitions for which we need to try again
			 */
			all_defs_found = 1;
			for (i = 0; i < config.anl_hps_n_defs; i++)
			{
				if ((al_hps->status[i][hps_idx] != ANL_HPS_STATUS_SUCCESS)
						&& (al_hps->status[i][hps_idx] != ANL_HPS_STATUS_SO_TOO_SMALL))
				{
					all_defs_found = 0;
				}
			}
			stop_iter = (stop_iter || all_defs_found);
			iter++;

			if (iter > 2)
				error(__FFL__,
						"Took more than two iterations to look for SO radii, internal error.\n");
		}
	}

	/*
	 * Part II: Bound-only definitions. Here, we select the sub-set of particles within the
	 * unbinding radius, transform them into a particle-potential object, and perform the iterative
	 * unbinding procedure before computing SO definitions.
	 */
	if (config.anl_hps_do_any_so_bnd)
	{
		// Find all particles that are included in the boundness calculation
		if (is_host)
			unbinding_radius = config.anl_hps_r_unbinding_host * R200m_spa;
		else
			unbinding_radius = config.anl_hps_r_unbinding_sub * R200m_spa;
		n_ptl_include = 0;
		for (i = 0; i < n_ptl_all; i++)
		{
			include_ptl[i] = (radii_all[i] <= unbinding_radius);
			if (include_ptl[i])
				n_ptl_include++;
		}

		// Set data necessary for unbinding
		pdata = (ParticlePotentialData*) memAlloc(__FFL__, MID_ANLHPS,
				n_ptl_include * sizeof(ParticlePotentialData));
		j = 0;
		for (i = 0; i < n_ptl_all; i++)
		{
			if (!include_ptl[i])
				continue;
			particle = tr_res->points[i];
			tracerRadiusVelocity(snap_idx, halo->cd.x, halo->cd.v, particle->x, particle->v,
					&r_phys, &v_phys);
			for (d = 0; d < 3; d++)
				pdata[j].x[d] = physicalRadius(particle->x[d], snap_idx);
			pdata[j].radius = r_phys;
			pdata[j].pe = 0.0;
			pdata[j].ke = 0.5 * v_phys * v_phys * CONVERSION_KIN_TO_POT;
			j++;
		}

		// Do the unbinding calculation
		performUnbinding(pdata, n_ptl_include);

		// Count bound particles
		n_ptl_bound = 0;
		for (j = 0; j < n_ptl_include; j++)
		{
			if (pdata[j].bound)
				n_ptl_bound++;
		}

		// Reserve memory and write bound particles' radii into array
		radii = (float*) memAlloc(__FFL__, MID_ANLHPS, n_ptl_bound * sizeof(float));
		j = 0;
		k = 0;
		for (i = 0; i < n_ptl_all; i++)
		{
			if (!include_ptl[i])
				continue;
			if (pdata[j].bound)
			{
				radii[k] = pdata[j].radius;
				k++;
			}
			j++;
		}
		memFree(__FFL__, MID_ANLHPS, pdata, n_ptl_include * sizeof(ParticlePotentialData));

		/*
		 * The sorting here should not be necessary, but we do it to make sure that the boundness
		 * algorithm did not mix up the order of the particles.
		 */
		qsort(radii, n_ptl_bound, sizeof(float), &compareFloatsAscending);

		/*
		 * For bound masses, we accept the total mass, i.e., allow the density to not reach the
		 * lower threshold at large radii, because one can easily imagine a very tightly bound
		 * collection of particles that never reaches the threshold but that does have a
		 * meaningful mass.
		 */
		for (i = 0; i < config.anl_hps_n_defs; i++)
		{
			do_def[i] = ((config.anl_hps_defs[i].type == HDEF_TYPE_SO)
					&& (config.anl_hps_defs[i].ptl_select == HDEF_PTLSEL_BOUND));
		}
		computeSphericalOverdensities(snap_idx, hps_idx, halo, radii, n_ptl_bound, al_hps, do_def,
				1);

		// Free memory
		memFree(__FFL__, MID_ANLHPS, radii, n_ptl_bound * sizeof(float));
	}

	/*
	 * Part III: Compute orbit-based definitions (that include only particles that have had a
	 * pericenter). The definitions considered here are distinct from those
	 */
#if OUTPUT_ANALYSIS_HALOPROPS_ORBITING
	if (config.anl_hps_do_any_so_orb)
	{
		int idx0, idx1;
		float r0, r1, N0, N1, Np, orbiting_search_radius;
		void *ptr;
		ID ptl_id;
		ResultOrbitCount *tmp_res;

		if (is_host)
			orbiting_search_radius = config.anl_hps_r_max_orb_host * R200m_spa;
		else
			orbiting_search_radius = config.anl_hps_r_max_orb_sub * R200m_spa;

		n_ptl_include = 0;
		for (i = 0; i < n_ptl_all; i++)
		{
			include_ptl[i] = 0;
			if (radii_all[i] > orbiting_search_radius)
				continue;

			/*
			 * Try to find OCT result. If it does not exist, or if it has no pericenter count or
			 * lower limit, we still discard the particle.
			 */
			ptl_id = tr_res->points[i]->id;
			ptr = bsearch(&(ptl_id), halo->tt[PTL].rs[OCT].data, halo->tt[PTL].rs[OCT].n,
					rsprops[OCT].size, &compareResults);
			if (ptr == NULL)
				continue;
			tmp_res = (ResultOrbitCount*) ptr;
			if ((tmp_res->n_pericenter == 0) && (!tmp_res->n_is_lower_limit))
				continue;

			/*
			 * At this point, we have an OCT result with at least one pericenter. We add the
			 * particle to the set.
			 */
			include_ptl[i] = 1;
			n_ptl_include++;
		}

		/*
		 * If there are zero orbiting particles, there is nothing we can do except set informative
		 * error statuses.
		 */
		if (n_ptl_include == 0)
		{
			for (i = 0; i < config.anl_hps_n_defs; i++)
			{
				if ((config.anl_hps_defs[i].type == HDEF_TYPE_SO)
						&& (config.anl_hps_defs[i].ptl_select == HDEF_PTLSEL_ORBITING))
				{
					al_hps->status[i][hps_idx] = ANL_HPS_STATUS_SO_TOO_SMALL;
				} else if ((config.anl_hps_defs[i].type == HDEF_TYPE_ORBITING)
						&& (config.anl_hps_defs[i].subtype == HDEF_SUBTYPE_ORB_PERCENTILE))
				{
					al_hps->status[i][hps_idx] = ANL_HPS_STATUS_ORB_ZERO;
				}
			}
		} else
		{
			// Create radius array, sort
			radii = (float*) memAlloc(__FFL__, MID_ANLHPS, n_ptl_include * sizeof(float));
			j = 0;
			for (i = 0; i < n_ptl_all; i++)
			{
				if (include_ptl[i])
				{
					radii[j] = radii_all[i];
					j++;
				}
			}
			qsort(radii, n_ptl_include, sizeof(float), &compareFloatsAscending);

			// Overdensity definitions
			for (i = 0; i < config.anl_hps_n_defs; i++)
			{
				do_def[i] = ((config.anl_hps_defs[i].type == HDEF_TYPE_SO)
						&& (config.anl_hps_defs[i].ptl_select == HDEF_PTLSEL_ORBITING));
			}
			computeSphericalOverdensities(snap_idx, hps_idx, halo, radii, n_ptl_include, al_hps,
					do_def, 1);

			// Percentile definitions
			for (i = 0; i < config.anl_hps_n_defs; i++)
			{
				if ((config.anl_hps_defs[i].type != HDEF_TYPE_ORBITING)
						|| (config.anl_hps_defs[i].subtype != HDEF_SUBTYPE_ORB_PERCENTILE))
					continue;

				/*
				 * Simple definition of mass: must be the percentile of all orbiting particles
				 * found.
				 *
				 * For the radius, we linearly interpolate between the two closest bins in Np
				 * space. If there is only one particle, we interpolate between N = 0, r = 0 and
				 * that particle's position. Otherwise, we interpolate between the radii and
				 * numbers of two adjacent particles.
				 */
				if (config.anl_hps_defs[i].quantity == HDEF_Q_MASS)
				{
					al_hps->defs[i][hps_idx] = config.particle_mass * n_ptl_include
							* config.anl_hps_defs[i].percentile * 0.01;
				} else if (config.anl_hps_defs[i].quantity == HDEF_Q_RADIUS)
				{
					Np = config.anl_hps_defs[i].percentile * 0.01 * n_ptl_include;
					if (Np < 1.0)
					{
						idx0 = -1;
						r0 = 0.0;
						N0 = 0.0;
					} else
					{
						N0 = floor(Np);
						idx0 = (int) N0;
						r0 = radii[idx0];
					}
					idx1 = idx0 + 1;
					if (idx1 >= n_ptl_include)
					{
						r1 = radii[n_ptl_include - 1];
						N1 = (float) n_ptl_include;
					} else
					{
						r1 = radii[idx1];
						N1 = N0 + 1.0;
					}
#if PARANOID
					if ((r1 < r0) || (N1 < N0) || (Np < N0))
						error(__FFL__,
								"Invalid interpolation data in ORB percentile (%d, %d, %.4f, %.4f, %.4f, %.4f).\n",
								idx0, idx1, r0, r1, N0, N1);
#endif
					al_hps->defs[i][hps_idx] = r0 + (Np - N0) * (r1 - r0);
				} else
				{
					error(__FFL__, "Invalid quantity in definition %s.\n",
							config.anl_hps_defs_str[i]);
				}

				al_hps->status[i][hps_idx] = ANL_HPS_STATUS_SUCCESS;
			}

			// Free memory
			memFree(__FFL__, MID_ANLHPS, radii, n_ptl_include * sizeof(float));
		}
	}
#endif

	/*
	 * Free some memory
	 */
	memFree(__FFL__, MID_ANLHPS, radii_all, n_ptl_all * sizeof(float));
	memFree(__FFL__, MID_ANLHPS, include_ptl, n_ptl_all * sizeof(int_least8_t));

	/*
	 * Part IV: Compute tracer-based masses for subhalos
	 */
	if (config.anl_hps_do_any_so_tcr && (!is_host))
	{
		// Collect radii of tracers, sort
		n_ptl_include = halo->tt[PTL].tcr.n;
		radii = (float*) memAlloc(__FFL__, MID_ANLHPS, n_ptl_include * sizeof(float));
		for (i = 0; i < n_ptl_include; i++)
			radii[i] = halo->tt[PTL].tcr.tcr[i].r[STCL_TCR - 1];
		qsort(radii, n_ptl_include, sizeof(float), &compareFloatsAscending);

		// Compute SO for relevant definitions, that is, all-defs or tcr-defs if host
		for (i = 0; i < config.anl_hps_n_defs; i++)
		{
			do_def[i] = ((config.anl_hps_defs[i].type == HDEF_TYPE_SO)
					&& (config.anl_hps_defs[i].ptl_select == HDEF_PTLSEL_TRACER));
		}
		computeSphericalOverdensities(snap_idx, hps_idx, halo, radii, n_ptl_include, al_hps, do_def,
				1);

		// Free memory
		memFree(__FFL__, MID_ANLHPS, radii, n_ptl_include * sizeof(float));
	}

	/*
	 * Free remaining memory
	 */
	memFree(__FFL__, MID_ANLHPS, do_def, config.anl_hps_n_defs * sizeof(int_least8_t));

	/*
	 * Compare to original routine
	 */
#if TEST_ORIGINAL_R200M
	float ratio_orig, R200m_orig, M200m_orig, R200m_cat_all, M200m_cat_all, R200m_spa_all;

	R200m_cat_all = physicalRadius(halo->cd.R200m_all_com, snap_idx);
	M200m_cat_all = soRtoM(R200m_cat_all, config.this_snap_rho_200m);
	computeR200m_pList(snap_idx, (float *) p_radii_vp, tr_res->num_points, M200m_cat_all,
			&R200m_orig, &M200m_orig);

	if ((config.anl_hps_defs[0].overdensity != 200.0)
			|| (config.anl_hps_defs[0].type != HDEF_TYPE_SO_MATTER)
			|| (config.anl_hps_defs[0].ptl_select != HDEF_PTLSEL_ALL)
			|| (config.anl_hps_defs[0].radius_mass != HDEF_Q_RADIUS))
	{
		error(__FFL__, "Cannot compare to original R200m if first definition is not R200m_all.\n");
	}

	R200m_spa_all = al_hps->RMdefs[0][snap_idx];
	ratio_orig = R200m_spa_all / R200m_orig;

	if (((ratio_orig < 0.95) || (ratio_orig > 1.05)) && (halo->cd.phantom == 0) && (n_ptl_include > 10) && !isGhost(halo))
	{
		output(0,
				"HPS_DEBUG: Diff to orig. ID %ld, Cat %7.3f,  orig %7.3f,  new %7.3f,  ratio %.3f\n",
				halo->cd.id, R200m_cat_all, R200m_orig, R200m_spa_all, ratio_orig);

		for (i = 0; i < n_ptl_include; i++)
		output(0, "         PTL %4d  r %7.3f  bnd %d\n", i, radii[i], bound[i]);
	}
#endif
}

/*
 * The actual spherical overdensity calculation. The calculation is performed only for those defs
 * where do_def is True.
 *
 * If accept_total_mass is on, we allow masses that include all particles. This makes sense if we
 * have a limited particle distribution that makes up a halo, but doesn't make sense where the
 * true distribution is infinite and we are limited by a selection radius.
 */
void computeSphericalOverdensities(int snap_idx, int hps_idx, Halo *halo, float *radii, int n_ptl,
		AnalysisHaloProps *al_hps, int_least8_t *do_def, int accept_total_mass)
{
	int i, j, counter_mdef[config.anl_hps_n_defs];
	float r, rho, z, rho_factor, r3_inv, M_Delta, rho_delta[config.anl_hps_n_defs];

	/*
	 * Compute overdensities at this snapshot.
	 */
	z = config.snap_z[snap_idx];
	for (j = 0; j < config.anl_hps_n_defs; j++)
	{
		if (!do_def[j])
			continue;
		rho_delta[j] = densityThreshold(&(config.cosmo), z, &(config.anl_hps_defs[j]));
		counter_mdef[j] = 0;
	}

	/*
	 * Go through particles
	 */
	rho_factor = config.particle_mass / (4.0 / 3.0 * MATH_pi);
	for (i = 0; i < n_ptl; i++)
	{
		/*
		 * Check density; if they are above the threshold, set the corresponding mass/radius. Note
		 * that we do NOT stop the loop at that point because multiple threshold crossings can
		 * occur. We want the outermost one.
		 */
		r = radii[i];
		r3_inv = 1.0 / (r * r * r);
		rho = (i + 1) * rho_factor * r3_inv;
		for (j = 0; j < config.anl_hps_n_defs; j++)
		{
			if (!do_def[j])
				continue;
			if (rho > rho_delta[j])
				counter_mdef[j] = i + 1;
		}
	}

	/*
	 * Compute each SO definition. We check for 0 mass (density threshold not reached at center) and
	 * all particles included (density threshold never low enough). We do not check whether the
	 * resulting radius is smaller than the force resolution; such a result would likely be spurious,
	 * but it is up to MORIA / the user to impose such cuts.
	 */
	for (j = 0; j < config.anl_hps_n_defs; j++)
	{
		if (!do_def[j])
			continue;

		/*
		 * Check that density threshold was actually reached
		 */
		if (counter_mdef[j] == 0)
		{
			debugHalo(halo, "anl_hps: SO threshold not reached in def %s, %d particles.",
					config.anl_hps_defs_str[j], n_ptl);
			al_hps->status[j][hps_idx] = ANL_HPS_STATUS_SO_TOO_SMALL;

		} else if ((counter_mdef[j] < n_ptl) || (accept_total_mass && (counter_mdef[j] == n_ptl)))
		{
			M_Delta = counter_mdef[j] * config.particle_mass;
			if (config.anl_hps_defs[j].quantity == HDEF_Q_MASS)
			{
				al_hps->defs[j][hps_idx] = M_Delta;
			} else
			{
				al_hps->defs[j][hps_idx] = soMtoR(M_Delta, rho_delta[j]);
			}
			al_hps->status[j][hps_idx] = ANL_HPS_STATUS_SUCCESS;

		} else if (!accept_total_mass && (counter_mdef[j] == n_ptl))
		{
			debugHalo(halo, "anl_hps: SO threshold not reached in def %s, %d particles.",
					config.anl_hps_defs_str[j], n_ptl);
			al_hps->status[j][hps_idx] = ANL_HPS_STATUS_SO_TOO_LARGE;
		} else
		{
			error(__FFL__, "In SO routine, counter reached %d but there are only %d particles.\n",
					counter_mdef[j], n_ptl);
		}
	}

#if TEST_CATALOG_R200M
	float R200m_spa_old, R200m_cat_all, R200m_cat_bnd, M200m_cat_all, M200m_cat_bnd, R200m_spa_bnd,
			M200m_spa_bnd, R200m_spa_all, M200m_spa_all;

	R200m_spa_old = haloR200m(halo, snap_idx);
	M200m_cat_bnd = halo->cd.M_bound;
	R200m_cat_all = physicalRadius(halo->cd.R200m_all_com, snap_idx);
	M200m_cat_all = soRtoM(R200m_cat_all, config.this_snap_rho_200m);
	R200m_cat_bnd = soMtoR(M200m_cat_bnd, config.this_snap_rho_200m);

	R200m_spa_bnd = -1.0;
	M200m_spa_bnd = -1.0;
	R200m_spa_all = -1.0;
	M200m_spa_all = -1.0;

	float ratio_spa = R200m_spa_bnd / R200m_spa_all;
	float ratio_cat = R200m_cat_bnd / R200m_cat_all;

	float ratio_all_cat_spa = R200m_spa_all / R200m_cat_all;
	float ratio_bnd_cat_spa = R200m_spa_bnd / R200m_cat_bnd;

	int N200m = round(M200m_cat_all / config.particle_mass);

	/*
	 * Output halos where the all radii agree, but where the bound radius is different from the all
	 * radius.
	 */
	if ((ratio_cat < 0.95) && (fabs(ratio_all_cat_spa - 1.0) < 0.1) && (N200m > 50)
			&& (halo->cd.phantom == 0) && !isGhost(halo))
	{
		output(0,
				"HPS_DEBUG: Halo %5ld, R200m_all cat/spa %.2e %.2e (r %.3f), R200m_bnd cat/spa %.2e %.2e (r %.3f) bound/all cat/spa %.3f %.3f prev r %.3f xyz %.3f %.3f %.3f\n",
				halo->cd.id, R200m_cat_all, R200m_spa_all, ratio_all_cat_spa, R200m_cat_bnd,
				R200m_spa_bnd, ratio_bnd_cat_spa, ratio_cat, ratio_spa,
				R200m_spa_all / R200m_spa_old, halo->cd.x[0], halo->cd.x[1], halo->cd.x[2]);

		output(0,
				"HPS_DEBUG: Halo %5ld, M200m_all cat/spa %.2e %.2e, M200m_bnd cat/spa %.2e %.2e\n",
				halo->cd.id, M200m_cat_all, M200m_spa_all, M200m_cat_bnd, M200m_spa_bnd);
	}

#endif
}

/*
 * Unbind particles
 */
void performUnbinding(ParticlePotentialData *pdata, int n_ptl)
{
	int i, j, converged, n_ptl_bound, n_ptl_bound_last, iter;
	TreePotentialPoint *pp;

	converged = 0;
	n_ptl_bound = n_ptl;
	n_ptl_bound_last = n_ptl_bound;
	iter = 0;

	while (!converged)
	{
		if (iter > 0)
			qsort(pdata, n_ptl_bound, sizeof(ParticlePotentialData), &compareParticlesRadius);

		/*
		 * Copy into other structure and compute potential, then copy back into standard particle
		 * structure.
		 */
		pp = (TreePotentialPoint*) memAlloc(__FFL__, MID_ANLHPS,
				n_ptl_bound * sizeof(TreePotentialPoint));
		for (i = 0; i < n_ptl_bound; i++)
			for (j = 0; j < 3; j++)
				pp[i].x[j] = pdata[i].x[j];
		computeTreePotential(pp, n_ptl_bound, config.particle_mass, config.this_snap_force_res,
				config.potential_err_tol, config.potential_points_per_leaf);
		for (i = 0; i < n_ptl_bound; i++)
			pdata[i].pe = pp[i].pe;
		memFree(__FFL__, MID_ANLHPS, pp, n_ptl_bound * sizeof(TreePotentialPoint));

		/*
		 * Note that we have applied the unit conversion to the kinetic energy field already.
		 */
		for (i = 0; i < n_ptl_bound; i++)
		{
			pdata[i].pe_ke_ratio = pdata[i].pe / pdata[i].ke;
			pdata[i].bound = (pdata[i].pe > pdata[i].ke);
		}
		qsort(pdata, n_ptl_bound, sizeof(ParticlePotentialData), &compareParticlesBoundness);

		/*
		 * If desired, test against direct summation potential
		 */
#if TEST_DIRECT_POTENTIAL

		float tmp;
		float diff, direct_pot[n_ptl_bound];

		for (i = 0; i < n_ptl_bound; i++)
		{
			direct_pot[i] = pdata[i].pe;
			pdata[i].pe = 0.0;
		}
		computeDirectPotential(pdata, n_ptl_bound);
		for (i = 0; i < n_ptl_bound; i++)
		{
			tmp = pdata[i].pe;
			pdata[i].pe = direct_pot[i];
			direct_pot[i] = tmp;
		}

		for (i = 0; i < n_ptl_bound; i++)
		{
			diff = fabs(pdata[i].pe / direct_pot[i] - 1.0);
			if ((diff > 0.2) && (iter == 0))
			output(0, "DEBUG particle %6d r %6.2f  pot %.3e  dir %.3e diff %.2f\n", i,
					pdata[i].radius, pdata[i].pe, direct_pot[i], diff);
		}
#endif

		/*
		 * If we are unbinding iteratively, count the number of bound particles and compare it to
		 * the last iteration.
		 */
		if (config.anl_hps_iterative_unbinding)
		{
			for (; n_ptl_bound > 0; n_ptl_bound--)
			{
				if (pdata[n_ptl_bound - 1].pe_ke_ratio >= 1.0)
					break;
			}
			converged = (n_ptl_bound_last - n_ptl_bound < 2) || (n_ptl_bound < 10);

#if DEBUG_UNBINDING
		if (((float) n_ptl_bound / (float) n_ptl_bound_last) < 0.5)
		{
			output(0, "Iteration %d, last %5d new %5d diff %3d\n", iter, n_ptl_bound_last,
					n_ptl_bound, n_ptl_bound_last - n_ptl_bound);
			for (i = 0; i < n_ptl_bound_last; i++)
			output(0, "    PTL %4d  r %7.3f  bnd %.3f\n", i, pdata[i].radius, pdata[i].pe_ke_ratio);
		}
#endif

			iter++;
			n_ptl_bound_last = n_ptl_bound;
		} else
		{
			converged = 1;
		}
	}
}

int compareParticlesRadius(const void *a, const void *b)
{
	const ParticlePotentialData *da = (const ParticlePotentialData*) a;
	const ParticlePotentialData *db = (const ParticlePotentialData*) b;

	return (da->radius > db->radius) - (da->radius < db->radius);
}

int compareParticlesBoundness(const void *a, const void *b)
{
	const ParticlePotentialData *da = (const ParticlePotentialData*) a;
	const ParticlePotentialData *db = (const ParticlePotentialData*) b;

	return (da->pe_ke_ratio < db->pe_ke_ratio) - (da->pe_ke_ratio > db->pe_ke_ratio);
}
#endif

/*
 * For 1D fields, there is no need to set any dimensions. For higher-dimensional fields, the higher
 * dimensions must be fixed.
 */
int outputFieldsAnalysisHaloProps(OutputField *outfields, int max_n_fields)
{
	int counter;

	counter = 0;

	outfields[counter].dtype = DTYPE_INT64;
	outfields[counter].offset = offsetof(AnalysisHaloProps, halo_id);
	sprintf(outfields[counter].name, "halo_id");
	counter++;

#if OUTPUT_ANALYSIS_HALOPROPS_RM
	int i;

	for (i = 0; i < config.anl_hps_n_defs; i++)
	{
		// Definition
		outfields[counter].n_dims = 2;
		outfields[counter].dim_ext[1] = config.anl_hps_n_redshifts;
		outfields[counter].dtype = DTYPE_FLOAT;
		outfields[counter].offset = offsetof(AnalysisHaloProps, defs)
				+ i * ANALYSIS_HALOPROPS_MAX_SNAPS * sizeof(float);
		sprintf(outfields[counter].name, config.anl_hps_defs_str[i]);
		counter++;
		if (counter >= max_n_fields)
			error(__FFL__,
					"Reached maximum number (%d) of analysis output fields in HaloProps analysis, please increase.\n",
					max_n_fields);

		// Status of definition
		outfields[counter].n_dims = 2;
		outfields[counter].dim_ext[1] = config.anl_hps_n_redshifts;
		outfields[counter].dtype = DTYPE_INT8;
		outfields[counter].offset = offsetof(AnalysisHaloProps, status)
				+ i * ANALYSIS_HALOPROPS_MAX_SNAPS * sizeof(int_least8_t);
		sprintf(outfields[counter].name, "status_%s", config.anl_hps_defs_str[i]);
		counter++;
		if (counter >= max_n_fields)
			error(__FFL__,
					"Reached maximum number (%d) of analysis output fields in HaloProps analysis, please increase.\n",
					max_n_fields);
	}
#endif

	return counter;
}

void outputConfigAnalysisHaloProps(hid_t grp)
{
	int i;
	char anl_hps_defs_str[200];

	if (config.anl_hps_redshifts[0] < 0.0)
	{
		hdf5WriteAttributeFloat1D(grp, "snap_redshifts_requested", 1, config.anl_hps_redshifts);
	} else
	{
		hdf5WriteAttributeFloat1D(grp, "snap_redshifts_requested", config.anl_hps_n_redshifts,
				config.anl_hps_redshifts);
	}
	hdf5WriteAttributeFloat1D(grp, "snap_redshifts_actual", config.anl_hps_n_redshifts,
			config.anl_hps_redshifts_actual);
	hdf5WriteAttributeInt1D(grp, "snap_idxs", config.anl_hps_n_redshifts, config.anl_hps_snaps);
	hdf5WriteAttributeInt1D(grp, "snap_idxs_in_anl", config.n_snaps, config.anl_hps_snap_idx);

	sprintf(anl_hps_defs_str, "%s", config.anl_hps_defs_str[0]);
	for (i = 1; i < config.anl_hps_n_defs; i++)
		sprintf(anl_hps_defs_str, "%s,%s", anl_hps_defs_str, config.anl_hps_defs_str[i]);
	hdf5WriteAttributeString(grp, "defs", anl_hps_defs_str);

	hdf5WriteAttributeFloat(grp, "r_max_so_host", config.anl_hps_r_max_so_host);
	hdf5WriteAttributeFloat(grp, "r_max_so_sub", config.anl_hps_r_max_so_sub);
	hdf5WriteAttributeFloat(grp, "r_max_orb_host", config.anl_hps_r_max_orb_host);
	hdf5WriteAttributeFloat(grp, "r_max_orb_sub", config.anl_hps_r_max_orb_sub);
	hdf5WriteAttributeFloat(grp, "r_unbinding_host", config.anl_hps_r_unbinding_host);
	hdf5WriteAttributeFloat(grp, "r_unbinding_sub", config.anl_hps_r_unbinding_sub);
	hdf5WriteAttributeInt(grp, "iterative_unbinding", config.anl_hps_iterative_unbinding);
}

#endif
