/*************************************************************************************************
 *
 * This unit implements the splashback radius analysis.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <time.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_fit.h>
#include <gsl/gsl_sf_erf.h>

#include "analysis_rsp.h"
#include "analyses.h"
#include "../config.h"
#include "../constants.h"
#include "../memory.h"
#include "../utils.h"
#include "../halos/halo.h"
#include "../halos/halo_definitions.h"
#include "../results/result_splashback.h"

#if DO_ANALYSIS_RSP

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Sample selection
 */
#define MIN_SNAPS_RSP 4
#define RSP_N_SIGMA_SEARCH 3.0

/*
 * Bootstrap percentiles
 */
#define USE_BSEARCH_PERC 1

/*
 * Correction for asymmetric bins
 */
#define CORR_N_FIT 4
#define CORR_N_FIT_RANGE_MAX 8
#define CORR_MAX_WEIGHT 0.5

/*
 * Processing variables; note that these are not necessarily the same as the possible quantities
 * in HDEF, since not all HDEF quantities have splashback equivalents that can be computed by this
 * module (e.g., vmax).
 */
enum
{
	Q_RADIUS, Q_MASS, Q_N
};

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct
{
	float qsp[Q_N];
	float weight;
} SplashbackEvent;

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void computeRsp(int snap_idx, Halo *halo, ResultSplashback *sbk, int n_sbk);
void computeMean(int idx_t, SplashbackEvent *spe, int n_bin, float ***qsp_mean,
		float ***qsp_mean_err);
float percentile(int n, float *qsp, float *weights, float *cuml, float perc);
void computePercentiles(int idx_t, AnalysisRsp *al_rsp, SplashbackEvent *spe_orig, int n_ev);
void computeCorrectionFactor(int i_snap, int i_anl, AnalysisRsp *al_rsp, float t_max_target,
		float t_max, float sigma, float t_range, float **qsp_mean, float **qsp_mean_err,
		float **corr_factor);
float weight_gaussian(float x, float x0, float sigma);
int compareEventsByRadius(const void *a, const void *b);
int compareEventsByMass(const void *a, const void *b);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initAnalysisRsp(AnalysisRsp *al_rsp)
{
	int i, j;

	al_rsp->halo_id = INVALID_ID;

	for (i = 0; i < ANALYSIS_RSP_MAX_SNAPS; i++)
	{
		al_rsp->status[i] = ANL_RSP_STATUS_UNDEFINED;

		for (j = 0; j < ANALYSIS_RSP_MAX_DEFINITIONS; j++)
			al_rsp->defs[j][i] = 0.0;
	}
}

void initConfigAnalysisRsp(int step)
{
	int i, j, l, found, idx, i_ap, n_perc;

	switch (step)
	{
	case CONFIG_INIT_STEP_DEFAULT:
		/*
		 * Redshifts
		 */
		for (i = 0; i < ANALYSIS_RSP_MAX_SNAPS; i++)
		{
			config.anl_rsp_redshifts_actual[i] = -1.0;
			config.anl_rsp_snaps[i] = -1;
		}
		for (i = 0; i < MAX_SNAPS; i++)
			config.anl_rsp_snap_idx[i] = -1;

		/*
		 * Definitions
		 */
		config.anl_rsp_n_percentiles = 0;
		config.anl_rsp_n_defs = 0;
		for (i = 0; i < ANALYSIS_RSP_MAX_DEFINITIONS; i++)
		{
			config.anl_rsp_percentiles[i] = 0;
			initHaloDefinition(&(config.anl_rsp_defs[i]));
		}

		/*
		 * Other parameters
		 */
		config.anl_rsp_min_rrm = DEFAULT_ANL_RSP_MIN_RRM;
		config.anl_rsp_max_rrm = DEFAULT_ANL_RSP_MAX_RRM;
		config.anl_rsp_min_smr = DEFAULT_ANL_RSP_MIN_SMR;
		config.anl_rsp_max_smr = DEFAULT_ANL_RSP_MAX_SMR;
		config.anl_rsp_demand_infall_rs = DEFAULT_ANL_RSP_DEMAND_INFALL_RS;
		config.anl_rsp_sigma_tdyn = DEFAULT_ANL_RSP_SIGMA_TDYN;
		config.anl_rsp_min_weight = DEFAULT_ANL_RSP_MIN_WEIGHT;
		config.anl_rsp_n_bootstrap = DEFAULT_ANL_RSP_N_BOOTSTRAP;
		config.anl_rsp_do_correction = DEFAULT_ANL_RSP_DO_CORRECTION;
		break;

	case CONFIG_INIT_STEP_CHECK:
		assert(config.anl_rsp_min_rrm >= 0.0);
		assert(config.anl_rsp_max_rrm >= 0.0);
		assert(config.anl_rsp_max_rrm > config.anl_rsp_min_rrm);
		assert(config.anl_rsp_max_smr >= 0.0);
		assert(config.anl_rsp_max_smr > config.anl_rsp_min_smr);
		assertBool(config.anl_rsp_demand_infall_rs);
		assert(config.anl_rsp_sigma_tdyn >= 0.0);
		assert(config.anl_rsp_min_weight >= 0.0);
		assert(config.anl_rsp_n_bootstrap > 0);
		assertBool(config.anl_rsp_do_correction);
		if (config.anl_rsp_n_defs <= 0)
			error(__FFL__,
					"Rsp analysis needs at least one output definition (set with the anl_rsp_defs parameter).\n");
		if (config.anl_rsp_n_redshifts <= 0)
			error(__FFL__,
					"Rsp analysis needs at least one output redshift (set with the anl_rsp_redshifts parameter).\n");
		break;

	case CONFIG_INIT_STEP_CONFIG:
		/*
		 * Redshifts
		 */
		qsort(config.anl_rsp_redshifts, config.anl_rsp_n_redshifts, sizeof(float),
				&compareFloatsDescending);
		if (config.anl_rsp_redshifts[0] < -0.9999)
		{
			config.anl_rsp_n_redshifts = config.n_snaps;
			if (config.anl_rsp_n_redshifts > ANALYSIS_RSP_MAX_SNAPS)
				error(__FFL__,
						"Trying to save halo properties for %d snapshots but ANALYSIS_RSP_MAX_SNAPS = %d.\n",
						config.anl_rsp_n_redshifts, ANALYSIS_RSP_MAX_SNAPS);
			for (i_ap = 0; i_ap < config.n_snaps; i_ap++)
			{
				config.anl_rsp_redshifts_actual[i_ap] = config.snap_z[i_ap];
				config.anl_rsp_snaps[i_ap] = i_ap;
				config.anl_rsp_snap_idx[i_ap] = i_ap;
			}
		} else
		{
			for (i_ap = 0; i_ap < config.anl_rsp_n_redshifts; i_ap++)
			{
				idx = closestInArray(config.snap_z, config.n_snaps, config.anl_rsp_redshifts[i_ap]);
				config.anl_rsp_redshifts_actual[i_ap] = config.snap_z[idx];
				config.anl_rsp_snaps[i_ap] = idx;
				config.anl_rsp_snap_idx[idx] = i_ap;
				if (fabs(
						config.anl_rsp_redshifts[i_ap]
								- config.anl_rsp_redshifts_actual[i_ap]) > ANL_REDSHIFT_TOLERANCE)
					error(__FFL__,
							"Redshift requested for halo properties analysis z = %.4f could not be found, closest match z = %.4f.\n",
							config.anl_rsp_redshifts[i_ap], config.anl_rsp_redshifts_actual[i_ap]);
			}
		}

		/*
		 * Mass definitions.
		 */
		config.anl_rsp_n_percentiles = 0;
		for (i = 0; i < ANALYSIS_RSP_MAX_DEFINITIONS; i++)
			config.anl_rsp_percentiles[i] = 0.0;

		for (i = 0; i < config.anl_rsp_n_defs; i++)
		{
			config.anl_rsp_defs[i] = stringToHaloDefinition(config.anl_rsp_defs_str[i], 0);
			config.anl_rsp_do_bootstrap = 0;

			// Duplicates
			for (j = 0; j < i; j++)
			{
				if (compareHaloDefinitions(config.anl_rsp_defs[i], config.anl_rsp_defs[j])
						== HDEF_DIFF_NONE)
					error(__FFL__, "In Rsp analysis, definitions %s and %s are the same.\n",
							config.anl_rsp_defs_str[i], config.anl_rsp_defs_str[j]);
			}

			// Only R/M
			if ((config.anl_rsp_defs[i].quantity != HDEF_Q_RADIUS)
					&& (config.anl_rsp_defs[i].quantity != HDEF_Q_MASS))
				error(__FFL__, "Halo definition %s does not denote radius or mass.\n",
						config.anl_rsp_defs_str[i]);

			// Only splashback
			if (config.anl_rsp_defs[i].type != HDEF_TYPE_SPLASHBACK)
				error(__FFL__,
						"Halo definition %s does not denote mean or percentile splashback radius (type %d).\n",
						config.anl_rsp_defs_str[i], config.anl_rsp_defs[i].type);

			// All particles only
			if ((config.anl_rsp_defs[i].ptl_select != HDEF_PTLSEL_DEFAULT)
					&& (config.anl_rsp_defs[i].ptl_select != HDEF_PTLSEL_ALL))
				error(__FFL__,
						"Halo definition %s does not include all particles, cannot be computed for splashback.\n",
						config.anl_rsp_defs_str[i]);

			// Only now
			if (config.anl_rsp_defs[i].time != HDEF_TIME_NOW)
				error(__FFL__,
						"Halo definition %s is not at current time, cannot be computed for splashback.\n",
						config.anl_rsp_defs_str[i]);

			// Source
			if (config.anl_rsp_defs[i].source != HDEF_SOURCE_ANY)
				error(__FFL__, "Halo definition %s has a source, not valid for splashback.\n",
						config.anl_rsp_defs_str[i]);

			// Error
			if ((config.anl_rsp_defs[i].is_error != HDEF_ERR_NO)
					&& (config.anl_rsp_defs[i].is_error != HDEF_ERR_1SIGMA))
				error(__FFL__,
						"Halo definition %s is an error other than 1-sigma, not valid for splashback.\n",
						config.anl_rsp_defs_str[i]);

			// Add to list of percentiles
			if (config.anl_rsp_defs[i].subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE)
			{
				found = 0;
				for (j = 0; j < config.anl_rsp_n_percentiles; j++)
				{
					if ((int) config.anl_rsp_defs[i].percentile == config.anl_rsp_percentiles[j])
					{
						found = 1;
						break;
					}
				}
				if (!found)
				{
					config.anl_rsp_percentiles[config.anl_rsp_n_percentiles] =
							(int) config.anl_rsp_defs[i].percentile;
					config.anl_rsp_n_percentiles++;
				}
			}

			// Check whether this definition needs bootstrapping
			if ((config.anl_rsp_defs[i].subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE)
					&& (config.anl_rsp_defs[i].is_error == HDEF_ERR_1SIGMA))
			{
				config.anl_rsp_do_bootstrap = 1;
			}
		}
		n_perc = config.anl_rsp_n_percentiles;

		/*
		 * Sort the percentiles in ascending order
		 */
		qsort(config.anl_rsp_percentiles, n_perc, sizeof(int), &compareIntegersAscending);

		/*
		 * For later convenience, set the indices of the definitions that correspond to mean/perc,
		 * mass and radius, as well as errors. The mean comes behind all the percentiles as the
		 * last element.
		 */
		j = 0;
		for (i = 0; i < config.anl_rsp_n_defs; i++)
		{
			if (config.anl_rsp_defs[i].quantity == HDEF_Q_RADIUS)
				j = 0;
			else if (config.anl_rsp_defs[i].quantity == HDEF_Q_MASS)
				j = 1;
			else
				error(__FFL__, "Halo definition %s does not denote radius or mass.\n",
						config.anl_rsp_defs_str[i]);

			idx = INVALID_IDX;
			if (config.anl_rsp_defs[i].subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN)
			{
				idx = n_perc;
			} else if (config.anl_rsp_defs[i].subtype
					== HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE)
			{
				for (l = 0; l < n_perc; l++)
				{
					if ((int) config.anl_rsp_defs[i].percentile == config.anl_rsp_percentiles[l])
						idx = l;
				}
			} else
			{
				error(__FFL__,
						"Halo definition %s does not denote mean or percentile splashback radius.\n",
						config.anl_rsp_defs_str[i]);
			}
			if (idx == INVALID_IDX)
				error(__FFL__, "Error in processing splashback definitions.\n");

			/*
			 * We set a pointer to this def into the def_idx array, and set the flag of the
			 * definition, indicating that it was added by the user and should be output.
			 */
			config.anl_rsp_defs[i].do_output = 1;
		}

		/*
		 * Convert definitions back into strings to be sure to get a unified string format, and to
		 * include definitions that were added manually above.
		 */
		for (i = 0; i < config.anl_rsp_n_defs; i++)
			haloDefinitionToString(config.anl_rsp_defs[i], config.anl_rsp_defs_str[i]);
		break;

	case CONFIG_INIT_STEP_PRINT:
		output(0, "[Main] Analysis: Splashback radius\n");

		if (config.anl_rsp_redshifts[0] <= -0.99999)
		{
			output(0, "[Main]     anl_rsp_redshifts             -1 (all)\n");
		} else
		{
			char anl_rsp_str[500];

			sprintf(anl_rsp_str, "%.2f", config.anl_rsp_redshifts[0]);
			for (i = 1; i < config.anl_rsp_n_redshifts; i++)
				sprintf(anl_rsp_str, "%s, %.2f", anl_rsp_str, config.anl_rsp_redshifts[i]);
			output(0, "[Main]     anl_rsp_redshifts (user)      %s\n", anl_rsp_str);

			sprintf(anl_rsp_str, "%.2f", config.anl_rsp_redshifts[0]);
			for (i = 1; i < config.anl_rsp_n_redshifts; i++)
				sprintf(anl_rsp_str, "%s, %.2f", anl_rsp_str, config.anl_rsp_redshifts_actual[i]);
			output(0, "[Main]     anl_rsp_redshifts (actual)    %s\n", anl_rsp_str);
		}

		char anl_rsp_defs_str[500];

		sprintf(anl_rsp_defs_str, "%s", config.anl_rsp_defs_str[0]);
		for (i = 1; i < config.anl_rsp_n_defs; i++)
		{
			sprintf(anl_rsp_defs_str, "%s, %s", anl_rsp_defs_str, config.anl_rsp_defs_str[i]);
			if (strlen(anl_rsp_defs_str) > 450)
				error(__FFL__, "String for rsp definitions is not long enough, increase length.\n");
			if (!config.anl_rsp_defs[i].do_output)
				sprintf(anl_rsp_defs_str, "%s(x)", anl_rsp_defs_str);
		}

		output(0, "[Main]     anl_rsp_defs (x = no output)  %s\n", anl_rsp_defs_str);

		output(0, "[Main]     anl_rsp_min_rrm               %.3f\n", config.anl_rsp_min_rrm);
		output(0, "[Main]     anl_rsp_max_rrm               %.3f\n", config.anl_rsp_max_rrm);
		output(0, "[Main]     anl_rsp_min_smr               %.3f\n", config.anl_rsp_min_smr);
		output(0, "[Main]     anl_rsp_max_smr               %.3f\n", config.anl_rsp_max_smr);
		output(0, "[Main]     anl_rsp_demand_infall_rs      %d\n", config.anl_rsp_demand_infall_rs);
		output(0, "[Main]     anl_rsp_sigma_tdyn            %.3f\n", config.anl_rsp_sigma_tdyn);
		output(0, "[Main]     anl_rsp_min_weight            %.3f\n", config.anl_rsp_min_weight);
		output(0, "[Main]     anl_rsp_n_bootstrap           %d\n", config.anl_rsp_n_bootstrap);
		output(0, "[Main]     anl_rsp_do_correction         %d\n", config.anl_rsp_do_correction);
		break;
	}
}

/*
 * Given a halo's tracer results, compute its Rsp/Msp history. This function compiles a set of
 * splashback events to use and passes it to the actual analysis function.
 */
void runAnalysisRsp(int snap_idx, Halo *halo, void *ls_vp)
{
	/*
	 * We only run on halos at the end of their life
	 */
	if (!(hasEnded(halo) && shouldBeSaved(halo)))
		return;

#if CAREFUL
	if (halo->al[RSP].n > 0)
		error(__FFL__, "Found already existing rsp analysis in halo.\n");
#endif

	int i, n_sbk_orig, n_sbk, counter;
	int n_valid;
	int_least8_t *mask;
	ResultSplashback *sbk, *sbk_valid;

	int n_ifl;
	ResultInfall *rs_ifl, temp_rs, *ifl;

	rs_ifl = halo->tt[PTL].rs[IFL].rs_ifl;
	n_ifl = halo->tt[PTL].rs[IFL].n;

	sbk = halo->tt[PTL].rs[SBK].rs_sbk;
	n_sbk_orig = halo->tt[PTL].rs[SBK].n;
	n_sbk = n_sbk_orig;

	mask = (int_least8_t*) memAlloc(__FFL__, MID_ANLRSP, sizeof(int_least8_t) * n_sbk_orig);
	n_valid = 0;

	for (i = 0; i < n_sbk; i++)
	{
		/*
		 * Check whether we should use this particular splashback result. First, we check that its
		 * rsp and (if applicable) msp are valid, as invalid msp (which can exist!) will mess up the
		 * calculation later. Second, we check the properties of the sbk event against minima and
		 * maxima. If it is allowed, we also look for a corresponding infall event.
		 */
		mask[i] = (sbk[i].rsp > 0.0);
		mask[i] = (mask[i] && (sbk[i].msp > 0.0));
		mask[i] = (mask[i] && (sbk[i].rrm >= config.anl_rsp_min_rrm));
		mask[i] = (mask[i] && (sbk[i].rrm <= config.anl_rsp_max_rrm));

		if (!mask[i])
			continue;

		temp_rs.tracer_id = sbk[i].tracer_id;
		ifl = (ResultInfall*) bsearch(&temp_rs, rs_ifl, n_ifl, sizeof(ResultInfall),
				&compareResults);
		if (ifl != NULL)
		{
			mask[i] =
					((ifl->smr >= config.anl_rsp_min_smr) && (ifl->smr <= config.anl_rsp_max_smr));
		} else
		{
			mask[i] = (config.anl_rsp_demand_infall_rs == 0);
		}

		if (mask[i])
			n_valid++;
	}

	/*
	 * If there are enough splashback events, create an array with valid splashback events and
	 * compute the mean/median/etc quantities using the valid events.
	 */
	if (n_valid >= 2)
	{
		sbk_valid = (ResultSplashback*) memAlloc(__FFL__, MID_ANLRSP,
				sizeof(ResultSplashback) * n_valid);
		counter = 0;
		for (i = 0; i < n_sbk; i++)
		{
			if (mask[i])
			{
				memcpy(&(sbk_valid[counter]), &(sbk[i]), sizeof(ResultSplashback));
				counter++;
			}
		}
#if CAREFUL
		if (counter != n_valid)
			error(__FFL__, "Counter = %d, n_valid = %d.\n", counter, n_valid);
#endif

		computeRsp(snap_idx, halo, sbk_valid, n_valid);
		memFree(__FFL__, MID_ANLRSP, sbk_valid, sizeof(ResultSplashback) * n_valid);
	}

	memFree(__FFL__, MID_ANLRSP, mask, sizeof(int_least8_t) * n_sbk_orig);

	return;
}

/*
 * This function performs the actual calculation of Rsp given a set of splashback events to use.
 */
void computeRsp(int snap_idx, Halo *halo, ResultSplashback *sbk, int n_sbk)
{
	AnalysisRsp *al_rsp;
	SplashbackEvent *spe;
	int i_snap, i_anl, i_def, j, q, j_min, j_max, idx, n_bin, do_correction;
	float t, t_range, t_min, t_max, t_max_target, sigma, ws, **qsp_mean, **qsp_mean_err,
			*corr_factor;
	HaloDefinition *def;

	/*
	 * Create the rsp analysis object
	 */
	al_rsp = addAnalysisRsp(__FFL__, &(halo->al[RSP]));
	al_rsp->halo_id = haloOriginalID(halo);

	/*
	 * Create array for mean values; they can have a different dimensionality from the Rsp output
	 * because we need all snaps for them. Note that we need to set the mean/mean_err to -1 because
	 * otherwise points might erroneously be included in the correction factor.
	 */
	qsp_mean = (float**) memAlloc(__FFL__, MID_ANLRSP, sizeof(float*) * Q_N);
	qsp_mean_err = (float**) memAlloc(__FFL__, MID_ANLRSP, sizeof(float*) * Q_N);
	for (q = 0; q < Q_N; q++)
	{
		qsp_mean[q] = (float*) memAlloc(__FFL__, MID_ANLRSP, sizeof(float) * MAX_SNAPS);
		qsp_mean_err[q] = (float*) memAlloc(__FFL__, MID_ANLRSP, sizeof(float) * MAX_SNAPS);

		for (j = 0; j < MAX_SNAPS; j++)
		{
			qsp_mean[q][j] = INVALID_R;
			qsp_mean_err[q][j] = INVALID_R;
		}
	}
	corr_factor = (float*) memAlloc(__FFL__, MID_ANLRSP, sizeof(float) * Q_N);

	/*
	 * First, sort the splashback results by time; this will allow us to find a range of events
	 * for each bin.
	 */
	qsort(sbk, n_sbk, sizeof(ResultSplashback), &compareResultSplashbackTime);

	/*
	 * Set all snaps in the analysis to the error code indicating that the halo did not exist, or
	 * that not enough snaps had been completed yet.
	 */
	for (j = 0; j < ANALYSIS_RSP_MAX_SNAPS; j++)
		al_rsp->status[j] = ANL_RSP_STATUS_HALO_NOT_VALID;

	/*
	 * Go through snapshots. If something goes wrong, we assign the appropriate error code.
	 */
	for (i_snap = halo->first_snap + MIN_SNAPS_RSP; i_snap <= snap_idx; i_snap++)
	{
		/*
		 * Check 1: Did the user request output at this snapshot?
		 */
		i_anl = config.anl_rsp_snap_idx[i_snap];

		/*
		 * Set the time window in which we consider events. Note that we cut out events that
		 * occurred in the final time bin because the sbk algorithm only places a small number of
		 * sbk events in the last (of four) bins it considers. Thus, these events are not a fair
		 * representation of the distribution. Thus, we need to correct if the final time goes
		 * past snap_idx-1.
		 */
		t = config.snap_t[i_snap];
		sigma = config.snap_t_dyn[i_snap] * config.anl_rsp_sigma_tdyn;
		t_range = RSP_N_SIGMA_SEARCH * sigma;
		t_min = t - t_range;
		t_max_target = t + t_range;
		do_correction = (t_max_target > config.snap_t[snap_idx - 1]);
		t_max = fminf(t_max_target, config.snap_t[snap_idx - 1]);

		/*
		 * Find all events in this bin. As they are sorted, we just need to find the first and last
		 * event.
		 */
		n_bin = 0;
		j = 0;
		while ((j < n_sbk - 1) && (sbk[j].tsp < t_min))
			j++;
		j_min = j;
		while ((j < n_sbk - 1) && (sbk[j].tsp <= t_max))
			j++;
		if (sbk[j].tsp <= t_max)
			j_max = j;
		else
			j_max = j - 1;
		n_bin = j_max - j_min + 1;

		/*
		 * If there are not enough events in the maximal time range, there is just nothing we can
		 * do, we abort.
		 */
		if (n_bin < 2)
		{
			if (i_anl >= 0)
				al_rsp->status[i_anl] = ANL_RSP_STATUS_INSUFFICIENT_EVENTS;
			continue;
		}

		/*
		 * For each event, we compute a weight and the sum of those weights.
		 */
		spe = (SplashbackEvent*) memAlloc(__FFL__, MID_ANLRSP, sizeof(SplashbackEvent) * n_bin);
		ws = 0.0;
		for (j = 0; j < n_bin; j++)
		{
			idx = j_min + j;

			spe[j].weight = weight_gaussian(sbk[idx].tsp, t, sigma);
			ws += spe[j].weight;

			spe[j].qsp[0] = sbk[idx].rsp;
			spe[j].qsp[1] = sbk[idx].msp;
#if CAREFUL
			if (spe[j].qsp[1] < 0.0)
				error(__FFL__,
						"Found invalid msp value, should have been filtered out; idx %d, orig idx %d, j_min %d, j_max %d, n_bin %d, t_min %.3d, t_max %.3d, t %.3d.\n",
						j, idx, j_min, j_max, n_bin, t_min, t_max, sbk[idx].tsp);
#endif
		}

		/*
		 * One final check: if the weight sum is too low, abort.
		 */
		if (ws < config.anl_rsp_min_weight)
		{
			memFree(__FFL__, MID_ANLRSP, spe, sizeof(SplashbackEvent) * n_bin);
			if (i_anl >= 0)
				al_rsp->status[i_anl] = ANL_RSP_STATUS_INSUFFICIENT_WEIGHT;
			continue;
		}

		/*
		 * Compute the statistics we are interested in. First, we always need the mean because
		 * that gets used in the correction factor; we put it in separate arrays.
		 */
		computeMean(i_snap, spe, n_bin, &qsp_mean, &qsp_mean_err);

		/*
		 * Compute the correction factor if we are close to the end of the halo's life. Most
		 * commonly, that is caused by the end of the simulation, but the halo can also merge
		 * away.
		 *
		 * The correction needs to be applied to the mean at each snapshot in an iterative fashion,
		 * where the correction at the next snap relies on the corrected mean from the previous
		 * snaps. Thus, we need to execute this function regardless of whether we are outputting
		 * splashback quantities at this snapshot.
		 */
		if (config.anl_rsp_do_correction && do_correction)
		{
			computeCorrectionFactor(i_snap, i_anl, al_rsp, t_max_target, t_max, sigma, t_range,
					qsp_mean, qsp_mean_err, &corr_factor);
			for (q = 0; q < Q_N; q++)
			{
				if (corr_factor[q] >= 0.0)
					qsp_mean[q][i_snap] *= corr_factor[q];
			}
		}

		/*
		 * If the user has not selected this snapshot for output, we can stop here. However, we do
		 * still need to clean up at the end, so we do not leave the loop.
		 */
		if (i_anl >= 0)
		{
			/*
			 * Compute percentiles, then go through definitions.
			 */
			computePercentiles(i_anl, al_rsp, spe, n_bin);

			for (i_def = 0; i_def < config.anl_rsp_n_defs; i_def++)
			{
				def = &(config.anl_rsp_defs[i_def]);

				// Set correct q
				if (def->quantity == HDEF_Q_RADIUS)
					q = 0;
				else if (def->quantity == HDEF_Q_MASS)
					q = 1;
				else
					error(__FFL__,
							"Found invalid quantity in Rsp definition; this should never happen.\n");

				/*
				 * If the user has requested means to be computed, we copy the values into the proper place
				 * now. Note that the correction factor has already been applied if necessary.
				 *
				 * For percentiles, we apply the correction factor, but not to errors. A negative
				 * correction factor indicates that it could not be computed, in which case we do
				 * not apply it.
				 */
				if (def->subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN)
				{
					if (def->is_error == HDEF_ERR_NO)
					{
						al_rsp->defs[i_def][i_anl] = qsp_mean[q][i_snap];
					} else if (def->is_error == HDEF_ERR_1SIGMA)
					{
						al_rsp->defs[i_def][i_anl] = qsp_mean_err[q][i_snap];
					} else
					{
						error(__FFL__,
								"Found invalid error identifier in Rsp definition; this should never happen.\n");
					}
				} else if (def->subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE)
				{
					if ((config.anl_rsp_do_correction && do_correction)
							&& (def->is_error == HDEF_ERR_NO))
					{
						if (corr_factor[q] >= 0.0)
							al_rsp->defs[i_def][i_anl] *= corr_factor[q];
					}
				}

				/*
				 * At this point, all definitions should have been set to some number one way or
				 * another. Check that the output isn't negative, zero, nan, or inf.
				 */
#if CAREFUL
				if (isnan(al_rsp->defs[i_def][i_anl]))
					error(__FFL__, "Rsp analysis found NaN in definition %s, halo ID %ld.\n",
							config.anl_rsp_defs_str[i_def], halo->cd.id);

				if (isinf(al_rsp->defs[i_def][i_anl]))
					error(__FFL__, "Rsp analysis found inf in definition %s, halo ID %ld.\n",
							config.anl_rsp_defs_str[i_def], halo->cd.id);

				if (al_rsp->defs[i_def][i_anl] <= 0.0)
					error(__FFL__, "Rsp analysis found value %.2e in definition %s, halo ID %ld.\n",
							al_rsp->defs[i_def][i_anl], config.anl_rsp_defs_str[i_def],
							halo->cd.id);
#endif
			}

			/*
			 * Set status to success.
			 */
			al_rsp->status[i_anl] = ANL_RSP_STATUS_SUCCESS;

			debugHalo(halo,
					"anl_rsp: snap %3d, idx in anl %3d, t_max %.4f, t_max_target %.4f, do_correction %d",
					i_snap, i_anl, t_max, t_max_target, do_correction);
		}

		memFree(__FFL__, MID_ANLRSP, spe, sizeof(SplashbackEvent) * n_bin);
	}

	for (q = 0; q < Q_N; q++)
	{
		memFree(__FFL__, MID_ANLRSP, qsp_mean[q], sizeof(float) * MAX_SNAPS);
		memFree(__FFL__, MID_ANLRSP, qsp_mean_err[q], sizeof(float) * MAX_SNAPS);
	}
	memFree(__FFL__, MID_ANLRSP, qsp_mean, sizeof(float*) * Q_N);
	memFree(__FFL__, MID_ANLRSP, qsp_mean_err, sizeof(float*) * Q_N);
	memFree(__FFL__, MID_ANLRSP, corr_factor, sizeof(float) * Q_N);
}

/*
 * Compute the weighted mean and variance of q (where q is r/m). Note that this could also
 * be done using the bootstrap, but the bootstrap mean is identical to the sample mean for
 * an infinite number of samples.
 *
 * Instead, we use the standard formula for the uncertainty on a weighted mean, computed
 * as variance / root(weightsum). The variance itself has an extra term in the denominator
 * to make it unbiased.
 */
void computeMean(int idx_t, SplashbackEvent *spe, int n_bin, float ***qsp_mean,
		float ***qsp_mean_err)
{
	int j, q;
	float q_mean, q_err, qdiff, ws, ws2;

	for (q = 0; q < Q_N; q++)
	{
		ws = 0.0;
		ws2 = 0.0;
		q_mean = 0.0;
		for (j = 0; j < n_bin; j++)
		{
			ws += spe[j].weight;
			ws2 += spe[j].weight * spe[j].weight;
			q_mean += spe[j].qsp[q] * spe[j].weight;
		}
		q_mean /= ws;

		q_err = 0.0;
		for (j = 0; j < n_bin; j++)
		{
			qdiff = spe[j].qsp[q] - q_mean;
			q_err += spe[j].weight * qdiff * qdiff;
		}
		q_err = sqrt(q_err / (ws * ws - ws2));

		(*qsp_mean)[q][idx_t] = q_mean;
		(*qsp_mean_err)[q][idx_t] = q_err;
	}
}

/*
 * Run over bootstrap samples. For each sample, we have to repeat part of the process above,
 * i.e. assign the right weights and values to each selected rsp/msp element.
 */
void computePercentiles(int idx_t, AnalysisRsp *al_rsp, SplashbackEvent *spe_orig, int n_ev)
{
	int i, b, j, q, j_rand, idx, *randoms, n_bs;
	float *qsp, *weights, *cuml, *perc_bs, bs_mean, bs_err, bs_diff;
	HaloDefinition *def;
	SplashbackEvent *spe_sorted;

	/*
	 * Allocate memory
	 */
	spe_sorted = (SplashbackEvent*) memAlloc(__FFL__, MID_ANLRSP,
			sizeof(SplashbackEvent) * n_ev * Q_N);
	qsp = (float*) memAlloc(__FFL__, MID_ANLRSP, sizeof(float) * n_ev);
	weights = (float*) memAlloc(__FFL__, MID_ANLRSP, sizeof(float) * n_ev);
	cuml = (float*) memAlloc(__FFL__, MID_ANLRSP, sizeof(float) * n_ev);

	/*
	 * If we are not bootstrapping, we execute the percentile code once; if we are bootstrapping,
	 * we pre-compute the random numbers that we will need. We sort the randoms for each BS sample
	 * so that the resulting values will be ascending as well.
	 *
	 * Note that we cannot create the randoms for all time (snapshot) bins at once because we are
	 * dealing with different numbers of events.
	 */
	randoms = NULL;
	perc_bs = NULL;
	n_bs = config.anl_rsp_n_bootstrap;
	if (config.anl_rsp_do_bootstrap)
	{
		perc_bs = (float*) memAlloc(__FFL__, MID_ANLRSP, sizeof(float) * n_bs);
		randoms = (int*) memAlloc(__FFL__, MID_ANLRSP, sizeof(int) * n_bs * n_ev);

		randomIntegers(0, n_ev - 1, n_bs * n_ev, randoms);
		for (b = 0; b < n_bs; b++)
			qsort(&(randoms[b * n_ev]), n_ev, sizeof(int), &compareIntegersAscending);
	}

	/*
	 * Create sorted arrays for each q. That way, we can sort the randoms in order to get sorted
	 * randomized arrays.
	 */
	for (q = 0; q < Q_N; q++)
	{
		memcpy(&(spe_sorted[q * n_ev]), spe_orig, sizeof(SplashbackEvent) * n_ev);
		if (q == 0)
			qsort(&(spe_sorted[q * n_ev]), n_ev, sizeof(SplashbackEvent), &compareEventsByRadius);
		else
			qsort(&(spe_sorted[q * n_ev]), n_ev, sizeof(SplashbackEvent), &compareEventsByMass);
	}

	/*
	 * Run over definitions. There are three cases:
	 * - the definition is mean or mean error, not handled in this function
	 * - the definition is a percentile, compute from distribution
	 * - the definition is the error on a percentile, compute from bootstrapping
	 */
	for (i = 0; i < config.anl_rsp_n_defs; i++)
	{
		def = &(config.anl_rsp_defs[i]);

		if (def->subtype != HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE)
			continue;

		// Set correct q
		if (def->quantity == HDEF_Q_RADIUS)
			q = 0;
		else if (def->quantity == HDEF_Q_MASS)
			q = 1;
		else
			error(__FFL__, "Found invalid quantity in Rsp definition; this should never happen.\n");

		// Treat different cases
		if (def->is_error == HDEF_ERR_NO)
		{
			/*
			 * Non-error quantity, evaluate percentile once from actual distribution.
			 */
			for (j = 0; j < n_ev; j++)
			{
				idx = q * n_ev + j;
				qsp[j] = spe_sorted[idx].qsp[q];
				weights[j] = spe_sorted[idx].weight;
			}
			al_rsp->defs[i][idx_t] = percentile(n_ev, qsp, weights, cuml, def->percentile);

		} else if (def->is_error == HDEF_ERR_1SIGMA)
		{
			/*
			 * 1-sigma error, evaluate by bootstrapping. Note that we do not actually output the
			 * mean of the bootstrap samples.
			 */
			bs_mean = 0.0;
			for (b = 0; b < n_bs; b++)
			{
				for (j = 0; j < n_ev; j++)
				{
					j_rand = randoms[b * n_ev + j];
					idx = q * n_ev + j_rand;
					qsp[j] = spe_sorted[idx].qsp[q];
					weights[j] = spe_sorted[idx].weight;
				}
				perc_bs[b] = percentile(n_ev, qsp, weights, cuml, def->percentile);
				bs_mean += perc_bs[b];
			}
			bs_mean /= n_bs;
			bs_err = 0.0;
			for (b = 0; b < n_bs; b++)
			{
				bs_diff = perc_bs[b] - bs_mean;
				bs_err += bs_diff * bs_diff;
			}
			bs_err = sqrt(bs_err / ((float) n_bs - 1.0));
			al_rsp->defs[i][idx_t] = bs_err;

		} else
		{
			error(__FFL__,
					"Found invalid error identifier in Rsp definition; this should never happen.\n");
		}
	}

	memFree(__FFL__, MID_ANLRSP, spe_sorted, sizeof(SplashbackEvent) * n_ev * Q_N);
	memFree(__FFL__, MID_ANLRSP, qsp, sizeof(float) * n_ev);
	memFree(__FFL__, MID_ANLRSP, weights, sizeof(float) * n_ev);
	memFree(__FFL__, MID_ANLRSP, cuml, sizeof(float) * n_ev);
	if (config.anl_rsp_do_bootstrap)
	{
		memFree(__FFL__, MID_ANLRSP, perc_bs, sizeof(float) * n_bs);
		memFree(__FFL__, MID_ANLRSP, randoms, sizeof(int) * n_bs * n_ev);
	}
}

/*
 * Compute weighted percentile of a distribution. The cuml array is only passed to avoid
 * re-allocating it each time the function is called.
 */
float percentile(int n, float *qsp, float *weights, float *cuml, float perc)
{
	int j, idxc, idxl, idxr, found;
	float factor, ret;

	ret = 0.0;

	/*
	 * For this calculation, we have to sort the array
	 * and compute the cumulative weight as a function of ascending qsp, normalized to 100.
	 * Each point is assumed to be at the mid-point at its weight, thus adding half its
	 * weight w at coordinate q. Thus, the cumulative array does not run from [0 .. 100]
	 * but rather [w0/(2 w_tot) .. 100 - wN-1/(2 w_tot)]. For all percentiles outside this
	 * range, we use the first / last values.
	 */
	cuml[0] = weights[0];
	for (j = 1; j < n; j++)
		cuml[j] = cuml[j - 1] + weights[j];
	factor = 100.0 / cuml[n - 1];
	for (j = 0; j < n; j++)
		cuml[j] = (cuml[j] - weights[j] * 0.5) * factor;

	/*
	 * Go through bins, find percentiles. Note that two percentiles can occur in the same
	 * q bin. There are two different ways of doing this: just going through all bins, or
	 * using a binary search. Surprisingly, the latter can be a bit slower...
	 */
#if USE_BSEARCH_PERC
	idxl = 0;
	idxr = n - 2;

	idxc = (idxr + idxl) / 2;
	found = 0;
	while (!found)
	{
#if CAREFUL
		if (idxc < 0)
			error(__FFL__, "idxc = %d, n_bin %d\n", idxc, n);
		if (idxc >= n - 1)
			error(__FFL__, "idxc = %d, n_bin %d\n", idxc, n);
#endif
		if (cuml[idxc + 1] < perc)
		{
			// shift right-wards
			if (idxr == idxl)
				break;
			idxl = idxc + 1;
			idxc = (idxr + idxl) / 2;
		} else
		{
			if (cuml[idxc] < perc)
			{
				// Found
				found = 1;
				break;
			} else
			{
				// Shift left-wards
				if (idxr == idxl)
					break;
				idxr = idxc;
				idxc = (idxr + idxl) / 2;
			}
		}
	}

#if CAREFUL
	if (idxc < 0)
		error(__FFL__, "idxc = %d, n_bin %d\n", idxc, n);
	if (idxc >= n - 1)
		error(__FFL__, "idxc = %d, n_bin %d\n", idxc, n);
#endif

	if (found)
	{
		ret = qsp[idxc]
				+ (perc - cuml[idxc]) / (cuml[idxc + 1] - cuml[idxc]) * (qsp[idxc + 1] - qsp[idxc]);
	} else if (perc >= cuml[n - 1])
	{
		ret = qsp[n - 1];
	} else if (perc <= cuml[0])
	{
		ret = qsp[0];
	}
#else
	/*
	 * Brute-force finding, starting from the highest bin
	 */
	j = n - 1;
	p = config.anl_rsp_n_percentiles - 1;
	perc = (float) al_rsp_percentiles[p];
	while (j >= 0)
	{
		if ((cuml[j] < perc) || (j == 0))
		{
			idx = p * NQSP * N_BOOTSTRAP + q * N_BOOTSTRAP + b;
			if (j == 0)
			{
				q_perc_bs[idx] = qsp[0];
			} else if (j == n - 1)
			{
				q_perc_bs[idx] = qsp[j];
			} else if (cuml[j] < perc)
			{
				q_perc_bs[idx] = qsp[j]
				+ (perc - cuml[j]) * (qsp[j + 1] - qsp[j])
				/ (cuml[j + 1] - cuml[j]);
			} else
			{
				error(__FFL__, "Internal error.\n");
			}
			bs_mean[p][q] += q_perc_bs[idx];

			p--;
			if (p < 0)
			break;
			perc = (float) al_rsp_percentiles[p];
		} else
		{
			j--;
		}
	}
#endif

	return ret;
}

/*
 * Compute the correction once for Rsp and once for Msp, and apply to all estimates
 */
void computeCorrectionFactor(int i_snap, int i_anl, AnalysisRsp *al_rsp, float t_max_target,
		float t_max, float sigma, float t_range, float **qsp_mean, float **qsp_mean_err,
		float **corr_factor)
{
	int i, q, n_fit, ret;
	double q_fit[CORR_N_FIT], w_fit[CORR_N_FIT], t_fit[CORR_N_FIT], f0_d, f1_d, cov00, cov01, cov11,
			chisq;
	float rt2, rt2sig, rt2pi, rtpi, wtot, wstar, t, y0, y1, erf0, erf1, qstar, q_new, ws_fit, f0,
			f1, wmean_use, wstar_use, tot_use;

	/*
	 * Pre-computed quantities
	 */
	t = config.snap_t[i_snap];
	rt2 = sqrt(2.0);
	rt2sig = rt2 * sigma;
	rt2pi = sqrt(2.0 * MATH_pi);
	rtpi = sqrt(MATH_pi);

	wtot = ((float) gsl_sf_erf(t_range / rt2sig)) * rt2pi * sigma;
	wstar = ((float) gsl_sf_erf(-(t_max - t) / rt2sig)) * rt2pi * sigma / 2.0 + wtot / 2.0;

	/*
	 * Run over radius and mass
	 */
	for (q = 0; q < Q_N; q++)
	{
		/*
		 * Negative values indicate something went wrong.
		 */
		(*corr_factor)[q] = -1.0;

		/*
		 * Linear fit to last few data points; first, see if we have enough data points going back
		 * in time.
		 */
		n_fit = 0;
		ws_fit = 0.0;
		for (i = i_snap - 1; (i > 0) && (i >= i_snap - CORR_N_FIT_RANGE_MAX); i--)
		{
			if (qsp_mean[q][i] > 0.0)
			{
				t_fit[n_fit] = config.snap_t[i];
				q_fit[n_fit] = qsp_mean[q][i];
				w_fit[n_fit] = 1.0 / qsp_mean_err[q][i];
				ws_fit += w_fit[n_fit];
				n_fit++;
				if (n_fit == CORR_N_FIT)
					break;
			}
		}

		if (n_fit < CORR_N_FIT)
			continue;

#ifdef CORR_FIT_CHI_MAX
		ret = gsl_fit_wlinear(t_fit, 1, w_fit, 1, q_fit, 1, n_fit, &f0_d, &f1_d, &cov00, &cov01,
				&cov11, &chisq);
		if (ret != GSL_SUCCESS)
			continue;
		chisq = chisq * ws_fit / ((float) CORR_N_FIT - 2.0);
		if (chisq > CORR_FIT_CHI_MAX)
			continue;
#else
		ret = gsl_fit_linear(t_fit, 1, q_fit, 1, n_fit, &f0_d, &f1_d, &cov00, &cov01, &cov11,
				&chisq);
		if (ret != GSL_SUCCESS)
			continue;
#endif

		f0 = (float) f0_d;
		f1 = (float) f1_d;

		/*
		 * Compute correction term
		 */
		y0 = (t_max - t) / rt2sig;
		y1 = (t_range) / rt2sig;
		erf0 = 0.5 * rtpi * ((float) gsl_sf_erf(y0));
		erf1 = 0.5 * rtpi * ((float) gsl_sf_erf(y1));
		qstar = rt2sig * (f0 + f1 * t) * (erf1 - erf0)
				+ rt2sig * rt2sig * f1 * 0.5 * (exp(-y0 * y0) - exp(-y1 * y1));
		qstar /= wstar;

		wmean_use = (wtot - wstar);
		wstar_use = fminf(wstar, CORR_MAX_WEIGHT * wtot);
		tot_use = wmean_use + wstar_use;
		q_new = (qsp_mean[q][i_snap] * wmean_use + qstar * wstar_use) / tot_use;
		(*corr_factor)[q] = q_new / qsp_mean[q][i_snap];
	}
}

/*
 * This function returns a Gaussian weight, normalized such that a point at the exact bin location
 * receives weight 1. This is appropriate here because we are not integrating weights over the
 * width of a bin, but rather evaluating them at an infinitesimal point.
 */
float weight_gaussian(float x, float x0, float sigma)
{
	float d;

	d = x - x0;

	return exp(-(d * d) * 0.5 / (sigma * sigma));
}

int compareEventsByRadius(const void *a, const void *b)
{
	if (((SplashbackEvent*) a)->qsp[0] > ((SplashbackEvent*) b)->qsp[0])
		return 1;
	else if (((SplashbackEvent*) a)->qsp[0] < ((SplashbackEvent*) b)->qsp[0])
		return -1;
	else
		return 0;
}

int compareEventsByMass(const void *a, const void *b)
{
	if (((SplashbackEvent*) a)->qsp[1] > ((SplashbackEvent*) b)->qsp[1])
		return 1;
	else if (((SplashbackEvent*) a)->qsp[1] < ((SplashbackEvent*) b)->qsp[1])
		return -1;
	else
		return 0;
}

/*
 * For 1D fields, there is no need to set any dimensions. For higher-dimensional fields, the higher
 * dimensions must be fixed.
 */
int outputFieldsAnalysisRsp(OutputField *outfields, int max_n_fields)
{
	int i, counter;

	counter = 0;

	outfields[counter].dtype = DTYPE_INT64;
	outfields[counter].offset = offsetof(AnalysisRsp, halo_id);
	sprintf(outfields[counter].name, "halo_id");
	counter++;

	outfields[counter].n_dims = 2;
	outfields[counter].dim_ext[1] = config.anl_rsp_n_redshifts;
	outfields[counter].dtype = DTYPE_INT8;
	outfields[counter].offset = offsetof(AnalysisRsp, status);
	sprintf(outfields[counter].name, "status");
	counter++;

	for (i = 0; i < config.anl_rsp_n_defs; i++)
	{
		if (config.anl_rsp_defs[i].do_output == 0)
			continue;
		outfields[counter].n_dims = 2;
		outfields[counter].dim_ext[1] = config.anl_rsp_n_redshifts;
		outfields[counter].dtype = DTYPE_FLOAT;
		outfields[counter].offset = offsetof(AnalysisRsp, defs[i]);
		sprintf(outfields[counter].name, "%s", config.anl_rsp_defs_str[i]);
		counter++;
		if (counter >= max_n_fields)
			error(__FFL__,
					"Reached maximum number (%d) of analysis output fields in Rsp analysis, please increase.\n",
					max_n_fields);
	}

	return counter;
}

void outputConfigAnalysisRsp(hid_t grp)
{
	int i;
	char anl_rsp_defs_str[500];

	if (config.anl_rsp_redshifts[0] < 0.0)
	{
		hdf5WriteAttributeFloat1D(grp, "snap_redshifts_requested", 1, config.anl_rsp_redshifts);
	} else
	{
		hdf5WriteAttributeFloat1D(grp, "snap_redshifts_requested", config.anl_rsp_n_redshifts,
				config.anl_rsp_redshifts);
	}
	hdf5WriteAttributeFloat1D(grp, "snap_redshifts_actual", config.anl_rsp_n_redshifts,
			config.anl_rsp_redshifts_actual);
	hdf5WriteAttributeInt1D(grp, "snap_idxs", config.anl_rsp_n_redshifts, config.anl_rsp_snaps);
	hdf5WriteAttributeInt1D(grp, "snap_idxs_in_anl", config.n_snaps, config.anl_rsp_snap_idx);

	sprintf(anl_rsp_defs_str, "%s", config.anl_rsp_defs_str[0]);
	for (i = 1; i < config.anl_rsp_n_defs; i++)
	{
		if (config.anl_rsp_defs[i].do_output == 0)
			continue;
		sprintf(anl_rsp_defs_str, "%s,%s", anl_rsp_defs_str, config.anl_rsp_defs_str[i]);
	}
	hdf5WriteAttributeString(grp, "defs", anl_rsp_defs_str);

	hdf5WriteAttributeFloat(grp, "min_rrm", config.anl_rsp_min_rrm);
	hdf5WriteAttributeFloat(grp, "max_rrm", config.anl_rsp_max_rrm);
	hdf5WriteAttributeFloat(grp, "min_smr", config.anl_rsp_min_smr);
	hdf5WriteAttributeFloat(grp, "max_smr", config.anl_rsp_max_smr);
	hdf5WriteAttributeInt(grp, "demand_infall_rs", config.anl_rsp_demand_infall_rs);
	hdf5WriteAttributeFloat(grp, "sigma_tdyn", config.anl_rsp_sigma_tdyn);
	hdf5WriteAttributeFloat(grp, "min_weight", config.anl_rsp_min_weight);
}

#endif
