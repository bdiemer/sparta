/*************************************************************************************************
 *
 * This unit implements an analysis for density profiles.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _ANALYSIS_PROFILES_H_
#define _ANALYSIS_PROFILES_H_

#include "analyses.h"
#include "../global_types.h"
#include "../io/io_hdf5.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * The profiles analysis currently uses only standard error codes.
 *
 * Note that these constants must be outside the #if protected block because they are used in
 * analysis software.
 */
#define ANL_PRF_STATUS_UNDEFINED ANALYSIS_STATUS_UNDEFINED
#define ANL_PRF_STATUS_SUCCESS ANALYSIS_STATUS_SUCCESS
#define ANL_PRF_STATUS_HALO_NOT_VALID ANALYSIS_STATUS_HALO_NOT_VALID
#define ANL_PRF_STATUS_SEARCH_RADIUS ANALYSIS_STATUS_OTHER
#define ANL_PRF_STATUS_CATALOG_RADIUS (ANALYSIS_STATUS_OTHER + 1)

#if DO_ANALYSIS_PROFILES

#define DEFAULT_ANL_PRF_RMIN 0.01
#define DEFAULT_ANL_PRF_RMAX 3.0
#define DEFAULT_ANL_PRF_DO_SUBS 1
#define DEFAULT_ANL_PRF_DO_GHOSTS 1

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initAnalysisProfiles(AnalysisProfiles *al_prf);
void initConfigAnalysisProfiles(int step);
float searchRadiusAnalsisProfiles(int snap_idx, Halo *halo);
void runAnalysisProfiles(int snap_idx, Halo *halo, void *ls_vp, void *tree_vp, void *tr_res_vp);
int outputFieldsAnalysisProfiles(OutputField *outfields, int max_n_fields);
void outputConfigAnalysisProfiles(hid_t grp);

#endif

#endif
