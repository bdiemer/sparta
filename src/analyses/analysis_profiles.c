/*************************************************************************************************
 *
 * This unit implements an analysis for density profiles.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <time.h>

#include "analysis_profiles.h"
#include "analyses.h"
#include "../constants.h"
#include "../config.h"
#include "../memory.h"
#include "../tree.h"
#include "../utils.h"
#include "../halos/halo.h"
#include "../halos/halo_so.h"
#include "../halos/halo_definitions.h"
#include "../tracers/tracers.h"
#include "../results/result_orbitcount.h"

#if DO_ANALYSIS_PROFILES

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

enum
{
	SELECTION_ALL, SELECTION_1HALO, SELECTION_CORRECT
};

/*
 * If a particle has zero pericenters but is marked as a lower limit, should we count it into the
 * 1-halo term? Typically, the answer should be yes -- even though we could not resolve the first
 * pericenter, the is_lower_limit flag indicates that the particle very likely did undergo at least
 * one orbit. This flag allows changing the algorithm for testing.
 */
#define LOWER_LIMIT_IN_1HALO 1

/*
 * Debug system for profiles. A halo ID and either bin ID or tracer ID must be specified to get
 * output. Note that this is the current halo ID, not original ID.
 */
#define PRF_DEBUG 0
#define PRF_DEBUG_TCR 0
#define PRF_DEBUG_HALO 0
#define PRF_DEBUG_BIN 0

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct
{
#if PRF_DEBUG
	ID tcr_id;
#endif
	float r;
	short int n_pericenter;
	int_least8_t n_is_lower_limit;
	int_least8_t use_in_profile;
} OrbitCountPtlInfo;

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int doProfilesSnap(int snap_idx, Halo *halo);
void correctProfiles(int snap_idx, Halo *halo, AnalysisProfiles *al_prf);
void computeProfiles(int snap_idx, Halo *halo, AnalysisProfiles *al_prf, TreeResults *tr_res);
void profileFromParticles(OrbitCountPtlInfo *ptl_info, int n_particles, float *prof, int selection,
		float R200m, float M200m, int debug_bin, int is_host, int is_phantom, HaloID halo_id);
int compareOrbitCountPtlInfo(const void *a, const void *b);
int includeInCorrection(int snap_idx, ResultOrbitCount *res_oct);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initAnalysisProfiles(AnalysisProfiles *al_prf)
{
	int i, j;

	al_prf->halo_id = INVALID_ID;

	for (i = 0; i < ANALYSIS_PROFILES_MAX_SNAPS; i++)
	{
		al_prf->status[i] = ANL_PRF_STATUS_UNDEFINED;

		for (j = 0; j < ANALYSIS_PROFILES_N_BINS; j++)
		{
#if DO_ANALYSIS_PROFILES_ALL
			al_prf->M_all[i][j] = INVALID_M;
#endif
#if DO_ANALYSIS_PROFILES_1HALO
			al_prf->M_1halo[i][j] = INVALID_M;
#endif
		}
	}
}

void initConfigAnalysisProfiles(int step)
{
	int i, idx, i_ap;
	double anl_prf_log_min, anl_prf_log_bin_width;

	switch (step)
	{
	case CONFIG_INIT_STEP_DEFAULT:
		config.anl_prf_rmin = DEFAULT_ANL_PRF_RMIN;
		config.anl_prf_rmax = DEFAULT_ANL_PRF_RMAX;
		config.anl_prf_do_subs = DEFAULT_ANL_PRF_DO_SUBS;
		config.anl_prf_do_ghosts = DEFAULT_ANL_PRF_DO_GHOSTS;
		for (i = 0; i < ANALYSIS_PROFILES_MAX_SNAPS; i++)
		{
			config.anl_prf_redshifts[i] = -1.0;
		}
		for (i = 0; i < ANALYSIS_PROFILES_MAX_SNAPS; i++)
		{
			config.anl_prf_redshifts_actual[i] = -1.0;
			config.anl_prf_snaps[i] = -1;
		}
		for (i = 0; i < MAX_SNAPS; i++)
			config.anl_prf_snap_idx[i] = -1;
		break;

	case CONFIG_INIT_STEP_CHECK:
		assert(config.anl_prf_rmin > 0.0);
		assert(config.anl_prf_rmax > 0.0);
		assert(config.anl_prf_rmax > config.anl_prf_rmin);
		assertBool(config.anl_prf_do_subs);
		assertBool(config.anl_prf_do_ghosts);
		if (config.anl_prf_n_redshifts <= 0)
			error(__FFL__,
					"Profile analysis needs at least one output redshift (set with the anl_prl_redshifts parameter).\n");
		break;

	case CONFIG_INIT_STEP_CONFIG:
		anl_prf_log_min = log10(config.anl_prf_rmin);
		anl_prf_log_bin_width = (log10(config.anl_prf_rmax) - anl_prf_log_min)
				/ (double) (ANALYSIS_PROFILES_N_BINS - 1);
		for (i_ap = 0; i_ap < ANALYSIS_PROFILES_N_BINS; i_ap++)
		{
			config.anl_prf_r_bins_log[i_ap] = anl_prf_log_min + i_ap * anl_prf_log_bin_width;
			config.anl_prf_r_bins_lin[i_ap] = pow(10.0, (float) config.anl_prf_r_bins_log[i_ap]);
		}

		qsort(config.anl_prf_redshifts, config.anl_prf_n_redshifts, sizeof(float),
				&compareFloatsDescending);
		if (config.anl_prf_redshifts[0] < -0.9999)
		{
			config.anl_prf_n_redshifts = config.n_snaps;
			if (config.anl_prf_n_redshifts > ANALYSIS_PROFILES_MAX_SNAPS)
				error(__FFL__,
						"Trying to save profiles for %d snapshots but ANALYSIS_PROFILES_MAX_SNAPS = %d.\n",
						config.anl_prf_n_redshifts, ANALYSIS_PROFILES_MAX_SNAPS);
			for (i_ap = 0; i_ap < config.n_snaps; i_ap++)
			{
				config.anl_prf_redshifts_actual[i_ap] = config.snap_z[i_ap];
				config.anl_prf_snaps[i_ap] = i_ap;
				config.anl_prf_snap_idx[i_ap] = i_ap;
			}
		} else
		{
			for (i_ap = 0; i_ap < config.anl_prf_n_redshifts; i_ap++)
			{
				idx = closestInArray(config.snap_z, config.n_snaps, config.anl_prf_redshifts[i_ap]);
				config.anl_prf_redshifts_actual[i_ap] = config.snap_z[idx];
				config.anl_prf_snaps[i_ap] = idx;
				config.anl_prf_snap_idx[idx] = i_ap;
				if (fabs(
						config.anl_prf_redshifts[i_ap]
								- config.anl_prf_redshifts_actual[i_ap]) > ANL_REDSHIFT_TOLERANCE)
					error(__FFL__,
							"Redshift requested for profile analysis z = %.4f could not be found, closest match z = %.4f.\n",
							config.anl_prf_redshifts[i_ap], config.anl_prf_redshifts_actual[i_ap]);
			}
		}
		break;

	case CONFIG_INIT_STEP_PRINT:
		output(0, "[Main] Analysis: Profiles\n");
		output(0, "[Main]     anl_prf_rmin                  %.3f\n", config.anl_prf_rmin);
		output(0, "[Main]     anl_prf_rmax                  %.3f\n", config.anl_prf_rmax);
		output(0, "[Main]     anl_prf_do_subs               %d\n", config.anl_prf_do_subs);
		output(0, "[Main]     anl_prf_do_ghosts             %d\n", config.anl_prf_do_ghosts);
		if (config.anl_prf_redshifts[0] <= -0.99999)
		{
			output(0, "[Main]     anl_prf_redshifts             -1 (all)\n");
		} else
		{
			char anl_prf_str[200];

			sprintf(anl_prf_str, "%.2f", config.anl_prf_redshifts[0]);
			for (i = 1; i < config.anl_prf_n_redshifts; i++)
				sprintf(anl_prf_str, "%s, %.2f", anl_prf_str, config.anl_prf_redshifts[i]);
			output(0, "[Main]     anl_prf_redshifts (user)      %s\n", anl_prf_str);

			sprintf(anl_prf_str, "%.2f", config.anl_prf_redshifts[0]);
			for (i = 1; i < config.anl_prf_n_redshifts; i++)
				sprintf(anl_prf_str, "%s, %.2f", anl_prf_str, config.anl_prf_redshifts_actual[i]);
			output(0, "[Main]     anl_prf_redshifts (actual)    %s\n", anl_prf_str);
		}
		break;
	}
}

/*
 * The search radius depends on whether we are outputting profiles at this snapshot and on which
 * halo types (hosts/subs/ghosts) we are including.
 */
float searchRadiusAnalsisProfiles(int snap_idx, Halo *halo)
{
	float r_search_fac;

	if (doProfilesSnap(snap_idx, halo))
		r_search_fac = config.anl_prf_rmax;
	else
		r_search_fac = 0.0;

	return r_search_fac;
}

/*
 * Check whether we are analyzing the profiles at this snap.
 *
 * We do not check whether the halo has reached the minimum mass any more, because
 */
int doProfilesSnap(int snap_idx, Halo *halo)
{
	int do_status, do_profiles;

	if (isGhostStatus(halo->history_status[snap_idx]))
		do_status = config.anl_prf_do_ghosts;
	else if (isSubStatus(halo->history_status[snap_idx]))
		do_status = config.anl_prf_do_subs;
	else
		do_status = 1;

	do_profiles = (do_status && (config.anl_prf_snap_idx[snap_idx] >= 0));

	return do_profiles;
}

/*
 * The main function of the profiles analysis.
 */
void runAnalysisProfiles(int snap_idx, Halo *halo, void *ls_vp, void *tree_vp, void *tr_res_vp)
{
	int do_profiles, do_correct;

	/*
	 * Check whether we are computing profiles for this halo for this snapshot. If we are neither
	 * computing nor correcting, abort.
	 */
	do_profiles = doProfilesSnap(snap_idx, halo);
	if ((snap_idx == 0) || (halo->first_snap == snap_idx))
		do_correct = 0;
	else
		do_correct = doProfilesSnap(snap_idx - 1, halo);

	debugHalo(halo, "anl_prf: do_profiles %d, do_correct %d (prf_idx %d/%d, is_host %d/%d).",
			do_profiles, do_correct, config.anl_prf_snap_idx[snap_idx],
			config.anl_prf_snap_idx[snap_idx - 1], isHost(halo), wasHostLastSnap(halo, snap_idx));

	if (!do_profiles && !do_correct)
		return;

	/*
	 * Find profile analysis object. If we are adding a new one, retroactively set the
	 * HALO_NOT_VALID status for all times since the halo was created.
	 */
	int i, prof_idx;
	AnalysisProfiles *al_prf = NULL;
	TreeResults *tr_res;

	if (halo->al[PRF].n == 0)
	{
		al_prf = addAnalysisProfiles(__FFL__, &(halo->al[PRF]));
		al_prf->halo_id = haloOriginalID(halo);
		for (i = halo->first_snap; i < snap_idx; i++)
			al_prf->status[i] = ANL_PRF_STATUS_HALO_NOT_VALID;
#if CAREFUL
		if (do_correct)
			error(__FFL__,
					"Creating profile analysis but also correcting previous analysis (halo ID %ld).\n",
					halo->cd.id);
#endif
	} else if (halo->al[PRF].n > 1)
	{
		error(__FFL__, "Found %d AnalysisProfiles objects in halo ID %ld.\n", halo->al[PRF].n,
				halo->cd.id);
	} else
	{
		al_prf = &(halo->al[PRF].al_prf[0]);
	}

	/*
	 * If we are trying to compute profiles, do a few additional checks. There are a few conditions
	 * that might prevent us from computing profiles.
	 */
	prof_idx = config.anl_prf_snap_idx[snap_idx];
	if (do_profiles)
	{
		/*
		 * If we are computing profiles, check that the search radius could be fulfilled; if not, we
		 * should not calculate profiles because they will be missing particles. If we are not
		 * computing profiles, we indicate that in the status field.
		 */
		if (halo->search_radius_decreased)
		{
			if (halo->r_search_com_actual < haloR200mComoving(halo, snap_idx) * config.anl_prf_rmax)
			{
				al_prf->status[prof_idx] = ANL_PRF_STATUS_SEARCH_RADIUS;
				do_profiles = 0;
				debugHalo(halo,
						"anl_prf: not computing profile because search radius was decreased.");
			}
		} else
		{
#if PARANOID
			if (halo->r_search_com_actual < config.anl_prf_rmax * haloR200mComoving(halo, snap_idx))
				error(__FFL__,
						"Did not find search_radius_decreased flag, but search radius too small for profiles (halo ID %ld, rsearch %.2f, expected %.2f).\n",
						halo->cd.id, halo->r_search_com_actual,
						config.anl_prf_rmax * haloR200mComoving(halo, snap_idx));
#endif
		}

		/*
		 * In some rare cases, R200m_all could not be found and we had to fallback to the catalog
		 * radius. In this case, we do not compute profiles as they would not be normalized by the
		 * same radius definition as for other halos.
		 */
		if (halo->radius_from_catalog)
		{
			al_prf->status[prof_idx] = ANL_PRF_STATUS_CATALOG_RADIUS;
			do_profiles = 0;
			debugHalo(halo, "anl_prf: not computing profile because R200m could not be computed.");
		}
	} else
	{
		if (prof_idx > -1)
			al_prf->status[prof_idx] = ANL_PRF_STATUS_HALO_NOT_VALID;
	}

#if PRF_DEBUG
	if (halo->cd.id == PRF_DEBUG_HALO)
		output(0, "PRF_DEBUG: HALO %ld, desc ID %ld, prog ID %ld, orig ID %ld\n", PRF_DEBUG_HALO,
				halo->cd.desc_id, halo->history_id[snap_idx - 1], haloOriginalID(halo));
#endif

	/*
	 * The tracer results passed to this function should never be NULL if we are trying to compute
	 * profiles.
	 */
	tr_res = (TreeResults*) tr_res_vp;
#if CAREFUL
	if (tr_res == NULL)
		error(__FFL__,
				"Found NULL for tree finding results, this should not happen in profile routine (halo ID %ld).\n",
				halo->cd.id);
#endif

	if (do_correct)
		correctProfiles(snap_idx, halo, al_prf);
	if (do_profiles)
		computeProfiles(snap_idx, halo, al_prf, tr_res);
}

/*
 * Obtain information on particles; we combine their radius and orbit info into a struct that
 * can then be sorted. Then compute all profiles selected.
 */
void computeProfiles(int snap_idx, Halo *halo, AnalysisProfiles *al_prf, TreeResults *tr_res)
{
	int i, prof_idx, n_particles, is_host;
	float R200m, M200m;
	OrbitCountPtlInfo *ptl_info;

#if DO_ANALYSIS_PROFILES_1HALO
	int n_res_found, n_res_not_found;
	void *ptr;
	ID ptl_id;
	ResultOrbitCount *tmp_res;

	n_res_found = 0;
	n_res_not_found = 0;
#endif

	is_host = isHost(halo);
	n_particles = tr_res->num_points;
	prof_idx = config.anl_prf_snap_idx[snap_idx];
	R200m = haloR200m(halo, snap_idx);
	M200m = haloM200m(halo, snap_idx);

	ptl_info = (OrbitCountPtlInfo*) memAlloc(__FFL__, MID_ALPRF,
			sizeof(OrbitCountPtlInfo) * n_particles);

	for (i = 0; i < n_particles; i++)
	{
		ptl_info[i].r = tracerRadius(snap_idx, halo->cd.x, tr_res->points[i]->x);

#if PRF_DEBUG
		if ((halo->cd.id == PRF_DEBUG_HALO) && (tr_res->points[i]->id == PRF_DEBUG_TCR))
		{
			output(0,
					"PRF DEBUG: HALO %ld, TRACER %ld, found halo at x [%.3f %.3f %.3f], particle at x [%.3f %.3f %.3f], setting radius %.2f.\n",
					PRF_DEBUG_HALO, PRF_DEBUG_TCR, halo->cd.x[0], halo->cd.x[1], halo->cd.x[2],
					tr_res->points[i]->x[0], tr_res->points[i]->x[1], tr_res->points[i]->x[2],
					ptl_info[i].r);
		}
#endif

#if DO_ANALYSIS_PROFILES_1HALO
		ptl_id = tr_res->points[i]->id;
		ptr = bsearch(&(ptl_id), halo->tt[PTL].rs[OCT].data, halo->tt[PTL].rs[OCT].n,
				rsprops[OCT].size, &compareResults);
		if (ptr != NULL)
		{
			tmp_res = (ResultOrbitCount*) ptr;
			ptl_info[i].n_pericenter = tmp_res->n_pericenter;
			ptl_info[i].n_is_lower_limit = tmp_res->n_is_lower_limit;
			n_res_found++;
		} else
		{
			/*
			 * Check if particle is inside halo -- if so, this has to be an error. However, this
			 * situation can be OK in subhalos and ghosts, where we are not adding new tracers.
			 */
#if PARANOID
			if (isHost(halo) && (ptl_info[i].r < R200m))
				error(__FFL__,
						"Found particle ID %ld without oct inside halo (halo orig ID %ld, r %.2f, R %.2f, rR %.2f).\n",
						tr_res->points[i]->id, haloOriginalID(halo), ptl_info[i].r, R200m,
						ptl_info[i].r / R200m);
#endif

			ptl_info[i].n_pericenter = 0;
			ptl_info[i].n_is_lower_limit = 0;
			n_res_not_found++;
		}
#endif

#if PRF_DEBUG
		ptl_info[i].tcr_id = tr_res->points[i]->id;
#endif
	}
	qsort(ptl_info, n_particles, sizeof(OrbitCountPtlInfo), &compareOrbitCountPtlInfo);

#if PRF_DEBUG
	if (halo->cd.id == PRF_DEBUG_HALO)
		output(0, "PRF_DEBUG: HALO %ld, creating profile, found %d particles, R200m %.4e.\n",
				halo->cd.id, n_particles, R200m);
#endif

	int debug_bin = -1;
#if PRF_DEBUG
	if (halo->cd.id == PRF_DEBUG_HALO)
		debug_bin = PRF_DEBUG_BIN;
	else
		debug_bin = -1;
#endif

#if DO_ANALYSIS_PROFILES_ALL
	profileFromParticles(ptl_info, n_particles, &(al_prf->M_all[prof_idx][0]), SELECTION_ALL, R200m,
			M200m, debug_bin, is_host, halo->cd.phantom, halo->cd.id);
#endif
#if DO_ANALYSIS_PROFILES_1HALO
	profileFromParticles(ptl_info, n_particles, &(al_prf->M_1halo[prof_idx][0]), SELECTION_1HALO,
			R200m, M200m, debug_bin, is_host, halo->cd.phantom, halo->cd.id);
#endif

	memFree(__FFL__, MID_ALPRF, ptl_info, n_particles * sizeof(OrbitCountPtlInfo));

	al_prf->status[prof_idx] = ANL_PRF_STATUS_SUCCESS;
}

/*
 * Decide if a particle needs to be retroactively added to the 1-halo term because it had a
 * pericenter at the previous snapshot that was only detected at this snapshot. Note that this
 * cannot happen if the lower_limit flag is set, since that is set before the profiles are
 * computed and is thus already taken into account.
 */
int includeInCorrection(int snap_idx, ResultOrbitCount *res_oct)
{
#if LOWER_LIMIT_IN_1HALO
	return ((res_oct->last_pericenter_snap == snap_idx - 1) && (res_oct->n_pericenter == 1)
			&& (!res_oct->n_is_lower_limit));
#else
	return ((res_oct->last_pericenter_snap == snap_idx - 1) && (res_oct->n_pericenter == 1));
#endif
}

/*
 * This function finds all tracers that have a pericenter added in this snapshot. In that case, the
 * last snapshot was already past the pericenter and the particles were thus misclassified as
 * having one fewer orbits.
 *
 * The information about the last pericenter is stored in the orbit count results, so we need to
 * go through all results and see if there are any where the last_pericenter snap is the same as
 * the snap_idx to be corrected. Then, we need to find the corresponding tracer to get the radius
 * at that snapshot.
 */
void correctProfiles(int snap_idx, Halo *halo, AnalysisProfiles *al_prf)
{
	/*
	 * If the 1-halo profile is not on, there is nothing to correct.
	 */
#if !DO_ANALYSIS_PROFILES_1HALO
	return;
#else

	/*
	 * Compute the index of the profile to be corrected, and check that there is a valid profile.
	 * If the profile had to be aborted in the previous snapshot, there is nothing to correct.
	 *
	 * If the profile was not aborted but the status is not SUCCESS, there is some sort of error
	 * that indicates that we should not be correcting in the first place. The same is true if the
	 * status is in order, but the profile contains uninitialized (negative) values.
	 */
	int prof_idx;

	prof_idx = config.anl_prf_snap_idx[snap_idx - 1];
	if (al_prf->status[prof_idx] == ANL_PRF_STATUS_SEARCH_RADIUS)
	{
		return;
	} else if (al_prf->status[prof_idx] == ANL_PRF_STATUS_CATALOG_RADIUS)
	{
		return;
	} else if (al_prf->status[prof_idx] != ANL_PRF_STATUS_SUCCESS)
	{
		error(__FFL__,
				"Found unexpected status %d in profile to be corrected (halo ID %ld, profile snap %d, profile idx %d).\n",
				al_prf->status[prof_idx], halo->cd.id, snap_idx - 1, prof_idx);
	}
	if (al_prf->M_1halo[prof_idx][0] < 0.0)
	{
		error(__FFL__,
				"Found invalid profile to be corrected (halo ID %ld, profile snap %d, profile idx %d).\n",
				halo->cd.id, snap_idx - 1, prof_idx);
	}

	/*
	 * Count the number of particles that had their first pericenter detected in this snapshot. If
	 * this number is zero, there is nothing to do.
	 */
	int i, n_ptl_correct, is_host;

	is_host = isHost(halo);
	n_ptl_correct = 0;
	for (i = 0; i < halo->tt[PTL].rs[OCT].n; i++)
	{
		if (includeInCorrection(snap_idx, &(halo->tt[PTL].rs[OCT].rs_oct[i])))
			n_ptl_correct++;
	}
	if (n_ptl_correct == 0)
		return;

	/*
	 * Gather the ptl info for all correcting particles. The radius needs to be reconstructed from
	 * the trajectory of the particle tracer (STCL_TCR - 2 for the previous snap).
	 */
	int counter;
	float R200m_prev, M200m_prev, prof[ANALYSIS_PROFILES_N_BINS];
	OrbitCountPtlInfo *ptl_info;
	Tracer *tcr, temp_tcr;
	ResultOrbitCount *res_oct;

	ptl_info = (OrbitCountPtlInfo*) memAlloc(__FFL__, MID_ALPRF,
			sizeof(OrbitCountPtlInfo) * n_ptl_correct);

	counter = 0;
	for (i = 0; i < halo->tt[PTL].rs[OCT].n; i++)
	{
		res_oct = &(halo->tt[PTL].rs[OCT].rs_oct[i]);
		if (includeInCorrection(snap_idx, res_oct))
		{
			temp_tcr.id = res_oct->tracer_id;
			tcr = (Tracer*) bsearch(&temp_tcr, halo->tt[PTL].tcr.tcr, halo->tt[PTL].tcr.n,
					sizeof(Tracer), &compareTracers);
			if (tcr == NULL)
				error(__FFL__,
						"In profile correction, could not find tracer for particle ID %ld.\n",
						res_oct->tracer_id);

			ptl_info[counter].r = tcr->r[STCL_TCR - 2];
#if PRF_DEBUG
			ptl_info[counter].tcr_id = tcr->id;
#endif
			counter++;
		}
	}

#if CAREFUL
	if (counter != n_ptl_correct)
		error(__FFL__, "Incorrect particle number.\n");
#endif

	qsort(ptl_info, n_ptl_correct, sizeof(OrbitCountPtlInfo), &compareOrbitCountPtlInfo);

	/*
	 * Compute profile from correcting particles and add it to the previously computed 1-halo
	 * profile.
	 */
	R200m_prev = haloR200m(halo, snap_idx - 1);
	M200m_prev = haloM200m(halo, snap_idx - 1);

#if PRF_DEBUG
	if (halo->history_id[snap_idx - 1] == PRF_DEBUG_HALO)
		output(0, "PRF_DEBUG: Correcting halo %ld, found %d particles, R200m_prev %.4e.\n",
				halo->history_id[snap_idx - 1], n_ptl_correct, R200m_prev);
#endif

	int debug_bin = -1;

#if DO_ANALYSIS_PROFILES_1HALO
#if PRF_DEBUG
	if (halo->history_id[snap_idx - 1] == PRF_DEBUG_HALO)
		debug_bin = PRF_DEBUG_BIN;
	else
		debug_bin = -1;
#endif

	/*
	 * Do the actual correction: we compute the mass profile of particles to be corrected and
	 * add it to the 1-halo term (effectively subtracting it from the infall term). Note that both
	 * the original and correcting profiles are cumulative, meaning they are simply added together.
	 */
	profileFromParticles(ptl_info, n_ptl_correct, &(prof[0]), SELECTION_CORRECT, R200m_prev,
			M200m_prev, debug_bin, is_host, halo->cd.phantom, halo->cd.id);
	for (i = 0; i < ANALYSIS_PROFILES_N_BINS; i++)
	{
#if PARANOID
		if (al_prf->M_1halo[prof_idx][i] < -1E-20)
			error(__FFL__,
					"Found negative profile bin (halo ID %ld, prof_idx (to correct) %d, snap %d, bin %d, value %.2e, in ptl %.2f, added %.2f.\n",
					halo->cd.id, prof_idx, snap_idx, i, al_prf->M_1halo[prof_idx][i],
					al_prf->M_1halo[prof_idx][i] / config.particle_mass,
					prof[i] / config.particle_mass);
#endif
		al_prf->M_1halo[prof_idx][i] += prof[i];

		/*
		 * Sanity check: the 1ht profile cannot be larger than the total profile. This check is good
		 * at catching binning errors: if we are adding to a wrong bin (due to wrong coordinates,
		 * for example), one of those bins will likely become larger than the total profile and
		 * trigger this error.
		 */
#if DO_ANALYSIS_PROFILES_ALL
		if (al_prf->M_1halo[prof_idx][i] > al_prf->M_all[prof_idx][i] * 1.000001)
		{
#if PRF_DEBUG
			int j;
			output(0, "Correcting tracers:\n");
			for (j = 0; j < n_ptl_correct; j++)
				output(0, " ID %8ld r %.4e\n", ptl_info[j].tcr_id, ptl_info[j].r);
#endif
			float bin_low, bin_high;
			bin_high = config.anl_prf_r_bins_lin[i] * R200m_prev;
			if (i > 0)
				bin_low = config.anl_prf_r_bins_lin[i - 1] * R200m_prev;
			else
				bin_low = 0.0;

			error(__FFL__,
					"Found 1-halo > all in profile bin (halo ID %ld, prof_idx (to correct) %d, snap %d, bin %d [%.3f .. %.3f], R200m_prev %.4f, all prof %.4f, old 1-halo prof %.4f, add %.4f.\n",
					halo->cd.id, prof_idx, snap_idx, i, bin_low, bin_high, R200m_prev,
					al_prf->M_all[prof_idx][i] / config.particle_mass,
					(al_prf->M_1halo[prof_idx][i] - prof[i]) / config.particle_mass,
					prof[i] / config.particle_mass);
		}
#endif
	}
#endif

	memFree(__FFL__, MID_ALPRF, ptl_info, n_ptl_correct * sizeof(OrbitCountPtlInfo));
#endif
}

/*
 * Given an ordered list of particles, compute the cumulative mass profile based on various
 * selections.
 */
void profileFromParticles(OrbitCountPtlInfo *ptl_info, int n_particles, float *prof, int selection,
		float R200m, float M200m, int debug_bin, int is_host, int is_phantom, HaloID halo_id)
{
	int i, k, m_count;

	/*
	 * Go through, mark particles for inclusion or exclusion in this profile. If we are correcting,
	 * the particles have already been pre-selected.
	 */
	for (i = 0; i < n_particles; i++)
	{
		switch (selection)
		{
		case SELECTION_ALL:
		case SELECTION_CORRECT:
			ptl_info[i].use_in_profile = 1;
			break;
		case SELECTION_1HALO:
#if LOWER_LIMIT_IN_1HALO
			ptl_info[i].use_in_profile = ((ptl_info[i].n_pericenter >= 1)
					|| (ptl_info[i].n_is_lower_limit));
#else
			ptl_info[i].use_in_profile = (ptl_info[i].n_pericenter >= 1);
#endif
			break;
		}
	}

	/*
	 * Go through the particle array until all bins are filled. The bins represent the right edge
	 * of each bin, so we start at r = [0, bin[0]].
	 */
	i = 0;
	k = 0;
	m_count = 0;
	while (k < n_particles)
	{
		/*
		 * First, check if this particle lies beyond the current right edge; if so, we do not count
		 * it into the current bin but first increase the bin index until it includes the particle.
		 */
		while ((i < ANALYSIS_PROFILES_N_BINS)
				&& (ptl_info[k].r > config.anl_prf_r_bins_lin[i] * R200m))
		{
			prof[i] = (float) m_count;
#if PRF_DEBUG
			if (i == debug_bin)
			{
				float bin_low, bin_high;
				if (i == 0)
					bin_low = 0.0;
				else
					bin_low = config.anl_prf_r_bins_lin[i - 1] * R200m;
				bin_high = config.anl_prf_r_bins_lin[i] * R200m;
				output(0,
						"PRF_DEBUG: HALO %ld, BIN %2d, bin radius [%.2e %.2e], sel %d, final val %.2f ptl\n",
						PRF_DEBUG_HALO, debug_bin, bin_low, bin_high, selection, prof[i]);
			}
#endif
			i++;
		}

		/*
		 * The bin index has been set, now increase the cumulative mass count if we're including
		 * this particle in the profile.
		 */
		if (ptl_info[k].use_in_profile)
			m_count++;

#if PRF_DEBUG
		if (i == debug_bin)
		{
			output(0,
					"PRF_DEBUG: HALO %ld, BIN %2d, sel %d, ptl ID %8ld, r %.2e, use %d, mcount %d\n",
					PRF_DEBUG_HALO, debug_bin, selection, ptl_info[k].tcr_id, ptl_info[k].r,
					ptl_info[k].use_in_profile, m_count);
		}
#endif

#if PRF_DEBUG && (PRF_DEBUG_TCR > 0)
		if ((ptl_info[k].tcr_id == PRF_DEBUG_TCR) && (halo_id == PRF_DEBUG_HALO))
		{
			output(0,
					"PRF_DEBUG: HALO %ld, TRACER %ld, found in bin %2d, r %.2e, selection %d, used %d.\n",
					PRF_DEBUG_HALO, PRF_DEBUG_TCR, i, ptl_info[k].r, selection,
					ptl_info[k].use_in_profile);
		}
#endif

#if CAREFUL
		if ((k > 0) && (ptl_info[k].r < ptl_info[k - 1].r))
			error(__FFL__, "Found non-ascending particle radii (selection %d, n %d, idx %d/%d)\n",
					selection, n_particles, k - 1, k);
#endif

		k++;
	}

	/*
	 * If there are no more particles but we haven't reached the final bins, the cumulative mass
	 * profile is the total mass reached.
	 */
	while (i < ANALYSIS_PROFILES_N_BINS)
	{
		prof[i] = (float) m_count;
		i++;
	}

	/*
	 * Convert to mass instead of number count
	 */
	for (i = 0; i < ANALYSIS_PROFILES_N_BINS; i++)
	{
		if (prof[i] > 0.0)
			prof[i] = prof[i] * config.particle_mass;
	}

#if CAREFUL
	if ((selection == SELECTION_ALL) && (is_host) && (!is_phantom))
	{
		for (i = 0; i < ANALYSIS_PROFILES_N_BINS - 1; i++)
		{
			if ((config.anl_prf_r_bins_lin[i] > 1.0) && (prof[i] < M200m * 0.9)
					&& (M200m > 10 * config.particle_mass))
			{
				int j, max_print;

				max_print = imin(n_particles, (int) (M200m / config.particle_mass) + 10);

				for (j = 0; j < max_print; j++)
					output(0, "Ptl %4d r %.3e r/R200m %.3e\n", j, ptl_info[j].r,
							ptl_info[j].r / R200m);
				error(__FFL__,
						"Halo %ld, Mass is %.2e (%d ptl), should be at least %.2e (%d ptl) at bin %d = %.3f R200m, R200m = %.1f, is_host = %d, is_phantom = %d\n",
						halo_id, prof[i], (int) (prof[i] / config.particle_mass), M200m,
						(int) (M200m / config.particle_mass), i, config.anl_prf_r_bins_lin[i],
						R200m, is_host, is_phantom);
			}
		}
	}

	for (i = 0; i < ANALYSIS_PROFILES_N_BINS - 1; i++)
	{
		if (prof[i + 1] < prof[i])
		{
			output(2, "[%4d]  R200m %.4e\n", proc, R200m);
			for (i = 0; i < n_particles; i++)
				output(2, "[%4d] Particle %4d radius %.4e\n", proc, i, ptl_info[i].r);
			for (i = 0; i < ANALYSIS_PROFILES_N_BINS; i++)
				output(2, "[%4d] Mass bin %4d radius %.4e mass %.4e\n", proc, i,
						config.anl_prf_r_bins_lin[i] * R200m, prof[i]);
			error(__FFL__, "MASS BINS DECREASING\n");
		}

		if (prof[i + 1] * prof[i] < 0.0)
			error(__FFL__, "ERRORS INCONSISTENT\n");
	}
#endif
}

int compareOrbitCountPtlInfo(const void *a, const void *b)
{
	if (((OrbitCountPtlInfo*) a)->r > ((OrbitCountPtlInfo*) b)->r)
		return 1;
	else if (((OrbitCountPtlInfo*) a)->r < ((OrbitCountPtlInfo*) b)->r)
		return -1;
	else
		return 0;
}

/*
 * For 1D fields, there is no need to set any dimensions. For higher-dimensional fields, the higher
 * dimensions must be fixed.
 */
int outputFieldsAnalysisProfiles(OutputField *outfields, int max_n_fields)
{
	int counter;

	counter = 0;

	outfields[counter].dtype = DTYPE_INT64;
	outfields[counter].offset = offsetof(AnalysisProfiles, halo_id);
	sprintf(outfields[counter].name, "halo_id");
	counter++;

	outfields[counter].n_dims = 2;
	outfields[counter].dim_ext[1] = config.anl_prf_n_redshifts;
	outfields[counter].dtype = DTYPE_INT8;
	outfields[counter].offset = offsetof(AnalysisProfiles, status);
	sprintf(outfields[counter].name, "status");
	counter++;

#if OUTPUT_ANALYSIS_PROFILES_ALL
	outfields[counter].n_dims = 3;
	outfields[counter].dim_ext[1] = config.anl_prf_n_redshifts;
	outfields[counter].dim_ext[2] = ANALYSIS_PROFILES_N_BINS;
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(AnalysisProfiles, M_all);
	sprintf(outfields[counter].name, "M_all");
	counter++;
#endif

#if OUTPUT_ANALYSIS_PROFILES_1HALO
	outfields[counter].n_dims = 3;
	outfields[counter].dim_ext[1] = config.anl_prf_n_redshifts;
	outfields[counter].dim_ext[2] = ANALYSIS_PROFILES_N_BINS;
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(AnalysisProfiles, M_1halo);
	sprintf(outfields[counter].name, "M_1halo");
	counter++;
#endif

	return counter;
}

void outputConfigAnalysisProfiles(hid_t grp)
{
	hdf5WriteAttributeFloat(grp, "rmin", config.anl_prf_rmin);
	hdf5WriteAttributeFloat(grp, "rmax", config.anl_prf_rmax);
	hdf5WriteAttributeDouble1D(grp, "r_bins_lin", ANALYSIS_PROFILES_N_BINS,
			config.anl_prf_r_bins_lin);

	if (config.anl_prf_redshifts[0] < 0.0)
	{
		hdf5WriteAttributeFloat1D(grp, "snap_redshifts_requested", 1, config.anl_prf_redshifts);
	} else
	{
		hdf5WriteAttributeFloat1D(grp, "snap_redshifts_requested", config.anl_prf_n_redshifts,
				config.anl_prf_redshifts);
	}
	hdf5WriteAttributeFloat1D(grp, "snap_redshifts_actual", config.anl_prf_n_redshifts,
			config.anl_prf_redshifts_actual);
	hdf5WriteAttributeInt1D(grp, "snap_idxs", config.anl_prf_n_redshifts, config.anl_prf_snaps);
	hdf5WriteAttributeInt1D(grp, "snap_idxs_in_anl", config.n_snaps, config.anl_prf_snap_idx);
}

#endif
