/*************************************************************************************************
 *
 * This unit implements functions that are common to all analysis types.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "analyses.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initConfigAnalyses(int step)
{
	int al;

	for (al = 0; al < NAL; al++)
	{
		if (alprops[al].initConfig != NULL)
			alprops[al].initConfig(step);
	}
}

float searchRadiusAnalses(int snap_idx, Halo *halo)
{
	int al;
	float r_search_factor;

	r_search_factor = 0.0;
	for (al = 0; al < NAL; al++)
	{
		if (alprops[al].searchRadius != NULL)
			r_search_factor = fmax(r_search_factor, alprops[al].searchRadius(snap_idx, halo));
	}

	return r_search_factor;
}

/*
 * The first analysis function happens during the halo processing step where particle results are
 * available. If particle reading is off, the tree and tr_res pointers are NULL.
 */
void runAnalyses1(int snap_idx, Halo *halo, LocalStatistics *ls, Tree *tree, TreeResults *tr_res)
{
	int al;
	double current_time;

	current_time = MPI_Wtime();
	ls->timers_2[T2_HALOLOOP] += current_time;
	ls->timers_2[T2_ANALYSES] -= current_time;

	for (al = 0; al < NAL; al++)
	{
		ls->timers_4[al] -= current_time;
		if (alprops[al].runAnalysis1 != NULL)
			alprops[al].runAnalysis1(snap_idx, halo, (void*) &(ls), (void*) tree, (void*) tr_res);
		current_time = MPI_Wtime();
		ls->timers_4[al] += current_time;
	}

	current_time = MPI_Wtime();
	ls->timers_2[T2_ANALYSES] += current_time;
	ls->timers_2[T2_HALOLOOP] -= current_time;
}

/*
 * The second analysis step happens after all halos have been processed.
 */
void runAnalyses2(int snap_idx, Halo *halo, LocalStatistics *ls)
{
	int al;
	double current_time;

	current_time = MPI_Wtime();
	ls->timers_2[T2_HALOLOOP] += current_time;
	ls->timers_2[T2_ANALYSES] -= current_time;

	for (al = 0; al < NAL; al++)
	{
		ls->timers_4[al] -= current_time;
		if (alprops[al].runAnalysis2 != NULL)
			alprops[al].runAnalysis2(snap_idx, halo, (void*) &(ls));
		current_time = MPI_Wtime();
		ls->timers_4[al] += current_time;
	}

	current_time = MPI_Wtime();
	ls->timers_2[T2_ANALYSES] += current_time;
	ls->timers_2[T2_HALOLOOP] -= current_time;
}
