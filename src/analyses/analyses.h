/*************************************************************************************************
 *
 * This unit implements functions that are common to all analysis types.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _ANALYSES_H_
#define _ANALYSES_H_

#include "../global_types.h"
#include "../statistics.h"
#include "../tree.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Each analysis should output a status field that indicates whether the analysis succeeded for a
 * particular halo and perhaps redshift. There are a number of status codes that are common to
 * all analyses, whereas higher number codes are specific to each analysis:
 *
 * ANALYSIS_STATUS_UNDEFINED       This code should be used as a placeholder, but never be written
 *                                 into the output file; that would indicate an internal error.
 *
 * ANALYSIS_STATUS_SUCCESS         The analysis succeeded, all output values can be used.
 *
 * ANALYSIS_STATUS_HALO_NOT_VALID  The halo did not exist at a particular snapshot, or could not
 *                                 be analyzed for some other reason (e.g., it had not lived long
 *                                 enough). The exact meaning of this field depends on the
 *                                 specific analysis.
 *
 * ANALYSIS_STATUS_HALO_NOT_SAVED  The halo was not saved to the SPARTA output file at all. This
 *                                 error code is not used within SPARTA, but can be used when
 *                                 analyzing the output file (e.g., by MORIA).
 *
 * ANALYSIS_STATUS_NOT_FOUND       The halo was written to the SPARTA output file, but does not
 *                                 have the analysis in question associated with it. This error
 *                                 code is not used within SPARTA, but can be used when analyzing
 *                                 the output file (e.g., by MORIA).
 *
 * ANALYSIS_STATUS_OTHER           The lowest number that individual analyses can use for other
 *                                 status values not defined here.
 */
#define ANALYSIS_STATUS_UNDEFINED 0
#define ANALYSIS_STATUS_SUCCESS 1
#define ANALYSIS_STATUS_HALO_NOT_VALID 2
#define ANALYSIS_STATUS_HALO_NOT_SAVED 3
#define ANALYSIS_STATUS_NOT_FOUND 4
#define ANALYSIS_STATUS_OTHER 5

/*
 * The redshift tolerance determines how far a the nearest snapshot can be off a requested
 * redshift before an error is thrown.
 */
#define ANL_REDSHIFT_TOLERANCE 0.2

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initConfigAnalyses(int step);
float searchRadiusAnalses(int snap_idx, Halo *halo);
void runAnalyses1(int snap_idx, Halo *halo, LocalStatistics *ls, Tree *tree, TreeResults *tr_res);
void runAnalyses2(int snap_idx, Halo *halo, LocalStatistics *ls);

#endif
