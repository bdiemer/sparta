/*************************************************************************************************
 *
 * This unit implements an analysis for halo properties such as SO radii.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _ANALYSIS_HALOPROPS_H_
#define _ANALYSIS_HALOPROPS_H_

#include "../global_types.h"
#include "../io/io_hdf5.h"

#include "analyses.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * The halo properties analysis currently uses only standard error codes.
 *
 * Note that these constants must be outside the #if protected block because they are used in
 * analysis software.
 */
#define ANL_HPS_STATUS_UNDEFINED ANALYSIS_STATUS_UNDEFINED
#define ANL_HPS_STATUS_SUCCESS ANALYSIS_STATUS_SUCCESS
#define ANL_HPS_STATUS_HALO_NOT_VALID ANALYSIS_STATUS_HALO_NOT_VALID
#define ANL_HPS_STATUS_HALO_NOT_SAVED ANALYSIS_STATUS_HALO_NOT_SAVED
#define ANL_HPS_STATUS_NOT_FOUND ANALYSIS_STATUS_NOT_FOUND
#define ANL_HPS_STATUS_SO_TOO_SMALL (ANALYSIS_STATUS_OTHER)
#define ANL_HPS_STATUS_SO_TOO_LARGE (ANALYSIS_STATUS_OTHER + 1)
#define ANL_HPS_STATUS_ORB_ZERO (ANALYSIS_STATUS_OTHER + 2)

#if DO_ANALYSIS_HALOPROPS

#define DEFAULT_ANL_HPS_R_MAX_SO_HOST 2.0
#define DEFAULT_ANL_HPS_R_MAX_SO_SUB 2.0
#define DEFAULT_ANL_HPS_R_MAX_ORB_HOST 3.0
#define DEFAULT_ANL_HPS_R_MAX_ORB_SUB 3.0
#define DEFAULT_ANL_HPS_R_UNBINDING_HOST 1.0
#define DEFAULT_ANL_HPS_R_UNBINDING_SUB 1.0
#define DEFAULT_ANL_HPS_ITERATIVE_UNBINDING 0

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initAnalysisHaloProps(AnalysisHaloProps *al_hps);
void initConfigAnalysisHaloprops(int step);
float searchRadiusAnalysisHaloProps(int snap_idx, Halo *halo);
void runAnalysisHaloProps(int snap_idx, Halo *halo, void *ls_vp, void *tree_vp, void *tr_res_vp);
int outputFieldsAnalysisHaloProps(OutputField *outfields, int max_n_fields);
void outputConfigAnalysisHaloProps(hid_t grp);

#endif

#endif
