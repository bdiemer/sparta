/*************************************************************************************************
 *
 * This module contains the memory management system for SPARTA.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>

#include "memory.h"
#include "config.h"

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

MemoryStatistics mem_stats;

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void addToMemStats(int mem_id, long int size);

/*************************************************************************************************
 * FUNCTIONS - GENERAL MEMORY ALLOCATION
 *************************************************************************************************/

void* memAlloc(const char *file, const char *func, int line, int mem_id, size_t size)
{
	void *mem;

	if (config.log_level_memory > 3)
		output(0, "[%4d] FILE %s, FUNCTION %s, LINE %d: Allocating %zu bytes, mem_id %d.\n", proc,
				file, func, line, size, mem_id);

	mem = malloc(size);
	if (mem == NULL)
		error(file, func, line, "[%4d] Memory allocation failed (size %zu).\n", proc, size);

	if (config.log_level_memory > 0)
		addToMemStats(mem_id, size);

	return mem;
}

void* memRealloc(const char *file, const char *func, int line, int mem_id, void *ptr,
		size_t new_size, size_t old_size)
{
	void *mem;

	if (config.log_level_memory > 3)
		output(0,
				"[%4d] FILE %s, FUNCTION %s, LINE %d: Re-allocating %zu bytes to %zu bytes, mem_id %d.\n",
				proc, file, func, line, old_size, new_size, mem_id);

	mem = realloc(ptr, new_size);
	if (mem == NULL)
		error(file, func, line, "[%4d] Memory re-allocation failed (new size %zu).\n", proc,
				new_size);

	if (config.log_level_memory > 0)
		addToMemStats(mem_id, new_size - old_size);

	return mem;
}

void memFree(const char *file, const char *func, int line, int mem_id, void *mem, size_t old_size)
{
	if (config.log_level_memory > 3)
		output(0, "[%4d] FILE %s, FUNCTION %s, LINE %d: Freeing %zu bytes, mem_id %d.\n", proc,
				file, func, line, old_size, mem_id);

	if (mem)
		free(mem);

	if (config.log_level_memory > 0)
		addToMemStats(mem_id, -old_size);
}

void addToMemStats(int mem_id, long int size)
{
	int i;

	if (mem_id == MID_IGNORE)
		return;

	mem_stats.current[mem_id] += size;
	if (mem_stats.current[mem_id] > mem_stats.peak[mem_id])
		mem_stats.peak[mem_id] = mem_stats.current[mem_id];

	mem_stats.cur_tot += size;
	if (mem_stats.cur_tot > mem_stats.peak_tot)
	{
		mem_stats.peak_tot = mem_stats.cur_tot;
		for (i = 0; i < MID_N; i++)
			mem_stats.at_tot_peak[i] = mem_stats.current[i];
	}
}

void resetMemoryStatistics()
{
	int i;

	mem_stats.cur_tot = 0;
	mem_stats.peak_tot = 0;
	for (i = 0; i < MID_N; i++)
	{
		mem_stats.current[i] = 0;
		mem_stats.peak[i] = 0;
		mem_stats.at_tot_peak[i] = 0;
	}
}
