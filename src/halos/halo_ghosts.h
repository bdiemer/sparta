/*************************************************************************************************
 *
 * This unit implements ghosts, halos that are not present in the halo finder catalogs.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _HALO_GHOSTS_H_
#define _HALO_GHOSTS_H_

#include "../global_types.h"
#include "../tree.h"
#include "../geometry.h"

#if DO_GHOSTS

/*
 * These defines need to be in the header file because they trigger changes outside of the ghost
 * unit. If DEBUG_OUTPUT_GHOSTS, files with ghost information are output at each snapshot (they
 * are written to a subdirectory called "ghosts").
 */
#define DEBUG_OUTPUT_GHOSTS 0
#define DEBUG_OUTPUT_GHOSTS_ID 0
#define DEBUG_OUTPUT_GHOSTS_STRIDE 20
#define DEBUG_OUTPUT_GHOSTS_MIN_PTL 10

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

ID createGhostID(Halo *halo, int snap_idx);
int makeHaloGhost(DynArray *halos, Halo *halo, int snap_idx);
void setHaloStatusGhost(int snap_idx, Halo *ghost, Halo *host);

void determineGhostProperties(Halo *halo, Halo *host, int snap_idx, TreeResults *tr_res);
void determinePhantomProperties(Halo *halo, Halo *host, int snap_idx, TreeResults *tr_res);

#if DEBUG_OUTPUT_GHOSTS
void outputGhostDebugFile(Halo *halo, Halo *host, int snap_idx, TreeResults *tr_res, Tree *tree);
#endif

#endif
#endif
