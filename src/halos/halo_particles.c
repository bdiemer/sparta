/*************************************************************************************************
 *
 * This unit implements routines to find and analyze the particle distribution around halos.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "halo_particles.h"
#include "halo.h"
#include "halo_ghosts.h"
#include "halo_so.h"
#include "../constants.h"
#include "../config.h"
#include "../memory.h"
#include "../utils.h"
#include "../tracers/tracers.h"
#include "../analyses/analyses.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * In SPARTA, the halo radius is considered to be a strict spherical overdensity (no unbinding)
 * R200m. This radius can be determined from the particle distribution, but in order to obtain
 * all the particles in the halo, we need to guess a search radius for the tree search first and,
 * if necessary, refine this guess.
 *
 * If the halo in the catalog has less than cat_reliable_n200m particles, we use the
 * very conservative SEARCH_RADIUS_FACTOR_UNRELIABLE estimate of R200m. If it has more particles,
 * we use SEARCH_RADIUS_GUESS_FACTOR. Both are larger than 1 because halo catalogs may or may not
 * have performed unbinding. However, if the halo catalog contains strict-SO R200m, the
 * SEARCH_RADIUS_GUESS_FACTOR can be set very close to 1. Since we are also taking the maximum
 * of the catalog radius and the previous radius, a factor of 1.5 means that the mass of a halo
 * would need to grow by more than a factor of ~3 between the snapshots to exceed this estimate.
 * This seems unlikely except in a mergere scenario where a halo can suddenly include another halo.
 *
 * If the search for a valid R200m fails, increase the search radius by this factor until a valid
 * radius is found. However, if the search sphere does not fit into the volume in memory any more,
 * the halo in question is aborted.
 */
#define SEARCH_RADIUS_GUESS_FACTOR 1.5
#define SEARCH_RADIUS_FACTOR_UNRELIABLE 3.0
#define SEARCH_RADIUS_HOST_INITIAL 1.5
#define SEARCH_RADIUS_HOST_INCREASE 1.3

/*
 * When looking for tracer particles, we can impose direct limits on the fraction of tracers that
 * we require to be found. We begin within a radius of SEARCH_RADIUS_TRACER_INITIAL times the
 * current halo radius (which should be generous and often gets all tracers on first pass). If less
 * than TRACER_FOUND_FRACTION_PASS tracers were found, keep expanding the radius by
 * SEARCH_RADIUS_TRACER_FACTOR until we read SEARCH_RADIUS_TRACER_MAX_BOX_FRAC times the box size.
 * Worst case, we allow halos with TRACER_FOUND_FRACTION_MIN of their tracers found to live on.
 */
#define SEARCH_RADIUS_TRACER_INITIAL 3.0
#define SEARCH_RADIUS_TRACER_INCREASE 2.0
#define SEARCH_RADIUS_TRACER_MAX_BOX_FRAC 0.1
#define TRACER_FOUND_FRACTION_PASS 0.99
#define TRACER_FOUND_FRACTION_MIN 0.2

/*
 * When considering ghosts, we multiply by yet another factor to account for the fact that the
 * ghost's position is unknown. However, this factor should not be too large because we can
 * otherwise run into extremely large search radii, for example if we compute profiles out to large
 * radii.
 */
#define SEARCH_RADIUS_FACTOR_GHOST 2.0

/*
 * We can compare the values for R200m and N200m we find to the catalog data. This comparison only
 * makes sense for halos above a certain particle number though, since otherwise small numerical
 * inaccuracies can dominate. The maximum radius difference in the N200m comparison ensures that
 * the radius used by SPARTA was actually the same as the catalog radius; if not, there is no
 * reason to expect that the particle numbers should match.
 *
 * For the R200m calculation, we trigger a warning/error if the fractional radius difference is
 * larger than max R diff for host halos.
 */
#define CATALOG_COMPARISON_N200M_MIN_PTL 5.0
#define CATALOG_COMPARISON_N200M_MAX_R_DIFF 0.01

#define CATALOG_COMPARISON_R200M_MIN_PTL 50
#define CATALOG_COMPARISON_R200M_MAX_R_DIFF 0.05

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

GSLVarsMassProfile gsl_mass_profile;

#if DO_MASS_PROFILE
MassProfileVars mass_profile_vars;
#endif

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void checkN200mAgainstCatalog(Halo *halo, int snap_idx, float *p_radii, int n_particles);
void checkR200mAgainstCatalog(Halo *halo, int snap_idx);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initGslMassProfile()
{
#if DO_MASS_PROFILE
	gsl_mass_profile.r200m_spline = gsl_spline_alloc(gsl_interp_linear, STCL_TCR);
	gsl_mass_profile.r200m_acc = gsl_interp_accel_alloc();

	gsl_mass_profile.spline_mass = gsl_spline2d_alloc(gsl_interp2d_bilinear, N_MBINS, STCL_TCR);
	gsl_mass_profile.acc_mass_r = gsl_interp_accel_alloc();
	gsl_mass_profile.acc_mass_t = gsl_interp_accel_alloc();
#endif
}

void endGslMassProfile()
{
#if DO_MASS_PROFILE
	gsl_spline2d_free(gsl_mass_profile.spline_mass);
	gsl_interp_accel_free(gsl_mass_profile.acc_mass_r);
	gsl_interp_accel_free(gsl_mass_profile.acc_mass_t);
#endif
}

/*
 * This function currently only initializes the mass profile variables, but there could, in
 * principle, be other variables to be initialized, thus the general name.
 */
void initMassProfileVars()
{
#if DO_MASS_PROFILE
	int i;

	mass_profile_vars.initialized = 0;
	for (i = 0; i < STCL_TCR; i++)
	{
		mass_profile_vars.grid_t[i] = INVALID_T;
		mass_profile_vars.grid_R200m[i] = INVALID_R;
	}
	for (i = 0; i < STCL_TCR * N_MBINS; i++)
		mass_profile_vars.grid_M[i] = INVALID_M;
#endif
}

/*
 * At each snapshot, we need to guess how large a sphere around the halo we will have to
 * consider for the next snapshot. This function is important for performace, since it will
 * determine the size of the volume that needs to be read from the snapshots as well as the number
 * of particles found in each tree search. The purpose of the function is three-fold, in that we
 * need to determine:
 *
 * 1) The factor by which we need to multiply R200m to get the final particle set, including the
 *    needs of tracers, density profiles, the halo mass profile and so on.
 * 2) A guess for what the radius will be, to be used when determining the overall size of the
 *    box for this process; at this point, we do not know the actual R200m yet.
 * 3) A guess for the initial search radius to be used when computing R200m, which should ideally
 *    be only slightly larger than the actual R200m.
 *
 *  Inputs we have to consider include:
 *
 * - R200m as computed in the previous snapshot (if available)
 * - R200m from the catalog
 * - the radius below which we accept new particle trajectories
 * - the radii demanded by certain analyses
 *
 * If this is the halo's first snapshot, we have no information except the catalog R200m and
 * multiply R200m_cat by some factor > 1.
 *
 * Note that the calculation involving r and v is in physical Mpc/h units, whereas the halo
 * properties are in comoving Mpc/h.
 */

void setSearchRadius(Halo *halo, int snap_idx)
{
	int is_sub, is_ghost, use_r_infall;
	float fac_tracers, fac_analyses, R200m_guess, M200m_guess, R200m_last_com, M200m_last,
			R200m_cat_com, R200m_cat_phys, M200m_cat, R200m_max_cat_last;

	/*
	 * Task 1: find the final search radius in units of R200m. We start from zero and keep taking
	 * the maximum of the possible radii. Note that we cannot include any safety factors in this
	 * part, as this factor will be used in the actual final search.
	 */
	halo->r_search_factor = 0.0;
	halo->needs_all_ptl = 0;
	is_sub = isSubPermanently(halo);
	is_ghost = isGhost(halo);

	/*
	 * Find tracer radius. For ghosts and tracked subhalos, we use the sub-tagging radius, for
	 * hosts the delete radius (which must be larger than the create radius). We could take into
	 * account the current tracer population: if there are no tracers expected to be between the
	 * create and delete radii, then we could not include that region. However, this estimate
	 * means looping over all tracers and is very unlikely to make a large difference in the end.
	 */
	fac_tracers = 0.0;
	if (is_ghost)
	{
		fac_tracers = config.tcr_ptl_subtag_radius;
	} else if (is_sub)
	{
#if DO_SUBHALO_TRACKING
		fac_tracers = config.tcr_ptl_subtag_radius;
#else
		fac_tracers = 0.0;
#endif
	} else
	{
		fac_tracers = config.tcr_ptl_delete_radius;
		halo->needs_all_ptl = 1;
	}
	halo->r_search_factor = fmax(halo->r_search_factor, fac_tracers);

	/*
	 * If we are computing the mass profile, we need to go out to the largest radial bin. As the
	 * mass profile goes out to the delete radius, this limit has likely already been implemented
	 * via the tracer limit above, but we explicitly enforce it here to respect the decisions in
	 * the hasMassProfile() function.
	 */
	if (hasMassProfile(halo))
	{
		halo->r_search_factor = fmax(halo->r_search_factor, config.tcr_ptl_delete_radius);
		halo->needs_all_ptl = 1;
	}

	/*
	 * Analyses can have their own demands on the search radius, for example if we are computing
	 * density profiles out to large radii. These radii can depend on many factors, including user
	 * settings and the current snapshot; thus, we use functions within each analysis to tell us
	 * how far out we need to go.
	 */
	fac_analyses = searchRadiusAnalses(snap_idx, halo);
	if (fac_analyses > 0.0)
	{
		halo->r_search_factor = fmax(halo->r_search_factor, fac_analyses);
		halo->needs_all_ptl = 1;
	}

	/*
	 * Task 2: find a guess for R200m to be used when determining the halo box. Here, we need to
	 * be generous as an insufficient halo box will lead to serious issues.
	 *
	 * If this is the first snap, we have no more information and use the catalog R200m as our
	 * best guess. Otherwise, we check whether the halo is very small or whether the new catalog
	 * radius is larger. If the previous mass was very low or the catalog estimate is larger, we
	 * use the catalog estimate. Otherwise we use the previous estimate.
	 */
#if DO_SUBHALO_TRACKING
	use_r_infall = 0;
#else
	use_r_infall = is_sub;
#endif

	R200m_cat_com = halo->cd.R200m_cat_com;
	R200m_cat_phys = physicalRadius(R200m_cat_com, snap_idx);
	M200m_cat = soRtoM(R200m_cat_phys, config.this_snap_rho_200m);

	if (snap_idx == halo->first_snap)
	{
		R200m_guess = R200m_cat_com;
		M200m_guess = M200m_cat;
		R200m_last_com = R200m_cat_com;
		R200m_max_cat_last = R200m_cat_com;
	} else
	{
		R200m_last_com = haloR200mComoving(halo, snap_idx - 1);
		M200m_last = haloM200m(halo, snap_idx - 1);
		R200m_max_cat_last = fmax(R200m_last_com, R200m_cat_com);

		/*
		 * If the halo is a ghost, we need a really generous search radius because we do not even
		 * know the halo's position accurately. This should not cause too many problems in general
		 * because all ghosts are subhalos, meaning they should be well contained within their
		 * host's search radius.
		 *
		 * If we are using R200m_last for subhalos, we can set that radius without any additional
		 * safety factors.
		 *
		 * Otherwise, we need to make a conservative worst-case estimate of how big the
		 * real R200m might be. If the halo is very small, things can get pretty weird, so we use a
		 * much larger estimate than otherwise.
		 */
		if (is_ghost)
		{
			R200m_guess = R200m_last_com * SEARCH_RADIUS_FACTOR_GHOST;
		} else
		{
			if (use_r_infall)
			{
				R200m_guess = R200m_last_com;
			} else
			{
				R200m_guess = R200m_max_cat_last;
				M200m_guess = fmax(M200m_last, M200m_cat);

				if (M200m_guess < config.reliable_mass_R200m)
					R200m_guess *= SEARCH_RADIUS_FACTOR_UNRELIABLE;
			}
		}
	}

	R200m_guess *= SEARCH_RADIUS_GUESS_FACTOR;
	halo->r_search_com_guess = R200m_guess * halo->r_search_factor;

	/*
	 * Check that the search radius has not reached some crazy value.
	 */
	if (halo->r_search_com_guess > config.box_size * 0.5)
	{
		error(__FFL__,
				"[%4d] Search radius larger than half the box size; halo ID %ld, R_search %.2e, R200m_guess %.2e, R200m_cat %.2e, R200m_last %.2e (all comoving), search factor %.2f.\n",
				proc, halo->cd.id, halo->r_search_com_guess, R200m_guess, R200m_cat_com,
				R200m_last_com, halo->r_search_factor);
	}

	/*
	 * Task 3: find an initial guess for the radius search. If this guess is too small, we
	 * iteratively increase it; thus, this estimate is not as important. If we are not tracking
	 * subhalos, this estimate should not be used for subs since we do not need to determine their
	 * radius.
	 */
	if (is_ghost)
	{
		halo->r_search_com_initial = R200m_last_com * SEARCH_RADIUS_FACTOR_GHOST;
	} else
	{
		if (use_r_infall)
		{
			halo->r_search_com_initial = R200m_last_com;
		} else
		{
			if (is_sub)
				halo->r_search_com_initial = R200m_max_cat_last * SEARCH_RADIUS_TRACER_INITIAL;
			else
				halo->r_search_com_initial = R200m_max_cat_last * SEARCH_RADIUS_HOST_INITIAL;
		}
	}

	debugHalo(halo,
			"Set search radius guess to %.2f comoving Mpc/h, %.2f times R200m_guess = %.2f; initial %.2f.",
			halo->r_search_com_guess, halo->r_search_factor, R200m_guess,
			halo->r_search_com_initial);
}

/*
 * We need to loop here, because some weird halos need a very large search radius in order to
 * determine a valid R200m.
 */
void findRadiusHost(Halo *halo, int snap_idx, Tree *tree, Box *box_halos)
{
#if PARANOID
	if (!usesAllParticles(halo))
		error(__FFL__, "Found non-host halo in host function.\n");
#endif

	int ret, found_R200m, iter_R200m, n_ptl, nptl_old;
	float R200m, M200m, M200m_cat, R200m_cat_phys, *p_radii, diff, r_search_com;
	TreeResults *tr_res;

	R200m_cat_phys = physicalRadius(halo->cd.R200m_cat_com, snap_idx);
	M200m_cat = soRtoM(R200m_cat_phys, config.this_snap_rho_200m);

	p_radii = NULL;
	found_R200m = 0;
	iter_R200m = 0;
	nptl_old = 0;
	r_search_com = halo->r_search_com_initial;
	halo->radius_from_catalog = 0;

	while (!found_R200m)
	{
		tr_res = treeResultsInit();
		treeFindSpherePeriodicCorrected(tree, tr_res, halo->cd.x, r_search_com);
		n_ptl = tr_res->num_points;

		/*
		 * There is one rare case in which we might find zero particles: if a halo is a phantom (i.e.
		 * interpolated between snapshots). If that is not the case and we find zero particles,
		 * something has definitely gone wrong.
		 */
		if (n_ptl == 0)
		{
			if (halo->cd.phantom)
			{
				warningOrError(__FFL__, config.err_level_zero_ptl_phantom,
						"[%4d] [SN %3d] Found 0 particles for phantom halo ID %ld, status %d, xyz %.4f %.4f %.4f, r_search (com) %.4f, R200m_cat %.3f.\n",
						proc, snap_idx, halo->cd.id, halo->status, halo->cd.x[0], halo->cd.x[1],
						halo->cd.x[2], r_search_com, halo->cd.R200m_cat_com);
			} else
			{
				warningOrError(__FFL__, config.err_level_zero_ptl,
						"[%4d] [SN %3d] Found 0 particles for halo ID %ld, status %d, xyz %.4f %.4f %.4f, r_search (com) %.4f, R200m_cat %.3f.\n",
						proc, snap_idx, halo->cd.id, halo->status, halo->cd.x[0], halo->cd.x[1],
						halo->cd.x[2], r_search_com, halo->cd.R200m_cat_com);
			}

			R200m = R200m_cat_phys;
			M200m = M200m_cat;
			debugHalo(halo,
					"Radius finder: Found zero particles, setting catalog radius %.2e kpc/h and mass %.2e Msun/h.",
					R200m, M200m);
			break;
		}

		/*
		 * At this point, we wish to compute R200m for the halo manually from the particle
		 * distribution. However, this does not make sense for phantoms, so we skip them and
		 * set the halo finder definitions.
		 */
		if (halo->cd.phantom)
			break;

		/*
		 * Compute the radius of the particles, store them in a sorted array to compute the
		 * mass profile. Note that the radius calculation is repeated in order to save memory.
		 * Also note that we do not need to watch out for periodic BCs here since that was
		 * already done in the tree search function.
		 */
		if (iter_R200m == 0)
		{
			p_radii = (float*) memAlloc(__FFL__, MID_HALOLOGIC, sizeof(float) * n_ptl);
			nptl_old = n_ptl;
		} else
		{
			p_radii = (float*) memRealloc(__FFL__, MID_HALOLOGIC, p_radii, sizeof(float) * n_ptl,
					sizeof(float) * nptl_old);
			nptl_old = n_ptl;
		}
		particleListFromTreeResults(snap_idx, halo, tr_res, p_radii);

		/*
		 * Compute the current R200m_all. This can have a number of return values.
		 */
		ret = computeSOFromParticles(snap_idx, halo, p_radii, n_ptl, M200m_cat,
				config.this_snap_rho_200m, &R200m, &M200m);

		if (ret == SO_EXIT_SUCCESS)
		{
			/*
			 * Check against initial guess from halo finder. If too different, use a brute-force method.
			 */
			diff = R200m / R200m_cat_phys;
			if ((diff < 0.8) || (diff > 1.2))
			{
				int ret_bf;
				float R200m_bf, M200m_bf;

				ret_bf = computeSOFromParticlesBruteForce(snap_idx, halo, p_radii, n_ptl,
						config.this_snap_rho_200m, &R200m_bf, &M200m_bf);

				if (ret_bf == SO_EXIT_SUCCESS)
				{
					R200m = R200m_bf;
					M200m = M200m_bf;
					debugHalo(halo,
							"Radius finder: Reverted to brute-force solution %.2e kpc/h and mass %.2e Msun/h.",
							R200m, M200m);
				} else if (ret_bf == SO_EXIT_DENSITY_LOW)
				{
					debugHalo(halo,
							"Radius finder: Tried brute-force solution, but density was too low; sticking with %.2e kpc/h and mass %.2e Msun/h.",
							R200m, M200m);
				} else if (ret_bf == SO_EXIT_DENSITY_HIGH)
				{
					debugHalo(halo,
							"Radius finder: Tried brute-force solution, but density was too high; sticking with %.2e kpc/h and mass %.2e Msun/h.",
							R200m, M200m);
				} else
				{
					error(__FFL__, "Unknown return code from brute-force SO function.\n");
				}
			}
			found_R200m = 1;

		} else if (ret == SO_EXIT_DENSITY_LOW)
		{
			error(__FFL__,
					"Found density too low status from iterative SO function. Internal error.\n");
		} else if (ret == SO_EXIT_DENSITY_HIGH)
		{
			/*
			 * Look for cases where R200m_all exceeds R200m_cat by an extreme ratio. Typically,
			 * this indicates that the catalog mass is based on bound particles only and that the
			 * halo is near another, larger halo. In this case, the R200m_all mass may be so much
			 * larger that it causes problems.
			 */
			if ((r_search_com > halo->cd.R200m_cat_com * config.halo_max_radius_ratio_cat)
					&& (M200m_cat / config.particle_mass > 10))
			{
				warningOrError(__FFL__, config.err_level_radius_bound_diff,
						"[%4d] For halo ID %ld, cannot find R200m within factor of %.2f of catalog radius (%.2f kpc/h). Falling back to catalog radius.\n",
						proc, halo->cd.id, config.halo_max_radius_ratio_cat, R200m_cat_phys);
				R200m = R200m_cat_phys;
				halo->radius_from_catalog = 1;
				found_R200m = 1;
				break;
			}

			/*
			 * Increase the search radius and try again. However, check that the new search sphere is
			 * within the box. If not, we have to abort this halo, and do so at the previous snapshot
			 * since we cannot provide even a valid R200m at this snapshot.
			 */
			r_search_com *= SEARCH_RADIUS_HOST_INCREASE;

			if (!sphereInBox(halo->cd.x, r_search_com, box_halos))
			{
				warningOrError(__FFL__, config.err_level_radius_outside_box,
						"[%4d] Halo ID %ld, cannot find R200m radius within domain box (x %.2f %.2f %.2f, r %.2f, box [%.2f..%.2f %.2f..%.2f %.2f..%.2f). Aborting.\n",
						proc, halo->cd.id, halo->cd.x[0], halo->cd.x[1], halo->cd.x[2],
						r_search_com, box_halos->min[0], box_halos->max[0], box_halos->min[2],
						box_halos->max[2], box_halos->min[2], box_halos->max[2]);

				endHalo(halo, snap_idx, snap_idx - 1, HALO_ENDED_SEARCH_RADIUS);
				memFree(__FFL__, MID_HALOLOGIC, p_radii, n_ptl * sizeof(float));
				p_radii = NULL;
				n_ptl = 0;
				R200m = INVALID_R;
				found_R200m = 1;
			} else
			{
				debugHalo(halo,
						"Computing R200m failed on iteration %d, %7d particles, increased search radius to %.3e.",
						iter_R200m, n_ptl, r_search_com);
			}

			iter_R200m++;
		} else
		{
			error(__FFL__, "Unknown return code from SO function.\n");
		}

		treeResultsFree(tr_res);
		tr_res = NULL;

		/*
		 * Sanity check that this function is not stuck.
		 */
		if (iter_R200m > 20)
			error(__FFL__, "Too many iterations in radius finder for halo %ld.\n", halo->cd.id);
	}

	if (halo->cd.phantom)
	{
		halo->history_R200m[snap_idx] = R200m_cat_phys;
		debugHalo(halo,
				"Since halo is phantom, assigned catalog R200m %.2e; found R200m %.2e, M200m %.2e, %d particles.",
				halo->history_R200m[snap_idx], R200m, M200m, (int ) (M200m / config.particle_mass));
	} else
	{
		halo->history_R200m[snap_idx] = R200m;
		debugHalo(halo, "Assigned new R200m %.2e, catalog R200m %.2e, M200m %.2e, %d particles.",
				halo->history_R200m[snap_idx], R200m_cat_phys, M200m,
				(int ) (M200m / config.particle_mass));
	}

	/*
	 * If desired, perform checks against the catalog R200m and N200m; those only work for hosts
	 * and if the catalog definition is R200m_all.
	 */
	if (isAlive(halo) && isHost(halo) && (!halo->cd.phantom) && (!halo->radius_from_catalog))
	{
		if (config.err_level_cat_radius_diff != ERR_LEVEL_IGNORE)
			checkR200mAgainstCatalog(halo, snap_idx);
		if (config.err_level_missing_halo_ptl != ERR_LEVEL_IGNORE)
			checkN200mAgainstCatalog(halo, snap_idx, p_radii, n_ptl);
	}

	/*
	 * We might have exited the loop early, in which case we need to free memory
	 */
	if (tr_res != NULL)
		treeResultsFree(tr_res);
	if (p_radii != NULL)
		memFree(__FFL__, MID_HALOLOGIC, p_radii, n_ptl * sizeof(float));
}

/*
 * Given halo center and R200m, find all particles out to the search radius. In rare cases where we
 * misestimated the search radius previously, we may run up against the edge of the particle box.
 */
void findHaloParticles(Halo *halo, int snap_idx, Tree *tree, Box *box_halos,
		TreeResults **tr_res_out, float **ptl_radii_out)
{
	int n_ptl;
	float *ptl_radii, R200m_com, r_search_com;
	TreeResults *tr_res;

	/*
	 * Now, the search radius is fully determined by R200m and search factor.
	 */
	ptl_radii = NULL;
	tr_res = treeResultsInit();
	R200m_com = haloR200mComoving(halo, snap_idx);
	r_search_com = R200m_com * halo->r_search_factor;

	/*
	 * Check that the radius actually fits into the box. Note that the tree function would not fail
	 * if we used too large a radius, it will simply miss particles beyond the box. To avoid this,
	 * we check explicitly and reduce the radius if necessary. To avoid the complications of
	 * computing the maximum radius (such as periodicity), we simply try reducing the radius until
	 * it fits.
	 */
	halo->search_radius_decreased = 0;
	while (!sphereInBox(halo->cd.x, r_search_com, box_halos))
	{
		r_search_com *= 0.99;
		halo->search_radius_decreased = 1;

		/*
		 * For host halos, it would be pretty strange if the search radius was smaller than R200m.
		 * For subs and ghosts, the radius could have been strongly misestimated when we
		 * constructed the halo box.
		 */
		if ((r_search_com < R200m_com) && (isHost(halo)))
			error(__FFL__,
					"Search radius had to be reduced to within R200m (%.2f cMpc/h) for host halo ID %ld.\n",
					R200m_com, halo->cd.id);
	}
	if (halo->search_radius_decreased)
	{
		warningOrError(__FFL__, config.err_level_search_radius,
				"[%4d] [SN %3d] Had to reduce search radius for halo ID %ld (from %.2f to %.2f R200m).\n",
				proc, snap_idx, halo->cd.id, halo->r_search_factor, r_search_com / R200m_com);
	}
	treeFindSpherePeriodicCorrected(tree, tr_res, halo->cd.x, r_search_com);
	*tr_res_out = tr_res;
	halo->r_search_com_actual = r_search_com;

	/*
	 * There is one rare case in which we might find zero particles: if a halo is a phantom (i.e.
	 * interpolated between snapshots). If that is not the case and we find zero particles,
	 * something has definitely gone wrong.
	 */
	n_ptl = tr_res->num_points;
	if (n_ptl == 0)
	{
		if (halo->cd.phantom)
		{
			warningOrError(__FFL__, config.err_level_zero_ptl_phantom,
					"[%4d] [SN %3d] Found 0 particles within search radius for phantom subhalo ID %ld, status %d, xyz %.4f %.4f %.4f, r_search (com) %.4f, R200m_cat %.3f.\n",
					proc, snap_idx, halo->cd.id, halo->status, halo->cd.x[0], halo->cd.x[1],
					halo->cd.x[2], r_search_com, halo->cd.R200m_cat_com);
		} else
		{
			warningOrError(__FFL__, config.err_level_zero_ptl,
					"[%4d] [SN %3d] Found 0 particles within search radius for subhalo ID %ld, status %d, xyz %.4f %.4f %.4f, r_search (com) %.4f, R200m_cat %.3f.\n",
					proc, snap_idx, halo->cd.id, halo->status, halo->cd.x[0], halo->cd.x[1],
					halo->cd.x[2], r_search_com, halo->cd.R200m_cat_com);
		}
	}

	/*
	 * The mass profile may or may not need to be computed for subhalos.
	 */
	if (hasMassProfile(halo))
	{
		ptl_radii = (float*) memAlloc(__FFL__, MID_HALOLOGIC, sizeof(float) * n_ptl);
		particleListFromTreeResults(snap_idx, halo, tr_res, ptl_radii);
		*ptl_radii_out = ptl_radii;
	} else
	{
		*ptl_radii_out = NULL;
	}
}

/*
 * This function differs from those above in that it looks only for tracked particles. If we
 * cannot find at least some fraction of those particles, the function fails and returns a NULL
 * pointer to tr_res.
 */
#if DO_TRACER_PARTICLES

TreeResults* findHaloParticlesTracers(Halo *halo, int snap_idx, Tree *tree, Box *box_halos)
{
	int i, j, d, found, failed, n_particles, n_tcr, n_found, n_iter, is_ghost;
	float initial_x[3], current_x[3], current_search_rcom, found_fraction, n_found_inv;
	TreeResults *tr_res, *tr_res_compact;
	Particle *particle;
	Tracer temp_tcr, *tcr;
	int_least8_t *mask;

	/*
	 * We immediately cover one case: if there are zero tracer particles in the halo, clearly we
	 * will not find any here. If this halo is a ghost, that should never happen since a ghost
	 * cannot be defined without particles. However, if it is a subhalo, this is not critical
	 * altough strange.
	 */
	is_ghost = (isGhost(halo));
	n_tcr = halo->tt[PTL].tcr.n;

	if (n_tcr == 0)
	{
		if (is_ghost)
		{
			error(__FFL__, "Trying to find particles for ghost ID %ld with zero tracers.\n",
					halo->cd.id);
		} else
		{
			debugHalo(halo, "Not looking for tracers in subhalo as it has zero tracers.");
			return NULL;
		}
	}

	n_iter = 0;
	found = 0;
	failed = 0;
	n_found = 0;

	/*
	 * Note that the initial position is from the catalog for subhalos, and from the previous
	 * snapshot for ghosts. Thus, we allow the position to vary as we find more particles.
	 */
	for (d = 0; d < 3; d++)
	{
		initial_x[d] = halo->cd.x[d];
		current_x[d] = initial_x[d];
	}
	current_search_rcom = halo->r_search_com_initial;

#if CAREFUL
	if (current_search_rcom <= 0.0)
		error(__FFL__, "Cannot start with a zero search radius for tracers in halo ID %ld.\n",
				halo->cd.id);
#endif

	while (!found && !failed)
	{
		n_found = 0;
		tr_res = treeResultsInit();
		treeFindSpherePeriodicCorrected(tree, tr_res, current_x, current_search_rcom);
		n_particles = tr_res->num_points;
		mask = (int_least8_t*) memAlloc(__FFL__, MID_TREE, sizeof(int_least8_t) * n_particles);
		memset(mask, 0, sizeof(int_least8_t) * n_particles);

		for (d = 0; d < 3; d++)
			current_x[d] = 0.0;

		for (i = 0; i < n_particles; i++)
		{
			particle = tr_res->points[i];
			temp_tcr.id = particle->id;
			tcr = (Tracer*) bsearch(&temp_tcr, halo->tt[PTL].tcr.tcr, n_tcr, sizeof(Tracer),
					&compareTracers);
			if (tcr != NULL)
			{
				mask[i] = 1;
				n_found++;
				for (d = 0; d < 3; d++)
					current_x[d] += particle->x[d];
			}
		}

		/*
		 * If we found zero particles, we cannot guess a new position and go back to the original one.
		 */
		if (n_found > 0)
		{
			n_found_inv = 1.0 / (float) n_found;
			for (d = 0; d < 3; d++)
				current_x[d] *= n_found_inv;
		} else
		{
			for (d = 0; d < 3; d++)
				current_x[d] = initial_x[d];
		}

		/*
		 * Check whether we found enough tracers. If not, we try to increase the search radius; if
		 * that fails, we are presumably already at a very large search radius, and something has
		 * gone wrong. If this is a ghost, we cannot continue without a particle set, and we end
		 * the halo. If it is not a ghost, this is not critical (though strange).
		 */
		found_fraction = (float) n_found / (float) n_tcr;
		if (found_fraction > TRACER_FOUND_FRACTION_PASS)
		{
			found = 1;
		} else
		{
			current_search_rcom *= SEARCH_RADIUS_TRACER_INCREASE;
			if (current_search_rcom > config.box_size * SEARCH_RADIUS_TRACER_MAX_BOX_FRAC)
			{
				if (found_fraction > TRACER_FOUND_FRACTION_MIN)
				{
					found = 1;
				} else
				{
					failed = 1;
					if (is_ghost)
					{
						debugHalo(halo,
								"Ending ghost halo, despite large search radius found only %d ptl (%.1f of tracers, minimum %.1f).",
								n_found, found_fraction, TRACER_FOUND_FRACTION_MIN);
						endHalo(halo, snap_idx, snap_idx - 1, HALO_ENDED_GHOST_NOT_FOUND);
					} else
					{
						debugHalo(halo,
								"Stopped looking for tracers in subhalo, despite large search radius found only %d ptl (%.1f of tracers, minimum %.1f).",
								n_found, found_fraction, TRACER_FOUND_FRACTION_MIN);
					}
				}
			}
		}

		if (!found)
		{
			treeResultsFree(tr_res);
			tr_res = NULL;
			memFree(__FFL__, MID_TREE, mask, sizeof(int_least8_t) * n_particles);
		}

		/*
		 * Sanity check: if the iterations exceed a reasonable number, something must have gone
		 * wrong.
		 */
		n_iter++;
		if (n_iter > 100)
			error(__FFL__, "Could not find tracer particles after %d iterations (halo ID %ld).\n",
					n_iter, halo->cd.id);
	}

	if (failed)
	{
		return NULL;
	}

	/*
	 * Create compacted tree results with only the tracer particles. Note that we do not delete
	 * those tracers that were not found at this point; that will be done for all halos in the
	 * particle-connect function, where they will be marked as UNCONNECTED.
	 */
	tr_res_compact = treeResultsInit();
	treeResultsAlloc(tr_res_compact, n_found);
	j = 0;
	for (i = 0; i < n_particles; i++)
	{
		if (mask[i])
		{
			memcpy(tr_res_compact->points[j], tr_res->points[i], sizeof(TREE_TYPE));
			j++;
		}
	}
#if PARANOID
	if (j != n_found)
		error(__FFL__, "Error while compressing particle array (%d, %d)\n", j, n_found);
#endif
	treeResultsFree(tr_res);
	debugHalo(halo, "Found %d/%d tracer particles.", n_found, n_tcr);

	/*
	 * Cleanup
	 */
	memFree(__FFL__, MID_TREE, mask, sizeof(int_least8_t) * n_particles);

	return tr_res_compact;
}

#endif

void particleListFromTreeResults(int snap_idx, Halo *halo, TreeResults *tr_res, float *p_radii)
{
	int i;

	for (i = 0; i < tr_res->num_points; i++)
		p_radii[i] = tracerRadius(snap_idx, halo->cd.x, tr_res->points[i]->x);

	qsort(p_radii, tr_res->num_points, sizeof(float), &compareFloatsAscending);
}

/*
 * Determine an SO radius from the tracers in a halo (rather than all particles). This function
 * cannot be called with a NULL tracer array. The tracer radii are recomputed, meaning the tracer
 * trajectories do not need to be up to date when this function is called.
 *
 * Note that this function uses the brute-force version of the SO finder, as it is intended for
 * subhalos and ghosts where there are many ways for the iterative version to fail, and where the
 * particle numbers tend to be lower.
 */
int computeSOFromTracers(int snap_idx, Halo *halo, TreeResults *tr_res, float rho_threshold,
		float *R, float *M)
{
	int i, n_ptl, ret;
	float *p_radii;

	n_ptl = tr_res->num_points;
	p_radii = (float*) memAlloc(__FFL__, MID_TREEPOT, n_ptl * sizeof(float));
	for (i = 0; i < n_ptl; i++)
		p_radii[i] = tracerRadius(snap_idx, halo->cd.x, tr_res->points[i]->x);
	qsort(p_radii, n_ptl, sizeof(float), &compareFloatsAscending);
	ret = computeSOFromParticlesBruteForce(snap_idx, halo, p_radii, n_ptl, rho_threshold, R, M);
	memFree(__FFL__, MID_TREEPOT, p_radii, n_ptl * sizeof(float));

	return ret;
}

/*
 * Attempt to compute the radius of a subhalo from its tracers. However, the tracer positions do
 * not need to be updated, i.e., the tracer radii are recomputed based on the current halo center.
 *
 * The function can fail in a number of ways, in which case we need to make some decisions (e.g.,
 * if SO fails we fall back to catalog radius).
 */
void computeR200mFromSubhaloTracers(int snap_idx, Halo *halo, TreeResults *tr_res)
{
	int ret;
	float R200m, M200m;

	if (tr_res == NULL)
	{
		halo->history_R200m[snap_idx] = physicalRadius(halo->cd.R200m_cat_com, snap_idx);
		debugHalo(halo,
				"Radius finder: No tracers, reverted to catalog R200m %.2e kpc/h and mass %.2e Msun/h.",
				haloR200m(halo, snap_idx), haloM200m(halo, snap_idx));
		return;
	}

	ret = computeSOFromTracers(snap_idx, halo, tr_res, config.this_snap_rho_200m, &R200m, &M200m);

	if (ret == SO_EXIT_SUCCESS)
	{
		halo->history_R200m[snap_idx] = R200m;
		debugHalo(halo,
				"Radius finder: Computed radius from tracers, R200m %.2e kpc/h and mass %.2e Msun/h.",
				R200m, M200m);
	} else if (ret == SO_EXIT_DENSITY_LOW)
	{
		halo->history_R200m[snap_idx] = physicalRadius(halo->cd.R200m_cat_com, snap_idx);
		debugHalo(halo,
				"Radius finder: Density too low to find R200m from tracers, reverted to catalog R200m %.2e kpc/h and mass %.2e Msun/h.",
				haloR200m(halo, snap_idx), haloM200m(halo, snap_idx));
	} else if (ret == SO_EXIT_DENSITY_HIGH)
	{
		halo->history_R200m[snap_idx] = R200m;
		debugHalo(halo,
				"Radius finder: Density too high to find R200m from tracers, reverted to total mass %.2e Msun/h and R200m %.2e kpc/h.",
				M200m, R200m);
	} else
	{
		error(__FFL__, "Unknown return code from tracer SO function.\n");
	}
}

/*
 * Compare the value of R200m_all we have found to the catalog.
 */
void checkR200mAgainstCatalog(Halo *halo, int snap_idx)
{
	int N200m_cat;
	float R200m, R200m_cat_phys, M200m, M200m_cat;

	if (halo->cd.phantom)
		return;

	if (!isHost(halo))
		error(__FFL__, "Cannot compare against catalog R200m for non-host halos.\n");

	R200m_cat_phys = physicalRadius(halo->cd.R200m_cat_com, snap_idx);
	M200m_cat = soRtoM(R200m_cat_phys, config.this_snap_rho_200m);
	N200m_cat = (int) (M200m_cat / config.particle_mass);

	if (N200m_cat < CATALOG_COMPARISON_R200M_MIN_PTL)
		return;

	R200m = haloR200m(halo, snap_idx);
	M200m = soRtoM(R200m, config.this_snap_rho_200m);

	if (fabs(R200m / R200m_cat_phys - 1.0) > CATALOG_COMPARISON_R200M_MAX_R_DIFF)
	{
		warningOrError(__FFL__, config.err_level_cat_radius_diff,
				"[%4d] [SN %3d] Different radius from catalog, R200m SPARTA/cat is %.3f, M200m SPARTA/cat %.3f, N200m %d.\n",
				proc, snap_idx, halo->cd.id, R200m / R200m_cat_phys, M200m / M200m_cat,
				halo->cd.x[0], halo->cd.x[1], halo->cd.x[2], N200m_cat);
	}
}

/*
 * Check that we've found all particles that Rockstar found. We give one particle tolerance,
 * but more should not be missing; Note that it IS possible to find more particles, since
 * Rockstar (by default) uses unbinding. There are numerous reasons which can explain a
 * difference:
 *
 * - the halo is marked as a phantom, i.e. interpolated between snapshots
 * - the halo can be a subhalo
 * - the catalog R200m might have been guessed from Rvir, in which case it is unreliable
 *
 * This function outputs quite a few warnings when it is activated due to floating point errors
 * that can lead to different search coordinates in Rockstar and SPARTA. This can happen when halos
 * reach across domain boundaries and the tree search has to be repeated with the box size added or
 * subtracted. For example, if R200m < 1E-4 * Lbox, a floating point error of 1E-6 can lead to
 * the equivalent of a 1% error in R200m. Thus, a tolerance of 1E-6 * Lbox is added below.
 *
 * Note that this function does NOT check the R200m calculation in SPARTA! It just compares the
 * particles inside the Rockstar radius to the mass reported by Rockstar.
 */
void checkN200mAgainstCatalog(Halo *halo, int snap_idx, float *p_radii, int n_particles)
{
	int i, N200m, N200m_cat;
	float R200m, M200m_cat, R200m_cat_phys;

	if (halo->cd.phantom)
		return;

	if (!isHost(halo))
		error(__FFL__, "Cannot compare against catalog particle number for non-host halos.\n");

	R200m_cat_phys = physicalRadius(halo->cd.R200m_cat_com, snap_idx);
	R200m = haloR200m(halo, snap_idx);

	if (fabs(R200m / R200m_cat_phys - 1.0) > CATALOG_COMPARISON_N200M_MAX_R_DIFF)
		return;

	M200m_cat = soRtoM(R200m_cat_phys, config.this_snap_rho_200m);
	N200m_cat = (int) (M200m_cat / config.particle_mass);
	N200m = 0;
	for (i = 0; i < n_particles; i++)
	{
		if (p_radii[i] <= (R200m_cat_phys + 1E-6 * config.box_size))
			N200m++;
	}

	if (N200m_cat - N200m > CATALOG_COMPARISON_N200M_MIN_PTL)
	{
		warningOrError(__FFL__, config.err_level_missing_halo_ptl,
				"[%4d] [SN %3d] Missing particles in halo ID %10ld, N200m != N200m_cat (%4d %4d, diff %4d), R200m/cat %.3f, M200m/cat %.3f, x [%.6f %.6f %.6f], R200mcat %.6f.\n",
				proc, snap_idx, halo->cd.id, N200m, N200m_cat, N200m - N200m_cat,
				R200m / R200m_cat_phys, (float) N200m / M200m_cat * config.particle_mass,
				halo->cd.x[0], halo->cd.x[1], halo->cd.x[2], halo->cd.R200m_cat_com);
	}
}

#if DO_MASS_PROFILE
/*
 * The mass profile is stored for the last STCL_TCR snapshots, going back in time so that profile
 * 0 is the one for the current snapshot. The profile is computed simply as the number of particles
 * contained within each mass bin, and saved in units of log10(M(r)). The interpolation
 * happens in log10(r/R200m)-t space.
 *
 * At the high-radius end, the bins are simply filled with the same mass which is NOT physically
 * correct. The mass profile should never be evaluated at radii larger than the search radius.
 */
void computeMassProfile(Halo *halo, int snap_idx, float *p_radii, int n_particles)
{
	int i, j, k, offset;
	float conv_factor, R200m;

	/*
	 * Move the old time bins, and compute the offset to the last time bin for convenience
	 */
	R200m = haloR200m(halo, snap_idx);
	for (j = 0; j < STCL_TCR; j++)
		mass_profile_vars.grid_R200m[j] = (double) halo->history_R200m[snap_idx - STCL_TCR + j + 1];
	for (j = 0; j < STCL_TCR - 1; j++)
		for (i = 0; i < N_MBINS; i++)
			halo->grid_M[j * N_MBINS + i] = halo->grid_M[(j + 1) * N_MBINS + i];
	offset = (STCL_TCR - 1) * N_MBINS;

	/*
	 * Go through the particle array until all bins are filled.
	 */
	i = 0;
	k = 0;
	while (k < n_particles)
	{
		while ((i < N_MBINS) && (p_radii[k] > config.r_bins_lin[i] * R200m))
		{
			halo->grid_M[offset + i] = (float) k;
			i++;
		}
		k++;
	}
	while (i < N_MBINS)
	{
		halo->grid_M[offset + i] = (float) k;
		i++;
	}

	/*
	 * Convert to log10(M), and set a negative constant for invalid bins
	 */
	conv_factor = config.particle_mass;
	for (i = 0; i < N_MBINS; i++)
	{
		if (halo->grid_M[offset + i] > 0.0)
		{
			halo->grid_M[offset + i] = log10(halo->grid_M[offset + i] * conv_factor);
		} else
		{
			halo->grid_M[offset + i] = LOG10_NO_MASS;
		}
	}

#if CAREFUL
	for (i = 0; i < N_MBINS - 1; i++)
	{
		if (halo->grid_M[offset + i + 1] < halo->grid_M[offset + i])
		{
			output(2, "[%4d]  R200m %.4e\n", proc, R200m);
			for (i = 0; i < n_particles; i++)
				output(2, "[%4d] Particle %4d radius %.4e\n", proc, i, p_radii[i]);
			for (i = 0; i < N_MBINS; i++)
				output(2, "[%4d] Mass bin %4d radius %.4e mass %.4e\n", proc, i,
						config.r_bins_lin[i] * R200m, halo->grid_M[offset + i]);
			error(__FFL__, "MASS BINS DECREASING\n");
		}
	}
#endif

	/*
	 * Initialize the interpolator.
	 *
	 * If we have not filled the first STCL_TCR time bins, there will be undefined values in the
	 * mass profile. Moreover, there should be no results for which we need to interpolate M
	 * anyway.
	 */
	if (snap_idx < STCL_TCR - 1)
		return;

	/*
	 * Set up the R200m interpolation
	 */
	gsl_spline_init(gsl_mass_profile.r200m_spline, mass_profile_vars.grid_t,
			mass_profile_vars.grid_R200m, STCL_TCR);

	/*
	 * Set up the mass interpolation
	 */
	for (i = 0; i < N_MBINS * STCL_TCR; i++)
		mass_profile_vars.grid_M[i] = (double) halo->grid_M[i];
	gsl_spline2d_init(gsl_mass_profile.spline_mass, config.r_bins_log, mass_profile_vars.grid_t,
			mass_profile_vars.grid_M,
			N_MBINS, STCL_TCR);

	/*
	 * Indicate that the mass profile has been successfully initialized.
	 */
	mass_profile_vars.initialized = 1;
}

/*
 * Interpolate in the r-t plane to find the mass enclosed by a certain radius at a certain time.
 */
float enclosedMass(Halo *halo, int snap_idx, float r, float t)
{
	double M;
	float R200m_t, log_r_R200m;

#if CAREFUL
	if (snap_idx < STCL_TCR - 1)
		error(__FFL__, "Trying to interpolate mass before STCL_TCR snaps.\n");

	if ((t < mass_profile_vars.grid_t[0]) || (t > mass_profile_vars.grid_t[STCL_TCR - 1]))
	{
		int i;
		for (i = 0; i < STCL_TCR; i++)
			output(0, "t grid %d %.5f\n", i, mass_profile_vars.grid_t[i]);
		error(__FFL__, "Trying to interpolate t = %.5f, not in grid [%.5f .. %.5f].\n", t,
				mass_profile_vars.grid_t[0], mass_profile_vars.grid_t[STCL_TCR - 1]);
	}
#endif

	/*
	 * Get enclosed mass at t, R200m(t)
	 */
	if (!mass_profile_vars.initialized)
		error(__FFL__,
				"Tried to interpolate uninitialized mass profile, halo orig ID %ld, status %d.\n",
				haloOriginalID(halo), halo->status);

	R200m_t = gsl_spline_eval(gsl_mass_profile.r200m_spline, t, gsl_mass_profile.r200m_acc);
	log_r_R200m = log10(r / R200m_t);

	if ((log_r_R200m <= config.r_bins_log[0]) || (log_r_R200m >= config.r_bins_log[N_MBINS - 1]))
	{
		M = INVALID_M;
	} else
	{
		if ((log_r_R200m <= config.r_bins_log[0])
				|| (log_r_R200m >= config.r_bins_log[N_MBINS - 1]))
		{
			error(__FFL__,
					"[%4d] Trying to interpolate log r / R200m = %.5f, not in grid [%.5f .. %.5f].\n",
					proc, log_r_R200m, config.r_bins_log[0], config.r_bins_log[N_MBINS - 1]);
		}
		M = gsl_spline2d_eval(gsl_mass_profile.spline_mass, log_r_R200m, t,
				gsl_mass_profile.acc_mass_r, gsl_mass_profile.acc_mass_t);
		M = pow(10.0, M);
	}

	return (float) M;
}
#endif
