/*************************************************************************************************
 *
 * This unit implements spherical overdensity routines.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _HALO_SO_H_
#define _HALO_SO_H_

#include "../global_types.h"
#include "../cosmology.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Possible exit codes of the SO functions. If density is low, that means that the density
 * threshold was not reached anywhere, and the returned mass is zero. If the density was high,
 * that means that the threshold was exceeded even at the largest radius, and that the total mass
 * of the particles was set.
 */
#define SO_EXIT_SUCCESS 0
#define SO_EXIT_DENSITY_LOW 1
#define SO_EXIT_DENSITY_HIGH 2

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

/*
 * GSL constructs such as root finders need to be initialized which should only be done once.
 * Thus, some of these constructs are stored in a collective global variable.
 */
typedef struct
{
	long int halo_id;
	int n_p;
	float rho_threshold;
	float *p_radii;
} RootFinderParamsSO;

/*
 * There are two separate interpolators here: one for R200m(t) and one for M(R/R200m(t), t). The
 * GSL vars are kept in a separate structure because they cannot be loaded during a restart, they
 * must be initialized.
 */
typedef struct
{
	gsl_root_fsolver *so_solver;
	gsl_function so_function;
	RootFinderParamsSO so_params;
} GSLVarsSO;

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

extern GSLVarsSO gsl_so;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initGslSO();
void endGslSO();

// General SO functions
float deltaVirialCritical(Cosmology *cosmo, float z);
float deltaVirialBackground(Cosmology *cosmo, float z);
float soRtoM(float R, float density_threshold);
float soMtoR(float M, float density_threshold);

// SO from particles
void radiusMassFromList(double pos, float *radii, int n_radii, float *M, float *R, float *rho,
		long int halo_id);
double equationSO(double x, void *params);
void outputSOError(int snap_idx, float *p_radii, int n_p, float M200m_guess, float x_guess,
		const char *message);
int computeSOFromParticles(int snap_idx, Halo *halo, float *p_radii, int n_p, float M_guess,
		float rho_threshold, float *R, float *M);
int computeSOFromParticlesBruteForce(int snap_idx, Halo *halo, float *p_radii, int n_p,
		float rho_threshold, float *R, float *M);

#endif
