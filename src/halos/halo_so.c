/*************************************************************************************************
 *
 * This unit implements spherical overdensity routines.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <gsl/gsl_errno.h>

#include "halo_so.h"
#include "halo.h"
#include "../constants.h"
#include "../config.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

#define RADIUS_FINDER_MAX_ITER 40
#define RADIUS_FINDER_FIRST_STEP_DN 0.8
#define RADIUS_FINDER_FIRST_STEP_UP 1.2
#define RADIUS_FINDER_FIRST_MINDIST 0.65
#define RADIUS_FINDER_STEP_DN 0.25
#define RADIUS_FINDER_STEP_UP 4.0

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

GSLVarsSO gsl_so;

/*************************************************************************************************
 * FUNCTIONS - GENERAL SPHERICAL OVERDENSITY
 *************************************************************************************************/

void initGslSO()
{
	gsl_so.so_solver = gsl_root_fsolver_alloc(gsl_root_fsolver_brent);
	gsl_so.so_function.function = &equationSO;
	gsl_so.so_function.params = &gsl_so.so_params;
}

void endGslSO()
{
	gsl_root_fsolver_free(gsl_so.so_solver);
}

/*
 * This function returns the Bryan & Norman 1998 fitting formula for the
 * virial overdensity of halos which is a function of redshift. See equation
 * 6 of their paper. Note that this returns Delta_crit, not Delta_bg.
 */
float deltaVirialCritical(Cosmology *cosmo, float z)
{
	float delta, x;

	x = OmegaM(cosmo, z) - 1.0;
	delta = 18.0 * pow(MATH_pi, 2) + 82.0 * x - 39.0 * x * x;

	return delta;
}

/*
 * Same as above, but for Delta wrt background density.
 */
float deltaVirialBackground(Cosmology *cosmo, float z)
{
	return deltaVirialCritical(cosmo, z) / OmegaM(cosmo, z);
}

/*
 * Compute the mass from a physical radius (in kpc/h, as long as the density threshold is in the
 * usual units).
 */
float soRtoM(float R, float density_threshold)
{
	return 4.0 * MATH_pi / 3.0 * R * R * R * density_threshold;
}

/*
 * Compute the physical radius in kpc/h.
 */
float soMtoR(float M, float density_threshold)
{
	return pow(3.0 * M / (4.0 * MATH_pi * density_threshold), 1.0 / 3.0);
}

/*************************************************************************************************
 * FUNCTIONS - CALCULATING SO FROM PARTICLE LISTS
 *************************************************************************************************/

/*
 * Compute the mass, radius, and density from a position in an ordered particle list. The values
 * of M(R) are computed by interpolating between the points (N_i - 0.5) at r_i, where N_i = i + 1
 * if i is the array index. This means that the density at the center reaches arbitrarily high
 * values, since if pos < 0.5, i.e. the position of a mass element with less than half a particle
 * mass, is such that the density becomes
 *
 * rho(x < 0.5) = (3 mp) / (32 pi r[0]^3 x^2)
 *
 * This behavior is desirable as it ensures that any spherical overdensity radius can be computed,
 * at least at the lower end of the distribution. While such a radius is not very physical, we
 * do not care much since halos with very small numbers of particles are of little interest anyway.
 *
 * Another weird case occurs when the innermost radius is zero. Then, the density within 0.5
 * positions is infinite. Instead, we set the radius to 1pc/h, which will lead to enormous
 * densities and thus push the radius out past half a position. Since such small measurements are
 * garbage anyway, this should never matter.
 */
void radiusMassFromList(double pos, float *radii, int n_radii, float *M, float *R, float *rho,
		long int halo_id)
{
	int il;
	double R_, Rr, Rl, Nl, Nr;

	if (pos < 0.5)
	{
		Rl = 0.0;
		if (radii[0] <= 0.0)
		{
			Rr = 1E-3;
		} else
		{
			Rr = radii[0];
		}
		Nl = 0.0;
		Nr = 0.5;
	} else
	{
		il = (int) floor(pos - 0.5);
		Rl = radii[il];
		Rr = radii[il + 1];
		Nl = (double) il + 0.5;
		Nr = Nl + 1.0;
	}

	*M = (float) (pos * config.particle_mass);
	R_ = Rl + (pos - Nl) * (Rr - Rl) / (Nr - Nl);
#if CAREFUL
	if (R_ <= 0.0)
	{
		int i;
		for (i = 0; i < n_radii; i++)
		{
			output(0, "  Radius %3d %.3f\n", i, radii[i]);
		}
		error(__FFL__, "Found R = 0 (pos = %.4f) in halo ID %ld.\n", pos, halo_id);
	}
	if (isnan(R_))
		error(__FFL__,
				"Found NAN for R; pos = %.3f, Rl %.3e, Rr %.3e, Nl %.3e, Nr %.3e, Nr - Nl %.3e\n",
				pos, Rl, Rr, Nl, Nr, Nr - Nl);
#endif
	*R = (float) R_;
	*rho = 3.0 * (*M) / (4.0 * MATH_pi * R_ * R_ * R_);
}

double equationSO(double x, void *params)
{
	float rho, M, R;
	double diff;
	RootFinderParamsSO *p;

	p = (RootFinderParamsSO*) params;

	radiusMassFromList(x, p->p_radii, p->n_p, &M, &R, &rho, p->halo_id);
#if PARANOID
	if (isinf(rho))
	{
		int i;
		for (i = 0; i < p->n_p; i++)
			output(0, "  Particle %4d radius %.2f\n", i, p->p_radii[i]);
		error(__FFL__, "SO function returned infinite density, x = %.2e, in halo ID %ld.\n", x,
				p->halo_id);
	}
	if (isnan(rho))
		error(__FFL__, "SO function returned nan density in halo ID %ld.\n", p->halo_id);
#endif
	diff = (double) (rho - p->rho_threshold);

	return diff;
}

void outputSOError(int snap_idx, float *p_radii, int n_p, float M_guess, float x_guess,
		const char *message)
{
	int i;
	float rho, M, R, R_guess, rho_threshold;

	for (i = 0; i < n_p; i++)
	{
		M = (i + 1) * config.particle_mass;
		R = p_radii[i];
		rho = 3.0 * (M) / (4.0 * MATH_pi * R * R * R);

		output(0, "Particle %4d, radius %.3e, density %.3e\n", i, p_radii[i], rho);
		if (i > 50)
			break;
	}

	rho_threshold = gsl_so.so_params.rho_threshold;
	R_guess = soMtoR(M_guess, rho_threshold);

	output(0, "[%4d] n_p = %d, rho = %.2e, R_guess = %.2f, M_guess = %.2e, x_guess = %.5e\n", proc,
			n_p, rho_threshold, R_guess, M_guess, x_guess);
	error(__FFL__, message);
}

/*
 * Compute R200m given an ordered list of particle radii (in physical kpc/h).
 */
int computeSOFromParticles(int snap_idx, Halo *halo, float *p_radii, int n_p, float M_guess,
		float rho_threshold, float *R, float *M)
{
	int i, status;
	double x, min_x, max_x, max_x_abs;
	float rho, x_guess;

#if CAREFUL
	if (n_p < 1)
		error(__FFL__, "Cannot compute radii for zero particles.\n");
	if (M_guess <= 0.0)
		error(__FFL__, "Invalid guess mass, M_guess = %.2e, in halo ID %ld.\n", M_guess,
				halo->cd.id);
#endif

#if PARANOID
	/*
	 * Check that particle list is ordered. It's better to throw an error here where we know the
	 * halo ID and can print meaningful debug.
	 */
	for (i = 1; i > n_p; i++)
	{
		if (p_radii[i - 1] > p_radii[i])
		{
			int j;

			for (j = 0; j < n_p; j++)
				output(0, "  Particle %4d radius %.2f\n", j, p_radii[j]);
			error(__FFL__, "Particle list for halo ID %ld was not ordered (see above).\n",
					halo->cd.id);
		}
	}
#endif

	gsl_root_fsolver *solver = gsl_so.so_solver;
	gsl_so.so_params.n_p = n_p;
	gsl_so.so_params.p_radii = p_radii;
	gsl_so.so_params.halo_id = halo->cd.id;
	gsl_so.so_params.rho_threshold = rho_threshold;

	/*
	 * Find a min and max radius where to start the solver. In some cases, the M200m guess is 0
	 * because the halo finder could not identify R200m. In that case, we start at a very small
	 * mass coordinate x and the largest allowed x.
	 */
	if (n_p > 1)
	{
		max_x_abs = (double) (n_p - 1.0);
	} else
	{
		max_x_abs = 0.5;
	}
	if (M_guess < config.particle_mass)
	{
		x_guess = -1.0;
		min_x = 0.1;
		max_x = max_x_abs;
	} else
	{
		x_guess = M_guess / config.particle_mass;
		max_x = fminf(RADIUS_FINDER_FIRST_STEP_UP * x_guess, max_x_abs);
		min_x = fminf(RADIUS_FINDER_FIRST_STEP_DN * x_guess, max_x * RADIUS_FINDER_FIRST_MINDIST);
	}

	/*
	 * Either way, we test both the min and max before feeding them to the solver, and iteratively
	 * increase / decrease them if necessary. For the min, this procedure should never fail.
	 */
	i = 0;
	do
	{
		radiusMassFromList(min_x, p_radii, n_p, M, R, &rho, halo->cd.id);
		if (rho > rho_threshold)
			break;
		min_x *= RADIUS_FINDER_STEP_DN;
		i++;
	} while (min_x > 1E-20);

	if (rho < rho_threshold)
	{
		output(0,
				"Failed to find a minimum radius for searching for R (i %d, n_p = %d, rho_threshold = %.2e, M_guess = %.2e, x_guess = %.5e, min_x = %.2e, rho = %.2e).\n",
				i, n_p, rho_threshold, M_guess, x_guess, min_x, rho);
		outputSOError(snap_idx, p_radii, n_p, M_guess, x_guess, "See error message above.\n");
	}

	/*
	 * For the max, we can only go to the maximum mass. This procedure can fail is if the density
	 * at the maximum x is still larger than rho200m - in that case we have to abort.
	 */
	i = 0;
	do
	{
		radiusMassFromList(max_x, p_radii, n_p, M, R, &rho, halo->cd.id);
		if (rho < rho_threshold)
		{
			break;
		} else
		{
			if (max_x >= max_x_abs)
			{
				/*
				 * If this happens, the density is still too high at the edge of the particle
				 * distribution. For normal halos, we treat that as a failure, but for ghosts
				 * we simply assign them the total mass.
				 */
				if (!halo->cd.phantom)
					warningOrError(__FFL__, config.err_level_radius_not_found,
							"Could not find R for non-phantom halo ID %ld; density above threshold at largest radius (rho_threshold %.2e, N_guess %.0f).\n",
							halo->cd.id, rho_threshold, x_guess);

				*M = n_p * config.particle_mass;
				*R = soMtoR(*M, rho_threshold);

				debugHalo(halo,
						"Stopping SO routine because density threshold is too high at edge of particles; assigning total particle mass %.2e Msun/h, radius %.2e kpc/h.",
						*M, *R);

				return SO_EXIT_DENSITY_HIGH;
			}
		}
		max_x = fminf(RADIUS_FINDER_STEP_UP * max_x, max_x_abs);
		i++;
	} while (1);

	if (min_x >= max_x)
	{
		outputSOError(snap_idx, p_radii, n_p, M_guess, x_guess, "min_x > max_x.\n");
	}

	/*
	 * Now we are ready to run the solver
	 */
	gsl_root_fsolver_set(solver, &(gsl_so.so_function), min_x, max_x);
	i = 0;
	do
	{
		gsl_root_fsolver_iterate(solver);
		x = gsl_root_fsolver_root(solver);
		min_x = gsl_root_fsolver_x_lower(solver);
		max_x = gsl_root_fsolver_x_upper(solver);
		status = gsl_root_test_interval(min_x, max_x, 0.0, 1E-4);
		i++;
	} while (status == GSL_CONTINUE && i < RADIUS_FINDER_MAX_ITER);

	if (status != GSL_SUCCESS)
		outputSOError(snap_idx, p_radii, n_p, M_guess, x_guess, "Failed to converge to R/M.\n");

	radiusMassFromList(x, p_radii, n_p, M, R, &rho, halo->cd.id);

	debugHalo(halo,
			"Computed SO radius R = %.2e kpc/h, M = %.2e Msun/h for threshold %.3e, M_guess %.2e.",
			*R, *M, rho_threshold, M_guess);

	return SO_EXIT_SUCCESS;
}

/*
 * A simple routine that goes through the particles starting from the largest radius in order to
 * catch the largest radius where the SO criterion is fulfilled.
 */
int computeSOFromParticlesBruteForce(int snap_idx, Halo *halo, float *p_radii, int n_p,
		float rho_threshold, float *R, float *M)
{
	int i;
	float M_tmp, R_tmp, rho;

	for (i = n_p - 1; i > -1; i--)
	{
		M_tmp = (i + 1) * config.particle_mass;
		R_tmp = fmax(p_radii[i], config.this_snap_force_res);
		rho = 3.0 * M_tmp / (4.0 * MATH_pi * R_tmp * R_tmp * R_tmp);
		if (rho > rho_threshold)
		{
			if (i == n_p - 1)
			{
				/*
				 * The profile never reaches rho200m; for ghosts, this is somewhat expected and
				 * we simply assign M as M200m.
				 */
				*M = M_tmp;
				*R = soMtoR(*M, rho_threshold);

				debugHalo(halo,
						"Stopping SO routine because density threshold is too high at edge of particles; assigning total particle mass %.2e Msun/h, radius %.2e kpc/h.",
						*M, *R);

				return SO_EXIT_DENSITY_HIGH;
			} else
			{
				*M = M_tmp;
				*R = soMtoR(*M, rho_threshold);

				return SO_EXIT_SUCCESS;
			}
		} else if (i == 0)
		{
			if (!halo->cd.phantom)
				warningOrError(__FFL__, config.err_level_radius_not_found,
						"Could not find R for non-phantom halo ID %ld; density below threshold at smallest radius (r %.2f kpc/h, rho %.2e, rho_threshold %.2e).\n",
						halo->cd.id, R_tmp, rho, rho_threshold);
			debugHalo(halo,
					"Stopping SO routine because density threshold is too low at center; assigning zero radius and mass.",
					*M, *R);
			*R = 0.0;
			*M = 0.0;

			return SO_EXIT_DENSITY_LOW;
		}
	}

	return SO_EXIT_SUCCESS;
}

