/*************************************************************************************************
 *
 * This unit implements routines to find and analyze the particle distribution around halos.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _HALO_PARTICLES_H_
#define _HALO_PARTICLES_H_

#include <gsl/gsl_spline.h>
#include <gsl/gsl_spline2d.h>

#include "../global_types.h"
#include "../tree.h"
#include "../geometry.h"

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct
{
	int_least8_t initialized;
	double grid_t[STCL_TCR];
	double grid_R200m[STCL_TCR];
	double grid_M[(N_MBINS * STCL_TCR)];
} MassProfileVars;

typedef struct
{
	gsl_spline *r200m_spline;
	gsl_interp_accel *r200m_acc;

	gsl_spline2d *spline_mass;
	gsl_interp_accel *acc_mass_r;
	gsl_interp_accel *acc_mass_t;
} GSLVarsMassProfile;

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

extern GSLVarsMassProfile gsl_mass_profile;

#if DO_MASS_PROFILE
extern MassProfileVars mass_profile_vars;
#endif

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initGslMassProfile();
void endGslMassProfile();
void initMassProfileVars();

void setSearchRadius(Halo *halo, int snap_idx);

void findRadiusHost(Halo *halo, int snap_idx, Tree *tree, Box *box_halos);
void findHaloParticles(Halo *halo, int snap_idx, Tree *tree, Box *box_halos,
		TreeResults **tr_res_out, float **ptl_radii_out);
#if DO_TRACER_PARTICLES
TreeResults* findHaloParticlesTracers(Halo *halo, int snap_idx, Tree *tree, Box *box_halos);
#endif

void particleListFromTreeResults(int snap_idx, Halo *halo, TreeResults *tr_res, float *p_radii);

int computeSOFromTracers(int snap_idx, Halo *halo, TreeResults *tr_res, float rho_threshold,
		float *R, float *M);
void computeR200mFromSubhaloTracers(int snap_idx, Halo *halo, TreeResults *tr_res);

void computeMassProfile(Halo *halo, int snap_idx, float *p_radii, int n_particles);
float enclosedMass(Halo *halo, int snap_idx, float r, float t);

#endif
