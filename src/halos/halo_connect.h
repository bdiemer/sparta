/*************************************************************************************************
 *
 * This unit connects halos between snapshots.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _HALO_CONNECT_H_
#define _HALO_CONNECT_H_

#include "../global_types.h"
#include "../statistics.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void connectHalos(int snap_idx, DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs);

#endif
