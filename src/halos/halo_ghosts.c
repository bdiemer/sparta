/*************************************************************************************************
 *
 * This unit implements ghosts, halos that are not present in the halo finder catalogs.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>
#include <ctype.h>
#include <string.h>
#include <sys/stat.h>

#include "halo_ghosts.h"
#include "halo.h"
#include "halo_so.h"
#include "halo_definitions.h"
#include "halo_particles.h"
#include "../config.h"
#include "../memory.h"
#include "../geometry.h"
#include "../utils.h"
#include "../tree_potential.h"
#include "../tracers/tracers.h"
#include "../io/io_hdf5.h"

#if DO_GHOSTS

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Calculate the uncertainty from the actual dispersion of particle positions?
 */
#define GHOST_RADIUS_SIGMA_EXACT 0

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct GhostParticleSorter
{
	float x[3];
	float pe;
} GhostParticleSorter;

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void determineGhostX(int snap_idx, Halo *halo, TreeResults *tr_res, float *e_pot);
void determineGhostV(int snap_idx, Halo *halo, TreeResults *tr_res);
void computeCenterOfMass(TreeResults *tr_res, float *x_com, float *v_com);
void computePotentialEnergy(int snap_idx, TreeResults *tr_res, float *e_pot);
float ghostRadiusUncertainty(Halo *halo, int snap_idx, TreeResults *tr_res, float R200m);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Create a unique ID for a ghost halo. This is safe as long as the normal halo catalog
 * IDs stay below some very large number such as 1E16.
 */
ID createGhostID(Halo *halo, int snap_idx)
{
	return (ID) GHOST_ID_OFFSET + snap_idx * ((ID) GHOST_ID_SNAP_OFFSET) + haloOriginalID(halo);
}

/*
 * Ghosts are created when a halo ends. Though this almost always happens to subhalos, this
 * function can, in principle, also handle the case of a host ghost.
 *
 * This function returns 0 if successful and 1 if not, in which case the halo needs to be treated
 * other than being converted into a ghost.
 */
int makeHaloGhost(DynArray *halos, Halo *halo, int snap_idx)
{
	int tt;

	/*
	 * First check that this function wasn't called in error
	 */
	if (isGhost(halo))
		error(__FFL__,
				"Calling ghost conversion function on halo %ld that is already ghost, status %d.\n",
				halo->cd.id, halo->status);

	/*
	 * Another check: if the halo already does not have enough tracers to be a ghost, there is
	 * no point in creating one.
	 */
	if (halo->tt[PTL].tcr.n < config.ghost_min_n200m)
	{
		debugHalo(halo,
				"Tried to convert halo to ghost, but not enough particle tracers (%d, min %d).",
				halo->tt[PTL].tcr.n, config.ghost_min_n200m);

		return 1;
	}

	SubPointer *sh, temp_sh;
	Halo *host;

	/*
	 * If a subhalo is merging into a larger halo, the descendant ID should be that halo's ID,
	 * in which case we are dealing with a sub ghost. If there is no descendant, this halo would
	 * have just vanished into thin air, which is a strange scenario.
	 */
	if (halo->cd.desc_id == -1)
	{
		halo->status = HALO_STATUS_GHOST_HOST;
	} else
	{
		halo->status = HALO_STATUS_GHOST_SUB;
	}

	/*
	 * Basic ghost changes: the ID will be changed to a ghost ID in the next snapshot, which we
	 * prepare here. And this halo is now the most massive progenitor of its ghost descendant,
	 * unlike in the case where it merges into a larger halo.
	 *
	 * Note that a ghost cannot be a phantom since it's unknown to the halo catalog, and since a
	 * phantom is reconstructed between two existing halos.
	 */
	halo->cd.desc_id = createGhostID(halo, snap_idx + 1);
	halo->cd.mmp = 1;
	halo->cd.phantom = 0;

	/*
	 * We do not yet set the status of the ghost, that will be done in a separate function that is
	 * called right after this one. However, we do need to change the descendant ID in the
	 * sub-pointer, otherwise search functions will fail in the next snapshot.
	 */
	if (halo->host_idx != -1)
	{
		host = &(halos->h[halo->host_idx]);

		temp_sh.id = halo->cd.id;
		sh = (SubPointer*) bsearch(&temp_sh, host->subs.sp, host->subs.n, sizeof(SubPointer),
				&compareSubhalos);
		if (sh == NULL)
			error(__FFL__, "Could not find subpointer for halo %ld in host %ld (becoming ghost).\n",
					host->cd.id, halo->cd.id);
		sh->desc_id = halo->cd.desc_id;

		debugHalo(halo, "Halo ended in catalog, converted to ghost (%d ptl tracers, host %ld).",
				halo->tt[PTL].tcr.n, host->cd.id);
	} else
	{
		halo->cd.desc_pid = -1;

		debugHalo(halo, "Halo ended in catalog, converted to ghost (%d ptl tracers), no host.",
				halo->tt[PTL].tcr.n);
	}

	/*
	 * Set the status of tracers in this halo to the instruction for ghosts
	 */
	setTracerInstruction(halo, INSTR_GHOST);

	/*
	 * Remove all ignore lists from the ghost. Since we are not adding new tracers, there is no
	 * need for them (and they are never stored to output files).
	 */
	for (tt = 0; tt < NTT; tt++)
	{
		freeDynArray(__FFL__, &(halo->tt[tt].iid));
	}

	return 0;
}

/*
 * Setting the ghost descendant is a little tricky as it depends on the future of its current host.
 * Depending on whether the host is alive in the next snapshot, a host or subhalo then, or even a
 * ghost itself, we either stay with the same host or switch to another one.
 */
void setHaloStatusGhost(int snap_idx, Halo *ghost, Halo *host)
{
	if ((host->cd.desc_id == -1) && (snap_idx != config.n_snaps - 1))
		error(__FFL__, "Host of ghost %ld is dying without descendant.\n", ghost->cd.id);

	/*
	 * Special case: if the host has died for some weird reason such as a halo catalog jump, we need
	 * to also end the ghost.
	 */
	if (!isAlive(host) && (host->status != HALO_ENDED_MERGED)
			&& (host->status != HALO_ENDED_LAST_SNAP))
	{
		debugHalo(ghost, "Host of ghost ended with status %d, ending ghost as well.", host->status);
		endHalo(ghost, snap_idx, snap_idx, HALO_ENDED_HOST_ENDED);
		return;
	}

	if (host->cd.desc_pid == -1)
	{
		ghost->cd.desc_pid = host->cd.desc_id;
		if (host->cd.mmp)
		{
			debugHalo(ghost,
					"Host of ghost is staying host, set desc_pid to host's descendant %ld.",
					ghost->cd.desc_pid);
		} else
		{
			ghost->status = HALO_STATUS_GHOST_SWITCHING;
			debugHalo(ghost, "Host of ghost is merging, set desc_pid to host's descendant %ld.",
					ghost->cd.desc_pid);
		}
	} else
	{
		if (host->cd.mmp)
		{
			if (isGhost(host))
			{
				ghost->status = HALO_STATUS_GHOST_SWITCHING;
				ghost->cd.desc_pid = host->cd.desc_pid;
				debugHalo(ghost,
						"Host of ghost is becoming ghost itself, set desc_pid to hosts's desc_pid %ld.",
						ghost->cd.desc_pid);
			} else
			{
				ghost->cd.desc_pid = host->cd.desc_id;
				debugHalo(ghost,
						"Host of ghost is becoming sub, set desc_pid to hosts's descendant ID %ld.",
						ghost->cd.desc_pid);
			}
		} else
		{
			ghost->status = HALO_STATUS_GHOST_SWITCHING;
			ghost->cd.desc_pid = host->cd.desc_id;
			debugHalo(ghost, "Host of ghost is merging, set desc_pid to descendant %ld.",
					ghost->cd.desc_pid);
		}
	}
}

/*
 * After finding the new ghost particles, we need to see if they still form a useful unit with
 * defined properties. This function tries to find a ghost position, velocity, and radius. If any
 * of them fail, we abort the ghost.
 */
void determineGhostProperties(Halo *halo, Halo *host, int snap_idx, TreeResults *tr_res)
{
	int s, n_ptl, has_merged, ret;
	float *e_pot, R200m, M200m, N200m, R200m_host_com, delta_x, R200m_sub_phys, N200m_sub, sigma_x,
			delta_x_ratio_err, delta_t_dyn;

	/*
	 * Compute ghost center.
	 */
	n_ptl = tr_res->num_points;
	e_pot = (float*) memAlloc(__FFL__, MID_GHOSTS, n_ptl * sizeof(float));
	computePotentialEnergy(snap_idx, tr_res, e_pot);
	determineGhostX(snap_idx, halo, tr_res, e_pot);
	memFree(__FFL__, MID_GHOSTS, e_pot, n_ptl * sizeof(float));

	/*
	 * Compute R200m and set it for the ghost. The function can fail in a number of ways, one of
	 * which is critical (too low density). Note that this is very similar to the corresponding
	 * function for subhalos, but here we react differently to failures.
	 */
	ret = computeSOFromTracers(snap_idx, halo, tr_res, config.this_snap_rho_200m, &R200m, &M200m);

	if (ret == SO_EXIT_SUCCESS)
	{
		debugHalo(halo,
				"Radius finder: Computed radius from tracers, R200m %.2e kpc/h and mass %.2e Msun/h.",
				R200m, M200m);
	} else if (ret == SO_EXIT_DENSITY_LOW)
	{
		debugHalo(halo, "Radius finder: Density too low to find R200m from tracers, ending ghost.");
		endHalo(halo, snap_idx, snap_idx - 1, HALO_ENDED_GHOST_TOO_SMALL);
	} else if (ret == SO_EXIT_DENSITY_HIGH)
	{
		debugHalo(halo,
				"Radius finder: Density too high to find R200m from tracers, reverted to total mass %.2e Msun/h and R200m %.2e kpc/h.",
				M200m, R200m);
	} else
	{
		error(__FFL__, "Unknown return code from tracer SO function.\n");
	}

	if (hasEnded(halo))
		return;
	halo->history_R200m[snap_idx] = R200m;

	/*
	 * Check that the ghost center has not been overlapping with the host center for too long. If
	 * so, we abort the ghost.
	 */
	has_merged = 0;
	s = snap_idx;
	while ((s >= 0) && (s >= halo->first_snap) && (s >= host->first_snap))
	{
		delta_x = periodicDistance(haloX(halo, s, snap_idx), haloX(host, s, snap_idx));
		R200m_host_com = haloR200mComoving(host, s);
		R200m_sub_phys = haloR200m(halo, s);
		N200m_sub = soRtoM(R200m_sub_phys, config.snap_rho_200m[s]) / config.particle_mass;

#if GHOST_RADIUS_SIGMA_EXACT
		sigma_x = ghostRadiusUncertainty(halo, snap_idx, tr_res, R200m_sub_phys);
#else
		sigma_x = comovingRadius(0.5 * R200m_sub_phys / sqrt(N200m_sub), s);
#endif

		delta_x_ratio_err = (delta_x - sigma_x) / R200m_host_com;
		delta_x_ratio_err = fmax(delta_x_ratio_err, 0.0);

		if (delta_x_ratio_err > config.ghost_min_host_distance)
		{
			debugHalo(halo,
					"Ghost center at %.3f R200m (%.3f incl uncertainty) of host at snapshot %d, not merging with host.",
					delta_x / R200m_host_com, delta_x_ratio_err, s);
			break;
		}
		delta_t_dyn = (config.snap_t[snap_idx] - config.snap_t[s]) / config.snap_t_dyn[snap_idx];
		if (delta_t_dyn > config.ghost_max_host_center_time)
		{
			has_merged = 1;
			debugHalo(halo,
					"Ghost center within %.3f R200m of host for %.2f (more than %.2f) dynamical times (since snapshot %d), ending ghost.",
					config.ghost_min_host_distance, delta_t_dyn, config.ghost_max_host_center_time,
					s);
			break;
		}
		s--;
	}

	if (has_merged)
	{
		endHalo(halo, snap_idx, snap_idx - 1, HALO_ENDED_GHOST_CENTER);
		return;
	}

	/*
	 * Check particle number against lower limit
	 */
	N200m = M200m / config.particle_mass;
	if (N200m < config.ghost_min_n200m)
	{
		debugHalo(halo, "Ending ghost, found N200m = %.0f particles (minimum %d).", N200m,
				config.ghost_min_n200m);
		endHalo(halo, snap_idx, snap_idx - 1, HALO_ENDED_GHOST_TOO_SMALL);
	}
	if (hasEnded(halo))
		return;

	/*
	 * Finally, determine the ghost's velocity. We had no reason to do this earlier in case the
	 * ghost got aborted.
	 */
	determineGhostV(snap_idx, halo, tr_res);
}

/*
 * This function can be called for a phantom halo if its tracers are not NULL, i.e., if we have
 * found some tracers.
 */
void determinePhantomProperties(Halo *halo, Halo *host, int snap_idx, TreeResults *tr_res)
{
	int n_ptl;
	float *e_pot;

	/*
	 * Compute phantom center.
	 */
	n_ptl = tr_res->num_points;
	e_pot = (float*) memAlloc(__FFL__, MID_GHOSTS, n_ptl * sizeof(float));
	computePotentialEnergy(snap_idx, tr_res, e_pot);
	determineGhostX(snap_idx, halo, tr_res, e_pot);
	memFree(__FFL__, MID_GHOSTS, e_pot, n_ptl * sizeof(float));

	/*
	 * Compute phantom radius. Here, we proceed as if for a subhalo, that is, if the radius
	 * calculation fails we fall back to the catalog and do not abort the halo as we would for a
	 * ghost.
	 *
	 * Note that this function works even if the tracer positions have not yet been updated.
	 */
	computeR200mFromSubhaloTracers(snap_idx, halo, tr_res);

	/*
	 * Compute the phantom velocity. Note that we need the radius to be computed first, as this
	 * function considers only particles within R200m.
	 */
	determineGhostV(snap_idx, halo, tr_res);
}

int compareGhostParticleSorter(const void *a, const void *b)
{
	const GhostParticleSorter *da = (const GhostParticleSorter*) a;
	const GhostParticleSorter *db = (const GhostParticleSorter*) b;

	return (da->pe < db->pe) - (da->pe > db->pe);
}

/*
 * If the halo has very few particles, set the center to the position of the particle with the
 * highest binding energy. This estimate is noisy as it relies on a single particle. If we have
 * enough particles, we pick the quarter with the highest binding energy, but no more than 20
 * particles, and compute their center of mass.
 */
void determineGhostX(int snap_idx, Halo *halo, TreeResults *tr_res, float *e_pot)
{
	int i, d, n_ptl, n_ptl_use, most_bnd_idx;
	float x_com[3], n_ptl_inv;
	GhostParticleSorter *ps;

	n_ptl = tr_res->num_points;

	if (n_ptl < 10)
	{
		most_bnd_idx = 0;
		for (i = 0; i < n_ptl; i++)
		{
			if (e_pot[i] > e_pot[most_bnd_idx])
				most_bnd_idx = i;
		}
		for (d = 0; d < 3; d++)
			halo->cd.x[d] = tr_res->points[most_bnd_idx]->x[d];
	} else
	{
		ps = (GhostParticleSorter*) memAlloc(__FFL__, MID_GHOSTS,
				n_ptl * sizeof(GhostParticleSorter));

		for (i = 0; i < n_ptl; i++)
		{
			ps[i].pe = e_pot[i];
			for (d = 0; d < 3; d++)
				ps[i].x[d] = tr_res->points[i]->x[d];
		}
		qsort(ps, n_ptl, sizeof(GhostParticleSorter), &compareGhostParticleSorter);

		n_ptl_use = fmin(n_ptl / 4, 20);
		n_ptl_inv = 1.0 / (float) n_ptl_use;
		for (d = 0; d < 3; d++)
			x_com[d] = 0.0;
		for (i = 0; i < n_ptl_use; i++)
		{
			for (d = 0; d < 3; d++)
				x_com[d] += ps[i].x[d];
		}
		for (d = 0; d < 3; d++)
			halo->cd.x[d] = x_com[d] * n_ptl_inv;

		memFree(__FFL__, MID_GHOSTS, ps, n_ptl * sizeof(GhostParticleSorter));
	}

	for (d = 0; d < 3; d++)
		halo->cd.x[d] = correctPeriodic(halo->cd.x[d], config.box_size);

	halo->updated_xv = 1;
}

/*
 * Average the velocity in physical velocities, convert back to km/s
 */
void determineGhostV(int snap_idx, Halo *halo, TreeResults *tr_res)
{
	int i, d, n_ptl, n_included;
	float v[3], r, v_phys_vec[3], v_phys, null_vec[3] =
		{0.0, 0.0, 0.0};
	Particle *ptl;

	n_ptl = tr_res->num_points;
	n_included = 0;

	for (d = 0; d < 3; d++)
		v[d] = 0.0;

	for (i = 0; i < n_ptl; i++)
	{
		ptl = tr_res->points[i];
		r = tracerRadius(snap_idx, halo->cd.x, ptl->x);
		if (r <= haloR200m(halo, snap_idx))
		{
			tracerVelocityVector(snap_idx, halo->cd.x, null_vec, ptl->x, ptl->v, &v_phys,
					v_phys_vec);
			n_included++;
			for (d = 0; d < 3; d++)
				v[d] += v_phys_vec[d];
		}
	}

	for (d = 0; d < 3; d++)
		halo->cd.v[d] = v[d] / n_included / config.v_conversion;

	halo->updated_xv = 1;
}

float ghostRadiusUncertainty(Halo *halo, int snap_idx, TreeResults *tr_res, float R200m)
{
	int i, n_in_r200m;
	float radius_err, sigma_r, r;

	n_in_r200m = 0;
	sigma_r = 0.0;
	for (i = 0; i < tr_res->num_points; i++)
	{
		r = tracerRadius(snap_idx, halo->cd.x, tr_res->points[i]->x);
		if (r < R200m)
		{
			n_in_r200m++;
			sigma_r += r * r;
		}
	}
	sigma_r = sqrt(sigma_r / (float) n_in_r200m);

	output(0, "sigma %.3f R200m, N %4d, err %.3f approx err %.3f err ratio %.2f\n", sigma_r / R200m,
			n_in_r200m, sigma_r / R200m / sqrt(n_in_r200m), 0.5 / sqrt(n_in_r200m),
			sigma_r / R200m / 0.5);

	radius_err = sigma_r / sqrt(n_in_r200m);

	return radius_err;
}

/*
 * Compute potential. Here, we need to convert to physical kpc/h. We could also do the
 * calculation in comoving units, but this_snap_force_res and the kin-pot conversion factors
 * assume the internal physical variable system.
 */
void computePotentialEnergy(int snap_idx, TreeResults *tr_res, float *e_pot)
{
	int i, d, n_ptl;
	TreePotentialPoint *pp;

	n_ptl = tr_res->num_points;
	pp = (TreePotentialPoint*) memAlloc(__FFL__, MID_GHOSTS, n_ptl * sizeof(TreePotentialPoint));
	for (i = 0; i < n_ptl; i++)
		for (d = 0; d < 3; d++)
			pp[i].x[d] = physicalRadius(tr_res->points[i]->x[d], snap_idx);
	computeTreePotential(pp, n_ptl, config.particle_mass, config.this_snap_force_res,
			config.potential_err_tol, config.potential_points_per_leaf);
	for (i = 0; i < n_ptl; i++)
		e_pot[i] = pp[i].pe;
	memFree(__FFL__, MID_GHOSTS, pp, n_ptl * sizeof(TreePotentialPoint));
}

/*
 * Compute center of mass and mean velocity. Note that we are taking the average of peculiar
 * velocities, but that is the same as taking the average velocity of physical velocities: the
 * latter carries an extra term of a * H * r_COM, where r_COM is the average position or the
 * center of mass. This means, however, that v_COM only makes sense when also choosing r_COM;
 * if we choose another center position, there is a contribution from the Hubble term.
 */
void computeCenterOfMass(TreeResults *tr_res, float *x_com, float *v_com)
{
	int i, d, n_ptl;
	float n_ptl_inv;
	Particle *ptl;

	n_ptl = tr_res->num_points;
	n_ptl_inv = 1.0 / (float) n_ptl;
	for (d = 0; d < 3; d++)
	{
		x_com[d] = 0.0;
		v_com[d] = 0.0;
	}
	for (i = 0; i < n_ptl; i++)
	{
		ptl = tr_res->points[i];
		for (d = 0; d < 3; d++)
		{
			x_com[d] += ptl->x[d];
			v_com[d] += ptl->v[d];
		}
	}
	for (d = 0; d < 3; d++)
	{
		x_com[d] *= n_ptl_inv;
		v_com[d] *= n_ptl_inv;
	}
}

/*
 * For debugging, we may want to compare the properties derived for a ghost to those found by the
 * halo finder. Thus, we treat a subhalo as if it were a ghost, but do not actually replace its
 * properties.
 *
 * There are a number of points where we may need to abort the ghostProperties function below. We
 * need to clean up memory, but most importantly, if we are in compare mode we need to restore the
 * original properties of the subhalo that we are comparing.
 */
#if DEBUG_OUTPUT_GHOSTS

void endDebugFileFunction(int snap_idx, Halo *halo, HaloCatalogData *cd, float *e_pot, int n_ptl,
		float R200m_original)
{
	memFree(__FFL__, MID_GHOSTS, e_pot, n_ptl * sizeof(float));
	memcpy(&(halo->cd), cd, sizeof(HaloCatalogData));
	halo->history_R200m[snap_idx] = R200m_original;
}

void outputGhostDebugFile(Halo *halo, Halo *host, int snap_idx, TreeResults *tr_res, Tree *tree)
{
	/*
	 * Check that we are actually outputting this halo. Note that the ghost ID has not yet been
	 * set at this point.
	 */
	if ((DEBUG_OUTPUT_GHOSTS_ID != 0) && (haloOriginalID(halo) != DEBUG_OUTPUT_GHOSTS_ID))
		return;

	if ((DEBUG_OUTPUT_GHOSTS_STRIDE != 0)
			&& (haloOriginalID(halo) % DEBUG_OUTPUT_GHOSTS_STRIDE != 0))
		return;

	/*
	 * Variables
	 */
	int i, s, n_ptl, n_ptl_hst, ret;
	float *e_pot, x_com[3], v_com[3], R200m_original, R200m, M200m, R200m_sub_phys, N200m_sub,
			sigma_x_ghost;
	HaloCatalogData cd;

	/*
	 * If we are in compare mode, we make a backup copy of the "true" data so that we can
	 * restored them later.
	 */
	memcpy(&cd, &(halo->cd), sizeof(HaloCatalogData));
	R200m_original = halo->history_R200m[snap_idx];

	/*
	 * Compute ghost center and velocity. Afterwards, there is no guarantee that the position obeys
	 * periodic BCs. During the process, the ghost can end, in which case we need to abort.
	 */
	n_ptl = tr_res->num_points;
	computeCenterOfMass(tr_res, x_com, v_com);
	e_pot = (float*) memAlloc(__FFL__, MID_GHOSTS, n_ptl * sizeof(float));
	computePotentialEnergy(snap_idx, tr_res, e_pot);
	determineGhostX(snap_idx, halo, tr_res, e_pot);

	/*
	 * Compute R200m and set it for the ghost. The function can fail in a number of ways, one of
	 * which is critical (too low density).
	 */
	ret = computeSOFromTracers(snap_idx, halo, tr_res, config.this_snap_rho_200m, &R200m, &M200m);
	if ((ret == SO_EXIT_DENSITY_LOW) || hasEnded(halo))
	{
		endDebugFileFunction(snap_idx, halo, &cd, e_pot, n_ptl, R200m_original);
		return;
	}
	halo->history_R200m[snap_idx] = R200m;

	/*
	 * Check that the ghost center has not been overlapping with the host center for too long. If
	 * so, we abort the ghost.
	 */
	R200m_sub_phys = haloR200m(halo, snap_idx);
	N200m_sub = haloM200m(halo, snap_idx) / config.particle_mass;
	sigma_x_ghost = comovingRadius(0.5 * R200m_sub_phys / sqrt(N200m_sub), snap_idx);
	determineGhostV(snap_idx, halo, tr_res);

	/*
	 * Variables for output
	 */
	hid_t file_id;
	int n_hist_sub, n_hist_host, j;
	char filename[100];
	float **ptl_x, **ptl_v, **host_x, **sub_x, *host_R200m_all_array, *sub_R200m_all_array,
			rho_200c, R200c, M200c, rho_500c, R500c, M500c, R200m_com;
	HaloDefinition def_500c, def_200c;
	TreeResults *tr_res_hst;

	/*
	 * Compute R500c and R200c
	 */
	def_200c = getHaloDefinitionSO(HDEF_SUBTYPE_SO_CRITICAL, 200.0);
	rho_200c = densityThreshold(&(config.cosmo), config.snap_z[snap_idx], &def_200c);
	ret = computeSOFromTracers(snap_idx, halo, tr_res, rho_200c, &R200c, &M200c);
	if (ret != EXIT_SUCCESS)
		R200c = 0.0;
	R200c = comovingRadius(R200c, snap_idx);

	def_500c = getHaloDefinitionSO(HDEF_SUBTYPE_SO_CRITICAL, 500.0);
	rho_500c = densityThreshold(&(config.cosmo), config.snap_z[snap_idx], &def_500c);
	ret = computeSOFromTracers(snap_idx, halo, tr_res, rho_500c, &R500c, &M500c);
	if (ret != EXIT_SUCCESS)
		R500c = 0.0;
	R500c = comovingRadius(R500c, snap_idx);

	R200m_com = comovingRadius(R200m, snap_idx);
	if (R200c > R200m_com)
		error(__FFL__, "R200c %.4e > R200m %.4e in halo ID %ld.\n", R200c, R200m_com, halo->cd.id);

	if (haloOriginalID(halo) == 71060)
		output(0, "R = %.2e %.2e %.2e \n", R200m_com, R200c, R500c);

	createPath("ghosts", 02755, 0);
	sprintf(filename, "ghosts/ghost_%08ld_snap%03d.hdf5", haloOriginalID(halo), snap_idx);
	file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForError(__FFL__, file_id, "creating ghost file");

	/*
	 * Write particle data (ghost tracers)
	 */
	ptl_x = (float**) memAlloc(__FFL__, MID_GHOSTS, n_ptl * sizeof(float*));
	ptl_x[0] = (float*) memAlloc(__FFL__, MID_GHOSTS, 3 * n_ptl * sizeof(float));
	ptl_v = (float**) memAlloc(__FFL__, MID_GHOSTS, n_ptl * sizeof(float*));
	ptl_v[0] = (float*) memAlloc(__FFL__, MID_GHOSTS, 3 * n_ptl * sizeof(float));

	for (i = 0; i < n_ptl; i++)
	{
		if (i != 0)
		{
			ptl_x[i] = ptl_x[0] + i * 3;
			ptl_v[i] = ptl_v[0] + i * 3;
		}

		for (j = 0; j < 3; j++)
		{
			ptl_x[i][j] = tr_res->points[i]->x[j];
			ptl_v[i][j] = tr_res->points[i]->v[j];
		}
	}

	hdf5WriteDataset2D(file_id, "ptl_x", DTYPE_FLOAT, n_ptl, 3, (void*) ptl_x[0]);
	hdf5WriteDataset2D(file_id, "ptl_v", DTYPE_FLOAT, n_ptl, 3, (void*) ptl_v[0]);
	hdf5WriteDataset(file_id, "ptl_epot", DTYPE_FLOAT, n_ptl, (void*) e_pot);

	memFree(__FFL__, MID_GHOSTS, ptl_x[0], 3 * n_ptl * sizeof(float));
	memFree(__FFL__, MID_GHOSTS, ptl_x, n_ptl * sizeof(float*));
	memFree(__FFL__, MID_GHOSTS, ptl_v[0], 3 * n_ptl * sizeof(float));
	memFree(__FFL__, MID_GHOSTS, ptl_v, n_ptl * sizeof(float*));

	/*
	 * Write particle data (host)
	 */
	tr_res_hst = treeResultsInit();
	treeFindSpherePeriodicCorrected(tree, tr_res_hst, host->cd.x,
			haloR200mComoving(host, snap_idx));
	n_ptl_hst = tr_res_hst->num_points;

	ptl_x = (float**) memAlloc(__FFL__, MID_GHOSTS, n_ptl_hst * sizeof(float*));
	ptl_x[0] = (float*) memAlloc(__FFL__, MID_GHOSTS, 3 * n_ptl_hst * sizeof(float));

	for (i = 0; i < n_ptl_hst; i++)
	{
		if (i != 0)
			ptl_x[i] = ptl_x[0] + i * 3;
		for (j = 0; j < 3; j++)
			ptl_x[i][j] = tr_res_hst->points[i]->x[j];
	}
	treeResultsFree(tr_res_hst);

	hdf5WriteDataset2D(file_id, "ptl_host_x", DTYPE_FLOAT, n_ptl_hst, 3, (void*) ptl_x[0]);

	memFree(__FFL__, MID_GHOSTS, ptl_x[0], 3 * n_ptl_hst * sizeof(float));
	memFree(__FFL__, MID_GHOSTS, ptl_x, n_ptl_hst * sizeof(float*));

	/*
	 * Write attributes
	 */
	hdf5WriteAttributeLong(file_id, "halo_id", halo->cd.id);
	hdf5WriteAttributeInt(file_id, "snap_idx", snap_idx);
	hdf5WriteAttributeFloat(file_id, "snap_z", config.snap_z[snap_idx]);
	hdf5WriteAttributeFloat(file_id, "particle_mass", config.particle_mass);
	hdf5WriteDataset(file_id, "snap_t_dyn", DTYPE_FLOAT, config.n_snaps, config.snap_t_dyn);
	hdf5WriteDataset(file_id, "snap_t", DTYPE_FLOAT, config.n_snaps, config.snap_t);
	hdf5WriteAttributeFloat(file_id, "r200c_tcr_com", R200c);
	hdf5WriteAttributeFloat(file_id, "r500c_tcr_com", R500c);
	hdf5WriteAttributeFloat(file_id, "sigma_x_ghost", sigma_x_ghost);

	/*
	 * Write a bit of history of the host and sub position and radii. We are not worrying about
	 * periodic BCs here, though they could be an issue.
	 */
	n_hist_host = snap_idx - host->first_snap + 1;
	n_hist_host = imin(n_hist_host, STCL_HALO_X + 1);

	host_x = (float**) memAlloc(__FFL__, MID_GHOSTS, n_hist_host * sizeof(float*));
	host_x[0] = (float*) memAlloc(__FFL__, MID_GHOSTS, 3 * n_hist_host * sizeof(float));
	host_R200m_all_array = (float*) memAlloc(__FFL__, MID_GHOSTS, n_hist_host * sizeof(float));

	for (i = 0; i < n_hist_host; i++)
	{
		if (i != 0)
			host_x[i] = host_x[0] + i * 3;

		s = snap_idx - n_hist_host + i + 1;
		for (j = 0; j < 3; j++)
			host_x[i][j] = haloX(host, s, snap_idx)[j];
		host_R200m_all_array[i] = haloR200mComoving(host, s);
	}
	for (j = 0; j < 3; j++)
		host_x[n_hist_host - 1][j] = host->cd.x[j];

	hdf5WriteDataset2D(file_id, "host_hist_x", DTYPE_FLOAT, n_hist_host, 3, (void*) host_x[0]);
	hdf5WriteDataset(file_id, "host_hist_r200m", DTYPE_FLOAT, n_hist_host, host_R200m_all_array);

	hdf5WriteDataset(file_id, "host_x", DTYPE_FLOAT, 3, host->cd.x);
	hdf5WriteDataset(file_id, "host_v", DTYPE_FLOAT, 3, host->cd.v);
	hdf5WriteAttributeFloat(file_id, "host_r200m", haloR200mComoving(host, snap_idx));

	memFree(__FFL__, MID_GHOSTS, host_x[0], 3 * n_hist_host * sizeof(float));
	memFree(__FFL__, MID_GHOSTS, host_x, n_hist_host * sizeof(float*));
	memFree(__FFL__, MID_GHOSTS, host_R200m_all_array, n_hist_host * sizeof(float));

	/*
	 * Now repeat for sub
	 */
	n_hist_sub = snap_idx - halo->first_snap + 1;
	n_hist_sub = imin(n_hist_sub, STCL_HALO_X + 1);

	sub_x = (float**) memAlloc(__FFL__, MID_GHOSTS, n_hist_sub * sizeof(float*));
	sub_x[0] = (float*) memAlloc(__FFL__, MID_GHOSTS, 3 * n_hist_sub * sizeof(float));
	sub_R200m_all_array = (float*) memAlloc(__FFL__, MID_GHOSTS, n_hist_sub * sizeof(float));

	for (i = 0; i < n_hist_sub; i++)
	{
		if (i != 0)
			sub_x[i] = sub_x[0] + i * 3;

		s = snap_idx - n_hist_sub + i + 1;
		for (j = 0; j < 3; j++)
			sub_x[i][j] = haloX(halo, s, snap_idx)[j];
		sub_R200m_all_array[i] = haloR200mComoving(halo, s);

		/*
		 * We did not set the current R200m in the sub as we are in compare mode
		 */
		if (s == snap_idx)
			sub_R200m_all_array[i] = R200m_com;
	}
	for (j = 0; j < 3; j++)
		sub_x[n_hist_sub - 1][j] = halo->cd.x[j];

	hdf5WriteDataset2D(file_id, "sub_hist_x", DTYPE_FLOAT, n_hist_sub, 3, (void*) sub_x[0]);
	hdf5WriteDataset(file_id, "sub_hist_r200m", DTYPE_FLOAT, n_hist_sub, sub_R200m_all_array);

	hdf5WriteDataset(file_id, "sub_x_com", DTYPE_FLOAT, 3, x_com);
	hdf5WriteDataset(file_id, "sub_v_com", DTYPE_FLOAT, 3, v_com);
	hdf5WriteDataset(file_id, "sub_x_gst", DTYPE_FLOAT, 3, halo->cd.x);
	hdf5WriteDataset(file_id, "sub_v_gst", DTYPE_FLOAT, 3, halo->cd.v);

	memFree(__FFL__, MID_GHOSTS, sub_x[0], 3 * n_hist_sub * sizeof(float));
	memFree(__FFL__, MID_GHOSTS, sub_x, n_hist_sub * sizeof(float*));
	memFree(__FFL__, MID_GHOSTS, sub_R200m_all_array, n_hist_sub * sizeof(float));

	/*
	 * The halo may or may not be a ghost; if the former, we should not output the previous x/v
	 * as they are the same as the ones determined here, not from the catalog.
	 */
	if (!isGhost(halo))
	{
		hdf5WriteDataset(file_id, "sub_x_cat", DTYPE_FLOAT, 3, cd.x);
		hdf5WriteDataset(file_id, "sub_v_cat", DTYPE_FLOAT, 3, cd.v);
		hdf5WriteAttributeFloat(file_id, "cat_r200m_all_com", cd.R200m_cat_com);
	}

	H5Fclose(file_id);

	endDebugFileFunction(snap_idx, halo, &cd, e_pot, n_ptl, R200m_original);
}
#endif

#endif
