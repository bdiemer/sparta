/*************************************************************************************************
 *
 * This unit implements basic routines related to the halos.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>
#include <ctype.h>
#include <stdarg.h>

#include "halo.h"
#include "halo_definitions.h"
#include "halo_ghosts.h"
#include "halo_subs.h"
#include "halo_so.h"
#include "../config.h"
#include "../memory.h"
#include "../utils.h"
#include "../geometry.h"
#include "../tracers/tracers.h"

/*************************************************************************************************
 * FUNCTIONS - DEBUG / INITIALIZE / FREE
 *************************************************************************************************/

void printDebugHalo(Halo *halo, const char *format, ...)
{
	if (halo == NULL)
		return;

	char halo_str[200];
	int do_print = 0;

	if ((halo->cd.id == DEBUG_HALO) || (haloOriginalID(halo) == DEBUG_HALO))
	{
		do_print = 1;
		sprintf(halo_str,
				"Halo ID %ld (orig_id %ld, pid %ld, desc_id %ld, desc_pid %ld, status %d)",
				halo->cd.id, haloOriginalID(halo), halo->cd.pid, halo->cd.desc_id,
				halo->cd.desc_pid, halo->status);
	} else if (halo->cd.pid == DEBUG_HALO)
	{
		do_print = 1;
		sprintf(halo_str,
				"Subhalo ID    %ld (orig_id %ld, pid %ld, desc_id %ld, desc_pid %ld, status %d)",
				halo->cd.id, haloOriginalID(halo), halo->cd.pid, halo->cd.desc_id,
				halo->cd.desc_pid, halo->status);
	} else if (halo->cd.desc_id == DEBUG_HALO)
	{
		do_print = 1;
		sprintf(halo_str,
				"Progenitor ID %ld (orig_id %ld, pid %ld, desc_id %ld, desc_pid %ld, status %d)",
				halo->cd.id, haloOriginalID(halo), halo->cd.pid, halo->cd.desc_id,
				halo->cd.desc_pid, halo->status);
	} else if (halo->cd.desc_pid == DEBUG_HALO)
	{
		do_print = 1;
		sprintf(halo_str,
				"Sub-prog ID   %ld (orig_id %ld, pid %ld, desc_id %ld, desc_pid %ld, status %d)",
				halo->cd.id, haloOriginalID(halo), halo->cd.pid, halo->cd.desc_id,
				halo->cd.desc_pid, halo->status);
	}

	if (do_print)
	{
		char msg[400];

		va_list args;
		va_start(args, format);
		vsprintf(msg, format, args);
		va_end(args);

		output(0, "[%4d] *** %s: %s\n", proc, halo_str, msg);
	}
}

void printDebugHaloCatalogData(int is_main, HaloCatalogData *cd, const char *format, ...)
{
	if (cd == NULL)
		return;

	char halo_str[200];
	int do_print = 0;

	if (cd->id == DEBUG_HALO)
	{
		do_print = 1;
		sprintf(halo_str, "Halo ID %ld (pid %ld, desc_id %ld, desc_pid %ld)", cd->id, cd->pid,
				cd->desc_id, cd->desc_pid);
	} else if (cd->pid == DEBUG_HALO)
	{
		do_print = 1;
		sprintf(halo_str, "Subhalo ID %ld (pid %ld, desc_id %ld, desc_pid %ld)", cd->id, cd->pid,
				cd->desc_id, cd->desc_pid);
	} else if (cd->desc_id == DEBUG_HALO)
	{
		do_print = 1;
		sprintf(halo_str, "Progenitor ID %ld (pid %ld, desc_id %ld, desc_pid %ld)", cd->id, cd->pid,
				cd->desc_id, cd->desc_pid);
	} else if (cd->desc_pid == DEBUG_HALO)
	{
		do_print = 1;
		sprintf(halo_str, "Sub-prog ID   %ld (pid %ld, desc_id %ld, desc_pid %ld)", cd->id, cd->pid,
				cd->desc_id, cd->desc_pid);
	}

	if (do_print)
	{
		char msg[400];

		va_list args;
		va_start(args, format);
		vsprintf(msg, format, args);
		va_end(args);

		if (is_main)
			output(0, "[Main] *** %s: %s\n", halo_str, msg);
		else
			output(0, "[%4d] *** %s: %s\n", proc, halo_str, msg);
	}
}

void initializeHalo(Halo *halo)
{
	int i, j, tt, rs;

	halo->status = HALO_STATUS_NONE;
	halo->needs_all_ptl = 0;
	halo->search_radius_decreased = 0;
	halo->radius_from_catalog = 0;
	halo->updated_xv = 0;

	for (tt = 0; tt < NTT; tt++)
	{
		halo->instr_tcr[tt] = 1;
		for (rs = 0; rs < NRS; rs++)
			halo->instr_rs[tt][rs] = config.all_instructions[tt][rs];
	}

	halo->id_sort = LARGE_ID;
	halo->first_snap = -1;
	halo->last_snap = -1;
	halo->host_idx = INVALID_IDX;

	halo->r_search_factor = 0.0;
	halo->r_search_com_guess = INVALID_R;
	halo->r_search_com_actual = INVALID_R;

	initHaloCatalogData(&(halo->cd));

	for (i = 0; i < 3; i++)
	{
		for (j = 0; j < STCL_HALO_X; j++)
			halo->history_x[j][i] = 0.0;
		for (j = 0; j < STCL_HALO_V; j++)
			halo->history_v[j][i] = 0.0;
	}
	for (i = 0; i < MAX_SNAPS; i++)
	{
		halo->history_id[i] = INVALID_ID;
#if OUTPUT_HALO_PARENT_ID
		halo->history_pid[i] = INVALID_ID;
#endif
		halo->history_R200m[i] = INVALID_R;
		halo->history_status[i] = HALO_STATUS_NONE;
	}
	halo->M_bound_peak = INVALID_M;

#if DO_MASS_PROFILE
	for (i = 0; i < N_MBINS * STCL_TCR; i++)
		halo->grid_M[i] = INVALID_M;
#endif
}

void initHaloDynArrays(Halo *halo)
{
	int tt, al;

	initDynArray(__FFL__, &(halo->subs), DA_TYPE_SUBPOINTER);
	for (tt = 0; tt < NTT; tt++)
		initTracerType(&(halo->tt[tt]));
	for (al = 0; al < NAL; al++)
		initDynArray(__FFL__, &(halo->al[al]), alprops[al].da_type);
}

void initHaloCatalogData(HaloCatalogData *cd)
{
	int d;

	cd->proc = INVALID_PROC;
	cd->id = LARGE_ID;
	cd->pid = LARGE_ID;
	cd->desc_id = LARGE_ID;
	cd->desc_pid = LARGE_ID;
	cd->mmp = 0;
	cd->phantom = 0;
	for (d = 0; d < 3; d++)
	{
		cd->x[d] = 0.0;
		cd->v[d] = 0.0;
	}
	cd->R200m_cat_com = INVALID_R;
	cd->M_bound = INVALID_M;
}

void freeHalo(Halo *halo)
{
	int tt, al;

	freeDynArray(__FFL__, &(halo->subs));
	for (tt = 0; tt < NTT; tt++)
		freeTracerType(&(halo->tt[tt]));
	for (al = 0; al < NAL; al++)
		freeDynArray(__FFL__, &(halo->al[al]));
}

void deleteHalos(DynArray *halos, int_least8_t *mask)
{
	int i;

	for (i = 0; i < halos->n; i++)
	{
		if (mask[i])
			freeHalo(&(halos->h[i]));
	}
	deleteDynArrayItems(__FFL__, halos, mask);
}

/*************************************************************************************************
 * FUNCTIONS - HALO STATUS
 *************************************************************************************************/

/*
 * Since all halos are stored until the end of the snapshot, this function does not really remove
 * a halo, it just deactivates it. Note that we are not removing the subhalo record in a host if
 * this halo is a sub; for the duration of the snapshot, the host keeps a reference to this deleted
 * sub until it is removed at the end of the snap. For this reason, we need to keep the halo ID etc
 * intact.
 *
 * We borrow the status field for the final status. The status during this final snap is stored in
 * the last field of history_status.
 *
 * If the snapshot where the halo is ended is before the current snapshot, we make sure to clean up
 * the history fields because they may be written to file. When the halo does not exist any more,
 * its history should also not exist as halo data may be incomplete starting at the final snap.
 */
void endHalo(Halo *halo, int snap_idx, int final_snap, int final_status)
{
	int i;

#if CAREFUL
	if (hasEnded(halo))
		error(__FFL__,
				"[%4d] Halo ID %ld, original ID %ld has already ended (status %d, new status %d).\n",
				proc, halo->cd.id, haloOriginalID(halo), halo->status, final_status);
#endif

	/*
	 * Set the final snapshot before the coming operations.
	 */
	halo->last_snap = final_snap;

	/*
	 * Clean up history fields that may contain values past the final snapshot of the halo. The
	 * loop never runs if the final snapshot is the current snapshot.
	 */
	for (i = final_snap + 1; i <= snap_idx; i++)
	{
		halo->history_id[i] = INVALID_ID;
#if OUTPUT_HALO_PARENT_ID
		halo->history_pid[i] = INVALID_ID;
#endif
		halo->history_status[i] = HALO_STATUS_NONE;
		halo->history_R200m[i] = INVALID_R;
	}

	/*
	 * The position and velocity histories are shifted to the end of the array, which is
	 * incompatible with the output routine. The exact arrangement depends on whether the halo
	 * ended at this snapshot or a previous snapshot.
	 */
#if (OUTPUT_HALO_X || OUTPUT_HALO_V)
	int d;

	for (i = halo->first_snap; i <= halo->last_snap; i++)
	{
#if OUTPUT_HALO_X
		memcpy(halo->history_x[i], haloX(halo, i, snap_idx), sizeof(float) * 3);
#endif
#if OUTPUT_HALO_V
		memcpy(halo->history_v[i], haloV(halo, i, snap_idx), sizeof(float) * 3);
#endif
	}

	/*
	 * Set the remaining elements to zero, since some of them will end up in the output files.
	 */
	for (i = halo->last_snap + 1; i < STCL_HALO_X; i++)
	{
		for (d = 0; d < 3; d++)
		{
#if OUTPUT_HALO_X
			halo->history_x[i][d] = 0.0;
#endif
#if OUTPUT_HALO_V
			halo->history_v[i][d] = 0.0;
#endif
		}
	}
#endif

	/*
	 * Actually set the status, which ends the halo
	 */
	halo->status = final_status;

	debugHalo(halo, "Ended, last snap %d, final status %d.", halo->last_snap, final_status);
}

/*
 * Delete all halos that have status ended. Return the number of deleted halos.
 */
int deleteEndedHalos(DynArray *halos)
{
	int i, n_deleted, n_old;
	int_least8_t *mask;

	n_deleted = 0;
	mask = (int_least8_t*) memAlloc(__FFL__, MID_HALOLOGIC, sizeof(int_least8_t) * halos->n);
	n_old = halos->n;
	for (i = 0; i < halos->n; i++)
	{
		mask[i] = hasEnded(&(halos->h[i]));
		if (mask[i])
		{
			freeHalo(&(halos->h[i]));
			n_deleted++;
		}
	}
	deleteDynArrayItems(__FFL__, halos, mask);
	memFree(__FFL__, MID_HALOLOGIC, mask, sizeof(int_least8_t) * n_old);

	return n_deleted;
}

void sortHalos(DynArray *halos, int by_desc_id)
{
	int i;

	for (i = 0; i < halos->n; i++)
	{
		if (by_desc_id)
			halos->h[i].id_sort = halos->h[i].cd.desc_id;
		else
			halos->h[i].id_sort = halos->h[i].cd.id;
	}
	qsort(halos->h, halos->n, sizeof(Halo), &compareHalos);
}

/*
 * Sort the halo array by ID. This is the LAST TIME this can be done before the rest of the halo
 * processing! We also sort the tracer arrays as all of them need to be searched at some
 * point. Note that the sort function is called for all halos, even those that do not have subs or
 * tracked particles. This avoids errors due to halo status complications.
 */
void sortArrays(DynArray *halos)
{
	int i, tt;
	Halo *h;

	sortHalos(halos, 0);

	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		qsort(h->subs.sp, h->subs.n, sizeof(SubPointer), &compareSubhalos);
		for (tt = 0; tt < NTT; tt++)
		{
			qsort(h->tt[tt].tcr.tcr, h->tt[tt].tcr.n, sizeof(Tracer), &compareTracers);
			qsort(h->tt[tt].iid.ids, h->tt[tt].iid.n, sizeof(ID), &compareLongsAscending);

			/*
			 * The result arrays should already have been sorted directly after running the results.
			 * If paranoid, we check that explicitly.
			 */
#if PARANOID
			int j, rs;
			long int *id1, *id2;

			for (rs = 0; rs < NRS; rs++)
			{
				for (j = 0; j < h->tt[tt].rs[rs].n - 1; j++)
				{
					id1 = (long int*) (h->tt[tt].rs[rs].data + j * (size_t) rsprops[rs].size);
					id2 = (long int*) (h->tt[tt].rs[rs].data + (j + 1) * (size_t) rsprops[rs].size);
					if (*id2 < *id1)
						error(__FFL__,
								"Found unsorted elements %ld and %ld in tt %d, rs %d, halo ID %ld.\n",
								*id1, *id2, tt, rs, h->cd.id);
				}
			}
#endif
		}
	}
}

/*
 * Set the status for a newly discovered halo. If a halo is born as a subhalo, we have to mark it
 * as BECOMING_SUB rather than SUB, as the latter status presupposes that the halo has been a sub
 * in the snap before (i.e. it has been attached to a host etc). In the weird case where a halo
 * is born a sub but becomes a host in the next snap, we set the BECOMING_SUB_HOST status (though
 * note that this can happen only if the halo exists in the next snap).
 *
 * Furthermore, we set the instructions for each tracer and result. If no results are active for
 * a particular tracer in this halo, we need not add the tracers in the first place.
 */
void setHaloStatusInitial(Halo *halo, HaloCatalogData *cd)
{
	int i, tt, rs;

	if (cd->pid == -1)
	{
		halo->status = HALO_STATUS_HOST;
	} else
	{
		if ((cd->desc_pid == -1) && (cd->desc_id != -1) && cd->mmp)
			halo->status = HALO_STATUS_BECOMING_SUB_HOST;
		else
			halo->status = HALO_STATUS_BECOMING_SUB;
	}

	for (i = 0; i < config.n_halo_instructions; i++)
	{
		if (config.halo_instructions[i].id == cd->id)
		{
			/*
			 * If this instruction has both a tcr and res, we set that precise result. If it
			 * switches a tcr on in general, we also just take that instruction. However, if it
			 * switches a tcr off, we need to also turn off that tracer's results as otherwise
			 * the check below will turn the tracer on again.
			 */
			HaloInstruction *hn = &(config.halo_instructions[i]);
			if (hn->rs == -1)
			{
				halo->instr_tcr[hn->tt] = hn->do_tt_rs;
				if (!hn->do_tt_rs)
				{
					for (rs = 0; rs < NRS; rs++)
						halo->instr_rs[hn->tt][rs] = 0;
				}
			} else
			{
				halo->instr_rs[hn->tt][hn->rs] = hn->do_tt_rs;
			}
		}
	}

	for (tt = 0; tt < NTT; tt++)
	{
		halo->instr_tcr[tt] = 0;
		for (rs = 0; rs < NRS; rs++)
		{
			if (halo->instr_rs[tt][rs])
			{
				halo->instr_tcr[tt] = 1;
				break;
			}
		}
	}
}

/*
 * Prepare to find halos in this snapshot: the CONNECTING status indicates that we expect to find
 * a descendant (regardless of whether the halo is host or sub).
 *
 * Also, we need to advance the ID of all tracked subhalos in their hosts so that they can be
 * assigned again.
 */
void setHaloStatusSnapStart(int snap_idx, DynArray *halos)
{
	int i;
	Halo *h;

	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);

		if (h->status == HALO_STATUS_HOST)
		{
			h->status = HALO_STATUS_CONNECTING_HOST;
		} else if (h->status == HALO_STATUS_SUB)
		{
			h->status = HALO_STATUS_CONNECTING_SUB;
		} else if (h->status == HALO_STATUS_SWITCHING_HOST)
		{
			h->status = HALO_STATUS_CONNECTING_SUB_SWITCH;
		} else if (isGhost(h))
		{
			debugHalo(h, "Found ghost at beginning of snapshot. Not connecting.");
		} else
		{
			error(__FFL__, "Unexpected halo status at beginning of snap, %d.\n", h->status);
		}

		/*
		 * The updated_xv flag indicates that the position changed after particles were found; we
		 * reset it at every snapshot.
		 */
		h->updated_xv = 0;

		debugHalo(h, "Initial status for snapshot set.");
	}
}

/*
 * We've found a descendant halo, now we need to decide what the status of this descendant is
 * going to be. First, we check for two possible errors:
 *
 * 1) No descendant was found, we have to abort the halo. In this case, the ID is set to a large
 *    ID so that the halo cannot be found in searches.
 * 2) The new halo has "jumped", i.e. is far from the progenitor. Again, we abort the halo, but
 *    keep its ID etc fields intact.
 *
 * If none of these two cases have happened, there are a few more possibilities:
 *
 * 3a) Both the progenitor and descendant are hosts, keep the same status
 * 3b) The progenitor was a host but now the descendant is becoming a sub, but becoming a host
 *     again in the next snap. This is only the case if a descendant exists, otherwise the halo
 *     ends as a sub (3c).
 * 3c) The progenitor was a host but now the descendant is not, indicate that the halo is becoming
 *     a subhalo at this snap.
 *
 * 4a) The progenitor was a subhalo, but is now a host again (i.e. it is a backsplash halo). We
 *     indicate in the status that it is becoming a host at this snapshot. This is only the case
 *     if a descendant exists, otherwise the halo ends as a sub (4b).
 * 4b) Both the progenitor and descendant are subhalos, keep the same status.
 *
 * 5)  Both the progenitor and descendant are subhalos, but in different hosts. In this case, we
 *     indicate that the halo is entering a new host (though not becoming a sub). This situation
 *     cannot be uniquely determined from the ID fields of the prog and desc halos alone, because
 *     the desc_pid can be changed for sub-sub halos, requiring that we "remember" the situation
 *     through the connection phase.
 */
void setHaloStatusConnect(int snap_idx, Halo *h, HaloCatalogData *new_cd)
{
	/*
	 * Case 1. If we end the halo, we must end it retroactively at the previous snapshot because
	 * the halo data will be corrupted from now on.
	 */
	if (new_cd->request_status != HALO_REQUEST_STATUS_FOUND)
	{
		if (new_cd->request_status == HALO_REQUEST_STATUS_NOT_FOUND)
		{
			warningOrError(__FFL__, config.err_level_desc_not_found,
					"[%4d] Descendant not found in catalog. Progenitor ID %ld x [%.2f %.2f %.2f], descendant ID %ld x [%.2f %.2f %.2f]. Aborting this halo.\n",
					proc, h->cd.id, h->cd.x[0], h->cd.x[1], h->cd.x[2], new_cd->id, new_cd->x[0],
					new_cd->x[1], new_cd->x[2]);
			endHalo(h, snap_idx, snap_idx - 1, HALO_ENDED_NOT_FOUND);
			new_cd->id = LARGE_ID;
			debugHalo(h, "Request marked as not found, ended halo.");
		} else
		{
			error(__FFL__, "Unknown request status, %d.\n", new_cd->request_status);
		}
		return;
	}

	/*
	 * Case 2. See comment above, we must end halos at the previous snapshot.
	 */
	float dist = periodicDistance(h->cd.x, new_cd->x);
	if (dist > config.jump_threshold)
	{
		warningOrError(__FFL__, config.err_level_halo_jump,
				"[%4d] Detected jump in halo position. Progenitor ID %ld x [%.2f %.2f %.2f], descendant ID %ld x [%.2f %.2f %.2f], distance %.2f. Aborting this halo.\n",
				proc, h->cd.id, h->cd.x[0], h->cd.x[1], h->cd.x[2], new_cd->id, new_cd->x[0],
				new_cd->x[1], new_cd->x[2], dist);
		endHalo(h, snap_idx, snap_idx - 1, HALO_ENDED_JUMP);
		debugHalo(h, "Detected jump in halo position, ended halo.");

		return;
	}

	/*
	 * Cases 3-5
	 */
	if (h->status == HALO_STATUS_CONNECTING_HOST)
	{
		// Case 3
		if (new_cd->pid == -1)
		{
			// Case 3a
			h->status = HALO_STATUS_HOST;
		} else
		{
			if ((new_cd->desc_pid == -1) && (new_cd->desc_id != -1) && new_cd->mmp)
			{
				// Case 3b
				h->status = HALO_STATUS_BECOMING_SUB_HOST;
			} else
			{
				// Case 3c
				h->status = HALO_STATUS_BECOMING_SUB;
			}
		}
	} else if (h->status == HALO_STATUS_CONNECTING_SUB)
	{
		if (new_cd->pid == -1)
			error(__FFL__,
					"[%4d] Expected connecting halo ID %ld to be a sub, progenitor %ld, progenitor pid %ld desc_pid %ld.\n",
					proc, new_cd->id, h->cd.id, h->cd.pid, h->cd.desc_pid);

		// Case 4
		if ((new_cd->desc_pid == -1) && (new_cd->desc_id != -1) && new_cd->mmp)
		{
			// Case 4a
			h->status = HALO_STATUS_BECOMING_HOST;
		} else
		{
			// Case 4b
			h->status = HALO_STATUS_SUB;
		}
	} else if (h->status == HALO_STATUS_CONNECTING_SUB_SWITCH)
	{
		if (new_cd->pid == -1)
			error(__FFL__,
					"[%4d] Expected connecting/switching halo ID %ld to be a sub, progenitor %ld, progenitor pid %ld desc_pid %ld.\n",
					proc, new_cd->id, h->cd.id, h->cd.pid, h->cd.desc_pid);

		// Case 5
		h->status = HALO_STATUS_SWITCHED_HOST;
	} else
	{
		error(__FFL__, "Unexpected halo status, %d, in progenitor ID %ld, desc ID %ld.\n",
				h->status, h->cd.id, new_cd->id);
	}
}

/*
 * Once we have found a host for a subhalo, we do not need to keep track of whether it switched
 * host any more. There are three possible options, the halo can stay with the same host, switch
 * to another host, or become a host itself.
 *
 * When determining whether the halo switches host, we check whether it's descendant pID matches
 * the host's descendant ID. However, there is a special case where the host merges with another
 * halo (which has the same descendant ID!).
 *
 * Note that we do NOT update the history_status field in the halo with this new status, as it is
 * meant to be temporary.
 *
 * For ghosts, we simply switch back a SWITCHING status since the switching should now have been
 * taken care of.
 */
void setHaloStatusSubhalos(Halo *sub, Halo *host, int snap_idx)
{
	if (isGhost(sub))
	{
		if (sub->status == HALO_STATUS_GHOST_SWITCHING)
			sub->status = HALO_STATUS_GHOST_SUB;
	} else
	{
		if (sub->status == HALO_STATUS_SWITCHED_HOST)
		{
			if ((sub->cd.desc_pid == -1) && (sub->cd.desc_id != -1) && sub->cd.mmp)
			{
				sub->status = HALO_STATUS_BECOMING_HOST;
			} else
			{
				sub->status = HALO_STATUS_SUB;
			}
		}

		if ((sub->cd.desc_pid != -1)
				&& ((sub->cd.desc_pid != host->cd.desc_id) || (host->cd.mmp != 1)
						|| (host->cd.desc_pid != -1)))
		{
			sub->status = HALO_STATUS_SWITCHING_HOST;
		}

		if (sub->status == HALO_STATUS_BECOMING_SUB)
			sub->status = HALO_STATUS_SUB;
	}
	debugHalo(sub, "Set subhalo status to %d.", sub->status);
}

/*
 * If this is not the most massive progenitor, the descendant will have another main branch
 * progenitor and we should NOT try to connect to the descendant.
 *
 * On the last snapshot, all halos end.
 */
void setHaloStatusSnapEnd(int snap_idx, DynArray *halos)
{
	int i, j, tt, sub_idx;
	Halo *h, *sub;
#if DO_GHOSTS
	Halo *host;
#endif

	/*
	 * Find halos that are ending at this snap. However, if we are tracking ghosts, subhalos are
	 * not ended but converted to ghosts. That can fail, however, in which case we do end the halo
	 * as we would otherwise.
	 */
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		if (isAlive(h) && (!h->cd.mmp))
		{
#if DO_GHOSTS
			if (isHost(h))
			{
				endHalo(h, snap_idx, snap_idx, HALO_ENDED_MERGED);
			} else if (isSubPermanently(h))
			{
				if (makeHaloGhost(halos, h, snap_idx) != 0)
					endHalo(h, snap_idx, snap_idx, HALO_ENDED_MERGED);
			} else if (isSub(h))
			{
				error(__FFL__,
						"At end of snapshot, halo ID %ld is ending but a non-permanent sub (status %d).\n",
						h->cd.id, h->status);
			} else
			{
				error(__FFL__,
						"At end of snapshot, halo ID %ld is ending but neither host nor sub (status %d).\n",
						h->cd.id, h->status);
			}

#else
			endHalo(h, snap_idx, snap_idx, HALO_ENDED_MERGED);
#endif
		}
	}

	/*
	 * Set the status of ghost halos, if necessary
	 */
#if DO_GHOSTS
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		if (!isGhost(h))
			continue;
		host = &(halos->h[h->host_idx]);
		setHaloStatusGhost(snap_idx, h, host);
	}
#endif

	/*
	 * Find subhalos that live in dying hosts, and set their status to switching away from these
	 * hosts. They will be removed later in this routine. We do not need to do this for ghosts.
	 */
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		if (isGhost(h))
			continue;
		if (isAlive(h) && isSubNextSnap(h) && !isAlive(&(halos->h[h->host_idx])))
			h->status = HALO_STATUS_SWITCHING_HOST;
	}

	/*
	 * Find halos that are leaving hosts, and delete their subhalo entries from the host. If a halo
	 * is becoming a host, change its status (this will delete its host_idx, so the order is
	 * important here).
	 */
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);

		if (isAlive(h) && isLeavingHost(h))
			removeSubFromHost(halos, h);

		/*
		 * When we reactivate a halo, we have to re-allocate the dynamical arrays. This is the
		 * inverse of making a halo a sub: we re-create the subpointer array. If subhalo
		 * particle tracking is on, we cannot recreate the particle tracer array because that will
		 * still exist. The function checks for such cases simply by making sure that the array
		 * isn't already allocated.
		 */
		if (isAlive(h) && isBecomingHost(h))
		{
			if (dynArrayWasFreed(&(h->subs)))
				initDynArray(__FFL__, &(h->subs), DA_TYPE_SUBPOINTER);
			h->status = HALO_STATUS_HOST;
			h->host_idx = INVALID_IDX;
			for (tt = 0; tt < NTT; tt++)
			{
				if (dynArrayWasFreed(&(h->tt[tt].tcr)))
					initDynArray(__FFL__, &(h->tt[tt].tcr), DA_TYPE_TRACER);
			}
		}
	}

	/*
	 * Deal with halos that have ended. We go through all subs in all hosts and check whether they
	 * have died. Note that this is done here at the end of the snap rather than at the time when a
	 * subhalo dies.
	 *
	 * It can also happen that both the host and sub are dying, which is why we check both hosts
	 * and dead halos.
	 */
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);

		j = 0;
		while (j < h->subs.n)
		{
			sub_idx = h->subs.sp[j].idx;
			if (sub_idx < 0)
				error(__FFL__, "In halo %ld, found subhalo %d with idx %d.\n", h->cd.id, j,
						sub_idx);
			else if (sub_idx > halos->n)
				error(__FFL__, "In halo %ld, found subhalo %d with idx %d, n %d.\n", h->cd.id, j,
						sub_idx, halos->n);

			sub = &(halos->h[sub_idx]);
			if ((sub->host_idx != i) || (sub->cd.id != h->subs.sp[j].id))
				error(__FFL__,
						"[%4d] Found non-matching subhalo record. In halo %ld, idx %d, sh idx %d, ID %ld. Sub_idx %d points to a halo ID %ld, host_idx %d.\n",
						proc, h->cd.id, i, j, h->subs.sp[j].id, sub_idx, sub->cd.id, sub->host_idx);

			if (hasEnded(sub))
				deleteDynArrayItem(__FFL__, &(h->subs), j);
			else
				j++;
		}
	}

#if CAREFUL
	/*
	 * If the host has died, all the subhalos should have been removed. If there are any left,
	 * that's an error.
	 */
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);

		if (hasEnded(h) && (h->subs.n > 0))
		{
			for (j = 0; j < h->subs.n; j++)
				output(0, "[%4d] Leftover sub %d, ID %ld, desc_id %ld.\n", proc, j,
						h->subs.sp[j].id, h->subs.sp[j].desc_id);
			error(__FFL__,
					"[%4d] Found %d leftover subhalos in halo %ld, pid %d, desc_id %ld, desc_pid %ld, status %d.\n",
					proc, h->subs.n, h->cd.id, h->cd.pid, h->cd.desc_id, h->cd.desc_pid, h->status);
		}
	}
#endif

	/*
	 * Now end halos if we have reached the last snap.
	 */
	if (snap_idx == config.n_snaps - 1)
	{
		for (i = 0; i < halos->n; i++)
		{
			h = &(halos->h[i]);
			if (isAlive(h))
				endHalo(h, snap_idx, config.n_snaps - 1, HALO_ENDED_LAST_SNAP);
		}
	}

#if CAREFUL
	if (snap_idx == config.n_snaps - 1)
	{
		for (i = 0; i < halos->n; i++)
		{
			h = &(halos->h[i]);
			if (h->last_snap < 0)
				error(__FFL__, "Found halo with last_snap < 0, ID %ld, first snap %d, status %d.\n",
						h->cd.id, h->first_snap, h->status);
		}
	}
#endif
}

int isAlive(Halo *halo)
{
	return (halo->status < HALO_STATUS_DEAD);
}

int hasEnded(Halo *halo)
{
	return (halo->status > HALO_STATUS_DEAD);
}

int isHostStatus(int_least8_t status)
{
	return (status == HALO_STATUS_HOST);
}

int isHost(Halo *halo)
{
	return ((halo->status == HALO_STATUS_HOST) || (halo->status == HALO_STATUS_GHOST_HOST));
}

int isHostOrConnecting(Halo *halo)
{
	return (isHost(halo) || (halo->status == HALO_STATUS_CONNECTING_HOST));
}

/*
 * We distinguish two cases: this function determines whether a halo is a subhalo in any
 * circumstance. The following function does the same, except in the case where the halo is a
 * sub only for one snapshot and will be a host again.
 */
int isSubStatus(int_least8_t status)
{
	return ((status == HALO_STATUS_SUB) || (status == HALO_STATUS_BECOMING_SUB)
			|| (status == HALO_STATUS_BECOMING_SUB_HOST) || (status == HALO_STATUS_BECOMING_HOST)
			|| (status == HALO_STATUS_SWITCHING_HOST) || (status == HALO_STATUS_SWITCHED_HOST)
			|| (status == HALO_STATUS_GHOST_SUB) || (status == HALO_STATUS_GHOST_SWITCHING));
}

int isSub(Halo *halo)
{
	return isSubStatus(halo->status);
}

int isSubPermanently(Halo *halo)
{
	return ((halo->status == HALO_STATUS_SUB) || (halo->status == HALO_STATUS_BECOMING_SUB)
			|| (halo->status == HALO_STATUS_BECOMING_HOST)
			|| (halo->status == HALO_STATUS_SWITCHING_HOST)
			|| (halo->status == HALO_STATUS_SWITCHED_HOST)
			|| (halo->status == HALO_STATUS_GHOST_SUB)
			|| (halo->status == HALO_STATUS_GHOST_SWITCHING));
}

int isSubNextSnap(Halo *halo)
{
	return ((halo->status == HALO_STATUS_SUB) || (halo->status == HALO_STATUS_BECOMING_SUB)
			|| (halo->status == HALO_STATUS_SWITCHING_HOST)
			|| (halo->status == HALO_STATUS_SWITCHED_HOST)
			|| (halo->status == HALO_STATUS_GHOST_SUB)
			|| (halo->status == HALO_STATUS_GHOST_SWITCHING));
}

int isSubOrConnecting(Halo *halo)
{
	return (isSub(halo) || (halo->status == HALO_STATUS_CONNECTING_SUB)
			|| (halo->status == HALO_STATUS_CONNECTING_SUB_SWITCH));
}

int isConnecting(Halo *halo)
{
	return ((halo->status == HALO_STATUS_CONNECTING_HOST)
			|| (halo->status == HALO_STATUS_CONNECTING_SUB)
			|| (halo->status == HALO_STATUS_CONNECTING_SUB_SWITCH));
}

/*
 * Note the difference between the following two functions: the first is True if a halo is becoming
 * a subhalo or switching host, i.e., any time it is entering a new host. The second function is
 * True only if the subhalo will be a subhalo for more than one snapshot.
 */
int isEnteringHost(Halo *halo)
{
	return ((halo->status == HALO_STATUS_BECOMING_SUB)
			|| (halo->status == HALO_STATUS_BECOMING_SUB_HOST)
			|| (halo->status == HALO_STATUS_SWITCHED_HOST)
			|| (halo->status == HALO_STATUS_GHOST_SWITCHING));
}

/*
 * This function is evaluated when checking whether we need to tag sub-particles. Thus, we do not
 * include switching ghosts here.
 */
int isEnteringHostPermanently(Halo *halo)
{
	return ((halo->status == HALO_STATUS_BECOMING_SUB)
			|| (halo->status == HALO_STATUS_SWITCHED_HOST));
}

int isLeavingHost(Halo *halo)
{
	return ((halo->status == HALO_STATUS_BECOMING_HOST)
			|| (halo->status == HALO_STATUS_BECOMING_SUB_HOST)
			|| (halo->status == HALO_STATUS_SWITCHING_HOST)
			|| (halo->status == HALO_STATUS_GHOST_SWITCHING));
}

int isBecomingHost(Halo *halo)
{
	return ((halo->status == HALO_STATUS_BECOMING_HOST)
			|| (halo->status == HALO_STATUS_BECOMING_SUB_HOST));
}

/*
 * Similar to the "entering host" functions, we distinguish whether a halo is becoming a subhalo
 * for one or for more snapshots.
 */
int isBecomingSub(Halo *halo)
{
	return ((halo->status == HALO_STATUS_BECOMING_SUB)
			|| (halo->status == HALO_STATUS_BECOMING_SUB_HOST));
}

int isBecomingSubPermanently(Halo *halo)
{
	return (halo->status == HALO_STATUS_BECOMING_SUB);
}

/*
 * Note that the DO_GHOSTS compile flag is not applied in this function so that it works in
 * external tools such as MORIA.
 */
int isGhostStatus(int_least8_t status)
{
	return ((status == HALO_STATUS_GHOST_HOST) || (status == HALO_STATUS_GHOST_SUB)
			|| (status == HALO_STATUS_GHOST_SWITCHING));
}

int isGhost(Halo *halo)
{
#if DO_GHOSTS
	return isGhostStatus(halo->status);
#else
	return 0;
#endif
}

/*
 * Idenfity a ghost not by a halo status but by its ID. This function is not in the ghosts unit
 * because it may be used even if ghosts are turned off at compile time, for example in MORIA.
 */
int isGhostID(ID halo_id)
{
	return (halo_id >= (ID) GHOST_ID_OFFSET);
}

/*
 * This function decides whether we need to search for particles around a halo. If we are doing
 * subhalo particle tracking, all halos can have particles. If not, permanent (!) subhalos cannot.
 */
int canHostParticles(Halo *halo)
{
#if DO_SUBHALO_TRACKING
	return 1;
#else
	return (!isSubPermanently(halo));
#endif
}

/*
 * Indicate whether a halo can have its own subhalos. This is always true for host halos, but also
 * for subhalos that are subs only temporarily.
 */
int canHostSubhalos(Halo *halo)
{
#if DO_GHOSTS
	return (!isGhost(halo));
#else
	return (!isSubPermanently(halo));
#endif
}

/*
 * This function decides whether all particles are used for the mass determination of a halo or a
 * subset. In practice, host halos and temporary subs use all particles whereas permanent subs and
 * ghosts use only their tracers.
 */
int usesAllParticles(Halo *halo)
{
	return (!isSubPermanently(halo) && !isGhost(halo));
}

/*
 * Currently, all non-permanent subs are given a mass profile because some results might want to
 * evaluate it. If other results or analyses would use the mass profile even for ghosts or permanent
 * subhalos, those cases would need to be added here.
 */
int hasMassProfile(Halo *halo)
{
#if DO_MASS_PROFILE
	return usesAllParticles(halo);
#else
	return 0;
#endif
}

int wasHostLastSnap(Halo *halo, int snap_idx)
{
	return (halo->history_status[snap_idx - 1] == HALO_STATUS_HOST);
}

int hasReachedMinimumMass(Halo *halo, int snap_idx)
{
	int i, mass_reached;

	mass_reached = 0;
	for (i = halo->first_snap; i <= snap_idx; i++)
	{
		if (halo->history_R200m[i] > 0.0)
		{
			if (haloM200m(halo, i) >= config.min_save_M200m)
			{
				mass_reached = 1;
				break;
			}
		}
	}

	return mass_reached;
}

int hasReachedMinimumMassByFinalSnap(Halo *halo)
{
	int i, mass_reached;

	mass_reached = 0;
	for (i = halo->first_snap; i <= halo->last_snap; i++)
	{
		if (halo->history_R200m[i] > 0.0)
		{
			if (haloM200m(halo, i) >= config.min_save_M200m)
			{
				mass_reached = 1;
				break;
			}
		}
	}

	return mass_reached;
}

int shouldBeSaved(Halo *halo)
{
	int tt, rs, ret;

	if (isAlive(halo))
		return 0;

	for (tt = 0; tt < NTT; tt++)
	{
		for (rs = 0; rs < NRS; rs++)
		{
			if (halo->tt[tt].rs[rs].n < config.output_min_results[tt][rs])
			{
				debugHalo(halo,
						"Will not be saved because min. number of %s results (%d) not reached (%d).",
						rsprops[rs].name_long, config.output_min_results[tt][rs],
						halo->tt[tt].rs[rs].n);
				return 0;
			}
		}
	}

	ret = hasReachedMinimumMassByFinalSnap(halo);

	return ret;
}

/*************************************************************************************************
 * FUNCTIONS - MASS PROFILE, UNIT CONVERSIONS
 *************************************************************************************************/

HaloID haloOriginalID(Halo *halo)
{
	return halo->history_id[halo->first_snap];
}

/*
 * Convert a physical radius in kpc/h to a comoving radius in Mpc/h
 */
float comovingRadius(float r_phys, int snap_idx)
{
	return r_phys * config.snap_com_conv[snap_idx];
}

/*
 * Convert a comoving radius in Mpc/h to a physical radius in kpc/h
 */
float physicalRadius(float r_com, int snap_idx)
{
	return r_com * config.snap_phys_conv[snap_idx];
}

/*
 * Convenience function since the way R200m and M200m are stored is not very intuitive. Returns the
 * halo radius in physical kpc/h.
 */
float haloR200m(Halo *halo, int snap_idx)
{
	return halo->history_R200m[snap_idx];
}

/*
 * The same as haloR200m, but returning comoving Mpc/h.
 */
float haloR200mComoving(Halo *halo, int snap_idx)
{
	return comovingRadius(halo->history_R200m[snap_idx], snap_idx);
}

float haloM200m(Halo *halo, int snap_idx)
{
	return soRtoM(haloR200m(halo, snap_idx), config.snap_rho_200m[snap_idx]);
}

float haloMassRatio(Halo *host, Halo *sub)
{
	return sub->M_bound_peak / host->cd.M_bound;
}

/*
 * Get the position of the halo. This is a little more complicated because the history is not
 * necessarily stored for all snapshots.
 *
 * When a halo ends, the history fields are rearranged before writing them to file. Thus, the
 * function will fail if called after a halo has ended.
 */
float* haloX(Halo *halo, int snap_idx, int current_snap_idx)
{
	if (snap_idx == current_snap_idx)
	{
		return halo->cd.x;
	} else if (snap_idx < current_snap_idx)
	{
#if PARANOID
		if (hasEnded(halo))
			error(__FFL__, "Tried to access position history of an ended halo, ID %ld.\n",
					halo->cd.id);
#endif

		int hist_idx;

		hist_idx = snap_idx - current_snap_idx + STCL_HALO_X;
		if (hist_idx < 0)
			error(__FFL__,
					"Requested halo position for snapshot %d, before history (first snap %d).\n",
					snap_idx, current_snap_idx - STCL_HALO_X);
		return halo->history_x[hist_idx];
	} else
	{
		error(__FFL__,
				"Requested halo position for snapshot %d which is in the future (current snap %d).\n",
				snap_idx, current_snap_idx);
	}

	return NULL;
}

float* haloV(Halo *halo, int snap_idx, int current_snap_idx)
{
	if (snap_idx == current_snap_idx)
	{
		return halo->cd.v;
	} else if (snap_idx < current_snap_idx)
	{
#if PARANOID
		if (hasEnded(halo))
			error(__FFL__, "Tried to access velocity history of an ended halo, ID %ld.\n",
					halo->cd.id);
#endif

		int hist_idx;

		hist_idx = snap_idx - current_snap_idx + STCL_HALO_V;
		if (hist_idx < 0)
			error(__FFL__,
					"Requested halo velocity for snapshot %d, before history (first snap %d).\n",
					snap_idx, current_snap_idx - STCL_HALO_V);
		return halo->history_v[hist_idx];
	} else
	{
		error(__FFL__,
				"Requested halo velocity for snapshot %d which is in the future (current snap %d).\n",
				snap_idx, current_snap_idx);
	}

	return NULL;
}

int posInsideHalo(int snap_idx, Halo *halo, float *x)
{
	return (tracerRadius(snap_idx, halo->cd.x, x) <= haloR200m(halo, snap_idx));
}

/*
 * Return the total memory taken up by a halo and the dynamic arrays it contains.
 */
long int haloMemory(Halo *h)
{
	int tt, rs, al;
	long int total;

	total = sizeof(Halo);
	total += h->subs.size * h->subs.n_alloc;
	for (tt = 0; tt < NTT; tt++)
	{
		total += h->tt[tt].tcr.size * h->tt[tt].tcr.n_alloc;
		total += h->tt[tt].iid.size * h->tt[tt].iid.n_alloc;
		for (rs = 0; rs < NRS; rs++)
			total += h->tt[tt].rs[rs].size * h->tt[tt].rs[rs].n_alloc;
	}
	for (al = 0; al < NAL; al++)
		total += h->al[al].size * h->al[al].n_alloc;

	return total;
}
