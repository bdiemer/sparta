/*************************************************************************************************
 *
 * This unit keeps track of host-subhalo relations.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _HALO_SUBS_H_
#define _HALO_SUBS_H_

#include "../global_types.h"
#include "../statistics.h"
#include "../tree.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initSubPointer(SubPointer *subhalo);
SubPointer* addSubToHost(Halo *host, Halo *sub, int sub_idx, int snap_idx);
void removeSubFromHost(DynArray *halos, Halo *sub);
void advanceSubhalos(int snap_idx, DynArray *halos);
void hostSubRelations(DynArray *halos, Tree *tree, int snap_idx, LocalStatistics *ls);

#endif
