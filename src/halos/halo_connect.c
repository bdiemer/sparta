/*************************************************************************************************
 *
 * This unit connects halos between snapshots.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "halo_connect.h"
#include "halo.h"
#include "halo_ghosts.h"
#include "../config.h"
#include "../memory.h"
#include "../utils.h"
#include "../lb/domain.h"
#include "../io/io_halocat.h"
#include "../tracers/tracer_subhalos.h"
#include "../tracers/tracers.h"

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void updateHaloData(Halo *h, int snap_idx, HaloCatalogData *cd_new);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Gather number of requested halos, then exchange requested halo IDs. Main reads the catalog and
 * returns the requested and new halos to each proc.
 *
 * Note: after the beginning of the function, the order of halos and their subhalos must not be
 * changed! None of the functions called can, for example, delete halos or subhalos from the
 * dynamic arrays.
 */
void connectHalos(int snap_idx, DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs)
{
	int i, n_received, n_requested[n_proc], displacements[n_proc], current_proc, proc_count[n_proc],
			n_halos_previous, is_sho_tcr;
	int n_requested_all = 0;
	double current_time;
	HaloID *request_ids = NULL, *idp = NULL;
	HaloRequest *requests;
	HaloCatalogData *cat_data_recv;
	Halo *temp_halo, *h;
	DynArray connect_ids;
	DynArray da_cd;

	/*
	 * First and foremost, sort the halo array by descendant ID. We'll keep indices to halos, so
	 * we cannot change the order after this point.
	 */
	n_halos_previous = halos->n;
	sortHalos(halos, 1);
	temp_halo = (Halo*) memAlloc(__FFL__, MID_CATREADING, sizeof(Halo));

	/*
	 * Generate connection requests for all tracked host and subhalos
	 */
	output(4, "[%4d] [SN %3d] Generating connection requests...\n", proc, snap_idx);
	initDynArray(__FFL__, &connect_ids, DA_TYPE_ID);
	for (i = 0; i < halos->n; i++)
	{
		if (isConnecting(&(halos->h[i])))
		{
			h = &(halos->h[i]);

			idp = addId(__FFL__, &connect_ids);
			*idp = h->cd.desc_id;
			debugHalo(h, "Adding to connection requests.");
		}
	}

#if DO_TRACER_SUBHALOS
	Tracer *tcr;
	DynArray shRequests;
	TracerRequest *treq, temp_treq;
	int_least8_t *connect_id_mask;
	int treq_idx, j;
	size_t connect_id_mask_size;

	/*
	 * Add the connection requests for tracked subhalos, not worrying about duplicate IDs. We will
	 * fix those later.
	 */
	initDynArray(__FFL__, &shRequests, DA_TYPE_TREQ);
	for (i = 0; i < halos->n; i++)
	{
		for (j = 0; j < halos->h[i].tt[SHO].tcr.n; j++)
		{
			/*
			 * Set the connect status for all subhalo tracers, regardless of whether they are
			 * catalog halos or ghosts. If they are not found, we need to throw an error.
			 */
			tcr = &(halos->h[i].tt[SHO].tcr.tcr[j]);
#if CAREFUL
			if (tcr->status != TCR_STATUS_ACTIVE)
				error(__FFL__, "Found tracer subhalo with invalid status %d, ID %ld in halo %ld.\n",
						tcr->status, tcr->id, halos->h[i].cd.id);
#endif
			tcr->status = TCR_STATUS_CONNECT;

			/*
			 * If the subhalo has become a ghost, we delete the subhalo tracer. It would be nice to
			 * follow them, but this causes a number of serious issues. First, we need to find the
			 * ghosts in the halo array below (as opposed to in the catalog requests), which would
			 * mean sorting the halo array. Second, the position of the ghost will not be known
			 * until it is processed much later -- and there is no guarantee that its host halo
			 * will not be processed first, meaning it would analyze its tracers before the ghost
			 * position is known at all. Thus, we simply delete the ghost tracer here.
			 */
#if DO_GHOSTS
			if (isGhostID(tcr->desc_id))
			{
				debugTracer(&(halos->h[i]), tcr, "Has become ghost, deleting subhalo tracer.");
				tcr->status = TCR_STATUS_DELETE;
				ls->tt[SHO].n[TCRSTAT_HALO_BECAME_GHOST]++;
				continue;
			}
#endif

			idp = addId(__FFL__, &connect_ids);
			*idp = tcr->desc_id;
			treq = addTracerRequest(__FFL__, &shRequests);
			treq->id = tcr->desc_id;
			treq->halo_idx = i;
			treq->tcr_idx = j;
			debugTracer(&(halos->h[i]), tcr, "Adding to catalog requests.");
		}
	}

	/*
	 * Now we must remove duplicate connection IDs, as those would lead to confusion later. We also
	 * sort the subhalo connection requests so we can find them later.
	 */
	qsort(shRequests.treq, shRequests.n, sizeof(TracerRequest), &compareTracerRequests);
	qsort(connect_ids.ids, connect_ids.n, sizeof(ID), &compareLongsAscending);
	connect_id_mask_size = sizeof(int_least8_t) * connect_ids.n;
	connect_id_mask = (int_least8_t*) memAlloc(__FFL__, MID_CATREADING, connect_id_mask_size);
	connect_id_mask[0] = 0;
	for (i = 0; i < connect_ids.n - 1; i++)
		connect_id_mask[i + 1] = (connect_ids.ids[i] == connect_ids.ids[i + 1]);
	deleteDynArrayItems(__FFL__, &connect_ids, connect_id_mask);
	memFree(__FFL__, MID_CATREADING, connect_id_mask, connect_id_mask_size);
#endif

#if PARANOID
	qsort(connect_ids.ids, connect_ids.n, sizeof(ID), &compareLongsAscending);
	for (i = 0; i < connect_ids.n - 1; i++)
	{
		if (connect_ids.ids[i] == connect_ids.ids[i + 1])
			error(__FFL__, "Found same connect ID %ld twice.\n", connect_ids.ids[i]);
	}
#endif

	/*
	 * Send / receive number of requested halos from this proc. Allocate memory for halo IDs
	 */
	output(4, "[%4d] [SN %3d] Sending %d requests\n", proc, snap_idx, connect_ids.n);
	MPI_Gather(&(connect_ids.n), 1, MPI_INT, n_requested, 1, MPI_INT, MAIN_PROC, comm);

	if (is_main_proc)
	{
		n_requested_all = 0;
		for (i = 0; i < n_proc; i++)
		{
			displacements[i] = n_requested_all;
			output(3, "[Main] [SN %3d] Proc %d sent %d connects, displacement %d.\n", snap_idx, i,
					n_requested[i], displacements[i]);
			n_requested_all += n_requested[i];
		}
		request_ids = (HaloID*) memAlloc(__FFL__, MID_CATREADING, sizeof(HaloID) * n_requested_all);
		output(2, "[Main] [SN %3d] Received %d halo connection requests.\n", snap_idx,
				n_requested_all);
	}

	/*
	 * Send / receive requested halo IDs
	 */
	MPI_Gatherv(connect_ids.ids, connect_ids.n, MPI_LONG, request_ids, n_requested, displacements,
			MPI_LONG, MAIN_PROC, comm);
	freeDynArray(__FFL__, &connect_ids);

	current_time = MPI_Wtime();
	ls->timers_1[T1_CONNECTREQUESTS] += current_time;
	ls->timers_1[T1_READCATS] -= current_time;

	/*
	 * Read halo catalog, fill requests
	 */
	if (is_main_proc)
	{
		output(4, "[Main] [SN %3d] Allocating %d halo requests.\n", snap_idx, n_requested_all);
		requests = (HaloRequest*) memAlloc(__FFL__, MID_CATREADING,
				sizeof(HaloRequest) * n_requested_all);
		current_proc = 0;
		for (i = 0; i < n_requested_all; i++)
		{
			if (request_ids[i] <= 0)
				error(__FFL__, "Got request ID 0, idx %d from proc %d.\n", i, current_proc);

			while ((current_proc < n_proc - 1) && (i >= displacements[current_proc + 1]))
				current_proc++;
			requests[i].id = request_ids[i];
			requests[i].proc = current_proc;
			requests[i].status = HALO_REQUEST_STATUS_OPEN;
		}
		memFree(__FFL__, MID_CATREADING, request_ids, sizeof(HaloID) * n_requested_all);
		readHaloCatalog(snap_idx, requests, n_requested_all, &da_cd);
		memFree(__FFL__, MID_CATREADING, requests, sizeof(HaloRequest) * n_requested_all);

		output(2, "[Main] [SN %3d] Connected %d halos, found %d new halos.\n", snap_idx,
				n_requested_all, da_cd.n - n_requested_all);

		for (i = 0; i < da_cd.n; i++)
		{
			if (da_cd.cd[i].proc == INVALID_PROC)
			{
				da_cd.cd[i].proc = procFromPosition(da_cd.cd[i].x);
				debugHaloCatalogData(1, &(da_cd.cd[i]),
						"Had process -1 (unknown), assigned to process %d.", da_cd.cd[i].proc);
			}
			if (da_cd.cd[i].proc == INVALID_PROC)
				error(__FFL__, "Could not find proc for position %.2f, %.2f, %.2f.\n",
						da_cd.cd[i].x[0], da_cd.cd[i].x[1], da_cd.cd[i].x[2]);

		}
		qsort(da_cd.cd, da_cd.n, sizeof(HaloCatalogData), &compareHaloCatalogDataByProc);
		for (i = 0; i < n_proc; i++)
			proc_count[i] = 0;
		for (i = 0; i < da_cd.n; i++)
		{
			proc_count[da_cd.cd[i].proc]++;
			debugHaloCatalogData(1, &(da_cd.cd[i]), "Sending to process %d, halo idx %d.",
					da_cd.cd[i].proc, i);
		}
		displacements[0] = 0;
		for (i = 0; i < n_proc; i++)
		{
			if (i > 0)
				displacements[i] = displacements[i - 1] + proc_count[i - 1];
			output(4, "[Main] [SN %3d] Assigned %d halos to process %d.\n", snap_idx, proc_count[i],
					i);
		}
	}

	/*
	 * Send / receive number of halos found, allocate memory for the halo data. The timing call
	 * is placed after the MPI call so that all procs are synched.
	 */
	MPI_Scatter(proc_count, 1, MPI_INT, &n_received, 1, MPI_INT, MAIN_PROC, comm);
	current_time = MPI_Wtime();
	ls->timers_1[T1_READCATS] += current_time;
	ls->timers_1[T1_CONNECT] -= current_time;

	/*
	 * Send / receive halo data. Since we're sending bytes, we need to convert into HaloCatalogData units
	 */
	cat_data_recv = (HaloCatalogData*) memAlloc(__FFL__, MID_CATREADING,
			sizeof(HaloCatalogData) * n_received);
	if (is_main_proc)
	{
		for (i = 0; i < n_proc; i++)
		{
			output(4, "[Main] [SN %3d] Preparing to send halo count to procs.\n", snap_idx);
			proc_count[i] *= sizeof(HaloCatalogData);
			displacements[i] *= sizeof(HaloCatalogData);
		}
	}
	MPI_Scatterv(da_cd.cd, proc_count, displacements, MPI_BYTE, cat_data_recv,
			n_received * sizeof(HaloCatalogData), MPI_BYTE, MAIN_PROC, comm);

	/*
	 * On the main proc, save some statistics for later and free the cat data array
	 */
	if (is_main_proc)
	{
		output(4, "[Main] [SN %3d] Sent halo count to procs, freeing catalog data.\n", snap_idx);
		(*gs).n_halos_connected = n_requested_all;
		(*gs).n_halos_new = da_cd.n - n_requested_all;
		freeDynArray(__FFL__, &da_cd);
	}

	/*
	 * Match halos and create new ones where necessary
	 */
	for (i = 0; i < n_received; i++)
	{
		debugHaloCatalogData(0, &(cat_data_recv[i]), "Received by process, idx %d.", i);

		/*
		 * If we are tracking subhalos, we need to first check whether this dataset matches one
		 * or multiple of them. In that case, no new halo should be created if no halo matches the
		 * dataset.
		 */
		is_sho_tcr = 0;
#if DO_TRACER_SUBHALOS
		temp_treq.id = cat_data_recv[i].id;
		treq = (TracerRequest*) bsearch(&temp_treq, shRequests.treq, shRequests.n,
				sizeof(TracerRequest), &compareTracerRequests);
		if (treq != NULL)
			is_sho_tcr = 1;
#endif

		/*
		 * Search for this halo
		 */
		temp_halo->id_sort = cat_data_recv[i].id;
		h = (Halo*) bsearch(temp_halo, halos->h, n_halos_previous, sizeof(Halo), &compareHalos);

#if DO_GHOSTS && CAREFUL
		if ((h != NULL) && (isGhost(h)))
			error(__FFL__, "Trying to connect to ghost ID %ld, received ID %ld.\n", h->cd.id,
					cat_data_recv[i].id);
#endif

		/*
		 * If h == NULL, the halo does not exist yet and we have to create a new one. However,
		 * even if we have found a match in the halo array, we have to be careful: in rare cases,
		 * a halo can have been destroyed since the sorted array was created. In that case, the
		 * halo history is disrupted, and we should create a new halo, just as if no progenitor
		 * halo had been found.
		 *
		 * Also, if in checking mode, let's make sure the MAIN proc sent us a halo that is
		 * actually in our volume.
		 */
		if ((!is_sho_tcr) && ((h == NULL) || (!isAlive(h))))
		{
			if (h == NULL)
			{
				debugHaloCatalogData(0, &(cat_data_recv[i]),
						"Does not exist yet, adding as new halo at position xyz %.4f %.4f %.4f.",
						cat_data_recv[i].x[0], cat_data_recv[i].x[1], cat_data_recv[i].x[2]);
			} else
			{
				debugHaloCatalogData(0, &(cat_data_recv[i]),
						"Adding as new halo at position xyz %.2f %.2f %.2f due to status %d.",
						cat_data_recv[i].x[0], cat_data_recv[i].x[1], cat_data_recv[i].x[2],
						h->status);
			}

			h = addHalo(__FFL__, halos, 1);
			h->first_snap = snap_idx;
			setHaloStatusInitial(h, &(cat_data_recv[i]));
			debugHaloCatalogData(0, &(cat_data_recv[i]), "Set initial status %d.", h->status);
		} else if (h != NULL)
		{
			debugHaloCatalogData(0, &(cat_data_recv[i]),
					"Setting status, current status %d, prog ID %ld, pid %ld, desc_id %ld, desc_pid %ld, req status %d.",
					h->status, h->cd.id, cat_data_recv[i].pid, cat_data_recv[i].desc_id,
					cat_data_recv[i].desc_pid, cat_data_recv[i].request_status);
			setHaloStatusConnect(snap_idx, h, &(cat_data_recv[i]));
			debugHaloCatalogData(0, &(cat_data_recv[i]), "Set status %d.", h->status);
		}

		/*
		 * Write the new catalog data into the halo, and update its history. Note that, in exotic
		 * cases such as jumps, the have can have ended here; however, if we do not call the update
		 * function, the halo record remains faulty which can lead to issues with host-sub
		 * relations and so on. Thus, we do execute the update step, but we are careful with the
		 * status field which now has a different meaning for ended halos.
		 */
		if (h != NULL)
			updateHaloData(h, snap_idx, &(cat_data_recv[i]));
	}

	/*
	 * "Connect" ghosts. In particular, we need to:
	 *
	 * - switch ID to previously generated ghost descendant ID
	 * - set a new descendant ID for the next snapshot
	 * - switch the parent ID accordingly
	 *
	 * Note that we cannot yet set the descendant parent ID. We could look for the host at this
	 * point, but that is done anyway when assigning host-sub relations later, so we fix it at
	 * that point.
	 *
	 * From this point on, it is understood that the other catalog data (x, v, R200m_all_com,
	 * M_bound) are outdated and need to be reconstructed from the ghost particles.
	 */
#if DO_GHOSTS
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		if (!isGhost(h))
			continue;

		h->cd.id = h->cd.desc_id;
		h->cd.desc_id = createGhostID(h, snap_idx + 1);
		h->cd.pid = h->cd.desc_pid;

		updateHaloData(h, snap_idx, NULL);
	}
#endif

	/*
	 * Connect subhalo tracers. This could not be done within the previous for-loop because the
	 * host halos need to be updated first for the trajectory computations.
	 */
#if DO_TRACER_SUBHALOS

	/*
	 * First, look through the received halos and assign them to catalog requests.
	 */
	for (i = 0; i < n_received; i++)
	{
		temp_treq.id = cat_data_recv[i].id;
		treq = (TracerRequest*) bsearch(&temp_treq, shRequests.treq, shRequests.n,
				sizeof(TracerRequest), &compareTracerRequests);

		if (treq != NULL)
		{
			/*
			 * There could be multiple matching requests for this subhalo, and the bsearch function
			 * could return any one of them. Thus, we make sure to jump to the first match and then
			 * continue upwards in the list.
			 */
			treq_idx = treq - shRequests.treq;
			while ((treq_idx > 0) && (shRequests.treq[treq_idx - 1].id == cat_data_recv[i].id))
				treq_idx--;
			while ((treq_idx < shRequests.n)
					&& (shRequests.treq[treq_idx].id == cat_data_recv[i].id))
			{
				treq = &(shRequests.treq[treq_idx]);
				h = &(halos->h[treq->halo_idx]);
				tcr = &(h->tt[SHO].tcr.tcr[treq->tcr_idx]);
				connectSubhalo(snap_idx, h, tcr, &(cat_data_recv[i]), &(ls->tt[SHO]));
				treq_idx++;
			}
		}
	}
	freeDynArray(__FFL__, &shRequests);

	/*
	 * We check the status; if it is still CONNECTING, we have not found a match and throw an
	 * error.
	 */
	for (i = 0; i < halos->n; i++)
	{
		for (j = 0; j < halos->h[i].tt[SHO].tcr.n; j++)
		{
			tcr = &(halos->h[i].tt[SHO].tcr.tcr[j]);

			if (tcr->status == TCR_STATUS_CONNECT)
				error(__FFL__, "Descendant for tracer subhalo ID %ld was not found.\n", tcr->id);
			debugTracer(&(halos->h[i]), tcr, "Found after connecting.");
		}
	}
#endif

	memFree(__FFL__, MID_CATREADING, cat_data_recv, sizeof(HaloCatalogData) * n_received);
	memFree(__FFL__, MID_CATREADING, temp_halo, sizeof(Halo));
	output(3, "[%4d] [SN %3d] Received %d halos from main, %d total.\n", proc, snap_idx, n_received,
			halos->n);

	/*
	 * Check that all halos that needed to be matched were found. Note that this should fail
	 * regardless of the err_level_req_not_found parameter, because such fails should be detected by
	 * main while reading the catalog.
	 */
#if CAREFUL
	for (i = 0; i < halos->n; i++)
	{
		if (isConnecting(&(halos->h[i])))
			error(__FFL__,
					"[%4d] [SN %3d] Internal error: Descendant ID %ld not found, halo ID %ld, idx %d/%d, n_sorted %d, status %d.\n",
					proc, snap_idx, halos->h[i].cd.desc_id, halos->h[i].cd.id, i, halos->n,
					n_halos_previous, halos->h[i].status);
	}
#endif
}

/*
 * Move the halo one snapshot forward in time. This function can take a new catalog data set or
 * NULL, as is the case for ghosts that have no equivalent in the catalog. Otherwise, we write the
 * new catalog data into the halo, and update its history.
 *
 * Note that the R200m set here is the catalog radius. For subhalos, we keep the previous radius,
 * that is, the radius at accretion, and we are not determining R200m from the particle
 * distribution. For host halos, the field will be overwritten later. This has profound consequences
 * when selecting those halos that will be saved to the output file: the algorithm prevents mass
 * growth once halos are subhalos, meaning that any mass the "accrete" as subhalos is deemed to be
 * unphysical and will not be counted when deciding which halos to output.
 *
 * For ghosts, we set the previous R200m at this point as an initial guess. Similarly, x and v
 * refer to the previous snapshot. M_bound_peak is not being evolved any more, but that does not
 * matter since it should never be used for ghosts anyway. Wherever possible, these quantities will
 * be corrected later.
 */
void updateHaloData(Halo *h, int snap_idx, HaloCatalogData *cd_new)
{
	int d, j;

	/*
	 * Note that the history fields run up to the previous snapshot, not the current one. Thus, we
	 * store the current values in the last field before overwriting them with this new snapshot.
	 */
	for (d = 0; d < 3; d++)
	{
		for (j = 0; j < STCL_HALO_X - 1; j++)
			h->history_x[j][d] = h->history_x[j + 1][d];
		for (j = 0; j < STCL_HALO_V - 1; j++)
			h->history_v[j][d] = h->history_v[j + 1][d];
		h->history_x[STCL_HALO_X - 1][d] = h->cd.x[d];
		h->history_v[STCL_HALO_V - 1][d] = h->cd.v[d];
	}

	if (cd_new != NULL)
		memcpy(&(h->cd), cd_new, sizeof(HaloCatalogData));

	/*
	 * Set those fields that have a simple historical snapshot order. Note that there is something
	 * special about the status here: the halo can, in exotic cases, have ended during the catalog
	 * step. In this case, we should NOT write its current status into the history field because it
	 * is actually its final status.
	 */
	if (!isAlive(h))
		return;

	h->history_status[snap_idx] = h->status;
	h->history_id[snap_idx] = h->cd.id;
#if OUTPUT_HALO_PARENT_ID
	h->history_pid[snap_idx] = h->cd.pid;
#endif

	if (cd_new != NULL)
	{
		/*
		 * For subhalos, we set the radius at the previous snapshot, but that can be overwritten by
		 * a more accurate determination later. Otherwise, we set the catalog radius with the
		 * expectation that it will be corrected.
		 */
		if (isSub(h) && (h->first_snap < snap_idx))
		{
			h->history_R200m[snap_idx] = h->history_R200m[snap_idx - 1];
#if PARANOID
			if (h->history_R200m[snap_idx] <= 0.0)
				error(__FFL__, "Trying to set zero radius from previous snapshot in halo ID %ld.\n",
						h->cd.id);
#endif
		} else
		{
			h->history_R200m[snap_idx] = physicalRadius(h->cd.R200m_cat_com, snap_idx);
		}
		h->M_bound_peak = fmax(h->M_bound_peak, h->cd.M_bound);

		debugHalo(h, "Connected, updated halo data, xyz %.4f %.4f %.4f.", h->cd.x[0], h->cd.x[1], h->cd.x[2]);
	} else
	{
		h->history_R200m[snap_idx] = h->history_R200m[snap_idx - 1];
		h->cd.R200m_cat_com = INVALID_R;
		h->cd.M_bound = INVALID_M;

		debugHalo(h,
				"Updated halo data for ghost; desc_pid is not valid at this point; guessed position %.1f %.1f %.1f.",
				h->cd.x[0], h->cd.x[1], h->cd.x[2]);
	}

#if CAREFUL
	if ((h->cd.id <= 0) || ((h->cd.desc_id <= 0) && (snap_idx != config.n_snaps - 1)))
		error(__FFL__, "Received invalid halo ID / Desc ID 0.\n");
#endif
}

