/*************************************************************************************************
 *
 * This unit implements the halo definition type.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _HALO_DEFINITIONS_H_
#define _HALO_DEFINITIONS_H_

#include "../global_types.h"
#include "../cosmology.h"

/*************************************************************************************************
 * HALO MASS DEFINITIONS
 *************************************************************************************************/

/*
 * Halo boundary definition type.
 *
 * Note that the quantity (R, M, Vcirc) and the bound state (bound or all) can be unknown, in which
 * case they are omitted from string conversions.
 *
 * The idx field can be useful when the index of a certain mass definition in some array needs to
 * be determined many times. Flag is a general-purpose variable that can be used as the user sees
 * fit.
 */
typedef struct HaloDefinition
{
	/*
	 * Fields that describe the mass definition and are translated to/from strings.
	 */
	int_least8_t quantity;
	int_least8_t type;
	int_least8_t subtype;
	int_least8_t time;
	int_least8_t ptl_select;
	int_least8_t source;
	int_least8_t is_error;
	int_least8_t flags;

	/*
	 * Fields that are used internally.
	 */
	int_least8_t do_output;
	int_least8_t do_convert;

	int idx;

	union
	{
		float overdensity;
		float percentile;
		float linking_length;
	};
} HaloDefinition;

enum
{
	HDEF_Q_UNKNOWN, HDEF_Q_RADIUS, HDEF_Q_MASS, HDEF_Q_PEAKHEIGHT, HDEF_Q_VCIRC, HDEF_Q_N
};

enum
{
	HDEF_TYPE_INVALID,
	HDEF_TYPE_SO,
	HDEF_TYPE_SPLASHBACK,
	HDEF_TYPE_FOF,
	HDEF_TYPE_VMAX,
	HDEF_TYPE_ORBITING,
	HDEF_TYPE_N
};

enum
{
	HDEF_SUBTYPE_SO_MATTER, HDEF_SUBTYPE_SO_CRITICAL, HDEF_SUBTYPE_SO_VIRIAL, HDEF_SUBTYPE_SO_N
};

enum
{
	HDEF_SUBTYPE_SP_SLOPE_PROFILE,
	HDEF_SUBTYPE_SP_SLOPE_MEDIAN,
	HDEF_SUBTYPE_SP_SLOPE_SHELL,
	HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN,
	HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE,
	HDEF_SUBTYPE_SP_APOCENTER_SHELL_MEAN,
	HDEF_SUBTYPE_SP_APOCENTER_SHELL_PERCENTILE,
	HDEF_SUBTYPE_SP_N
};

#define HDEF_SUBTYPE_FOF_N 1
#define HDEF_SUBTYPE_VMAX_N 1

enum
{
	HDEF_SUBTYPE_ORB_ALL, HDEF_SUBTYPE_ORB_PERCENTILE, HDEF_SUBTYPE_ORB_N
};

enum
{
	HDEF_TIME_NOW, HDEF_TIME_ACC, HDEF_TIME_PEAK, HDEF_TIME_N
};

enum
{
	HDEF_PTLSEL_DEFAULT,
	HDEF_PTLSEL_ALL,
	HDEF_PTLSEL_BOUND,
	HDEF_PTLSEL_TRACER,
	HDEF_PTLSEL_ORBITING,
	HDEF_PTLSEL_N
};

enum
{
	HDEF_SOURCE_ANY, HDEF_SOURCE_CATALOG, HDEF_SOURCE_SPARTA, HDEF_SOURCE_SHELLFISH, HDEF_SOURCE_N
};

enum
{
	HDEF_ERR_NO, HDEF_ERR_1SIGMA, HDEF_ERR_2SIGMA, HDEF_ERR_N
};

#define DEFAULT_HALO_DEF_STRLEN 30

/*
 * The following list are the return values from the compareHaloDefinitions() function. They are
 * checked in the following order, and the first difference (if any) is returned:
 *
 * HDEF_DIFF_TYPE:       different type
 * HDEF_DIFF_SUBTYPE:    different subtype
 * HDEF_DIFF_NUMBER:     different number (SO threshold, percentile, linking length etc)
 * HDEF_DIFF_PROPERTIES: different properties (bound type, time, source, error)
 * HDEF_DIFF_QUANTITY:   different quantity (radius / mass etc)
 * HDEF_DIFF_NONE:       the definitions are the same
 *
 * Note that the quantity is returned last because it is often exchangable. Thus, if
 * HDEF_DIFF_QUANTITY is returned, we know that the definitions are the same except for the
 * quantity. Similarly, the user can check the size of the return value; if it is greater or
 * equal than a certain number, that implies that the differences between the definitions are
 * less important than that value.
 */
enum
{
	HDEF_DIFF_TYPE,
	HDEF_DIFF_SUBTYPE,
	HDEF_DIFF_NUMBER,
	HDEF_DIFF_PROPERTIES,
	HDEF_DIFF_QUANTITY,
	HDEF_DIFF_NONE
};

/*
 * The following flag values can be set as logical | in the flag field, they must have values of
 * 1, 2, 4 etc. Their meaning is:
 *
 * HDEF_FLAG_INTERNAL   Internally used value
 */
#define HDEF_FLAG_INTERNAL 1

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initHaloDefinition(HaloDefinition *def);
HaloDefinition getHaloDefinitionSO(int overdensity_type, float overdensity);
void haloDefinitionToString(HaloDefinition def, char *str);
HaloDefinition stringToHaloDefinition(char *str, int allow_errors);
void haloDefinitionToLabelQuantity(HaloDefinition *def, char *str);
void haloDefinitionToLabelType(HaloDefinition *def, char *str);
int haloDefinitionHasNumber(HaloDefinition *def);
int compareHaloDefinitions(HaloDefinition a, HaloDefinition b);
int compareSOThresholds(HaloDefinition a, HaloDefinition b);
float densityThreshold(Cosmology *cosmo, float z, HaloDefinition *def);

#endif
