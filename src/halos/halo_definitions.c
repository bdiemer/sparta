/*************************************************************************************************
 *
 * This unit implements the halo definition type.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>

#include "halo_definitions.h"
#include "halo_so.h"
#include "../constants.h"
#include "../../src/utils.h"

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int haloDefinitionHasNumber(HaloDefinition *def);

/*************************************************************************************************
 * FUNCTIONS - HALO DEFINITION TYPE
 *************************************************************************************************/

void initHaloDefinition(HaloDefinition *def)
{
	def->quantity = HDEF_Q_UNKNOWN;
	def->type = HDEF_TYPE_INVALID;
	def->subtype = HDEF_TYPE_INVALID;
	def->time = HDEF_TIME_NOW;
	def->ptl_select = HDEF_PTLSEL_DEFAULT;
	def->source = HDEF_SOURCE_ANY;
	def->is_error = HDEF_ERR_NO;
	def->overdensity = 0.0;
	def->idx = -1;
	def->flags = 0;

	def->do_output = 0;
	def->do_convert = 0;
}

HaloDefinition getHaloDefinitionSO(int overdensity_type, float overdensity)
{
	HaloDefinition def;

	initHaloDefinition(&def);
	def.type = HDEF_TYPE_SO;
	def.subtype = overdensity_type;
	def.overdensity = overdensity;

	return def;
}

/*
 * Returns a mass definition identifier, such as "R500c_all" or "M200m_bnd".
 */
void haloDefinitionToString(HaloDefinition def, char *str)
{
	/*
	 * Start with the radius/mass identifier. This variable can be unknown, in which case we
	 * output a general string without it.
	 */
	switch (def.quantity)
	{
	case HDEF_Q_UNKNOWN:
		sprintf(str, "%s", "");
		break;
	case HDEF_Q_RADIUS:
		sprintf(str, "%s", "R");
		break;
	case HDEF_Q_MASS:
		sprintf(str, "%s", "M");
		break;
	case HDEF_Q_PEAKHEIGHT:
		sprintf(str, "%s", "nu");
		break;
	case HDEF_Q_VCIRC:
		sprintf(str, "%s", "V");
		break;
	default:
		error(__FFL__, "Invalid halo quantity mass/radius/v, %d.\n", def.quantity);
		break;
	}

	/*
	 * Now we distinguish overdensity types and other definitions
	 */
	switch (def.type)
	{

	case HDEF_TYPE_SO:
		switch (def.subtype)
		{
		case HDEF_SUBTYPE_SO_MATTER:
			sprintf(str, "%s%.0fm", str, def.overdensity);
			break;
		case HDEF_SUBTYPE_SO_CRITICAL:
			sprintf(str, "%s%.0fc", str, def.overdensity);
			break;
		case HDEF_SUBTYPE_SO_VIRIAL:
			sprintf(str, "%svir", str);
			break;
		default:
			error(__FFL__, "Invalid halo definition SO subtype, %d.\n", def.subtype);
			break;
		}
		break;

	case HDEF_TYPE_SPLASHBACK:
		sprintf(str, "%ssp", str);
		switch (def.subtype)
		{
		case HDEF_SUBTYPE_SP_SLOPE_PROFILE:
			sprintf(str, "%s-slp-prf", str);
			break;
		case HDEF_SUBTYPE_SP_SLOPE_MEDIAN:
			sprintf(str, "%s-slp-med", str);
			break;
		case HDEF_SUBTYPE_SP_SLOPE_SHELL:
			sprintf(str, "%s-slp-shl", str);
			break;
		case HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN:
			sprintf(str, "%s-apr-mn", str);
			break;
		case HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE:
			sprintf(str, "%s-apr-p%.0f", str, def.percentile);
			break;
		case HDEF_SUBTYPE_SP_APOCENTER_SHELL_MEAN:
			sprintf(str, "%s-aps-mn", str);
			break;
		case HDEF_SUBTYPE_SP_APOCENTER_SHELL_PERCENTILE:
			sprintf(str, "%s-aps-p%.0f", str, def.percentile);
			break;
		default:
			error(__FFL__, "Invalid halo definition splashback subtype, %d.\n", def.subtype);
			break;
		}
		break;

	case HDEF_TYPE_FOF:
		sprintf(str, "%sfof-b%.2f", str, def.linking_length);
		break;

	case HDEF_TYPE_VMAX:
		sprintf(str, "%smax", str);
		break;

	case HDEF_TYPE_ORBITING:
		sprintf(str, "%sorb", str);
		switch (def.subtype)
		{
		case HDEF_SUBTYPE_ORB_ALL:
			sprintf(str, "%s-all", str);
			break;
		case HDEF_SUBTYPE_ORB_PERCENTILE:
			sprintf(str, "%s-p%.0f", str, def.percentile);
			break;
		default:
			error(__FFL__, "Invalid halo definition orbiting subtype, %d.\n", def.subtype);
			break;
		}
		break;

	default:
		error(__FFL__, "Invalid halo definition type, %d.\n", def.type);
		break;
	}

	/*
	 * Add acc/peak identifier if this definition is acc or peak.
	 */
	switch (def.time)
	{
	case HDEF_TIME_NOW:
		break;
	case HDEF_TIME_ACC:
		sprintf(str, "%s_acc", str);
		break;
	case HDEF_TIME_PEAK:
		sprintf(str, "%s_peak", str);
		break;
	default:
		error(__FFL__, "Invalid halo definition time identifier, %d.\n", def.time);
		break;
	}

	/*
	 * Add bound/all identifier if known.
	 */
	switch (def.ptl_select)
	{
	case HDEF_PTLSEL_DEFAULT:
		break;
	case HDEF_PTLSEL_ALL:
		sprintf(str, "%s_all", str);
		break;
	case HDEF_PTLSEL_BOUND:
		sprintf(str, "%s_bnd", str);
		break;
	case HDEF_PTLSEL_TRACER:
		sprintf(str, "%s_tcr", str);
		break;
	case HDEF_PTLSEL_ORBITING:
		sprintf(str, "%s_orb", str);
		break;
	default:
		error(__FFL__, "Invalid particle selection, %d.\n", def.ptl_select);
		break;
	}

	/*
	 * Add error identifier if any.
	 */
	switch (def.is_error)
	{
	case HDEF_ERR_NO:
		break;
	case HDEF_ERR_1SIGMA:
		sprintf(str, "%s_err", str);
		break;
	case HDEF_ERR_2SIGMA:
		sprintf(str, "%s_err2s", str);
		break;
	default:
		error(__FFL__, "Invalid error state identifier, %d.\n", def.is_error);
		break;
	}

	/*
	 * Add source identifier if any.
	 */
	switch (def.source)
	{
	case HDEF_SOURCE_ANY:
		break;
	case HDEF_SOURCE_CATALOG:
		sprintf(str, "%s_cat", str);
		break;
	case HDEF_SOURCE_SPARTA:
		sprintf(str, "%s_spa", str);
		break;
	case HDEF_SOURCE_SHELLFISH:
		sprintf(str, "%s_sfish", str);
		break;
	default:
		error(__FFL__, "Invalid source identifier, %d.\n", def.source);
		break;
	}

	/*
	 * Additional flags
	 */
	if (def.flags != 0)
	{
		if (def.flags & HDEF_FLAG_INTERNAL)
		{
			sprintf(str, "%s_internal", str);
		}
	}
}

HaloDefinition stringToHaloDefinition(char *str, int allow_errors)
{
	HaloDefinition def;
	char type_str[30], num_str[10], tmp_str[20];
	int i, j, first_sep, started_type_str, started_num_str, completed_type_str, completed_num_str;
	float num;

	/*
	 * Initialize to defaults
	 */
	initHaloDefinition(&def);

	/*
	 * Parse first letter, which indicates the quantity (R or M or V or nu). We allow for
	 * differently capitalized versions, although they are not strictly correct in the sense that
	 * they will never be output when converting back to a string.
	 */
	i = 0;
	if ((str[0] == 'R') || (str[0] == 'r'))
	{
		def.quantity = HDEF_Q_RADIUS;
		i++;
	} else if ((str[0] == 'M') || ((str[0] == 'm') && (!stringStartsWith(str, "max"))))
	{
		def.quantity = HDEF_Q_MASS;
		i++;
	} else if (stringStartsWith(str, "NU") || stringStartsWith(str, "Nu")
			|| stringStartsWith(str, "nu"))
	{
		def.quantity = HDEF_Q_PEAKHEIGHT;
		i += 2;
	} else if ((str[0] == 'V') || ((str[0] == 'v') && (!stringStartsWith(str, "vir"))))
	{
		def.quantity = HDEF_Q_VCIRC;
		i++;
	} else
	{
		def.quantity = HDEF_Q_UNKNOWN;
	}

	/*
	 * Now parse into the next few components: a type string, possibly a number, and possibly a
	 * second type string (c/m). The identifier ends if the string ends or if we get into the
	 * additional identifiers starting with _.
	 */
	started_type_str = 0;
	started_num_str = 0;
	completed_type_str = 0;
	completed_num_str = 0;
	j = 0;
	while ((i < strlen(str)) && (str[i] != '_'))
	{
		if (isdigit(str[i]) || (str[i] == '.'))
		{
			if (started_type_str && (!completed_type_str))
			{
				completed_type_str = 1;
				type_str[j] = '\0';
			}

			if (!started_num_str)
			{
				started_num_str = 1;
				j = 0;
			}

			if (completed_num_str)
			{
				if (allow_errors)
				{
					def.type = HDEF_TYPE_INVALID;
					return def;
				} else
				{
					error(__FFL__, "Found more than one numerical string in definition %s.\n", str);
				}
			}

			num_str[j] = str[i];
			j++;
		} else
		{
			if (started_num_str && (!completed_num_str))
			{
				completed_num_str = 1;
				num_str[j] = '\0';
			}

			if (!started_type_str)
			{
				started_type_str = 1;
				j = 0;
			}

			if (completed_type_str)
			{
				if (allow_errors)
				{
					def.type = HDEF_TYPE_INVALID;
					return def;
				} else
				{
					error(__FFL__, "Found more than one type string in definition %s.\n", str);
				}
			}

			type_str[j] = str[i];
			j++;
		}
		i++;
	}

	if (started_type_str && (!completed_type_str))
	{
		completed_type_str = 1;
		type_str[j] = '\0';
	} else if (started_num_str && (!completed_num_str))
	{
		completed_num_str = 1;
		num_str[j] = '\0';
	}

	/*
	 * Now, type string must be set, no exceptions
	 */
	if (!completed_type_str)
	{
		if (allow_errors)
		{
			def.type = HDEF_TYPE_INVALID;
			return def;
		} else
		{
			error(__FFL__, "Could not find type identifier in definition %s.\n", str);
		}
	}

	/*
	 * Convert numerical part
	 */
	if (!completed_num_str)
		num = -1.0;
	else
		num = atof(num_str);

	/*
	 * Parse types / numbers
	 */
	if (strcmp(type_str, "max") == 0)
	{
		def.type = HDEF_TYPE_VMAX;
	} else if (strcmp(type_str, "vir") == 0)
	{
		def.type = HDEF_TYPE_SO;
		def.subtype = HDEF_SUBTYPE_SO_VIRIAL;
		def.overdensity = 0.0;
	} else if (strcmp(type_str, "c") == 0)
	{
		def.type = HDEF_TYPE_SO;
		def.subtype = HDEF_SUBTYPE_SO_CRITICAL;
		def.overdensity = num;
	} else if (strcmp(type_str, "m") == 0)
	{
		def.type = HDEF_TYPE_SO;
		def.subtype = HDEF_SUBTYPE_SO_MATTER;
		def.overdensity = num;
	} else if (strcmp(type_str, "sp-slp-prf") == 0)
	{
		def.type = HDEF_TYPE_SPLASHBACK;
		def.subtype = HDEF_SUBTYPE_SP_SLOPE_PROFILE;
	} else if (strcmp(type_str, "sp-slp-med") == 0)
	{
		def.type = HDEF_TYPE_SPLASHBACK;
		def.subtype = HDEF_SUBTYPE_SP_SLOPE_MEDIAN;
	} else if (strcmp(type_str, "sp-slp-shl") == 0)
	{
		def.type = HDEF_TYPE_SPLASHBACK;
		def.subtype = HDEF_SUBTYPE_SP_SLOPE_SHELL;
	} else if (strcmp(type_str, "sp-apr-mn") == 0)
	{
		def.type = HDEF_TYPE_SPLASHBACK;
		def.subtype = HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN;
	} else if (strcmp(type_str, "sp-apr-p") == 0)
	{
		def.type = HDEF_TYPE_SPLASHBACK;
		def.subtype = HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE;
		def.percentile = num;
	} else if (strcmp(type_str, "sp-aps-mn") == 0)
	{
		def.type = HDEF_TYPE_SPLASHBACK;
		def.subtype = HDEF_SUBTYPE_SP_APOCENTER_SHELL_MEAN;
	} else if (strcmp(type_str, "sp-aps-p") == 0)
	{
		def.type = HDEF_TYPE_SPLASHBACK;
		def.subtype = HDEF_SUBTYPE_SP_APOCENTER_SHELL_PERCENTILE;
		def.percentile = num;
	} else if (strcmp(type_str, "fof-b") == 0)
	{
		def.type = HDEF_TYPE_FOF;
		def.linking_length = num;
	} else if (strcmp(type_str, "orb-all") == 0)
	{
		def.type = HDEF_TYPE_ORBITING;
		def.subtype = HDEF_SUBTYPE_ORB_ALL;
	} else if (strcmp(type_str, "orb-p") == 0)
	{
		def.type = HDEF_TYPE_ORBITING;
		def.subtype = HDEF_SUBTYPE_ORB_PERCENTILE;
		def.percentile = num;
	} else
	{
		if (allow_errors)
		{
			def.type = HDEF_TYPE_INVALID;
			return def;
		} else
		{
			error(__FFL__, "In definition %s, found unknwon type identifier %s.\n", str, type_str);
		}
	}

	/*
	 * Check for invalid numbers
	 */
	if (haloDefinitionHasNumber(&def))
	{
		if (def.overdensity < 0.0)
		{
			if (allow_errors)
			{
				def.type = HDEF_TYPE_INVALID;
				return def;
			} else
			{
				error(__FFL__,
						"In definition %s, found unknwon numerical value %.3f from string %s.\n",
						str, num, num_str);
			}
		}
	}

	/*
	 * At this point, we should either have reached the end of the string or the next character
	 * should be '_'.
	 */
	if ((i < strlen(str)) && (str[i] != '_'))
		error(__FFL__,
				"Found excess character %c after definition in string %s. Expected end of string or _.\n",
				str[i], str);

	/*
	 * Parse additional identifiers. At this point, i gives the position and we just keep looking
	 * for strings separated by _.
	 */
	j = 0;
	first_sep = 1;
	while (i < strlen(str))
	{
		if (str[i] != '_')
		{
			tmp_str[j] = str[i];
			j++;
		}
		if ((str[i] == '_') || (i == strlen(str) - 1))
		{
			if (first_sep)
			{
				first_sep = 0;
			} else
			{
				tmp_str[j] = '\0';
				if (strcmp(tmp_str, "all") == 0)
				{
					def.ptl_select = HDEF_PTLSEL_ALL;
				} else if (strcmp(tmp_str, "bnd") == 0)
				{
					def.ptl_select = HDEF_PTLSEL_BOUND;
				} else if (strcmp(tmp_str, "tcr") == 0)
				{
					def.ptl_select = HDEF_PTLSEL_TRACER;
				} else if (strcmp(tmp_str, "orb") == 0)
				{
					def.ptl_select = HDEF_PTLSEL_ORBITING;
				} else if (strcmp(tmp_str, "acc") == 0)
				{
					def.time = HDEF_TIME_ACC;
				} else if (strcmp(tmp_str, "peak") == 0)
				{
					def.time = HDEF_TIME_PEAK;
				} else if (strcmp(tmp_str, "cat") == 0)
				{
					def.source = HDEF_SOURCE_CATALOG;
				} else if (strcmp(tmp_str, "spa") == 0)
				{
					def.source = HDEF_SOURCE_SPARTA;
				} else if (strcmp(tmp_str, "sfish") == 0)
				{
					def.source = HDEF_SOURCE_SHELLFISH;
				} else if (strcmp(tmp_str, "err") == 0)
				{
					def.is_error = HDEF_ERR_1SIGMA;
				} else if (strcmp(tmp_str, "err2s") == 0)
				{
					def.is_error = HDEF_ERR_2SIGMA;
				} else if (strcmp(tmp_str, "internal") == 0)
				{
					def.flags = def.flags | HDEF_FLAG_INTERNAL;
				} else
				{
					if (allow_errors)
					{
						def.type = HDEF_TYPE_INVALID;
						return def;
					} else
					{
						error(__FFL__,
								"In halo definition %s, found unrecognized identifier string %s.\n",
								str, tmp_str);
					}
				}
				j = 0;
			}
		}
		i++;
	}

	return def;
}

void haloDefinitionToLabelQuantity(HaloDefinition *def, char *str)
{
	switch (def->quantity)
	{
	case HDEF_Q_UNKNOWN:
		sprintf(str, "%s", "unknown");
		break;
	case HDEF_Q_RADIUS:
		sprintf(str, "%s", "radius");
		break;
	case HDEF_Q_MASS:
		sprintf(str, "%s", "mass");
		break;
	case HDEF_Q_PEAKHEIGHT:
		sprintf(str, "%s", "peak height");
		break;
	case HDEF_Q_VCIRC:
		sprintf(str, "%s", "circular veocity");
		break;
	default:
		error(__FFL__, "Invalid halo quantity, %d.\n", def->quantity);
		break;
	}
}

void haloDefinitionToLabelType(HaloDefinition *def, char *str)
{
	HaloDefinition tmp_def;

	initHaloDefinition(&tmp_def);
	tmp_def.type = def->type;
	tmp_def.subtype = def->subtype;

	haloDefinitionToString(tmp_def, str);
}

/*
 * Not all definitions use any numerical value, and it is often important to figure out which ones
 * those are (and, conversely, for which the numerical value is meaningless).
 */
int haloDefinitionHasNumber(HaloDefinition *def)
{
	int b;

	b = 0;

	if (def->type == HDEF_TYPE_SO)
	{
		b = (b || (def->subtype == HDEF_SUBTYPE_SO_CRITICAL));
		b = (b || (def->subtype == HDEF_SUBTYPE_SO_MATTER));
	} else if (def->type == HDEF_TYPE_SPLASHBACK)
	{
		b = (b || (def->subtype == HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE));
		b = (b || (def->subtype == HDEF_SUBTYPE_SP_APOCENTER_SHELL_PERCENTILE));
	} else if (def->type == HDEF_TYPE_FOF)
	{
		b = 1;
	} else if (def->type == HDEF_TYPE_VMAX)
	{
		b = 0;
	} else if (def->type == HDEF_TYPE_ORBITING)
	{
		b = (b || (def->subtype == HDEF_SUBTYPE_ORB_PERCENTILE));
	} else
	{
		error(__FFL__, "Unknown halo definition type, %d.\n", def->type);
	}

	return b;
}

/*
 * This function returns a range of values depending on the differences between two definitions,
 * and HDEF_DIFF_NONE they are the same (this value is NOT zero!). See the header file for
 * details on the meaning of the values.
 *
 * The float values are considered the same if they differ by less than 1/1000 in either absolute
 * or relative units.
 */
int compareHaloDefinitions(HaloDefinition a, HaloDefinition b)
{
	/*
	 * First check the simple value comparisons; they must all be the same or we return the
	 * corresponding difference code.
	 */
	if (a.type != b.type)
		return HDEF_DIFF_TYPE;

	if (a.subtype != b.subtype)
		return HDEF_DIFF_SUBTYPE;

	/*
	 * The main enum values are the same; do we also need to compare the overdensity / percentile?
	 * This depends on the definition.
	 */
	if (haloDefinitionHasNumber(&a))
	{
		int diff_float;

		diff_float = (fabs(a.overdensity - b.overdensity) > 1E-3);
		if ((!diff_float) && (b.overdensity > 0.0))
			diff_float = (diff_float && (fabs(a.overdensity / b.overdensity - 1.0) > 1E-3));

		if (diff_float)
			return HDEF_DIFF_NUMBER;
	}

	/*
	 * Now we compare the less important properties.
	 */
	if ((a.ptl_select != b.ptl_select) || (a.time != b.time) || (a.source != b.source)
			|| (a.is_error != b.is_error))
		return HDEF_DIFF_PROPERTIES;

	/*
	 * Finally, we have established that the definitions are the same, but not that the quantity is
	 * the same.
	 */
	if (a.quantity != b.quantity)
		return HDEF_DIFF_QUANTITY;

	return HDEF_DIFF_NONE;
}

/*
 * This function is a specific sub-function of the general one above: we are only interested in
 * whether two definitions have the same SO threshold, not whether they are R or M, bound or all
 * etc.
 */
int compareSOThresholds(HaloDefinition a, HaloDefinition b)
{
	int diff_float;

	/*
	 * Check that both defs are SO
	 */
	if ((a.type != HDEF_TYPE_SO) || (b.type != HDEF_TYPE_SO))
		return 1;

	/*
	 * Check that they are the same type
	 */
	if (a.subtype != b.subtype)
		return 1;

	/*
	 * If virial, they are the same
	 */
	if (a.subtype == HDEF_SUBTYPE_SO_VIRIAL)
		return 0;

	/*
	 * The defs are the same; compare the overdensity
	 */
	diff_float = (fabs(a.overdensity - b.overdensity) > 1E-3);
	if ((!diff_float) && (b.overdensity > 0.0))
		diff_float = (diff_float && (fabs(a.overdensity / b.overdensity - 1.0) > 1E-3));

	return diff_float;
}

/*
 * This function returns a density in physical units of M_sun h^2 / kpc^3, for
 * a given overdensity and an overdensity type; there are three allowed types:
 *
 * - HDEF_SUBTYPE_SO_MATTER   (mean matter density)
 * - HDEF_SUBTYPE_SO_CRITICAL (critical density)
 * - HDEF_SUBTYPE_SO_VIRIAL   (virial according to Bryan & Norman 98)
 *
 * Note that the last type corresponds to an overdensity in itself, and the
 * overdensity value is ignored.
 */
float densityThreshold(Cosmology *cosmo, float z, HaloDefinition *def)
{
	float result = 0.0;

	if (def->type != HDEF_TYPE_SO)
		error(__FFL__, "Cannot compute SO threshold for non-SO mass definition.\n");

	switch (def->subtype)
	{
	case HDEF_SUBTYPE_SO_MATTER:
		result = rhoM(cosmo, z) * def->overdensity;
		break;
	case HDEF_SUBTYPE_SO_CRITICAL:
		result = rhoCrit(cosmo, z) * def->overdensity;
		break;
	case HDEF_SUBTYPE_SO_VIRIAL:
		result = rhoCrit(cosmo, z) * deltaVirialCritical(cosmo, z);
		break;
	default:
		error(__FFL__, "Invalid SO subtype %d.\n", def->subtype);
		break;
	}

	return result;
}
