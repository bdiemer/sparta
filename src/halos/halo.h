/*************************************************************************************************
 *
 * This unit implements basic routines related to the halos.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _HALO_H_
#define _HALO_H_

#include "../global_types.h"
#include "../statistics.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * The halo status field is important. The constants should be internal to this unit where they are
 * evaluate them in a self-consistent manner. For example, the status should always be queried by
 * the is*() functions. However, some external tools might need to interpret the status fields
 * which is why they are given in this header file.
 *
 * First, the halo can exist or not exist at a given snapshot. For external tools, it is also
 * possible that the halo was not found (this status is not used within SPARTA).
 *
 * NOT_FOUND               The halo could not be found, e.g., in a SPARTA output file.
 * NONE                    The halo does not exist (at this time).
 *
 * HOST                    Halo is a host halo, that is, not a subhalo of any other halo.
 * CONNECTING_HOST         We are looking for a host descendant (temporary/internal status).
 *
 * SUB                     The halo is a subhalo, belonging to some host on the same process.
 *                         The status can change back to host for backsplash halos.
 * BECOMING_SUB            The halo was a host in the last snapshot, but is a sub now.
 * BECOMING_HOST           The halo was and is a sub, but will become a host in the next snap.
 * BECOMING_SUB_HOST       The halo was a host, is a sub, and will become a host in the next
 *                         snapshot again.
 * SWITCHED_HOST           The halo was and is a sub, but switched host.
 * CONNECTING_SUB          We are looking for a sub descendant (temporary/internal status).
 * CONNECTING_SUB_SWITCH   We are looking for a sub descendant that used to be a sub before, but
 *                         has to be treated as if becoming a sub (temporary/internal status).
 * SWITCHING_HOST          The halo is a subhalo, but will be in a different host in this
 *                         snapshot than in the last. We need to keep track of this special case
 *                         because there is no indication in the pid and desc_pid of a catalog
 *                         entry that this is happening (temporary/internal status).
 *
 * Once a halo has disappeared from the catalogs, we can still track its particles in which case
 * it becomes a ghost:
 *
 * GHOST_HOST              The halo is being tracked through its particles and is a host.
 * GHOST_SUB               The halo is being tracked through its particles and is a subhalo of
 *                         another (non-ghost) halo.
 * GHOST_SWITCHING         The halo is a subhalo ghost and is switching from one host to
 *                         another, typically because the host merges away or becomes a ghost
 *                         itself.
 *
 * Finally, the DEAD status indicates the highest number for which the halo is still alive, as
 * opposed to the final statuses listed below which must be larger:
 *
 * DEAD                    All final statuses must be larger than this number
 */
#define HALO_STATUS_NOT_FOUND -2
#define HALO_STATUS_NONE -1

#define HALO_STATUS_HOST 10
#define HALO_STATUS_CONNECTING_HOST 11

#define HALO_STATUS_SUB 20
#define HALO_STATUS_BECOMING_SUB 21
#define HALO_STATUS_BECOMING_HOST 22
#define HALO_STATUS_BECOMING_SUB_HOST 23
#define HALO_STATUS_SWITCHED_HOST 24
#define HALO_STATUS_CONNECTING_SUB 25
#define HALO_STATUS_CONNECTING_SUB_SWITCH 26
#define HALO_STATUS_SWITCHING_HOST 27

#define HALO_STATUS_GHOST_HOST 30
#define HALO_STATUS_GHOST_SUB 31
#define HALO_STATUS_GHOST_SWITCHING 32

#define HALO_STATUS_DEAD 40

/*
 * A halo can fundamentally end for three reasons: it merged into another halo, it reached the end
 * of the simulation, or an error occurred. Some of these scenarios are further distinguished. For
 * example, ghosts have specific codes for how they disappeared, but the following statuses
 * all indicate that the halo merged away:
 *
 * MERGED            The halo's main branch ended, i.e. it merged with another, larger halo.
 * GHOST_CENTER      A ghost was found to have merged with its host as its center was within a
 *                   small fraction of the host's radius for a significant fraction of a dynamical
 *                   time.
 * GHOST_TOO_SMALL   The ghost was found and its center determined, but its mass was below the
 *                   limiting number of particles.
 * GHOST_NOT_FOUND   Not enough of the ghost's particles could be found at all in the new
 *                   snapshot (this should be rare, because it means that the particles moved far
 *                   from where they were expected to be)
 * GHOST_POSITION    The position and/or velocity of the ghost could not be reliably determined.
 *
 * There is only one status for reaching the last snapshot, regardless of whether it refers to a
 * halo or ghost:
 *
 * LAST_SNAP         The halo reached the last snap that was evaluated.
 *
 * Finally, there are a number of error codes that should occur never or rarely:
 *
 * NOT_FOUND         The halo could not be found in the catalog. This should never (or very rarely
 *                   occur, otherwise there is something wrong with the merger tree code).
 * JUMP              The halo jumped a physically impossible distance. This indicates a problem in
 *                   the merger tree code.
 * HOST_ENDED        Could not find host during exchange step (should never happen).
 * SEARCH_RADIUS     No physically sensible particle distribution could be found. This can happen
 *                   with very small or interpolated ('phantom') halos.
 */
#define HALO_ENDED_MERGED 50
#define HALO_ENDED_GHOST_CENTER 51
#define HALO_ENDED_GHOST_TOO_SMALL 52
#define HALO_ENDED_GHOST_NOT_FOUND 53
#define HALO_ENDED_GHOST_POSITION 54

#define HALO_ENDED_LAST_SNAP 60

#define HALO_ENDED_NOT_FOUND 70
#define HALO_ENDED_JUMP 71
#define HALO_ENDED_HOST_ENDED 72
#define HALO_ENDED_SEARCH_RADIUS 73

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void printDebugHalo(Halo *halo, const char *format, ...);
void printDebugHaloCatalogData(int is_main, HaloCatalogData *cd, const char *format, ...);

void initializeHalo(Halo *halo);
void initHaloDynArrays(Halo *halo);
void freeTracerType(TracerType *tt);
void freeHalo(Halo *halo);
void initHaloCatalogData(HaloCatalogData *cd);

// Other halo functions
void deleteHalos(DynArray *halos, int_least8_t *mask);
int deleteEndedHalos(DynArray *halos);
void endHalo(Halo *halo, int snap_idx, int final_snap, int final_status);

void sortHalos(DynArray *halos, int by_desc_id);
void sortArrays(DynArray *halos);

void setHaloStatusInitial(Halo *halo, HaloCatalogData *cd);
void setHaloStatusSnapStart(int snap_idx, DynArray *halos);
void setHaloStatusConnect(int snap_idx, Halo *progenitor, HaloCatalogData *descendant);
void setHaloStatusSubhalos(Halo *sub, Halo *host, int snap_idx);
void setHaloStatusSnapEnd(int snap_idx, DynArray *halos);

int isHostStatus(int_least8_t status);
int isSubStatus(int_least8_t status);
int isGhostStatus(int_least8_t status);
int isGhostID(ID halo_id);

int isAlive(Halo *halo);
int hasEnded(Halo *halo);
int isHost(Halo *halo);
int isHostOrConnecting(Halo *halo);
int isSub(Halo *halo);
int isSubPermanently(Halo *halo);
int isSubNextSnap(Halo *halo);
int isSubOrConnecting(Halo *halo);
int isConnecting(Halo *halo);
int isEnteringHost(Halo *halo);
int isEnteringHostPermanently(Halo *halo);
int isLeavingHost(Halo *halo);
int isBecomingHost(Halo *halo);
int isBecomingSub(Halo *halo);
int isBecomingSubPermanently(Halo *halo);
int isGhost(Halo *halo);
int canHostParticles(Halo *halo);
int canHostSubhalos(Halo *halo);
int usesAllParticles(Halo *halo);
int hasMassProfile(Halo *halo);
int wasHostLastSnap(Halo *halo, int snap_idx);
int hasReachedMinimumMass(Halo *halo, int snap_idx);
int hasReachedMinimumMassByFinalSnap(Halo *halo);
int shouldBeSaved(Halo *halo);

// Mass profile, unit conversions
HaloID haloOriginalID(Halo *halo);
float haloR200m(Halo *halo, int snap_idx);
float haloR200mComoving(Halo *halo, int snap_idx);
float haloM200m(Halo *halo, int snap_idx);
float* haloX(Halo *halo, int snap_idx, int current_snap_idx);
float* haloV(Halo *halo, int snap_idx, int current_snap_idx);
float haloMassRatio(Halo *host, Halo *sub);
float comovingRadius(float r_phys, int snap_idx);
float physicalRadius(float r_com, int snap_idx);
int posInsideHalo(int snap_idx, Halo *halo, float *x);
void advanceHaloPosition(int snap_idx, Halo *halo);

long int haloMemory(Halo *h);

#endif
