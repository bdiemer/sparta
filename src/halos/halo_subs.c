/*************************************************************************************************
 *
 * This unit keeps track of host-subhalo relations.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "halo_subs.h"
#include "halo.h"
#include "../memory.h"
#include "../utils.h"
#include "../tracers/tracers.h"
#include "../tracers/tracer_subhalos.h"
#include "../tracers/tracer_subtagging.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initSubPointer(SubPointer *subhalo)
{
	subhalo->idx = INVALID_IDX;
	subhalo->id = INVALID_ID;
	subhalo->desc_id = INVALID_ID;
}

SubPointer* addSubToHost(Halo *host, Halo *sub, int sub_idx, int snap_idx)
{
	SubPointer *sh;

	sh = addSubPointer(__FFL__, &(host->subs));
	sh->idx = sub_idx;
	sh->id = sub->cd.id;
	sh->desc_id = sub->cd.desc_id;
	sh->first_snap = snap_idx;
	debugHalo(host, "Adding subhalo %ld.", sub->cd.id);
	debugHalo(sub, "Adding to host %ld.", host->cd.id);

	return sh;
}

void removeSubFromHost(DynArray *halos, Halo *sub)
{
	int i, found;
	Halo *host;

	if (sub->host_idx < 0)
		error(__FFL__, "Trying to remove sub from host_idx %d, subhalo ID %ld.\n", sub->host_idx,
				sub->cd.id);
	else if (sub->host_idx >= halos->n)
		error(__FFL__, "Trying to remove sub from host_idx %d, subhalo ID %ld, n %d.\n",
				sub->host_idx, sub->cd.id, halos->n);

	host = &(halos->h[sub->host_idx]);
	found = 0;
	for (i = 0; i < host->subs.n; i++)
	{
		if (host->subs.sp[i].id == sub->cd.id)
		{
			deleteDynArrayItem(__FFL__, &(host->subs), i);
			found = 1;
			break;
		}
	}
	if (!found)
		error(__FFL__,
				"[%4d] Trying to remove subhalo ID %ld from host %ld, host_idx %d, but could not find it in %d subhalos.\n",
				proc, sub->cd.id, host->cd.id, sub->host_idx, host->subs.n);
	sub->host_idx = INVALID_IDX;
	debugHalo(host, "Removing subhalo %ld.", sub->cd.id);
	debugHalo(sub, "Removing from host %ld.", host->cd.id);
}

/*
 * At the beginning of the snapshot, we need to advance the IDs of all subhalo pointers to their
 * descendant IDs so we can find them again.
 */
void advanceSubhalos(int snap_idx, DynArray *halos)
{
	int i, j;
	Halo *h;

	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		if (!canHostSubhalos(h))
			continue;

		for (j = 0; j < h->subs.n; j++)
			h->subs.sp[j].id = h->subs.sp[j].desc_id;
	}
}

/*
 * Assign hosts to subhalos. This function is a collection of many important steps that we need to
 * apply to host-subhalo pairs:
 *
 * - for all new subs, assign them their host for the first time
 * - tag their particles as coming from a subhalo
 * - remove certain tracers and subhalos from them
 * - for all existing subs, update their host_idx
 * - in the host, update the sub_idx and sub_id
 * - for ghosts, update the descendant pid
 */
void hostSubRelations(DynArray *halos, Tree *tree, int snap_idx, LocalStatistics *ls)
{
	int i, j, tt, rs, host_idx, *n_subs_previous;
	int_least8_t *changed_subs;
	Halo *temp_halo, *h, *host, *sub;
	SubPointer *sh, temp_sh;
	Tracer *tcr;

	/*
	 * Step 1: We need to record the size of the sub arrays, as we might be adding new subhalos
	 * which would confuse the search function. We also reset all indices so we can check later
	 * whether they were set to the correct index.
	 */
	n_subs_previous = (int*) memAlloc(__FFL__, MID_HALOLOGIC, sizeof(int) * halos->n);
	changed_subs = (int_least8_t*) memAlloc(__FFL__, MID_HALOLOGIC,
			sizeof(int_least8_t) * halos->n);

	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		h->host_idx = INVALID_IDX;
		n_subs_previous[i] = h->subs.n;
		changed_subs[i] = 0;
		for (j = 0; j < h->subs.n; j++)
			h->subs.sp[j].idx = INVALID_IDX;
	}

	/*
	 * Step 2: Find each sub's host. If the subhalo is new to this host, we add a record (if the
	 * subhalo is not already being tracked) and tag the particles in the subhalo. The latter step
	 * is performed even if we are already tracking the subhalo, since there could be new particles
	 * that were not tracked previously. The previous tags are not changed by this new tagging.
	 *
	 * If the halo was already a sub before, we find and update its record.
	 *
	 * Note that we do not care whether the host is alive or not; dead halos are removed at the
	 * end of the snap and should be treated the same until then.
	 */
	temp_halo = (Halo*) memAlloc(__FFL__, MID_HALOLOGIC, sizeof(Halo));
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		if (!isSub(h))
			continue;

		temp_halo->id_sort = h->cd.pid;
		host = (Halo*) bsearch(temp_halo, halos->h, halos->n, sizeof(Halo), &compareHalos);

		if (host == NULL)
			error(__FFL__, "Host halo %ld not found, subhalo %ld, status %d\n", h->cd.pid, h->cd.id,
					h->status);

		host_idx = host - halos->h;
		h->host_idx = host_idx;

		/*
		 * Subhalos are allowed to host ghosts, but not other halos. If host and sub have the same
		 * ID, there must have been some internal error.
		 */
		if (!isGhost(h) && isSubPermanently(host))
			error(__FFL__,
					"Looking for subhalo record of sub %ld in halo %ld, but that is a subhalo itself.\n",
					h->cd.id, host->cd.id);
		if (host->cd.id == h->cd.id)
			error(__FFL__,
					"Found host, but host and sub have the same ID %ld (indices %d and %d).\n",
					h->cd.id, i, host_idx);

		/*
		 * For ghosts, update the desc_pid in the halo itself. We could not do that earlier because
		 * we did not know the host, and there was no catalog to tell us. At this point, the
		 * ghost's record is up to date in terms of halo ID information (though we don't have
		 * position, velocity etc yet.
		 */
#if DO_GHOSTS
		if (isGhost(h))
		{
			if (host->cd.desc_pid == -1)
			{
				h->cd.desc_pid = host->cd.desc_id;
			} else
			{
				h->cd.desc_pid = host->cd.desc_pid;
			}
			debugHalo(h, "Updated ghost desc_pid.");
		}
#endif

		/*
		 * When adding a new sub, we do not re-sort the arrays right away because that might
		 * be executed many times for the same host halo. Instead, we remember that the subs
		 * in this particular host were changed, and re-sort later. Note that the
		 * addSubhaloTracer() function itself re-sorts the IID array, so we only have to worry
		 * about the tracer and sub pointer arrays.
		 *
		 * If this subhalo is not entering a new host, it is simply staying a subhalo in the same
		 * host. We search for it in the host's records.
		 */
		if (isEnteringHost(h))
		{
			sh = addSubToHost(host, h, i, snap_idx);
			changed_subs[host_idx] = 1;

			/*
			 * We add a subhalo tracer regardless of whether the subhalo is going to stay in this host
			 * for one or more snapshots because tracing subhalos is cheap, even after they leave the
			 * halo. In a number of cases, the halo will come back and become a sub again, in which
			 * case it is good to have its whole trajectory.
			 *
			 * We do not add ghosts as ghost subhalo tracers are currently not implemented.
			 */
#if DO_TRACER_SUBHALOS
			if (!isGhost(h))
				addSubhaloTracer(snap_idx, host, h, &(ls->tt[SHO]));
#endif
		} else
		{
#if CAREFUL
			if (host->subs.n == 0)
				error(__FFL__,
						"Looking for subhalo record of sub %ld in host %ld, but found zero records.\n",
						h->cd.id, host->cd.id);
#endif
			temp_sh.id = h->cd.id;
			sh = (SubPointer*) bsearch(&temp_sh, host->subs.sp, n_subs_previous[host_idx],
					sizeof(SubPointer), &compareSubhalos);
			if (sh == NULL)
			{
				output(0, "Current subhalos in host:\n");
				for (j = 0; j < host->subs.n; j++)
					output(0, "  Subhalo %3d, ID %8ld idx %3d\n", j, host->subs.sp[j].id,
							host->subs.sp[j].idx);
				error(__FFL__,
						"[%4d] Could not find subhalo %ld in host %ld, status %d, n_subs %d, n_subs_prev %d.\n",
						proc, temp_sh.id, host->cd.id, host->status, host->subs.n,
						n_subs_previous[host_idx]);
			}
		}

		/*
		 * Subhalo tagging is, fundamentally, the step of identifying particles that belong to
		 * the subhalo. However, this function takes on additional tasks, namely tagging
		 * tracers in the host halo if necessary and adding/deleting tracers from the subhalo
		 * if subhalo tracking is on.
		 *
		 * Note that we are not passing the tracer object, but just the raw subhalo data to
		 * this function. This means that a few calculations, such as the radius, have to be
		 * repeated, but it is not always guaranteed that the tracer object was really created,
		 * for example the subhalo tracers might be off for this particular halo, but we still
		 * with to tag the particle tracers.
		 *
		 * Note that this function re-sorts the particle tracers and IID arrays, but not the
		 * subhalo arrays.
		 */
#if DO_SUBHALO_TAGGING
		if (isEnteringHostPermanently(h))
			tagSubParticles(snap_idx, host, h, tree, &(ls->tt[PTL]));
#endif

		/*
		 * Set or update properties of SubPointer
		 */
		sh->idx = i;
		sh->id = h->cd.id;
		sh->desc_id = h->cd.desc_id;
	}

	/*
	 * Step 3: Deal with halos that are (permanently, as in for more than one snap) becoming a
	 * subhalo. In this case, we need to get rid of certain tracers and subhalos.
	 *
	 * If the halo is becoming a sub (which is slightly different from entering host in
	 * that it does not happen if the sub switched hosts), we delete all subhalo tracers.
	 * If subhalo tracking is off, we do the same for particles. Otherwise, the particle
	 * tracers have already been dealt with in the tagging function above.
	 *
	 * Note that in this case we do not mark tracers for deletion, but that we directly
	 * free the array. We do not put particles on the ignore list even if they have entered
	 * the halo. In this way, the halo becoming a subhalo is a different event from a
	 * tracer-specific reason for the tracer being deleted. The logic is that, if the tracer
	 * is not on the ignore list at this point, it should not go on the list because the halo
	 * became a subhalo; if it is not reborn, the tracer will never be considered again anyway
	 * except perhaps as a tracer in the subhalo, which means different rules. If the halo is
	 * reborn, the tracer may come back, but in that case we need to figure out its status from
	 * previous results etc anyway.
	 *
	 * For all tracers and results, we compress the result arrays to minimize their memory
	 * consumption, since most of them will likely not be added to while the halo is a sub.
	 */
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		if (!isBecomingSubPermanently(h))
			continue;

		/*
		 * If we are not allowing sub-subhalos, this subhalo loses all its own subhalos at this
		 * point. If we are tracking ghosts, we only delete non-ghost sub-pointers. We do not
		 * have a guarantee that all ghost subs will have been found because some might have ended.
		 */
#if DO_SUBSUBS
		j = 0;
		while (j < h->subs.n)
		{
			if (h->subs.sp[j].idx == INVALID_IDX)
			{
				/*
				 * If we need to look for a halo, we might need to sort the subpointer array first
				 * if it has been changed.
				 */
				if (changed_subs[i])
				{
					qsort(h->subs.sp, h->subs.n, sizeof(SubPointer), &compareSubhalos);
					changed_subs[i] = 0;
				}
				sh = &(h->subs.sp[j]);
				temp_halo->id_sort = sh->id;

				sub = (Halo*) bsearch(temp_halo, halos->h, halos->n, sizeof(Halo), &compareHalos);
				if (sub == NULL)
					error(__FFL__,
							"In becoming subhalo %ld, status %d, found orphaned subhalo record idx %d ID %ld desc_id %ld.\n",
							h->cd.id, h->status, j, sh->id, sh->desc_id);
			} else
			{
				sub = &(halos->h[h->subs.sp[j].idx]);
			}

			/*
			 * We could also handle ended subs here, but that is done in a separate loop below, and
			 * we leave it there for consistency.
			 */
			if (!isGhost(sub))
			{
				deleteDynArrayItem(__FFL__, &(h->subs), j);
				debugHalo(sub, "Deleted sub pointer because host %ld has become subhalo.", h->cd.id);
			} else
			{
				j++;
			}
		}
#else
		freeDynArray(__FFL__, &(h->subs));
		debugHalo(h, "Removed all subhalos.");
#endif

		for (tt = 0; tt < NTT; tt++)
		{
			/*
			 * Compress result arrays regardless of whether subtracking is on for this halo
			 */
			for (rs = 0; rs < NRS; rs++)
			{
				compressDynArray(__FFL__, &(h->tt[tt].rs[rs]));
			}

			/*
			 * Delete tracers unless we are subtracking
			 */
#if DO_SUBHALO_TRACKING
			if (ttParticles(tt))
			{
				debugHalo(h, "Not deleting particles because subhalo particles are being tracked.");
				continue;
			}
#endif

			for (j = 0; j < h->tt[tt].tcr.n; j++)
			{
				tcr = &(h->tt[tt].tcr.tcr[j]);

				if (tcr->status != TCR_STATUS_DELETE)
				{
					ls->tt[tt].n[TCRSTAT_HALO_BECAME_SUB]++;
					ls->tt[tt].n[TCRSTAT_DELETED]++;
					debugTracer(h, tcr, "Deleting because host halo became sub.");
				} else
				{
					debugTracer(h, tcr,
							"Not deleting because host halo became sub, already has status delete.");
				}
			}
			freeDynArray(__FFL__, &(h->tt[tt].tcr));
			debugHalo(h, "Removed all tracer particles.");
		}
	}

	/*
	 * Step 4: Set the new status for subhalos.
	 *
	 * We did not perform this step in the previous loop over subhalos because we would lose the
	 * information about halos that are becoming subs, we we need now to treat its tracers.
	 * Moreover, the order in which we set the new status for subhalos should not matter, since it
	 * is essentially random.
	 */
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		if (!isSub(h))
			continue;
		host = &(halos->h[h->host_idx]);
		setHaloStatusSubhalos(h, host, snap_idx);
	}

	/*
	 * Step 5: Go through subhalo records, look for unassigned records. If the subhalo the record
	 * points to has ended, destroy the record.
	 */
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);

		if (isSub(h) && (h->host_idx == INVALID_IDX))
			error(__FFL__,
					"Halo %ld, idx %d is a sub (status %d, pid %ld) but no host was found.\n",
					h->cd.id, i, h->status, h->cd.pid);

		if (!canHostSubhalos(h))
			continue;

		debugHalo(h, "Checking for subhalos in this halo.");
		j = 0;
		while (j < h->subs.n)
		{
			if (h->subs.sp[j].idx == INVALID_IDX)
			{
				sh = &(h->subs.sp[j]);
				temp_halo->id_sort = sh->id;
				sub = (Halo*) bsearch(temp_halo, halos->h, halos->n, sizeof(Halo), &compareHalos);
				if (sub == NULL)
				{
					error(__FFL__,
							"Host halo %ld, status %d, found orphaned subhalo record idx %d ID %ld desc_id %ld.\n",
							h->cd.id, h->status, j, sh->id, sh->desc_id);
				} else
				{
					if (hasEnded(sub))
					{
						deleteDynArrayItem(__FFL__, &(h->subs), j);
						debugHalo(sub,
								"Deleted sub pointer because sub has ended, desc_id %ld, desc_pid %ld.",
								sub->cd.desc_id, sub->cd.desc_pid);
					} else
					{
						error(__FFL__,
								"Host halo %ld, found sub for orphaned record %d, sub ID %ld, desc_id %ld. Sub has ID %ld, status %d, host_idx %d, pid %ld. Should have been found in main loop.\n",
								h->cd.id, j, sh->id, sh->desc_id, sub->cd.id, sub->status,
								sub->host_idx, sub->cd.pid);
					}
				}
			} else
			{
				j++;
			}
		}
	}

	/*
	 * Step 6: We have added sub pointers and subhalo tracers to certain host halos, and re-sort
	 * their arrays. However, the IID array is sorted within the function that adds subhalo tracers.
	 */
	for (i = 0; i < halos->n; i++)
	{
		if (changed_subs[i])
		{
			h = &(halos->h[i]);
			qsort(h->subs.sp, h->subs.n, sizeof(SubPointer), &compareSubhalos);
#if DO_TRACER_SUBHALOS
			qsort(h->tt[SHO].tcr.tcr, h->tt[SHO].tcr.n, sizeof(Tracer), &compareTracers);
#endif
		}
	}

	/*
	 * Free memory
	 */
	memFree(__FFL__, MID_HALOLOGIC, n_subs_previous, sizeof(int) * halos->n);
	memFree(__FFL__, MID_HALOLOGIC, changed_subs, sizeof(int_least8_t) * halos->n);
	memFree(__FFL__, MID_HALOLOGIC, temp_halo, sizeof(Halo));
}
