/*************************************************************************************************
 *
 * This unit implements the main work flow of sparta, namely a loop over all snapshots. The steps
 * that do not demand communication between processes are separated into the processSnapshot()
 * function.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "run.h"
#include "config.h"
#include "memory.h"
#include "statistics.h"
#include "geometry.h"
#include "tree.h"
#include "halos/halo.h"
#include "halos/halo_connect.h"
#include "halos/halo_particles.h"
#include "halos/halo_subs.h"
#include "halos/halo_ghosts.h"
#include "lb/exchange.h"
#include "lb/lb.h"
#include "lb/domain.h"
#include "analyses/analyses.h"
#include "io/io_output.h"
#include "io/io_restart.h"
#include "io/io_snaps.h"
#include "results/results.h"
#include "tracers/tracers.h"
#include "tracers/tracer_subtagging.h"
#include "tracers/tracer_particles.h"
#include "tracers/tracer_subhalos.h"

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void processSnapshot(int snap_idx, DynArray *halos, Box *box_halos, Particle *particles,
		int n_particles, LocalStatistics *ls);
void processHalo(Tree *tree, Halo *halo, Halo *host, Box *box_halos, int snap_idx,
		LocalStatistics *ls);
void freeTempMemory(float *p_radii, TreeResults *tr_res_all, TreeResults *tr_res_tcr);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * This function contains the main workflow of sparta. In particular, it "owns" the list of halos
 * on each process.
 */
void processSimulation(GlobalStatistics *gs, int restart)
{
	// Memory that persists between snapshots
	int snap_idx;
	LocalStatistics ls, ls_lastsnap;
	OutputFile *of = NULL;
	DynArray halos;

	// Temporary fields
	int snap_idx_start, i, n_particles;
	double current_time;
	Box box_halos;
	Particle *particles;
#if DO_READ_PARTICLES
	int n_files_read;
#endif

	/*
	 * Initial memory allocation and initialization. We need to allocate memory for the output file,
	 * which is then either overwritten by loading a restart file or initialized. The same is true
	 * for the local statistics.
	 */
	if (is_main_proc)
		of = (OutputFile*) memAlloc(__FFL__, MID_OUTPUTFILES, sizeof(OutputFile));

	if (restart)
	{
		readOrWriteRestartFiles(0, &snap_idx_start, gs, of, &ls, &ls_lastsnap, &halos);
		snap_idx_start++;
	} else
	{
		initDynArray(__FFL__, &halos, DA_TYPE_HALO);
		initLocalStatistics(&ls);
		snap_idx_start = 0;
	}

	/*
	 * We waited to initialize the MPI communicator in case there was a restart.
	 */
	createMPICommunicator();

	/*
	 * Open the output file; that's the last of the initialization tasks, so we set the timers.
	 */
	current_time = MPI_Wtime();
	if (is_main_proc)
	{
		openOutputFile(of, restart);

		gs->timers_0_cum[T0_INIT] += current_time;
		gs->timers_0_cum[T0_SNAPS] -= current_time;
	}

	/*
	 * Main loop over snapshots. Note that the initial time stamp in this loop is either from the
	 * last step of the last iteration, or from the one before the loop started.
	 */
	for (snap_idx = snap_idx_start; snap_idx < config.n_snaps; snap_idx++)
	{
		/*
		 * Set global convenience variables for this snapshot.
		 */
		setSnapshotVariables(snap_idx);

		/*
		 * Preparation work. Note that the current time was set before exchanging statistics
		 * in the last snapshot.
		 */
		memcpy(&ls_lastsnap, &ls, sizeof(LocalStatistics));
		initLocalStatistics(&ls);
		ls.timers_1[T1_STATISTICS] -= current_time;
		if (is_main_proc)
			output(0,
					"[Main] [SN %3d] Snapshot:       %d/%d, file %03d, a = %.5f, z = %.5f, t = %.5f\n",
					snap_idx, snap_idx + 1, config.n_snaps, config.snap_id[snap_idx],
					config.this_snap_a, config.this_snap_z, config.this_snap_t);
		current_time = MPI_Wtime();
		ls.timers_1[T1_STATISTICS] += current_time;

		/*
		 * Request descendant halos, read catalog, distribute connected and new halos.
		 */
		ls.timers_1[T1_CONNECTREQUESTS] -= current_time;
		MPI_Barrier(comm);
		output(3, "[%4d] [SN %3d] Connecting halos...\n", proc, snap_idx);
		setHaloStatusSnapStart(snap_idx, &halos);
		advanceSubhalos(snap_idx, &halos);
		connectHalos(snap_idx, &halos, &ls, gs);
		current_time = MPI_Wtime();
		ls.timers_1[T1_CONNECT] += current_time;

		/*
		 * Adjust domain boundaries. This step can be turned off or only be started after a certain
		 * number of snapshots.
		 */
		if ((config.lb_do_load_balancing) && (snap_idx >= config.lb_adjust_min_snap)
				&& (n_proc > 1))
		{
			ls.timers_1[T1_LB] -= current_time;
			output(3, "[%4d] [SN %3d] Load balancing...\n", proc, snap_idx);
			loadBalance(snap_idx, &halos, &ls_lastsnap);
			current_time = MPI_Wtime();
			ls.timers_1[T1_LB] += current_time;
		}

		/*
		 * Exchange halos between processors. While load balancing is optional, this step is not
		 * as subhalos and hosts have to be on the same process.
		 */
		ls.timers_1[T1_EXCHANGE] -= current_time;
		if (n_proc > 1)
		{
			output(3, "[%4d] [SN %3d] Exchanging halos...\n", proc, snap_idx);
			exchangeHalos(snap_idx, &halos, &ls, gs);
		}

		/*
		 * Above a certain log level, output information about the total memory used on each
		 * process after the exchange step.
		 */
		if (config.log_level >= 2)
		{
			printMemoryStatisticsProcess(&halos, snap_idx, "exchange", 1);
		}

		current_time = MPI_Wtime();
		ls.timers_1[T1_EXCHANGE] += current_time;

		/*
		 * Compute a search radius around each (host) halo, and from those create a box surrounding
		 * all halos.
		 */
		ls.timers_1[T1_HALOBOX] -= current_time;
		output(3, "[%4d] [SN %3d] Computing halo box...\n", proc, snap_idx);
		for (i = 0; i < halos.n; i++)
			setSearchRadius(&(halos.h[i]), snap_idx);
		computeHaloBox(&halos, &box_halos);
		ls.snpstats[SNPSTAT_VRATIO] = boxVolume(&box_halos)
				/ (pow(config.box_size, 3) / (float) n_proc);
		output(2,
				"[%4d] [SN %3d] Halo box [%6.2f..%6.2f  %6.2f..%6.2f  %6.2f..%6.2f  p %d %d %d], V-ratio %.2f.\n",
				proc, snap_idx, box_halos.min[0], box_halos.max[0], box_halos.min[1],
				box_halos.max[1], box_halos.min[2], box_halos.max[2], box_halos.periodic[0],
				box_halos.periodic[1], box_halos.periodic[2], ls.snpstats[SNPSTAT_VRATIO]);
		current_time = MPI_Wtime();
		ls.timers_1[T1_HALOBOX] += current_time;

		/*
		 * Read all particles within the halo box.
		 */
#if DO_READ_PARTICLES
		ls.timers_1[T1_READSNAPS] -= current_time;
		output(3, "[%4d] [SN %3d] Reading particles...\n", proc, snap_idx);
		readSnapshot(snap_idx, &box_halos, &particles, &n_particles, &n_files_read);
		output(3, "[%4d] [SN %3d] Read %d particles from %d files.\n", proc, snap_idx, n_particles,
				n_files_read);
		ls.snpstats[SNPSTAT_FILESREAD] = (double) n_files_read;
		current_time = MPI_Wtime();
		ls.timers_1[T1_READSNAPS] += current_time;
#else
		particles = NULL;
		n_particles = 0;
#endif

		/*
		 * Process halos and their particles in this snapshot separately on each proc. We also
		 * check the halo status for ended halos.
		 */
		ls.timers_1[T1_PROCESS] -= current_time;
		ls.timers_2[0] -= current_time;
		output(3, "[%4d] [SN %3d] Processing snapshot...\n", proc, snap_idx);
		processSnapshot(snap_idx, &halos, &box_halos, particles, n_particles, &ls);
		current_time = MPI_Wtime();
		ls.timers_1[T1_PROCESS] += current_time;
		ls.timers_1[T1_IDLE] -= current_time;

		/*
		 * To measure how long each processor spent idle, we insert a barrier. This does not slow
		 * the code down much as it is immediately followed by an MPI call anyway. Then we write
		 * ended halos to file.
		 */
		MPI_Barrier(comm);
		current_time = MPI_Wtime();
		ls.timers_1[T1_IDLE] += current_time;
		ls.timers_1[T1_SAVE] -= current_time;
		output(3, "[%4d] [SN %3d] Saving results...\n", proc, snap_idx);
		saveHaloResults(&halos, of, gs);
		current_time = MPI_Wtime();
		ls.timers_1[T1_SAVE] += current_time;

		/*
		 * Delete ended halos
		 */
		ls.timers_1[T1_DELETEHALOS] -= current_time;
		output(3, "[%4d] [SN %3d] Deleting ended halos...\n", proc, snap_idx);
		ls.snpstats[SNPSTAT_NDELETEDHALOS] = deleteEndedHalos(&halos);
		current_time = MPI_Wtime();
		ls.timers_1[T1_DELETEHALOS] += current_time;

		/*
		 * Collect all statistics. We need to collect the particle statistics for the first time,
		 * the snap statistics because they changed after the final halo update, and the timing
		 * statistics because they contain new fields. Note that the collectAllStatistics()
		 * function can only be executed here and once per snapshot, otherwise cumulative
		 * statistics will be double-counted.
		 */
		output(3, "[%4d] [SN %3d] Collecting statistics...\n", proc, snap_idx);
		collectAllStatistics(&halos, &ls, gs);

		/*
		 * Write restart files if necessary (if we have reached every nth snap, but not for the
		 * first and last snap)
		 */
		if (config.output_restart_files && (snap_idx > 0)
				&& (snap_idx % config.output_restart_every == 0)
				&& (snap_idx != config.n_snaps - 1))
		{
			ls.timers_1[T1_RESTARTFILES] -= current_time;
			output(3, "[%4d] [SN %3d] Writing restart files...\n", proc, snap_idx);
			readOrWriteRestartFiles(1, &snap_idx, gs, of, &ls, &ls_lastsnap, &halos);
			current_time = MPI_Wtime();
			ls.timers_1[T1_RESTARTFILES] += current_time;
		}

		/*
		 * Synchronize processes, print statistics. Note that we flush on every proc because there
		 * could be some output; if processes have little output, their output can be delayed by
		 * many snapshots (depending on the operating system).
		 */
		MPI_Barrier(comm);
		if (is_main_proc)
			printSnapStatistics(snap_idx, gs, 1);
		printMemoryStatistics(snap_idx, 0);
		if (is_main_proc)
			printLine(1);
		fflush(stdout);
	}

	/*
	 * At this point, all halos should have ended and been written to file if necessary.
	 */
	if (halos.n != 0)
		error(__FFL__, "Found halos at the end.\n");
	freeDynArray(__FFL__, &halos);

	/*
	 * Close the output files, record the final time.
	 */
	if (is_main_proc)
	{
		current_time = MPI_Wtime();
		gs->timers_0_cum[T0_SNAPS] += current_time;
		gs->timers_0_cum[T0_FINALIZE] = -current_time;
		closeOutputFile(of);
		memFree(__FFL__, MID_OUTPUTFILES, of, sizeof(OutputFile));
		gs->timers_0_cum[T0_FINALIZE] += MPI_Wtime();
	}
}

/*
 * This function contains the work on each snapshot that is independent for each process, i.e.
 * demands no communication.  Note that within this function, the halo array itself remains
 * unchanged after the sortArrays() function is called, meaning an index to the halo array remains
 * valid (e.g. the host index of subhalos), but only after that point.
 */
void processSnapshot(int snap_idx, DynArray *halos, Box *box_halos, Particle *particles,
		int n_particles, LocalStatistics *ls)
{
	int i;
	double current_time;
	Halo *halo, *host;
	Tree *tree = NULL;

	/*
	 * If there are no halos in the volume, there is nothing to do. We have to gracefully end the
	 * currently running timer, though.
	 */
	current_time = MPI_Wtime();
	if (halos->n == 0)
	{
		output(3, "[%4d] Got 0 halos total, ending snapshot.\n", proc);
		ls->timers_2[0] += current_time;
		return;
	}

	/*
	 * Pre-compute the time coordinates for the mass interpolation. Note that this
	 * array counts up rather than down, as GSL demands increasing coordinates.
	 */
#if DO_MASS_PROFILE
	int t_idx;

	for (i = 0; i < STCL_TCR; i++)
	{
		t_idx = snap_idx - STCL_TCR + i + 1;
		if (t_idx >= 0)
		{
			mass_profile_vars.grid_t[i] = (double) config.snap_t[t_idx];
		} else
		{
			mass_profile_vars.grid_t[i] = (double) INVALID_T;
		}
	}
#endif

	/*
	 * Build a tree with the particles. Note that the tree construction changes the order of the
	 * particle array, but we don't care since the particles don't come in any particular order in
	 * the first place. Note that the timing command needs to be outside the #if protected part
	 * because this is the first L2 timer and it was started in the function that called this
	 * function.
	 */
#if DO_READ_PARTICLES
	output(3, "[%4d] [SN %3d] Building tree...\n", proc, snap_idx);
	tree = treeInit(n_particles, particles);
	current_time = MPI_Wtime();
#endif
	ls->timers_2[T2_BUILDTREE] += current_time;

	/*
	 * Sort halos, their tracked particles and ignore IDs so that we can search the sorted
	 * arrays later.
	 */
	ls->timers_2[T2_SORT] -= current_time;
	output(3, "[%4d] [SN %3d] Sorting halos...\n", proc, snap_idx);
	sortArrays(halos);
	current_time = MPI_Wtime();
	ls->timers_2[T2_SORT] += current_time;

	/*
	 * Find those halos that will become subs in the next time step. Find their hosts, and mark
	 * the tracked particles in the host that are currently in the subhalo.
	 */
	ls->timers_2[T2_TAGSUBS] -= current_time;
	output(3, "[%4d] [SN %3d] Establishing subhalo relations...\n", proc, snap_idx);
	hostSubRelations(halos, tree, snap_idx, ls);
	current_time = MPI_Wtime();
	ls->timers_2[T2_TAGSUBS] += current_time;

	/*
	 * After exchanging and host-sub relations, we know all the halos on this process and how large
	 * their memory is. In some extreme cases, we might want to warn the user, for example, about
	 * halos that take up a lot of memory.
	 *
	 * We add this time to the halo loop counter, since it should be minor.
	 */
	ls->timers_2[T2_HALOLOOP] -= current_time;
	printHaloWarnings(snap_idx, halos);

	/*
	 * Now run over all halos, compute their mass profile, process their tracers and tracer results.
	 * Note that after this function has ended, the tracer etc arrays are not guaranteed to be
	 * sorted any more.
	 */
	output(3, "[%4d] [SN %3d] Processing halos...\n", proc, snap_idx);
	for (i = 0; i < halos->n; i++)
	{
		halo = &(halos->h[i]);
		if (halo->host_idx != -1)
			host = &(halos->h[halo->host_idx]);
		else
			host = NULL;
		processHalo(tree, halo, host, box_halos, snap_idx, ls);
	}
#if CAREFUL
	checkTracerStatistics(ls);
#endif

#if DO_READ_PARTICLES
	treeFree(&tree);
	memFree(__FFL__, MID_PARTICLES, particles, sizeof(Particle) * n_particles);
#endif
	current_time = MPI_Wtime();
	ls->timers_2[T2_HALOLOOP] += current_time;

	/*
	 * Update the halo status. In particular, find halos that we need to end so they can be
	 * analyzed and saved. Also, if a subhalo is becoming a host again, we separate it from its
	 * current host at this point.
	 */
	ls->timers_2[T2_FINDENDEDHALOS] -= current_time;
	output(3, "[%4d] [SN %3d] Updating halo status...\n", proc, snap_idx);
	setHaloStatusSnapEnd(snap_idx, halos);
	current_time = MPI_Wtime();
	ls->timers_2[T2_FINDENDEDHALOS] += current_time;

	/*
	 * Analyze results
	 */
	ls->timers_2[T2_HALOLOOP] -= current_time;
	output(3, "[%4d] [SN %3d] Running analyses...\n", proc, snap_idx);
	for (i = 0; i < halos->n; i++)
		runAnalyses2(snap_idx, &(halos->h[i]), ls);
	current_time = MPI_Wtime();
	ls->timers_2[T2_HALOLOOP] += current_time;
}

/*
 * This function executes the work that is separate for each halo, i.e., where halos do not anything
 * about each other any more. The exception is the host of the halo in the case of subhalos; for
 * hosts, this pointer should be NULL.
 *
 * Depending on what actions are performed, this function finds all particles in a halo, computes a
 * mass profile, and runs all tracer results and analyses (that work halo-by-halo).
 */
void processHalo(Tree *tree, Halo *halo, Halo *host, Box *box_halos, int snap_idx,
		LocalStatistics *ls)
{
	/*
	 * If we are not tracking particles in this halo, there is nothing to do here. This situation
	 * may or may not occur for subhalos, depending on the settings.
	 */
	if (!canHostParticles(halo))
		return;

	/*
	 * The halo can already have ended, for example because there was an issue with the halo
	 * catalog.
	 */
	if (hasEnded(halo))
		return;

	/*
	 * The halo loop timer needs to be stopped and started again at the end of the loop; if we exit
	 * above, it is stopped in the main loop.
	 */
	double current_time;

	current_time = MPI_Wtime();
	ls->timers_2[T2_HALOLOOP] += current_time;

	/*
	 * Debug output. This is only printed at a high log level, since it produces a lot of output.
	 */
	if (config.log_level >= 4)
	{
		output(4,
				"[%4d] Processing halo %ld, status %2d, host ID %8ld; memory %8.2f MB, current total %.1f MB.\n",
				proc, halo->cd.id, halo->status, (host == NULL) ? -1 : host->cd.id,
				MEGABYTE * haloMemory(halo),
				MEGABYTE * mem_stats.cur_tot);
	}
	debugHalo(halo, "Begin processing.");

	/* --------------------------------------------------------------------------------------------
	 * Part 1: Find particles for this halo
	 * -------------------------------------------------------------------------------------------*/

#if DO_READ_PARTICLES
	TreeResults *tr_res_all = NULL, *tr_res_tcr = NULL;
	float *ptl_radii = NULL;

	/*
	 * Find the particles in this halo in the particle tree.
	 */
	ls->timers_2[T2_TREESEARCH] -= current_time;

	if (isGhost(halo))
	{
		/*
		 * Ghost halos: Find particles. Note that the ghost can end here, if not enough of its
		 * tracers are found.
		 */
#if DO_GHOSTS
		tr_res_tcr = findHaloParticlesTracers(halo, snap_idx, tree, box_halos);
		if ((tr_res_tcr != NULL) && (tr_res_tcr->num_points < config.ghost_min_n200m))
		{
			debugHalo(halo, "Ending ghost, found only %d tracers (minimum %d).",
					tr_res_tcr->num_points, config.ghost_min_n200m);
			endHalo(halo, snap_idx, snap_idx - 1, HALO_ENDED_GHOST_NOT_FOUND);
		}
#endif
	} else if (usesAllParticles(halo))
	{
		/*
		 * Host halos: Find particles in this halo and compute R200m_all.
		 */
		findRadiusHost(halo, snap_idx, tree, box_halos);
	} else
	{
		/*
		 * If we are tracking subhalo particles, let's get the tracer particles and compute
		 * a tracer-based radius. However, this function can return NULL if no tracers exist or
		 * can be found, so we need to keep alternatives in mind.
		 */
#if DO_SUBHALO_TRACKING
		tr_res_tcr = findHaloParticlesTracers(halo, snap_idx, tree, box_halos);
#endif
	}

	current_time = MPI_Wtime();
	ls->timers_2[T2_TREESEARCH] += current_time;

	/*
	 * The halo can have ended in which case we have to abort gracefully. We do not count the
	 * processed particles into the statistics as that might cause an inconsistency.
	 */
	if (hasEnded(halo))
	{
		freeTempMemory(ptl_radii, tr_res_all, tr_res_tcr);
		current_time = MPI_Wtime();
		ls->timers_2[T2_HALOLOOP] -= current_time;
		return;
	}

	/* --------------------------------------------------------------------------------------------
	 * Part 2: Determine particle-based halo properties
	 * -------------------------------------------------------------------------------------------*/

#if DO_SUBHALO_TRACKING
	ls->timers_2[T2_GHOSTPROPS] -= current_time;

	/*
	 * For test purposes, we can apply the ghost logic to subhalos and compare the results.
	 * We perform this before the actual ghost properties are computed so that the original catalog
	 * data are intact for phantoms (for which they may be overwritten).
	 */
#if DO_GHOSTS && DEBUG_OUTPUT_GHOSTS
	if (isGhost(halo) || !usesAllParticles(halo))
	{
		if ((tr_res_tcr != NULL) && (halo->tt[PTL].tcr.n >= DEBUG_OUTPUT_GHOSTS_MIN_PTL))
			outputGhostDebugFile(halo, host, snap_idx, tr_res_tcr, tree);
	}
#endif

	/*
	 * For host halos, we already know the halo center and radius. If we are not tracking subhalo
	 * particles, we keep the radius from the previous snapshot (which was set earlier, so we do not
	 * need to do anything). Otherwise, we compute the halo center and radius as necessary.
	 */
	if (isGhost(halo))
	{
		/*
		 * For ghosts, we always need to compute basic halo properties at this point. The ghost
		 * properties function includes the radius.
		 */
#if DO_GHOSTS
		determineGhostProperties(halo, host, snap_idx, tr_res_tcr);
#endif
	} else if (!usesAllParticles(halo))
	{
		/*
		 * If phantom, calculate position like for ghost (but only if ghosts are active).
		 * Otherwise, we apply the same function as for all subhalos, i.e., we simply accept the
		 * catalog position and calculate the radius from tracers.
		 */
		if (halo->cd.phantom && config.halo_correct_phantoms && (tr_res_tcr != NULL)
				&& (tr_res_tcr->num_points >= config.ghost_min_n200m))
		{
#if DO_GHOSTS
			determinePhantomProperties(halo, host, snap_idx, tr_res_tcr);
#else
			computeR200mFromSubhaloTracers(snap_idx, halo, tr_res_tcr);
#endif
		} else
		{
			/*
			 * We attempt to measure R200m from the tracer distribution. If we found no tracers,
			 * tr_res_tcr is NULL but the subhalo R200m function knows how to deal with that.
			 */
			computeR200mFromSubhaloTracers(snap_idx, halo, tr_res_tcr);
		}
	}

	current_time = MPI_Wtime();
	ls->timers_2[T2_GHOSTPROPS] += current_time;
#endif

	/*
	 * Again, check if the halo has ended; for example, ghosts can have their center determination
	 * fail.
	 */
	if (hasEnded(halo))
	{
		freeTempMemory(ptl_radii, tr_res_all, tr_res_tcr);
		current_time = MPI_Wtime();
		ls->timers_2[T2_HALOLOOP] -= current_time;
		return;
	}

	/*
	 * If the halo needs an all-particle set, find those particles. The ptl_radii array
	 * contains the physical radii of the particles, which we can use to compute the mass
	 * profile. Note, however, that this array is NOT in the same order as the tree results
	 * any more. To avoid confusion, the array is thus not passed to any other functions,
	 * e.g., in analyses. It is safer to re-compute the radii there if necessary.
	 *
	 * If desired, we compare against the catalog R200m, but only for host halos where such
	 * a comparison makes sense.
	 */
	ls->timers_2[T2_MASSPROFILE] -= current_time;
	if (halo->needs_all_ptl)
		findHaloParticles(halo, snap_idx, tree, box_halos, &tr_res_all, &ptl_radii);

#if DO_TRACER_PARTICLES
	TreeResults *tr_res_connect = NULL;

	if (tr_res_tcr != NULL)
		tr_res_connect = tr_res_tcr;
	else
		tr_res_connect = tr_res_all;
#endif

	/*
	 * For the next part, normal halos and subhalos/ghosts need to be treated somewhat differently.
	 * First, we safeguard against uninitialized interpolation of the mass profile.
	 */
#if DO_MASS_PROFILE
	mass_profile_vars.initialized = 0;
	if (hasMassProfile(halo))
		computeMassProfile(halo, snap_idx, ptl_radii, tr_res_all->num_points);
#endif
	current_time = MPI_Wtime();
	ls->timers_2[T2_MASSPROFILE] += current_time;

	/*
	 * Everything up to here was only done if DO_READ_PARTICLES
	 */
#endif

	/* --------------------------------------------------------------------------------------------
	 * Part 3: Connect and prepare all tracers, run the result routines. Then post-process the
	 * tracers to look for deleted ones etc.
	 * -------------------------------------------------------------------------------------------*/

#if DO_TRACER_PARTICLES
	ls->timers_2[T2_PTL_PRERESULTS] -= current_time;
	if (halo->instr_tcr[PTL])
		beforeTracerResultsParticles(halo, snap_idx, tree, tr_res_connect, &(ls->tt[PTL]));
	current_time = MPI_Wtime();
	ls->timers_2[T2_PTL_PRERESULTS] += current_time;
#endif
#if DO_TRACER_SUBHALOS
	ls->timers_2[T2_SHO_PRERESULTS] -= current_time;
	if (canHostSubhalos(halo) && (halo->instr_tcr[SHO]))
		beforeTracerResultsSubhalos(halo, snap_idx, &(ls->tt[SHO]));
	current_time = MPI_Wtime();
	ls->timers_2[T2_SHO_PRERESULTS] += current_time;
#endif

	/*
	 * Perform the actual analysis on the trajectories. We do this even for subhalos; their tracers
	 * carry the necessary information for each result to decide whether it should be applied.
	 *
	 * After this point, no results can be added any more so that the arrays stay sorted until the
	 * end of the snapshot, meaning they can be searched safely.
	 */
	ls->timers_2[T2_HALOLOOP] -= current_time;
	runResults(halo, snap_idx, ls);
	current_time = MPI_Wtime();
	ls->timers_2[T2_HALOLOOP] += current_time;

	/*
	 * Check for tracers that need to be deleted, either because all their analyses have finished
	 * or because there is some tracer-specific reason (e.g., particles have strayed too far from
	 * the halo). We do not delete the tracers yet because the individual tracer types might need
	 * to react to ending tracers in particular ways, such as putting them on an ignore list.
	 */
	ls->timers_2[T2_TCR_MARKENDING] -= current_time;
	markEndingTracers(halo, ls);
	current_time = MPI_Wtime();
	ls->timers_2[T2_TCR_MARKENDING] += current_time;

	/*
	 * Post-process tracers with routines individual to particular tracer types. Once again,
	 * subhalo tracers cannot exist in subhalos.
	 */
#if DO_TRACER_PARTICLES
	ls->timers_2[T2_PTL_POSTRESULTS] -= current_time;
	if (halo->instr_tcr[PTL])
		afterTracerResultsParticles(halo, snap_idx, &(ls->tt[PTL]));
	current_time = MPI_Wtime();
	ls->timers_2[T2_PTL_POSTRESULTS] += current_time;
#endif
#if DO_TRACER_SUBHALOS
	ls->timers_2[T2_SHO_POSTRESULTS] -= current_time;
	if (canHostSubhalos(halo) && (halo->instr_tcr[SHO]))
		afterTracerResultsSubhalos(halo, snap_idx, &(ls->tt[SHO]));
	current_time = MPI_Wtime();
	ls->timers_2[T2_SHO_POSTRESULTS] += current_time;
#endif

	/*
	 * Update the ignore lists with tracers that are to be deleted. Ghosts do not keep ignore
	 * lists, so there is no need to add to them.
	 */
	if (!isGhost(halo))
	{
		ls->timers_2[T2_TCR_IGNLIST] -= current_time;
		updateIgnoreLists(halo);
		current_time = MPI_Wtime();
		ls->timers_2[T2_TCR_IGNLIST] += current_time;
	}

	/* --------------------------------------------------------------------------------------------
	 * Part 4: Run analyses that use the particle data (as opposed to the ones that use only halo-
	 * based data; these ones get the tree results).
	 * -------------------------------------------------------------------------------------------*/

	ls->timers_2[T2_HALOLOOP] -= current_time;
#if DO_READ_PARTICLES
	runAnalyses1(snap_idx, halo, ls, tree, tr_res_all);
#else
	runAnalyses1(snap_idx, halo, ls, NULL, NULL);
#endif
	current_time = MPI_Wtime();
	ls->timers_2[T2_HALOLOOP] += current_time;

	/* --------------------------------------------------------------------------------------------
	 * Part 5: Clean up, actually delete tracer objects.
	 * -------------------------------------------------------------------------------------------*/

	ls->timers_2[T2_TCR_DELETE] -= current_time;
	deleteTracers(halo, ls);
	current_time = MPI_Wtime();
	ls->timers_2[T2_TCR_DELETE] += current_time;

	/*
	 * At this point, we can free the tree search results and other particle related data.
	 */
#if DO_READ_PARTICLES
	freeTempMemory(ptl_radii, tr_res_all, tr_res_tcr);
#endif

	/*
	 * Debug output
	 */
	debugHalo(halo, "Finished processing.");
	if (config.log_level >= 4)
	{
		output(4,
				"[%4d] Finished processing halo %ld (halo memory %8.2f MB, current total %.1f MB.\n",
				proc, halo->cd.id, MEGABYTE * haloMemory(halo),
				MEGABYTE * mem_stats.cur_tot);
	}

	current_time = MPI_Wtime();
	ls->timers_2[T2_HALOLOOP] -= current_time;
}

void freeTempMemory(float *p_radii, TreeResults *tr_res_all, TreeResults *tr_res_tcr)
{
	if (p_radii != NULL)
		memFree(__FFL__, MID_HALOLOGIC, p_radii, sizeof(float) * tr_res_all->num_points);
	if (tr_res_all != NULL)
		treeResultsFree(tr_res_all);
	if (tr_res_tcr != NULL)
		treeResultsFree(tr_res_tcr);
}
