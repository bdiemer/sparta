/*************************************************************************************************
 *
 * Tree implementation to compute gravitational potentials.
 *
 * (c) Peter Behroozi, Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _TREE_POTENTIAL_H_
#define _TREE_POTENTIAL_H_

#include "global_types.h"

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

/*
 * Representation of a point in the potential tree. The coordinates x are an input, pe is the
 * output.
 *
 * Note that the idx field is necessary because the tree unit shuffles the order of the potential
 * points. This field is used internally and should NOT be set by the user!
 */
typedef struct TreePotentialPoint
{
	int idx;
	float x[3];
	float pe;
} TreePotentialPoint;

typedef struct TreePotentialNode TreePotentialNode_;

typedef struct TreePotentialNode
{
	int num_points;
	int div_dim;
	float min[3];
	float max[3];
	float mass_center[3];
	float m;
	float dmin;
	TreePotentialNode_ *left, *right, *parent;
	TreePotentialPoint *points;
} TreePotentialNode;

typedef struct
{
	int num_points;
	int num_nodes;
	int allocated_nodes;
	int points_per_leaf;
	TreePotentialPoint *points;
	TreePotentialNode *root;
} TreePotential;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void printTree(TreePotential *pt);
void computeDirectPotential(TreePotentialPoint *p, int n, float ptl_mass, float force_res);
void computeTreePotential(TreePotentialPoint *p, int n, float ptl_mass, float force_res,
		float err_tol, int points_per_leaf);

#endif
