/*************************************************************************************************
 *
 * This unit implements a generic rectangular sub-box of a simulation box which may span
 * across the periodic boundaries.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _GEOMETRY_H_
#define _GEOMETRY_H_

#include "global_types.h"

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

/*
 * A general box bounding a certain volume or set of particles or halos. The min and max
 * coordinates must, at all times, be greater than 0 and smaller than Lbox. If the box crosses
 * the periodic boundary in one direction, the periodic flag must be set. In this case, max < min.
 *
 * If the ignore flag is set, something is wrong with this box (for example, a snapshot box
 * represents a corrupted file).
 */
typedef struct
{
	int ignore;
	int periodic[3];
	float min[3];
	float max[3];
} Box;

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

/*
 * The bounding boxes are in a 1D array with indices [n_snap * n_files_per_snap + n_file]
 */
#if DO_READ_PARTICLES
extern Box *snap_boxes;
#endif

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void setEmptyBox(Box *box);
void resetBox(Box *box);
void getSnapBoxes();
void getSnapBoxFilename(int snap_idx, char *str);
int boxesIntersect(Box *box1, Box *box2);
void intersectingSnapFiles(int snap_idx, Box *box, int **file_intersect, int *n_intersect);
int inBox(float x[3], Box *box);
int sphereInBox(float x[3], float r, Box *box);
float boxVolume(Box *box);
void periodicDistanceVector(float *x1, float *x2, float *vec_out);
float periodicDistance(float *x1, float *x2);
float distance(float *x1, float *x2);
float correctPeriodic(float x, float L);
void unitVector(float *x_in, float *x_out);;
float dotProduct(float *x, float *y);

#endif
