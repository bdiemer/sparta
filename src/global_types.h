/*************************************************************************************************
 *
 * This unit implements the data structures used throughout SPARTA.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _GLOBAL_TYPES_H_
#define _GLOBAL_TYPES_H_

#include <hdf5.h>

#include "global.h"

/*************************************************************************************************
 * FUNDAMENTAL DATA TYPES
 *************************************************************************************************/

/*
 * Data types used in SPARTA. Note that these types are also used in HDF5 output routines. Thus,
 * each type must have a corresponding type in HDF5, otherwise we would need to explicitly
 * convert the data to an HDF5 data type.
 *
 * While the integer types are intended to be of length 8/16/32/64 bits, their sizes could, in
 * principle, differ on some operating system as they are not tied to a particular length but to
 * a particular variable type as follows:
 *
 * DTYPE_INT8   -> int_least8_t
 * DTYPE_INT16  -> short int
 * DTYPE_INT32  -> int
 * DTYPE_INT64  -> long int
 * DTYPE_FLOAT  -> float
 * DTYPE_DOUBLE -> double
 * DTYPE_STRING -> indicates string type; not mappable on number
 *
 * For consistency, they should only ever be used to describe variables of those types.
 */
enum
{
	DTYPE_INVALID,
	DTYPE_INT8,
	DTYPE_INT16,
	DTYPE_INT32,
	DTYPE_INT64,
	DTYPE_FLOAT,
	DTYPE_DOUBLE,
	DTYPE_STRING
};

#define NDARRAY_MAX_DIM 3

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

enum
{
	HALO_REQUEST_STATUS_OPEN,
	HALO_REQUEST_STATUS_CLOSED,
	HALO_REQUEST_STATUS_FOUND,
	HALO_REQUEST_STATUS_NOT_FOUND
};

enum
{
	INSTR_OUT,
	INSTR_IN,
	INSTR_IN_NEW,
	INSTR_IN_REBORN,
	INSTR_SUBTRACK_NEW,
	INSTR_SUBTRACK_CONTD,
	INSTR_GHOST,
	INSTR_HAS_RES,
	INSTR_TCR_RECREATED,
	INSTR_N
};

/*
 * Available types for dynamic arrays
 */
enum
{
	DA_TYPE_INT,
	DA_TYPE_HALO,
	DA_TYPE_SUBPOINTER,
	DA_TYPE_ID,
	DA_TYPE_CD,
	DA_TYPE_TREQ,
	DA_TYPE_TRACER,
	DA_TYPE_RSIFL,
	DA_TYPE_RSSBK,
	DA_TYPE_RSTJY,
	DA_TYPE_RSOCT,
	DA_TYPE_ALRSP,
	DA_TYPE_ALPRF,
	DA_TYPE_ALHPS,
	DA_N_TYPES
};

/*************************************************************************************************
 * TYPE FORWARD DECLARATIONS
 *************************************************************************************************/

typedef long int ID;
typedef ID HaloID;
typedef ID ParticleID;

typedef struct Halo Halo_;
typedef struct SubPointer SubPointer_;
typedef struct Tracer Tracer_;
typedef struct TracerType TracerType_;
typedef struct ResultInfall ResultInfall_;
typedef struct ResultSplashback ResultSplashback_;
typedef struct ResultTrajectory ResultTrajectory_;
typedef struct ResultOrbitCount ResultOrbitCount_;
typedef struct AnalysisRsp AnalysisRsp_;
typedef struct AnalysisProfiles AnalysisProfiles_;
typedef struct AnalysisHaloProps AnalysisHaloProps_;
typedef struct HaloCatalogData HaloCatalogData_;
typedef struct HaloRequest HaloRequest_;
typedef struct TracerRequest TracerRequest_;

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

/*
 * This struct represents a dynamically allocated array that can contain variables of a number
 * of types. Note that a DynArray can hold INTMAX (2^31 ~ 2E9) elements, the size of the individual
 * elements can be up to INTMAX bytes (2 GB), but their product, i.e. the memory content of the
 * DynArray, can be LONGMAX bytes (on a 64-bit system).
 *
 * Important: one can *never* keep pointers to elements in DynArrays and expect them to be valid
 * after *any* operation on the DynArray! For example, a simple addition of an element can lead to
 * a realloc call which will render all pointers invalid. The indices of elements, however, are
 * preserved unless elements are deleted.
 *
 * Note that the addToDynArray() function is not public. The user should always call the specific
 * add() function for each type in order to avoid uninitialized elements.
 */
typedef struct
{
	int n;
	int n_alloc;
	int size;
	int_least8_t type;

	union
	{
		void *data;
		char *bytes;
		int *ints;
		Halo_ *h;
		SubPointer_ *sp;
		ID *ids;
		Tracer_ *tcr;
		ResultInfall_ *rs_ifl;
		ResultSplashback_ *rs_sbk;
		ResultTrajectory_ *rs_tjy;
		ResultOrbitCount_ *rs_oct;
		AnalysisRsp_ *al_rsp;
		AnalysisProfiles_ *al_prf;
		AnalysisHaloProps_ *al_hps;
		HaloCatalogData_ *cd;
		HaloRequest_ *hreq;
		TracerRequest_ *treq;
	};
} DynArray;

/*
 * A general container for an N-D array, where N can currently be 1, 2, or 3. This system is convenient
 * when handling multiple variables with different data types that should all be treated in a
 * consistent way. The idea is that the user should initialize the NDArray first to set all
 * variables to their defaults. Then, the user can set the fields that indicate the array contents,
 * and call the allocate() and free() functions as appropriate, which handle the rest.
 *
 * The memory layout for multi-D fields is always contiguous. In a non-contiguous assignment, we
 * would create an array of pointers that are individually allocated the second dimension. In the
 * contiguous way, the memory is reserved in one chunk and an array of pointers points to the
 * respective elements. The HDF5 library uses the contiguous way, so NDArrays that are initialized
 * from HDF5 datasets automatically come in this layout.
 */
typedef struct
{
	int_least8_t n_dim;
	int_least8_t dtype;
	int_least8_t allocated;

	int n[NDARRAY_MAX_DIM];

	union
	{
		void *q_void;
		char *q_bytes;
		int_least8_t *q_int8;
		short int *q_int16;
		int *q_int32;
		long int *q_int64;
		float *q_float;
		double *q_double;
	};

	union
	{
		void **q_void_2;
		char **q_bytes_2;
		int_least8_t **q_int8_2;
		short int **q_int16_2;
		int **q_int32_2;
		long int **q_int64_2;
		float **q_float_2;
		double **q_double_2;

		void ***q_void_3;
		char ***q_bytes_3;
		int_least8_t ***q_int8_3;
		short int ***q_int16_3;
		int ***q_int32_3;
		long int ***q_int64_3;
		float ***q_float_3;
		double ***q_double_3;
	};
} NDArray;

/*
 * This struct represents a dynamical tracer tracked by SPARTA, namely a particle or subhalo. In
 * this struct, units are physical (i.e. physical kpc/h, physical kpc/h/Gyr, and Gyr).
 *
 * The time ordering is time-forward within the stencil, i.e., r[0] is the radius at the oldest
 * snapshot, r[STCL_TCR - 1] the current radius.
 *
 * The meaning of the ID field changes depending on the tracer. For particles, the ID is constant
 * over all snapshots. For subhalos, this field denotes the original ID of the halo, whereas the
 * desc_id denotes its ID in later snapshots. These IDs are (and have to be!) unique with in one
 * type of tracer, and are used to identify the results belonging to this tracer.
 *
 * The status_entered field indicates whether the tracer ever entered within R200m (or at least was
 * considered part of the halo), which determines whether it is added to the ignore list when it is
 * deleted. Note that for re-created tracers, i.e., tracers that were added to the ignore list but
 * re-created nevertheless, this field refers to the current incarnation of the tracer. In a
 * "previous life", the tracer could have entered the halo even if status_entered == 0.
 *
 * For some tracer results, the tracer object need to carry additional information about particular
 * analysis types, i.e. information that will end up in a particular result, because that
 * information is available at a different point in time than when the result is computed. For
 * example, the subhalo mass ratio (smr) is available when a subhalo falls into a host, but that
 * is not necessarily the same time when all its particles fall into the host.
 *
 * NOTE: the ordering of the field matters for the way the compiler "packs" the struct. For
 * example, compilers tend to start a new 4-byte block for each new type of variable. Thus, it is
 * advantageous to group variables of the same type, even if that means multiple #if blocks.
 */
typedef struct Tracer
{
	// IDs
	ID id;
#if DO_TRACER_SUBHALOS
	ID desc_id;
#endif

	// 1-byte integers
	int_least8_t status;
	int_least8_t status_entered;
	int_least8_t status_rs[NRS];
#if DO_RESULT_SPLASHBACK
	int_least8_t n_up_crossings;
#endif
#if DO_RESULT_ORBITCOUNT
	int_least8_t oct_direction;
#endif

	// 2-byte integers
	short int first_snap;

	// floats and other 4-byte variables
	float r[STCL_TCR];
	float vr[STCL_TCR];
#if DO_TRACER_VT
	float vt[STCL_TCR];
#endif
#if DO_TRACER_INITIAL_ANGLE
	float theta_ini;
	float phi_ini;
#endif
#if DO_TRACER_POS
	float theta[STCL_TCR];
	float phi[STCL_TCR];
#endif
#if DO_TRACER_X
	float x[3];
#endif
#if DO_TRACER_V
	float v[3];
#endif
#if DO_TRACER_SMR
	float smr;
#endif
#if DO_RESULT_SPLASHBACK
	float r_min;
	float R_min;
#endif
} Tracer;

/*
 * This struct implements a type of tracers, such as particles or a subhalo. Not all of its
 * contents may be relevant for each type of tracer, but the overhead for empty arrays is very
 * small.
 */
typedef struct TracerType
{
	DynArray tcr;
	DynArray iid;
	DynArray rs[NRS];
} TracerType;

/*
 * A result for the infall of a tracer into a halo. Strictly speaking, the optional position should
 * be saved as theta/phi coordinates because the radius is given by the radius of the halo at the
 * time of infall. However, that radius is interpolated in time, meaning it is not totally obvious
 * how to reconstruct the position of infall.
 */
typedef struct ResultInfall
{
	ID tracer_id;
	int_least8_t born_in_halo;
	float tifl;
#if DO_RESULT_INFALL_SMR
	float smr;
#endif
#if DO_RESULT_INFALL_VRV200
	float vrv200;
#endif
#if DO_RESULT_INFALL_VTV200
	float vtv200;
#endif
#if DO_RESULT_INFALL_X
	float x[3];
#endif
} ResultInfall;

/*
 * A splashback event written to the output file. There are two fields which are always written,
 * namely the splashback time and radius. All other fields can be switched on and off.
 */
typedef struct ResultSplashback
{
	ID tracer_id;
	float tsp;
	float rsp;
#if DO_RESULT_SPLASHBACK_MSP
	float msp;
#endif
#if DO_RESULT_SPLASHBACK_RRM
	float rrm;
#endif
#if DO_RESULT_SPLASHBACK_POS
	float theta;
	float phi;
#endif
} ResultSplashback;

/*
 * A result type that represents the full trajectory of a tracer.
 */
typedef struct ResultTrajectory
{
	ID tracer_id;
	short int first_snap;
	short int last_snap;

#if DO_RESULT_TRAJECTORY_R
	float r[MAX_SNAPS];
#endif
#if DO_RESULT_TRAJECTORY_VR
	float vr[MAX_SNAPS];
#endif
#if DO_RESULT_TRAJECTORY_VT
	float vt[MAX_SNAPS];
#endif
#if DO_RESULT_TRAJECTORY_X
	float x[MAX_SNAPS][3];
#endif
#if DO_RESULT_TRAJECTORY_V
	float v[MAX_SNAPS][3];
#endif
} ResultTrajectory;

/*
 * A result that keeps track of the orbit counter. The union fields have different meanings
 * depending on where we have found any pericenters yet and whether the number of pericenters is
 * a lower limit. If the orbit counter is zero and lower_limit is False:
 *
 * infall_snap             The snapshot where the tracer first entered the halo
 * n_crossings             The number of times there has been a switching in the radial velocity
 *                         (but no well-defined pericenter)
 *
 * If n_pericenter is zero but lower limit is True:
 *
 * lower_limit_snap        The snapshot where the counter became a lower limit (i.e., same meaning
 *                         as for last_pericenter_snap, but distinguished by name for clarity.
 *
 * If n_pericenter is greater than zero:
 *
 * last_pericenter_snap    The snapshot where the last pericenter was detected
 * is_final                Whether the pericenter count has reached its final value (because it
 *                         reached the maximum count, for example)
 */
typedef struct ResultOrbitCount
{
	ID tracer_id;
	short int n_pericenter;
	union
	{
		short int infall_snap;
		short int last_pericenter_snap;
		short int lower_limit_snap;
	};
	int_least8_t n_is_lower_limit;
	union
	{
		int_least8_t n_crossings;
		int_least8_t is_final;
	};
} ResultOrbitCount;

/*
 * The output of an analysis of the splashback radius and mass of a halo. The halo ID field may
 * seem wasteful since halos keep track of their own analyses until they are saved. However, the
 * analyses might refer to different times in a halo's history, and thus different IDs.
 *
 * The definitions can be either R, M, or their respective errors, as determined by the
 * HaloDefinition identifiers set in the config.
 */
typedef struct AnalysisRsp
{
	HaloID halo_id;
	int_least8_t status[ANALYSIS_RSP_MAX_SNAPS];
	float defs[ANALYSIS_RSP_MAX_DEFINITIONS][ANALYSIS_RSP_MAX_SNAPS];
} AnalysisRsp;

typedef struct AnalysisProfiles
{
	HaloID halo_id;
	int_least8_t status[ANALYSIS_PROFILES_MAX_SNAPS];
#if DO_ANALYSIS_PROFILES_ALL
	float M_all[ANALYSIS_PROFILES_MAX_SNAPS][ANALYSIS_PROFILES_N_BINS];
#endif
#if DO_ANALYSIS_PROFILES_1HALO
	float M_1halo[ANALYSIS_PROFILES_MAX_SNAPS][ANALYSIS_PROFILES_N_BINS];
#endif
} AnalysisProfiles;

typedef struct AnalysisHaloProps
{
	HaloID halo_id;
	float defs[ANALYSIS_HALOPROPS_MAX_DEFINITIONS][ANALYSIS_HALOPROPS_MAX_SNAPS];
	int_least8_t status[ANALYSIS_HALOPROPS_MAX_DEFINITIONS][ANALYSIS_HALOPROPS_MAX_SNAPS];
} AnalysisHaloProps;

/*
 * This struct represents a line in a halo catalog file. It does not contain all the information
 * we track for each halo, but the information returned by the catalog reader. Any supported
 * catalog format must be able to provide these fields, exceptions are noted as optional. All
 * positions and radii are in comoving Mpc/h, and velocities in km/s. The fields are:
 *
 * id               Halo ID
 * pid              Parent ID; -1 if halo is host
 * desc_id          Descendant ID
 * desc_pid         Parent ID of descendant (tells us whether the halo will be a subhalo in the
 *                  next snap)
 * request_status   Internal status field for sending data between procs
 * mmp              Most massive progenitor of its descendant?
 * phantom          Has this halo been reconstructed even though it could not be found? For such
 *                  halos, the position is typically inaccurate and should be used with care, but
 *                  they are useful in order not to interrupt halo histories.
 * proc             The process to which this data is sent
 * x                Position of the halo in the box
 * v                Velocity of the halo center
 * R200m_all_com    This is a tricky field. If possible, this should be set to something as close
 *                  as possible to all-particle (i.e., strict-SO) R200m as determined by SPARTA.
 *                  For subhalos, this quantity makes no sense and should not be used. For host
 *                  halos, SPARTA does its own calculation but needs the catalog value as an
 *                  initial guess, and for checking its own value. For ghosts, this radius is
 *                  never defined.
 * M_bound          The bound-only M200m mass of the halo. If only other mass definitions are
 *                  available, that is OK. This mass should be used ONLY to compute a sub-to-host
 *                  mass ratio. Thus, the difference to an M200m-based ratio should be small-ish.
 * scale_radius_com (optional) only used for a particular way of computing bounded-ness; a halo
 *                  finder implementation may not return this value.
 *
 * The order of elements in this struct may matter for memory padding!
 */
typedef struct HaloCatalogData
{
	HaloID id;
	HaloID pid;
	HaloID desc_id;
	HaloID desc_pid;
	int_least8_t request_status;
	int_least8_t mmp;
	int_least8_t phantom;
	short int proc;
	float x[3];
	float v[3];
	float R200m_cat_com;
	float M_bound;
} HaloCatalogData;

/*
 * This struct represents the full information that is kept for each halo, whether it is active
 * or not. Only the process that owns a given halo needs to keep this information, all catalog
 * information is exchanged via the catalog struct above. The catalog struct in this struct always
 * contains the catalog information from the most recent snapshot.
 *
 * Each tracer, i.e. particles and subhalos, as well as their ignore IDs and results, are kept in
 * separate dynamic arrays because their ID spaces overlap, meaning it is not always possible to
 * uniquely distinguish a particle ID from a halo ID.
 *
 * Each halo keeps a set of instructions that determine which tracers are active for this halo
 * (all by default) and which results operate on each tracer. The former is derived by checking
 * whether any analysis is performed on a given tracer for this halo. If not, the tracer is
 * turned off altogether. These instructions are set once when the halo is created, and should
 * never be modified thereafter.
 *
 * The length units in this struct are mixed. The maximum search radius is in comoving
 * Mpc/h, whereas the history of the halo radii as well as all tracked particle and result
 * information is in physical units. The catalog data position and history_x are in comoving box
 * coordinates, so one must take care with periodic boundary conditions when using these
 * fields. The search radii have the following meaning:
 *
 * r_search_factor       This factor times R200m is the radius to which we need to find all
 *                       particles.
 * r_search_com_guess    A guess for the comoving search radius, given that we do not yet know the
 *                       actual R200m of the halo.
 * r_search_com_initial  A guess for R200m for the first iteration of the particle finder; this
 *                       should be close to the actual R200m for performance reasons.
 * r_search_com_actual   The actual search radius achieved. This should almost always be equal to
 *                       r_search_factor * R200m, but in the rare cases where it is not we need to
 *                       react (e.g., by setting certain error flags).
 *
 * The ordering in the grid_M array is [i_t * N_MBINS + i_mass_bin]. The array is defined
 * one-dimensionally to accommodate certain GSL interpolation routines.
 *
 * The history fields have different offsets; fields that go to MAX_SNAPS follow the default
 * snapshot ordering of the simulation. Fields that go to STCL_HALO contain values at STCL_HALO
 * snapshots where the last element is the last (not current!) snapshot, meaning they go
 * [snap_idx - STCL_HALO .. snap_idx - 1].
 *
 * Note: The order of elements in this struct may matter for memory padding!
 */
typedef struct Halo
{
	int_least8_t status;

	int_least8_t needs_all_ptl;
	int_least8_t search_radius_decreased;
	int_least8_t radius_from_catalog;
	int_least8_t updated_xv;

	int_least8_t instr_tcr[NTT];
	int_least8_t instr_rs[NTT][NRS];
	int_least8_t history_status[MAX_SNAPS];

	short int first_snap;
	short int last_snap;

	int host_idx;

	HaloID id_sort;
	HaloID history_id[MAX_SNAPS];
#if OUTPUT_HALO_PARENT_ID
	HaloID history_pid[MAX_SNAPS];
#endif

	float r_search_factor;
	float r_search_com_guess;
	float r_search_com_initial;
	float r_search_com_actual;

	float M_bound_peak;
	float history_R200m[MAX_SNAPS];
	float history_x[STCL_HALO_X][3];
	float history_v[STCL_HALO_V][3];

#if DO_MASS_PROFILE
	float grid_M200m[STCL_TCR];
	float grid_M[(N_MBINS * STCL_TCR)];
#endif

	HaloCatalogData cd;
	DynArray subs;
	TracerType tt[NTT];
	DynArray al[NAL];
} Halo;

typedef struct SubPointer
{
	short int first_snap;
	int idx;
	HaloID id;
	HaloID desc_id;
} SubPointer;

typedef struct HaloRequest
{
	HaloID id;
	short int proc;
	short int status;
} HaloRequest;

/*
 * This struct generally connects an ID with a traced object.
 */
typedef struct TracerRequest
{
	ID id;
	int halo_idx;
	int tcr_idx;
} TracerRequest;

/*
 * General particle struct. The particle is a global struct in the sense that it is not unique to
 * a particular part of the code: it is used by the IO routines, the tree etc. In this struct,
 * units are comoving, i.e. comoving Mpc/h and km/s.
 */
typedef struct
{
	long int id;
	float x[3];
	float v[3];
} Particle;

/*
 * This type is a simplified version of a full output dataset for the HDF5 output. It is used for
 * results and analyses to set their output fields which can then be processed by the output unit
 * in a fully automatic fashion.
 *
 * The n_dims and dim_ext fields need to be set only by outputs with dim>1.
 */
typedef struct OutputField
{
	char name[50];
	int dtype;
	int offset;
	int n_dims;
	int dim_ext[N_DSET_DIM_MAX];
} OutputField;

/*
 * Global properties of tracer types. Most importantly, this struct carries instructions for what
 * results to attempt when tracers are created under different circumstances. See the
 * initialization in global_types.c for details on the various scenarios.
 */
typedef struct TracerTypeProperties
{
	char *name_long;
	char *name_short;
	int_least8_t do_output_tcr;
	int_least8_t do_output_rs[NRS];
	int_least8_t instr[NRS][INSTR_N];
} TracerTypeProperties;

/*
 * Each result has a run() function that takes the number of previous results of this type and
 * the result statistics as the last two arguments. The latter needs to be a void pointer here
 * because the statistics type has not yet been defined.
 */
typedef struct ResultTypeProperties
{
	char *name_long;
	char *name_short;
	int da_type;
	int size;
	void (*initConfig)(int);
	void (*runResult)(int, Halo*, Tracer*, TracerType*, int, void*);
	int (*checkRecreateIgnoredTracer)(Halo*, int, ID, int);
	int (*outputFields)(OutputField*, int);
	void (*outputConfig)(hid_t);
} ResultTypeProperties;

/*
 * Each analysis can be run in two different places: while the halo is being processed and the
 * particle results are accessible, and after all halos have been processed. At that point, the
 * final status of the halo and all tracers is available, but the particle data are not. The void
 * pointers in runAnalysis1() refer to analysis statistics, tree, tree results, and particle radii.
 * Each analysis can pass NULL for either of those functions in which case they are being ignored.
 */
typedef struct AnalysisTypeProperties
{
	char *name_long;
	char *name_short;
	int da_type;
	int size;
	int do_output;
	void (*initConfig)(int);
	void (*runAnalysis1)(int, Halo*, void*, void*, void*);
	void (*runAnalysis2)(int, Halo*, void*);
	int (*outputFields)(OutputField*, int);
	void (*outputConfig)(hid_t);
	float (*searchRadius)(int, Halo*);
} AnalysisTypeProperties;

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

extern const TracerTypeProperties ttprops[NTT];
extern const ResultTypeProperties rsprops[NRS];
extern const AnalysisTypeProperties alprops[NAL];

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

// Output
void printTypeSizes();

size_t datatypeSize(int dtype);

// Initialization
void initMPITypes();

// General dynamic array functions
void initDynArray(const char *file, const char *func, int line, DynArray *da, int type);
void initDynArrayN(const char *file, const char *func, int line, DynArray *da, int type, int N);
void resetDynArray(const char *file, const char *func, int line, DynArray *da);
int dynArrayWasFreed(DynArray *da);
void deleteDynArrayItem(const char *file, const char *func, int line, DynArray *da, int idx);
void deleteDynArrayItems(const char *file, const char *func, int line, DynArray *da,
		int_least8_t *mask);
void checkDynArray(const char *file, const char *func, int line, DynArray *da);
void compressDynArray(const char *file, const char *func, int line, DynArray *da);
void freeDynArray(const char *file, const char *func, int line, DynArray *da);

// Adding to dynamic arrays and initialization
int* addInt(const char *file, const char *func, int line, DynArray *da);
Halo* addHalo(const char *file, const char *func, int line, DynArray *da, int init_dyn_arrays);
SubPointer* addSubPointer(const char *file, const char *func, int line, DynArray *da);
ID* addId(const char *file, const char *func, int line, DynArray *da);
Tracer* addTracer(const char *file, const char *func, int line, DynArray *da);
HaloCatalogData* addHaloCatalogData(const char *file, const char *func, int line, DynArray *da);
TracerRequest* addTracerRequest(const char *file, const char *func, int line, DynArray *da);

#if DO_RESULT_INFALL
ResultInfall* addResultInfall(const char *file, const char *func, int line, DynArray *da);
#endif
#if DO_RESULT_SPLASHBACK
ResultSplashback* addResultSplashback(const char *file, const char *func, int line, DynArray *da);
#endif
#if DO_RESULT_TRAJECTORY
ResultTrajectory* addResultTrajectory(const char *file, const char *func, int line, DynArray *da);
#endif
#if DO_RESULT_ORBITCOUNT
ResultOrbitCount* addResultOrbitCount(const char *file, const char *func, int line, DynArray *da);
#endif
#if DO_ANALYSIS_RSP
AnalysisRsp* addAnalysisRsp(const char *file, const char *func, int line, DynArray *da);
#endif
#if DO_ANALYSIS_PROFILES
AnalysisProfiles* addAnalysisProfiles(const char *file, const char *func, int line, DynArray *da);
#endif
#if DO_ANALYSIS_HALOPROPS
AnalysisHaloProps* addAnalysisHaloProps(const char *file, const char *func, int line, DynArray *da);
#endif

void initId(ID *id);

// NDArrays
void initializeNDArray(NDArray *nda);
size_t sizeOfNDArray(NDArray *nda);
void allocateNDArray(const char *file, const char *func, int line, int mem_id, NDArray *nda);
void freeNDArray(const char *file, const char *func, int line, int mem_id, NDArray *nda);

#endif
