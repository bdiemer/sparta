/*************************************************************************************************
 *
 * This unit implements cosmology functions.
 *
 * Part of the code was adapted from Matt Becker's CosmoCalc module.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf.h>

#include "global.h"
#include "cosmology.h"
#include "constants.h"
#include "utils.h"

/**************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

enum
{
	INTEGRAL_TYPE_ONEOVEREZ, INTEGRAL_TYPE_ONEOVEREZ1PZ, INTEGRAL_TYPE_ONEOVEREZTIMES1PZ
};

/**************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct
{
	double z;
	double R;
	Cosmology *cosmo;
	double target_sigma;
	int j;
} PowerSpectrumParams;

typedef struct
{
	Cosmology *cosmo;
	double z_minmax;
	int find_min;
	int integral_type;
	double ez_integral_target;
} EzIntegralParams;

/**************************************************************************************************
 * INTERNAL FUNCTIONS
 *************************************************************************************************/

double EzIntegral(Cosmology *cosmo, double z_min, double z_max, int integral_type);
double EzIntegralInverse(Cosmology *cosmo, double ez_integral, double z_minmax, int find_min,
		int integral_type);

double sigmaUnnormalized(Cosmology *cosmo, double R, int j);

/**************************************************************************************************
 * COSMOLOGY PARAMETERS
 *************************************************************************************************/

void initCosmology(Cosmology *cosmo)
{
	cosmo->flat = 1;
	cosmo->h = 0.0;
	cosmo->Omega_m = 1.0;
	cosmo->Omega_b = 0.0;
	cosmo->Omega_L = 0.0;
	cosmo->Omega_r = 0.0;
	cosmo->w = -1;
	cosmo->ns = 1.0;
	cosmo->sigma8 = 0.0;
	cosmo->T_CMB = 2.725;

	resetCosmologySplines(cosmo);
}

/*
 * Return a cosmology object from presets. See header file for documentation.
 */
Cosmology getCosmology(int cosmology)
{
	Cosmology result;

	initCosmology(&result);

	result.ps_type = POWERSPECTRUM_TYPE_EH98;
	result.ps_filter = FILTER_TOPHAT;
	result.ps_norm = -1.0;

	switch (cosmology)
	{
	case COSMOLOGY_NONE:
		result.flat = 1;
		result.h = 0.0;
		result.Omega_m = 0.0;
		result.Omega_b = 0.0;
		result.Omega_L = 0.0;
		result.Omega_r = 0.0;
		result.w = -1;
		result.ns = 0.0;
		result.sigma8 = 0.0;
		result.T_CMB = 2.725;
		break;
	case COSMOLOGY_BOLSHOI:
		result.flat = 1;
		result.h = 0.7;
		result.Omega_m = 0.27;
		result.Omega_b = 0.0469;
		result.Omega_L = 0.73;
		result.Omega_r = 0.0;
		result.w = -1;
		result.ns = 0.95;
		result.sigma8 = 0.82;
		result.T_CMB = 2.725;
		break;
	case COSMOLOGY_WMAP9:
		result.flat = 1;
		result.h = 0.6933;
		result.Omega_m = 0.288;
		result.Omega_b = 0.0472;
		result.Omega_L = 0.712;
		result.Omega_r = 0.0;
		result.w = -1;
		result.ns = 0.971;
		result.sigma8 = 0.83;
		result.T_CMB = 2.725;
		break;
	case COSMOLOGY_PLANCK:
		result.flat = 1;
		result.h = 0.67;
		result.Omega_m = 0.32;
		result.Omega_b = 0.0491;
		result.Omega_L = 0.68;
		result.Omega_r = 0.0;
		result.w = -1;
		result.ns = 0.9624;
		result.sigma8 = 0.834;
		result.T_CMB = 2.725;
		break;
	default:
		error(__FFL__, "Unknown cosmology %d.\n", cosmology);
		break;
	}

	resetCosmology(&result);

	return result;
}

/*
 * Return a self-similar power-law cosmology. Here, ns = n takes on the role of the power spectrum
 * slope. Note that the user must set h and sigma8 to obtain a useful cosmology.
 */
Cosmology getCosmologyPowerLaw(int cosmology, double n)
{
	Cosmology result;

	initCosmology(&result);

	result.ps_type = POWERSPECTRUM_TYPE_POWERLAW;
	result.ps_filter = FILTER_TOPHAT;
	result.ps_norm = -1.0;

	resetCosmology(&result);

	return result;
}

/*
 * If parameters of a cosmology are changed, this function MUST be called in order to avoid using
 * outdated information.
 */
void resetCosmology(Cosmology *cosmo)
{
	cosmo->interpolate = 1;
	cosmo->spline_z_D_initialized = 0;
	cosmo->spline_r_sigma_initialized = 0;
}

/*
 * Note that the splines being allocated is not the same as the interpolation tables being set.
 */
void resetCosmologySplines(Cosmology *cosmo)
{
	cosmo->spline_z_D = NULL;
	cosmo->spline_z_D_acc = NULL;

	cosmo->spline_r_sigma = NULL;
	cosmo->spline_r_sigma_acc = NULL;
	cosmo->spline_sigma_r = NULL;
	cosmo->spline_sigma_r_acc = NULL;
}

void freeCosmology(Cosmology *cosmo)
{
	if (cosmo->spline_r_sigma != NULL)
		gsl_spline_free(cosmo->spline_r_sigma);
	if (cosmo->spline_r_sigma_acc != NULL)
		gsl_interp_accel_free(cosmo->spline_r_sigma_acc);
}

/**************************************************************************************************
 * E(z) ROUTINES
 *************************************************************************************************/

/*
 * The normalized Hubble constant, E(z).
 */
double Ez(Cosmology *cosmo, double z)
{
	double result;

	if (cosmo->flat)
	{
		result = pow(
				cosmo->Omega_r * pow(1 + z, 4) + cosmo->Omega_m * pow(1 + z, 3)
						+ (1 - cosmo->Omega_m - cosmo->Omega_r) * pow(1 + z, -3 * (1 + cosmo->w)),
				0.5);
	} else
	{
		result = pow(
				cosmo->Omega_r * pow(1 + z, 4) + cosmo->Omega_m * pow(1 + z, 3)
						+ cosmo->Omega_L * pow(1 + z, 3 * (1 + cosmo->w))
						+ (1 - cosmo->Omega_m - cosmo->Omega_r - cosmo->Omega_L) * pow(1 + z, 2),
				0.5);
	}

	return result;
}

/*
 * 1/E(z) for a given redshift and cosmology.
 */
double oneOverEz(Cosmology *cosmo, double z)
{
	double result;

	result = 1.0 / Ez(cosmo, z);

	return result;
}

double oneOverEzVoid(double x, void *params)
{
	Cosmology *cosmo = (Cosmology*) params;

	return oneOverEz(cosmo, x);
}

double oneOverEz1pzVoid(double x, void *params)
{
	Cosmology *cosmo = (Cosmology*) params;

	return oneOverEz(cosmo, x) / (1.0 + x);
}

double oneOverEzTimes1pzVoid(double x, void *params)
{
	Cosmology *cosmo = (Cosmology*) params;

	return pow(oneOverEz(cosmo, x), 3) * (1.0 + x);
}

/*
 * General integration function which can be fed 1/E(z), 1/[E(z)(1+z)] etc. In principle, the
 * function can take any redshift, including negative redshifts.
 */
double EzIntegral(Cosmology *cosmo, double z_min, double z_max, int integral_type)
{
	gsl_integration_workspace *workspace;
	gsl_function F;
	double abserr, result;
	const int WORKSPACE_NUM = 10000;

	workspace = gsl_integration_workspace_alloc((size_t) WORKSPACE_NUM);
	F.params = cosmo;

	switch (integral_type)
	{
	case INTEGRAL_TYPE_ONEOVEREZ:
		F.function = &oneOverEzVoid;
		break;
	case INTEGRAL_TYPE_ONEOVEREZ1PZ:
		F.function = &oneOverEz1pzVoid;
		break;
	case INTEGRAL_TYPE_ONEOVEREZTIMES1PZ:
		F.function = &oneOverEzTimes1pzVoid;
		break;
	default:
		error(__FFL__, "Unknown integral type, %d.\n", integral_type);
		break;
	}

	gsl_integration_qag(&F, z_min, z_max, 1e-10, 1e-5, (size_t) WORKSPACE_NUM, 1, workspace,
			&result, &abserr);

	return result;
}

/*
 * This function serves as the equation to set to zero when inverting an Ez integral.
 */
double EzIntegralWrapper(double x, void *params)
{
	double z_min, z_max;
	EzIntegralParams *p = (EzIntegralParams*) params;

	if (p->find_min)
	{
		z_min = x;
		z_max = p->z_minmax;
	} else
	{
		z_max = x;
		z_min = p->z_minmax;
	}

	return EzIntegral(p->cosmo, z_min, z_max, p->integral_type) - p->ez_integral_target;
}

/*
 * Find the zmin or zmax that gives a particular value of the Ez integral function above, given a
 * fixed zmin or zmax.
 */
double EzIntegralInverse(Cosmology *cosmo, double ez_integral, double z_minmax, int find_min,
		int integral_type)
{
	const int MAX_ITER = 100;
	const double ACCURACY = 1E-5;

	double zz, zz_min, zz_max;
	int i;
	int status;
	const gsl_root_fsolver_type *TSolver;
	gsl_root_fsolver *s;
	gsl_function F;
	EzIntegralParams solverParams;

	solverParams.z_minmax = z_minmax;
	solverParams.find_min = find_min;
	solverParams.integral_type = integral_type;
	solverParams.ez_integral_target = ez_integral;
	solverParams.cosmo = cosmo;
	F.function = &EzIntegralWrapper;
	F.params = &solverParams;
	TSolver = gsl_root_fsolver_brent;
	s = gsl_root_fsolver_alloc(TSolver);

	zz_min = COSMO_Z_MIN;
	zz_max = COSMO_Z_MAX;
	gsl_root_fsolver_set(s, &F, zz_min, zz_max);

	i = 0;
	do
	{
		i++;
		status = gsl_root_fsolver_iterate(s);
		zz = gsl_root_fsolver_root(s);
		zz_min = gsl_root_fsolver_x_lower(s);
		zz_max = gsl_root_fsolver_x_upper(s);
		status = gsl_root_test_interval(zz_min, zz_max, 0, ACCURACY);
	} while (status == GSL_CONTINUE && i < MAX_ITER);

	return zz;
}

/**************************************************************************************************
 * COSMOLOGICAL TIMES
 *************************************************************************************************/

/*
 * Returns 1/H0 in units of Gyr, since we usually use 1/H0 to express a time.
 */
double oneOverH0(Cosmology *cosmo)
{
	return 1E-16 * AST_Mpc / (AST_year * cosmo->h);
}

/*
 * The lookback time in units of Gyr.
 */
double lookbackTime(Cosmology *cosmo, double z)
{
	return oneOverH0(cosmo) * EzIntegral(cosmo, 0.0, z, INTEGRAL_TYPE_ONEOVEREZ1PZ);
}

/*
 * The lookback time in units of Gyr.
 */
double lookbackTimeInverse(Cosmology *cosmo, double t)
{
	return EzIntegralInverse(cosmo, t / oneOverH0(cosmo), 0.0, 0, INTEGRAL_TYPE_ONEOVEREZ1PZ);
}

/*
 * The age of the universe in units of Gyr.
 */
double age(Cosmology *cosmo, double z)
{
	return oneOverH0(cosmo) * EzIntegral(cosmo, z, COSMO_Z_MAX, INTEGRAL_TYPE_ONEOVEREZ1PZ);
}

/*
 * The redshift when the universe was a certain age in Gyr.
 */
double ageInverse(Cosmology *cosmo, double t)
{
	return EzIntegralInverse(cosmo, t / oneOverH0(cosmo), COSMO_Z_MAX, 1,
			INTEGRAL_TYPE_ONEOVEREZ1PZ);
}

/*
 * The Hubble time.
 */
double hubbleTime(Cosmology *cosmo, double z)
{
	return oneOverH0(cosmo) / Ez(cosmo, z);
}

/**************************************************************************************************
 * COSMOLOGICAL DISTANCES AND VOLUMES
 *************************************************************************************************/

/*
 * Returns the comoving distance in Mpc for a flat universe, given a cosmology. Erroneous input
 * is checked in oneOverEzIntegral. All the following functions come in two versions which return
 * Mpc, and Mpc / h respectively.
 */
double comovingDistanceMpch(Cosmology *cosmo, double z_min, double z_max)
{
	return EzIntegral(cosmo, z_min, z_max, INTEGRAL_TYPE_ONEOVEREZ) * PHY_c / (100 * 100000);
}

double comovingDistanceMpch0(Cosmology *cosmo, double z)
{
	return comovingDistanceMpch(cosmo, 0, z);
}

double comovingDistance(Cosmology *cosmo, double z)
{
	return comovingDistanceMpch0(cosmo, z) / cosmo->h;
}

/*
 * Returns the luminosity distance in Mpc for a flat universe. Erroneous input is checked in
 * oneOverEzIntegral.
 */
double luminosityDistanceMpch(Cosmology *cosmo, double z)
{
	return comovingDistanceMpch0(cosmo, z) * (1 + z);
}

double luminosityDistance(Cosmology *cosmo, double z)
{
	return luminosityDistanceMpch(cosmo, z) / cosmo->h;
}

/*
 * Returns the angular diameter distance in Mpc for a flat universe. Erroneous input is checked
 * in oneOverEzIntegral.
 */
double angDiamDistanceMpch(Cosmology *cosmo, double z)
{
	return comovingDistanceMpch0(cosmo, z) / (1 + z);
}

double angDiamDistance(Cosmology *cosmo, double z)
{
	return angDiamDistanceMpch(cosmo, z) / cosmo->h;
}

/*
 * Returns the distance modulus in a flat universe. Erroneous input is checked in
 * oneOverEzIntegral.
 */
double distanceModulus(Cosmology *cosmo, double z)
{
	return 5 * log10(luminosityDistance(cosmo, z) / 1E-5);
}

/*
 * Get Vmax as the comoving volume in Gpc^3 for a flat universe, 4/3 pi * d_proper^3 where the
 * comoving distance in given in Mpc. Erroneous input is checked in oneOverEzIntegral.
 */
double comovingVolume(Cosmology *cosmo, double z)
{
	double result = 0;
	double dc;

	dc = comovingDistance(cosmo, z);

	result = 4.0 / 3.0 * MATH_pi * pow(dc / 1000.0, 3);

	return result;
}

/**************************************************************************************************
 * DENSITIES
 *************************************************************************************************/

/*
 * rho_crit(z) in units of M_sun h^2 / kpc^3.
 */
double rhoCrit(Cosmology *cosmo, double z)
{
	double result, E;

	E = Ez(cosmo, z);
	result = AST_RHO_CRIT_0_KPC3 * E * E;

	return result;
}

/*
 * rho_m(z) in units of M_sun h^2 / kpc^3.
 */
double rhoM(Cosmology *cosmo, double z)
{
	double result;

	result = cosmo->Omega_m * AST_RHO_CRIT_0_KPC3 * pow(1 + z, 3);

	return result;
}

/*
 * Omega_m(z). This could also be computed from the ratio of the rhoCrit and rhoM functions above,
 * but they include AST_RHO_CRIT_0_KPC3 which is potentially a large / small number depending on
 * the units chosen. This is cleaner numerically.
 */
double OmegaM(Cosmology *cosmo, double z)
{
	double oneOverE, Omega_m_z;

	oneOverE = oneOverEz(cosmo, z);
	Omega_m_z = cosmo->Omega_m * pow(1 + z, 3) * oneOverE * oneOverE;

	return Omega_m_z;
}

void createGrowthFactorTable(Cosmology *cosmo)
{
	int i;
	double z[COSMO_TABLE_Z_N], D[COSMO_TABLE_Z_N], norm, log_min, log_max;

	/*
	 * Initialize the spline object if necessary. It's no prob
	 */
	if (cosmo->spline_z_D == NULL)
	{
		cosmo->spline_z_D = gsl_spline_alloc(gsl_interp_cspline, (size_t) COSMO_TABLE_Z_N);
		cosmo->spline_z_D_acc = gsl_interp_accel_alloc();
	}

	/*
	 * Compute the norm once, then create table
	 */
	norm = 1.0 / growthFactorUnnormalized(cosmo, 0.0);
	log_min = log10(1.0 + COSMO_TABLE_Z_MIN);
	log_max = log10(1.0 + COSMO_TABLE_Z_MAX);
	linearIndicesDouble(z, COSMO_TABLE_Z_N, log_min, log_max);
	for (i = 0; i < COSMO_TABLE_Z_N; i++)
	{
		z[i] = pow(10, z[i]) - 1.0;
		D[i] = growthFactorUnnormalized(cosmo, z[i]) * norm;
	}
	gsl_spline_init(cosmo->spline_z_D, z, D, (size_t) COSMO_TABLE_Z_N);
	cosmo->spline_z_D_initialized = 1;
}

/*
 * The growth factor as defined in Eisenstein & Hu 99, equ. 8. There are other normalizations
 * in the literature. Also see Percival 2005, eq. 15.
 */
double growthFactorUnnormalized(Cosmology *cosmo, double z)
{
	return 5.0 / 2.0 * cosmo->Omega_m * Ez(cosmo, z)
			* EzIntegral(cosmo, z, 1E4, INTEGRAL_TYPE_ONEOVEREZTIMES1PZ);
}

/*
 * The growth factor normalized to z = 0.
 */
double growthFactor(Cosmology *cosmo, double z)
{
	double D;

	if ((cosmo->interpolate) && (z >= COSMO_TABLE_Z_MIN) && (z <= COSMO_TABLE_Z_MAX))
	{
		if (!cosmo->spline_z_D_initialized)
			createGrowthFactorTable(cosmo);
		D = gsl_spline_eval(cosmo->spline_z_D, z, cosmo->spline_z_D_acc);
	} else
	{
		D = growthFactorUnnormalized(cosmo, z) / growthFactorUnnormalized(cosmo, 0.0);
	}

	return D;
}

/************************************************************************************
 * POWER SPECTRUM, CORRELATION FUNCTION, SIGMA, PEAK HEIGHT
 ***********************************************************************************/

/*
 * The transfer function formula from Eisenstein & Hu 1998.
 */
double transferFunctionEH98(Cosmology *cosmo, double kin)
{
	double k;
	double Tk, Tb, Tc, T0t, T0t11, T0t1bc;
	double omb, om0, omc, h;
	double theta2p7, f, ac, bc, s, q;
	double C, C11, C1bc, st, bb, ab, ksilk, bnode, Gy, y, zeq;
	double a1, a2, b1, b2, keq, Rd, zd, Req, b1d, b2d;

	omb = cosmo->Omega_b;
	om0 = cosmo->Omega_m;
	omc = cosmo->Omega_m - cosmo->Omega_b;
	h = cosmo->h;
	theta2p7 = cosmo->T_CMB / 2.7;

	// Convert k from hMpc^-1 to Mpc^-1
	k = kin * h;

	// Equation 2
	zeq = 2.50e4 * om0 * h * h / (theta2p7 * theta2p7 * theta2p7 * theta2p7);

	// Equation 3
	keq = 7.46e-2 * om0 * h * h / (theta2p7 * theta2p7); //Mpc^-{1} (NOT h/Mpc)

	// Equation 4
	b1d = 0.313 * pow(om0 * h * h, -0.419) * (1.0 + 0.607 * pow(om0 * h * h, 0.674));
	b2d = 0.238 * pow(om0 * h * h, 0.223);
	zd = 1291.0 * pow(om0 * h * h, 0.251) / (1.0 + 0.659 * pow(om0 * h * h, 0.828))
			* (1.0 + b1d * pow(omb * h * h, b2d));

	// Equation 5
	Rd = 31.5 * omb * h * h / (theta2p7 * theta2p7 * theta2p7 * theta2p7) / ((zd) / 1e3);
	Req = 31.5 * omb * h * h / (theta2p7 * theta2p7 * theta2p7 * theta2p7) / (zeq / 1e3);

	// Equation 6
	s = 2.0 / 3.0 / keq * sqrt(6.0 / Req)
			* log((sqrt(1.0 + Rd) + sqrt(Rd + Req)) / (1.0 + sqrt(Req)));

	// Equation 7
	ksilk = 1.6 * pow(omb * h * h, 0.52) * pow(om0 * h * h, 0.73)
			* (1.0 + pow(10.4 * om0 * h * h, -0.95));

	// Equation 10
	q = k / 13.41 / keq;

	// Equation 11
	a1 = pow(46.9 * om0 * h * h, 0.670) * (1.0 + pow(32.1 * om0 * h * h, -0.532));
	a2 = pow(12.0 * om0 * h * h, 0.424) * (1.0 + pow(45.0 * om0 * h * h, -0.582));
	ac = pow(a1, -1.0 * omb / om0) * pow(a2, -1.0 * (omb / om0) * (omb / om0) * (omb / om0));

	// Equation 12
	b1 = 0.944 / (1.0 + pow(458.0 * om0 * h * h, -0.708));
	b2 = pow(0.395 * om0 * h * h, -0.0266);
	bc = 1.0 / (1.0 + b1 * (pow(omc / om0, b2) - 1.0));

	// Equation 15
	y = (1.0 + zeq) / (1.0 + zd);
	Gy = y
			* (-6.0 * sqrt(1.0 + y)
					+ (2.0 + 3.0 * y) * log((sqrt(1.0 + y) + 1.0) / (sqrt(1.0 + y) - 1.0)));

	// Equation 14
	ab = 2.07 * keq * s * pow(1.0 + Rd, -3.0 / 4.0) * Gy;

	/*
	 * Get CDM part of transfer function
	 */
	// Equation 18
	f = 1.0 / (1.0 + (k * s / 5.4) * (k * s / 5.4) * (k * s / 5.4) * (k * s / 5.4));

	// Equation 20
	C = 14.2 / ac + 386.0 / (1.0 + 69.9 * pow(q, 1.08));

	// Equation 19
	T0t = log(M_E + 1.8 * bc * q) / (log(M_E + 1.8 * bc * q) + C * q * q);

	// Equation 17
	C1bc = 14.2 + 386.0 / (1.0 + 69.9 * pow(q, 1.08));
	T0t1bc = log(M_E + 1.8 * bc * q) / (log(M_E + 1.8 * bc * q) + C1bc * q * q);
	Tc = f * T0t1bc + (1.0 - f) * T0t;

	/*
	 * Get baryon part of transfer function
	 */
	// Equation 24
	bb = 0.5 + omb / om0
			+ (3.0 - 2.0 * omb / om0) * sqrt((17.2 * om0 * h * h) * (17.2 * om0 * h * h) + 1.0);

	// Equation 23
	bnode = 8.41 * pow(om0 * h * h, 0.435);

	// Equation 22
	st = s / pow(1.0 + (bnode / k / s) * (bnode / k / s) * (bnode / k / s), 1.0 / 3.0);

	// Equation 21
	C11 = 14.2 + 386.0 / (1.0 + 69.9 * pow(q, 1.08));
	T0t11 = log(M_E + 1.8 * q) / (log(M_E + 1.8 * q) + C11 * q * q);
	Tb = (T0t11 / (1.0 + (k * s / 5.2) * (k * s / 5.2))
			+ ab / (1.0 + (bb / k / s) * (bb / k / s) * (bb / k / s)) / exp(pow(k / ksilk, 1.4)))
			* sin(k * st) / (k * st);

	/*
	 * Total transfer function
	 */
	Tk = omb / om0 * Tb + omc / om0 * Tc;

	return Tk;
}

/*
 * The transfer function formula from Eisenstein & Hu 1998, without BAO wiggles.
 */
double transferFunctionEH98Smooth(Cosmology *cosmo, double kin)
{
	double k, Tk;
	double omb, om0, h;
	double theta2p7, s, q;
	double Gamma, alphaGamma, L0, C0;

	omb = cosmo->Omega_b;
	om0 = cosmo->Omega_m;
	h = cosmo->h;
	theta2p7 = cosmo->T_CMB / 2.7;

	// Convert k from hMpc^-1 to Mpc^-1
	k = kin * h;

	// Equation 26
	s = 44.5 * log(9.83 / om0 / h / h) / sqrt(1.0 + 10.0 * pow(omb * h * h, 0.75));

	// Equation 31
	alphaGamma = 1.0 - 0.328 * log(431.0 * om0 * h * h) * omb / om0
			+ 0.38 * log(22.3 * om0 * h * h) * (omb / om0) * (omb / om0);

	// Equation 30
	Gamma = om0 * h * (alphaGamma + (1.0 - alphaGamma) / (1.0 + pow(0.43 * k * s, 4.0)));

	// Equation 28
	q = kin * theta2p7 * theta2p7 / Gamma;

	// Equation 29
	C0 = 14.2 + 731.0 / (1.0 + 62.5 * q);
	L0 = log(2.0 * exp(1.0) + 1.8 * q);
	Tk = L0 / (L0 + C0 * q * q);

	return Tk;
}

/*
 * The unnormalized power spectrum P(k). This function computes P(k) depending on the chosen power
 * spectrum type.
 */
double PkUnnormalized(Cosmology *cosmo, double k)
{
	double T, Pk = 0.0;

	if (cosmo->ps_type == POWERSPECTRUM_TYPE_EH98)
	{
		T = transferFunctionEH98(cosmo, k);
		Pk = T * T * pow(k, cosmo->ns);
	} else if (cosmo->ps_type == POWERSPECTRUM_TYPE_EH98SMOOTH)
	{
		T = transferFunctionEH98Smooth(cosmo, k);
		Pk = T * T * pow(k, cosmo->ns);
	} else if (cosmo->ps_type == POWERSPECTRUM_TYPE_POWERLAW)
	{
		Pk = pow(k, cosmo->ns);
	}

	return Pk;
}

/*
 * Use sigma8 to normalize the power spectrum. Note that the norm refers to P(k), while sigma is
 * proportional to root(norm). Furthermore, sigma8 is always defined with respect to the tophat
 * sigma, which is why we ignore the value set in the cosmology object.
 */
void setPkNorm(Cosmology *cosmo)
{
	int tmp_filter;

	tmp_filter = cosmo->ps_filter;
	cosmo->ps_filter = FILTER_TOPHAT;
	cosmo->ps_norm = pow(cosmo->sigma8 / sigmaUnnormalized(cosmo, 8.0, 0), 2);
	cosmo->ps_filter = tmp_filter;
}

/*
 * Normalized power spectrum P(k).
 */
double Pk(Cosmology *cosmo, double k)
{
	if (cosmo->ps_norm < 0.0)
		setPkNorm(cosmo);

	return PkUnnormalized(cosmo, k) * cosmo->ps_norm;
}

/*
 * The integrand for the correlationFunction(). Note that this integrand does not include the
 * sin(kr) term, since it is included via the GSL function used for the integration.
 */
double correlationFunctionIntegrand(double k, void *p)
{
	PowerSpectrumParams *pars = (PowerSpectrumParams*) p;

	return Pk(pars->cosmo, k) * k / pars->R;
}

/*
 * The linear matter-matter correlation function, where R is in of Mpc/h.
 */
double correlationFunction(Cosmology *cosmo, double z, double r)
{
	const int WORKSPACE_NUM = 200;

	double xi, abserr;
	gsl_integration_workspace *workspace;
	gsl_function F;
	gsl_integration_qawo_table *wf;
	gsl_integration_workspace *cycle_workspace;
	PowerSpectrumParams params;

	workspace = gsl_integration_workspace_alloc((size_t) WORKSPACE_NUM);
	cycle_workspace = gsl_integration_workspace_alloc((size_t) WORKSPACE_NUM);
	wf = gsl_integration_qawo_table_alloc(r, 1.0, GSL_INTEG_SINE, (size_t) WORKSPACE_NUM);

	F.function = &correlationFunctionIntegrand;
	F.params = &params;
	params.R = r;
	params.cosmo = cosmo;

	gsl_integration_qawf(&F, 1E-8, 1E-3, (size_t) WORKSPACE_NUM, workspace, cycle_workspace, wf,
			&xi, &abserr);
	gsl_integration_qawo_table_free(wf);
	gsl_integration_workspace_free(cycle_workspace);
	gsl_integration_workspace_free(workspace);

	xi /= 2.0 * MATH_pi * MATH_pi;

	/*
	 * Only evaluate growth factor if z > 0.
	 */
	if (z > 1E-5)
	{
		double D = growthFactor(cosmo, z);
		xi *= D * D;
	}

	return xi;
}

/*
 * The filter function used when smoothing the power spectrum. The small-k limit of the Window
 * function is 1.
 */
static double filter(int filter, double k, double R)
{
	double ret, x;

	x = k * R;
	ret = 0.0;

	switch (filter)
	{
	case FILTER_TOPHAT:
		if (x < 1e-3)
			ret = 1.0;
		else
			ret = 3.0 / (x * x * x) * (sin(x) - x * cos(x));
		break;
	case FILTER_GAUSSIAN:
		ret = exp(-x * x);
		break;
	default:
		error(__FFL__, "Unknown filter, %d.\n", filter);
	}

	return ret;
}

double sigmaIntegrandLin(double k, void *p)
{
	PowerSpectrumParams *params = (PowerSpectrumParams*) p;
	double W, Pk, ret;

	ret = 0.0;
	W = filter(params->cosmo->ps_filter, k, params->R);
	Pk = PkUnnormalized(params->cosmo, k);
	ret = W * W * Pk * k * k / (2.0 * MATH_pi * MATH_pi);
	if (params->j != 0)
		ret *= pow(k, 2.0 * params->j);

	return ret;
}

/*
 * In ln space, dk = k d ln(k)
 */
double sigmaIntegrandLog(double lnk, void *p)
{
	double k;

	k = exp(lnk);

	return sigmaIntegrandLin(k, p) * k;
}

/*
 * Sigma, i.e. the root variance inside a sphere of radius R, at no particular redshift. The
 * redshift scaling and normalization are introduced below.
 *
 * The parameter j can be used to compute higher powers of sigma. The default j = 0 gives the rms
 * fluctuation sigma_0, j = 1 means that the integrand is multiplied by k^2, j = 2 --> k^4 etc.
 */
double sigmaUnnormalized(Cosmology *cosmo, double R, int j)
{
	/*
	 * Constants for testing integration limits. Note that these are in
	 * ln(k), not log10(k), so that the limits are roughly 1E-15 / 1E15.
	 */
	const int WORKSPACE_NUM = 10000000;
	const double epsabs = 1E-15;
	const int N_TEST_K = 60;
	const double TEST_K_MIN = -15.0;
	const double TEST_K_MAX = 35.0;
	const double TEST_INTEGRAND_MIN = 1E-10;

	double sigma, sigma2;

	sigma2 = 0.0;
	if ((cosmo->ps_filter == FILTER_TOPHAT) && (j > 1))
	{
		error(__FFL__, "Higher order moments are not implemented for top hat.\n");
	} else if (cosmo->ps_type == POWERSPECTRUM_TYPE_POWERLAW)
	{
		/*
		 * For power-law power spectra, there are analytical expressions that we should use for
		 * speed, but also because for some slopes the numerical function has trouble.
		 */
		double n;

		n = cosmo->ns + 2 * j;

		if (cosmo->ps_filter == FILTER_TOPHAT)
		{
			sigma2 = 9.0 * pow(R, -n - 3.0) * pow(2.0, -n - 1.0) / (pow(MATH_pi, 2) * (n - 3.0));
			if (fabs(n + 2.0) < 1E-3)
				sigma2 *= -(1.0 + n) * MATH_pi
						/ (2.0 * gsl_sf_gamma(2.0 - n) * cos(MATH_pi * n * 0.5));
			else
				sigma2 *= sin(n * MATH_pi * 0.5) * gsl_sf_gamma(n + 2.0) / ((n - 1.0) * n);
		} else if (cosmo->ps_filter == FILTER_GAUSSIAN)
		{
			sigma2 = pow(R, -n - 3.0) * gsl_sf_gamma((n + 3.0) * 0.5) / (4.0 * pow(MATH_pi, 2));
		} else
		{
			error(__FFL__, "Unknown filter, %d.\n", filter);
		}
	} else
	{
		/*
		 * A tabulated power spectrum has a finite reach, so we cannot integrate to infinity. The
		 * infinite integral almost always has trouble converging in some regimes. Thus, we
		 * determine sensible limits.
		 */
		double min_k, max_k, abserr, test_k[N_TEST_K], test_k_integrand[N_TEST_K], integrand_max,
				epsrel;
		int i, min_index, max_index;
		gsl_integration_workspace *workspace;
		gsl_function F;
		PowerSpectrumParams params;

		workspace = gsl_integration_workspace_alloc((size_t) WORKSPACE_NUM);
		F.params = &params;
		params.R = R;
		params.cosmo = cosmo;
		params.j = j;

		/*
		 * With j > 1, the integral gets very difficult to evaluate.
		 */
		if (j > 0)
			epsrel = 1E-4;
		else
			epsrel = 1E-6;

		for (i = 0; i < N_TEST_K; i++)
			test_k[i] = TEST_K_MIN + i * (TEST_K_MAX - TEST_K_MIN) / (N_TEST_K - 1);

		integrand_max = 0.0;
		for (i = 0; i < N_TEST_K; i++)
		{
			test_k_integrand[i] = sigmaIntegrandLog(test_k[i], &params);
			integrand_max = fmax(integrand_max, test_k_integrand[i]);
		}

		min_index = 0;
		while (test_k_integrand[min_index] < integrand_max * TEST_INTEGRAND_MIN)
		{
			min_index++;
			if (min_index > N_TEST_K - 2)
				error(__FFL__, "Could not find min. integration index.\n");
		}

		min_index--;
		max_index = min_index + 1;

		while (test_k_integrand[max_index] > integrand_max * TEST_INTEGRAND_MIN)
		{
			max_index++;
			if (max_index == N_TEST_K)
				error(__FFL__, "Could not find max. integration index.\n");
		}

		min_k = test_k[min_index];
		max_k = test_k[max_index];

		F.function = &sigmaIntegrandLog;
		gsl_integration_qags(&F, min_k, max_k, epsabs, epsrel, (size_t) WORKSPACE_NUM, workspace,
				&sigma2, &abserr);

		gsl_integration_workspace_free(workspace);
	}

	sigma = sqrt(sigma2);

	return sigma;
}

/*
 * Create an interpolation table in log(R) space.
 */
void createRSigmaTable(Cosmology *cosmo)
{
	int i;
	double logR[COSMO_TABLE_R_SIGMA_N], logsigma[COSMO_TABLE_R_SIGMA_N],
			logR_rev[COSMO_TABLE_R_SIGMA_N], logsigma_rev[COSMO_TABLE_R_SIGMA_N], root_norm;

	/*
	 * Initialize the spline object if necessary. It's no prob
	 */
	if (cosmo->spline_r_sigma == NULL)
	{
		cosmo->spline_r_sigma = gsl_spline_alloc(gsl_interp_cspline,
				(size_t) COSMO_TABLE_R_SIGMA_N);
		cosmo->spline_r_sigma_acc = gsl_interp_accel_alloc();
		cosmo->spline_sigma_r = gsl_spline_alloc(gsl_interp_cspline,
				(size_t) COSMO_TABLE_R_SIGMA_N);
		cosmo->spline_sigma_r_acc = gsl_interp_accel_alloc();
	}

	/*
	 * Normalize the power spectrum if necessary.
	 */
	if (cosmo->ps_norm < 0.0)
		setPkNorm(cosmo);
	root_norm = sqrt(cosmo->ps_norm);

	/*
	 * Compute the table
	 */
	logIndicesDouble(logR, COSMO_TABLE_R_SIGMA_N, COSMO_TABLE_R_SIGMA_MIN, COSMO_TABLE_R_SIGMA_MAX);
	for (i = 0; i < COSMO_TABLE_R_SIGMA_N; i++)
	{
		logsigma[i] = log10(sigmaUnnormalized(cosmo, logR[i], 0) * root_norm);
		logR[i] = log10(logR[i]);
		logsigma_rev[COSMO_TABLE_R_SIGMA_N - i - 1] = logsigma[i];
		logR_rev[COSMO_TABLE_R_SIGMA_N - i - 1] = logR[i];
	}
	gsl_spline_init(cosmo->spline_r_sigma, logR, logsigma, (size_t) COSMO_TABLE_R_SIGMA_N);
	gsl_spline_init(cosmo->spline_sigma_r, logsigma_rev, logR_rev, (size_t) COSMO_TABLE_R_SIGMA_N);
	cosmo->spline_r_sigma_initialized = 1;
}

/*
 * The variance of a filtered volume with radius R, at redshift z (a sphere in the case of the
 * tophat filter). R is in units of Mpc / h.
 *
 * This function needs to normalize sigma independently of the power spectrum because the
 * unnormalized sigma function is USED to normalize the power spectrum in the first place, and
 * thus can't rely on the normalization. However, the ps_norm factor is the normalization in the
 * power spectrum, but since sigma^2 propto P(k) we need to multiply by root(norm).
 *
 * Higher order sigmas are normalized by sigma0(8 Mpc/h).
 *
 * For power-law cosmologies, we could use the unnormalized sigma function because it has an analytical
 * expression that should be used instead of the interpolation table. However, that expression may
 * still be slower than the interpolation table.
 */
double sigma(Cosmology *cosmo, double z, double R, int j)
{
	double sigma, sigma_z0;

	if ((cosmo->interpolate) && (j == 0) && (R >= COSMO_TABLE_R_SIGMA_MIN)
			&& (R <= COSMO_TABLE_R_SIGMA_MAX))
	{
		if (!cosmo->spline_r_sigma_initialized)
			createRSigmaTable(cosmo);
		sigma_z0 = pow(10,
				gsl_spline_eval(cosmo->spline_r_sigma, log10(R), cosmo->spline_r_sigma_acc));
	} else
	{
		sigma_z0 = sigmaUnnormalized(cosmo, R, j);
		if (cosmo->ps_norm < 0.0)
			setPkNorm(cosmo);
		sigma_z0 *= sqrt(cosmo->ps_norm);
	}

	sigma = sigma_z0 * growthFactor(cosmo, z);

	return sigma;
}

/*
 * Compute sigma(M) = sigma(R(M)). j indicates the moment, j = 0 is the variance.
 */
double sigmaFromMass(Cosmology *cosmo, double z, double M, int j)
{
	double rho, R;

	rho = rhoM(cosmo, 0.0) * 1E9;
	R = pow(3.0 * M / (4.0 * MATH_pi * rho), 1.0 / 3.0);

	return sigma(cosmo, z, R, j);
}

/*
 * Returns the peak height of a certain mass M at a certain redshift z. Actually, delta_c = 1.686
 * only for an Einstein-de Sitter universe. The dependence on Omega_m and Omega_L is small though.
 * See e.g. Percival 2005.
 */
double peakHeight(Cosmology *cosmo, double z, double M)
{
	return AST_DELTA_COLLAPSE / sigmaFromMass(cosmo, z, M, 0);
}

/*
 * Inverts the sigma(R) function using the interpolation splines. This function can only be used if
 * cosmo.interpolate is on, and if the requested sigma is within the limits of the interpolation
 * table.
 */
double radiusFromSigma(Cosmology *cosmo, double z, double sigma)
{
	double R, sigma_z0;

	if (!cosmo->interpolate)
		error(__FFL__, "Interpolation must be on in order to invert sigma(R) function.\n");

	if (!cosmo->spline_r_sigma_initialized)
		createRSigmaTable(cosmo);
	sigma_z0 = sigma / growthFactor(cosmo, z);
	R = pow(10, gsl_spline_eval(cosmo->spline_sigma_r, log10(sigma_z0), cosmo->spline_sigma_r_acc));

	return R;
}

/*
 * Inverts the expression for sigma(M).
 */
double massFromSigma(Cosmology *cosmo, double z, double sigma)
{
	double rho, R, M;

	R = radiusFromSigma(cosmo, z, sigma);
	rho = rhoM(cosmo, 0.0) * 1E9;
	M = 4.0 / 3.0 * MATH_pi * rho * R * R * R;

	return M;
}

/*
 * Inversion of the peak height function.
 */
double massFromPeakHeight(Cosmology *cosmo, double z, double nu)
{
	return massFromSigma(cosmo, z, AST_DELTA_COLLAPSE / nu);
}

/*
 * The non-linear mass is defined as the mass with nu = 1, sigma = delta_c at a given redshift.
 */
double nonLinearMass(Cosmology *cosmo, double z)
{
	return massFromSigma(cosmo, z, AST_DELTA_COLLAPSE);
}
