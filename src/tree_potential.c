/*************************************************************************************************
 *
 * Tree implementation to compute gravitational potentials.
 *
 * This unit is heavily based on Peter Behroozi's fast3treepotpot, a fast BSP tree implementation.
 * The unit returns the potential in units of mass/distance, corresponding to the given mass and
 * the distances passed in the potential points.
 *
 * (c) Peter Behroozi, Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <assert.h>

#include "tree_potential.h"
#include "constants.h"
#include "memory.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * When we run out of nodes, increase by this factor.
 */
#define TREE_POTENTIAL_ALLOC_FACTOR 2

/*
 * Throw errors if the tree build does not proceed as expected, e.g., for degenerate cells etc.
 */
#define TREE_POTENTIAL_STRICT 1

#ifndef __APPLE__
#ifndef isfinite
#define isfinite(x) finitef(x)
#endif
#endif

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void build(TreePotential *t);

TreePotential* treePotentialInit(int n, TreePotentialPoint *p, int points_per_leaf);
void treePotentialFree(TreePotential **t);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Given the size of a point distribution in all dimensions, find the dimension that has the
 * largest extent.
 */
static inline int findLargestDim(float *min, float *max)
{
	int i, dim;
	float d, d2;

	dim = 3 - 1;
	d = max[3 - 1] - min[3 - 1];
	for (i = 0; i < (3 - 1); i++)
	{
		d2 = max[i] - min[i];
		if (d2 > d)
		{
			d = d2;
			dim = i;
		}
	}

	return dim;
}

/*
 * Split particles in node into left and right along the spatial center of the node in the largest
 * (most extended) dimension. Return the number of particles on the left.
 */
static inline int sortDimPos(TreePotentialNode *node)
{
	int dim, i, j;
	float lim;
	TreePotentialPoint *p, temp;

	/*
	 * Find the largest dimension and its centerpoint, on which we'll split the particle
	 * distribution.
	 */
	dim = findLargestDim(node->min, node->max);
	node->div_dim = dim;
	p = node->points;
	j = node->num_points - 1;
	lim = 0.5 * (node->max[dim] + node->min[dim]);

	/*
	 * If the node has no spatial extent, there is no way to do this; abort. Depending on the strict
	 * setting, throw an error or just leave the node with many particles.
	 */
	if (node->max[dim] == node->min[dim])
#if TREE_POTENTIAL_STRICT
		error(__FFL__, "Found node with no spatial extent.\n");
#else
	return node->num_points;
#endif

	/*
	 * Go through the particles. If they lie on the right side (larger than limit), we exchange
	 * with the current particle until we find one that is on the left and move on.
	 */
	for (i = 0; i < j; i++)
	{
		if (p[i].x[dim] > lim)
		{
			temp = p[j];
			p[j] = p[i];
			p[i] = temp;
			i--;
			j--;
		}
	}
	if ((i == j) && (p[i].x[dim] <= lim))
		i++;

	return i;
}

/*
 * Find the extent of the points in a node in all dimensions
 */
static inline void findMinMax(TreePotentialNode *node)
{
	int i, j;
	float x;
	TreePotentialPoint *p = node->points;

	assert(node->num_points > 0);
	for (j = 0; j < 3; j++)
		node->min[j] = node->max[j] = p[0].x[j];
	for (i = 1; i < node->num_points; i++)
	{
		for (j = 0; j < 3; j++)
		{
			x = p[i].x[j];
			if (x < node->min[j])
				node->min[j] = x;
			else if (x > node->max[j])
				node->max[j] = x;
		}
	}
}

/*
 * During tree construction, split a node if it has more than the allowed number of leaf points.
 */
void splitNode(TreePotential *t, TreePotentialNode *node)
{
	int num_left, left_index, node_index;
	TreePotentialNode *left, *right, *null_ptr = NULL;

	/*
	 * Sort particles into left and right
	 */
	num_left = sortDimPos(node);

	/*
	 * There is an edge case where all particles are either left or right, meaning they are all in
	 * the same position. We could just return and not split the node further so that its potential
	 * will be computed by direct summation. However, this should not happen for reasonable particle
	 * distributions and we throw an error instead.
	 */
	if (num_left == node->num_points || num_left == 0)
	{
#if TREE_POTENTIAL_STRICT
		error(__FFL__, "Could not split node, all particles in the same place.\n");
#else
		node->div_dim = -1;
		return;
#endif
	}

	/*
	 * If we need more memory to split the node, reallocate the nodes.
	 */
	node_index = node - t->root;
	if ((t->num_nodes + 2) > t->allocated_nodes)
	{
		int n_alloc_old;

		n_alloc_old = t->allocated_nodes;
		t->allocated_nodes *= TREE_POTENTIAL_ALLOC_FACTOR;
		t->root = (TreePotentialNode*) memRealloc(__FFL__, MID_TREEPOT, t->root,
				sizeof(TreePotentialNode) * (t->allocated_nodes),
				sizeof(TreePotentialNode) * n_alloc_old);
		node = t->root + node_index;
	}

	/*
	 * Add two nodes for the left and right and initialize them with the correct pointers to the
	 * parent (this node), number of points, pointer into points array etc.
	 *
	 * Note that the left, right, parent pointer are NOT actually pointers into the node array but
	 * offsets. Their actual values are re-inserted later.
	 */
	left_index = t->num_nodes;
	t->num_nodes += 2;
	node->left = null_ptr + left_index;
	node->right = null_ptr + (left_index + 1);
	left = t->root + left_index;
	right = t->root + (left_index + 1);

	memset(left, 0, sizeof(TreePotentialNode) * 2);

	right->parent = null_ptr + node_index;
	left->parent = null_ptr + node_index;
	left->num_points = num_left;
	right->num_points = node->num_points - num_left;
	left->div_dim = -1;
	right->div_dim = -1;
	left->points = node->points;
	right->points = node->points + num_left;

	findMinMax(left);
	findMinMax(right);

	/*
	 * Recursively keep splitting nodes. We need to renew the pointer to right after we have split
	 * left because the pointer to t->root can have changed if new memory was allocated.
	 */
	if (left->num_points > t->points_per_leaf)
		splitNode(t, left);
	right = t->root + (left_index + 1);
	if (right->num_points > t->points_per_leaf)
		splitNode(t, right);
}

void rebuildPointers(TreePotential *t)
{
	int i;
	TreePotentialNode *nullptr = NULL;

	for (i = 0; i < t->num_nodes; i++)
	{
#define REBUILD(x) x = t->root + (x - nullptr)
		REBUILD(t->root[i].left);
		REBUILD(t->root[i].right);REBUILD(t->root[i].parent);
#undef REBUILD
	}
}

/*
 * Split a particle distribution into nodes in a recursive manner
 */
void build(TreePotential *t)
{
	int i, j, n_alloc_old;
	TreePotentialNode *root;

	/*
	 * Allocate node memory, set root node as pointer
	 */
	n_alloc_old = t->allocated_nodes;
	t->allocated_nodes = (3 + t->num_points / (t->points_per_leaf / 2));
	t->root = (TreePotentialNode*) memRealloc(__FFL__, MID_TREEPOT, t->root,
			sizeof(TreePotentialNode) * (t->allocated_nodes),
			sizeof(TreePotentialNode) * n_alloc_old);
	t->num_nodes = 1;

#if CAREFUL
	/*
	 * Check for nan/inf
	 */
	for (i = 0; i < t->num_points; i++)
	{
		for (j = 0; j < 3; j++)
			if (!isfinite(t->points[i].x[j]))
				error(__FFL__, "Found non-finite value in tree points.\n");
	}
#endif

	root = t->root;
	memset(root, 0, sizeof(TreePotentialNode));
	root->num_points = t->num_points;
	root->points = t->points;
	root->div_dim = -1;
	if (t->num_points)
		findMinMax(root);

#if CAREFUL
	for (j = 0; j < 3; j++)
		assert(isfinite(root->min[j]));
	for (j = 0; j < 3; j++)
		assert(isfinite(root->max[j]));
#endif

	if (root->num_points > t->points_per_leaf)
		splitNode(t, root);

	/*
	 * Reallocate memory in case we needed less than we had allocated. This is not really
	 * necessary if the potential is used for one computation and then released: after all, we
	 * have already gotten the memory. On the other hand, reducing via memRealloc should just
	 * leave the current memory in place, so it also doesn't do any harm.
	 */
	t->root = (TreePotentialNode*) memRealloc(__FFL__, MID_TREEPOT, t->root,
			sizeof(TreePotentialNode) * t->num_nodes,
			sizeof(TreePotentialNode) * t->allocated_nodes);
	t->allocated_nodes = t->num_nodes;
	rebuildPointers(t);
}

float distanceSquared(float *p1, float *p2)
{
	int k;
	float dx, r2;

	r2 = 0.0;
	for (k = 0; k < 3; k++)
	{
		dx = p1[k] - p2[k];
		r2 += dx * dx;
	}
	return (r2);
}

float invDistance(float *p1, float *p2, float force_res)
{
	float r;

	r = sqrt(distanceSquared(p1, p2));
	if (r < force_res)
		r = force_res;

	return (1.0 / r);
}

/*
 * Compute the extent of a node in terms of how far removed the points are from the center of
 * mass. This measure is at least the distance between the farthest point from the center of mass,
 * but can be larger depending on the rms distance and the error tolerance.
 *
 * dmin is used when determining whether a node is too close to apply the monopole potential.
 */
void computeDMin(TreePotentialNode *n, float err_tol)
{
	int i;
	float sum_x2, bmax, dx;

	sum_x2 = 0.0;
	bmax = 0.0;
	for (i = 0; i < n->num_points; i++)
	{
		dx = distanceSquared(n->mass_center, n->points[i].x);
		if (bmax < dx)
			bmax = dx;
		sum_x2 += dx;
	}
	bmax = sqrt(bmax) / 2.0;
	n->dmin = bmax + sqrt(bmax * bmax + sum_x2 / (n->num_points * err_tol));
}

/*
 * Compute the center of mass of this node. If it's a leaf node, this is the mass-weighted average
 * position of the particles. If it is not, it is the mass-weighted average of the child nodes.
 */
void computeMassCenters(TreePotentialNode *n, float ptl_mass, float err_tol)
{
	int i, j;
	float x[3] =
		{0};

	if (n->div_dim < 0)
	{
		// Leaf node
		n->m = ptl_mass * n->num_points;
		for (i = 0; i < n->num_points; i++)
			for (j = 0; j < 3; j++)
				x[j] += n->points[i].x[j];
		for (j = 0; j < 3; j++)
			n->mass_center[j] = x[j] ? x[j] / (float) n->num_points : 0;
	} else
	{
		// Non-leaf node
		computeMassCenters(n->left, ptl_mass, err_tol);
		computeMassCenters(n->right, ptl_mass, err_tol);
		n->m = n->left->m + n->right->m;
		for (j = 0; j < 3; j++)
			n->mass_center[j] =
					n->m ? ((n->right->mass_center[j] * n->right->m
							+ n->left->mass_center[j] * n->left->m) / n->m) :
							0;
	}
	computeDMin(n, err_tol);
}

void addDirectPotential(TreePotentialPoint *p, int n, float ptl_mass, float force_res)
{
	int i, j;
	float dpo;

	for (i = 0; i < n; i++)
	{
		for (j = i + 1; j < n; j++)
		{
			dpo = ptl_mass * invDistance(p[i].x, p[j].x, force_res);
			p[i].pe += dpo;
			p[j].pe += dpo;
		}
	}
}

/*
 * Check if the monopole approximation is good enough for two nodes. This is the case if their
 * distance^2 is greater than the product of their dmin.
 */
int monopoleAcceptable(TreePotentialNode *n, TreePotentialNode *n2)
{
	float r2;

	r2 = distanceSquared(n->mass_center, n2->mass_center);
	if (r2 > n2->dmin * n2->dmin)
		return 1;

	return 0;
}

/*
 * If the nodes are the same, we compute the self-potential for the points inside the node from
 * direct summation. If the nodes are different, we check if we can apply the monopole potential
 * from node2 to node1. If not, we fall back on direct summation or open the second node further
 * if it is not a leaf node.
 *
 * We also check for degenerate nodes. This can happen if many particles are in the exactly same
 * location so that the node could not be split. This should not happen in SPARTA, so we throw
 * an error if strict is on.
 */
void computeMonopolePotentials(TreePotentialNode *n, TreePotentialNode *n2, float ptl_mass,
		float force_res, int points_per_leaf)
{
	int i, j;

	assert(n->div_dim < 0);

	/*
	 * If the nodes are the same, we compute the potential for the points inside the node from
	 * direct summation.
	 *
	 * We also check for degenerate nodes. This can happen if many particles are in the exactly same
	 * location so that the node could not be split. This should not happen in SPARTA, so we throw
	 * an error if strict is on.
	 */
	if (n == n2)
	{
		if (n->num_points > (2 * points_per_leaf))
#if TREE_POTENTIAL_STRICT
			error(__FFL__, "Found degenerate node in potential tree.\n");
#else
		return;
#endif

		addDirectPotential(n->points, n->num_points, ptl_mass, force_res);
		return;
	}

	/*
	 * The nodes are different. Test whether n2 totally encompasses n (i == 3). If that is the case,
	 * or if the monopole is deemed too inaccurate, we need to either compute the potential by
	 * direct summation between the particles of n and n2 (if n2 is a leaf node) or we need to walk
	 * the tree further (if n2 is not a leaf node).
	 *
	 * If n2 does not encompass n and the monopole is acceptable, we compute the monopole potential
	 * on each particle in n.
	 */
	for (i = 0; i < 3; i++)
		if (n->min[i] < n2->min[i] || n->max[i] > n2->max[i])
			break;

	if (i == 3 || !monopoleAcceptable(n, n2))
	{
		if (n2->div_dim < 0)
		{
			/*
			 * If both n and n2 are leaf nodes, n2 cannot totally enclose n
			 */
			if (i == 3)
			{
				output(0,
						"Extents x [%.3f ..%.3f] [%.3f .. %.3f] y [%.3f ..%.3f] [%.3f .. %.3f] z [%.3f ..%.3f] [%.3f .. %.3f]\n",
						n->min[0], n->max[0], n2->min[0], n2->max[0], n->min[1], n->max[1],
						n2->min[1], n2->max[1], n->min[2], n->max[2], n2->min[2], n2->max[2]);
				error(__FFL__, "Leaf node encloses other leaf node, error in tree construction.\n");
			}

			/*
			 * Compute potential between each point in each node
			 */
			for (i = 0; i < n->num_points; i++)
				for (j = 0; j < n2->num_points; j++)
					n->points[i].pe += ptl_mass
							* invDistance(n->points[i].x, n2->points[j].x, force_res);
		} else
		{
			computeMonopolePotentials(n, n2->left, ptl_mass, force_res, points_per_leaf);
			computeMonopolePotentials(n, n2->right, ptl_mass, force_res, points_per_leaf);
		}
	} else
	{
		for (i = 0; i < n->num_points; i++)
			n->points[i].pe += n2->m * invDistance(n->points[i].x, n2->mass_center, force_res);
	}
}

TreePotential* treePotentialInit(int n, TreePotentialPoint *p, int points_per_leaf)
{
	TreePotential *tp = NULL;

	tp = (TreePotential*) memAlloc(__FFL__, MID_TREEPOT, sizeof(TreePotential));
	memset(tp, 0, sizeof(TreePotential));

	tp->points = p;
	tp->num_points = n;
	tp->points_per_leaf = points_per_leaf;

	build(tp);

	return tp;
}

void treePotentialFree(TreePotential **t)
{
	if (!t)
		return;
	TreePotential *u = *t;
	if (u)
	{
		memFree(__FFL__, MID_TREEPOT, u->root, sizeof(TreePotentialNode) * u->allocated_nodes);
		memFree(__FFL__, MID_TREEPOT, u, sizeof(TreePotential));
	}
	*t = NULL;
}

int comparePotentialsIdx(const void *a, const void *b)
{
	const TreePotentialPoint *da = (const TreePotentialPoint*) a;
	const TreePotentialPoint *db = (const TreePotentialPoint*) b;

	return (da->idx > db->idx) - (da->idx < db->idx);
}

void printNode(TreePotentialNode *n, int level)
{
	int i;
	char str[100];

	sprintf(str, "%s", "  Tree: ");
	for (i = 0; i < level; i++)
		sprintf(str, "%s--", str);
	output(0, "%sNode with %5d ptl, div_dim %2d\n", str, n->num_points, n->div_dim);
	if (n->div_dim > -1)
	{
		printNode(n->left, level + 1);
		printNode(n->right, level + 1);
	}
}

void printTree(TreePotential *pt)
{
	printNode(pt->root, 0);
}

/*
 * Set up a potential tree. Then compute the center of mass and mass of each node. Then add
 * potentials to each node by computing the potential between each leaf node and the root (all
 * other nodes).
 *
 * During this process, the points will get shuffled, but we remember their index in the beginning
 * and put them in the correct order. Thus, the user only needs to set the coordinates of the
 * points, everything else will be taken care of.
 */
void computeTreePotential(TreePotentialPoint *p, int n, float ptl_mass, float force_res,
		float err_tol, int points_per_leaf)
{
	int i;
	TreePotential *pt = NULL;

	/*
	 * If there are few particles compared to the number of leafs, direct computation tends to be
	 * faster than the tree due to its overhead. We could be more aggressive than the factor of 2
	 * here.
	 */
	if (n < 2 * points_per_leaf)
	{
		computeDirectPotential(p, n, ptl_mass, force_res);
		return;
	}

	for (i = 0; i < n; i++)
	{
		p[i].idx = i;
		p[i].pe = 0.0;
	}

	pt = treePotentialInit(n, p, points_per_leaf);
	computeMassCenters(pt->root, ptl_mass, err_tol);

	for (i = 0; i < pt->num_nodes; i++)
	{
		if (pt->root[i].div_dim < 0)
			computeMonopolePotentials(pt->root + i, pt->root, ptl_mass, force_res,
					pt->points_per_leaf);
	}

	treePotentialFree(&pt);
	qsort(p, n, sizeof(TreePotentialPoint), &comparePotentialsIdx);
}

void computeDirectPotential(TreePotentialPoint *p, int n, float ptl_mass, float force_res)
{
	int i;

	for (i = 0; i < n; i++)
	{
		p[i].idx = i;
		p[i].pe = 0.0;
	}

	addDirectPotential(p, n, ptl_mass, force_res);
}
