/*************************************************************************************************
 *
 * This unit implements various utility functions.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <assert.h>

#include "utils.h"
#include "global.h"
#include "global_types.h"
#include "config.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Default permissions for directories created by SPARTA and MORIA, namely rwx for the user and
 * r-x for all others.
 */
const mode_t DEFAULT_DIR_PERMISSIONS = 02755;

/*************************************************************************************************
 * FUNCTIONS - COMPARISONS
 *************************************************************************************************/

int compareIntegersAscending(const void *a, const void *b)
{
	const int *da = (const int*) a;
	const int *db = (const int*) b;
	return (*da > *db) - (*da < *db);
}

int compareIntegersDescending(const void *a, const void *b)
{
	const int *da = (const int*) a;
	const int *db = (const int*) b;
	return (*da < *db) - (*da > *db);
}

int compareLongsAscending(const void *a, const void *b)
{
	const long *da = (const long*) a;
	const long *db = (const long*) b;
	return (*da > *db) - (*da < *db);
}

int compareLongsDescending(const void *a, const void *b)
{
	const long *da = (const long*) a;
	const long *db = (const long*) b;
	return (*da < *db) - (*da > *db);
}

int compareFloatsAscending(const void *a, const void *b)
{
	const float *da = (const float*) a;
	const float *db = (const float*) b;
	return (*da > *db) - (*da < *db);
}

int compareFloatsDescending(const void *a, const void *b)
{
	const float *da = (const float*) a;
	const float *db = (const float*) b;
	return (*da < *db) - (*da > *db);
}

int compareTracers(const void *a, const void *b)
{
	if (((Tracer*) a)->id > ((Tracer*) b)->id)
		return 1;
	else if (((Tracer*) a)->id < ((Tracer*) b)->id)
		return -1;
	else
		return 0;
}

int compareHalos(const void *a, const void *b)
{
	if (((Halo*) a)->id_sort > ((Halo*) b)->id_sort)
		return 1;
	else if (((Halo*) a)->id_sort < ((Halo*) b)->id_sort)
		return -1;
	else
		return 0;
}

int compareSubhalos(const void *a, const void *b)
{
	if (((SubPointer*) a)->id > ((SubPointer*) b)->id)
		return 1;
	else if (((SubPointer*) a)->id < ((SubPointer*) b)->id)
		return -1;
	else
		return 0;
}

int compareHaloRequests(const void *a, const void *b)
{
	if (((HaloRequest*) a)->id > ((HaloRequest*) b)->id)
		return 1;
	else if (((HaloRequest*) a)->id < ((HaloRequest*) b)->id)
		return -1;
	else
		return 0;
}

int compareTracerRequests(const void *a, const void *b)
{
	if (((TracerRequest*) a)->id > ((TracerRequest*) b)->id)
		return 1;
	else if (((TracerRequest*) a)->id < ((TracerRequest*) b)->id)
		return -1;
	else
		return 0;
}

int compareResults(const void *a, const void *b)
{
	if (*((ID*) a) > *((ID*) b))
		return 1;
	else if (*((ID*) a) < *((ID*) b))
		return -1;
	else
		return 0;
}

int compareHaloCatalogDataByProc(const void *a, const void *b)
{
	if (((HaloCatalogData*) a)->proc > ((HaloCatalogData*) b)->proc)
		return 1;
	else if (((HaloCatalogData*) a)->proc < ((HaloCatalogData*) b)->proc)
		return -1;
	else
		return 0;
}

int compareHaloCatalogDataByID(const void *a, const void *b)
{
	if (((HaloCatalogData*) a)->id > ((HaloCatalogData*) b)->id)
		return 1;
	else if (((HaloCatalogData*) a)->id < ((HaloCatalogData*) b)->id)
		return -1;
	else
		return 0;
}

/*************************************************************************************************
 * FUNCTIONS - ARRAY AND NUMBER UTILS
 *************************************************************************************************/

/*
 * It is tempting to write these functions macro-like as return (a < b) ? a : b;. The issue is
 * that this evaluates the parameters twice which can have unintended consequences.
 */
int imin(int a, int b)
{
	if (a > b)
		return b;
	return a;
}

int imax(int a, int b)
{
	if (a > b)
		return a;
	return b;
}

/*
 * Return the index of the array element closest to x.
 */
int closestInArray(float *a, int n, float x)
{
	int i, idx;

	idx = 0;
	for (i = 1; i < n; i++)
	{
		if (fabs(a[i] - x) < fabs(a[idx] - x))
			idx = i;
	}

	return idx;
}

void linearIndices(float *indices, int N, float min, float max)
{
	int i;

	for (i = 0; i < N; i++)
		indices[i] = min + i * (max - min) / (N - 1);
}

void linearIndicesDouble(double *indices, int N, double min, double max)
{
	int i;

	for (i = 0; i < N; i++)
		indices[i] = min + i * (max - min) / (N - 1);
}

void logIndices(float *indices, int N, float min, float max)
{
	int i;
	float min_log, max_log, width;

	min_log = log10(min);
	max_log = log10(max);
	width = (max_log - min_log) / (N - 1);
	for (i = 0; i < N; i++)
		indices[i] = pow(10, min_log + i * width);
}

void logIndicesDouble(double *indices, int N, double min, double max)
{
	int i;
	double min_log, max_log, width;

	min_log = log10(min);
	max_log = log10(max);
	width = (max_log - min_log) / (N - 1);
	for (i = 0; i < N; i++)
		indices[i] = pow(10, min_log + i * width);
}

/*
 * Return n integers between min and max into the out buffer. Note that the function uses a fixed
 * seed if so chosen by the user.
 */
void randomIntegers(int min, int max, int n, int *out)
{
	int i, range;

	if (config.random_seed > 0)
		srand(config.random_seed);
	else
		srand(time(0));

	range = max - min;
	for (i = 0; i < n; i++)
		out[i] = min + rand() % (range + 1);

	return;
}

/*************************************************************************************************
 * FUNCTIONS - STRING UTILS
 *************************************************************************************************/

int stringStartsWith(const char *str, const char *start)
{
	return strncmp(start, str, strlen(start)) == 0;
}

int stringEndsWith(const char *str, const char *end)
{
	size_t len_str, len_end;

	len_str = strlen(str);
	len_end = strlen(end);
	if (len_end > len_str)
		return 0;
	return strncmp(str + len_str - len_end, end, len_end) == 0;
}

int numberInString(char *str, int number_start, int number_end)
{
	int i, idx, res;
	char num[10];

	for (i = 0; i <= number_end - number_start; i++)
	{
		idx = i + number_start;
		num[i] = str[idx];
	}
	num[i + 1] = '\n';
	res = atoi(num);

	return res;
}

int stringToListInt(char *str, int *values, int n_max, int allow_duplicates)
{
	char *p, val[20];
	int i, j, n_found, n_digits;

	n_found = 0;
	p = str;
	n_digits = 0;
	for (i = 0; i <= strlen(str); i++)
	{
		if ((i == strlen(str)) || (*p == ','))
		{
			if (n_found >= n_max)
				error(__FFL__, "Found too many numbers (max %d) in string %s.\n", n_max, str);
			val[n_digits] = '\0';
			values[n_found] = atoi(val);
			n_digits = 0;
			n_found++;
		} else
		{
			val[n_digits] = *p;
			n_digits++;
		}

		p++;
	}

	if (!allow_duplicates)
	{
		for (i = 0; i < n_found; i++)
		{
			for (j = i + 1; j < n_found; j++)
			{
				if (values[i] == values[j])
					error(__FFL__, "Found duplicated value %d in string %s.\n", values[i], str);
			}
		}
	}

	return n_found;
}

int stringToListLong(char *str, long int *values, int n_max, int allow_duplicates)
{
	char *p, val[20];
	int i, j, n_found, n_digits;

	n_found = 0;
	p = str;
	n_digits = 0;
	for (i = 0; i <= strlen(str); i++)
	{
		if ((i == strlen(str)) || (*p == ','))
		{
			if (n_found >= n_max)
				error(__FFL__, "Found too many numbers (max %d) in string %s.\n", n_max, str);
			val[n_digits] = '\0';
			values[n_found] = atoi(val);
			n_digits = 0;
			n_found++;
		} else
		{
			val[n_digits] = *p;
			n_digits++;
		}

		p++;
	}

	if (!allow_duplicates)
	{
		for (i = 0; i < n_found; i++)
		{
			for (j = i + 1; j < n_found; j++)
			{
				if (values[i] == values[j])
					error(__FFL__, "Found duplicated value %ld in string %s.\n", values[i], str);
			}
		}
	}

	return n_found;
}

int stringToListFloat(char *str, float *values, int n_max, int allow_duplicates)
{
	char *p, val[20];
	int i, j, n_found, n_digits;

	n_found = 0;
	p = str;
	n_digits = 0;
	for (i = 0; i <= strlen(str); i++)
	{
		if ((i == strlen(str)) || (*p == ','))
		{
			if (n_found >= n_max)
				error(__FFL__, "Found too many numbers (max %d) in string %s.\n", n_max, str);
			val[n_digits] = '\0';
			values[n_found] = atof(val);
			n_digits = 0;
			n_found++;
		} else
		{
			val[n_digits] = *p;
			n_digits++;
		}

		p++;
	}

	if (!allow_duplicates)
	{
		for (i = 0; i < n_found; i++)
		{
			for (j = i + 1; j < n_found; j++)
			{
				if (fabs(values[i] - values[j]) < 1E-12)
					error(__FFL__, "Found duplicated value %.6f in string %s.\n", values[i], str);
			}
		}
	}

	return n_found;
}

/*
 * The output to this function is a simple char pointer to avoid complex formats that depend on
 * how the output is specified. The user simply passes (char *) [some char structure] as well as
 * the length of the individual output strings.
 *
 * Note that spaces are ignored only after separators (commas), but are kept otherwise. That is,
 * the individual string elements are allowed to contain spaces, and those spaces are preserved.
 */
int stringToListString(char *str, char *values, int n_max, int allow_duplicates, int str_len)
{
	char *p, val[100];
	int i, j, n_found, n_digits, ignore_space;

	if (strlen(str) == 0)
		return 0;

	if (str_len > 100)
		error(__FFL__, "Cannot handle more than 100 characters in values.\n");

	n_found = 0;
	p = str;
	n_digits = 0;
	ignore_space = 0;
	for (i = 0; i <= strlen(str); i++)
	{
		if ((*p == ' ') && ignore_space)
		{
			p++;
			continue;
		}

		if ((i == strlen(str)) || (*p == ','))
		{
			if (n_found >= n_max)
				error(__FFL__, "Found too many values (max %d) in string %s.\n", n_max, str);
			val[n_digits] = '\0';
			strcpy(values + n_found * str_len, val);
			n_digits = 0;
			n_found++;
			ignore_space = 1;
		} else
		{
			val[n_digits] = *p;
			n_digits++;
			if (n_digits >= str_len - 1)
				error(__FFL__, "Found value with too many characters (%d), allowed %d.\n", n_digits,
						str_len);
			ignore_space = 0;
		}

		p++;
	}

	if (!allow_duplicates)
	{
		for (i = 0; i < n_found; i++)
		{
			for (j = i + 1; j < n_found; j++)
			{
				if (strcmp(values + i * str_len, values + j * str_len) == 0)
					error(__FFL__, "Found duplicated value '%s' in string %s.\n",
							values + i * str_len, str);
			}
		}
	}

	return n_found;
}

/*************************************************************************************************
 * FUNCTIONS - SYSTEM
 *************************************************************************************************/

/*
 * Recursively create paths using mkdir. If is_file_path, we assume that the top element is a
 * filename (rather than directory name) and do not create it. This function deliberately does
 * not use dirname / basename since they depend on the library environment to some extent.
 */
void createPathPermissions(char *path, mode_t permissions, int is_file_path)
{
	char tmp[1000], *p = NULL;
	size_t len;

	sprintf(tmp, "%s", path);
	len = strlen(tmp);
	if (tmp[len - 1] == '/')
		tmp[len - 1] = 0;
	for (p = tmp + 1; *p; p++)
		if (*p == '/')
		{
			*p = 0;
			mkdir(tmp, permissions);
			*p = '/';
		}
	if (!is_file_path)
		mkdir(tmp, permissions);
}

/*
 * Wrapper that uses the default folder permissions
 */
void createPath(char *path, int is_file_path)
{
	createPathPermissions(path, DEFAULT_DIR_PERMISSIONS, is_file_path);
}

/*************************************************************************************************
 * FUNCTIONS - OTHER
 *************************************************************************************************/

void assertBool(int b)
{
	assert((b == 0) || (b == 1));
}
