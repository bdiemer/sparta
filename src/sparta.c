/*************************************************************************************************
 *
 * This unit contains the main() runtime function for sparta.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <mpi.h>

#include "global.h"
#include "global_types.h"
#include "statistics.h"
#include "constants.h"
#include "config.h"
#include "geometry.h"
#include "memory.h"
#include "run.h"
#include "halos/halo_particles.h"
#include "halos/halo_so.h"
#include "lb/domain.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

int main(int argc, char **argv)
{
	int ret, restart, cfg_file_idx;
	GlobalStatistics *gs = NULL;

	/*
	 * Create an MPI communicator. We'll add a topology later in the domain decomposition step.
	 */
	ret = MPI_Init(&argc, &argv);
	if (ret != MPI_SUCCESS)
	{
		output(0, "[%4d] MPI error %d while starting program. Terminating.\n", proc, ret);
		MPI_Abort(MPI_COMM_WORLD, ret);
	}

	MPI_Comm_size(MPI_COMM_WORLD, &n_proc);
	MPI_Comm_rank(MPI_COMM_WORLD, &proc);
	is_main_proc = (proc == MAIN_PROC);

	if ((is_main_proc) && (n_proc > MAX_PROCS))
		error(__FFL__,
				"Found %d processes, greater than MAX_PROCS = %d. Please increase MAX_PROCS.\n",
				n_proc, MAX_PROCS);

	/*
	 * Check input parameters. There can be 1 (argc = 2), which must be the config file, or 2,
	 * where the first is a restart flag (argc = 3).
	 */
	restart = 0;
	cfg_file_idx = -1;
	if (argc < 2)
	{
		if (is_main_proc)
			error(__FFL__,
					"[Main] Missing config file parameter. Usage: sparta [-restart] <config file>.\n");
	} else if (argc == 2)
	{
		restart = 0;
		cfg_file_idx = 1;
	} else if (argc == 3)
	{
		if (!(strcmp(argv[1], "-restart") == 0))
			if (is_main_proc)
				error(__FFL__,
						"[Main] Found unknown parameter '%s'. Usage: sparta [-restart] <config file>.\n",
						argv[1]);
		restart = 1;
		cfg_file_idx = 2;
	} else
	{
		if (is_main_proc)
			error(__FFL__, "[Main] Too many parameters. Usage: sparta [-restart] <config file>.\n");
	}

	/*
	 * Check basic input, create global statistics object
	 */
	if (is_main_proc)
	{
		gs = (GlobalStatistics*) memAlloc(__FFL__, MID_IGNORE, sizeof(GlobalStatistics));
		initGlobalStatistics(gs);
		gs->timers_0_cum[T0_INIT] = -MPI_Wtime();

		printLine(0);
		if (restart)
			output(0, "[Main] Restarting SPARTA. Running on %d processes.\n", n_proc);
		else
			output(0, "[Main] Welcome to SPARTA. Running on %d processes.\n", n_proc);
		printLine(0);
	}

	/*
	 * Create certain MPI types. This needs to be done regardless of whether we are restarting.
	 */
	initMPITypes();

	/*
	 * Initialize any global GSL variables.
	 */
	initGslMassProfile();
	initGslSO();

	/*
	 * Process config file, snapshot list etc, broadcast to all procs. This is not necessary if we
	 * are restarting, since we'll load the config from the restart files; however, we do need one
	 * parameter: the directory where the restart files are. Thus, we process the config even in
	 * case of a restart, but with the file_only flag on.
	 */
	processConfig(argv[cfg_file_idx], restart);

	/*
	 * Reset the memory statistics even if we are restarting. In that case, we need to start
	 * counting allocated memory now and then combine that with the stored information later.
	 */
	resetMemoryStatistics();

	if (!restart)
	{
		/*
		 * Print the compile-time and run-time configuration.
		 */
		if (is_main_proc)
		{
			printCompiledConfig();
			printTypeSizes();
			printConfig();
		}

		/*
		 * Allocate global memory. If we are restarting, we first need to re-load the full config
		 * because the size of some fields depends on it.
		 *
		 * Second, we need to reset some of the global variables to a defined initial state. Again,
		 * that does not need to happen during a restart.
		 */
		allocateGlobalMemory();
		initMassProfileVars();

		/*
		 * Split up the domain spatially, compute each proc's coordinates and box. Note that this
		 * function does not yet create an MPI communicator, meaning we need to use MPI_COMM_WORLD
		 * until run() has been executed.
		 */
		initialDomainDecomposition();

		fflush(stdout);
		MPI_Barrier(MPI_COMM_WORLD);
		if (is_main_proc)
			printLine(2);
		fflush(stdout);
		MPI_Barrier(MPI_COMM_WORLD);

		/*
		 * Create bounding boxes around the snapshot files, or read the pre-computed boxes from files.
		 */
#if DO_READ_PARTICLES
		getSnapBoxes();
#endif
	}

	/*
	 * Perform the actual work, i.e. the part that needs access to the main array of halos.
	 */
	processSimulation(gs, restart);

	/*
	 * Finalize, print timing statistics
	 */
	endGslMassProfile();
	endGslSO();

	MPI_Barrier(comm);
	if (is_main_proc)
	{
		printTimingStatisticsFinal(gs);
		printLine(0);
		memFree(__FFL__, MID_IGNORE, gs, sizeof(GlobalStatistics));
	}
	freeGlobalMemory();
	printMemoryStatistics(0, 1);
	if (is_main_proc)
	{
		printLine(0);
		output(0, "[Main] Shutting down.\n");
		printLine(0);
	}

	MPI_Finalize();

	return EXIT_SUCCESS;
}
