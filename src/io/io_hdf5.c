/*************************************************************************************************
 *
 * This unit contains HDF5 utilities that are not specific to SPARTA.
 *
 * Many of the functions in this unit can take any data type as indicated by the dtype parameter.
 * This parameter can be any of the following, and will be mapped to these HDF5 data types:
 *
 * DTYPE_INT8   -> H5T_NATIVE_INT_LEAST8
 * DTYPE_INT16  -> H5T_NATIVE_INT16
 * DTYPE_INT32  -> H5T_NATIVE_INT
 * DTYPE_INT64  -> H5T_NATIVE_LONG
 * DTYPE_FLOAT  -> H5T_NATIVE_FLOAT
 * DTYPE_DOUBLE -> H5T_NATIVE_DOUBLE
 *
 * This library automatically compresses all datasets using zlib.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>

#include "io_hdf5.h"
#include "../global.h"
#include "../global_types.h"
#include "../config.h"
#include "../memory.h"

/*************************************************************************************************
 * FUNCTIONS - GENERAL
 *************************************************************************************************/

void getHdf5TypeString(char *str, hid_t type)
{
	switch (type)
	{
	case H5I_FILE:
		sprintf(str, "%s", "file");
		break;
	case H5I_GROUP:
		sprintf(str, "%s", "group");
		break;
	case H5I_DATATYPE:
		sprintf(str, "%s", "datatype");
		break;
	case H5I_DATASPACE:
		sprintf(str, "%s", "dataspace");
		break;
	case H5I_DATASET:
		sprintf(str, "%s", "dataset");
		break;
	case H5I_ATTR:
		sprintf(str, "%s", "attribute");
		break;
	default:
		error(__FFL__, "Unknwon HDF5 type, %s.\n", type);
	}
}

/*
 * A simple error check that stops the code if a HDF5 function failed. The code will probably
 * crash anyway, this provides a slightly more elegant error message.
 */
void hdf5CheckForError(const char *file, const char *func, int line, hid_t value,
		const char *format, ...)
{
	if (value < 0)
	{
		char msg[1000];

		va_list args;
		va_start(args, format);
		vsprintf(msg, format, args);
		va_end(args);

		error(file, func, line, "HDF5 return value %d indicates error when %s.\n", value, msg);
	}
}

/*
 * A more advanced error checker for functions that open fields such as groups and datasets. Note
 * that the HDF5 code prints its own error stack, but that can be confusing. This function provides
 * the user with a clear-ish message as to what went wrong.
 */
void hdf5CheckForErrorOpen(const char *file, const char *func, int line, hid_t loc_id, hid_t value,
		const char *name, hid_t type)
{
	if (value < 0)
	{
		char locstr[100], typestr[20];

		H5Iget_name(loc_id, locstr, 100);
		getHdf5TypeString(typestr, type);
		error(file, func, line, "Could not open HDF5 %s %s/%s. Please check that it exists.\n",
				typestr, locstr, name);
	}
}

void hdf5CheckForErrorWrite(const char *file, const char *func, int line, hid_t loc_id, hid_t value,
		const char *name, hid_t type)
{
	if (value < 0)
	{
		char locstr[100], typestr[20];

		H5Iget_name(loc_id, locstr, 100);
		getHdf5TypeString(typestr, type);
		error(file, func, line, "Could not write HDF5 %s %s/%s.\n", typestr, locstr, name);
	}
}

/*
 * Internally, we use different data types than HDF5 to account for those types hdf5 cannot
 * natively handle, such as int8. This routine converts the datatypes.
 */
hid_t hdf5DatatypeFromSparta(int dtype)
{
	switch (dtype)
	{
	case DTYPE_INT8:
		return H5T_NATIVE_INT_LEAST8;
	case DTYPE_INT16:
		return H5T_NATIVE_INT16;
	case DTYPE_INT32:
		return H5T_NATIVE_INT;
	case DTYPE_INT64:
		return H5T_NATIVE_LONG;
	case DTYPE_FLOAT:
		return H5T_NATIVE_FLOAT;
	case DTYPE_DOUBLE:
		return H5T_NATIVE_DOUBLE;
	case DTYPE_STRING:
		error(__FFL__, "Cannot convert DTYPE_STRING to hdf5 type.\n");
	default:
		error(__FFL__, "Unknown data type, %d.\n", dtype);
	}

	return DTYPE_INVALID;
}

/*
 * Internally, we use different data types than HDF5 to account for those types hdf5 cannot
 * natively handle, such as int8. This routine converts the datatypes.
 */
hid_t hdf5DatatypeToSparta(hid_t dtype_hdf5)
{
	if (H5Tequal(dtype_hdf5, H5T_NATIVE_INT_LEAST8))
		return DTYPE_INT8;
	else if (H5Tequal(dtype_hdf5, H5T_NATIVE_INT16))
		return DTYPE_INT16;
	else if (H5Tequal(dtype_hdf5, H5T_NATIVE_INT))
		return DTYPE_INT32;
	else if (H5Tequal(dtype_hdf5, H5T_NATIVE_LONG))
		return DTYPE_INT64;
	else if (H5Tequal(dtype_hdf5, H5T_NATIVE_FLOAT))
		return DTYPE_FLOAT;
	else if (H5Tequal(dtype_hdf5, H5T_NATIVE_DOUBLE))
		return DTYPE_DOUBLE;
	else
		error(__FFL__, "Unknown data type, %d.\n", dtype_hdf5);

	return DTYPE_INVALID;
}

void hdf5ListDatasetsInGroup(hid_t group_id)
{
	int i, object_type;
	hsize_t n_ds;
	char ds_name[100];

	H5Gget_num_objs(group_id, &n_ds);
	for (i = 0; i < n_ds; i++)
	{
		H5Gget_objname_by_idx(group_id, (hsize_t) i, ds_name, (size_t) 100);
		object_type = H5Gget_objtype_by_idx(group_id, (size_t) i);

		if (object_type != H5G_DATASET)
			continue;

		output(0, "%s\n", ds_name);
	}
}

/*
 * By default, testing the group would cause error output if it does not exist. Thus, we
 * temporarily turn off error output.
 */
int hdf5GroupExists(hid_t obj_id, char *group_name)
{
	herr_t status;

	H5Eset_auto2(H5E_DEFAULT, NULL, NULL);
	status = H5Gget_objinfo(obj_id, group_name, 0, NULL);
	H5Eset_auto2(H5E_DEFAULT, (void*) H5Eprint, NULL);

	return (status == 0);
}

int hdf5ObjectExists(hid_t obj_id, char *ds_name)
{
	herr_t status;

	H5Eset_auto2(H5E_DEFAULT, NULL, NULL);
	status = H5Oexists_by_name(obj_id, ds_name, 0);
	H5Eset_auto2(H5E_DEFAULT, (void*) H5Eprint, NULL);

	return (status > 0);
}

void hdf5GetDatasetDimensions(hid_t loc_id, const char *name, int *dim1)
{
	int ndims;
	hid_t dset, filespace;
	hsize_t dsp_dims[2], dsp_max_dims[2];

	dset = H5Dopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, dset, name, H5I_DATASET);
	filespace = H5Dget_space(dset);
	ndims = H5Sget_simple_extent_dims(filespace, dsp_dims, dsp_max_dims);

	if (ndims != 1)
		error(__FFL__, "Expected dimensionality 1, found %d.\n", ndims);

	*dim1 = dsp_dims[0];

	H5Dclose(dset);
	H5Sclose(filespace);
}

void hdf5GetDatasetDimensions2D(hid_t loc_id, const char *name, int *dim1, int *dim2)
{
	int ndims;
	hid_t dset, filespace;
	hsize_t dsp_dims[2], dsp_max_dims[2];

	dset = H5Dopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, dset, name, H5I_DATASET);
	filespace = H5Dget_space(dset);
	ndims = H5Sget_simple_extent_dims(filespace, dsp_dims, dsp_max_dims);

	if (ndims != 2)
		error(__FFL__, "Expected dimensionality 2, found %d.\n", ndims);

	*dim1 = dsp_dims[0];
	*dim2 = dsp_dims[1];

	H5Dclose(dset);
	H5Sclose(filespace);
}

/*
 * Not all characters are allowed in HDF5 name strings, e.g., the names of datasets. This function
 * removes such characters from a string.
 */
int hdf5SafeName(char *name_in, char *name_out)
{
	int i, j, len, changed;
	char c;

	len = strlen(name_in);
	j = 0;
	changed = 0;
	for (i = 0; i < len; i++)
	{
		c = name_in[i];
		if ((c == '/') || (c == '\\'))
		{
			changed = 1;
			continue;
		}
		name_out[j] = c;
		j++;
	}
	if (j == 0)
		error(__FFL__,
				"While making the string '%s' HDF5-safe, could not find any allowed characters.\n",
				name_in);
	name_out[j] = '\0';

	return changed;
}

/*************************************************************************************************
 * FUNCTIONS - DATASPACES AND DATASETS
 *************************************************************************************************/

hid_t hdf5Dataspace1DFixed(hsize_t N)
{
	hid_t dsp;
	hsize_t current_dims[1] =
		{N};
	hsize_t max_dims[1] =
		{N};

	dsp = H5Screate(H5S_SIMPLE);
	H5Sset_extent_simple(dsp, 1, current_dims, max_dims);

	return dsp;
}

hid_t hdf5Dataspace2DFixed(hsize_t N1, hsize_t N2)
{
	hid_t dsp;
	hsize_t current_dims[2] =
		{N1, N2};
	hsize_t max_dims[2] =
		{N1, N2};

	dsp = H5Screate(H5S_SIMPLE);
	H5Sset_extent_simple(dsp, 2, current_dims, max_dims);

	return dsp;
}

hid_t hdf5Dataspace3DFixed(hsize_t N1, hsize_t N2, hsize_t N3)
{
	hid_t dsp;
	hsize_t current_dims[3] =
		{N1, N2, N3};
	hsize_t max_dims[3] =
		{N1, N2, N3};

	dsp = H5Screate(H5S_SIMPLE);
	H5Sset_extent_simple(dsp, 3, current_dims, max_dims);

	return dsp;
}

hid_t hdf5DataspaceNDFixed(int n_dims, hsize_t *N)
{
	hid_t dsp;

	dsp = H5Screate(H5S_SIMPLE);
	H5Sset_extent_simple(dsp, n_dims, N, N);

	return dsp;
}

hid_t hdf5Dataspace1DVar(hsize_t N, hsize_t N1max)
{
	hid_t dsp;
	hsize_t current_dims[1] =
		{N};
	hsize_t max_dims[1] =
		{N1max};

	dsp = H5Screate(H5S_SIMPLE);
	H5Sset_extent_simple(dsp, 1, current_dims, max_dims);

	return dsp;
}

hid_t hdf5Dataspace2DVar(hsize_t N1, hsize_t N1max, hsize_t N2, hsize_t N2max)
{
	hid_t dsp;
	hsize_t current_dims[2] =
		{N1, N2};
	hsize_t max_dims[2] =
		{N1max, N2max};

	dsp = H5Screate(H5S_SIMPLE);
	H5Sset_extent_simple(dsp, 2, current_dims, max_dims);

	return dsp;
}

hid_t hdf5Dataspace3DVar(hsize_t N1, hsize_t N1max, hsize_t N2, hsize_t N2max, hsize_t N3,
		hsize_t N3max)
{
	hid_t dsp;
	hsize_t current_dims[3] =
		{N1, N2, N3};
	hsize_t max_dims[3] =
		{N1max, N2max, N3max};

	dsp = H5Screate(H5S_SIMPLE);
	H5Sset_extent_simple(dsp, 3, current_dims, max_dims);

	return dsp;
}

hid_t hdf5DataspaceNDVar(int n_dims, hsize_t *N, hsize_t *Nmax)
{
	hid_t dsp;

	dsp = H5Screate(H5S_SIMPLE);
	H5Sset_extent_simple(dsp, n_dims, N, Nmax);

	return dsp;
}

/*************************************************************************************************
 * FUNCTIONS - DATASETS
 *************************************************************************************************/

/*
 * This function uses the information in the dataspace field to determine the correct layout and
 * chunking of a dataset with fixed size.
 */
hid_t hdf5DatasetFixed(hid_t loc_id, hid_t dsp, const char *name, int dtype)
{
	int i, ndims;
	hid_t params, dataset;

	ndims = H5Sget_simple_extent_ndims(dsp);
	hsize_t dsp_dims[ndims], dsp_max_dims[ndims], chunk_size[ndims];
	H5Sget_simple_extent_dims(dsp, dsp_dims, dsp_max_dims);

	for (i = 0; i < ndims; i++)
	{
		if (dsp_max_dims[i] > HDF5_DEFAULT_CHUNK_SIZE)
			chunk_size[i] = HDF5_DEFAULT_CHUNK_SIZE;
		else
			chunk_size[i] = dsp_max_dims[i];
	}

	params = H5Pcreate(H5P_DATASET_CREATE);
	H5Pset_chunk(params, ndims, chunk_size);
	H5Pset_deflate(params, config.output_compression_level);
	dataset = H5Dcreate(loc_id, name, hdf5DatatypeFromSparta(dtype), dsp, H5P_DEFAULT, params,
	H5P_DEFAULT);
	hdf5CheckForError(__FFL__, dataset, "creating 1D fixed dataset %s", name);
	H5Pclose(params);

	return dataset;
}

/*
 * When the dataspace has variable size, we do not know what the optimal chunking would be and let
 * the user decide.
 */
hid_t hdf5Dataset1DVar(hid_t loc_id, hid_t dsp, const char *name, int dtype, hsize_t chunk_size)
{
	hsize_t chunk_dims[1] =
		{chunk_size};
	hid_t params, dataset;

	params = H5Pcreate(H5P_DATASET_CREATE);
	H5Pset_chunk(params, 1, chunk_dims);
	H5Pset_deflate(params, config.output_compression_level);
	dataset = H5Dcreate(loc_id, name, hdf5DatatypeFromSparta(dtype), dsp, H5P_DEFAULT, params,
	H5P_DEFAULT);
	hdf5CheckForError(__FFL__, dataset, "creating 1D variable dataset %s", name);
	H5Pclose(params);

	return dataset;
}

hid_t hdf5Dataset2DVar(hid_t loc_id, hid_t dsp, const char *name, int dtype, hsize_t chunk_size_1,
		hsize_t chunk_size_2)
{
	hsize_t chunk_dims[2] =
		{chunk_size_1, chunk_size_2};
	hid_t params, dataset;

	params = H5Pcreate(H5P_DATASET_CREATE);
	H5Pset_chunk(params, 2, chunk_dims);
	H5Pset_deflate(params, config.output_compression_level);
	dataset = H5Dcreate(loc_id, name, hdf5DatatypeFromSparta(dtype), dsp, H5P_DEFAULT, params,
	H5P_DEFAULT);
	hdf5CheckForError(__FFL__, dataset, "creating 2D variable dataset %s", name);
	H5Pclose(params);

	return dataset;
}

hid_t hdf5Dataset3DVar(hid_t loc_id, hid_t dsp, const char *name, int dtype, hsize_t chunk_size_1,
		hsize_t chunk_size_2, hsize_t chunk_size_3)
{
	hsize_t chunk_dims[3] =
		{chunk_size_1, chunk_size_2, chunk_size_3};
	hid_t params, dataset;

	params = H5Pcreate(H5P_DATASET_CREATE);
	H5Pset_chunk(params, 3, chunk_dims);
	H5Pset_deflate(params, config.output_compression_level);
	dataset = H5Dcreate(loc_id, name, hdf5DatatypeFromSparta(dtype), dsp, H5P_DEFAULT, params,
	H5P_DEFAULT);
	hdf5CheckForError(__FFL__, dataset, "creating 3D variable dataset %s", name);
	H5Pclose(params);

	return dataset;
}

hid_t hdf5DatasetNDVar(int n_dims, hid_t loc_id, hid_t dsp, const char *name, int dtype,
		hsize_t *chunk_size)
{
	hid_t params, dataset;

	params = H5Pcreate(H5P_DATASET_CREATE);
	H5Pset_chunk(params, n_dims, chunk_size);
	H5Pset_deflate(params, config.output_compression_level);
	dataset = H5Dcreate(loc_id, name, hdf5DatatypeFromSparta(dtype), dsp, H5P_DEFAULT, params,
	H5P_DEFAULT);
	hdf5CheckForError(__FFL__, dataset, "creating ND variable dataset %s", name);
	H5Pclose(params);

	return dataset;
}

void hdf5Dataset1DResize(hid_t loc_id, hsize_t N)
{
	hsize_t current_dims[1] =
		{N};

	H5Dset_extent(loc_id, current_dims);
}

void hdf5Dataset2DResize(hid_t loc_id, hsize_t N1, hsize_t N2)
{
	hsize_t current_dims[2] =
		{N1, N2};

	H5Dset_extent(loc_id, current_dims);
}

void hdf5Dataset3DResize(hid_t loc_id, hsize_t N1, hsize_t N2, hsize_t N3)
{
	hsize_t current_dims[3] =
		{N1, N2, N3};

	H5Dset_extent(loc_id, current_dims);
}

void hdf5DatasetNDResize(hid_t loc_id, hsize_t *N)
{
	H5Dset_extent(loc_id, N);
}

/*************************************************************************************************
 * FUNCTIONS - WRITE/READ ATTRIBUTES
 *************************************************************************************************/

void hdf5WriteAttributeInt(hid_t loc_id, const char *name, int value)
{
	hid_t dsp, attr;

	dsp = H5Screate(H5S_SCALAR);
	attr = H5Acreate(loc_id, name, H5T_NATIVE_INT, dsp, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForErrorWrite(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Awrite(attr, H5T_NATIVE_INT, &value);
	H5Aclose(attr);
	H5Sclose(dsp);
}

void hdf5WriteAttributeLong(hid_t loc_id, const char *name, long int value)
{
	hid_t dsp, attr;

	dsp = H5Screate(H5S_SCALAR);
	attr = H5Acreate(loc_id, name, H5T_NATIVE_LONG, dsp, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForErrorWrite(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Awrite(attr, H5T_NATIVE_LONG, &value);
	H5Aclose(attr);
	H5Sclose(dsp);
}

void hdf5WriteAttributeFloat(hid_t loc_id, const char *name, float value)
{
	hid_t dsp, attr;

	dsp = H5Screate(H5S_SCALAR);
	attr = H5Acreate(loc_id, name, H5T_NATIVE_FLOAT, dsp, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForErrorWrite(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Awrite(attr, H5T_NATIVE_FLOAT, &value);
	H5Aclose(attr);
	H5Sclose(dsp);
}

void hdf5WriteAttributeDouble(hid_t loc_id, const char *name, double value)
{
	hid_t dsp, attr;

	dsp = H5Screate(H5S_SCALAR);
	attr = H5Acreate(loc_id, name, H5T_NATIVE_DOUBLE, dsp, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForErrorWrite(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Awrite(attr, H5T_NATIVE_DOUBLE, &value);
	H5Aclose(attr);
	H5Sclose(dsp);
}

void hdf5WriteAttributeString(hid_t loc_id, const char *name, char *value)
{
	hid_t dsp, attr, dtype;

	dtype = H5Tcopy(H5T_C_S1);
	H5Tset_size(dtype, strlen(value));

	dsp = H5Screate(H5S_SCALAR);
	attr = H5Acreate(loc_id, name, dtype, dsp, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForErrorWrite(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Awrite(attr, dtype, value);
	H5Aclose(attr);
	H5Sclose(dsp);
}

void hdf5WriteAttributeInt1D(hid_t loc_id, const char *name, int N, int *values)
{
	hid_t dsp, attr;

	dsp = hdf5Dataspace1DFixed(N);
	attr = H5Acreate(loc_id, name, H5T_NATIVE_INT, dsp, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForErrorWrite(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Awrite(attr, H5T_NATIVE_INT, values);
	H5Aclose(attr);
	H5Sclose(dsp);
}

void hdf5WriteAttributeFloat1D(hid_t loc_id, const char *name, int N, float *values)
{
	hid_t dsp, attr;

	dsp = hdf5Dataspace1DFixed(N);
	attr = H5Acreate(loc_id, name, H5T_NATIVE_FLOAT, dsp, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForErrorWrite(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Awrite(attr, H5T_NATIVE_FLOAT, values);
	H5Aclose(attr);
	H5Sclose(dsp);
}

void hdf5WriteAttributeDouble1D(hid_t loc_id, const char *name, int N, double *values)
{
	hid_t dsp, attr;

	dsp = hdf5Dataspace1DFixed(N);
	attr = H5Acreate(loc_id, name, H5T_NATIVE_DOUBLE, dsp, H5P_DEFAULT, H5P_DEFAULT);
	hdf5CheckForErrorWrite(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Awrite(attr, H5T_NATIVE_DOUBLE, values);
	H5Aclose(attr);
	H5Sclose(dsp);
}

void hdf5ReadAttributeInt(hid_t loc_id, const char *name, int *value)
{
	hid_t attr;

	attr = H5Aopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Aread(attr, H5T_NATIVE_INT, value);
	H5Aclose(attr);
}

void hdf5ReadAttributeLong(hid_t loc_id, const char *name, long int *value)
{
	hid_t attr;

	attr = H5Aopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Aread(attr, H5T_NATIVE_LONG, value);
	H5Aclose(attr);
}

void hdf5ReadAttributeFloat(hid_t loc_id, const char *name, float *value)
{
	hid_t attr;

	attr = H5Aopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Aread(attr, H5T_NATIVE_FLOAT, value);
	H5Aclose(attr);
}

void hdf5ReadAttributeDouble(hid_t loc_id, const char *name, double *value)
{
	hid_t attr;

	attr = H5Aopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, attr, name, H5I_ATTR);
	H5Aread(attr, H5T_NATIVE_DOUBLE, value);
	H5Aclose(attr);
}

void hdf5ReadAttributeString(hid_t loc_id, const char *name, int *N, char **data, int mem_id)
{
	hid_t dsp_vector, attr, dtype, filetype;
	size_t string_len;

	attr = H5Aopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, attr, name, H5I_ATTR);
	dsp_vector = H5Aget_space(attr);
	filetype = H5Aget_type(attr);
	string_len = H5Tget_size(filetype);
	string_len++;
	dtype = H5Tcopy(H5T_C_S1);
	H5Tset_size(dtype, string_len);

	*data = (char*) memAlloc(__FFL__, mem_id, sizeof(char) * string_len);
	*N = string_len;

	H5Aread(attr, dtype, *data);

	H5Tclose(dtype);
	H5Aclose(attr);
	H5Sclose(dsp_vector);
}

void hdf5ReadAttributeInt1D(hid_t loc_id, const char *name, int *N, int **data, int mem_id)
{
	hid_t dsp_vector, attr;
	hsize_t dsp_dims[1], dsp_max_dims[1], ndims;

	attr = H5Aopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, attr, name, H5I_ATTR);
	dsp_vector = H5Aget_space(attr);
	ndims = H5Sget_simple_extent_dims(dsp_vector, dsp_dims, dsp_max_dims);

	if (ndims != 1)
		error(__FFL__, "Expected dimensionality 1, found %d.\n", ndims);

	*N = dsp_dims[0];
	*data = (int*) memAlloc(__FFL__, mem_id, sizeof(int) * dsp_dims[0]);

	H5Aread(attr, H5T_NATIVE_INT, *data);
	H5Aclose(attr);
	H5Sclose(dsp_vector);
}

void hdf5ReadAttributeLong1D(hid_t loc_id, const char *name, int *N, long int **data, int mem_id)
{
	hid_t dsp_vector, attr;
	hsize_t dsp_dims[1], dsp_max_dims[1], ndims;

	attr = H5Aopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, attr, name, H5I_ATTR);
	dsp_vector = H5Aget_space(attr);
	ndims = H5Sget_simple_extent_dims(dsp_vector, dsp_dims, dsp_max_dims);

	if (ndims != 1)
		error(__FFL__, "Expected dimensionality 1, found %d.\n", ndims);

	*N = dsp_dims[0];
	*data = (long int*) memAlloc(__FFL__, mem_id, sizeof(long int) * dsp_dims[0]);

	H5Aread(attr, H5T_NATIVE_LONG, *data);
	H5Aclose(attr);
	H5Sclose(dsp_vector);
}

void hdf5ReadAttributeFloat1D(hid_t loc_id, const char *name, int *N, float **data, int mem_id)
{
	hid_t dsp_vector, attr;
	hsize_t dsp_dims[1], dsp_max_dims[1], ndims;

	attr = H5Aopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, attr, name, H5I_ATTR);
	dsp_vector = H5Aget_space(attr);
	ndims = H5Sget_simple_extent_dims(dsp_vector, dsp_dims, dsp_max_dims);

	if (ndims != 1)
		error(__FFL__, "Expected dimensionality 1, found %d.\n", ndims);

	*N = dsp_dims[0];
	*data = (float*) memAlloc(__FFL__, mem_id, sizeof(float) * dsp_dims[0]);

	H5Aread(attr, H5T_NATIVE_FLOAT, *data);
	H5Aclose(attr);
	H5Sclose(dsp_vector);
}

void hdf5ReadAttributeDouble1D(hid_t loc_id, const char *name, int *N, double **data, int mem_id)
{
	hid_t dsp_vector, attr;
	hsize_t dsp_dims[1], dsp_max_dims[1], ndims;

	attr = H5Aopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, attr, name, H5I_ATTR);
	dsp_vector = H5Aget_space(attr);
	ndims = H5Sget_simple_extent_dims(dsp_vector, dsp_dims, dsp_max_dims);

	if (ndims != 1)
		error(__FFL__, "Expected dimensionality 1, found %d.\n", ndims);

	*N = dsp_dims[0];
	*data = (double*) memAlloc(__FFL__, mem_id, sizeof(double) * dsp_dims[0]);

	H5Aread(attr, H5T_NATIVE_DOUBLE, *data);
	H5Aclose(attr);
	H5Sclose(dsp_vector);
}

void hdf5InitializeAttribute(Hdf5Attribute *attr)
{
	sprintf(attr->name, "%s", "");
	attr->dtype = DTYPE_INVALID;
	attr->d_int = 0;
	attr->n_alloc = 0;
	attr->p_int = NULL;
}

void hdf5FreeAttribute(Hdf5Attribute *attr, int mem_id)
{
	if (attr->dtype == DTYPE_INT32)
	{
		if (attr->n_alloc > 0)
			memFree(__FFL__, mem_id, attr->p_int, datatypeSize(attr->dtype) * attr->n_alloc);
	} else if (attr->dtype == DTYPE_FLOAT)
	{
		if (attr->n_alloc > 0)
			memFree(__FFL__, mem_id, attr->p_float, datatypeSize(attr->dtype) * attr->n_alloc);
	} else if (attr->dtype == DTYPE_STRING)
	{
		memFree(__FFL__, mem_id, attr->p_float, sizeof(char) * attr->n_alloc);
	} else
	{
		error(__FFL__, "Found invalid attribute datatype %d.\n", attr->dtype);
	}

	hdf5InitializeAttribute(attr);
}

/*
 * Convenience function that reads all attributes in a group into a data structure. Currently,
 * only int32, float32, and string can be treated. Int and float can be 1D arrays.
 */
int hdf5ReadAllAttributes(hid_t loc_id, Hdf5Attribute *attrs, int n_max, int mem_id)
{
	int n_attr, i, ndims;
	hid_t id_attr, id_type, id_dspace;
	H5T_class_t t_class;
	hsize_t dsp_dims[2], dsp_max_dims[2];

	/*
	 * Count attributes, abort if there is not enough output space
	 */
	n_attr = H5Aget_num_attrs(loc_id);
	if (n_attr > n_max)
		error(__FFL__, "Found %d attributes, can only store %d.\n", n_attr, n_max);

	/*
	 * Open each attribute and get its name. Thereafter, it depends on the kind of attribute how
	 * we proceed.
	 */
	for (i = 0; i < n_attr; i++)
	{
		id_attr = H5Aopen_idx(loc_id, (unsigned int) i);
		H5Aget_name(id_attr, HDF5_ATTRIBUTE_NAME_MAXLEN, attrs[i].name);

		// Get type
		id_type = H5Aget_type(id_attr);
		t_class = H5Tget_class(id_type);
		H5Tclose(id_type);

		// Get size
		id_dspace = H5Aget_space(id_attr);
		ndims = H5Sget_simple_extent_dims(id_dspace, dsp_dims, dsp_max_dims);
		H5Sclose(id_dspace);

		if (t_class < 0)
		{
			error(__FFL__, "Found invalid datatype from identifier.\n");
		} else if (t_class == H5T_INTEGER)
		{
			attrs[i].dtype = DTYPE_INT32;
			if (ndims == 0)
			{
				hdf5ReadAttributeInt(loc_id, attrs[i].name, &(attrs[i].d_int));
			} else if (ndims == 1)
			{
				hdf5ReadAttributeInt1D(loc_id, attrs[i].name, &(attrs[i].n_alloc),
						&(attrs[i].p_int), mem_id);
			} else
			{
				error(__FFL__, "Found dimensionality %d in attribute %s.\n", ndims, attrs[i].name);
			}
		} else if (t_class == H5T_FLOAT)
		{
			attrs[i].dtype = DTYPE_FLOAT;
			if (ndims == 0)
			{
				hdf5ReadAttributeFloat(loc_id, attrs[i].name, &(attrs[i].d_float));
			} else if (ndims == 1)
			{
				hdf5ReadAttributeFloat1D(loc_id, attrs[i].name, &(attrs[i].n_alloc),
						&(attrs[i].p_float), mem_id);
			} else
			{
				error(__FFL__, "Found dimensionality %d in attribute %s.\n", ndims, attrs[i].name);
			}
		} else if (t_class == H5T_STRING)
		{
			attrs[i].dtype = DTYPE_STRING;
			if (ndims > 0)
				error(__FFL__, "Found dimensionality %d in string attribute %s.\n", ndims,
						attrs[i].name);
			hdf5ReadAttributeString(loc_id, attrs[i].name, &(attrs[i].n_alloc), &(attrs[i].p_str),
					mem_id);
		} else
		{
			error(__FFL__, "Found untreated datatype from identifier.\n");
		}

		H5Aclose(id_attr);

		if (attrs[i].n_alloc < 0)
			error(__FFL__, "Found negative size of attribute %s, %d.\n", attrs[i].name,
					attrs[i].n_alloc);
	}

	return n_attr;
}

/*
 * Reverse to the read function above, writes a set of attributes
 */
void hdf5WriteAllAttributes(hid_t loc_id, Hdf5Attribute *attrs, int n)
{
	int i;

	for (i = 0; i < n; i++)
	{
		if (attrs[i].dtype == DTYPE_INT32)
		{
			if (attrs[i].n_alloc == 0)
				hdf5WriteAttributeInt(loc_id, attrs[i].name, attrs[i].d_int);
			else
				hdf5WriteAttributeInt1D(loc_id, attrs[i].name, attrs[i].n_alloc, attrs[i].p_int);
		} else if (attrs[i].dtype == DTYPE_FLOAT)
		{
			if (attrs[i].n_alloc == 0)
				hdf5WriteAttributeFloat(loc_id, attrs[i].name, attrs[i].d_float);
			else
				hdf5WriteAttributeFloat1D(loc_id, attrs[i].name, attrs[i].n_alloc,
						attrs[i].p_float);
		} else if (attrs[i].dtype == DTYPE_STRING)
		{
			if (attrs[i].n_alloc == 0)
				error(__FFL__, "Found dimensionality %d in attribute %s.\n", attrs[i].n_alloc,
						attrs[i].name);
			hdf5WriteAttributeString(loc_id, attrs[i].name, attrs[i].p_str);
		} else
		{
			error(__FFL__, "Found untreated datatype in attribute %s.\n", attrs[i].name);
		}
	}
}

/*************************************************************************************************
 * FUNCTIONS - WRITE/READ FIXED-SIZE DATASETS
 *************************************************************************************************/

/*
 * Write an array to a dataset. We only need to re-buffer if the size of the HDF5 datatype does
 * not match the given data type.
 */
void hdf5WriteDataset(hid_t loc_id, const char *name, int dtype, int count, void *mem)
{
	hid_t dset, dsp, dtype_hdf5;

	dset = 0;
	dsp = hdf5Dataspace1DFixed(count);
	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);
	dset = hdf5DatasetFixed(loc_id, dsp, name, dtype);
	H5Dwrite(dset, dtype_hdf5, H5S_ALL, H5S_ALL, H5P_DEFAULT, mem);
	H5Dclose(dset);
	H5Sclose(dsp);
}

/*
 * Note that the 2D array fed to this function must be contiguously allocated.
 */
void hdf5WriteDataset2D(hid_t loc_id, const char *name, int dtype, int count, int count2, void *mem)
{
	hid_t dset, dsp, dtype_hdf5;

	dset = 0;
	dsp = hdf5Dataspace2DFixed(count, count2);
	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);
	dset = hdf5DatasetFixed(loc_id, dsp, name, dtype);
	H5Dwrite(dset, dtype_hdf5, H5S_ALL, H5S_ALL, H5P_DEFAULT, mem);
	H5Dclose(dset);
	H5Sclose(dsp);
}

/*
 * Note that the 2D array fed to this function must be contiguously allocated.
 */
void hdf5WriteDataset3D(hid_t loc_id, const char *name, int dtype, int count, int count2,
		int count3, void *mem)
{
	hid_t dset, dsp, dtype_hdf5;

	dset = 0;
	dsp = hdf5Dataspace3DFixed(count, count2, count3);
	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);
	dset = hdf5DatasetFixed(loc_id, dsp, name, dtype);
	H5Dwrite(dset, dtype_hdf5, H5S_ALL, H5S_ALL, H5P_DEFAULT, mem);
	H5Dclose(dset);
	H5Sclose(dsp);
}

/*
 * Create a buffer using a mask before writing a dataset. Note that this function also accepts a
 * NULL mask, in which case it is equivalent to the standard hdf5WriteDataset function.
 */
void hdf5WriteDatasetMask(hid_t loc_id, const char *name, int dtype, int count, void *mem,
		int_least8_t *mask, int n_write)
{
	int i, j;
	char *p, *buffer;
	hsize_t s;

	if (mask == NULL)
	{
		buffer = (char*) mem;
	} else
	{
		s = datatypeSize(dtype);
		buffer = (char*) memAlloc(__FFL__, MID_HDF5BUFFER, n_write * s);
		p = (char*) mem;

		j = 0;
		for (i = 0; i < count; i++)
		{
			if (mask[i])
			{
				memcpy(&(buffer[j * s]), (char* ) p, s);
				j++;
			}
			p += s;
		}
	}

	hdf5WriteDataset(loc_id, name, dtype, n_write, (void*) buffer);

	if (mask != NULL)
		memFree(__FFL__, MID_HDF5BUFFER, buffer, n_write * s);
}

/*
 * Read a 1D dataset from an HDF5 file of a given type. The dimension is returned in dim1.
 *
 * Note that there is also an NDArray function that determines the type automatically and returns
 * a structure containing that information. This function represents a more low-level interface.
 */
void hdf5ReadDataset(hid_t loc_id, const char *name, int dtype, int *dim1, void **mem, int mem_id)
{
	int ndims;
	void *data_use = NULL;
	hid_t dset, filespace, dtype_hdf5;
	hsize_t dsp_dims[1], dsp_max_dims[1], dtype_size;

	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);
	dtype_size = datatypeSize(dtype);
	dset = H5Dopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, dset, name, H5I_DATASET);
	filespace = H5Dget_space(dset);
	ndims = H5Sget_simple_extent_dims(filespace, dsp_dims, dsp_max_dims);

	if (ndims != 1)
		error(__FFL__, "Expected dimensionality 1, found %d.\n", ndims);

	data_use = (void*) memAlloc(__FFL__, mem_id, dsp_dims[0] * dtype_size);
	H5Dread(dset, dtype_hdf5, H5S_ALL, H5S_ALL, H5P_DEFAULT, data_use);

	*dim1 = dsp_dims[0];
	*mem = data_use;

	H5Dclose(dset);
	H5Sclose(filespace);
}

/*
 * Read a 2D dataset from an HDF5 file of a given type. The dimensions are returned.
 *
 * Note that there is also an NDArray function that determines the type automatically and returns
 * a structure containing that information. This function represents a more low-level interface.
 *
 * IMPORTANT: The array returned by this function is structured differently than the other arrays
 * in SPARTA, due to the array definition of HDF5. The first element (pointer) allocated the entire
 * 2D memory space, even though the pointers in the first dim1 elements act as if they had been
 * used to allocated the dim2 elements. In other words, the array is forced to be allocated
 * contiguously.
 */
void hdf5ReadDataset2D(hid_t loc_id, const char *name, int dtype, int *dim1, int *dim2, void ***mem,
		int mem_id)
{
	int i, ndims;
	void **data_use = NULL;
	hid_t dset, filespace, dtype_hdf5;
	hsize_t dsp_dims[2], dsp_max_dims[2], dtype_size;

	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);
	dtype_size = datatypeSize(dtype);

	dset = H5Dopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, dset, name, H5I_DATASET);
	filespace = H5Dget_space(dset);
	ndims = H5Sget_simple_extent_dims(filespace, dsp_dims, dsp_max_dims);

	if (ndims != 2)
		error(__FFL__, "Expected dimensionality 2, found %d.\n", ndims);

	data_use = (void**) memAlloc(__FFL__, mem_id, dsp_dims[0] * sizeof(void*));
	data_use[0] = (void*) memAlloc(__FFL__, mem_id, dsp_dims[0] * dsp_dims[1] * dtype_size);
	for (i = 1; i < dsp_dims[0]; i++)
		data_use[i] = data_use[0] + i * dsp_dims[1] * dtype_size;

	H5Dread(dset, dtype_hdf5, H5S_ALL, H5S_ALL, H5P_DEFAULT, data_use[0]);

	*dim1 = dsp_dims[0];
	*dim2 = dsp_dims[1];
	*mem = data_use;

	H5Dclose(dset);
	H5Sclose(filespace);
}

void hdf5ReadDataset3D(hid_t loc_id, const char *name, int dtype, int *dim1, int *dim2, int *dim3,
		void ****mem, int mem_id)
{
	int i, j, ndims;
	void ***data_use = NULL;
	hid_t dset, filespace, dtype_hdf5;
	hsize_t dsp_dims[3], dsp_max_dims[3], dtype_size;

	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);
	dtype_size = datatypeSize(dtype);

	dset = H5Dopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, dset, name, H5I_DATASET);
	filespace = H5Dget_space(dset);
	ndims = H5Sget_simple_extent_dims(filespace, dsp_dims, dsp_max_dims);

	if (ndims != 3)
		error(__FFL__, "Expected dimensionality 3, found %d.\n", ndims);

	data_use = (void***) memAlloc(__FFL__, mem_id, dsp_dims[0] * sizeof(void**));
	for (i = 0; i < dsp_dims[0]; i++)
		data_use[i] = (void**) memAlloc(__FFL__, mem_id, dsp_dims[1] * sizeof(void*));

	data_use[0][0] = (void*) memAlloc(__FFL__, mem_id,
			dsp_dims[0] * dsp_dims[1] * dsp_dims[2] * dtype_size);

	for (i = 0; i < dsp_dims[0]; i++)
		for (j = 0; j < dsp_dims[1]; j++)
			data_use[i][j] = data_use[0][0] + i * dsp_dims[1] * dsp_dims[2] * dtype_size
					+ j * dsp_dims[2] * dtype_size;

	H5Dread(dset, dtype_hdf5, H5S_ALL, H5S_ALL, H5P_DEFAULT, data_use[0][0]);

	*dim1 = dsp_dims[0];
	*dim2 = dsp_dims[1];
	*dim3 = dsp_dims[2];
	*mem = data_use;

	H5Dclose(dset);
	H5Sclose(filespace);
}

/*************************************************************************************************
 * FUNCTIONS - WRITE FIXED-SIZE DATASETS FROM STRUCTS
 *************************************************************************************************/

/*
 * This function is a wrapper for the more general masked function.
 */
void hdf5WriteStructMember(hid_t loc_id, const char *name, int dtype, int n_tot, int struct_size,
		int struct_offset, void *mem)
{
	hdf5WriteStructMemberMask(loc_id, name, dtype, n_tot, struct_size, struct_offset, mem, NULL, 0);
}

/*
 * This function is a wrapper for the more general masked 2D function.
 */
void hdf5WriteStructMember2D(hid_t loc_id, const char *name, int dtype, int n_tot, int n2,
		int struct_size, int struct_offset, void *mem)
{
	hdf5WriteStructMember2DMask(loc_id, name, dtype, n_tot, n2, struct_size, struct_offset, mem,
	NULL, 0);
}

/*
 * The n_tot is the total length of the array of structs, whereas n_write is a smaller number that
 * MUST correspond to the number of non-zero elements in mask.
 */
void hdf5WriteStructMemberMask(hid_t loc_id, const char *name, int dtype, int n_tot,
		int struct_size, int struct_offset, void *mem, int_least8_t *mask, int n_write)
{
	int i, j;
	char *p, *buffer;
	hid_t dset, dsp, dtype_hdf5;
	size_t s, ms_buffer;

	if (mask == NULL)
		n_write = n_tot;

	dsp = hdf5Dataspace1DFixed(n_write);
	dset = hdf5DatasetFixed(loc_id, dsp, name, dtype);
	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);

	s = datatypeSize(dtype);
	ms_buffer = s * n_write;

	p = (char*) mem;
	buffer = (char*) memAlloc(__FFL__, MID_HDF5BUFFER, ms_buffer);

	j = 0;
	for (i = 0; i < n_tot; i++)
	{
		if ((mask == NULL) || (mask[i]))
		{
			memcpy(&(buffer[j * s]), (char* ) (p + struct_offset), s);
			j++;
		}
		p += struct_size;
	}

	H5Dwrite(dset, dtype_hdf5, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer);
	memFree(__FFL__, MID_HDF5BUFFER, buffer, ms_buffer);
	H5Dclose(dset);
	H5Sclose(dsp);
}

/*
 * The n_tot is the total length of the array of structs, whereas n_write is a smaller number that
 * MUST correspond to the number of non-zero elements in mask.
 */
void hdf5WriteStructMember2DMask(hid_t loc_id, const char *name, int dtype, int n_tot, int n2,
		int struct_size, int struct_offset, void *mem, int_least8_t *mask, int n_write)
{
	int i, j;
	size_t ms_buffer, s, s2;
	hid_t dsp, dset, dtype_hdf5;
	char *p, *buffer;

	if (mask == NULL)
		n_write = n_tot;

	dsp = hdf5Dataspace2DFixed(n_write, n2);
	dset = hdf5DatasetFixed(loc_id, dsp, name, dtype);
	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);

	s = datatypeSize(dtype);
	s2 = s * n2;
	ms_buffer = s2 * n_write;

	p = (char*) mem;
	buffer = (char*) memAlloc(__FFL__, MID_HDF5BUFFER, ms_buffer);

	j = 0;
	for (i = 0; i < n_tot; i++)
	{
		if ((mask == NULL) || (mask[i]))
		{
			memcpy(&(buffer[j * s2]), (char* ) (p + struct_offset), s2);
			j++;
		}
		p += struct_size;
	}

	H5Dwrite(dset, dtype_hdf5, H5S_ALL, H5S_ALL, H5P_DEFAULT, buffer);
	memFree(__FFL__, MID_HDF5BUFFER, buffer, ms_buffer);
	H5Dclose(dset);
	H5Sclose(dsp);
}

/*************************************************************************************************
 * FUNCTIONS - WRITE VARIABLE-SIZE DATASETS FROM STRUCTS INCREMENTALLY
 *************************************************************************************************/

/*
 * This function writes a particular field from an array of structs into a contiguous field in an
 * HDF5 location. This location needs to be a dataset, in contrast to the non-incremental function
 * above where it can be a group.
 */
void hdf5WriteStructMemberInc(hid_t loc_id, long int file_offset, void *mem, int count,
		int struct_size, int struct_offset, int dtype)
{
	int i, ndims;
	size_t s, ms_buffer;
	hid_t filespace, memspace, dtype_hdf5;
	char *p, *buffer;
	hsize_t dim_offset[1] =
		{file_offset};
	hsize_t dim_count[1] =
		{count};
	hsize_t dsp_dims[1], dsp_max_dims[1];

	/*
	 * Check the properties of the data set we're writing to.
	 */
	filespace = H5Dget_space(loc_id);
	ndims = H5Sget_simple_extent_ndims(filespace);
	if (ndims != 1)
		error(__FFL__, "Trying to write 1D data to data space with dimensionality %d.\n", ndims);
	H5Sget_simple_extent_dims(filespace, dsp_dims, dsp_max_dims);
	if (file_offset + count > dsp_dims[0])
		error(__FFL__,
				"Trying to write data elements %ld to %ld, but dataspace has size %ld, max %ld.\n",
				file_offset, file_offset + count, dsp_dims[0], dsp_max_dims[0]);

	/*
	 * Create the correct memspace etc
	 */
	memspace = H5Screate_simple(1, dim_count, NULL);
	H5Sselect_hyperslab(filespace, H5S_SELECT_SET, dim_offset, NULL, dim_count, NULL);
	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);

	/*
	 * Create the buffer
	 */
	s = datatypeSize(dtype);
	ms_buffer = s * count;
	buffer = (char*) memAlloc(__FFL__, MID_HDF5BUFFER, ms_buffer);
	p = (char*) mem;

	for (i = 0; i < count; i++)
	{
		memcpy(&(buffer[i * s]), (char* ) (p + struct_offset), s);
		p += struct_size;
	}

	H5Dwrite(loc_id, dtype_hdf5, memspace, filespace, H5P_DEFAULT, buffer);
	memFree(__FFL__, MID_HDF5BUFFER, buffer, ms_buffer);
	H5Sclose(memspace);
	H5Sclose(filespace);
}

void hdf5WriteStructMemberInc2D(hid_t loc_id, long int file_offset, void *mem, int count,
		int count2, int struct_size, int struct_offset, int dtype)
{
	int i, ndims;
	size_t s, s2, ms_buffer;
	hid_t filespace, memspace, dtype_hdf5;
	char *p, *buffer;
	hsize_t dim_offset[2] =
		{file_offset, 0};
	hsize_t dim_count[2] =
		{count, count2};
	hsize_t dsp_dims[2], dsp_max_dims[2];

	/*
	 * Check the properties of the data set we're writing to.
	 */
	filespace = H5Dget_space(loc_id);
	ndims = H5Sget_simple_extent_ndims(filespace);
	if (ndims != 2)
		error(__FFL__, "Trying to write 2D data to data space with dimensionality %d.\n", ndims);
	H5Sget_simple_extent_dims(filespace, dsp_dims, dsp_max_dims);
	if (file_offset + count > dsp_dims[0])
		error(__FFL__,
				"Trying to write data elements %ld to %ld, but dataspace has size %ld, max %ld.\n",
				file_offset, file_offset + count, dsp_dims[0], dsp_max_dims[0]);

	/*
	 * The following commands are the same for all data types
	 */
	memspace = H5Screate_simple(2, dim_count, NULL);
	H5Sselect_hyperslab(filespace, H5S_SELECT_SET, dim_offset, NULL, dim_count, NULL);
	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);

	/*
	 * Create the buffer
	 */
	s = datatypeSize(dtype);
	s2 = s * count2;
	ms_buffer = s2 * count;
	buffer = (char*) memAlloc(__FFL__, MID_HDF5BUFFER, ms_buffer);
	p = (char*) mem;

	for (i = 0; i < count; i++)
	{
		memcpy(&(buffer[i * s2]), (char* ) (p + struct_offset), s2);
		p += struct_size;
	}

	H5Dwrite(loc_id, dtype_hdf5, memspace, filespace, H5P_DEFAULT, buffer);
	memFree(__FFL__, MID_HDF5BUFFER, buffer, ms_buffer);
	H5Sclose(memspace);
	H5Sclose(filespace);
}

/*
 * The assumption is that we are appending an array in the first dimension, with the other
 * dimensions being constant in the sense that they are not offset from zero. The size of all
 * dimensions is passed in the dim_count array, whereas the file_offset refers to the first
 * dimension only.
 */
void hdf5WriteStructMemberIncND(int n_dims, hid_t loc_id, long int file_offset, void *mem,
		hsize_t *dim_count, int struct_size, int struct_offset, int dtype)
{
	int i, ndims, high_d_count;
	size_t s, s_ndim, ms_buffer;
	hid_t filespace, memspace, dtype_hdf5;
	char *p, *buffer;
	hsize_t dim_offset[n_dims];
	hsize_t dsp_dims[n_dims], dsp_max_dims[n_dims];

	/*
	 * Set offset array and count total of higher dimensions
	 */
	dim_offset[0] = file_offset;
	high_d_count = 1;
	for (i = 1; i < n_dims; i++)
	{
		high_d_count *= dim_count[i];
		dim_offset[i] = 0;
	}

	/*
	 * Check the properties of the data set we're writing to.
	 */
	filespace = H5Dget_space(loc_id);
	ndims = H5Sget_simple_extent_ndims(filespace);
	if (ndims != n_dims)
		error(__FFL__, "Trying to write %dD data to data space with dimensionality %d.\n", n_dims,
				ndims);
	H5Sget_simple_extent_dims(filespace, dsp_dims, dsp_max_dims);
	if (file_offset + dim_count[0] > dsp_dims[0])
		error(__FFL__,
				"Trying to write data elements %ld to %ld, but dataspace has size %ld, max %ld.\n",
				file_offset, file_offset + dim_count[0], dsp_dims[0], dsp_max_dims[0]);

	/*
	 * The following commands are the same for all data types
	 */
	memspace = H5Screate_simple(n_dims, dim_count, NULL);
	H5Sselect_hyperslab(filespace, H5S_SELECT_SET, dim_offset, NULL, dim_count, NULL);
	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);

	/*
	 * Create the buffer
	 */
	s = datatypeSize(dtype);
	s_ndim = s * high_d_count;
	ms_buffer = s_ndim * dim_count[0];
	buffer = (char*) memAlloc(__FFL__, MID_HDF5BUFFER, ms_buffer);
	p = (char*) mem;

	for (i = 0; i < dim_count[0]; i++)
	{
		memcpy(&(buffer[i * s_ndim]), (char* ) (p + struct_offset), s_ndim);
		p += struct_size;
	}

	H5Dwrite(loc_id, dtype_hdf5, memspace, filespace, H5P_DEFAULT, buffer);
	memFree(__FFL__, MID_HDF5BUFFER, buffer, ms_buffer);
	H5Sclose(memspace);
	H5Sclose(filespace);
}

/*************************************************************************************************
 * FUNCTIONS - NDARRAYS
 *************************************************************************************************/

void hdf5ReadNDArray(hid_t loc_id, const char *name, NDArray *nda, int mem_id)
{
	hid_t dset, filespace, dtype_hdf5, dtype_native;
	hsize_t dsp_dims[2], dsp_max_dims[2];

	if (nda->allocated)
		error(__FFL__, "Trying to read HDF5 dataset into NDArray that is already allocated.\n");

	dset = H5Dopen(loc_id, name, H5P_DEFAULT);
	hdf5CheckForErrorOpen(__FFL__, loc_id, dset, name, H5I_DATASET);
	filespace = H5Dget_space(dset);
	nda->n_dim = H5Sget_simple_extent_dims(filespace, dsp_dims, dsp_max_dims);

	if (nda->n_dim > 2)
		error(__FFL__, "Found HDF5 dataset with dimensionality %d, cannot read into NDArray.\n",
				nda->n_dim);

	dtype_hdf5 = H5Dget_type(dset);
	dtype_native = H5Tget_native_type(dtype_hdf5, H5T_DIR_ASCEND);
	nda->dtype = hdf5DatatypeToSparta(dtype_native);

	nda->n[0] = dsp_dims[0];

	if (nda->n_dim == 1)
	{
		allocateNDArray(__FFL__, mem_id, nda);
		H5Dread(dset, dtype_hdf5, H5S_ALL, H5S_ALL, H5P_DEFAULT, nda->q_void);
	} else if (nda->n_dim == 2)
	{
		int n1, n2;

		nda->n[1] = dsp_dims[1];

		hdf5ReadDataset2D(loc_id, name, nda->dtype, &n1, &n2, (void***) &nda->q_void_2, mem_id);
		if (n1 != nda->n[0])
			error(__FFL__, "Internal error: n1 != n1\n");
		if (n2 != nda->n[1])
			error(__FFL__, "Internal error: n2 != n2\n");

		/*
		 * To fulfill the speification of the NDArray, we set the allocated memory pointer to
		 * where the memory actually is.
		 */
		nda->q_bytes = nda->q_void_2[0];
		nda->allocated = 1;

	} else
	{
		error(__FFL__, "Cannot read NDArray of dimensionality %d from HDF5 file.\n", nda->n_dim);
	}

	H5Tclose(dtype_hdf5);
	H5Tclose(dtype_native);
	H5Dclose(dset);
	H5Sclose(filespace);
}

void hdf5WriteNDArray(hid_t loc_id, const char *name, NDArray *nda)
{
	if (nda->n_dim == 1)
	{
		hdf5WriteDataset(loc_id, name, nda->dtype, nda->n[0], nda->q_void);
	} else if (nda->n_dim == 2)
	{
		hdf5WriteDataset2D(loc_id, name, nda->dtype, nda->n[0], nda->n[1], nda->q_void);
	} else if (nda->n_dim == 3)
	{
		hdf5WriteDataset3D(loc_id, name, nda->dtype, nda->n[0], nda->n[1], nda->n[2], nda->q_void);
	} else
	{
		error(__FFL__, "Cannot write NDArray of dimensionality %d to HDF5 file.\n", nda->n_dim);
	}
}

void hdf5WriteNDArrayMask(hid_t loc_id, const char *name, NDArray *nda, int_least8_t *mask,
		int n_write)
{
	if (nda->n_dim == 1)
	{
		hdf5WriteDatasetMask(loc_id, name, nda->dtype, nda->n[0], nda->q_void, mask, n_write);
	} else
	{
		error(__FFL__, "Cannot write masked NDArray of dimensionality %d to HDF5 file.\n",
				nda->n_dim);
	}
}
