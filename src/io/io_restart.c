/*************************************************************************************************
 *
 * This unit writes and reads restart files, i.e., memory dumps.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

#include "io_restart.h"
#include "../config.h"
#include "../memory.h"
#include "../geometry.h"
#include "../halos/halo.h"
#include "../halos/halo_particles.h"
#include "../lb/domain.h"
#include "../lb/domain_sfc.h"
#include "../lb/domain_slabs.h"

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void restartFilePath(char *str);
void restartFileName(char *str, int snap_idx, int proc_id);
void restartFileNameHdf5(char *str, int snap_idx);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void restartFilePath(char *str)
{
	if (strlen(config.output_path) == 0)
		sprintf(str, "%s", "restart");
	else
		sprintf(str, "%s/restart", config.output_path);
}

void restartFileName(char *str, int snap_idx, int proc_id)
{
	char path[200];

	restartFilePath(path);
	sprintf(str, "%s/restart_%04d_%04d.bin", path, snap_idx, proc_id);
}

void restartFileNameHdf5(char *str, int snap_idx)
{
	char path[200];

	restartFilePath(path);
	sprintf(str, "%s/restart_%04d.hdf5", path, snap_idx);
}

int getRestartFileNumber(char *filename)
{
	int idx;

	idx = -1;
	sscanf(filename, "restart_%d", &idx);

	return idx;
}

void readWriteOperation(FILE *f, int rw, void *mem, size_t n_bytes)
{
	if (rw == 0)
		fread(mem, sizeof(char), n_bytes, f);
	else
		fwrite(mem, sizeof(char), n_bytes, f);
}

void readWriteOperationAllocate(FILE *f, int rw, void **mem, size_t n_bytes, int mem_id)
{
	if (rw == 0)
		(*mem) = (void*) memAlloc(__FFL__, mem_id, n_bytes);
	readWriteOperation(f, rw, *mem, n_bytes);
}

void readWriteOperationAllocate2D(FILE *f, int rw, void ***mem, size_t size, int n1, int n2,
		int mem_id)
{
	if (rw == 0)
	{
		int i;

		(*mem) = (void**) memAlloc(__FFL__, mem_id, sizeof(void*) * n1);
		(*mem)[0] = (void*) memAlloc(__FFL__, mem_id, size * n1 * n2);
		for (i = 1; i < n1; i++)
			(*mem)[i] = (*mem)[0] + i * size * n2;
	}

	readWriteOperation(f, rw, (*mem)[0], size * n1 * n2);
}

void readWriteOperationAllocate3D(FILE *f, int rw, void ****mem, size_t size, int n1, int n2,
		int n3, int mem_id)
{
	if (rw == 0)
	{
		int i, j;

		(*mem) = (void***) memAlloc(__FFL__, mem_id, sizeof(void**) * n1);
		for (i = 0; i < n1; i++)
			(*mem)[i] = (void**) memAlloc(__FFL__, mem_id, sizeof(void**) * n2);
		(*mem)[0][0] = (void*) memAlloc(__FFL__, mem_id, size * n1 * n2 * n3);
		for (i = 0; i < n1; i++)
			for (j = 0; j < n2; j++)
				(*mem)[i][j] = (*mem)[0][0] + i * size * n2 * n3 + j * size * n3;
	}

	readWriteOperation(f, rw, (*mem)[0][0], size * n1 * n2 * n3);
}

void readWriteDynArray(FILE *f, int rw, DynArray *da)
{
	readWriteOperation(f, rw, da, sizeof(DynArray));
	if (rw == 0)
		resetDynArray(__FFL__, da);
	readWriteOperation(f, rw, da->data, da->size * da->n_alloc);
}

void readWriteNDArray(FILE *f, int rw, NDArray *nda, int mem_id)
{
	size_t s;

	if (rw == 0)
	{
		nda->allocated = 0;
		allocateNDArray(__FFL__, mem_id, nda);
	}
	s = sizeOfNDArray(nda);
	readWriteOperation(f, rw, nda->q_void, s);
}

void readOrWriteRestartFiles(int rw, int *snap_idx, GlobalStatistics *gs, OutputFile *outputFiles,
		LocalStatistics *ls, LocalStatistics *ls_lastsnap, DynArray *halos)
{
	int i, al, tt, rs, reading, writing, file_idx, n_files_tot, found, status;
	double current_time, time_tmp;
	FILE *f;
	char fn1[400], fn2[400], *fn_from, *fn_to, tmpstr[400];
	struct dirent **files;
	struct stat file_info;
	MemoryStatistics mem_stats_tmp;
	pid_t pid = -1;

	// Shortcuts
	reading = (rw == 0);
	writing = (rw == 1);

	/*
	 * If reading, we need to find the last valid set of restart files. We do this on the main proc
	 * to avoid all procs browsing the directory, and then broadcast the correct index.
	 */
	if (reading)
	{
		if (is_main_proc)
		{
			// Load list of files in restart path
			restartFilePath(fn1);
			n_files_tot = scandir(fn1, &files, 0, alphasort);
			if (n_files_tot < 0)
				error(__FFL__, "[Main] Could not open restart directory '%s'.\n", fn1);
			if (n_files_tot < n_proc)
				error(__FFL__,
						"[Main] Did not find sufficient number restart files in restart directory (at least %d, found %d).\n",
						n_proc, n_files_tot);

			// Find highest snapshot index; due to alphabetical sorting, that should be the last file
			file_idx = getRestartFileNumber(files[n_files_tot - 1]->d_name);

			// Go through indices, look for full set
			found = 0;
			while (file_idx > 0)
			{
				found = 1;
				for (i = 0; i < n_proc; i++)
				{
					restartFileName(fn1, file_idx, i);
					if (stat(fn1, &file_info) != 0)
					{
						found = 0;
						break;
					}
				}
				if (found)
					break;
				file_idx--;
			}
			if (!found)
				error(__FFL__, "Could not find a full set of restart files for all %d processes.\n",
						n_proc);
			output(0, "[Main] Restarting from snapshot %d.\n", file_idx);
		}
		MPI_Bcast(&file_idx, 1, MPI_INT, MAIN_PROC, MPI_COMM_WORLD);
	} else
	{
		file_idx = *snap_idx;
	}

	/*
	 * One potentially time-consuming task is to copy the HDF5 output file to and from the restart
	 * directory.
	 */
	if (is_main_proc)
	{
		outputFileName(fn1);
		restartFileNameHdf5(fn2, file_idx);

		if (reading)
		{
			remove(fn1);
			fn_from = fn2;
			fn_to = fn1;
			sprintf(tmpstr, "cp %s %s", fn2, fn1);
		} else
		{
			fn_from = fn1;
			fn_to = fn2;
			sprintf(tmpstr, "cp %s %s", fn1, fn2);
		}

		pid = fork();

		if (pid < 0)
		{
			warningOrError(__FFL__, config.err_level_cannot_fork,
					"[Main] Could not fork process. This may slow down the execution of system functions.\n");
			execl("/bin/cp", "-ip", fn_from, fn_to, (char*) NULL);
		} else if (pid == 0)
		{
			output(3, "[%4d] Copying file %s to %s\n", proc, fn_from, fn_to);
			execl("/bin/cp", "-ip", fn_from, fn_to, (char*) NULL);
			exit(0);
		}
	}

	/*
	 * If reading, we need to be careful with the memory statistics as they will be a combination
	 * of the current and stored fields. We back up the current field into a temporary variable.
	 *
	 * If writing, we initialize the mem_stats_tmp field to avoid compiler warnings.
	 */
	if (reading)
		memcpy(&mem_stats_tmp, &mem_stats, sizeof(MemoryStatistics));
	else
		memset(&mem_stats_tmp, 0, sizeof(MemoryStatistics));

	/*
	 * Open file
	 */
	restartFileName(fn1, file_idx, proc);
	if (reading)
	{
		output(3, "[%4d] Reading restart file %s\n", proc, fn1);
		f = fopen(fn1, "rb");
	} else
	{
		f = fopen(fn1, "wb");
	}
	if (f == NULL)
		error(__FFL__, "Could not open restart file %s.\n", fn1);

	/*
	 * Read/write 1: Main proc fields
	 *
	 * If writing, we need to fix the timing statistics before writing them to disk; otherwise,
	 * they contain random timing values. The logic is that during writing, the current values
	 * should be changed only temporarily, meaning the same time should be added and subtracted.
	 *
	 * While reading, we preseve the INIT time since that has been running and add it to the
	 * init time from the original run.
	 */
	current_time = MPI_Wtime();
	time_tmp = 0.0;
	if (is_main_proc)
	{
		if (writing)
			gs->timers_0_cum[T0_SNAPS] += current_time;
		if (reading)
			time_tmp = gs->timers_0_cum[T0_INIT];
		readWriteOperation(f, rw, gs, sizeof(GlobalStatistics));
		if (writing)
			gs->timers_0_cum[T0_SNAPS] -= current_time;
		if (reading)
			gs->timers_0_cum[T0_INIT] += time_tmp;

		readWriteOperation(f, rw, outputFiles, sizeof(OutputFile));
	}

	/*
	 * Read/write 2: Variables used in run() function
	 *
	 * Again, we need to fix timers.
	 */
	readWriteOperation(f, rw, snap_idx, sizeof(int));
	readWriteOperation(f, rw, ls_lastsnap, sizeof(LocalStatistics));
	if (writing)
		ls->timers_1[T1_RESTARTFILES] += current_time;
	readWriteOperation(f, rw, ls, sizeof(LocalStatistics));
	if (writing)
		ls->timers_1[T1_RESTARTFILES] -= current_time;

	/*
	 * Read/write 3: Global variables from global.h
	 *
	 * We are not reading the GSL variables because they need to be initialized rather than
	 * copied.
	 */
	readWriteOperation(f, rw, &config, sizeof(ConfigData));
	readWriteOperation(f, rw, &mem_stats, sizeof(MemoryStatistics));
#if DO_MASS_PROFILE
	readWriteOperation(f, rw, &mass_profile_vars, sizeof(MassProfileVars));
#endif

	/*
	 * Deal with memory statistics before allocating global memory
	 */
	if (reading)
	{
		mem_stats.cur_tot = mem_stats_tmp.cur_tot;
		for (i = 0; i < MID_N; i++)
			mem_stats.current[i] = mem_stats_tmp.current[i];
	}

	/*
	 * This is the time to allocate the global memory. We will need it for some of the following
	 * fields, but we also needed to first load the config.
	 */
	if (reading)
		allocateGlobalMemory();

	/*
	 * Read/write 4: Global variables from other units
	 */
	// Global variables from geometry.h
#if DO_READ_PARTICLES
	readWriteOperation(f, rw, snap_boxes, sizeof(Box) * config.n_snaps * config.n_files_per_snap);
#endif

	// Global variables from lb/domain.h
	readWriteOperation(f, rw, &proc_domain_box, sizeof(Box));

	// Global variables from lb/domain_sfc.h
#if DOMAIN_DECOMP_SFC
	readWriteOperation(f, rw, &sfcDomain, sizeof(SFCDomain));

#if DOMAIN_SFC_INDEX_TABLE
	readWriteOperation(f, rw, sfcIndexLookup.idx, sizeof(int) * SFC_MAX);
	readWriteOperation(f, rw, sfcIndexLookup.pos, sizeof(int) * SFC_MAX * 3);
#endif
#endif

	// Global variables from lb/domain_slabs.h
#if DOMAIN_DECOMP_SLABS
	readWriteOperation(f, rw, &slabDomain, sizeof(SlabDomain));
	readWriteOperation(f, rw, slab_domain_coor, sizeof(int) * 3);
#endif

	/*
	 * Read/write 5: Variable-sized variables (DynArrays)
	 */
	readWriteDynArray(f, rw, halos);
	for (i = 0; i < halos->n; i++)
	{
		readWriteDynArray(f, rw, &(halos->h[i].subs));
		for (tt = 0; tt < NTT; tt++)
		{
			readWriteDynArray(f, rw, &(halos->h[i].tt[tt].tcr));
			readWriteDynArray(f, rw, &(halos->h[i].tt[tt].iid));
			for (rs = 0; rs < NRS; rs++)
				readWriteDynArray(f, rw, &(halos->h[i].tt[tt].rs[rs]));
		}
		for (al = 0; al < NAL; al++)
			readWriteDynArray(f, rw, &(halos->h[i].al[al]));

		if (reading)
			debugHalo(&(halos->h[i]), "Found in restart files.");
	}

	/*
	 * Done with file reading / writing. At this point, the main process needs to check on the
	 * child process that is copying the HDF5 output file to/from the restart folder.
	 */
	fclose(f);

	if (is_main_proc)
	{
		if (pid > 0)
		{
			output(3, "[Main] Waiting for copy process...\n");
			waitpid(pid, &status, 0);
			if (status != 0)
			{
				if (reading)
					warningOrError(__FFL__, config.err_level_file_copy_failed,
							"Copying output file from restart folder failed, status code %d. Restart may fail as a consequence.\n",
							status);
				else
					warningOrError(__FFL__, config.err_level_file_copy_failed,
							"[Main] Copying output file to restart folder failed, status code %d.\n",
							status);
			}
		}
	}

	/*
	 * Wait for all procs to finish writing files. When we are sure that all procs have finished
	 * writing, delete old files.
	 */
	MPI_Barrier(MPI_COMM_WORLD);

	if (writing && is_main_proc)
	{
		restartFilePath(fn1);
		n_files_tot = scandir(fn1, &files, 0, alphasort);
		for (i = 0; i < n_files_tot; i++)
		{
			file_idx = getRestartFileNumber(files[i]->d_name);
			if ((file_idx >= 0) && (file_idx < *snap_idx))
			{
				sprintf(fn2, "%s/%s", fn1, files[i]->d_name);
				output(4, "[Main] Removing outdated restart file %s.\n", fn2);
				remove(fn2);
			}
		}
	}

	if (reading && is_main_proc)
		printLine(2);
}
