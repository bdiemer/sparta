/*************************************************************************************************
 *
 * This unit implements the reading of LGadget snapshot files.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <hdf5.h>

#include "io_snaps_gadget3.h"
#include "io_snaps.h"
#include "io_hdf5.h"
#include "../memory.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

SnapshotHeaderGadget3* readSnapshotHeaderGadget3Internal(char *filename)
{
	int i, tmp_int, *tmp_n1, *tmp_n2;
	unsigned long int offset;
	double *tmp_double;
	hid_t file_id, group_id;
	SnapshotHeaderGadget3 *header;

	header = (SnapshotHeaderGadget3*) memAlloc(__FFL__, MID_SNAPREADBUF,
			sizeof(SnapshotHeaderGadget3));

	file_id = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
	group_id = H5Gopen(file_id, "/Header", H5P_DEFAULT);

	/*
	 * Number of particles in this file
	 */
	hdf5ReadAttributeInt1D(group_id, "NumPart_ThisFile", &tmp_int, &tmp_n1, MID_SNAPREADBUF);
	if (tmp_int != 6)
		error(__FFL__, "Expected array of length 6 for NumPart_Total in header, found %d.\n",
				tmp_int);
	for (i = 0; i < 6; i++)
		header->npart[i] = tmp_n1[i];
	memFree(__FFL__, MID_SNAPREADBUF, tmp_n1, sizeof(int) * 6);

	/*
	 * Total number of particles
	 */
	hdf5ReadAttributeInt1D(group_id, "NumPart_Total", &tmp_int, &tmp_n1, MID_SNAPREADBUF);
	if (tmp_int != 6)
		error(__FFL__, "Expected array of length 6 for NumPart_Total in header, found %d.\n",
				tmp_int);
	hdf5ReadAttributeInt1D(group_id, "NumPart_Total_HighWord", &tmp_int, &tmp_n2, MID_SNAPREADBUF);
	if (tmp_int != 6)
		error(__FFL__, "Expected array of length 6 for NumPart_Total in header, found %d.\n",
				tmp_int);
	offset = (1 << 31);
	for (i = 0; i < 6; i++)
		header->npartTotal[i] = tmp_n1[i] + offset * tmp_n2[i];
	memFree(__FFL__, MID_SNAPREADBUF, tmp_n1, sizeof(int) * 6);
	memFree(__FFL__, MID_SNAPREADBUF, tmp_n2, sizeof(int) * 6);

	/*
	 * Particle masses
	 */
	hdf5ReadAttributeDouble1D(group_id, "MassTable", &tmp_int, &tmp_double, MID_SNAPREADBUF);
	if (tmp_int != 6)
		error(__FFL__, "Expected array of length 6 for NumPart_Total in header, found %d.\n",
				tmp_int);
	for (i = 0; i < 6; i++)
		header->mass[i] = tmp_double[i];
	memFree(__FFL__, MID_SNAPREADBUF, tmp_double, sizeof(double) * 6);

	/*
	 * Other parameters
	 */
	hdf5ReadAttributeInt(group_id, "NumFilesPerSnapshot", &(header->num_files));

	hdf5ReadAttributeDouble(group_id, "BoxSize", &(header->BoxSize));
	hdf5ReadAttributeDouble(group_id, "Time", &(header->time));
	hdf5ReadAttributeDouble(group_id, "Redshift", &(header->redshift));
	hdf5ReadAttributeDouble(group_id, "Omega0", &(header->Omega0));
	hdf5ReadAttributeDouble(group_id, "OmegaLambda", &(header->OmegaLambda));
	hdf5ReadAttributeDouble(group_id, "HubbleParam", &(header->HubbleParam));

	hdf5ReadAttributeInt(group_id, "Flag_Sfr", &(header->flag_sfr));
	hdf5ReadAttributeInt(group_id, "Flag_Feedback", &(header->flag_feedback));
	hdf5ReadAttributeInt(group_id, "Flag_StellarAge", &(header->flag_stellarage));
	hdf5ReadAttributeInt(group_id, "Flag_Metals", &(header->flag_metals));
	hdf5ReadAttributeInt(group_id, "Flag_Cooling", &(header->flag_cooling));

	H5Gclose(group_id);
	H5Fclose(file_id);

	return header;
}

SnapshotHeader readSnapshotHeaderGadget3(char *filename)
{
	SnapshotHeaderGadget3 *header_gadget3;
	SnapshotHeader header;

	header_gadget3 = readSnapshotHeaderGadget3Internal(filename);

	header.a = header_gadget3->time;
	header.n_particles = header_gadget3->npartTotal[1];
	header.box_size = header_gadget3->BoxSize / 1000.0;
	header.particle_mass = header_gadget3->mass[1] * 1E10;
	header.Omega_m = header_gadget3->Omega0;
	header.Omega_L = header_gadget3->OmegaLambda;
	header.h = header_gadget3->HubbleParam;
	header.n_files_per_snap = header_gadget3->num_files;

	memFree(__FFL__, MID_SNAPREADBUF, header_gadget3, sizeof(SnapshotHeaderGadget3));

	return header;
}

void printGadget3Header(SnapshotHeaderGadget3 *header)
{
	int i;

	for (i = 0; i < 6; i++)
	{
		output(0, "Particle species %d, N %10d, mass %.3e\n", i, header->npart[i], header->mass[i]);
	}

	output(0, "Scale factor a   %.4f\n", header->time);
	output(0, "Redshift z       %.4f\n", header->redshift);
	output(0, "Num files        %d\n", header->num_files);
	output(0, "Box size         %.2f\n", header->BoxSize);
	output(0, "Omega0           %.2f\n", header->Omega0);
	output(0, "OmegaLambda      %.2f\n", header->OmegaLambda);
	output(0, "HubbleParam      %.2f\n", header->HubbleParam);
}

void readParticlesGadget3(char *filename, Particle **particles, int *n_particles, int check_corrupt)
{
	int i, j, ret, n_part, n_part_tmp, n_dim_tmp, *buffer_int;
	double **buffer_double, a, root_a;
	hid_t file_id, group_id;

	/*
	 * Open file, find out redshift from header
	 */
	file_id = H5Fopen(filename, H5F_ACC_RDONLY, H5P_DEFAULT);
	group_id = H5Gopen(file_id, "/Header", H5P_DEFAULT);
	hdf5ReadAttributeDouble(group_id, "Time", &a);
	root_a = sqrt(a);
	H5Gclose(group_id);

	/*
	 * Read ID data set, find out how many particles there are
	 */
	group_id = H5Gopen(file_id, "/PartType1", H5P_DEFAULT);
	hdf5ReadDataset(group_id, "ParticleIDs", DTYPE_INT32, &n_part_tmp, (void**) &buffer_int,
			MID_SNAPREADBUF);
	n_part = n_part_tmp;
	*particles = (Particle*) memAlloc(__FFL__, MID_PARTICLES, sizeof(Particle) * n_part);
	for (i = 0; i < n_part; i++)
		(*particles)[i].id = buffer_int[i];
	memFree(__FFL__, MID_SNAPREADBUF, buffer_int, sizeof(int) * n_part);

	if (check_corrupt)
	{
		for (i = 0; i < n_part; ++i)
		{
			ret = checkParticleID((*particles)[i].id);
			if (ret != PART_OK)
				error(__FFL__,
						"Detected file corruption: invalid ID (status %d) in particle %d: %ld, file %s.\n",
						ret, i, (*particles)[i].id, filename);
		}
	}

	/*
	 * Read coordinates
	 */
	hdf5ReadDataset2D(group_id, "Coordinates", DTYPE_DOUBLE, &n_part_tmp, &n_dim_tmp,
			(void***) &buffer_double, MID_SNAPREADBUF);
	if (n_dim_tmp != 3)
		error(__FFL__, "Expected 3 dimensions, found %d.\n", n_dim_tmp);
	if (n_part_tmp != n_part)
		error(__FFL__, "Expected %d particle coordinates, found %d.\n", n_part, n_part_tmp);
	for (i = 0; i < n_part_tmp; i++)
	{
		for (j = 0; j < 3; j++)
			(*particles)[i].x[j] = (float) (buffer_double[i][j] * 0.001);
	}
	memFree(__FFL__, MID_SNAPREADBUF, buffer_double[0], sizeof(double) * n_part * 3);
	memFree(__FFL__, MID_SNAPREADBUF, buffer_double, sizeof(double*) * n_part);

	if (check_corrupt)
	{
		for (i = 0; i < n_part; ++i)
		{
			ret = checkParticlePosition((*particles)[i].x);
			if (ret != PART_OK)
				error(__FFL__,
						"Detected file corruption: invalid position (status %d) in particle %d: %.4e %.4e %.4e, file %s.\n",
						ret, i, (*particles)[i].x[0], (*particles)[i].x[1], (*particles)[i].x[2],
						filename);
		}
	}

	/*
	 * Read coordinates
	 */
	hdf5ReadDataset2D(group_id, "Velocities", DTYPE_DOUBLE, &n_part_tmp, &n_dim_tmp,
			(void***) &buffer_double, MID_SNAPREADBUF);
	if (n_dim_tmp != 3)
		error(__FFL__, "Expected 3 dimensions, found %d.\n", n_dim_tmp);
	if (n_part_tmp != n_part)
		error(__FFL__, "Expected %d particle velocities, found %d.\n", n_part, n_part_tmp);
	for (i = 0; i < n_part_tmp; i++)
	{
		for (j = 0; j < 3; j++)
			(*particles)[i].v[j] = (float) (buffer_double[i][j] * root_a);
	}
	memFree(__FFL__, MID_SNAPREADBUF, buffer_double[0], sizeof(double) * n_part * 3);
	memFree(__FFL__, MID_SNAPREADBUF, buffer_double, sizeof(double*) * n_part);

	if (check_corrupt)
	{
		for (i = 0; i < n_part; ++i)
		{
			ret = checkParticleVelocity((*particles)[i].v);
			if (ret != PART_OK)
				error(__FFL__,
						"Detected file corruption: invalid velocity (status %d) in particle %d: %.4e %.4e %.4e, file %s.\n",
						ret, i, (*particles)[i].v[0], (*particles)[i].v[1], (*particles)[i].v[2],
						filename);
		}
	}

	/*
	 * Finalize
	 */
	H5Gclose(group_id);
	H5Fclose(file_id);
	*n_particles = n_part_tmp;
}
