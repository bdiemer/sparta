/*************************************************************************************************
 *
 * This unit contains functions related to Rockstar halo catalogs.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _IO_HALOCAT_ROCKSTAR_H_
#define _IO_HALOCAT_ROCKSTAR_H_

#include "../global_types.h"
#include "../config.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * There are functions in this interface that convert between masses and radii. These status fields
 * capture these decisions.
 */
enum
{
	ROCKSTAR_CONVERT_STATUS_NONE, ROCKSTAR_CONVERT_STATUS_M_TO_R
};

#define CATALOG_FIELDS_ROCKSTAR \
ENUM(CAT_ID, id, id)\
ENUM(CAT_DESC_ID, desc_id, desc_id)\
ENUM(CAT_PID, pid, pid)\
ENUM(CAT_DESC_PID, desc_pid, desc_pid)\
ENUM(CAT_NUM_PROG, num_prog, num_prog)\
ENUM(CAT_MMP, mmp, mmp?)\
ENUM(CAT_PHANTOM, phantom, phantom)\
ENUM(CAT_X, x, x)\
ENUM(CAT_Y, y, y)\
ENUM(CAT_Z, z, z)\
ENUM(CAT_VX, vx, vx)\
ENUM(CAT_VY, vy, vy)\
ENUM(CAT_VZ, vz, vz)\
ENUM(CAT_M200M_ALL, m200m_all, M200b_all)\
ENUM(CAT_RBOUND, rbound, R200b)\
ENUM(CAT_MBOUND, mbound, M200b)

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Functions that fulfill the general halo catalog interface
 */
int getNCatFieldsRockstar();
char** getCatFieldNamesRockstar();
char** getCatFieldDefaultsRockstar();
void readHaloCatalogRockstar(int snap_idx, HaloRequest *requests, int n_requests, DynArray *da_cd);
void matchCatalogsToSnapshotsRockstar(int n_snaps_tot, int *snapshot_id, float *snapshot_a);

/*
 * Functions specific to Rockstar
 */
void getCatalogFileNameRockstar(float a, char *str);
void parseCatalogHeaderRockstar(char *line, char cat_fields[][CAT_FIELD_LENGTH], int n_cat_fields,
		char *parse_str, int **field_order, int **field_idxs);
int haloDefinitionToRockstar(HaloDefinition def, HaloDefinition main_def, int is_strict_so,
		int is_bound_props, char *str);

#endif
