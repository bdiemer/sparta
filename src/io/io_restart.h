/*************************************************************************************************
 *
 * This unit writes and reads restart files, i.e., memory dumps.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _IO_RESTART_H_
#define _IO_RESTART_H_

#include <hdf5.h>

#include "io_output.h"
#include "../statistics.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void readWriteOperation(FILE *f, int rw, void *mem, size_t n_bytes);
void readWriteOperationAllocate(FILE *f, int rw, void **mem, size_t n_bytes, int mem_id);
void readWriteOperationAllocate2D(FILE *f, int rw, void ***mem, size_t size, int n1, int n2,
		int mem_id);
void readWriteOperationAllocate3D(FILE *f, int rw, void ****mem, size_t size, int n1, int n2, int n3,
		int mem_id);
void readWriteDynArray(FILE *f, int rw, DynArray *da);
void readWriteNDArray(FILE *f, int rw, NDArray *nda, int mem_id);

void readOrWriteRestartFiles(int rw, int *snap_idx, GlobalStatistics *gs, OutputFile *outputFiles,
		LocalStatistics *ls, LocalStatistics *ls_lastsnap, DynArray *halos);

#endif
