/*************************************************************************************************
 *
 * This unit implements the reading of LGadget snapshot files.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "io_snaps_lgadget.h"
#include "io_snaps.h"
#include "../memory.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

SnapshotHeaderLGadget* readSnapshotHeaderLGadgetInternal(char *filename)
{
	FILE *f;
	SnapshotHeaderLGadget *header;
	int dummy;

	header = (SnapshotHeaderLGadget*) memAlloc(__FFL__, MID_SNAPREADBUF,
			sizeof(SnapshotHeaderLGadget));
	f = fopen(filename, "r");
	if (f == NULL)
		error(__FFL__, "Could not open file %s.\n", filename);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	fread(header, sizeof(SnapshotHeaderLGadget), (size_t) 1, f);
	fclose(f);

	return header;
}

SnapshotHeader readSnapshotHeaderLGadget(char *filename)
{
	SnapshotHeaderLGadget *header_lgadget;
	SnapshotHeader header;

	header_lgadget = readSnapshotHeaderLGadgetInternal(filename);
	header.a = header_lgadget->time;
	header.n_particles = header_lgadget->npartTotal[1];
	header.box_size = header_lgadget->BoxSize;
	header.particle_mass = header_lgadget->mass[1] * 1E10;
	header.Omega_m = header_lgadget->Omega0;
	header.Omega_L = header_lgadget->OmegaLambda;
	header.h = header_lgadget->HubbleParam;
	header.n_files_per_snap = header_lgadget->num_files;

	memFree(__FFL__, MID_SNAPREADBUF, header_lgadget, sizeof(SnapshotHeaderLGadget));

	return header;
}

void printLGadgetHeader(SnapshotHeaderLGadget *header)
{
	int i;

	for (i = 0; i < 6; i++)
	{
		output(0, "Particle species %d, N %10d, mass %.3e\n", i, header->npart[i], header->mass[i]);
	}

	output(0, "Scale factor a   %.4f\n", header->time);
	output(0, "Redshift z       %.4f\n", header->redshift);
	output(0, "Num files        %d\n", header->num_files);
	output(0, "Box size         %.2f\n", header->BoxSize);
	output(0, "Omega0           %.2f\n", header->Omega0);
	output(0, "OmegaLambda      %.2f\n", header->OmegaLambda);
	output(0, "HubbleParam      %.2f\n", header->HubbleParam);
}

void readParticlesLGadget(char *filename, Particle **particles, int *n_particles, int check_corrupt)
{
	FILE *f;
	SnapshotHeaderLGadget header;
	int i, dummy, *buffer_int, ret;
	long *buffer_long;
	float *buffer_float, root_a;

	f = fopen(filename, "r");
	if (f == NULL)
		error(__FFL__, "Could not open file %s.\n", filename);

	/*
	 * Read header, compute number of particles and velocity conversion factor.
	 */
	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	fread(&header, sizeof(SnapshotHeaderLGadget), (size_t) 1, f);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	root_a = sqrt(header.time);
	*n_particles = 0;
	for (i = 0; i < 5; i++)
		*n_particles += header.npart[i];

	/*
	 * Allocate memory and read the particle positions. When transcribing the positions,
	 * make sure to take the periodic bcs into account.
	 */
	*particles = (Particle*) memAlloc(__FFL__, MID_PARTICLES, sizeof(Particle) * (*n_particles));
	buffer_float = (float*) memAlloc(__FFL__, MID_SNAPREADBUF, sizeof(float) * (*n_particles) * 3);

	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	fread(buffer_float, (size_t) (*n_particles), 3 * sizeof(float), f);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	if (check_corrupt)
	{
		for (i = 0; i < (*n_particles); ++i)
		{
			ret = checkParticlePosition(&(buffer_float[i * 3 + 0]));
			if (ret != PART_OK)
				error(__FFL__,
						"Detected file corruption: invalid position (status %d) in particle %d: %.4e %.4e %.4e, file %s.\n",
						ret, i, buffer_float[i * 3 + 0], buffer_float[i * 3 + 1],
						buffer_float[i * 3 + 2], filename);
		}
	}

	for (i = 0; i < (*n_particles); ++i)
	{
		(*particles)[i].x[0] = buffer_float[i * 3 + 0];
		(*particles)[i].x[1] = buffer_float[i * 3 + 1];
		(*particles)[i].x[2] = buffer_float[i * 3 + 2];
	}

	/*
	 * Read the particle velocities.
	 */
	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	fread(buffer_float, (size_t) (*n_particles), 3 * sizeof(float), f);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	if (check_corrupt)
	{
		for (i = 0; i < (*n_particles); ++i)
		{
			ret = checkParticleVelocity(&(buffer_float[i * 3 + 0]));
			if (ret != PART_OK)
				error(__FFL__,
						"Detected file corruption: invalid velocity (status %d) in particle %d: %.4e %.4e %.4e, file %s.\n",
						ret, i, buffer_float[i * 3 + 0], buffer_float[i * 3 + 1],
						buffer_float[i * 3 + 2], filename);
		}
	}

	for (i = 0; i < (*n_particles); ++i)
	{
		(*particles)[i].v[0] = buffer_float[i * 3 + 0] * root_a;
		(*particles)[i].v[1] = buffer_float[i * 3 + 1] * root_a;
		(*particles)[i].v[2] = buffer_float[i * 3 + 2] * root_a;
	}

	memFree(__FFL__, MID_SNAPREADBUF, buffer_float, sizeof(float) * (*n_particles) * 3);

	/*
	 * Read the particle IDs. Depending on the file format, the IDs may be ints or longs.
	 */
	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	if (dummy == (*n_particles) * 4)
	{
		buffer_int = (int*) memAlloc(__FFL__, MID_SNAPREADBUF, sizeof(int) * (*n_particles));
		fread(buffer_int, (size_t) (*n_particles), sizeof(int), f);
		for (i = 0; i < (*n_particles); ++i)
			(*particles)[i].id = (HaloID) buffer_int[i];
		memFree(__FFL__, MID_SNAPREADBUF, buffer_int, sizeof(int) * (*n_particles));

	} else if (dummy == (*n_particles) * 8)
	{
		buffer_long = (long int*) memAlloc(__FFL__, MID_SNAPREADBUF,
				sizeof(long int) * (*n_particles));
		fread(buffer_long, (size_t) (*n_particles), sizeof(long int), f);
		for (i = 0; i < (*n_particles); ++i)
			(*particles)[i].id = buffer_long[i];
		memFree(__FFL__, MID_SNAPREADBUF, buffer_long, sizeof(long int) * (*n_particles));

	} else
	{
		error(__FFL__,
				"[%4d] Size of ID block in LGadget file %s corresponds neither to int nor long (%d, %d).\n",
				proc, filename, *n_particles, dummy);
	}

	if (check_corrupt)
	{
		for (i = 0; i < (*n_particles); ++i)
		{
			ret = checkParticleID((*particles)[i].id);
			if (ret != PART_OK)
				error(__FFL__,
						"Detected file corruption: invalid ID (status %d) in particle %d: %ld, file %s.\n",
						ret, i, (*particles)[i].id, filename);
		}
	}

	fclose(f);
}
