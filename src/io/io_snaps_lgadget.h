/*************************************************************************************************
 *
 * This unit implements the reading of LGadget snapshot files.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _IO_SNAPS_LGADGET_H_
#define _IO_SNAPS_LGADGET_H_

#include "../global_types.h"
#include "io_snaps.h"

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

/*
 * The fields in the Gadget header files have the following meanings:
 *
 * npart:             Number of particles in each type. npart[1] gives the number of dark matter
 *                    particles in the present file, other particle types are ignored here.
 * mass               The mass of each particle species; mass[1] is the dark matter mass.
 * time               Cosmological scale factor a
 * redshift           Reshift
 * flag_sfr           Whether star formation is used (not available in L-Gadget2)
 * flag_feedback      Whether feedback from star formation is included
 * npartTotal         npart[1] gives the total number of particles in the run. If this number exceeds 2^32, the npartTotal[2] stores
 *                    the result of a division of the particle number by 2^32, while npartTotal[1] holds the remainder.
 * flag_cooling       Whether radiative cooling is included
 * num_files          Number of files that are used for a snapshot
 * BoxSize            Simulation box size (in code units)
 * Omega0             Matter density
 * OmegaLambda        Vacuum energy density
 * HubbleParam        Little 'h'
 * flag_stellarage    Whether the age of newly formed stars is recorded and saved
 * flag_metals        Whether metal enrichment is included
 * hashtabsize        The size of the hashtable belonging to this snapshot file
 * npartTotalHighWord High word of the total number of particles of each type
 */
typedef struct
{
	unsigned int npart[6];
	double mass[6];
	double time;
	double redshift;
	int flag_sfr;
	int flag_feedback;
	unsigned int npartTotal[6];
	int flag_cooling;
	int num_files;
	double BoxSize;
	double Omega0;
	double OmegaLambda;
	double HubbleParam;
	int flag_stellarage;
	int flag_metals;
	int hashtabsize;
	unsigned int npartTotalHighWord[6];
	char fill[60];
} SnapshotHeaderLGadget;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

SnapshotHeaderLGadget *readSnapshotHeaderLGadgetInternal(char *filename);
SnapshotHeader readSnapshotHeaderLGadget(char *filename);
void printLGadgetHeader(SnapshotHeaderLGadget *header);
void readParticlesLGadget(char *filename, Particle **particles, int *n_particles, int check_corrupt);

#endif
