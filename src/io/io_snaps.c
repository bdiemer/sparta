/*************************************************************************************************
 *
 * This unit contains routines to read snapshot files that are not specific to a particular
 * format.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "io_snaps.h"
#include "io_snaps_lgadget.h"
#include "io_snaps_gadget3.h"
#include "../global.h"
#include "../config.h"
#include "../memory.h"
#include "../tracers/tracers.h"
#include "../geometry.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Constants used when obtaining snapshot listing. The MAX_NUMBER is an absolute limit on where
 * to look for a snapshot.
 */
#define MAX_SNAP_NUMBER 1000
#define MAX_SNAP_GAP 2

/*
 * If READ_SNAPSHOT_COMM, read snapshots the "smart" way, i.e. with MPI communication. Otherwise,
 * each core reads its own snapshots. This old routine is kept for testing purposes.
 *
 * COMP_PARTICLES triggers a comparison between the two routines.
 */
#define READ_SNAPSHOT_COMM 1
#define COMP_PARTICLES 0

/*
 * If >0, this setting imposes an overall time limit on the loading process and creates an error
 * message if a process exceeds that time. This can be useful if processes hang for some reason,
 * rather than causing an error message. Turning this on demands an extra MPI call though.
 */
#define FL_MAX_WAIT_TIME 0

/*
 * Tags for particle exchange messages. As a precaution, these tags do not overlap with other
 * asynchronous communication tags, e.g. those for halo exchange.
 */
#define PMES_TAG_RCVD 10
#define PMES_TAG_DONE 11
#define PMES_TAG_ALL_DONE 12
#define PMES_TAG_PARTICLES 13

/*
 * Log level for snapshot exchange operations. This should normally be a very high log level
 * as there are many (unimportant) messages generated, but it can be turned to a lower level for
 * debugging.
 */
#define LOG_LEVEL_SNAP_EXC 4

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

#if DO_READ_PARTICLES
int freeParticleMessage(ParticleMessage *pmes);
#endif

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * This function generates a filename given the snap index as stored on disk, which does not need
 * to be the internal snap index. Typically, the function getSnapshotFilename() should be used
 * instead.
 */
void getSnapshotFilenameDirOrder(int snap_dir_idx, int file_idx, char *str)
{
	getFilePathSnapChunk(config.snap_path, snap_dir_idx, file_idx, str);
}

/*
 * This function converts the internal snap_idx to the actual index number of this snapshot on
 * disk, so that this conversion is not duplicated across the code.
 */
void getSnapshotFilename(int snap_idx, int file_idx, char *str)
{
	getSnapshotFilenameDirOrder(config.snap_id[snap_idx], file_idx, str);
}

/*
 * This function generates a generalized snapshot header from the headers of the file format of the
 * simulation we're running on.
 */
SnapshotHeader readSnapshotHeader(int snap_idx, int file_idx)
{
	char filename[300];
	SnapshotHeader header;

	getSnapshotFilename(snap_idx, file_idx, filename);

	switch (config.snap_sim_type)
	{
	case SIM_TYPE_LGADGET:
		header = readSnapshotHeaderLGadget(filename);
		break;
	case SIM_TYPE_GADGET3:
	case SIM_TYPE_AREPO:
		header = readSnapshotHeaderGadget3(filename);
		break;
	default:
		error(__FFL__, "[Main] No routine to read redshift from files of simulation type %d.\n",
				config.snap_sim_type);
		break;
	}

	return header;
}

/*
 * Transfer certain fields from the header to the config.
 */
void analyzeSnapshotHeader()
{
	SnapshotHeader header;

	header = readSnapshotHeader(0, 0);

	config.n_files_per_snap = header.n_files_per_snap;
	config.n_particles = header.n_particles;
	config.box_size = header.box_size;
	config.particle_mass = header.particle_mass;
	config.Omega_m = header.Omega_m;
	config.Omega_L = header.Omega_L;
	config.h = header.h;
}

/*
 * Note that the input to this function is an index along the list of snapshots in the snap dir,
 * NOT the list of snapshots that the code will eventually run on.
 */
float snapA(int snap_idx)
{
	SnapshotHeader header;

	header = readSnapshotHeader(snap_idx, 0);

	return header.a;
}

/*
 * Go through snapshot directory, see how many snapshots there are and get their redshifts.
 * The method of querying files with higher and higher numbers is a little clumsy, but given
 * that the user can specify the snap_path freely it is difficult to search only for snapshot
 * directories or files.
 */
int getSnapshotListing(int *snapshot_id, float *snapshot_a)
{
	int i, n_snaps_tot;
	char filename[300];
	FILE *f_snap;

	i = -1;
	n_snaps_tot = 0;
	while (1)
	{
		i++;

		/*
		 * Here, we need the snap filename where i corresponds to the order of snaps stored on disk
		 * rather than the internal order (which is yet to be determined).
		 */
		getSnapshotFilenameDirOrder(i, 0, filename);
		f_snap = fopen(filename, "r");
		if (f_snap == NULL)
		{
			output(3, "[Main] Looked for snapshot file %s, not found.\n", filename);
			if ((i > MAX_SNAP_NUMBER) || ((n_snaps_tot >= 10) && (i > MAX_SNAP_GAP * n_snaps_tot)))
			{
				output(5, "[Main] Stopping search for new snapshots at number %d.\n", i);
				break;
			}
		} else
		{
			fclose(f_snap);
			if (n_snaps_tot >= MAX_SNAPS)
				error(__FFL__, "Exceeded maximum number of snapshots (%d).\n", MAX_SNAPS);

			/*
			 * Note that we need to (prematurely) set the snap_dir_idx in the actual config,
			 * because that is used to read the header and obtain the redshift.
			 */
			snapshot_id[n_snaps_tot] = i;
			config.snap_id[n_snaps_tot] = i;
			snapshot_a[n_snaps_tot] = snapA(n_snaps_tot);
			output(2, "[Main] Found snapshot number %d, a = %.5f\n", snapshot_id[n_snaps_tot],
					snapshot_a[n_snaps_tot]);
			n_snaps_tot++;
		}
	}

	return n_snaps_tot;
}

/*
 * Check the validity of a particle ID to detect whether a file is corrupted. In valid Gadget2
 * snapshots, the IDs run from 1 to N_particles. When the file is corrupt, some IDs will typically
 * run higher than N_particles.
 */
int checkParticleID(long int id)
{
	if (id < 0)
		return PART_ID_NEGATIVE;
	if (id > config.n_particles)
	{
		return PART_ID_LARGE;
	}

	return PART_OK;
}

/*
 * Check the validity of a particle position to detect whether a file is corrupted.
 */
int checkParticlePosition(float *x)
{
	int d;

	for (d = 0; d < 3; d++)
	{
		if (isnan(x[d]))
			return PART_POS_NAN;
		if (isinf(x[d]))
			return PART_POS_INF;
		if (x[d] < 0)
			return PART_POS_SMALL;
		if (x[d] > config.box_size)
			return PART_POS_LARGE;
	}

	return PART_OK;
}

/*
 * As the position function above, except that there are no real bounds on the velocity.
 */
int checkParticleVelocity(float *v)
{
	int d;

	for (d = 0; d < 3; d++)
	{
		if (isnan(v[d]))
			return PART_VEL_NAN;
		if (isinf(v[d]))
			return PART_VEL_INF;
	}

	return PART_OK;
}

#if DO_READ_PARTICLES
/*
 * This function implements a distributed reading of snapshot files where each file is only read
 * once and particles are sent between processes. Not every process must be a reader. All
 * communication happens asynchronously, and blocking calls need to be avoided at all cost because
 * they are likely to lead to situations where a process can get stuck.
 *
 * In particular, there are two special cases that can easily cause trouble: when a process sends
 * particles to itself, and when a process sends repeated messages to the same process(es). Thus,
 * the code should never assume that MPI commands are received in any particular order.
 *
 * Furthermore, MPI_Wait and MPI_Test only check whether a message was picked up by the MPI
 * library, but NOT whether the message was actually received by the code on the other process.
 * Thus, we manually confirm the receipt of particle chunks with a confirmation message. As one
 * process can send multiple messages to the same process, these confirmations need to carry a
 * unique ID assigning them to a sent message.
 */
void readSnapshot(int snap_idx, Box *box, Particle **particles, int *n_particles, int *n_files_read)
{
#if READ_SNAPSHOT_COMM

	const int max_n_mes = config.snap_max_waiting_messages + n_proc;

	int i, j, reader_step, is_reader, ifile, n_read, reader_idx, read_file_idx, next_file_idx,
			n_file, n_mask, n_waiting_mes, finished, pmes_idx, counter, attempt_receive, n_tot,
			n_proc_finished, n_read_procs, has_msg, old_req_finished, n_recv, finish_sent,
			n_intersect, tmp_proc;
	long int confirmed_id;
	int_least8_t *mask, intersect[n_proc];
	char filename[300];
	Box all_boxes[n_proc], *snap_box;
	Particle *p_file, *p_all;
	ParticleMessage *pmes = NULL;
	ParticleReceipt receipts[n_proc];
	MPI_Status mpi_status;
	MPI_Request done_request, proc_finish_requests[n_proc];

#if CAREFUL
	int n_sent_to_proc[n_proc], n_rcvd_from_proc[n_proc];
#endif
#if FL_MAX_WAIT_TIME > 0
	double t0;
	if (is_main_proc)
		t0 = MPI_Wtime();
	MPI_Bcast(&t0, 1, MPI_DOUBLE, MAIN_PROC, comm);
#endif

	/*
	 * Exchange all boxes with all cores
	 */
	MPI_Allgather(box, 1, mpiTypes.box, all_boxes, 1, mpiTypes.box, comm);

	/*
	 * Figure out whether this proc needs to load files, and which. Reading on the main can
	 * apparently cause an MPI crash on at least one system where the code was tested; the reason
	 * is currently unclear.
	 * TODO
	 */
	n_read_procs = config.snap_max_read_procs;
	if (config.snap_read_on_main)
	{
		if (n_proc < n_read_procs)
			n_read_procs = n_proc;
		if (config.n_files_per_snap < n_read_procs)
			n_read_procs = config.n_files_per_snap;
		reader_idx = -1;
		reader_step = n_proc / n_read_procs;
		is_reader = ((proc % reader_step == 0) && (proc / reader_step < n_read_procs));
		if (is_reader)
			reader_idx = proc / reader_step;
	} else
	{
		if (MAIN_PROC != 0)
			error(__FFL__, "Main process must be 0 if snap_read_on_main option is off.\n");
		if (n_proc - 1 < n_read_procs)
			n_read_procs = n_proc - 1;
		if (config.n_files_per_snap < n_read_procs)
			n_read_procs = config.n_files_per_snap;
		reader_idx = -1;
		reader_step = (n_proc - 1) / n_read_procs;
		is_reader = ((!is_main_proc) && (proc % reader_step == 0)
				&& ((proc - 1) / reader_step < n_read_procs));
		if (is_reader)
			reader_idx = (proc - 1) / reader_step;
	}

	output(LOG_LEVEL_SNAP_EXC,
			"[%4d] [SN %3d] File loading: %d readers, step %d, is_reader %d, reader_idx %d\n", proc,
			snap_idx, n_read_procs, reader_step, is_reader, reader_idx);

	/*
	 * Prepare
	 */
	if (is_reader)
	{
		n_read = 0;
		ifile = 0;
		n_waiting_mes = 0;
		read_file_idx = reader_idx + ifile * n_read_procs;
		pmes = (ParticleMessage*) memAlloc(__FFL__, MID_PARTICLEMESSAGES,
				sizeof(ParticleMessage) * max_n_mes);
		for (i = 0; i < max_n_mes; i++)
		{
			pmes[i].in_use = 0;
			pmes[i].allocated = 0;
			pmes[i].confirmed = 0;
			pmes[i].proc = -1;
			pmes[i].msg_id = -1;
			pmes[i].req = MPI_REQUEST_NULL;
		}
	} else
	{
		read_file_idx = 0;
	}

	n_proc_finished = 0;
	p_all = NULL;
	n_tot = 0;
	finished = 0;
	finish_sent = 0;
	for (i = 0; i < n_proc; i++)
	{
#if CAREFUL
		n_sent_to_proc[i] = 0;
		n_rcvd_from_proc[i] = 0;
#endif
		proc_finish_requests[i] = MPI_REQUEST_NULL;
		receipts[i].req = MPI_REQUEST_NULL;
		receipts[i].msg_id = -1;
	}

	/*
	 * Loop until all files have been loaded
	 */
	while (!finished)
	{
		/*
		 * If reader, read a file and send out messages
		 */
		if ((is_reader) && (read_file_idx < config.n_files_per_snap)
				&& (n_waiting_mes < config.snap_max_waiting_messages))
		{
			ifile++;
			next_file_idx = reader_idx + ifile * n_read_procs;

			/*
			 * Get snap box for this snapshot and file, abort if it is set to be ignored.
			 */
			snap_box = &(snap_boxes[snap_idx * config.n_files_per_snap + read_file_idx]);
			if (snap_box->ignore)
			{
				read_file_idx = next_file_idx;
				continue;
			}

			/*
			 * Check intersections. If there are none, we don't need to read the file at all.
			 */
			n_intersect = 0;
			for (i = 0; i < n_proc; i++)
			{
				intersect[i] = boxesIntersect(&(all_boxes[i]), snap_box);
				if (intersect[i])
					n_intersect++;
			}
			if (n_intersect == 0)
			{
				read_file_idx = next_file_idx;
				continue;
			}

			/*
			 * Read file
			 */
			getSnapshotFilename(snap_idx, read_file_idx, filename);
			output(LOG_LEVEL_SNAP_EXC, "[%4d] [SN %3d] File loading: Reading file %s\n", proc,
					snap_idx, filename);
			readParticles(filename, &p_file, &n_file, 0);
			mask = (int_least8_t*) memAlloc(__FFL__, MID_SNAPREADBUF,
					sizeof(int_least8_t) * n_file);

			pmes_idx = 0;
			for (i = 0; i < n_proc; i++)
			{
				/*
				 * Find particles in intersection, if any
				 */
				if (intersect[i])
				{
					n_mask = 0;
					for (j = 0; j < n_file; j++)
					{
						if (inBox(p_file[j].x, &(all_boxes[i])))
						{
							mask[j] = 1;
							n_mask++;
						} else
						{
							mask[j] = 0;
						}
					}

					/*
					 * Don't send messages with zero particles, that would be wasteful and leads
					 * to errors when allocating memory.
					 */
					if (n_mask == 0)
						continue;

					/*
					 * Find a free message object, allocate buffer
					 */
					while (pmes[pmes_idx].in_use)
						pmes_idx++;
					pmes[pmes_idx].in_use = 1;
					pmes[pmes_idx].allocated = 1;
					pmes[pmes_idx].confirmed = 0;
					pmes[pmes_idx].proc = i;
					pmes[pmes_idx].n = n_mask;
					pmes[pmes_idx].buffer = (Particle*) memAlloc(__FFL__, MID_PARTICLEMESSAGES,
							sizeof(Particle) * (n_mask));
					n_waiting_mes++;

					/*
					 * Write particles into buffer
					 */
					counter = 0;
					for (j = 0; j < n_file; j++)
					{
						if (mask[j])
						{
							memcpy(&(pmes[pmes_idx].buffer[counter]), &(p_file[j]),
									sizeof(Particle));
							counter++;
						}
					}

					/*
					 * Use the first particle's ID as a message identifier. This ID cannot appear
					 * twice since a particle can only be in the files once.
					 */
					pmes[pmes_idx].msg_id = pmes[pmes_idx].buffer[0].id;

					/*
					 * Send MPI message
					 */
					MPI_Isend(pmes[pmes_idx].buffer, pmes[pmes_idx].n, mpiTypes.particle, i,
					PMES_TAG_PARTICLES, comm, &(pmes[pmes_idx].req));
#if CAREFUL
					n_sent_to_proc[i] = pmes[pmes_idx].n;
#endif

					output(LOG_LEVEL_SNAP_EXC,
							"[%4d] [SN %3d] File loading: Sent %d particles to proc %d, message ID %ld.\n",
							proc, snap_idx, pmes[pmes_idx].n, i, pmes[pmes_idx].msg_id);
				}
			}

			memFree(__FFL__, MID_SNAPREADBUF, mask, sizeof(int_least8_t) * n_file);
			memFree(__FFL__, MID_PARTICLES, p_file, sizeof(Particle) * n_file);
			n_read++;
			read_file_idx = next_file_idx;
		}

		/*
		 * Executed by every proc: read messages, add to buffers
		 */
		attempt_receive = 1;
		while (attempt_receive)
		{
			MPI_Iprobe(MPI_ANY_SOURCE, PMES_TAG_PARTICLES, comm, &has_msg, &mpi_status);
			if (has_msg)
			{
				/*
				 * First, make sure that there is no old confirmation lying around. It would seem
				 * that using the wait command would be OK in this instance because even if the
				 * sender process whose confirmation hasn't finished sends another particle message,
				 * that message would be asynchronous. However, that process could be the same
				 * process as this one, in which case it could hang and never get to receive the
				 * confirmation. Thus, we test in a non-blocking fashion and wait with picking up
				 * the particle message if the confirmation hasn't been received yet. In that case,
				 * we also interrupt the loop so that we don't get stuck trying to receive the same
				 * message over and over again.
				 */
				tmp_proc = mpi_status.MPI_SOURCE;
				if (receipts[tmp_proc].req == MPI_REQUEST_NULL)
				{
					old_req_finished = 1;
				} else
				{
					MPI_Test(&(receipts[tmp_proc].req), &old_req_finished, MPI_STATUS_IGNORE);
				}

				if (old_req_finished)
				{
					/*
					 * Now get the particle count and allocate the buffer.
					 */
					MPI_Get_count(&mpi_status, mpiTypes.particle, &n_recv);
					if (p_all == NULL)
					{
						p_all = (Particle*) memAlloc(__FFL__, MID_PARTICLES,
								sizeof(Particle) * (n_tot + n_recv));
					} else
					{
						p_all = (Particle*) memRealloc(__FFL__, MID_PARTICLES, p_all,
								sizeof(Particle) * (n_tot + n_recv), sizeof(Particle) * n_tot);
					}
					MPI_Recv(&(p_all[n_tot]), n_recv, mpiTypes.particle, tmp_proc,
					PMES_TAG_PARTICLES, comm, MPI_STATUS_IGNORE);

					/*
					 * Checking the particle ID will typically filter out garbled MPI messages.
					 */
#if PARANOID
					for (j = 0; j < n_recv; j++)
					{
						if (checkParticleID(p_all[n_tot + j].id) != PART_OK)
						{
							error(__FFL__,
									"Found invalid ID %ld, from proc %d, index %d, total index %d, n_recv %d.\n",
									p_all[n_tot + j].id, tmp_proc, j, n_tot + j, n_recv);
						}
					}
#endif

					/*
					 * Let the sending process know that the message was received. We send the ID of
					 * the first particle in the message back so that the sender process can identify
					 * which message has been received.
					 */
					receipts[tmp_proc].msg_id = p_all[n_tot].id;
					MPI_Isend(&(receipts[tmp_proc].msg_id), 1, MPI_LONG, tmp_proc, PMES_TAG_RCVD,
							comm, &(receipts[tmp_proc].req));

					/*
					 * Now we increase the counter to include the added particles.
					 */
#if CAREFUL
					n_rcvd_from_proc[tmp_proc] = n_recv;
#endif
					n_tot += n_recv;
					output(LOG_LEVEL_SNAP_EXC,
							"[%4d] [SN %3d] File loading: Received %d particles from proc %d, message ID %ld.\n",
							proc, snap_idx, n_recv, tmp_proc, receipts[tmp_proc].msg_id);
				} else
				{
					attempt_receive = 0;
				}
			} else
			{
				attempt_receive = 0;
			}
		}

		/*
		 * Readers only: check for messages that were received. There are multiple steps to this
		 * procedure.
		 */
		if (is_reader)
		{
			/*
			 * Step 1: wait for an asynchronous message confirming that the other process
			 * actually received the message. If so, we mark the message as confirmed. Note that
			 * we do not attempt to de-allocate in this step, that process needs to be separate
			 * because it is not guaranteed in which order MPI will do things.
			 */
			attempt_receive = 1;
			while (attempt_receive)
			{
				MPI_Iprobe(MPI_ANY_SOURCE, PMES_TAG_RCVD, comm, &has_msg, &mpi_status);
				if (has_msg)
				{
					tmp_proc = mpi_status.MPI_SOURCE;
					MPI_Recv(&confirmed_id, 1, MPI_LONG, tmp_proc, PMES_TAG_RCVD, comm,
							MPI_STATUS_IGNORE);
					output(LOG_LEVEL_SNAP_EXC,
							"[%4d] [SN %3d] File loading: Got confirmation from proc %d, message ID %ld.\n",
							proc, snap_idx, tmp_proc, confirmed_id);
					for (i = 0; i < max_n_mes + 1; i++)
					{
						if (i == max_n_mes)
						{
							for (j = 0; j < max_n_mes; j++)
								output(0, "[%4d] Message %3d in_use %d proc %3d ID %8ld\n", proc, j,
										pmes[j].in_use, pmes[j].proc, pmes[j].msg_id);
							error(__FFL__,
									"Could not find particle message record matching received confirmation from proc %d, ID %ld.\n",
									tmp_proc, confirmed_id);
						}
						if ((pmes[i].in_use) && (pmes[i].proc == tmp_proc)
								&& (pmes[i].msg_id == confirmed_id))
						{
							pmes[i].confirmed = 1;
							break;
						}
					}
				} else
				{
					attempt_receive = 0;
				}
			}

			/*
			 * Step 2: Try freeing buffers that were picked up by MPI. This can be done before
			 * a confirmation has been received. If successful, the free function will set the
			 * allocated flag to False.
			 */
			for (i = 0; i < max_n_mes; i++)
			{
				if ((pmes[i].in_use) && (pmes[i].allocated))
					freeParticleMessage(&(pmes[i]));
			}

			/*
			 * Step 3: If messages have both been de-allocated and confirmed, we can re-open the
			 * message slot.
			 */
			for (i = 0; i < max_n_mes; i++)
			{
				if ((pmes[i].in_use) && (pmes[i].allocated == 0) && (pmes[i].confirmed))
				{
					pmes[i].in_use = 0;
					pmes[i].proc = -1;
					n_waiting_mes--;
				}
			}

			/*
			 * If we are a reader and done, we let the MAIN PROC know. Once all readers are done,
			 * we can stop the loop.
			 */
			if ((read_file_idx >= config.n_files_per_snap) && (n_waiting_mes == 0)
					&& (!finish_sent))
			{
				MPI_Isend(NULL, 0, MPI_INT, MAIN_PROC, PMES_TAG_DONE, comm, &(done_request));
				finish_sent = 1;
				output(LOG_LEVEL_SNAP_EXC,
						"[%4d] [SN %3d] File loading: Reader process sent complete signal.\n", proc,
						snap_idx);
			}
		}

		/*
		 * On the main proc, check for readers that are done and count them. If all readers are
		 * send a message to all procs.
		 */
		if (is_main_proc)
		{
			attempt_receive = 1;
			while (attempt_receive)
			{
				MPI_Iprobe(MPI_ANY_SOURCE, PMES_TAG_DONE, comm, &has_msg, &mpi_status);
				if (has_msg)
				{
					MPI_Recv(NULL, 0, MPI_INT, mpi_status.MPI_SOURCE, PMES_TAG_DONE, comm,
							MPI_STATUS_IGNORE);
					n_proc_finished++;
					output(LOG_LEVEL_SNAP_EXC,
							"[Main] [SN %3d] File loading: A total of %d reader processes have finished.\n",
							snap_idx, n_proc_finished);
				} else
				{
					attempt_receive = 0;
				}
			}

			if (n_proc_finished == n_read_procs)
			{
				for (i = 0; i < n_proc; i++)
					MPI_Isend(NULL, 0, MPI_INT, i, PMES_TAG_ALL_DONE, comm,
							&(proc_finish_requests[i]));
				output(LOG_LEVEL_SNAP_EXC,
						"[Main] [SN %3d] File loading: Sent final signal to all procs.\n",
						snap_idx);
			}
		}

		/*
		 * On all procs, check whether the main proc has sent an abort message.
		 */
		MPI_Iprobe(MAIN_PROC, PMES_TAG_ALL_DONE, comm, &has_msg, &mpi_status);
		if (has_msg)
		{
			MPI_Recv(NULL, 0, MPI_INT, MAIN_PROC, PMES_TAG_ALL_DONE, comm, MPI_STATUS_IGNORE);
			if (is_reader)
				MPI_Wait(&done_request, MPI_STATUS_IGNORE);
			finished = 1;
		}

		/*
		 * Check whether a time limit has been exceeded. In this case, one of the processes appears
		 * to have crashed.
		 */
#if FL_MAX_WAIT_TIME > 0
		if ((MPI_Wtime() - t0) > FL_MAX_WAIT_TIME)
		{
			output(0,
					"Proc %2d, is_reader %d, read_file_idx %d, n_waiting_mes %d, finish_sent %d.\n",
					proc, is_reader, read_file_idx, n_waiting_mes, finish_sent);
			for (i = 0; i < max_n_mes; i++)
			{
				if (pmes[i].in_use)
				{
					output(0,
							"Proc %2d, message %d in use, allocated %d, confirmed %d, proc %d, n %d.\n",
							proc, i, pmes[i].allocated, pmes[i].confirmed, pmes[i].proc, pmes[i].n);
				}
			}
			t0 = MPI_Wtime();
			if (proc == MAIN_PROC)
			{
				delay(2000);
				error(__FFL__, "Exceeded time limit in file reading routine.\n");
			}
		}
#endif
	}

	/*
	 * Force the completion of rcvd confirmations on all procs.
	 */
	for (i = 0; i < n_proc; i++)
	{
		if (receipts[i].req != MPI_REQUEST_NULL)
			MPI_Wait(&(receipts[i].req), MPI_STATUS_IGNORE);
	}

	/*
	 * Force the completion of the finish messages on main proc.
	 */
	if (is_main_proc)
	{
		for (i = 0; i < n_proc; i++)
			MPI_Wait(&(proc_finish_requests[i]), MPI_STATUS_IGNORE);
	}

	/*
	 * Free memory
	 */
	if (is_reader)
	{
		memFree(__FFL__, MID_PARTICLEMESSAGES, pmes, sizeof(ParticleMessage) * max_n_mes);
	}

#if PARANOID
	int all_n_sent[n_proc * n_proc];
	int all_n_rcvd[n_proc * n_proc];
	int i1, i2;

	MPI_Gather(n_sent_to_proc, n_proc, MPI_INT, all_n_sent, n_proc, MPI_INT, MAIN_PROC, comm);
	MPI_Gather(n_rcvd_from_proc, n_proc, MPI_INT, all_n_rcvd, n_proc, MPI_INT, MAIN_PROC, comm);

	if (is_main_proc)
	{
		for (i = 0; i < n_proc; i++)
		{
			for (j = 0; j < n_proc; j++)
			{
				i1 = n_proc * i + j;
				i2 = n_proc * j + i;
				if (all_n_sent[i1] != all_n_rcvd[i2])
					error(__FFL__, "Proc %d sent %d particles to proc %d, but %d were received.\n",
							i, all_n_sent[i1], j, all_n_rcvd[i2]);
			}
		}
	}
#endif

#if COMP_PARTICLES
	/*
	 * Check that the old and new methods found the same number of particles. For more detailed
	 * checks, we would need to sort the particles as they are not going to be read in the same
	 * order.
	 */
	readParticlesInBox(snap_idx, box, particles, n_particles, n_files_read, 0);

	if (*n_particles != n_tot)
	error(__FFL__, "proc %d, Found %d particles old and %d new\n", proc, n_particles, n_tot);
	else
	output(0, "Found %d old %d new\n", *n_particles, n_tot);
#endif

	for (i = 0; i < n_tot; i++)
		debugTracerByID(p_all[i].id, "Found particle, index %d, x [%.2f, %.2f, %.2f].", i,
				p_all[i].x[0], p_all[i].x[1], p_all[i].x[2]);

	*particles = p_all;
	*n_particles = n_tot;
	*n_files_read = n_read;

#else
	readParticlesInBox(snap_idx, box, particles, n_particles, n_files_read, 0);
#endif
}

int freeParticleMessage(ParticleMessage *pmes)
{
	int ret, do_free;

	ret = 1;
	do_free = 0;

	if (pmes->req == MPI_REQUEST_NULL)
	{
		do_free = (pmes->n > 0);

		if (do_free && is_main_proc)
		{
			output(LOG_LEVEL_SNAP_EXC,
					"[%4d]          Request is NULL, freeing particle message to proc %d, n %d, ID %ld.\n",
					proc, pmes->proc, pmes->n, pmes->buffer[0].id);
		}

	} else
	{
		MPI_Test(&(pmes->req), &do_free, MPI_STATUS_IGNORE);
		if (!do_free)
			ret = 0;

		if (do_free && is_main_proc)
		{
			output(LOG_LEVEL_SNAP_EXC,
					"[%4d]          Request tested completed, freeing particle message to proc %d, n %d, ID %ld.\n",
					proc, pmes->proc, pmes->n, pmes->buffer[0].id);
		}

	}

	if (do_free)
	{
		memFree(__FFL__, MID_PARTICLEMESSAGES, pmes->buffer, sizeof(Particle) * pmes->n);
		pmes->n = 0;
		pmes->allocated = 0;
	}

	return ret;
}

void readParticles(char *filename, Particle **particles, int *n_particles, int check_corrupt)
{
	void (*read_function)(char*, Particle**, int*, int) = NULL;

	switch (config.snap_sim_type)
	{
	case SIM_TYPE_LGADGET:
		read_function = &readParticlesLGadget;
		break;
	case SIM_TYPE_GADGET3:
	case SIM_TYPE_AREPO:
		read_function = &readParticlesGadget3;
		break;
	default:
		error(__FFL__, "[Main] No routine to read particles from files of simulation type %d.\n",
				config.snap_sim_type);
		break;
	}

	read_function(filename, particles, n_particles, check_corrupt);
}

void readParticlesInBox(int snap_idx, Box *box, Particle **particles, int *n_particles,
		int *n_files_read, int fix_periodic)
{
	int *file_intersect, n_intersect;
	int i, j, d, n_file, n_mask, n_tot, counter;
	int_least8_t *mask;
	char filename[300];
	Particle *p_file, *p_all;

#if CHECK_POSITION
	int check_counter, check_counter_all;
	const float check_pos[3] =
	{	CHECK_POSITION_X, CHECK_POSITION_Y, CHECK_POSITION_Z};
	check_counter = 0;
	check_counter_all = 0;
#endif

	n_tot = 0;
	intersectingSnapFiles(snap_idx, box, &file_intersect, &n_intersect);
	p_all = NULL;
	for (i = 0; i < config.n_files_per_snap; i++)
	{
#if CHECK_POSITION
		file_intersect[i] = 1;
#endif
		if (file_intersect[i])
		{
			getSnapshotFilename(snap_idx, i, filename);
			output(5, "[%4d] [SN %3d] Reading file %s\n", proc, snap_idx, filename);
			readParticles(filename, &p_file, &n_file, 0);
			mask = (int_least8_t*) memAlloc(__FFL__, MID_SNAPREADBUF,
					sizeof(int_least8_t) * n_file);
			n_mask = 0;
			for (j = 0; j < n_file; j++)
			{
#if CHECK_POSITION
				if (is_main_proc)
				{
					int k;
					float r, dx;

					r = 0.0;
					for (k = 0; k < 3; k++)
					{
						dx = fabs(p_file[j].x[k] - check_pos[k]);
						dx = fminf(dx, fabs(p_file[j].x[k] - check_pos[k] + config.box_size));
						dx = fminf(dx, fabs(p_file[j].x[k] - check_pos[k] - config.box_size));
						r += dx * dx;
					}
					r = sqrt(r);
					if (r <= CHECK_POSITION_R)
					{
						check_counter++;
						output(0, "CHECK_POSITION: Found particle %d x %.6f %.6f %.6f id %ld\n",
								check_counter, p_file[j].x[0], p_file[j].x[1], p_file[j].x[2],
								p_file[j].id);
					}
					check_counter_all++;
				}
#endif

				if (inBox(p_file[j].x, box))
				{
					mask[j] = 1;
					n_mask++;
				} else
				{
					mask[j] = 0;
				}
			}
			counter = n_tot;
			if (n_mask > 0)
			{
				if (p_all == NULL)
				{
					p_all = (Particle*) memAlloc(__FFL__, MID_PARTICLES,
							sizeof(Particle) * (n_tot + n_mask));
				} else
				{
					p_all = (Particle*) memRealloc(__FFL__, MID_PARTICLES, p_all,
							sizeof(Particle) * (n_tot + n_mask), sizeof(Particle) * n_tot);
				}
				for (j = 0; j < n_file; j++)
				{
					if (mask[j])
					{
						memcpy(&(p_all[counter]), &(p_file[j]), sizeof(Particle));
						counter++;
					}
				}
				n_tot += n_mask;
			}
			memFree(__FFL__, MID_SNAPREADBUF, mask, sizeof(int_least8_t) * n_file);
			memFree(__FFL__, MID_PARTICLES, p_file, sizeof(Particle) * n_file);
		}
	}

	memFree(__FFL__, MID_GEOMETRY, file_intersect, sizeof(int) * config.n_files_per_snap);

#if CHECK_POSITION
	if (is_main_proc)
	output(0, "CHECK_POSITION: found %d out of %d particles.\n", check_counter, check_counter_all);
#endif

	*n_particles = n_tot;
	*n_files_read = n_intersect;
	*particles = p_all;

	if (fix_periodic)
	{
		for (d = 0; d < 3; d++)
		{
			if (box->periodic[d])
			{
				for (i = 0; i < n_tot; i++)
				{
					if (p_all[i].x[d] < box->min[d])
						p_all[i].x[d] += config.box_size;
				}
			}
		}
	}
}
#endif
