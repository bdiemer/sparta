/*************************************************************************************************
 *
 * This unit contains functions related to halo catalogs.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _IO_HALOCAT_H_
#define _IO_HALOCAT_H_

#include "../global_types.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

int getNCatFields();
char **getCatFieldNames();
char **getCatFieldDefaults();
void readHaloCatalog(int snap_idx, HaloRequest *requests, int n_requests, DynArray *da_cd);
void matchCatalogsToSnapshots(int n_snaps_tot, int *snapshot_id, float *snapshot_a);

#endif
