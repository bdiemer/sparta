/*************************************************************************************************
 *
 * This unit contains routines to read snapshot files that are not specific to a particular
 * format.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _IO_SNAPS_H_
#define _IO_SNAPS_H_

#include "../geometry.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Return values for the function that checks that particle data are OK.
 */
enum
{
	PART_ID_NEGATIVE,
	PART_ID_LARGE,
	PART_OK,
	PART_POS_NAN,
	PART_POS_INF,
	PART_POS_SMALL,
	PART_POS_LARGE,
	PART_VEL_NAN,
	PART_VEL_INF
};

/*
 * Debug option: print the number of particles around a certain position. This is useful for
 * debugging the tree unit. Note that this definition needs to be in a header file because it
 * must be respected by the geometry unit.
 */
#define CHECK_POSITION 0
#define CHECK_POSITION_X 0.0
#define CHECK_POSITION_Y 0.0
#define CHECK_POSITION_Z 0.0
#define CHECK_POSITION_R 0.0

/*************************************************************************************************
 * DATA STRUCTURES
 *************************************************************************************************/

/*
 * The structure a reader process keeps after sending a particle message to another process. A
 * reader will typically send multiple messages to the same process, so we need to keep a unique
 * message identifier that can be used in the confirmation message. Here, we use the ID of the
 * first particle in the message as msg_id.
 */
typedef struct
{
	short int in_use;
	short int allocated;
	short int confirmed;
	int proc;
	long int msg_id;
	int n;
	Particle *buffer;
	MPI_Request req;
} ParticleMessage;

typedef struct
{
	long int msg_id;
	MPI_Request req;
} ParticleReceipt;

/*
 * This struct represents a generalized snapshot header, containing the information that needs
 * to be obtainable from ANY snapshot format in order for the code to work. The headers of specific
 * snapshot formats can be converted to this header.
 */
typedef struct
{
	// Should be the same in all snapshots
	int n_files_per_snap;
	int n_particles;
	float box_size;
	float particle_mass;
	float Omega_m;
	float Omega_L;
	float h;

	// Specific to this snapshot
	float a;
} SnapshotHeader;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void getSnapshotFilenameDirOrder(int snap_dir_idx, int file_idx, char *str);
void getSnapshotFilename(int snap_dir_idx, int file_idx, char *str);
SnapshotHeader readSnapshotHeader(int snap_dir_idx, int file_idx);
void analyzeSnapshotHeader();
float snapA(int snap_idx);
int getSnapshotListing(int *snapshot_id, float *snapshot_a);

int checkParticleID(long int id);
int checkParticlePosition(float *x);
int checkParticleVelocity(float *v);

#if DO_READ_PARTICLES
void readSnapshot(int snap_idx, Box *box, Particle **particles, int *n_particles, int *n_files_read);
void readParticles(char *filename, Particle **particles, int *n_particles, int check_corrupt);
void readParticlesInBox(int snap_idx, Box *box, Particle **particles, int *n_particles,
		int *n_files_read, int fix_periodic);
#endif

#endif
