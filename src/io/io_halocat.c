/*************************************************************************************************
 *
 * This unit contains functions related to halo catalogs.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "io_halocat.h"
#include "io_halocat_rockstar.h"
#include "../utils.h"
#include "../halos/halo.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

int getNCatFields()
{
	switch (config.cat_type)
	{
	case CAT_TYPE_ROCKSTAR:
		return getNCatFieldsRockstar();
		break;
	default:
		error(__FFL__,
				"[Main] No routine to return number of catalog fields for catalog type %d.\n",
				config.cat_type);
		break;
	}

	return 0;
}

char** getCatFieldNames()
{
	switch (config.cat_type)
	{
	case CAT_TYPE_ROCKSTAR:
		return getCatFieldNamesRockstar();
		break;
	default:
		error(__FFL__, "[Main] No routine to return catalog field names for catalog type %d.\n",
				config.cat_type);
		break;
	}

	return NULL;
}

char** getCatFieldDefaults()
{
	switch (config.cat_type)
	{
	case CAT_TYPE_ROCKSTAR:
		return getCatFieldDefaultsRockstar();
		break;
	default:
		error(__FFL__, "[Main] No routine to return catalog field defaults for catalog type %d.\n",
				config.cat_type);
		break;
	}

	return NULL;
}

/*
 * This function is executed once per snapshot on the main proc. It reads the halo catalog and
 * sorts the catalog data sets according to the connection requests. When reading a halo, there are
 * a number of possibilities:
 *
 * 1) This is a new halo (it has num_prog = 0). Add it, proc to be determined later.
 * 2) This halo existed before (num_prog > 0), and has been requested. Set the recepient proc to
 *    be the proc that requested the halo.
 * 3) This halo existed before (num_prog > 0), but has not been requested. This should not happen
 *    very often, but sometimes connections are lost (e.g. if a halo jumps unphysically), meaning
 *    a "new" halo pops up. Again, we add it, and leave the proc to be determined later.
 *
 * This function does not return sub-subhalos, i.e. the host is changed to the host-host in such
 * cases.
 */
void readHaloCatalog(int snap_idx, HaloRequest *requests, int n_requests, DynArray *da_cd)
{
	int i;
	HaloCatalogData *cdp, temp_cd, *sub, *host, *hosthost;

	/*
	 * Read the catalog. This depends on the catalog type.
	 */
	switch (config.cat_type)
	{
	case CAT_TYPE_ROCKSTAR:
		readHaloCatalogRockstar(snap_idx, requests, n_requests, da_cd);
		break;
	default:
		error(__FFL__, "[Main] No routine to read catalog file for catalog type %d.\n",
				config.cat_type);
		break;
	}

	/*
	 * We now perform checks that are general regardless of the catalog format.
	 *
	 * Check for sub-sub halos. If the host is itself a sub, we assign it the next higher host.
	 * Note that the first step could be done by simply comparing pid and upid, but we do not have
	 * desc_upid. It is important to anticipate whether the sub-sub status persists, as otherwise
	 * we might fool ourselves later into thinking that the sub will switch host.
	 */
	qsort(da_cd->cd, da_cd->n, sizeof(HaloCatalogData), &compareHaloCatalogDataByID);
	for (i = 0; i < da_cd->n; i++)
	{
		sub = &(da_cd->cd[i]);

		if (sub->pid == -1)
			continue;

		temp_cd.id = sub->pid;
		host = (HaloCatalogData*) bsearch(&temp_cd, da_cd->cd, da_cd->n, sizeof(HaloCatalogData),
				&compareHaloCatalogDataByID);

		if (host == NULL)
			error(__FFL__, "[Main] Could not find host ID %ld for sub %ld in catalog data.\n",
					sub->pid, sub->id);

		while (host->pid != -1)
		{
			temp_cd.id = host->pid;
			hosthost = (HaloCatalogData*) bsearch(&temp_cd, da_cd->cd, da_cd->n,
					sizeof(HaloCatalogData), &compareHaloCatalogDataByID);
			if (hosthost == NULL)
				error(__FFL__, "[Main] Could not find host ID %ld for sub %ld in catalog data.\n",
						host->pid, host->id);

			sub->pid = hosthost->id;
			if ((sub->desc_pid != -1) && (sub->desc_pid == host->desc_id)
					&& (host->desc_pid == hosthost->desc_id))
			{
				sub->desc_pid = hosthost->desc_id;
			}
			debugHaloCatalogData(1, sub, "Correcting pid to %ld (desc_id %ld, desc_pid %ld).",
					sub->pid, sub->desc_id, sub->desc_pid);
			host = hosthost;
		}
	}

	/*
	 * Check if any requests remained unanswered. If so, we can either abort or cancel the halo.
	 */
	for (i = 0; i < n_requests; i++)
	{
		if (requests[i].status == HALO_REQUEST_STATUS_OPEN)
		{
			warningOrError(__FFL__, config.err_level_halo_req_not_found,
					"[Main] Requested halo ID %ld from process %d could not be found. Aborting halo.\n",
					requests[i].id, requests[i].proc);

			cdp = addHaloCatalogData(__FFL__, da_cd);
			cdp->id = requests[i].id;
			cdp->proc = requests[i].proc;
			cdp->request_status = HALO_REQUEST_STATUS_NOT_FOUND;
			requests[i].status = HALO_REQUEST_STATUS_CLOSED;
		}
	}
}

/*
 * This function should only be called once in the very beginning. Given a list of snapshot IDs and
 * redshifts, find the matching catalog files. This depends on the catalog format. The following
 * fields must be set:
 *
 * config.n_snaps
 * config.snap_id
 * config.snap_a
 * config.snap_z
 */
void matchCatalogsToSnapshots(int n_snaps_tot, int *snapshot_id, float *snapshot_a)
{
	switch (config.cat_type)
	{
	case CAT_TYPE_ROCKSTAR:
		matchCatalogsToSnapshotsRockstar(n_snaps_tot, snapshot_id, snapshot_a);
		break;
	default:
		error(__FFL__, "[Main] No routine to assign catalog files for catalog type %d.\n",
				config.cat_type);
		break;
	}
}
