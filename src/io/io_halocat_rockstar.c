/*************************************************************************************************
 *
 * This unit contains functions related to Rockstar halo catalogs.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>

#include "io_halocat_rockstar.h"
#include "../memory.h"
#include "../utils.h"
#include "../halos/halo.h"
#include "../halos/halo_definitions.h"
#include "../halos/halo_so.h"

/*************************************************************************************************
 * INTERNAL CONSTANTS
 *************************************************************************************************/

#define MAX_LINE 30000
#define ALLOC_FACTOR_CD_REQUESTS 1.2

/*
 * Catalog fields
 */
#define ENUM(x, y, z) x,
enum
{
	CATALOG_FIELDS_ROCKSTAR/*space*/N_CATALOG_FIELDS_ROCKSTAR
};
#undef ENUM

#define ENUM(x, y, z) #y,
const char *CATALOG_FIELD_NAMES_ROCKSTAR[N_CATALOG_FIELDS_ROCKSTAR] =
	{CATALOG_FIELDS_ROCKSTAR};
#undef ENUM

#define ENUM(x, y, z) #z,
const char *CATALOG_FIELD_DEFAULTS_ROCKSTAR[N_CATALOG_FIELDS_ROCKSTAR] =
	{CATALOG_FIELDS_ROCKSTAR};
#undef ENUM

/*
 * In some rare cases the halo finder cannot determine R200m at all, and we have to guess it from
 * Rvir. The guess below is a very conservative estimate to make sure that the search radius used
 * includes the actual R200m.
 */
#define R200M_OVER_RVIR_GUESS 2.0

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int isCatalogFileRockstar(const char *filename);
float getAFromCatalogRockstar(const char *filename);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

int getNCatFieldsRockstar()
{
	return N_CATALOG_FIELDS_ROCKSTAR;
}

char** getCatFieldNamesRockstar()
{
	return (char**) CATALOG_FIELD_NAMES_ROCKSTAR;
}

char** getCatFieldDefaultsRockstar()
{
	return (char**) CATALOG_FIELD_DEFAULTS_ROCKSTAR;
}

/*
 * Finding a Rockstar catalog name is a little tricky because the number of significant digits
 * depends on the software version. We try all known formats and throw an error if none of those
 * files exist.
 *
 * Moreover, when using the 5-digit versions, small numerical errors can shift the number a tiny
 * bit, so we try one digit above and one below.
 */
void getCatalogFileNameRockstar(float a, char *str)
{
	FILE *f;

	sprintf(str, "%s/hlist_%.4f0.list", config.cat_path, a);
	f = fopen(str, "r");
	if (f != NULL)
	{
		fclose(f);
		return;
	}

	sprintf(str, "%s/hlist_%.5f.list", config.cat_path, a);
	f = fopen(str, "r");
	if (f != NULL)
	{
		fclose(f);
		return;
	}

	warningOrError(__FFL__, config.err_level_rockstar_filename,
			"Cannot find catalog file hlist_%.5f.list; trying hlist_%.5f.list and hlist_%.5f.list.\n",
			a, a + 1E-5, a - 1E-5);

	sprintf(str, "%s/hlist_%.5f.list", config.cat_path, a - 1E-5);
	f = fopen(str, "r");
	if (f != NULL)
	{
		fclose(f);
		return;
	}

	sprintf(str, "%s/hlist_%.5f.list", config.cat_path, a + 1E-5);
	f = fopen(str, "r");
	if (f != NULL)
	{
		fclose(f);
		return;
	}

	error(__FFL__,
			"Could not find Rockstar catalog file for a = %.5f in directory %s (or file opening failed for another reason).\n",
			a, config.cat_path);
}

void getSnapCatalogFilename(int snap_idx, char *str)
{
	getCatalogFileNameRockstar(config.snap_a[snap_idx], str);
}

/*
 * This function is specific to the Rockstar file format, but there should always be a way to
 * find out the redshift of a catalog file.
 */
float getAFromCatalogRockstar(const char *filename)
{
	int i, l;
	char astr[7];
	float a;

	l = strlen(filename);
	for (i = 0; i < 7; i++)
		astr[i] = filename[l - 12 + i];
	a = atof(astr);

	return a;
}

int isCatalogFileRockstar(const char *filename)
{
	int i, l, is_cat;
	char s[6];

	l = strlen(filename);
	if (l < 6)
		return 0;
	for (i = 0; i < 5; i++)
		s[i] = filename[l - 5 + i];
	s[5] = '\0';
	is_cat = (strcmp(s, ".list") == 0);

	return is_cat;
}

/*
 * Output a string that identifies a definition in a Rockstar catalog. This is tricky and somewhat
 * ambiguous because the catalog header is not clear as to whether definitions are bound-only.
 *
 * If the catalog was run with STRICT_SO_MASSES and BOUND_PROPS, then the main definition has M_bnd,
 * R_bnd, and M_all. All other definitions will have only M_all.
 *
 * If STRICT_SO_MASSES is off, then all definitions will be _bnd (even if labeled otherwise).
 */
int haloDefinitionToRockstar(HaloDefinition def, HaloDefinition main_def, int is_strict_so,
		int is_bound_props, char *str)
{
	int status, is_main_def, use_main_def;
	char def_str[DEFAULT_HALO_DEF_STRLEN];

	/*
	 * Create string for error output
	 */
	haloDefinitionToString(def, def_str);
	sprintf(str, "%s", "");

	/*
	 * Check erroneous input
	 */
	if (def.is_error != HDEF_ERR_NO)
		error(__FFL__, "Cannot convert halo definition %s to Rockstar field (is error).\n",
				def_str);

	if ((def.type != HDEF_TYPE_VMAX) && (def.type != HDEF_TYPE_SO))
		error(__FFL__, "Can convert only SO and Vmax definitions to Rockstar, not %s.\n", def_str);

	/*
	 * Find out whether we're dealing with the main definition. This will have an impact on the
	 * status. Note that even if this has the same SO threshold as the main def, it doesn't mean we
	 * can use it.
	 */
	status = ROCKSTAR_CONVERT_STATUS_NONE;
	is_main_def = (compareSOThresholds(def, main_def) == 0);
	if (is_bound_props)
		use_main_def = ((is_main_def) && (def.ptl_select == HDEF_PTLSEL_BOUND)
				&& (def.time == HDEF_TIME_NOW));
	else
		use_main_def = ((is_main_def) && (def.ptl_select == HDEF_PTLSEL_ALL)
				&& (def.time == HDEF_TIME_NOW));

	/*
	 * Unless we are dealing with the main def, we have only either bound-only or all-particle
	 * masses.
	 */
	if (!use_main_def)
	{
		if (is_strict_so && (def.ptl_select == HDEF_PTLSEL_BOUND))
			error(__FFL__,
					"Definition %s is bound-only, but Rockstar catalog is strict-so and not bound-props.\n",
					def_str);
		if (!is_strict_so && (def.ptl_select == HDEF_PTLSEL_ALL))
			error(__FFL__,
					"Definition %s is all-particles, but Rockstar catalog is not strict-so.\n",
					def_str);
	}

	switch (def.quantity)
	{
	case HDEF_Q_RADIUS:
		/*
		 * The user has requested a radius, but we only have masses in Rockstar for most defs. If
		 * this is the main def, then we have the radius, but only for bound-only.
		 */
		if (use_main_def)
		{
			sprintf(str, "%s", "R");
		} else
		{
			sprintf(str, "%s", "M");
			status = ROCKSTAR_CONVERT_STATUS_M_TO_R;
		}
		break;
	case HDEF_Q_MASS:
		sprintf(str, "%s", "M");
		if (def.time == HDEF_TIME_ACC)
			sprintf(str, "%s%s", str, "acc");
		else if (def.time == HDEF_TIME_PEAK)
			sprintf(str, "%s%s", str, "peak");
		break;
	case HDEF_Q_VCIRC:
		if (def.type != HDEF_TYPE_VMAX)
			error(__FFL__,
					"Cannot convert halo definition %s to Rockstar field (vcirc vs. vmax).\n",
					def_str);
		if (def.time == HDEF_TIME_NOW)
			sprintf(str, "%s", "vmax");
		else if (def.time == HDEF_TIME_ACC)
			sprintf(str, "%s", "Vacc");
		else if (def.time == HDEF_TIME_PEAK)
			sprintf(str, "%s", "Vpeak");
		break;
	default:
		error(__FFL__, "Cannot convert halo definition %s to Rockstar field (invalid quantity).\n",
				def_str);
		break;
	}

	if ((def.time == HDEF_TIME_NOW) && (def.type != HDEF_TYPE_VMAX))
	{
		switch (def.subtype)
		{
		case HDEF_SUBTYPE_SO_MATTER:
			sprintf(str, "%s%.0fb", str, def.overdensity);
			break;
		case HDEF_SUBTYPE_SO_CRITICAL:
			sprintf(str, "%s%.0fc", str, def.overdensity);
			break;
		case HDEF_SUBTYPE_SO_VIRIAL:
			sprintf(str, "%svir", str);
			break;
		default:
			error(__FFL__, "Cannot convert halo definition %s to Rockstar field (type).\n",
					def_str);
			break;
		}

		if ((def.ptl_select == HDEF_PTLSEL_ALL) && is_main_def)
			sprintf(str, "%s_all", str);
	}

	return status;
}

/*
 * Go through snaps, see which ones have halos. The main principle is that any catalog file MUST
 * be matched by a snapshot file, but not vice versa: a catalog file cannot be omitted without
 * breaking halo histories, but the catalog / merger tree could have jumped one or more snapshot
 * files.
 *
 * We open the catalog directory, find first file that has halos. Ignore files that are not
 * catalogs. Then go through catalog list and match to a list of halos. Store the results in the
 * config structure.
 *
 * Note that this routine is pretty unique to Rockstar, because the .hlist files are named
 * according to their redshift, not their snapshot number. This makes the matching much harder.
 */
void matchCatalogsToSnapshotsRockstar(int n_snaps_tot, int *snapshot_id, float *snapshot_a)
{
	int i, has_halos, n_cat_tot, first_snap_idx, first_cat_idx, snap_idx, i_cat, i_snap, i_done;
	float a_first, a_snap, a_cat;
	char filename[300], line[40];
	FILE *f_cat;
	struct dirent **cat_files;

	n_cat_tot = scandir(config.cat_path, &cat_files, 0, alphasort);
	if (n_cat_tot < 0)
		error(__FFL__, "[Main] Could not open catalog directory %s.\n", config.cat_path);
	i = 0;
	has_halos = 0;
	while ((i < n_cat_tot) && (!has_halos))
	{
		if (isCatalogFileRockstar(cat_files[i]->d_name))
		{
			sprintf(filename, "%s/%s", config.cat_path, cat_files[i]->d_name);
			f_cat = fopen(filename, "r");
			if (f_cat == NULL)
				error(__FFL__, "[Main] Could not open catalog file %s.\n", filename);
			while (fgets(line, 40, f_cat) != NULL)
			{
				if (line[0] != '#')
				{
					has_halos = 1;
					break;
				}
			}
			fclose(f_cat);
		}
		i++;
	}
	if (!has_halos)
		error(__FFL__, "Found %d files in catalog directory, but no valid catalogs with halos.\n",
				n_cat_tot);
	first_cat_idx = i - 1;

	/*
	 * Find out which snapshot the first catalog with halos belongs to. The -2 in the print
	 * statement comes from the . and .. directory entries.
	 */
	a_first = getAFromCatalogRockstar(filename);
	output(5, "[Main] Cat file %s, a %.4f\n", filename, a_first);
	first_snap_idx = closestInArray(snapshot_a, n_snaps_tot, a_first);
	output(5, "[Main] First catalog file with halos is %d, a %.4f\n", first_cat_idx - 2, a_first);

	/*
	 * Go through list of all catalog files. Make sure their redshift matches the snapshot redshift
	 * to a given accuracy. Check that we're not exceeding the max. number of snapshots.
	 */
	i_snap = 0;
	i_done = 0;
	i_cat = first_cat_idx;
	while (i_cat < n_cat_tot)
	{
		if (!isCatalogFileRockstar(cat_files[i_cat]->d_name))
		{
			i_cat++;
			continue;
		}

		snap_idx = first_snap_idx + i_snap;

		/*
		 * First chance to stop here: the user might have requested not to go past a certain
		 * snapshot number (in catalog snapshots)
		 */
		if ((config.last_snap >= 0) && ((i_cat - first_cat_idx) >= config.last_snap))
			break;

		if (i_snap >= MAX_SNAPS)
			error(__FFL__,
					"[Main] Found too many snapshots (%d), maximum is %d. Please increase the MAX_SNAPS value.\n",
					i_snap + 1, MAX_SNAPS);

		if (snap_idx >= n_snaps_tot)
			error(__FFL__, "[Main] Catalog files (%d) run past last snapshot (%d).\n", i_snap,
					n_snaps_tot);

		sprintf(filename, "%s/%s", config.cat_path, cat_files[i_cat]->d_name);
		a_snap = snapshot_a[snap_idx];
		a_cat = getAFromCatalogRockstar(filename);

		/*
		 * Second chance to drop out: the user might have specified not to go past a certain
		 * redshift
		 */
		if ((config.last_scale_factor >= 0) && (a_cat > config.last_scale_factor))
			break;

		if (fabs(a_snap - a_cat) <= config.cat_tolerance_a_snap)
		{
			config.snap_id[i_done] = snapshot_id[snap_idx];
			config.snap_a[i_done] = a_cat;
			config.snap_z[i_done] = 1.0 / a_cat - 1.0;
			output(5,
					"[Main] Matched catalog file %d, a = %.5f, to snapshot number %d, a = %.5f.\n",
					i_done, a_cat, config.snap_id[i_done], a_snap);
			i_cat++;
			i_done++;
		} else
		{
			warningOrError(__FFL__, config.err_level_skipping_snap,
					"[Main] Skipping snapshot %4d\n", snap_idx);
			if (a_snap > a_cat + config.cat_tolerance_a_snap)
				error(__FFL__,
						"[Main] Could not find matching snapshot for the redshift of catalog file %s (%.4f).\n",
						cat_files[i_cat]->d_name, a_cat, snap_idx);
		}
		i_snap++;
	}
	config.n_snaps = i_done;
	config.cat_n_chunks = 1;

	/*
	 * Final warnings and errors: are there more snapshots than catalog files?
	 */
	if ((config.last_snap < 0) && (config.last_scale_factor < 0.0)
			&& (first_snap_idx + i_snap + 1 < n_snaps_tot))
		warningOrError(__FFL__, config.err_level_cat_snap_mismatch,
				"[Main] Found catalog files only up to snapshot %d out of %d.\n",
				first_snap_idx + i_snap + 1, n_snaps_tot);

	/*
	 * Free memory
	 */
	for (i = 0; i < n_cat_tot; i++)
		memFree(__FFL__, MID_IGNORE, cat_files[i], sizeof(struct dirent));
	memFree(__FFL__, MID_IGNORE, cat_files, sizeof(struct dirent*) * n_cat_tot);
}

/*
 * Find all the necessary fields in the halo catalog header line and record their positions. The
 * output can give two different numbers: the position in the array generated by the scanff
 * function with parse_str (field_order), or the index of the field in the Rockstar column
 * ordering (field_idxs). If field_idxs is NULL, the latter is not output.
 *
 * The function allows a name to appear multiple times in cat_fields.
 */
void parseCatalogHeaderRockstar(char *line, char cat_fields[][CAT_FIELD_LENGTH], int n_cat_fields,
		char *parse_str, int **field_order, int **field_idxs)
{
	int i, pos, idx, found, field_len;
	char *cp, field_name[100];

	for (i = 0; i < n_cat_fields; i++)
	{
		(*field_order)[i] = -1;
		if (field_idxs != NULL)
			(*field_idxs)[i] = -1;
	}

	sprintf(parse_str, "%s", "");
	cp = strsep(&line, "#");
	pos = 0;
	idx = 0;
	while (cp != NULL)
	{
		/*
		 * Find next space-separated field in string
		 */
		cp = strsep(&line, " ");
		if (cp == NULL)
			break;

		/*
		 * Find LAST occurrence of open bracket, which indicates the counter number (there can be
		 * brackets within the name). This is hard to do with strsep, better to just search manually
		 * starting at the end.
		 */
		field_len = strlen(cp);
		i = field_len - 1;
		while (i > 0)
		{
			if (cp[i] == '(')
				break;
			i--;
		}
		if (cp[i] != '(')
			error(__FFL__, "Could not find number (x) after field %s.\n", cp);
		memcpy(field_name, cp, i);
		field_name[i] = '\0';

		/*
		 * Now go through the fields we are looking for and see if they match.
		 */
		found = 0;
		for (i = 0; i < n_cat_fields; i++)
		{
			/*
			 * Don't break when we've found this name in the cat fields, because it might appear
			 * multiple times. We run through all cat fields and only if we have found the string
			 * at least once we increase the position counter.
			 */
			if (strcmp(field_name, cat_fields[i]) == 0)
			{
				(*field_order)[i] = pos;
				if (field_idxs != NULL)
					(*field_idxs)[i] = idx;
				found = 1;
			}
		}

		/*
		 * For each field, we must either extract it or not; even if there are multiple matches, we
		 * add only one %lf.
		 */
		if (found)
		{
			sprintf(parse_str, "%s%%lf ", parse_str);
			pos++;
		} else
		{
			sprintf(parse_str, "%s%%*lf ", parse_str);
		}

		idx++;
	}

	for (i = 0; i < n_cat_fields; i++)
		if ((*field_order)[i] == -1)
			error(__FFL__, "Could not find catalog field %s in header.\n", cat_fields[i]);
}

/*
 * Set halo properties from catalog elements. This demands some cosmological info, mass conversion
 * and such. For example, the virial radii in the catalog are in comoving kpc/h.
 *
 * Furthermore, we convert M200m to get R200m. In some rare cases, when halos are small and R200m
 * is smaller than the main mass definition of the catalog (generally Rvir), the halo finder can
 * fail to identify M200m / R200m. In that case, we make a crude guess that R200m = 1.5 Rvir. This
 * is not accurate at all, but does not matter much since the catalog R200m is only used as a
 * guess before computing R200m properly from the particle distribution.
 */
void setHaloData(int snap_idx, HaloCatalogData *cd, int proc, double *values)
{
	cd->proc = proc;
	cd->request_status = HALO_REQUEST_STATUS_FOUND;

	cd->id = (HaloID) values[CAT_ID];
	cd->desc_id = (HaloID) values[CAT_DESC_ID];
	cd->pid = (HaloID) values[CAT_PID];
	cd->desc_pid = (HaloID) values[CAT_DESC_PID];
	cd->mmp = values[CAT_MMP];
	cd->phantom = values[CAT_PHANTOM];
	cd->x[0] = values[CAT_X];
	cd->x[1] = values[CAT_Y];
	cd->x[2] = values[CAT_Z];
	cd->v[0] = values[CAT_VX];
	cd->v[1] = values[CAT_VY];
	cd->v[2] = values[CAT_VZ];
	cd->M_bound = values[CAT_MBOUND];

	/*
	 * Convert R200m to units of comoving Mpc/h (from comoving kpc/h in the Rockstar catalogs)
	 */
	if (values[CAT_M200M_ALL] <= config.particle_mass)
	{
		cd->R200m_cat_com = values[CAT_RBOUND] / 1000.0 * R200M_OVER_RVIR_GUESS;
#if !DO_MASS_PROFILE
		error(__FFL__,
				"An invalid M200m (%.3e) was found in the catalog, and the mass profile is not being computed.\n",
				values[CAT_M200M_ALL]);
#endif
	} else
	{
		cd->R200m_cat_com = comovingRadius(soMtoR(values[CAT_M200M_ALL], config.this_snap_rho_200m),
				snap_idx);
	}

#if PARANOID
	int i;
	for (i = 0; i < 3; i++)
	{
		if ((cd->x[i] < 0.0) || (cd->x[i] > config.box_size))
			error(__FFL__, "Found x = %.4e, not within box.\n", cd->x[i]);
	}
#endif
}

/*
 * This function is executed once per snapshot on the main proc. It reads the halo catalog and
 * sorts the catalog data sets according to the connection requests. When reading a halo, there are
 * a number of possibilities:
 *
 * 1) This is a new halo (it has num_prog = 0). Add it, proc to be determined later.
 * 2) This halo existed before (num_prog > 0), and has been requested. Set the recepient proc to
 *    be the proc that requested the halo.
 * 3) This halo existed before (num_prog > 0), but has not been requested. This should not happen
 *    very often, but sometimes connections are lost (e.g. if a halo jumps unphysically), meaning
 *    a "new" halo pops up. Again, we add it, and leave the proc to be determined later.
 *
 * This function does not return sub-subhalos, i.e. the host is changed to the host-host in such
 * cases.
 *
 * The incoming requests contain a proc_id that we assign to the outgoing halos as well. If a halo
 * is new, its proc_id will be assigned depending on its position (in a different function).
 */
void readHaloCatalogRockstar(int snap_idx, HaloRequest *requests, int n_requests, DynArray *da_cd)
{
	int i, halo_proc, n_reqs_found, n_copies, hr_idx, *field_order, temp_num_prog;
	double tmp[N_CATALOG_FIELDS_ROCKSTAR], values[N_CATALOG_FIELDS_ROCKSTAR];
	char filename[300], line[MAX_LINE], parse_str[3000];
	FILE *f_cat;
	HaloID temp_id;
	HaloRequest *hr, temp_req, *hrp;
	HaloCatalogData *cdp;

	/*
	 * Find catalog file
	 */
	getSnapCatalogFilename(snap_idx, filename);
	f_cat = fopen(filename, "r");
	if (f_cat == NULL)
		error(__FFL__, "[Main] Catalog file %s not found.\n", filename);
	output(2, "[Main] [SN %3d] Reading catalog file %s.\n", snap_idx, filename);

	/*
	 * Find fields, create the parsing string for the scanff function
	 */
	field_order = (int*) memAlloc(__FFL__, MID_CATREADING, N_CATALOG_FIELDS_ROCKSTAR * sizeof(int));
	if (fgets(line, MAX_LINE, f_cat) != NULL)
	{
		parseCatalogHeaderRockstar(line, config.cat_fields, N_CATALOG_FIELDS_ROCKSTAR, parse_str,
				&field_order, NULL);
	} else
	{
		error(__FFL__, "Catalog file is empty.\n");
	}

	/*
	 * Prepare for reading: sort halo requests by ID. We allocate more halo data than requests
	 * because there may also be new halos.
	 */
	initDynArrayN(__FFL__, da_cd, DA_TYPE_CD, n_requests * ALLOC_FACTOR_CD_REQUESTS);
	qsort(requests, n_requests, sizeof(HaloRequest), &compareHaloRequests);

	/*
	 * Go through lines of catalog file
	 */
	while (fgets(line, MAX_LINE, f_cat) != NULL)
	{
		if (line[0] == '#')
			continue;

		sscanf(line, parse_str, &(tmp[0]), &(tmp[1]), &(tmp[2]), &(tmp[3]), &(tmp[4]), &(tmp[5]),
				&(tmp[6]), &(tmp[7]), &(tmp[8]), &(tmp[9]), &(tmp[10]), &(tmp[11]), &(tmp[12]),
				&(tmp[13]), &(tmp[14]), &(tmp[15]), &(tmp[16]));
		for (i = 0; i < N_CATALOG_FIELDS_ROCKSTAR; i++)
			values[i] = tmp[field_order[i]];
		temp_num_prog = (int) values[CAT_NUM_PROG];
		temp_id = (HaloID) values[CAT_ID];

		/*
		 * If num_proc == 0, just leave the proc to be determined later. Otherwise, check if there
		 * is a request for this halo, or even multiple requests. Because of the latter possibility,
		 * we have to be careful: bsearch can return any of the matching requests. We thus make
		 * sure to go to the first request and then count up.
		 */
		halo_proc = INVALID_PROC;
		n_reqs_found = 0;
		hr = NULL;
		hr_idx = 0;
		if (temp_num_prog > 0)
		{
			temp_req.id = temp_id;
			hr = (HaloRequest*) bsearch(&temp_req, requests, n_requests, sizeof(HaloRequest),
					&compareHaloRequests);

			if (hr != NULL)
			{
				hr_idx = hr - requests;
				while ((hr_idx > 0) && (requests[hr_idx - 1].id == temp_id))
					hr_idx--;
				while ((hr_idx + n_reqs_found < n_requests)
						&& (requests[hr_idx + n_reqs_found].id == temp_id))
					n_reqs_found++;
			} else
			{
				warningOrError(__FFL__, config.err_level_found_unexpected,
						"[Main] Found existing halo that was not requested, ID %ld.\n", temp_id);
			}
		}

		/*
		 * Add this catalog dataset, multiple times if necessary.
		 */
		n_copies = imax(1, n_reqs_found);
		for (i = 0; i < n_copies; i++)
		{
			if (n_reqs_found > 0)
			{
				hrp = &(requests[hr_idx + i]);
				hrp->status = HALO_REQUEST_STATUS_CLOSED;
				halo_proc = hrp->proc;
			}
			cdp = addHaloCatalogData(__FFL__, da_cd);
			setHaloData(snap_idx, cdp, halo_proc, values);
		}

		/*
		 * Here we cannot use the usual debugHalo() macro because we have no halo object to pass.
		 */
#if DEBUG_HALO
		HaloID temp_desc_id, temp_pid, temp_desc_pid, temp_mmp;

		temp_desc_id = (HaloID) values[CAT_DESC_ID];
		temp_pid = (HaloID) values[CAT_PID];
		temp_desc_pid = (HaloID) values[CAT_DESC_PID];
		temp_mmp = (HaloID) values[CAT_MMP];

		if (temp_id == DEBUG_HALO)
			output(0,
					"[Main] *** Halo ID %ld (pid %ld, desc_id %ld, desc_pid %ld, num_prog %d, mmp %d): Found in catalog. Sending to process %d.\n",
					DEBUG_HALO, temp_pid, temp_desc_id, temp_desc_pid, temp_num_prog, temp_mmp,
					halo_proc);
		if (temp_desc_id == DEBUG_HALO)
			output(0,
					"[Main] *** Progenitor ID %ld (pid %ld, id %ld, desc_id %ld, desc_pid %ld, num_prog %d, mmp %d): Found progenitor in catalog. Sending to process %d.\n",
					temp_id, temp_pid, temp_id, temp_desc_id, temp_desc_pid, temp_num_prog,
					temp_mmp, halo_proc);
		if (temp_pid == DEBUG_HALO)
			output(0,
					"[Main] *** Subhalo ID    %ld (pid %ld, id %ld, desc_id %ld, desc_pid %ld, num_prog %d, mmp %d): Found subhalo in catalog. Sending to process %d.\n",
					temp_id, temp_pid, temp_id, temp_desc_id, temp_desc_pid, temp_num_prog,
					temp_mmp, halo_proc);
		if (temp_desc_pid == DEBUG_HALO)
			output(0,
					"[Main] *** Sub-prog ID   %ld (pid %ld, id %ld, desc_id %ld, desc_pid %ld, num_prog %d, mmp %d): Found future subhalo in catalog. Sending to process %d.\n",
					temp_id, temp_pid, temp_id, temp_desc_id, temp_desc_pid, temp_num_prog,
					temp_mmp, halo_proc);
#endif
	}

	memFree(__FFL__, MID_CATREADING, field_order, N_CATALOG_FIELDS_ROCKSTAR * sizeof(int));
	fclose(f_cat);
}
