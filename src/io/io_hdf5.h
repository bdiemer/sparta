/*************************************************************************************************
 *
 * This unit contains HDF5 utilities that are not specific to SPARTA.
 *
 * Many of the functions in this unit can take any data type as indicated by the dtype parameter.
 * This parameter can be any of the following, and will be mapped to these HDF5 data types:
 *
 * DTYPE_INT8   -> H5T_NATIVE_INT_LEAST8
 * DTYPE_INT16  -> H5T_NATIVE_INT16
 * DTYPE_INT32  -> H5T_NATIVE_INT
 * DTYPE_INT64  -> H5T_NATIVE_LONG
 * DTYPE_FLOAT  -> H5T_NATIVE_FLOAT
 * DTYPE_DOUBLE -> H5T_NATIVE_DOUBLE
 *
 * This library automatically compresses all datasets using zlib.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _IO_HDF5_H_
#define _IO_HDF5_H_

#include <hdf5.h>

#include "../global_types.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * This chunk size is used in all functions where the user does not explicitly specify a chunk
 * size. It is large enough so that small datasets are essentially stored contiguously.
 */
#define HDF5_DEFAULT_CHUNK_SIZE 10000

/*
 * The maximum length an attribute name is allowed to have
 */
#define HDF5_ATTRIBUTE_NAME_MAXLEN 100

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

/*
 * General type to encapsulate a HDF5 attribute. For simple int and float values, we store them
 * directly. For int/float 1D arrays and strings, we allocate to a pointer.
 */
typedef struct
{
	char name[HDF5_ATTRIBUTE_NAME_MAXLEN];
	int dtype;
	int n_alloc;
	union
	{
		int d_int;
		float d_float;
	};
	union
	{
		void *p_void;
		int *p_int;
		float *p_float;
		char *p_str;
	};
} Hdf5Attribute;

/*************************************************************************************************
 * FUNCTIONS - GENERAL
 *************************************************************************************************/

void hdf5CheckForError(const char *file, const char *func, int line, hid_t value,
		const char *format, ...);
void hdf5CheckForErrorOpen(const char *file, const char *func, int line, hid_t loc_id, hid_t value,
		const char *name, hid_t type);
void hdf5CheckForErrorWrite(const char *file, const char *func, int line, hid_t loc_id, hid_t value,
		const char *name, hid_t type);

hid_t hdf5Datatype(int dtype);
hid_t hdf5DatatypeFromSparta(int dtype);

void hdf5ListDatasetsInGroup(hid_t group_id);
int hdf5GroupExists(hid_t obj_id, char *group_name);
int hdf5ObjectExists(hid_t obj_id, char *ds_name);
void hdf5GetDatasetDimensions(hid_t loc_id, const char *name, int *dim1);
void hdf5GetDatasetDimensions2D(hid_t loc_id, const char *name, int *dim1, int *dim2);

int hdf5SafeName(char *name_in, char *name_out);

/*************************************************************************************************
 * FUNCTIONS - DATASPACES
 *************************************************************************************************/

hid_t hdf5Dataspace1DFixed(hsize_t N);
hid_t hdf5Dataspace2DFixed(hsize_t N1, hsize_t N2);
hid_t hdf5Dataspace3DFixed(hsize_t N1, hsize_t N2, hsize_t N3);
hid_t hdf5DataspaceNDFixed(int n_dims, hsize_t *N);

hid_t hdf5Dataspace1DVar(hsize_t N, hsize_t N1max);
hid_t hdf5Dataspace2DVar(hsize_t N1, hsize_t N1max, hsize_t N2, hsize_t N2max);
hid_t hdf5Dataspace3DVar(hsize_t N1, hsize_t N1max, hsize_t N2, hsize_t N2max, hsize_t N3,
		hsize_t N3max);
hid_t hdf5DataspaceNDVar(int n_dims, hsize_t *N, hsize_t *Nmax);

/*************************************************************************************************
 * FUNCTIONS - DATASETS
 *************************************************************************************************/

hid_t hdf5DatasetFixed(hid_t loc_id, hid_t dsp, const char *name, int dtype);

hid_t hdf5Dataset1DVar(hid_t loc_id, hid_t dsp, const char *name, int dtype, hsize_t chunk_size);
hid_t hdf5Dataset2DVar(hid_t loc_id, hid_t dsp, const char *name, int dtype, hsize_t chunk_size_1,
		hsize_t chunk_size_2);
hid_t hdf5Dataset3DVar(hid_t loc_id, hid_t dsp, const char *name, int dtype, hsize_t chunk_size_1,
		hsize_t chunk_size_2, hsize_t chunk_size_3);
hid_t hdf5DatasetNDVar(int n_dims, hid_t loc_id, hid_t dsp, const char *name, int dtype,
		hsize_t *chunk_size);

void hdf5Dataset1DResize(hid_t loc_id, hsize_t N);
void hdf5Dataset2DResize(hid_t loc_id, hsize_t N1, hsize_t N2);
void hdf5Dataset3DResize(hid_t loc_id, hsize_t N1, hsize_t N2, hsize_t N3);
void hdf5DatasetNDResize(hid_t loc_id, hsize_t *N);

/*************************************************************************************************
 * FUNCTIONS - WRITE/READ ATTRIBUTES
 *************************************************************************************************/

void hdf5WriteAttributeInt(hid_t loc_id, const char *name, int value);
void hdf5WriteAttributeLong(hid_t loc_id, const char *name, long int value);
void hdf5WriteAttributeFloat(hid_t loc_id, const char *name, float value);
void hdf5WriteAttributeDouble(hid_t loc_id, const char *name, double value);
void hdf5WriteAttributeString(hid_t loc_id, const char *name, char *value);

void hdf5WriteAttributeInt1D(hid_t loc_id, const char *name, int N, int *values);
void hdf5WriteAttributeFloat1D(hid_t loc_id, const char *name, int N, float *values);
void hdf5WriteAttributeDouble1D(hid_t loc_id, const char *name, int N, double *values);

void hdf5ReadAttributeInt(hid_t loc_id, const char *name, int *value);
void hdf5ReadAttributeLong(hid_t loc_id, const char *name, long int *value);
void hdf5ReadAttributeFloat(hid_t loc_id, const char *name, float *value);
void hdf5ReadAttributeDouble(hid_t loc_id, const char *name, double *value);
void hdf5ReadAttributeString(hid_t loc_id, const char *name, int *N, char **data, int mem_id);

void hdf5ReadAttributeInt1D(hid_t loc_id, const char *name, int *N, int **data, int mem_id);
void hdf5ReadAttributeLong1D(hid_t loc_id, const char *name, int *N, long int **data, int mem_id);
void hdf5ReadAttributeFloat1D(hid_t loc_id, const char *name, int *N, float **data, int mem_id);
void hdf5ReadAttributeDouble1D(hid_t loc_id, const char *name, int *N, double **data, int mem_id);

void hdf5InitializeAttribute(Hdf5Attribute *attr);
void hdf5FreeAttribute(Hdf5Attribute *attr, int mem_id);
int hdf5ReadAllAttributes(hid_t loc_id, Hdf5Attribute *attrs, int n_max, int mem_id);
void hdf5WriteAllAttributes(hid_t loc_id, Hdf5Attribute *attrs, int n);

/*************************************************************************************************
 * FUNCTIONS - WRITE/READ FIXED-SIZE DATASETS
 *************************************************************************************************/

void hdf5WriteDataset(hid_t loc_id, const char *name, int dtype, int count, void *mem);
void hdf5WriteDataset2D(hid_t loc_id, const char *name, int dtype, int count, int count2, void *mem);
void hdf5WriteDataset3D(hid_t loc_id, const char *name, int dtype, int count, int count2,
		int count3, void *mem);

void hdf5WriteDatasetMask(hid_t loc_id, const char *name, int dtype, int count, void *mem,
		int_least8_t *mask, int n_write);

void hdf5ReadDataset(hid_t loc_id, const char *name, int dtype, int *dim1, void **mem, int mem_id);
void hdf5ReadDataset2D(hid_t loc_id, const char *name, int dtype, int *dim1, int *dim2, void ***mem,
		int mem_id);
void hdf5ReadDataset3D(hid_t loc_id, const char *name, int dtype, int *dim1, int *dim2, int *dim3,
		void ****mem, int mem_id);

/*************************************************************************************************
 * FUNCTIONS - WRITE FIXED-SIZE DATASETS FROM STRUCTS
 *************************************************************************************************/

void hdf5WriteStructMember(hid_t loc_id, const char *name, int dtype, int n_tot, int struct_size,
		int struct_offset, void *mem);
void hdf5WriteStructMember2D(hid_t loc_id, const char *name, int dtype, int n_tot, int n2,
		int struct_size, int struct_offset, void *mem);
void hdf5WriteStructMemberMask(hid_t loc_id, const char *name, int dtype, int n_tot,
		int struct_size, int struct_offset, void *mem, int_least8_t *mask, int n_write);
void hdf5WriteStructMember2DMask(hid_t loc_id, const char *name, int dtype, int n_tot, int n2,
		int struct_size, int struct_offset, void *mem, int_least8_t *mask, int n_write);

/*************************************************************************************************
 * FUNCTIONS - WRITE VARIABLE-SIZE DATASETS FROM STRUCTS INCREMENTALLY
 *************************************************************************************************/

void hdf5WriteStructMemberInc(hid_t loc_id, long int file_offset, void *mem, int count,
		int struct_size, int struct_offset, int dtype);
void hdf5WriteStructMemberInc2D(hid_t loc_id, long int file_offset, void *mem, int count,
		int count2, int struct_size, int struct_offset, int dtype);
void hdf5WriteStructMemberIncND(int n_dims, hid_t loc_id, long int file_offset, void *mem,
		hsize_t *dim_count, int struct_size, int struct_offset, int dtype);

/*************************************************************************************************
 * FUNCTIONS - NDARRAYS
 *************************************************************************************************/

void hdf5ReadNDArray(hid_t loc_id, const char *name, NDArray *nda, int mem_id);

void hdf5WriteNDArray(hid_t loc_id, const char *name, NDArray *nda);
void hdf5WriteNDArrayMask(hid_t loc_id, const char *name, NDArray *nda, int_least8_t *mask,
		int n_write);

#endif
