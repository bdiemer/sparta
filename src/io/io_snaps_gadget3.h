/*************************************************************************************************
 *
 * This unit implements the reading of Gadget3 (HDF5) snapshot files.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _IO_SNAPS_GADGET3_H_
#define _IO_SNAPS_GADGET3_H_

#include "../global_types.h"
#include "io_snaps_lgadget.h"

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef SnapshotHeaderLGadget SnapshotHeaderGadget3;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

SnapshotHeaderGadget3 *readSnapshotHeaderGadget3Internal(char *filename);
SnapshotHeader readSnapshotHeaderGadget3(char *filename);
void printGadget3Header(SnapshotHeaderGadget3 *header);
void readParticlesGadget3(char *filename, Particle **particles, int *n_particles, int check_corrupt);

#endif
