/*************************************************************************************************
 *
 * This unit implements cosmology functions.
 *
 * Part of the code was adapted from Matt Becker's CosmoCalc module.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _COSMOLOGY_H_
#define _COSMOLOGY_H_

#include <gsl/gsl_roots.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_spline.h>

/**************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * The minimum and maximum redshifts for which the E(z) routine can be inverted, and generally the
 * limits of redshift used in the cosmology module. Note that not all functions have been carefully
 * tested over the entire redshift range.
 */
#define COSMO_Z_MIN -0.9
#define COSMO_Z_MAX 1E4

/*
 * Parameters for interpolation tables in z, for example D+(z). Note that the higher end of this
 * interpolation table is not the integration limit COSMO_Z_MAX, but rather a smaller number. The
 * error in D(z) due to interpolation is smaller than 1E-3 everywhere in the valid z range.
 */
#define COSMO_TABLE_Z_N 100
#define COSMO_TABLE_Z_MIN COSMO_Z_MIN
#define COSMO_TABLE_Z_MAX 200.0

/*
 * Parameters for the interpolation table for sigma(R). Calculations that involve integrals over
 * the power spectrum are typically quite slow, keeping an interpolation table can speed them up.
 * The interpolation error is less than 1E-3 at any R.
 */
#define COSMO_TABLE_R_SIGMA_N 100
#define COSMO_TABLE_R_SIGMA_MIN 1E-12
#define COSMO_TABLE_R_SIGMA_MAX 1E3

enum
{
	COSMOLOGY_NONE, COSMOLOGY_BOLSHOI, COSMOLOGY_WMAP9, COSMOLOGY_PLANCK
};

enum
{
	POWERSPECTRUM_TYPE_EH98, POWERSPECTRUM_TYPE_EH98SMOOTH, POWERSPECTRUM_TYPE_POWERLAW
};

enum
{
	FILTER_TOPHAT, FILTER_GAUSSIAN
};

/**************************************************************************************************
 * DATA STRUCTURES
 *************************************************************************************************/

/*
 * This structure describes a cosmology.
 *
 * IMPORTANT: If the user changes any of the cosmological parameters, the resetCosmology() function
 * must be called. Otherwise, certain cashed data may not match the new cosmology any more.
 *
 * Fundamentally, the cosmology can be set to
 *
 * - flat: In this case Omega_Lambda = 1 - Omega_m - Omega_r, i.e. the value set in this structure
 *   will be ignored.
 * - non-flat: in this case the value of curvature is determined from the values given in this
 *   struct.
 *
 * A quick way to generate a cosmology record is to use the getCosmology() function with one of
 * the following pre-set cosmologies:
 *
 * COSMOLOGY_NONE       Return an empty object, though with some pre-sets
 * COSMOLOGY_BOLSHOI    The cosmology of the Bolshoi simulation (Klypin et al 2011)
 * COSMOLOGY_WMAP9      The WMAP9 cosmology of Hinshaw et al 2012
 * COSMOLOGY_PLANCK     The cosmology from table 2 of the first Planck cosmology paper; some
 *                      values are rounded for convenience
 *
 * All values are given in the usual units, and H0 is given in km / s / Mpc. The variables starting
 * with ps_ contain information related to the power spectrum of the cosmology, namely the way it
 * is computed:
 *
 * POWERSPECTRUM_TYPE_EH98			Eisenstein & Hu 98
 * POWERSPECTRUM_TYPE_EH98SMOOTH	Eisenstein & Hu 98, smooth
 * POWERSPECTRUM_TYPE_POWERLAW      Self-similar power law cosmology, slope ns
 *
 * The ps_filter field determines the smoothing (to compute the variance):
 *
 * FILTER_TOPHAT                    Top-hat filter (the usual choice)
 * FILTER_GAUSSIAN                  Gaussian filter
 *
 * The normalizationis computed automatically and should not be set by the user.
 */
typedef struct
{
	// Settings
	int interpolate;

	// Standard cosmological parameters
	int flat;
	double h;
	double Omega_m;
	double Omega_b;
	double Omega_r;
	double Omega_L;
	double w;
	double T_CMB;
	double sigma8;
	double ns;

	// Power spectrum
	int ps_type;
	int ps_filter;
	double ps_norm;

	// Interpolation tables
	int spline_z_D_initialized;
	gsl_spline *spline_z_D;
	gsl_interp_accel *spline_z_D_acc;

	int spline_r_sigma_initialized;
	gsl_spline *spline_r_sigma;
	gsl_interp_accel *spline_r_sigma_acc;
	gsl_spline *spline_sigma_r;
	gsl_interp_accel *spline_sigma_r_acc;
} Cosmology;

/**************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

Cosmology getCosmology(int cosmology);
Cosmology getCosmologyPowerLaw(int cosmology, double n);
void initCosmology(Cosmology *cosmo);
void resetCosmology(Cosmology *cosmo);
void resetCosmologySplines(Cosmology *cosmo);
void freeCosmology(Cosmology *cosmo);

double Ez(Cosmology *cosmo, double z);
double oneOverH0(Cosmology *cosmo);
double lookbackTime(Cosmology *cosmo, double z);
double lookbackTimeInverse(Cosmology *cosmo, double t);
double age(Cosmology *cosmo, double z);
double ageInverse(Cosmology *cosmo, double t);
double hubbleTime(Cosmology *cosmo, double z);

double comovingDistanceMpch(Cosmology *cosmo, double z_min, double z_max);
double comovingDistanceMpch0(Cosmology *cosmo, double z);
double comovingDistance(Cosmology *cosmo, double z);
double luminosityDistanceMpch(Cosmology *cosmo, double z);
double luminosityDistance(Cosmology *cosmo, double z);
double angDiamDistanceMpch(Cosmology *cosmo, double z);
double angDiamDistance(Cosmology *cosmo, double z);
double distanceModulus(Cosmology *cosmo, double z);
double comovingVolume(Cosmology *cosmo, double z);

double rhoCrit(Cosmology *cosmo, double z);
double rhoM(Cosmology *cosmo, double z);
double OmegaM(Cosmology *cosmo, double z);

double growthFactorUnnormalized(Cosmology *cosmo, double z);
double growthFactor(Cosmology *cosmo, double z);

double Pk(Cosmology *cosmo, double k);
double correlationFunction(Cosmology *cosmo, double z, double r);
double sigma(Cosmology *cosmo, double z, double R, int j);
double sigmaFromMass(Cosmology *cosmo, double z, double M, int j);
double peakHeight(Cosmology *cosmo, double z, double M);
double radiusFromSigma(Cosmology *cosmo, double z, double sigma);
double massFromSigma(Cosmology *cosmo, double z, double sigma);
double massFromPeakHeight(Cosmology *cosmo, double z, double nu);
double nonLinearMass(Cosmology *cosmo, double z);

#endif
