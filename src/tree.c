/*************************************************************************************************
 *
 * This is fast3tree, a fast BSP tree implementation.
 *
 * (c) 2010 Peter Behroozi, Stanford University. Adapted for SPARTA by Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <assert.h>

#include "tree.h"
#include "config.h"
#include "memory.h"

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

#ifndef __APPLE__
#ifndef isfinite
#define isfinite(x) finitef(x)
#endif
#endif

void _fast3tree_build(Tree *t);
void _fast3tree_maxmin_rebuild(TreeNode *n);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

Tree* treeInit(int64_t n, TREE_TYPE *p)
{
	Tree *new = NULL;
	new = (Tree*) memAlloc(__FFL__, MID_TREE, sizeof(Tree));
	if (!new)
		return 0;
	memset(new, 0, sizeof(Tree));
	treeRebuild(new, n, p);
	return new;
}

void treeRebuild(Tree *t, int64_t n, TREE_TYPE *p)
{
	t->points = p;
	t->num_points = n;
	_fast3tree_build(t);
}

void treeMaxMinRebuild(Tree *t)
{
	_fast3tree_maxmin_rebuild(t->root);
}

void treeFree(Tree **t)
{
	if (!t)
		return;
	Tree *u = *t;
	if (u)
	{
		memFree(__FFL__, MID_TREE, u->root, sizeof(TreeNode) * u->allocated_nodes);
		memFree(__FFL__, MID_TREE, u, sizeof(Tree));
	}
	*t = NULL;
}

/*
 * The system for allocating tree results is to first init a pointer with just the results
 * structure but no actual results. Then, either a set of pointers is allocated in various
 * functions, or the alloc function is used to actually allocated memory for those pointers
 * as well. In that case, the allocated number is set to -1. The free function can discern
 * between those cases.
 */
TreeResults* treeResultsInit(void)
{
	TreeResults *res = (TreeResults*) memAlloc(__FFL__, MID_TREE, sizeof(TreeResults));
	res->points = NULL;
	res->num_points = 0;
	res->num_allocated_points = 0;
	return res;
}

void treeResultsAlloc(TreeResults *res, int n)
{
	int i;

	res->points = (TREE_TYPE**) memAlloc(__FFL__, MID_TREE, sizeof(TREE_TYPE*) * n);
	res->points[0] = (TREE_TYPE*) memAlloc(__FFL__, MID_TREE, sizeof(TREE_TYPE) * n);
	for (i = 1; i < n; i++)
		res->points[i] = res->points[0] + i;
	res->num_points = n;
	res->num_allocated_points = -1;
}

void treeResultsFree(TreeResults *res)
{
	if (!res)
		return;

	if (res->points)
	{
		if (res->num_allocated_points == -1)
		{
			memFree(__FFL__, MID_TREE, res->points[0], sizeof(TREE_TYPE) * res->num_points);
			res->num_allocated_points = res->num_points;
		}
		memFree(__FFL__, MID_TREE, res->points, res->num_allocated_points * sizeof(TREE_TYPE*));
	}
	memFree(__FFL__, MID_TREE, res, sizeof(TreeResults));
}

/* Some false negatives, but best compromise for speed and accuracy.  */
static inline int _fast3tree_fast_box_not_intersect_sphere(TreeNode *node, float c[TREE_DIM],
		float r)
{
	int i;
	for (i = 0; i < TREE_DIM; i++)
	{
		if ((node->min[i] - c[i]) > r)
			return 1;
		if ((c[i] - node->max[i]) > r)
			return 1;
	}
	return 0;
}

/* Fast, accurate. */
static inline int _fast3tree_box_inside_sphere(TreeNode *node, float c[TREE_DIM], float r)
{
	int i;
	float dx, dx2, dist = 0, r2 = r * r;
	if (fabsf(c[0] - node->min[0]) > r)
		return 0; //Rapid short-circuit.
	for (i = 0; i < TREE_DIM; i++)
	{
		dx = node->min[i] - c[i];
		dx *= dx;
		dx2 = c[i] - node->max[i];
		dx2 *= dx2;
		if (dx2 > dx)
			dx = dx2;
		dist += dx;
		if (dist > r2)
			return 0;
	}
	return 1;
}

static inline int _fast3tree_sphere_inside_box(TreeNode *node, float c[TREE_DIM], float r)
{
	int i;
	for (i = 0; i < TREE_DIM; i++)
	{
		if (c[i] - r < node->min[i])
			return 0;
		if (c[i] + r < node->max[i])
			return 0;
	}
	return 1;
}

static inline void _fast3tree_check_results_space(TreeNode *n, TreeResults *res)
{
	if (res->num_points + n->num_points > res->num_allocated_points)
	{
		int n_points_old;

		n_points_old = res->num_allocated_points;
		res->num_allocated_points = res->num_points + n->num_points + 1000;
		res->points = memRealloc(__FFL__, MID_TREE, res->points,
				res->num_allocated_points * sizeof(TREE_TYPE*), n_points_old * sizeof(TREE_TYPE*));
	}
}

void _fast3tree_find_sphere(TreeNode *n, TreeResults *res, float c[TREE_DIM], float r)
{
	int64_t i, j;
	float r2, dist, dx;

	if (_fast3tree_fast_box_not_intersect_sphere(n, c, r))
		return;
	if (_fast3tree_box_inside_sphere(n, c, r))
	{ /* Entirely inside sphere */
		_fast3tree_check_results_space(n, res);
		for (i = 0; i < n->num_points; i++)
			res->points[res->num_points + i] = n->points + i;
		res->num_points += n->num_points;
		return;
	}

	r2 = r * r;
	if (n->div_dim < 0)
	{ /* Leaf node */
		_fast3tree_check_results_space(n, res);
		for (i = 0; i < n->num_points; i++)
		{
			j = dist = 0;
			for (; j < TREE_DIM; j++)
			{
				dx = c[j] - n->points[i].x[j];
				dist += dx * dx;
			}
			if (dist < r2)
			{
				res->points[res->num_points] = n->points + i;
				res->num_points++;
			}
		}
		return;
	}
	/* Otherwise, search in descendant tree nodes. */
	//if ((n->left->min[n->div_dim] - c[n->div_dim]) < r && (c[n->div_dim] - n->left->max[n->div_dim]) < r)
	_fast3tree_find_sphere(n->left, res, c, r);
	_fast3tree_find_sphere(n->right, res, c, r);
}

//static
void treeFindSphere(Tree *t, TreeResults *res, float c[TREE_DIM], float r)
{
	res->num_points = 0;
	_fast3tree_find_sphere(t->root, res, c, r);
}

void _fast3tree_find_sphere_periodic_dim(Tree *t, TreeResults *res, float c[TREE_DIM], float r,
		float dims[TREE_DIM], int dim)
{
	float c2[TREE_DIM];
	if (dim < 0)
	{
		_fast3tree_find_sphere(t->root, res, c, r);
		return;
	}
	memcpy(c2, c, sizeof(float) * TREE_DIM);
	_fast3tree_find_sphere_periodic_dim(t, res, c2, r, dims, dim - 1);
	if (c[dim] + r > t->root->max[dim])
	{
		c2[dim] = c[dim] - dims[dim];
		_fast3tree_find_sphere_periodic_dim(t, res, c2, r, dims, dim - 1);
	}
	if (c[dim] - r < t->root->min[dim])
	{
		c2[dim] = c[dim] + dims[dim];
		_fast3tree_find_sphere_periodic_dim(t, res, c2, r, dims, dim - 1);
	}
}

static inline int64_t _fast3tree_find_largest_dim(float *min, float *max)
{
	int64_t i, dim = TREE_DIM - 1;
	float d = max[TREE_DIM - 1] - min[TREE_DIM - 1], d2;
	for (i = 0; i < (TREE_DIM - 1); i++)
	{
		d2 = max[i] - min[i];
		if (d2 > d)
		{
			d = d2;
			dim = i;
		}
	}
	return dim;
}

static inline int64_t _fast3tree_sort_dim_pos(TreeNode *node)
{
	int64_t dim = node->div_dim = _fast3tree_find_largest_dim(node->min, node->max);
	TREE_TYPE *p = node->points;
	int64_t i, j = node->num_points - 1;
	TREE_TYPE temp;
	float lim = 0.5 * (node->max[dim] + node->min[dim]);

	if (node->max[dim] == node->min[dim])
		return node->num_points;

	for (i = 0; i < j; i++)
	{
		if (p[i].x[dim] > lim)
		{
			temp = p[j];
			p[j] = p[i];
			p[i] = temp;
			i--;
			j--;
		}
	}
	if ((i == j) && (p[i].x[dim] <= lim))
		i++;
	return i;
}

static inline void _fast3tree_find_minmax(TreeNode *node)
{
	int64_t i, j;
	float x;
	TREE_TYPE *p = node->points;
	assert(node->num_points > 0);
	for (j = 0; j < TREE_DIM; j++)
		node->min[j] = node->max[j] = p[0].x[j];
	for (i = 1; i < node->num_points; i++)
	{
		for (j = 0; j < TREE_DIM; j++)
		{
			x = p[i].x[j];
			if (x < node->min[j])
				node->min[j] = x;
			else if (x > node->max[j])
				node->max[j] = x;
		}
	}
}

void _fast3tree_split_node(Tree *t, TreeNode *node)
{
	int64_t num_left;
	TreeNode *null_ptr = NULL;
	TreeNode *left, *right;
	int64_t left_index, node_index;

	num_left = _fast3tree_sort_dim_pos(node);
	if (num_left == node->num_points || num_left == 0)
	{ //In case all node points are at same spot
		node->div_dim = -1;
		return;
	}

	node_index = node - t->root;
	if ((t->num_nodes + 2) > t->allocated_nodes)
	{
		int n_alloc_old;

		n_alloc_old = t->allocated_nodes;
		t->allocated_nodes += 1000;
		t->root = (TreeNode*) memRealloc(__FFL__, MID_TREE, t->root,
				sizeof(TreeNode) * (t->allocated_nodes), sizeof(TreeNode) * n_alloc_old);
		node = t->root + node_index;
	}

	left_index = t->num_nodes;
	t->num_nodes += 2;

	node->left = null_ptr + left_index;
	node->right = null_ptr + (left_index + 1);

	left = t->root + left_index;
	right = t->root + (left_index + 1);
	memset(left, 0, sizeof(TreeNode) * 2);

	right->parent = left->parent = null_ptr + node_index;
	left->num_points = num_left;
	right->num_points = node->num_points - num_left;
	left->div_dim = right->div_dim = -1;
	left->points = node->points;
	right->points = node->points + num_left;

	_fast3tree_find_minmax(left);
	_fast3tree_find_minmax(right);

	if (left->num_points > config.tree_points_per_leaf)
		_fast3tree_split_node(t, left);

	right = t->root + (left_index + 1);
	if (right->num_points > config.tree_points_per_leaf)
		_fast3tree_split_node(t, right);
}

void _fast3tree_rebuild_pointers(Tree *t)
{
	int64_t i;
	TreeNode *nullptr = NULL;
	for (i = 0; i < t->num_nodes; i++)
	{
#define REBUILD(x) x = t->root + (x - nullptr)
		REBUILD(t->root[i].left);
		REBUILD(t->root[i].right);REBUILD(t->root[i].parent);
#undef REBUILD
	}
}

void _fast3tree_build(Tree *t)
{
	int64_t i, j;
	int n_alloc_old;
	TreeNode *root;
	TREE_TYPE tmp;

#if PARANOID
	if (config.tree_points_per_leaf <= 0)
		error(__FFL__, "The tree_points_per_leaf is %d, cannot be less than one.\n",
				config.tree_points_per_leaf);
#endif
	n_alloc_old = t->allocated_nodes;
	t->allocated_nodes = (3 + t->num_points / (config.tree_points_per_leaf / 2));
	t->root = (TreeNode*) memRealloc(__FFL__, MID_TREE, t->root,
			sizeof(TreeNode) * (t->allocated_nodes), sizeof(TreeNode) * n_alloc_old);
	t->num_nodes = 1;

	//Get rid of NaNs / infs
	for (i = 0; i < t->num_points; i++)
	{
		for (j = 0; j < TREE_DIM; j++)
			if (!isfinite(t->points[i].x[j]))
				break;
		if (j < TREE_DIM)
		{
			tmp = t->points[i];
			t->num_points--;
			t->points[i] = t->points[t->num_points];
			t->points[t->num_points] = tmp;
			i--;
		}
	}

	root = t->root;
	memset(root, 0, sizeof(TreeNode));
	root->num_points = t->num_points;
	root->points = t->points;
	root->div_dim = -1;
	if (t->num_points)
		_fast3tree_find_minmax(root);
	for (j = 0; j < TREE_DIM; j++)
		assert(isfinite(root->min[j]));
	for (j = 0; j < TREE_DIM; j++)
		assert(isfinite(root->max[j]));

	if (root->num_points > config.tree_points_per_leaf)
		_fast3tree_split_node(t, root);

	t->root = (TreeNode*) memRealloc(__FFL__, MID_TREE, t->root, sizeof(TreeNode) * t->num_nodes,
			sizeof(TreeNode) * t->allocated_nodes);
	t->allocated_nodes = t->num_nodes;
	_fast3tree_rebuild_pointers(t);
}

void _fast3tree_maxmin_rebuild(TreeNode *n)
{
	int i;
	if (n->div_dim < 0 && n->num_points)
	{
		_fast3tree_find_minmax(n);
		return;
	}
	_fast3tree_maxmin_rebuild(n->left);
	_fast3tree_maxmin_rebuild(n->right);
	memcpy(n->min, n->left->min, sizeof(float) * TREE_DIM);
	memcpy(n->max, n->right->max, sizeof(float) * TREE_DIM);
	for (i = 0; i < TREE_DIM; i++)
	{
		if (n->min[i] > n->right->min[i])
			n->min[i] = n->right->min[i];
		if (n->max[i] < n->left->max[i])
			n->max[i] = n->left->max[i];
	}
}

void _fast3tree_set_minmax(Tree *t, float min, float max)
{
	int i;
	for (i = 0; i < TREE_DIM; i++)
	{
		t->root->min[i] = min;
		t->root->max[i] = max;
	}
}

float fast3tree_find_next_closest_distance(Tree *t, TreeResults *res, float c[TREE_DIM])
{
	int64_t i = 0, j;
	float dist = 0, dx, min_dist = 0;
	TreeNode *nd = t->root;

	while (nd->div_dim >= 0)
	{
		if (c[nd->div_dim] <= (nd->left->max[nd->div_dim]))
		{
			nd = nd->left;
		} else
		{
			nd = nd->right;
		}
	}

	while (nd != t->root && (nd->min[nd->parent->div_dim] == nd->max[nd->parent->div_dim]))
		nd = nd->parent;

	min_dist = 0;
	for (i = 0; i < nd->num_points; i++)
	{
		dist = 0;
		for (j = 0; j < TREE_DIM; j++)
		{
			dx = c[j] - nd->points[i].x[j];
			dist += dx * dx;
		}
		if (!min_dist || (dist && dist < min_dist))
			min_dist = dist;
	}
	min_dist = sqrt(min_dist);

	treeFindSphere(t, res, c, min_dist * 1.01);
	for (i = 0; i < res->num_points; i++)
	{
		dist = 0;
		for (j = 0; j < TREE_DIM; j++)
		{
			dx = res->points[i]->x[j] - c[j];
			dist += dx * dx;
		}
		if ((dist && dist < min_dist))
			min_dist = dist;
	}

	return sqrt(min_dist);
}

/*************************************************************************************************
 * SEARCH FUNCTIONS
 *************************************************************************************************/

/*
 * This recursive
 */
int treeFindSpherePeriodicSimple(Tree *t, TreeResults *res, float c[TREE_DIM], float r)
{
	float dims[TREE_DIM];
	int i;

#if CAREFUL
	if (r < 0.0)
		error(__FFL__, "Trying to do tree search with negative radius %.3e\n", r);
#endif

	if (_fast3tree_sphere_inside_box(t->root, c, r))
	{
		treeFindSphere(t, res, c, r);
		return 2;
	}

	for (i = 0; i < TREE_DIM; i++)
	{
		dims[i] = t->root->max[i] - t->root->min[i];
		if (r * 2.0 > dims[i])
			return 0; //Avoid wraparound intersections.
	}

	res->num_points = 0;
	_fast3tree_find_sphere_periodic_dim(t, res, c, r, dims, TREE_DIM - 1);
	return 1;
}

/*
 * Search the tree for all particles with in r of x. This gets difficult in the case of
 * periodicity in multiple dimensions where we have to generate a lot of "mirror points" to
 * search around.
 *
 * This function returns corrected positions, i.e. absolute coordinates from the search point
 * irrespective of periodicity. Because of this correction, the function needs to allocate a new
 * array with tree points found rather than just returning pointers, meaning that the user must
 * free the returned memory.
 */
int treeFindSpherePeriodicCorrected(Tree *tree, TreeResults *res, float x[TREE_DIM], float r)
{
	int i, d, n_periodic;

#if CAREFUL
	if (r < 0.0)
		error(__FFL__, "Trying to do tree search with negative radius %.3e\n", r);
#endif

	/*
	 * Check periodicity in all three dimensions
	 */
	n_periodic = 0;
	for (d = 0; d < 3; d++)
	{
		if ((x[d] - r < 0.0) || (x[d] + r > config.box_size))
			n_periodic++;
	}

	/*
	 * If no periodicity, return a simple tree search
	 */
	if (n_periodic == 0)
	{
		treeFindSphere(tree, res, x, r);
		return EXIT_SUCCESS;
	}

	/*
	 * If we're doing a periodic search, we need to check that the search radius is not larger
	 * than half the box size. In that case, we will get duplicate results across the periodic
	 * boundaries.
	 */
	if (r >= 0.5 * config.box_size)
		error(__FFL__, "Found tree search radius %.2e, larger than half the box size (%.2e).\n", r,
				config.box_size);

	/*
	 * We are close to the boundary in at least one dimension. For each periodic dimension,
	 * the number of points multiplies by two (near side = 2, near edge = 4, near corner = 8).
	 */
	int n_queries, add_queries, dd, n_total, counter, j;
	n_queries = 1 << n_periodic;
	float query_vector[n_queries][3], shift;
	TreeResults **tempres;

	for (d = 0; d < 3; d++)
		query_vector[0][d] = x[d];

	n_queries = 1;
	for (d = 0; d < 3; d++)
	{
		add_queries = 0;
		shift = 0.0;
		if (query_vector[0][d] - r < 0.0)
		{
			add_queries = 1;
			shift = config.box_size;
		} else if (query_vector[0][d] + r > config.box_size)
		{
			add_queries = 1;
			shift = -config.box_size;
		}

		if (add_queries)
		{
			for (i = 0; i < n_queries; i++)
			{
				for (dd = 0; dd < 3; dd++)
					query_vector[n_queries + i][dd] = query_vector[i][dd];
				query_vector[n_queries + i][d] = query_vector[0][d] + shift;
			}

			n_queries *= 2;
		}
	}

	tempres = (TreeResults**) memAlloc(__FFL__, MID_TREE, sizeof(TreeResults*) * n_queries);
	n_total = 0;
	for (i = 0; i < n_queries; i++)
	{
		tempres[i] = treeResultsInit();
		treeFindSphere(tree, tempres[i], query_vector[i], r);
		n_total += tempres[i]->num_points;
	}

	/*
	 * Combine all the points found into one array, and correct their positions such that
	 * they are in the same coordinate frame as the halo. The -1 flag in the allocated
	 * field indicates that the particles were copied into a new structure and need to be
	 * freed.
	 */
	if (res->num_allocated_points > 0)
		memFree(__FFL__, MID_TREE, res->points, res->num_allocated_points * sizeof(TREE_TYPE*));
	treeResultsAlloc(res, n_total);
	counter = 0;
	for (i = 0; i < n_queries; i++)
	{
		for (j = 0; j < tempres[i]->num_points; j++)
		{
			memcpy(res->points[counter], tempres[i]->points[j], sizeof(TREE_TYPE));
			for (d = 0; d < 3; d++)
				res->points[counter]->x[d] = tempres[i]->points[j]->x[d]
						- (query_vector[i][d] - query_vector[0][d]);
			counter++;
		}

		treeResultsFree(tempres[i]);
	}

	memFree(__FFL__, MID_TREE, tempres, sizeof(TreeResults*) * n_queries);

	return EXIT_SUCCESS;
}
