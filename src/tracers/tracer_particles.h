/*************************************************************************************************
 *
 * This unit implements functions that are specific to particle tracers.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _TRACER_PARTICLES_H_
#define _TRACER_PARTICLES_H_

#include "../statistics.h"
#include "../tree.h"

#if DO_TRACER_PARTICLES

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void beforeTracerResultsParticles(Halo *halo, int snap_idx, Tree *tree, TreeResults *tr_res,
		TracerStatistics *ts);
void afterTracerResultsParticles(Halo *halo, int snap_idx, TracerStatistics *ts);

#endif
#endif
