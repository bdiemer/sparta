/*************************************************************************************************
 *
 * This unit implements the tagging of particles in subhalos.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _TRACER_SUBTAGGING_H_
#define _TRACER_SUBTAGGING_H_

#include "../global.h"
#include "../statistics.h"
#include "../tree.h"

#if DO_SUBHALO_TAGGING

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void tagSubParticles(int snap_idx, Halo *host, Halo *sub, Tree *tree, TracerStatistics *ps);

#endif
#endif
