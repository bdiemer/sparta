/*************************************************************************************************
 *
 * This unit implements the tagging of particles in subhalos.
 *
 * This particular step in the code is a little messy in that it deals with tracers, particles,
 * subhalos, and infall events at the same time.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <sys/stat.h>

#include "tracer_subtagging.h"
#include "tracers.h"
#include "../global.h"
#include "../constants.h"
#include "../config.h"
#include "../memory.h"
#include "../utils.h"
#include "../geometry.h"
#include "../tree_potential.h"
#include "../halos/halo.h"
#include "../halos/halo_so.h"
#include "../results/results.h"
#include "../io/io_hdf5.h"

#if DO_SUBHALO_TAGGING

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Output particle data for every STRIDEth subhalo tagging event, if the event considered MIN_N
 * particles. Careful, this can create enormous amounts of data!
 */
#define DEBUG_OUTPUT_SUBTAGGING_DATA 0
#define DEBUG_OUTPUT_SUBTAGGING_STRIDE 50
#define DEBUG_OUTPUT_SUBTAGGING_MIN_N 100
#define DEBUG_OUTPUT_SUBTAGGING_DIRECTPOT 0

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

#if DO_SUBHALO_TAGGING_IFL_AGE
/*
 * Determine membership from the dynamical age of infall events. If tcr_age is not NULL, output the
 * age in units of dynamical time.
 */
int isInSubhaloInfallAge(int snap_idx, Halo *host, Halo *sub, Particle *particle,
		ResultInfall *res_ifl, float *tcr_age_tdyn)
{
	int do_tag;
	float t_diff, tcr_age;

	t_diff = config.this_snap_t - res_ifl->tifl;
	tcr_age = t_diff / config.snap_t_dyn[snap_idx];
	do_tag = (tcr_age >= config.tcr_ptl_subtag_ifl_age_min);

	debugTracerByID(particle->id, "isInSubhaloInfallAge: t_diff %.2f, t/tdyn %.2f, do_tag %d.",
			t_diff, tcr_age, do_tag);

	if (tcr_age_tdyn != NULL)
	*tcr_age_tdyn = tcr_age;

	return do_tag;
}
#endif

#if DO_SUBHALO_TAGGING_IFL_DISTANCE

/*
 * Check the distance between an infall event and the host halo (!) at the time of infall. Tag if
 * that distance is far enough away to make it unlikely that the particle belonged to the host.
 */
int isInSubhaloInfallDistance(int snap_idx, Halo *host, Halo *sub, Particle *particle,
		ResultInfall *res_ifl, float *tcr_distance)
{
	int j, s1, s2, do_tag;
	float t_ifl, t1, t2, a_t, frac, host_r200m_t, sub_x_t[3], host_x_t[3], ifl_x_t[3], *sub_x_t1,
			*sub_x_t2, *host_x_t1, *host_x_t2, sub_vector[3], host_vector[3], host_ifl_dist;

	do_tag = 0;
	t_ifl = res_ifl->tifl;

#if PARANOID
	if (t_ifl < config.snap_t[0] * 0.999)
		error(__FFL__, "Found infall time %.3f, smaller than first snapshot time %.3f.\n", t_ifl,
				config.snap_t[0]);
#endif

	if (t_ifl < config.snap_t[host->first_snap])
	{
		/*
		 * Case 1: the infall event is so old that the host halo did not exist yet. In that
		 * case, we do tag the event.
		 */
		do_tag = 1;
		host_ifl_dist = -1.0;
	} else
	{
		/*
		 * Case 2: We have access to the halo's position history, so we interpolate the x
		 * and radius of the host and position of the sub to the infall time.
		 */
#if PARANOID
		if (t_ifl < config.snap_t[sub->first_snap])
			error(__FFL__, "Found infall time %.3f, smaller than first snapshot of halo (%.3f).\n",
					t_ifl, config.snap_t[sub->first_snap]);
#endif

		// Find snapshot time before and after
		for (j = 0; j < config.n_snaps; j++)
		{
			if (config.snap_t[j] > t_ifl)
				break;
		}

		s1 = j - 1;
		s2 = s1 + 1;
		t1 = config.snap_t[s1];
		t2 = config.snap_t[s2];

#if CAREFUL
		if (!((t1 <= t_ifl) && (t2 > t_ifl)))
			error(__FFL__, "Wrong time bin, t1 %.2f t2 %.2f tifl %.2f.\n", t1, t2, t_ifl);
		if (s1 < host->first_snap)
			error(__FFL__, "First snap %d, host first %d.\n", s1, host->first_snap);
#endif

		/*
		 * Linearly interpolate halo positions and radii
		 *
		 * The conversion from comoving to physical is not exactly correct because we are
		 * linearly interpolating to get a(t). To be precise, we should use the z(t)
		 * function in the cosmology module. That function, however, is iterative and
		 * costly. Given that we are after a crude estimate here, this should be good
		 * enough.
		 *
		 * The entire calculation is done in comoving box units so that we can take
		 * care of the corrections for periodic BCs.
		 */
		frac = (t_ifl - t1) / (t2 - t1);

		a_t = config.snap_a[s1] + frac * (config.snap_a[s2] - config.snap_a[s1]);

		host_r200m_t = host->history_R200m[s1]
				+ frac * (host->history_R200m[s2] - host->history_R200m[s1]);
		host_r200m_t = host_r200m_t / 1000.0 / a_t;

#if CAREFUL
		if ((host->history_R200m[s1] < 0.0) || (host->history_R200m[s2] < 0.0))
		{
			for (j = host->first_snap; j <= snap_idx; j++)
				output(0, "HISTORY %2d %.1f\n", j, host->history_R200m[j]);
			error(__FFL__, "-1 in history, s1 %d s2 %d, t1 %.2f t2 %.2f, tifl %.2f.\n", s1, s2, t1,
					t2, t_ifl);
		}

#endif
		host_ifl_dist = 0.0;
		host_x_t1 = haloX(host, s1, snap_idx);
		host_x_t2 = haloX(host, s2, snap_idx);
		sub_x_t1 = haloX(sub, s1, snap_idx);
		sub_x_t2 = haloX(sub, s2, snap_idx);

		periodicDistanceVector(host_x_t1, host_x_t2, host_vector);
		periodicDistanceVector(sub_x_t1, sub_x_t2, sub_vector);

		for (j = 0; j < 3; j++)
		{
			host_x_t[j] = host_x_t1[j] + frac * host_vector[j];
			sub_x_t[j] = sub_x_t1[j] + frac * sub_vector[j];
			ifl_x_t[j] = sub_x_t[j] + (res_ifl->x[j] / 1000.0 / a_t);
#if CAREFUL
			if (res_ifl->x[0] < INVALID_INDICATOR)
				error(__FFL__, "Found infall result with invalid position.\n");
#endif
		}

		host_ifl_dist = periodicDistance(host_x_t, ifl_x_t) / host_r200m_t;
		do_tag = (host_ifl_dist > config.tcr_ptl_subtag_ifl_dist_min);

#if CAREFUL
		if (host_ifl_dist < 0.0)
			error(__FFL__, "Negative distance %.2e, host_r200m_t %.2e\n",
					host_ifl_dist * host_r200m_t, host_r200m_t);
#endif
	}

	if (tcr_distance != NULL)
		*tcr_distance = host_ifl_dist;

	return do_tag;
}
#endif

/*
 * If we are using boundness for tagging, compute a potential-to-kinetic energy ratio for each
 * particle within a certain search radius.
 */
#if DO_SUBHALO_TAGGING_BOUND
void isInSubhaloBound(int snap_idx, Halo *sub, TreeResults *tr_res, int n_ptl_all,
		int_least8_t *bound, float *bound_ratio, float *bound_ratio_direct)
{
	int i, j, d, n_ptl_include;
	float v_phys, sub_R200m_all_phys_kpc, r_phys, ke, pe_ke_ratio;
	TreePotentialPoint *pot;
	int_least8_t *include_ptl;
	Particle *particle;

	/*
	 * Only include particles within a tighter radius than the general search, as set by the
	 * SUBTAG_METHOD_BOUND_RADIUS parameter.
	 */
	include_ptl = (int_least8_t*) memAlloc(__FFL__, MID_TCRPTL, n_ptl_all * sizeof(int_least8_t));
	n_ptl_include = 0;
	sub_R200m_all_phys_kpc = haloR200m(sub, snap_idx);
	for (i = 0; i < n_ptl_all; i++)
	{
		particle = tr_res->points[i];
		bound[i] = 0;

		tracerRadiusVelocity(snap_idx, sub->cd.x, sub->cd.v, particle->x, particle->v, &r_phys,
				&v_phys);
		include_ptl[i] = (r_phys <= config.tcr_ptl_subtag_bound_radius * sub_R200m_all_phys_kpc);
		if (include_ptl[i])
			n_ptl_include++;
	}

	/*
	 * Find potential. We need to initialize a tree potential structure with the coordinates of the
	 * particles.
	 */
	pot = (TreePotentialPoint*) memAlloc(__FFL__, MID_TCRPTL,
			n_ptl_include * sizeof(TreePotentialPoint));
	j = 0;
	for (i = 0; i < n_ptl_all; i++)
	{
		if (!include_ptl[i])
			continue;
		for (d = 0; d < 3; d++)
			pot[j].x[d] = physicalRadius(tr_res->points[i]->x[d], snap_idx);
		j++;
	}
	computeTreePotential(pot, n_ptl_include, config.particle_mass, config.this_snap_force_res,
			config.potential_err_tol, config.potential_points_per_leaf);

	/*
	 * Compute energy ratio and decide if particle is bound.
	 */
	j = 0;
	for (i = 0; i < n_ptl_all; i++)
	{
		if (!include_ptl[i])
			continue;
		particle = tr_res->points[i];
		tracerRadiusVelocity(snap_idx, sub->cd.x, sub->cd.v, particle->x, particle->v, &r_phys,
				&v_phys);
		ke = 0.5 * v_phys * v_phys * CONVERSION_KIN_TO_POT;
		pe_ke_ratio = pot[j].pe / ke;
		bound[i] = (pe_ke_ratio >= config.tcr_ptl_subtag_bound_ratio);
#if CAREFUL
		if (pe_ke_ratio < 0.0)
			error(__FFL__, "Negative bound ratio, pe %.2e ke %.2e bound %d, %d ptl, %d included.\n",
					pot[j].pe, ke, bound[i], n_ptl_all, n_ptl_include);
#endif
		if (bound_ratio != NULL)
			bound_ratio[i] = pe_ke_ratio;
		j++;
	}

#if (DEBUG_OUTPUT_SUBTAGGING_DATA && DEBUG_OUTPUT_SUBTAGGING_DIRECTPOT)
	/*
	 * If we are outputting data, also compute the potential by direct summation to allow for
	 * accuracy checks. Note that the kinetic energy is currently stored in the pe_ke_ratio field
	 * of the potential items.
	 *
	 * This is very time-consuming and thus has its own switch.
	 */
	computeDirectPotential(pot, n_ptl_include, config.particle_mass, config.this_snap_force_res);
	j = 0;
	for (i = 0; i < n_ptl_all; i++)
	{
		if (!include_ptl[i])
			continue;
		particle = tr_res->points[i];
		tracerRadiusVelocity(snap_idx, sub->cd.x, sub->cd.v, particle->x, particle->v, &r_phys,
				&v_phys);
		ke = 0.5 * v_phys * v_phys * CONVERSION_KIN_TO_POT;
		pe_ke_ratio = pot[j].pe / ke;
		bound_ratio_direct[i] = pot[j].pe / pe_ke_ratio;
		j++;
	}
#endif

	memFree(__FFL__, MID_TCRPTL, pot, n_ptl_include * sizeof(TreePotentialPoint));
	memFree(__FFL__, MID_TCRPTL, include_ptl, n_ptl_all * sizeof(int_least8_t));
}
#endif

/*
 * This particle should be carried as a tracer. First, wecheck whether a tracer already
 * exists. If so, we just update it to have an active status again (and we correct the
 * counters which were set earlier).
 *
 * If no tracer exists, we first need to check whether the particle is on the ignore
 * list and take it off the list if necessary. Then we create a new tracer.
 *
 * Not all particles that have been identified will be tracers at this point (in general),
 * so we create them. If there are tracers that are not identified, we delete them.
 */
void initializeSubTracer(int snap_idx, Halo *sub, Particle *particle, int sub_n_tcr, int sub_n_iid,
		int *sub_iid_changed, DynArray *iidDeleteList, TracerStatistics *ts)
{
	int rs, rs_status, *ptr_int;
	Tracer temp_tcr, *sub_tcr;
	ID *idp;

	temp_tcr.id = particle->id;
	sub_tcr = (Tracer*) bsearch(&temp_tcr, sub->tt[PTL].tcr.tcr, sub_n_tcr, sizeof(Tracer),
			&compareTracers);
	if (sub_tcr != NULL)
	{
		/*
		 * Tracer exists, keep alive. Here, we need to manually set the status of the
		 * results because this is a unique case: we are not creating a tracer, but its
		 * status changes. We respect the special case RS_STATUS_MAINTAIN which indicates
		 * to keep the old status.
		 */
		sub_tcr->status = TCR_STATUS_ACTIVE;
		ts->n[TCRSTAT_HALO_BECAME_SUB]--;
		debugTracer(sub, sub_tcr, "Updated during subtracking initialization, keeping alive.");

		for (rs = 0; rs < NRS; rs++)
		{
			if (sub->instr_rs[PTL][rs])
			{
				rs_status = ttprops[PTL].instr[rs][INSTR_SUBTRACK_CONTD];
				if (rs_status != RS_STATUS_MAINTAIN)
				{
					sub_tcr->status_rs[rs] = rs_status;
					debugTracer(sub, sub_tcr,
							"During subtracking initialization, set result %s status %d.",
							rsprops[rs].name_short, rs_status);
				} else
				{
					debugTracer(sub, sub_tcr,
							"During subtracking initialization, maintained result %s status %d.",
							rsprops[rs].name_short, rs_status);
				}
#if CAREFUL
				if (sub_tcr->status_rs[rs] == RS_STATUS_NONE)
					error(__FFL__, "Found RS_STATUS_NONE during subtrack initialization.\n");
				if (sub_tcr->status_rs[rs] == RS_STATUS_MAINTAIN)
					error(__FFL__, "Found RS_STATUS_MAINTAIN during subtrack initialization.\n");
#endif
			}
		}
	} else
	{
		/*
		 * Check ignore list
		 */
		idp = (ID*) bsearch(&(particle->id), sub->tt[PTL].iid.ids, sub_n_iid, sizeof(ID),
				&compareLongsAscending);
		if (idp != NULL)
		{
			ptr_int = addInt(__FFL__, iidDeleteList);
			*ptr_int = (idp - sub->tt[PTL].iid.ids);
			*sub_iid_changed = 1;

			debugTracerByID(particle->id,
					"During subtracking initialization, found particle on ignore list of halo %ld; removing from list.",
					sub->cd.id);
		}

		/*
		 * Create a new tracer. The status of the results is automatically set in the
		 * createTracerObject() function which detects that this is a becoming subhalo.
		 */
		sub_tcr = createTracerObject(sub, PTL, particle->id, snap_idx, particle->x, sub_n_iid, 0);
		if (sub_tcr != NULL)
		{
			tracerTrajectory(snap_idx, sub_tcr, sub->cd.x, sub->cd.v, particle->x, particle->v,
					haloR200m(sub, snap_idx), 0);
			ts->n[TCRSTAT_PROCESSED]++;
			ts->n[TCRSTAT_NEW]++;
			debugTracer(sub, sub_tcr, "Created during initialization of sub-tracking.");
		} else
		{
			*sub_iid_changed = 1;
			error(__FFL__, "Failed to add sub-tracking tracer.\n");
		}
	}

	/*
	 * Regardless of previous state, we decided that the tracer has entered the (sub)halo
	 * by virtue of having been selected as a member particle. This way, once all results
	 * are done, the tracer should end up (back) on the ignore list. However, we indicate that the
	 * tracer was only added in a subgtagging event and may not have crossed R200m. If it has, then
	 * that status should already be set and we leave it alone.
	 */
	if (sub_tcr->status_entered == TCR_ENTERED_NO)
		sub_tcr->status_entered = TCR_ENTERED_SUBTAG;
}

/*
 * First, check whether this particle is on the host's ignore list. If so, we cannot
 * tag a tracer (though we can still find a pre-existing infall event). If the particle
 * is not on the ignore list, we look for a tracer and create one if necessary.
 */
void tagHostParticle(int snap_idx, Halo *host, Particle *particle, int host_n_tcr, int host_n_iid,
		float smr, TracerStatistics *ts, int *host_tcr_added, int *host_iid_added)
{
	float r_phys, t_diff;
	ID *idp;
	Tracer temp_tcr, *host_tcr;
	ResultInfall temp_res_ifl, *res_ifl;

	host_tcr = NULL;
	idp = (ID*) bsearch(&(particle->id), host->tt[PTL].iid.ids, host_n_iid, sizeof(ID),
			&compareLongsAscending);
	if (idp != NULL)
	{
		ts->n[TCRSTAT_SUB_TAG_IGNORED]++;
	} else
	{
		temp_tcr.id = particle->id;
		host_tcr = (Tracer*) bsearch(&temp_tcr, host->tt[PTL].tcr.tcr, host_n_tcr, sizeof(Tracer),
				&compareTracers);
		if (host_tcr == NULL)
		{
			/*
			 * There is no host tracer, so we should create one. But we need to perform
			 * one final check: if the position of this particle is outside the tracer
			 * delete radius for the host, the tracer will immediately be destroyed again,
			 * causing issues. Thus, we only create tracers if they are within the
			 * tracer destruction radius times a safety factor.
			 */
			r_phys = tracerRadius(snap_idx, host->cd.x, particle->x);
			if (r_phys <= haloR200m(host, snap_idx) * config.tcr_ptl_delete_radius * 0.99)
			{
				host_tcr = createTracerObject(host, PTL, particle->id, snap_idx, particle->x,
						host_n_iid, 0);
				if (host_tcr != NULL)
				{
					tracerTrajectory(snap_idx, host_tcr, host->cd.x, host->cd.v, particle->x,
							particle->v, haloR200m(host, snap_idx), 0);
					ts->n[TCRSTAT_PROCESSED]++;
					ts->n[TCRSTAT_NEW]++;
					ts->n[TCRSTAT_SUB_TAG_CREATED]++;
					*host_tcr_added = 1;
					debugTracer(host, host_tcr, "Created while tagging particles in host halo.");
				} else
				{
					*host_iid_added = 1;
				}
			} else
			{
				debugTracer(host, host_tcr,
						"Did not create while tagging host particles because outside of delete radius (r %.2e, R200m %.2e, Rdelete %.2e).",
						r_phys, haloR200m(host, snap_idx),
						haloR200m(host, snap_idx) * config.tcr_ptl_delete_radius);
			}
		}

		/*
		 * Now check whether this tracer exists (was there before of was successfully
		 * created), and whether it has already been tagged.
		 */
		if ((host_tcr != NULL) && (host_tcr->smr < smr))
		{
			host_tcr->smr = smr;
			ts->n[TCRSTAT_SUB_TAG]++;
			debugTracer(host, host_tcr, "Tagged with SMR %.6f.", host_tcr->smr);
		}
	}

	/*
	 * Regardless of whether we tagged a tracer, there might already be an infall event, so
	 * we need to find that event if it exists and tag it as well.
	 */
	temp_res_ifl.tracer_id = particle->id;
	res_ifl = (ResultInfall*) bsearch(&temp_res_ifl, host->tt[PTL].rs[IFL].rs_ifl,
			host->tt[PTL].rs[IFL].n, sizeof(ResultInfall), &compareResults);
	if (res_ifl == NULL)
	{
		debugTracer(host, host_tcr,
				"Not tagging pre-existing infall event because non could be found.");
	} else if (res_ifl->smr >= smr)
	{
		debugTracer(host, host_tcr,
				"Not tagging pre-existing infall event, SMR >= sub SMR (%.4f, %.4f).", res_ifl->smr,
				smr);
	} else
	{
		t_diff = config.this_snap_t - res_ifl->tifl;
		if (t_diff < config.tcr_ptl_subtag_host_max_age * config.snap_t_dyn[snap_idx])
		{
			res_ifl->smr = smr;
			ts->n[TCRSTAT_SUB_TAG_RESULT]++;
			debugTracer(host, host_tcr,
					"Tagging pre-existing infall event (SMR %.6f) with SMR %.6f.", res_ifl->smr,
					smr);
		} else
		{
			debugTracer(host, host_tcr,
					"Not tagging pre-existing infall event because too old (t_diff %.2f, %.2f tdyn).",
					t_diff, t_diff/config.snap_t_dyn[snap_idx]);
		}
	}
}

/*
 * Post-process tracers and ignore list in the host halo.
 *
 * We re-sort the particle tracer array in the host since we might have added tracers.
 * This is critical, otherwise tracers can be added multiple times by different infalling
 * subhalos!
 */
void finalizeHostTracers(Halo *host, int host_tcr_added, int host_iid_added)
{
	if (host_tcr_added)
		qsort(host->tt[PTL].tcr.tcr, host->tt[PTL].tcr.n, sizeof(Tracer), &compareTracers);
	if (host_iid_added)
		qsort(host->tt[PTL].iid.ids, host->tt[PTL].iid.n, sizeof(ParticleID),
				&compareLongsAscending);
}

/*
 * Actually delete particles for which we created tracers off the ignore list, and delete tracers
 * wherever necessary.
 */
void finalizeSubTracers(Halo *sub, DynArray *iidDeleteList, int sub_iid_changed,
		TracerStatistics *ts)
{
	int i, ms_mask;
	int_least8_t *mask;
	Tracer *sub_tcr;

	if (sub_iid_changed)
	{
		ms_mask = sizeof(int_least8_t) * sub->tt[PTL].iid.n;
		mask = (int_least8_t*) memAlloc(__FFL__, MID_TCRPTL, ms_mask);
		memset(mask, 0, ms_mask);
		for (i = 0; i < iidDeleteList->n; i++)
			mask[iidDeleteList->ints[i]] = 1;
		deleteDynArrayItems(__FFL__, &(sub->tt[PTL].iid), mask);
		memFree(__FFL__, MID_TCRPTL, mask, ms_mask);

		qsort(sub->tt[PTL].iid.ids, sub->tt[PTL].iid.n, sizeof(ParticleID),
				&compareLongsAscending);
	}
	freeDynArray(__FFL__, iidDeleteList);

	/*
	 * We can safely assume that tracers were changed. We could use the more general
	 * deleteTracers() function, but this function goes through all tracer types instead of
	 * just particles.
	 *
	 * Note that we do not put tracers on the ignore list if they are deleted in this way. This is
	 * the same behavior as when all tracers are deleted, that is, when sub-tracking is off. See
	 * the respective comments for why tracers should not go on the ignore list in this case.
	 */
	ms_mask = sizeof(int_least8_t) * sub->tt[PTL].tcr.n;
	mask = (int_least8_t*) memAlloc(__FFL__, MID_TRACERHANDLING, ms_mask);
	for (i = 0; i < sub->tt[PTL].tcr.n; i++)
	{
		sub_tcr = &(sub->tt[PTL].tcr.tcr[i]);

		mask[i] = (sub_tcr->status == TCR_STATUS_DELETE);
		if (mask[i])
		{
			ts->n[TCRSTAT_DELETED]++;
			debugTracer(sub, sub_tcr, "Deleting during subtracking initialization.");
		}
	}
	deleteDynArrayItems(__FFL__, &(sub->tt[PTL].tcr), mask);
	memFree(__FFL__, MID_TRACERHANDLING, mask, ms_mask);

	/*
	 * Compress the tracer dyn array, since it will not be added to while the halo is a subhalo.
	 * We do not do this for the ignore list because that will still be added to when subhalo
	 * tracers are deleted.
	 */
	compressDynArray(__FFL__, &(sub->tt[PTL].tcr));

	/*
	 * Re-sort the tracer array after the changes we have made.
	 */
	qsort(sub->tt[PTL].tcr.tcr, sub->tt[PTL].tcr.n, sizeof(Tracer), &compareTracers);
}

/*
 * Find all particle tracers that belong to an infalling subhalo, and mark them with the mass
 * ratio.
 *
 * The second action is to create a set of particle tracers in the subhalo if particles are being
 * tracked, or to delete some or all particle tracers if not.
 */
void tagSubParticles(int snap_idx, Halo *host, Halo *sub, Tree *tree, TracerStatistics *ts)
{
	int i, host_n_tcr, host_n_iid, host_tcr_added, host_iid_added, sub_n_tcr, sub_n_iid,
			sub_iid_changed, sub_age_snaps, do_tag_host_particles, do_create_sub_tracers,
			tag_counter, n_ptl_all;
	int_least8_t tagged, tagged_ifl_age, tagged_ifl_dist, tagged_bound;
	float smr, sub_R200m_all_com_mpc;
	TreeResults *tr_res;
	Particle *particle;
	DynArray iidDeleteList;

#if DEBUG_OUTPUT_SUBTAGGING_DATA
	int_least8_t *tagged_all, *tagged_ifl_age_all, *tagged_ifl_dist_all, *tagged_bound_all;
	float *ifl_dist_all = NULL;
#endif
#if (DO_SUBHALO_TAGGING_IFL_AGE || DO_SUBHALO_TAGGING_IFL_DISTANCE)
	ResultInfall temp_res_ifl, *res_ifl;
#endif
#if (DEBUG_OUTPUT_SUBTAGGING_DATA || DO_SUBHALO_TAGGING_IFL_AGE)
	float *ifl_age_all = NULL;
#endif
#if (DEBUG_OUTPUT_SUBTAGGING_DATA || DO_SUBHALO_TAGGING_IFL_DISTANCE)
	float ifl_distance;
#endif
	#if (DEBUG_OUTPUT_SUBTAGGING_DATA || DO_SUBHALO_TAGGING_BOUND)
	float *bound_ratio_all = NULL;
	float *bound_ratio_direct_all = NULL;
#endif
#if DO_SUBHALO_TAGGING_BOUND
	int_least8_t *bound;
#endif

	/*
	 * Determine once and for all which actions we need to perform for this host and sub. Criteria
	 * for tagging host particles are:
	 *
	 * - Whether the host halo has particle tracers at all.
	 * - Whether the subhalo was born into this host, i.e. did not exist prior to infall. In this
	 *   case, we abort because it is not clear that the particles really fell in with this
	 *   subhalo.
	 */
#if DO_SUBHALO_TAGGING
	do_tag_host_particles = 1;

	if (!host->instr_tcr[PTL])
	{
		do_tag_host_particles = 0;
		debugHalo(host,
				"Not tagging particles in sub ID %ld because particle tracers are turned off for host halo.",
				sub->cd.id);
	}

	sub_age_snaps = snap_idx - sub->first_snap;
	if (sub_age_snaps == 0)
	{
		do_tag_host_particles = 0;
		debugHalo(host, "Not tagging particles in sub ID %ld because it was born as a sub.",
				sub->cd.id);
		debugHalo(sub, "Not tagging particles in new host ID %ld because it was born as a sub.",
				host->cd.id);
	}
#else
	do_tag_host_particles = 0;
#endif

	if (do_tag_host_particles)
	{
		debugHalo(host, "Tagging particles in sub ID %ld.", sub->cd.id);
		debugHalo(sub, "Tagging particles in new host ID %ld.", host->cd.id);
	}

	/*
	 * Criteria for creating subhalo tracers are:
	 *
	 * - whether the compile switch is on
	 * - whether particle tracers are on for the subhalo
	 * - whether the halo is actually becoming a subhalo or switching host
	 */
#if DO_SUBHALO_TRACKING
	do_create_sub_tracers = 1;
	if (!sub->instr_tcr[PTL])
	{
		do_create_sub_tracers = 0;
		debugHalo(sub, "Not creating subhalo tracers because particle tracers are turned off.");
	}
	if (!isBecomingSub(sub))
	{
		do_create_sub_tracers = 0;
		debugHalo(sub, "Not creating subhalo tracers because halo is switching hosts.");
	}
#else
	do_create_sub_tracers = 0;
#endif

	if (do_create_sub_tracers)
		debugHalo(sub, "Tagging particles for subhalo tracking.");

	/*
	 * Define a radius to search within which we are looking for taggable particles. This radius
	 * must be smaller or the same as the tracking radius beyond which we delete tracers again,
	 * otherwise a tracer can be created and deleted in the same snapshot which has messy
	 * consequences. Thus, we add a small numerical factor of 0.999 to our search radius.
	 *
	 * This is also the reason why it is not a good idea to use something like M_bound here; in
	 * rare, weird cases where the catalog M_bound is greater than M200m_all, the particle can be
	 * outside the tracking radius at birth. Better to rely on the same underlying quantity.
	 */
	sub_R200m_all_com_mpc = haloR200mComoving(sub, snap_idx);
	tr_res = treeResultsInit();
	treeFindSpherePeriodicCorrected(tree, tr_res, sub->cd.x,
			sub_R200m_all_com_mpc * config.tcr_ptl_subtag_radius * 0.999);
	n_ptl_all = tr_res->num_points;

	/*
	 * We record the number of previously existing tracers, since we might add tracers in the
	 * process.
	 */
	smr = haloMassRatio(host, sub);

	host_n_tcr = host->tt[PTL].tcr.n;
	host_tcr_added = 0;
	host_n_iid = host->tt[PTL].iid.n;
	host_iid_added = 0;

	sub_n_tcr = sub->tt[PTL].tcr.n;
	sub_n_iid = sub->tt[PTL].iid.n;
	sub_iid_changed = 0;

	tag_counter = 0;

	/*
	 * If we are distinguishing between different subhalo tracers, we also need to delete the ones
	 * that aren't in the subhalo. Thus, we set the status of all to delete and then change the
	 * ones that are being preserved. Note that we keep track only of the counting for reasons why
	 * tracers are being deleted, the actual delete counter will be set below.
	 */
	if (do_create_sub_tracers)
	{
		Tracer *tcr;
		int j;
		for (j = 0; j < sub->tt[PTL].tcr.n; j++)
		{
			tcr = &(sub->tt[PTL].tcr.tcr[j]);
			if (tcr->status != TCR_STATUS_ACTIVE)
				error(__FFL__,
						"During subtracking, expected tracer status to be ACTIVE, but found %d.\n",
						tcr->status);
			tcr->status = TCR_STATUS_DELETE;
			ts->n[TCRSTAT_HALO_BECAME_SUB]++;
			debugTracer(sub, tcr,
					"Temporarily set tracer status to DELETE because halo has become sub.");
		}

		/*
		 * Initialize a list of ignore IDs to be deleted. There could be a lot, so much better to
		 * delete them all at once at the end.
		 */
		initDynArray(__FFL__, &iidDeleteList, DA_TYPE_INT);
	}

#if DEBUG_OUTPUT_SUBTAGGING_DATA
	int j;
	float **ptl_x, **ptl_v;

	tagged_all = (int_least8_t *) memAlloc(__FFL__, MID_TCRPTL, n_ptl_all * sizeof(int_least8_t));
	tagged_ifl_age_all = (int_least8_t *) memAlloc(__FFL__, MID_TCRPTL,
			n_ptl_all * sizeof(int_least8_t));
	tagged_ifl_dist_all = (int_least8_t *) memAlloc(__FFL__, MID_TCRPTL,
			n_ptl_all * sizeof(int_least8_t));
	tagged_bound_all = (int_least8_t *) memAlloc(__FFL__, MID_TCRPTL,
			n_ptl_all * sizeof(int_least8_t));

	ifl_age_all = (float *) memAlloc(__FFL__, MID_TCRPTL, n_ptl_all * sizeof(float));
	ifl_dist_all = (float *) memAlloc(__FFL__, MID_TCRPTL, n_ptl_all * sizeof(float));
	bound_ratio_all = (float *) memAlloc(__FFL__, MID_TCRPTL, n_ptl_all * sizeof(float));
#if DEBUG_OUTPUT_SUBTAGGING_DIRECTPOT
	bound_ratio_direct_all = (float *) memAlloc(__FFL__, MID_TCRPTL, n_ptl_all * sizeof(float));
#endif

	ptl_x = (float **) memAlloc(__FFL__, MID_TCRPTL, 3 * sizeof(float *));
	ptl_v = (float **) memAlloc(__FFL__, MID_TCRPTL, 3 * sizeof(float *));
	ptl_x[0] = (float *) memAlloc(__FFL__, MID_TCRPTL, 3 * n_ptl_all * sizeof(float));
	ptl_v[0] = (float *) memAlloc(__FFL__, MID_TCRPTL, 3 * n_ptl_all * sizeof(float));

	for (i = 1; i < 3; i++)
	{
		ptl_x[i] = ptl_x[0] + i * n_ptl_all;
		ptl_v[i] = ptl_v[0] + i * n_ptl_all;
	}

	for (i = 0; i < n_ptl_all; i++)
	{
		tagged_all[i] = 0;
		tagged_ifl_age_all[i] = 0;
		tagged_ifl_dist_all[i] = 0;
		tagged_bound_all[i] = 0;

		ifl_age_all[i] = -1.0;
		ifl_dist_all[i] = -1.0;
		bound_ratio_all[i] = -1.0;
#if DEBUG_OUTPUT_SUBTAGGING_DIRECTPOT
		bound_ratio_direct_all[i] = -1.0;
#endif
	}
#endif

	/*
	 * Determine boundness; this has to be done for all particles at the same time to
	 * compute the gravitational potential.
	 */
#if DO_SUBHALO_TAGGING_BOUND
	bound = (int_least8_t*) memAlloc(__FFL__, MID_TCRPTL, n_ptl_all * sizeof(int_least8_t));
	isInSubhaloBound(snap_idx, sub, tr_res, n_ptl_all, bound, bound_ratio_all,
			bound_ratio_direct_all);
#endif

	/*
	 * Run through all candidate particles
	 */
	for (i = 0; i < n_ptl_all; i++)
	{
		particle = tr_res->points[i];
#if DEBUG_OUTPUT_SUBTAGGING_DATA
		for (j = 0; j < 3; j++)
		{
			ptl_x[j][i] = particle->x[j];
			ptl_v[j][i] = particle->v[j];
		}
#endif

		/*
		 * Initialize tagging indicators
		 */
		if (config.tcr_ptl_subtag_inclusive)
			tagged = 0;
		else
			tagged = 1;
		tagged_ifl_age = -1;
		tagged_ifl_dist = -1;
		tagged_bound = -1;

		/*
		 * Check different tagging indicators
		 */
#if (DO_SUBHALO_TAGGING_IFL_AGE || DO_SUBHALO_TAGGING_IFL_DISTANCE)
		temp_res_ifl.tracer_id = particle->id;
		res_ifl = (ResultInfall*) bsearch(&temp_res_ifl, sub->tt[PTL].rs[IFL].rs_ifl,
				sub->tt[PTL].rs[IFL].n, sizeof(ResultInfall), &compareResults);
		if (res_ifl != NULL)
		{
			/*
			 * Check infall age
			 */
#if DO_SUBHALO_TAGGING_IFL_AGE
			tagged_ifl_age = isInSubhaloInfallAge(snap_idx, host, sub, particle, res_ifl,
					&(ifl_age_all[i]));
			if (config.tcr_ptl_subtag_inclusive)
			tagged = tagged || tagged_ifl_age;
#if DEBUG_OUTPUT_SUBTAGGING_DATA
			tagged_ifl_age_all[i] = tagged_ifl_age;
#endif
#endif

			/*
			 * Check infall distance
			 */
#if DO_SUBHALO_TAGGING_IFL_DISTANCE
			tagged_ifl_dist = isInSubhaloInfallDistance(snap_idx, host, sub, particle, res_ifl,
					&ifl_distance);
			if (config.tcr_ptl_subtag_inclusive)
				tagged = tagged || tagged_ifl_dist;
#if DEBUG_OUTPUT_SUBTAGGING_DATA
			ifl_dist_all[i] = ifl_distance;
			tagged_ifl_dist_all[i] = tagged_ifl_dist;
#endif
#endif
		} else
		{
			tagged_ifl_age = 0;
			tagged_ifl_dist = 0;
		}
#endif

		/*
		 * Check bound
		 */
#if DO_SUBHALO_TAGGING_BOUND
		tagged_bound = bound[i];
#if DEBUG_OUTPUT_SUBTAGGING_DATA
		tagged_bound_all[i] = tagged_bound;
#endif
#endif

		/*
		 * Combine into logic. If inclusive, just one applied criterion must be true. If exclusive,
		 * just one applied criterion must be false for us not to tag.
		 */
		if (config.tcr_ptl_subtag_inclusive)
		{
			tagged = ((tagged_ifl_age == 1) || (tagged_ifl_dist == 1) || (tagged_bound == 1));
		} else
		{
			tagged = ((tagged_ifl_age != 0) && (tagged_ifl_dist != 0) && (tagged_bound != 0));
		}
#if DEBUG_OUTPUT_SUBTAGGING_DATA
		tagged_all[i] = tagged;
#endif
		if (tagged)
			tag_counter++;

		/*
		 * Part 1: Create subhalo tracers.
		 */
		if (tagged && do_create_sub_tracers)
		{
			initializeSubTracer(snap_idx, sub, particle, sub_n_tcr, sub_n_iid, &sub_iid_changed,
					&iidDeleteList, ts);
		}

		/*
		 * Part 2: Tag host tracers
		 */
		if (tagged && do_tag_host_particles)
		{
			tagHostParticle(snap_idx, host, particle, host_n_tcr, host_n_iid, smr, ts,
					&host_tcr_added, &host_iid_added);
		}
	}

	/*
	 * Free memory
	 */
	treeResultsFree(tr_res);
#if DO_SUBHALO_TAGGING_BOUND
	memFree(__FFL__, MID_TCRPTL, bound, n_ptl_all * sizeof(int_least8_t));
#endif

	/*
	 * If desired, output a file with data about the particle tagging.
	 */
#if DEBUG_OUTPUT_SUBTAGGING_DATA
	hid_t file_id;
	int n_hist_host, n_hist_sub, s;
	char filename[100];
	float **host_x, **sub_x, *sub_R200m_all_array, *host_R200m_all_array, *sub_R200m_phys_array,
			sub_R200m_bnd_phys_kpc, sub_R200m_bnd_com_mpc, *sub_x_final, *sub_x_t1, *host_x_t1,
			host_vector[3], sub_vector[3];

	if ((host->cd.id % DEBUG_OUTPUT_SUBTAGGING_STRIDE == 0)
			&& (n_ptl_all >= DEBUG_OUTPUT_SUBTAGGING_MIN_N))
	{
		createPath("subtag", 02755, 0);
		sprintf(filename, "subtag/subtag_snap%03d_sub%08ld_hst%08ld.hdf5", snap_idx, sub->cd.id,
				host->cd.id);
		file_id = H5Fcreate(filename, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
		hdf5CheckForError(__FFL__, file_id, "creating subtagging file");

		/*
		 * Write attributes
		 */
		sub_R200m_bnd_phys_kpc = soMtoR(sub->cd.M_bound, config.this_snap_rho_200m);
		sub_R200m_bnd_com_mpc = comovingRadius(sub_R200m_bnd_phys_kpc, snap_idx);

		hdf5WriteAttributeLong(file_id, "host_id", host->cd.id);
		hdf5WriteAttributeLong(file_id, "sub_id", sub->cd.id);
		hdf5WriteAttributeLong(file_id, "host_orig_id", haloOriginalID(host));
		hdf5WriteAttributeLong(file_id, "sub_orig_id", haloOriginalID(sub));
		hdf5WriteAttributeInt(file_id, "snap_idx", snap_idx);
		hdf5WriteAttributeFloat(file_id, "snap_z", config.snap_z[snap_idx]);
		hdf5WriteAttributeFloat(file_id, "particle_mass", config.particle_mass);
		hdf5WriteAttributeFloat(file_id, "sub_R200m_bound", sub_R200m_bnd_com_mpc);
		hdf5WriteAttributeFloat(file_id, "subtrack_radius",
				sub_R200m_all_com_mpc * config.tcr_ptl_subtag_radius);
		hdf5WriteAttributeFloat(file_id, "smr", smr);

		/*
		 * Write a bit of history of the host and sub position and radii. We need to worry about
		 * periodic BCs; the coordinates should be non-periodic wrt the subhalo position at the
		 * final snapshot, so we compute a vector to that position and correct it if necessary
		 */
		n_hist_host = snap_idx - host->first_snap + 1;
		n_hist_sub = snap_idx - sub->first_snap + 1;
		n_hist_host = imin(n_hist_host, STCL_HALO_X + 1);
		n_hist_sub = imin(n_hist_sub, STCL_HALO_X + 1);

		host_x = (float **) memAlloc(__FFL__, MID_TCRPTL, n_hist_host * sizeof(float *));
		host_x[0] = (float *) memAlloc(__FFL__, MID_TCRPTL, 3 * n_hist_host * sizeof(float));
		host_R200m_all_array = (float *) memAlloc(__FFL__, MID_TCRPTL, n_hist_host * sizeof(float));

		sub_x = (float **) memAlloc(__FFL__, MID_TCRPTL, n_hist_sub * sizeof(float *));
		sub_x[0] = (float *) memAlloc(__FFL__, MID_TCRPTL, 3 * n_hist_sub * sizeof(float));
		sub_R200m_all_array = (float *) memAlloc(__FFL__, MID_TCRPTL, n_hist_sub * sizeof(float));
		sub_R200m_phys_array = (float *) memAlloc(__FFL__, MID_TCRPTL, n_hist_sub * sizeof(float));

		sub_x_final = haloX(sub, snap_idx, snap_idx);
		for (i = 0; i < n_hist_host; i++)
		{
			if (i != 0)
				host_x[i] = host_x[0] + i * 3;

			for (j = 0; j < 3; j++)
				host_x[i][j] = INVALID_X;
			s = snap_idx - n_hist_host + i + 1;
			host_x_t1 = haloX(host, s, snap_idx);
			periodicDistanceVector(sub_x_final, host_x_t1, host_vector);
			for (j = 0; j < 3; j++)
				host_x[i][j] = sub_x_final[j] + host_vector[j];
			host_R200m_all_array[i] = haloR200mComoving(host, s);
		}

		for (i = 0; i < n_hist_sub; i++)
		{
			if (i != 0)
				sub_x[i] = sub_x[0] + i * 3;

			for (j = 0; j < 3; j++)
				sub_x[i][j] = INVALID_X;
			s = snap_idx - n_hist_sub + i + 1;
			sub_x_t1 = haloX(sub, s, snap_idx);
			periodicDistanceVector(sub_x_final, sub_x_t1, sub_vector);
			for (j = 0; j < 3; j++)
				sub_x[i][j] = sub_x_final[j] + sub_vector[j];
			sub_R200m_all_array[i] = haloR200mComoving(sub, s);
			sub_R200m_phys_array[i] = haloR200m(sub, s) / 1000.0;
		}

		hdf5WriteDataset2D(file_id, "host_x", DTYPE_FLOAT, n_hist_host, 3, (void *) host_x[0]);
		hdf5WriteDataset(file_id, "host_v", DTYPE_FLOAT, 3, host->cd.v);
		hdf5WriteDataset(file_id, "host_R200m_all", DTYPE_FLOAT, n_hist_host, host_R200m_all_array);

		hdf5WriteDataset2D(file_id, "sub_x", DTYPE_FLOAT, n_hist_sub, 3, (void *) sub_x[0]);
		hdf5WriteDataset(file_id, "sub_v", DTYPE_FLOAT, 3, sub->cd.v);
		hdf5WriteDataset(file_id, "sub_R200m_all", DTYPE_FLOAT, n_hist_sub, sub_R200m_all_array);
		hdf5WriteDataset(file_id, "sub_R200m_all_phys", DTYPE_FLOAT, n_hist_sub,
				sub_R200m_phys_array);

		hdf5WriteDataset2D(file_id, "ptl_x", DTYPE_FLOAT, 3, n_ptl_all, (void *) ptl_x[0]);
		hdf5WriteDataset2D(file_id, "ptl_v", DTYPE_FLOAT, 3, n_ptl_all, (void *) ptl_v[0]);
		hdf5WriteDataset(file_id, "snap_t_dyn", DTYPE_FLOAT, config.n_snaps, config.snap_t_dyn);
		hdf5WriteDataset(file_id, "snap_t", DTYPE_FLOAT, config.n_snaps, config.snap_t);

		hdf5WriteDataset(file_id, "tagged", DTYPE_INT8, n_ptl_all, (void *) tagged_all);
		hdf5WriteDataset(file_id, "tagged_ifl_age", DTYPE_INT8, n_ptl_all,
				(void *) tagged_ifl_age_all);
		hdf5WriteDataset(file_id, "tagged_ifl_dist", DTYPE_INT8, n_ptl_all,
				(void *) tagged_ifl_dist_all);
		hdf5WriteDataset(file_id, "tagged_bound", DTYPE_INT8, n_ptl_all, (void *) tagged_bound_all);

		hdf5WriteDataset(file_id, "tcr_age", DTYPE_FLOAT, n_ptl_all, (void *) ifl_age_all);
		hdf5WriteDataset(file_id, "tcr_distance", DTYPE_FLOAT, n_ptl_all, (void *) ifl_dist_all);
		hdf5WriteDataset(file_id, "bound_ratio", DTYPE_FLOAT, n_ptl_all, (void *) bound_ratio_all);
#if DEBUG_OUTPUT_SUBTAGGING_DIRECTPOT
		hdf5WriteDataset(file_id, "bound_ratio_direct", DTYPE_FLOAT, n_ptl_all,
				(void *) bound_ratio_direct_all);
#endif

		H5Fclose(file_id);

		memFree(__FFL__, MID_TCRPTL, host_x[0], 3 * n_hist_host * sizeof(float));
		memFree(__FFL__, MID_TCRPTL, host_x, n_hist_host * sizeof(float *));
		memFree(__FFL__, MID_TCRPTL, host_R200m_all_array, n_hist_host * sizeof(float));

		memFree(__FFL__, MID_TCRPTL, sub_x[0], 3 * n_hist_sub * sizeof(float));
		memFree(__FFL__, MID_TCRPTL, sub_x, n_hist_sub * sizeof(float *));
		memFree(__FFL__, MID_TCRPTL, sub_R200m_all_array, n_hist_sub * sizeof(float));
		memFree(__FFL__, MID_TCRPTL, sub_R200m_phys_array, n_hist_sub * sizeof(float));
	}

	memFree(__FFL__, MID_TCRPTL, tagged_all, n_ptl_all * sizeof(int_least8_t));
	memFree(__FFL__, MID_TCRPTL, tagged_ifl_age_all, n_ptl_all * sizeof(int_least8_t));
	memFree(__FFL__, MID_TCRPTL, tagged_ifl_dist_all, n_ptl_all * sizeof(int_least8_t));
	memFree(__FFL__, MID_TCRPTL, tagged_bound_all, n_ptl_all * sizeof(int_least8_t));
	memFree(__FFL__, MID_TCRPTL, ifl_age_all, n_ptl_all * sizeof(float));
	memFree(__FFL__, MID_TCRPTL, ifl_dist_all, n_ptl_all * sizeof(float));
	memFree(__FFL__, MID_TCRPTL, bound_ratio_all, n_ptl_all * sizeof(float));
#if DEBUG_OUTPUT_SUBTAGGING_DIRECTPOT
	memFree(__FFL__, MID_TCRPTL, bound_ratio_direct_all, n_ptl_all * sizeof(float));
#endif
	memFree(__FFL__, MID_TCRPTL, ptl_x[0], 3 * n_ptl_all * sizeof(float));
	memFree(__FFL__, MID_TCRPTL, ptl_x, 3 * sizeof(float *));
	memFree(__FFL__, MID_TCRPTL, ptl_v[0], 3 * n_ptl_all * sizeof(float));
	memFree(__FFL__, MID_TCRPTL, ptl_v, 3 * sizeof(float *));
#endif

	/*
	 * Post-process tracers and ignore list in the host halo.
	 */
	if (do_tag_host_particles)
	{
		finalizeHostTracers(host, host_tcr_added, host_iid_added);
	}

	/*
	 * Post-process tracers and ignore list in the subhalo.
	 */
	if (do_create_sub_tracers)
	{
		finalizeSubTracers(sub, &iidDeleteList, sub_iid_changed, ts);
	}

	debugHalo(sub, "Finished subhalo particle tagging, tagged %d/%d considered particles.",
			tag_counter, n_ptl_all);
}

#endif
