/*************************************************************************************************
 *
 * This unit implements functions that are common to all types of tracers.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdarg.h>

#include "tracers.h"
#include "../config.h"
#include "../memory.h"
#include "../utils.h"
#include "../geometry.h"
#include "../halos/halo.h"
#include "../results/results.h"

/*************************************************************************************************
 * FUNCTIONS (PHYSICS)
 *************************************************************************************************/

/*
 * Compute the radius (or distance) of a tracer, taking periodic BCs into account. The radius
 * is in physical kpc / h, the coordinates must all be given in comoving Mpc/h box units.
 */
float tracerRadius(int snap_idx, float *halo_pos, float *tcr_pos)
{
	int d;
	float r_com, rhat_com[3], r_phys;

	periodicDistanceVector(halo_pos, tcr_pos, rhat_com);

	r_com = 0.0;
	for (d = 0; d < 3; d++)
		r_com += rhat_com[d] * rhat_com[d];
	r_com = sqrt(r_com);
	r_phys = physicalRadius(r_com, snap_idx);

	return r_phys;
}

/*
 * This function provides a simplified version of the full radius and velocity calculation below.
 * Here, we only calculate the physical radius and the total velocity.
 *
 * Note that the output units are v in physical kpc / h / Gyr, and r in physical kpc / h.
 */
void tracerRadiusVelocity(int snap_idx, float *halo_pos, float *halo_v, float *tcr_pos,
		float *tcr_v, float *out_r_phys, float *out_v_phys)
{
	int d;
	float r_com, rhat_com[3], v_pec_d, v_phys_d, v_phys2;

	periodicDistanceVector(halo_pos, tcr_pos, rhat_com);

	r_com = 0.0;
	v_phys2 = 0.0;
	for (d = 0; d < 3; d++)
	{
		v_pec_d = tcr_v[d] - halo_v[d];
		r_com += rhat_com[d] * rhat_com[d];
		v_phys_d = v_pec_d + rhat_com[d] * config.snap_aH[snap_idx];
		v_phys2 += v_phys_d * v_phys_d;
	}

	*out_r_phys = physicalRadius(sqrt(r_com), snap_idx);
	*out_v_phys = sqrt(v_phys2) * config.v_conversion;

	return;
}

/*
 * This function provides the full 3-velocity as well as radial velocity. Note that the output
 * units are v in physical kpc / h / Gyr.
 */
void tracerVelocityVector(int snap_idx, float *halo_pos, float *halo_v, float *tcr_pos,
		float *tcr_v, float *out_v_phys, float *out_v_phys_vec)
{
	int d;
	float rhat_com[3], v_pec_d, v_phys_d, v_phys2;

	periodicDistanceVector(halo_pos, tcr_pos, rhat_com);

	v_phys2 = 0.0;
	for (d = 0; d < 3; d++)
	{
		v_pec_d = tcr_v[d] - halo_v[d];
		v_phys_d = (v_pec_d + rhat_com[d] * config.snap_aH[snap_idx]) * config.v_conversion;
		v_phys2 += v_phys_d * v_phys_d;
		out_v_phys_vec[d] = v_phys_d;
	}

	*out_v_phys = sqrt(v_phys2);

	return;
}

/*
 * Compute the radius, radial velocity, tangential velocity etc for a tracer given its position
 * and velocity as well as the halo's. These coordinates must all be given in comoving Mpc/h box
 * units. Periodicity is taken into account by this function (although tracers resuting from tree
 * searches should already be corrected anyway).
 *
 * When computing the radial velocity, we need to take the Hubble flow into account as follows:
 *
 * r = a * x
 * rdot = a * xdot + adot * x = v_pec + a * H(a) * x
 *
 * Furthermore, we convert v to physical kpc / h / Gyr, and r to physical kpc / h.
 */
void tracerTrajectory(int snap_idx, Tracer *tcr, float *halo_pos, float *halo_v, float *tcr_pos,
		float *tcr_v, float halo_R, int advance_history)
{
	int i, d;
	float r_com, r_com_inv, rhat_com[3], r_phys, v_pec_d, v_phys_d[3], v_phys2, vr_kms, vr_phys,
			v_hubble;
#if DO_TRACER_VT
	float vt_kms, vt_phys;
#endif

	periodicDistanceVector(halo_pos, tcr_pos, rhat_com);

	r_com = 0.0;
	for (d = 0; d < 3; d++)
		r_com += rhat_com[d] * rhat_com[d];
	r_com = sqrt(r_com);
	r_phys = physicalRadius(r_com, snap_idx);

	r_com_inv = 1.0 / r_com;
	for (d = 0; d < 3; d++)
		rhat_com[d] *= r_com_inv;
	v_phys2 = 0.0;
	vr_kms = 0.0;
	v_hubble = config.snap_aH[snap_idx] * r_com;
	if (r_com > 0.0)
	{
		for (d = 0; d < 3; d++)
		{
			v_pec_d = tcr_v[d] - halo_v[d];
			v_phys_d[d] = v_pec_d + rhat_com[d] * v_hubble;
			vr_kms += v_pec_d * rhat_com[d];
			v_phys2 += v_phys_d[d] * v_phys_d[d];
		}
		vr_kms = (vr_kms + v_hubble);
		vr_phys = vr_kms * config.v_conversion;
#if DO_TRACER_VT
		vt_kms = sqrt(v_phys2 - vr_kms * vr_kms);
		if (isnan(vt_kms))
			vt_kms = 0.0;
		vt_phys = vt_kms * config.v_conversion;
#endif
	} else
	{
		vr_phys = 0.0;
#if DO_TRACER_VT
		vt_phys = 0.0;
#endif
	}

	/*
	 * There are some cases where a tracer has just been created or needs to be updated; in such
	 * cases, we do not advance the history.
	 */
	if (advance_history)
	{
		for (i = 0; i < STCL_TCR - 1; i++)
		{
			tcr->r[i] = tcr->r[i + 1];
			tcr->vr[i] = tcr->vr[i + 1];
#if DO_TRACER_VT
			tcr->vt[i] = tcr->vt[i + 1];
#endif
#if DO_TRACER_POS
			tcr->theta[i] = tcr->theta[i + 1];
			tcr->phi[i] = tcr->phi[i + 1];
#endif
		}
	}

	tcr->r[STCL_TCR - 1] = r_phys;
	tcr->vr[STCL_TCR - 1] = vr_phys;
#if DO_TRACER_VT
	tcr->vt[STCL_TCR - 1] = vt_phys;
#endif
#if DO_TRACER_POS
	tcr->theta[STCL_TCR - 1] = acos(rhat_com[2]);
	tcr->phi[STCL_TCR - 1] = atan2(rhat_com[1], rhat_com[0]);
#endif

	for (d = 0; d < 3; d++)
	{
#if DO_TRACER_X
		tcr->x[d] = rhat_com[d] * r_phys;
#endif
#if DO_TRACER_V
		tcr->v[d] = v_phys_d[d] * config.v_conversion;
#endif
	}

	/*
	 * Note that if the tracer is already in the halo due to a sub-tagging or other event, but has
	 * not crossed the halo radius, we need to update the status here if it has crossed. Note that
	 * it must be a <= rather than < condition because we use the former when assigning the tracer
	 * instructions.
	 */
	if ((tcr->status_entered != TCR_ENTERED_R200M) && (r_phys <= halo_R))
		tcr->status_entered = TCR_ENTERED_R200M;

	/*
	 * Steps that are executed only if this is the tracer's first snapshot. If we are being really
	 * paranoid, we can check whether they are initialized.
	 */
	if (tcr->first_snap == snap_idx)
	{
#if DO_TRACER_INITIAL_ANGLE
		tcr->theta_ini = acos(rhat_com[2]);
		tcr->phi_ini = atan2(rhat_com[1], rhat_com[0]);
#endif
	} else
	{
#if (PARANOID && DO_TRACER_INITIAL_ANGLE)
		if (tcr->theta_ini < -10.0)
			error(__FFL__,
					"Found uninitialized initial angle theta_ini in tracer ID %ld (first snap %d).\n",
					tcr->id, tcr->first_snap);
		if (tcr->phi_ini < -10.0)
			error(__FFL__,
					"Found uninitialized initial angle phi_ini in tracer ID %ld (first snap %d).\n",
					tcr->id, tcr->first_snap);
#endif
	}

	debugTracer(NULL, tcr,
			"Trajectory: got halo x [%.3f %.3f %.3f], tcr x [%.3f %.3f %.3f], r %.2f, vr %.2f, R %.2f, r/R %.2f, status_entered %d.",
			halo_pos[0], halo_pos[1], halo_pos[2], tcr_pos[0], tcr_pos[1], tcr_pos[2], r_phys,
			vr_phys, halo_R, r_phys / halo_R, tcr->status_entered);
}

void cartesianCoordinates(float r, float theta, float phi, float *x, float *y, float *z)
{
	float sintheta;

	sintheta = sin(theta);
	*x = r * sintheta * cos(phi);
	*y = r * sintheta * sin(phi);
	*z = r * cos(theta);
}

/*
 * In some cases, we want a unit vector pointing in the direction of the coordinates. In that
 * case, it makes no sense to multiply by r only to then divide by r.
 */
void cartesianCoordinatesUnit(float theta, float phi, float *x, float *y, float *z)
{
	float sintheta;

	sintheta = sin(theta);
	*x = sintheta * cos(phi);
	*y = sintheta * sin(phi);
	*z = cos(theta);
}

void sphericalCoordinates(float x, float y, float z, float *r, float *theta, float *phi)
{
	float r_tmp;

	r_tmp = sqrt(x * x + y * y + z * z);

	if (r != NULL)
		*r = r_tmp;
	if (theta != NULL)
	{
		if (r_tmp < 1E-20)
			*theta = acos(0.0);
		else
			*theta = acos(z / r_tmp);
	}
	if (phi != NULL)
		*phi = atan2(y, x);
}

void interpolateSphericalCoordinatesToX(float r1, float theta1, float phi1, float t1, float r2,
		float theta2, float phi2, float t2, float t, float *x, float *y, float *z)
{
	float x1, x2, y1, y2, z1, z2, frac;

	cartesianCoordinates(r1, theta1, phi1, &x1, &y1, &z1);
	cartesianCoordinates(r2, theta2, phi2, &x2, &y2, &z2);

	frac = (t - t1) / (t2 - t1);
#if CAREFUL
	if ((frac > 1.0001) || (frac < -0.0001))
		error(__FFL__, "Time to be interpolated (%.4f) outside interval [%.4f, %.4f].\n", t, t1,
				t2);
#endif

	*x = x1 + (x2 - x1) * frac;
	*y = y1 + (y2 - y1) * frac;
	*z = z1 + (z2 - z1) * frac;
}

void interpolateSphericalCoordinates(float r1, float theta1, float phi1, float t1, float r2,
		float theta2, float phi2, float t2, float t, float *r, float *theta, float *phi)
{
	float x, y, z;

	interpolateSphericalCoordinatesToX(r1, theta1, phi1, t1, r2, theta2, phi2, t2, t, &x, &y, &z);
	sphericalCoordinates(x, y, z, r, theta, phi);
}

/*************************************************************************************************
 * FUNCTIONS (TRACER OBJECTS)
 *************************************************************************************************/

void initTracer(Tracer *tcr)
{
	int i, rs;

	tcr->id = LARGE_ID;
#if DO_TRACER_SUBHALOS
	tcr->desc_id = INVALID_ID;
#endif
	tcr->status = TCR_STATUS_NONE;
	tcr->status_entered = TCR_ENTERED_NO;
	tcr->first_snap = -1;
	for (rs = 0; rs < NRS; rs++)
		tcr->status_rs[rs] = RS_STATUS_SEEKING;
	for (i = 0; i < STCL_TCR; i++)
	{
		tcr->r[i] = INVALID_R;
		tcr->vr[i] = INVALID_V;
#if DO_TRACER_VT
		tcr->vt[i] = INVALID_V;
#endif
#if DO_TRACER_INITIAL_ANGLE
		tcr->theta_ini = INVALID_ANGLE;
		tcr->phi_ini = INVALID_ANGLE;
#endif
#if DO_TRACER_POS
		tcr->theta[i] = INVALID_ANGLE;
		tcr->phi[i] = INVALID_ANGLE;
#endif
	}
#if DO_TRACER_X
	for (i = 0; i < 3; i++)
		tcr->x[i] = INVALID_X;
#endif
#if DO_TRACER_V
	for (i = 0; i < 3; i++)
		tcr->v[i] = INVALID_V;
#endif
#if DO_TRACER_SMR
	tcr->smr = INVALID_SMR;
#endif

#if DO_RESULT_SPLASHBACK
	tcr->n_up_crossings = 0;
	tcr->r_min = INVALID_R;
	tcr->R_min = INVALID_R;
#endif

#if DO_RESULT_ORBITCOUNT
	tcr->oct_direction = -1;
#endif
}

void initTracerType(TracerType *tt)
{
	int rs;

	initDynArray(__FFL__, &(tt->tcr), DA_TYPE_TRACER);
	initDynArray(__FFL__, &(tt->iid), DA_TYPE_ID);
	for (rs = 0; rs < NRS; rs++)
		initDynArray(__FFL__, &(tt->rs[rs]), rsprops[rs].da_type);
}

void freeTracerType(TracerType *tt)
{
	int rs;

	freeDynArray(__FFL__, &(tt->tcr));
	freeDynArray(__FFL__, &(tt->iid));
	for (rs = 0; rs < NRS; rs++)
		freeDynArray(__FFL__, &(tt->rs[rs]));
}

void initTracerRequest(TracerRequest *tr)
{
	tr->id = INVALID_ID;
	tr->halo_idx = INVALID_IDX;
	tr->tcr_idx = INVALID_IDX;
}

/*
 * When creating tracer objects, we need to check their starting conditions and translate them into
 * an initial status for each result.
 *
 * Note that this function does NOT check whether a tracer ID is on the ignore list! It is assumed
 * that that has already been done when this function is called.
 */
Tracer* createTracerObject(Halo *halo, int tt, ID tracer_id, int snap_idx, float *tracer_pos,
		int n_ignore_list, int is_recreated_tcr)
{
	int rs, instruction, rs_status[NRS], tcr_active;
	float tcr_r, R200m;
	Tracer *tcr = NULL;

	/*
	 * First check: the is_recreated_tcr flag indicates a revived particle tracer, but that can
	 * never happen with subhalo tracers, indicating an internal error.
	 */
#if CAREFUL
	if (ttSubhalos(tt) && is_recreated_tcr)
		error(__FFL__, "Subhalo tracers cannot be recreated.\n");
#endif

	/*
	 * Check the starting conditions of this tracer. Depending on whether the halo is new,
	 * whether the tracer is inside or outside the halo etc, the result statuses will vary. We also
	 * check whether the halo is becoming a subhalo, in which case the status can be entirely
	 * different.
	 */
	tcr_r = tracerRadius(snap_idx, halo->cd.x, tracer_pos);
	R200m = haloR200m(halo, snap_idx);

	if (isBecomingSub(halo))
	{
		instruction = INSTR_SUBTRACK_NEW;
	} else
	{
		if (tcr_r <= R200m)
		{
			if (halo->first_snap == snap_idx)
			{
				instruction = INSTR_IN_NEW;
			} else if ((snap_idx > 0) && !wasHostLastSnap(halo, snap_idx))
			{
				instruction = INSTR_IN_REBORN;
			} else
			{
				instruction = INSTR_IN;
			}
		} else
		{
			instruction = INSTR_OUT;
		}
	}

	/*
	 * Now go through results and check whether they are active for the whole halo, and whether
	 * there are pre-existing results. Technically, we do not have to set the status of a result
	 * if the halo ignores it anyway, but it would be ugly to have a status other than NOT_SEEKING
	 * in that case.
	 */
	tcr_active = 0;
	for (rs = 0; rs < NRS; rs++)
	{
		/*
		 * First, check whether this tracer result is turned off for the entire halo. If so, we
		 * set NOT_SEEKING regardless of the scenario
		 */
		if (halo->instr_rs[tt][rs])
		{
			rs_status[rs] = ttprops[tt].instr[rs][instruction];

			/*
			 * If this is a recreated tracer, we need to give the result a chance to overwrite
			 * the standard status.
			 */
			if (is_recreated_tcr)
			{
				if (ttprops[tt].instr[rs][INSTR_TCR_RECREATED] != RS_STATUS_MAINTAIN)
					rs_status[rs] = ttprops[tt].instr[rs][INSTR_TCR_RECREATED];
			}

			/*
			 * There is one more thing to check: if there is a pre-existing event and the given
			 * result specifies that an existing event means SUCCESS, then that overrides the
			 * other instructions.
			 *
			 * This part of the logic is not totally optimal because the status of a given
			 * result may depend on its history, e.g. previously saved events. However, such a
			 * logic gets complicated and can be checked inside the result. If the result has
			 * specified RS_STATUS_SEEKING for pre-existing results, it must be able to deal with
			 * such cases.
			 */
			if (ttprops[tt].instr[rs][INSTR_HAS_RES] == RS_STATUS_SUCCESS)
			{
				long int tmp_long;
				void *ptr;

				tmp_long = tracer_id;
				ptr = bsearch(&tmp_long, halo->tt[tt].rs[rs].data, halo->tt[tt].rs[rs].n,
						rsprops[rs].size, &compareResults);
				if (ptr != NULL)
					rs_status[rs] = RS_STATUS_SUCCESS;
			} else if (ttprops[tt].instr[rs][INSTR_HAS_RES] != RS_STATUS_SEEKING)
			{
				error(__FFL__,
						"Found status %d for instruction has_result, can only be success or seeking.\n",
						ttprops[tt].instr[rs][INSTR_HAS_RES]);
			}
		} else
		{
			rs_status[rs] = RS_STATUS_NOT_SEEKING;
		}

		/*
		 * Check that some status was set
		 */
		if (rs_status[rs] == RS_STATUS_NONE)
		{
			error(__FFL__, "Found RS_STATUS_NONE in halo ID %ld, tracer %ld, tt %d rs %d.\n",
					halo->cd.id, tracer_id, tt, rs);
		}

		if (resultShouldBeRun(rs_status[rs]))
			tcr_active = 1;

		/*
		 * Sanity check: not all statuses are allowed after initialization.
		 */
#if CAREFUL
		if (rs_status[rs] == RS_STATUS_NONE)
			error(__FFL__, "Internal error: Found RS_STATUS_NONE while creating tracer.\n");
		if (rs_status[rs] == RS_STATUS_MAINTAIN)
			error(__FFL__, "Internal error: Found RS_STATUS_MAINTAIN while creating tracer.\n");
#endif
	}

	/*
	 * Now there are two possibilities: we are creating the tracer or not. Reasons to create the
	 * tracer are either if it would be active, or if the halo is a subhalo in which case even
	 * inactive tracers are kept.
	 *
	 * If de do not keep it, we might need to put its ID on the ignore list though, otherwise
	 * tracers in a new or re-born halo might come back in the next snapshot and be mis-identified.
	 * Even if not, if we do not ignore this tracer it will be found (and attempted to be created)
	 * over and over again.
	 */
	if (tcr_active || isBecomingSub(halo))
	{
		tcr = addTracer(__FFL__, &(halo->tt[tt].tcr));
		tcr->id = tracer_id;
		tcr->status = TCR_STATUS_ACTIVE;
		tcr->first_snap = snap_idx;
		for (rs = 0; rs < NRS; rs++)
			tcr->status_rs[rs] = rs_status[rs];
		debugTracer(halo, tcr, "Created, first snap %d, instruction %d, r = %.2f = %.2f R200m.",
				tcr->first_snap, instruction, tcr_r, tcr_r / R200m);
	} else
	{
		addToIgnoreList(&(halo->tt[tt]), &tracer_id, n_ignore_list);
	}

	return tcr;
}

void printDebugTracer(Halo *halo, Tracer *tcr, const char *format, ...)
{
	if (tcr == NULL)
		return;

#if DO_TRACER_SUBHALOS
	if ((tcr->id == DEBUG_TRACER) || (tcr->desc_id == DEBUG_TRACER))
#else
	if (tcr->id == DEBUG_TRACER)
#endif
	{
		char msg[400], halostr[40];

		va_list args;
		va_start(args, format);
		vsprintf(msg, format, args);
		va_end(args);

		sprintf(halostr, "Halo");
		if (halo == NULL)
			sprintf(halostr, "%s        ?", halostr);
		else
			sprintf(halostr, "%s %8ld", halostr, haloOriginalID(halo));

#if DO_TRACER_SUBHALOS
		output(0, "[%4d] *** %s, Tracer %ld (desc_id %ld, status %d): %s\n", proc, halostr, tcr->id,
				tcr->desc_id, tcr->status, msg);
#else
		output(0, "[%4d] *** %s, Tracer %ld (status %d): %s\n", proc, halostr, tcr->id, tcr->status,
				msg);
#endif
	}
}

void printDebugTracerByID(long int tcr_id, const char *format, ...)
{
	if (tcr_id == DEBUG_TRACER)
	{
		char msg[400];

		va_list args;
		va_start(args, format);
		vsprintf(msg, format, args);
		va_end(args);

		output(0, "[%4d] *** Halo        ?, Tracer %ld: %s\n", proc, tcr_id, msg);
	}
}

int ttParticles(int tt)
{
	int ret = 0;

#if DO_TRACER_PARTICLES
	ret = (tt == PTL);
#endif

	return ret;
}

int ttSubhalos(int tt)
{
	int ret = 0;

#if DO_TRACER_SUBHALOS
	ret = (tt == SHO);
#endif

	return ret;
}

int isActive(Tracer *tcr)
{
	return ((tcr->status == TCR_STATUS_ACTIVE) || (tcr->status == TCR_STATUS_DELETE_MMP));
}

/*
 * Change all tracers in a halo to a particular instruction, for example because the halo has
 * become a ghost.
 */
void setTracerInstruction(Halo *halo, int instruction)
{
	int i, tt, rs, new_status;
	Tracer *tcr;

	for (tt = 0; tt < NTT; tt++)
	{
		/*
		 * If this halo cannot host subhalos, it also cannot host subhalo tracers.
		 */
		if (ttSubhalos(tt) && (!canHostSubhalos(halo)))
			continue;

		/*
		 * If this tracer type is turned off altogether, we need not even check the indiviual
		 * results.
		 */
		if (!halo->instr_tcr[tt])
			continue;

		for (i = 0; i < halo->tt[tt].tcr.n; i++)
		{
			tcr = &(halo->tt[tt].tcr.tcr[i]);

			if (tcr->status == TCR_STATUS_DELETE)
				continue;

			for (rs = 0; rs < NRS; rs++)
			{
				/*
				 * First, check whether this tracer result is turned off for the entire halo. If so, we
				 * set NOT_SEEKING regardless of the scenario
				 */
				if (!halo->instr_rs[tt][rs])
					continue;

				new_status = ttprops[tt].instr[rs][instruction];
				if (new_status == RS_STATUS_MAINTAIN)
				{
					debugTracer(halo, tcr,
							"Setting instruction %d for tracer %s result %s, found maintain, keeping status %d.",
							instruction, ttprops[tt].name_short, rsprops[rs].name_short,
							tcr->status_rs[rs]);
				} else
				{
					debugTracer(halo, tcr,
							"Setting instruction %d for tracer %s result %s, switching tracer from status %d to %d.",
							instruction, ttprops[tt].name_short, rsprops[rs].name_short,
							tcr->status_rs[rs], new_status);
					tcr->status_rs[rs] = new_status;
				}

				/*
				 * Sanity check: not all statuses are allowed.
				 */
#if CAREFUL
				if (tcr->status_rs[rs] == RS_STATUS_NONE)
					error(__FFL__,
							"Internal error: Found RS_STATUS_NONE while imposing instruction %d.\n",
							instruction);
#endif
			}
		}
	}
}

/*
 * This function marks tracers as ending if all their analyses have been completed. Note that the
 * individual tracer types may have additional criteria for ending tracers, so we should not
 * overwrite the previous DELETE status.
 */
void markEndingTracers(Halo *halo, LocalStatistics *ls)
{
	int i, tt, rs, all_rs_done;
	Tracer *tcr;

	for (tt = 0; tt < NTT; tt++)
	{
		/*
		 * We do not apply this function to particle tracers in subhalos because their tracers are
		 * kept even if no results are active.
		 */
		if (ttParticles(tt) && isSubPermanently(halo))
			continue;

		/*
		 * If this halo cannot host subhalos, it also cannot host subhalo tracers.
		 */
		if (ttSubhalos(tt) && (!canHostSubhalos(halo)))
			continue;

		/*
		 * If this tracer type is turned off altogether, we need not even check the indiviual
		 * results.
		 */
		if (!halo->instr_tcr[tt])
			continue;

		for (i = 0; i < halo->tt[tt].tcr.n; i++)
		{
			tcr = &(halo->tt[tt].tcr.tcr[i]);

			if (tcr->status == TCR_STATUS_DELETE)
				continue;

			all_rs_done = 1;
			for (rs = 0; rs < NRS; rs++)
			{
				if (halo->instr_rs[tt][rs] && resultIsNotDone(tcr->status_rs[rs]))
				{
					all_rs_done = 0;
					break;
				}
			}

			if (all_rs_done)
			{
				tcr->status = TCR_STATUS_DELETE;
				ls->tt[tt].n[TCRSTAT_ALL_RS_DONE]++;
				debugTracer(halo, tcr, "All results done, setting to delete status.");
			}
		}
	}
}

void addToIgnoreList(TracerType *tt, ID *id, int n_ignore_list)
{
	ParticleID *idp;

	idp = (ID*) bsearch(id, tt->iid.ids, n_ignore_list, sizeof(ID), &compareLongsAscending);
	if (idp == NULL)
	{
		idp = addId(__FFL__, &(tt->iid));
		*idp = *id;
	}
}

/*
 * Those tracers that are to be removed need to be added to the ignore list if they had entered
 * the halo. We add them if they have entered for any reason, as in, if they are considered to have
 * been part of the halo; not just if they actually crossed the halo radius.
 *
 * Note that we are not sorting the IID arrays in this function; they should still be sorted at the
 * beginning of the function, and don't need to be at the end.
 */
void updateIgnoreLists(Halo *halo)
{
	int i, tt, n_ignore_list;
	Tracer *tcr;

	for (tt = 0; tt < NTT; tt++)
	{
		n_ignore_list = halo->tt[tt].iid.n;

		for (i = 0; i < halo->tt[tt].tcr.n; i++)
		{
			tcr = &(halo->tt[tt].tcr.tcr[i]);
			if (tcr->status == TCR_STATUS_DELETE)
			{
				if (tcr->status_entered != TCR_ENTERED_NO)
				{
					addToIgnoreList(&(halo->tt[tt]), &(tcr->id), n_ignore_list);
					debugTracer(halo, tcr, "Has entered halo, adding to ignore list.");
				} else
				{
					debugTracer(halo, tcr, "Has not entered halo, not adding to ignore list.");
				}
			}
		}
	}
}

void deleteTracers(Halo *halo, LocalStatistics *ls)
{
	int i, tt;
	long int *stat_n;
	size_t ms_mask;
	int_least8_t *mask;
	Tracer *tcr;

	for (tt = 0; tt < NTT; tt++)
	{
		if (!halo->instr_tcr[tt])
			continue;

		stat_n = ls->tt[tt].n;
		ms_mask = sizeof(int_least8_t) * halo->tt[tt].tcr.n;
		mask = (int_least8_t*) memAlloc(__FFL__, MID_TRACERHANDLING, ms_mask);
		for (i = 0; i < halo->tt[tt].tcr.n; i++)
		{
			tcr = &(halo->tt[tt].tcr.tcr[i]);

			mask[i] = (tcr->status == TCR_STATUS_DELETE);
			if (mask[i])
			{
				stat_n[TCRSTAT_DELETED]++;
				debugTracer(halo, tcr, "Deleting.");
			}
		}
		deleteDynArrayItems(__FFL__, &(halo->tt[tt].tcr), mask);
		memFree(__FFL__, MID_TRACERHANDLING, mask, ms_mask);
	}
}

void checkTracerStatistics(LocalStatistics *ls)
{
	int tt;
	TracerStatistics *ts;

	for (tt = 0; tt < NTT; tt++)
	{
		ts = &(ls->tt[tt]);

		if (ttParticles(tt))
		{
			if (ts->n[TCRSTAT_NEW] + ts->n[TCRSTAT_RECREATED] + ts->n[TCRSTAT_IGNORED]
					+ ts->n[TCRSTAT_TRACKED] + ts->n[TCRSTAT_NOT_TRACKED]
					!= ts->n[TCRSTAT_PROCESSED])
			{
				error(__FFL__,
						"[%4d] Tracer %s: n_new %ld + n_recreated %ld + n_ignored %ld + n_tracked %ld + n_not_tracked %ld != n_processed %ld\n",
						proc, ttprops[tt].name_long, ts->n[TCRSTAT_NEW], ts->n[TCRSTAT_RECREATED],
						ts->n[TCRSTAT_IGNORED], ts->n[TCRSTAT_TRACKED], ts->n[TCRSTAT_NOT_TRACKED],
						ts->n[TCRSTAT_PROCESSED]);
			}
		}

		if (ts->n[TCRSTAT_HALO_BECAME_SUB] + ts->n[TCRSTAT_HALO_BECAME_GHOST]
				+ ts->n[TCRSTAT_LEFT_HALO] + ts->n[TCRSTAT_UNCONNECTED] + ts->n[TCRSTAT_ALL_RS_DONE]
				!= ts->n[TCRSTAT_DELETED])
		{
			error(__FFL__,
					"[%4d] Tracer %s: n_halo_became_sub %ld + n_halo_became_ghost %ld + n_left_halo %ld + n_unconnected %ld + n_all_results_done %ld != n_deleted %ld\n",
					proc, ttprops[tt].name_long, ts->n[TCRSTAT_HALO_BECAME_SUB],
					ts->n[TCRSTAT_HALO_BECAME_GHOST], ts->n[TCRSTAT_LEFT_HALO],
					ts->n[TCRSTAT_UNCONNECTED], ts->n[TCRSTAT_ALL_RS_DONE], ts->n[TCRSTAT_DELETED]);
		}
	}
}
