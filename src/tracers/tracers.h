/*************************************************************************************************
 *
 * This unit implements functions that are common to all types of tracers.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _TRACERS_H_
#define _TRACERS_H_

#include "../statistics.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * The status of tracer objects. This listing contains a few statuses that are designed for
 * particular tracer types.
 *
 * TCR_STATUS_NONE         Invalid status
 * TCR_STATUS_DELETE       Delete this tracer during this snapshot (typically at the end)
 * TCR_STATUS_CONNECT      Connect this tracer to the next snapshot. If this status is still set
 *                         after a connect routine, we know that the connection failed.
 * TCR_STATUS_ACTIVE       The standard status of tracers during a snapshot.
 * TCR_STATUS_DELETE_MMP   Particular to subhalo tracers. If a subhalo is not the most massive
 *                         progenitor (mmp), we know that it will merge away in the next snapshot
 *                         and that the tracer should be deleted.
 */
enum
{
	TCR_STATUS_NONE, TCR_STATUS_DELETE, TCR_STATUS_CONNECT, TCR_STATUS_ACTIVE, TCR_STATUS_DELETE_MMP
};

/*
 * A status indicating whether the tracer has entered the halo.
 */
enum
{
	TCR_ENTERED_NO, TCR_ENTERED_SUBTAG, TCR_ENTERED_R200M
};

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

float tracerRadius(int snap_idx, float *halo_pos, float *tcr_pos);
void tracerRadiusVelocity(int snap_idx, float *halo_pos, float *halo_v, float *tcr_pos,
		float *tcr_v, float *out_r_phys, float *out_v_phys);
void tracerVelocityVector(int snap_idx, float *halo_pos, float *halo_v, float *tcr_pos,
		float *tcr_v, float *out_v_phys, float *out_v_phys_vec);
void tracerEnergy(int snap_idx, Halo *halo, float r_phys, float v_phys, float *out_E_kin,
		float *out_E_pot);
void tracerTrajectory(int snap_idx, Tracer *tcr, float *halo_pos, float *halo_v, float *tcr_pos,
		float *tcr_v, float halo_R, int advance_history);

void cartesianCoordinates(float r, float theta, float phi, float *x, float *y, float *z);
void cartesianCoordinatesUnit(float theta, float phi, float *x, float *y, float *z);
void sphericalCoordinates(float x, float y, float z, float *r, float *theta, float *phi);
void interpolateSphericalCoordinatesToX(float r1, float theta1, float phi1, float t1, float r2,
		float theta2, float phi2, float t2, float t, float *x, float *y, float *z);
void interpolateSphericalCoordinates(float r1, float theta1, float phi1, float t1, float r2,
		float theta2, float phi2, float t2, float t, float *r, float *theta, float *phi);

void initTracer(Tracer *tp);
void initTracerType(TracerType *tt);
void freeTracerType(TracerType *tt);
void initTracerRequest(TracerRequest *tr);
Tracer* createTracerObject(Halo *halo, int tt, ID tracer_id, int snap_idx, float *tracer_pos,
		int n_ignore_list, int is_recreated_tcr);
void printDebugTracer(Halo *halo, Tracer *tcr, const char *format, ...);
void printDebugTracerByID(long int tcr_id, const char *format, ...);

int ttParticles(int tt);
int ttSubhalos(int tt);
int isActive(Tracer *tcr);

void setTracerInstruction(Halo *halo, int instruction);
void markEndingTracers(Halo *halo, LocalStatistics *ls);
void addToIgnoreList(TracerType *tt, ID *id, int n_ignore_list);
void updateIgnoreLists(Halo *halo);
void deleteTracers(Halo *halo, LocalStatistics *ls);

void checkTracerStatistics(LocalStatistics *ls);

#endif
