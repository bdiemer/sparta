/*************************************************************************************************
 *
 * This unit implements functions that are specific to particle tracers.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "tracer_particles.h"
#include "tracers.h"
#include "../config.h"
#include "../memory.h"
#include "../utils.h"
#include "../halos/halo.h"
#include "../results/results.h"

#if DO_TRACER_PARTICLES

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void connectParticles(Halo *halo, int snap_idx, TreeResults *tr_res, TracerStatistics *ts);
void markEndingParticles(Halo *halo, int snap_idx, TracerStatistics *ts);
void checkEnteredStatus(Halo *halo, int snap_idx);
void checkDuplicateParticles(Halo *halo);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Prepare all particle tracers for analysis. First, we check whether this halo was just born or
 * re-born as an isolated halo, in which case we put all particles currently in the halo on the
 * ignore list because we will not be able to follow their infall, splashback etc.
 *
 * Then we connect particles to the tracers known in the previous snapshot and create new tracers
 * where appropriate. Finally, we check for tracers that need to be deleted later and indicate that
 * in their status so that they do not get analyzed, as such analysis might give weird results.
 *
 * For host halos, we need to check that their has-entered status is correct. This field was set
 * in the trajectory calculation, but might have become outdated if the halo R200m has changed.
 */
void beforeTracerResultsParticles(Halo *halo, int snap_idx, Tree *tree, TreeResults *tr_res,
		TracerStatistics *ts)
{
	connectParticles(halo, snap_idx, tr_res, ts);
	markEndingParticles(halo, snap_idx, ts);

	if (isHost(halo))
		checkEnteredStatus(halo, snap_idx);

	/*
	 * At this point, all particles have been created / connected etc. If we want to be really
	 * paranoid, we can check that there are no duplicate tracers due to a bug.
	 */
#if PARANOID
	checkDuplicateParticles(halo);
#endif
}

/*
 * The routine exists for consistency, but there is nothing to do here for particles.
 */
void afterTracerResultsParticles(Halo *halo, int snap_idx, TracerStatistics *ts)
{

}

void connectParticles(Halo *halo, int snap_idx, TreeResults *tr_res, TracerStatistics *ts)
{
	int i, n_particles, n_tp_last_snap, n_ignore_last_step, added_tcr, changed_ignore_list,
			tcr_recreated, *ptr_int, is_host;
	float r_phys, R200m;
	ParticleID *idp;
	Particle *particle;
	Tracer temp_tcr, *tcr;
	DynArray iidDeleteList;

	/*
	 * Check: the tree results can be NULL if the halo is a sub with zero tracers. Otherwise, that
	 * should not be possible.
	 */
	if (tr_res == NULL)
	{
		if (isSubPermanently(halo) && (halo->tt[PTL].tcr.n == 0))
		{
			debugHalo(halo, "Found zero tracers, not connecting particles.");
			return;
		} else
		{
			error(__FFL__,
					"Found NULL for particle list while trying to connect tracers (halo ID %ld).",
					halo->cd.id);
		}
	}

	/*
	 * Note that we do not need to sort the particle tracer array here, even though tagging
	 * subhalo particles can add new particle tracers; that function should re-sort itself.
	 *
	 * It is important that we remember the number of sorted elements, since these lists get
	 * added to all the time. Unsorted elements will throw the bsearch function.
	 */
	is_host = isHost(halo);
	added_tcr = 0;
	changed_ignore_list = 0;
	n_tp_last_snap = halo->tt[PTL].tcr.n;
	n_ignore_last_step = halo->tt[PTL].iid.n;

	R200m = haloR200m(halo, snap_idx);
	n_particles = tr_res->num_points;
	ts->n[TCRSTAT_PROCESSED] += n_particles;

	/*
	 * If there is a chance that we will re-create tracers, than their IDs will need to be taken
	 * off the ignore list. We add their indices to a dynamic array of integers so that we do not
	 * need to keep a whole mask.
	 */
	if ((config.mightRecreateTracers) && is_host)
		initDynArray(__FFL__, &iidDeleteList, DA_TYPE_INT);

	/*
	 * Set the CONNECT status, but not for particles that were added in this snap
	 */
	for (i = 0; i < halo->tt[PTL].tcr.n; i++)
	{
#if CAREFUL
		if (halo->tt[PTL].tcr.tcr[i].status == TCR_STATUS_DELETE)
			error(__FFL__, "Found ignored particle at beginning of snap.\n");
#endif
		if (halo->tt[PTL].tcr.tcr[i].first_snap < snap_idx)
			halo->tt[PTL].tcr.tcr[i].status = TCR_STATUS_CONNECT;
	}

	/*
	 * Go through all particles found
	 */
	for (i = 0; i < n_particles; i++)
	{
		particle = tr_res->points[i];

		/*
		 * Check whether the particle is already being tracked.
		 */
		temp_tcr.id = particle->id;
		tcr = (Tracer*) bsearch(&temp_tcr, halo->tt[PTL].tcr.tcr, n_tp_last_snap, sizeof(Tracer),
				&compareTracers);

		if (tcr != NULL)
		{
			/*
			 * We are already tracking this particle. We set the status to ACTIVE to indicate that
			 * this particle was found, and compute the trajectory data.
			 *
			 * However, if the particle was created in this snap, its status remains unchanged and
			 * we decrease the PROCESSED counter because it was already increased for this
			 * particle when it was created. At that point, the trajectory should also have been
			 * set, so we do not repeat that operation -- unless the halo's x/v has been updated,
			 * in which case we need to update the trajectory (without advancing the history).
			 */
			if (tcr->first_snap == snap_idx)
			{
				ts->n[TCRSTAT_PROCESSED]--;
				if (halo->updated_xv)
					tracerTrajectory(snap_idx, tcr, halo->cd.x, halo->cd.v, particle->x,
							particle->v, haloR200m(halo, snap_idx), 0);
			} else
			{
				tcr->status = TCR_STATUS_ACTIVE;
				ts->n[TCRSTAT_TRACKED]++;
				tracerTrajectory(snap_idx, tcr, halo->cd.x, halo->cd.v, particle->x, particle->v,
						haloR200m(halo, snap_idx), 1);
			}
		} else if (!is_host)
		{
#if DO_GHOSTS && CAREFUL
			if (isGhost(halo))
				error(__FFL__,
						"Found non-tracked particle in ghost particles, halo %ld, particle %ld.\n",
						halo->cd.id, particle->id);
#endif
			/*
			 * If the particle does not exist and this is a subhalo, we do not create new tracers.
			 */
			ts->n[TCRSTAT_NOT_TRACKED]++;
		} else
		{
#if DO_GHOSTS && CAREFUL
			if (isGhost(halo))
				error(__FFL__,
						"Found non-tracked particle in ghost particles, halo %ld, particle %ld.\n",
						halo->cd.id, particle->id);
#endif

			/*
			 * We are not tracking this particle yet, there are a few possibilities:
			 *
			 * - the particle is outside the tracer creation radius; let's ignore it
			 * - the particle is inside the tracer creation radius, but on the ignore list; in this
			 *   case, we may need to ask the tracer result units whether they want the particle
			 *   tracer to be re-created.
			 * - the particle is inside the tracer creation radius and not on the ignore list.
			 *   Let's create it and set its initial trajectory data.
			 *
			 * Note that if the particle tracer cannot be created, there is a
			 * chance it was added to the ignore list, meaning we have to sort that list at the
			 * end of the function.
			 */
			r_phys = tracerRadius(snap_idx, halo->cd.x, particle->x);

			if (r_phys <= R200m * config.tcr_ptl_create_radius)
			{
				tcr_recreated = 0;

				/*
				 * Check if this particle is already in the ignore list. Generally, such particles
				 * should be ignored, i.e., no tracer created. However, there are certain tracer
				 * results that do want particles to be re-created under certain circumstances and
				 * provide a function to make this decision.
				 *
				 * If the tracer is re-created, we add the index (!) of this tracer on the ignore
				 * list to another list so that it will later be deleted.
				 */
				idp = (ParticleID*) bsearch(&particle->id, halo->tt[PTL].iid.ids,
						n_ignore_last_step, sizeof(ParticleID), &compareLongsAscending);
				if (idp != NULL)
				{
					if (config.mightRecreateTracers
							&& (checkRecreateIgnoredTracer(halo, snap_idx, *idp, PTL)))
					{
						tcr_recreated = 1;
						ptr_int = addInt(__FFL__, &iidDeleteList);
						*ptr_int = (idp - halo->tt[PTL].iid.ids);
					} else
					{
						ts->n[TCRSTAT_IGNORED]++;
						continue;
					}
				}

				/*
				 * Create the tracer. It is critical that we pass the tcr_recreated flag because it
				 * changes the status of a number of results.
				 */
				tcr = createTracerObject(halo, PTL, particle->id, snap_idx, particle->x,
						n_ignore_last_step, tcr_recreated);
				if (tcr != NULL)
				{
					tracerTrajectory(snap_idx, tcr, halo->cd.x, halo->cd.v, particle->x,
							particle->v, haloR200m(halo, snap_idx), 0);
					if (tcr_recreated)
						ts->n[TCRSTAT_RECREATED]++;
					else
						ts->n[TCRSTAT_NEW]++;
					added_tcr = 1;
				} else
				{
					ts->n[TCRSTAT_NOT_TRACKED]++;
					changed_ignore_list = 1;
					if (tcr_recreated)
						error(__FFL__, "Recreated tracer particle was not active, particle ID %ld.",
								particle->id);
				}
			} else
			{
				ts->n[TCRSTAT_NOT_TRACKED]++;
			}
		}
	}

	/*
	 * If we have re-created tracers, we need to take those particle IDs off the ignore list
	 * (they could stay there in principle, but it would be ugly to have existing tracers on the
	 * ignore list).
	 */
	if ((config.mightRecreateTracers) && is_host)
	{
		int_least8_t *mask;
		int ms_mask;

		ms_mask = sizeof(int_least8_t) * halo->tt[PTL].iid.n;
		mask = (int_least8_t*) memAlloc(__FFL__, MID_TCRPTL, ms_mask);
		memset(mask, 0, ms_mask);
		for (i = 0; i < iidDeleteList.n; i++)
			mask[iidDeleteList.ints[i]] = 1;
		deleteDynArrayItems(__FFL__, &(halo->tt[PTL].iid), mask);
		memFree(__FFL__, MID_TCRPTL, mask, ms_mask);

		changed_ignore_list = 1;

		freeDynArray(__FFL__, &iidDeleteList);
	}

	/*
	 * Re-sort arrays if necessary.
	 */
	if (added_tcr)
		qsort(halo->tt[PTL].tcr.tcr, halo->tt[PTL].tcr.n, sizeof(Tracer), &compareTracers);
	if (changed_ignore_list)
		qsort(halo->tt[PTL].iid.ids, halo->tt[PTL].iid.n, sizeof(ParticleID),
				&compareLongsAscending);
}

/*
 * Look for unconnected tracked particles and particles that have moved too far away from the halo.
 *
 * At the beginning of the snapshot, we did not know the final radius of some halos yet (namely of
 * subhalos and ghosts), meaning that we might have created tracers that are now outside the
 * tracking radius. In we detect such a case, namely that the tracer was created this snapshot, we
 * keep it because deleting it causes issues with infall events and the ignore list. Since the
 * tracer is presumably very near the tracking radius, it may well enter the halo anyway. Even
 * if not, it should hopefully not add a lot of work to keep it for an extra snapshot.
 */
void markEndingParticles(Halo *halo, int snap_idx, TracerStatistics *ts)
{
	int i;
	float max_track_factor, max_r_track;
	Tracer *tcr;

	if (isHost(halo))
		max_track_factor = config.tcr_ptl_delete_radius;
	else
		max_track_factor = config.tcr_ptl_subtag_radius;
	max_r_track = haloR200m(halo, snap_idx) * max_track_factor;

	for (i = 0; i < halo->tt[PTL].tcr.n; i++)
	{
		tcr = &(halo->tt[PTL].tcr.tcr[i]);

		if ((tcr->status != TCR_STATUS_DELETE) && (tcr->r[STCL_TCR - 1] > max_r_track))
		{
			if (tcr->first_snap == snap_idx)
			{
				debugTracer(halo, tcr,
						"Greater than max tracking radius (r = %.2f R200m, max track %.2f), but created this snap; keeping.",
						tcr->r[STCL_TCR - 1] / haloR200m(halo, snap_idx), max_track_factor);
			} else
			{
				tcr->status = TCR_STATUS_DELETE;
				ts->n[TCRSTAT_LEFT_HALO]++;
				debugTracer(halo, tcr,
						"Greater than max tracking radius (r = %.2f R200m, max track %.2f), setting to delete.",
						tcr->r[STCL_TCR - 1] / haloR200m(halo, snap_idx), max_track_factor);
			}
		} else if (tcr->status == TCR_STATUS_CONNECT)
		{
			tcr->status = TCR_STATUS_DELETE;
			ts->n[TCRSTAT_UNCONNECTED]++;
			debugTracer(halo, tcr, "Unconnected, setting to delete.");
		}
	}
}

/*
 * This function checks whether particle tracers have entered R200m, and sets the corresponding
 * status if necessary.
 */
void checkEnteredStatus(Halo *halo, int snap_idx)
{
	Tracer *tcr;
	int i;

	for (i = 0; i < halo->tt[PTL].tcr.n; i++)
	{
		tcr = &(halo->tt[PTL].tcr.tcr[i]);
		if (tcr->status_entered == TCR_ENTERED_NO)
		{
			if (tcr->r[STCL_TCR - 1] < haloR200m(halo, snap_idx))
			{
				tcr->status_entered = TCR_ENTERED_R200M;
				debugTracer(halo, tcr,
						"Adjusted entered_status to %d because tracer is inside adjusted R200m (r %.2f, R%.2f, rR %.2f).",
						TCR_ENTERED_R200M, tcr->r[STCL_TCR - 1], haloR200m(halo, snap_idx),
						tcr->r[STCL_TCR - 1]/haloR200m(halo, snap_idx));
			}
		}
	}
}

void checkDuplicateParticles(Halo *halo)
{
	int i;

	qsort(halo->tt[PTL].tcr.tcr, halo->tt[PTL].tcr.n, sizeof(Tracer), &compareTracers);
	for (i = 0; i < halo->tt[PTL].tcr.n - 1; i++)
	{
		if (halo->tt[PTL].tcr.tcr[i].id < 0)
			error(__FFL__, "Found particle ID %ld, idx %d, status %d, host %ld.\n",
					halo->tt[PTL].tcr.tcr[i].id, i, halo->tt[PTL].tcr.tcr[i].status, halo->cd.id);

		if (halo->tt[PTL].tcr.tcr[i].id == halo->tt[PTL].tcr.tcr[i + 1].id)
			error(__FFL__,
					"Found duplicate particle ID %ld, host %ld (orig ID %ld), idx %d/%d, status %d/%d.\n",
					halo->tt[PTL].tcr.tcr[i].id, halo->cd.id, haloOriginalID(halo), i, i + 1,
					halo->tt[PTL].tcr.tcr[i].status, halo->tt[PTL].tcr.tcr[i + 1].status);
	}
}

#endif
