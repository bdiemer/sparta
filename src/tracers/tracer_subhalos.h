/*************************************************************************************************
 *
 * This unit implements functions that are specific to subhalo tracers.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _TRACER_SUBHALOS_H_
#define _TRACER_SUBHALOS_H_

#include "../statistics.h"

#if DO_TRACER_SUBHALOS

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void addSubhaloTracer(int snap_idx, Halo *host, Halo *sub, TracerStatistics *ts);
void connectSubhalo(int snap_idx, Halo *halo, Tracer *tcr, HaloCatalogData *cd,
		TracerStatistics *ts);
void beforeTracerResultsSubhalos(Halo *halo, int snap_idx, TracerStatistics *ts);
void afterTracerResultsSubhalos(Halo *halo, int snap_idx, TracerStatistics *ts);

#endif

#endif
