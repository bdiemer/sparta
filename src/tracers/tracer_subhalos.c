/*************************************************************************************************
 *
 * This unit implements functions that are specific to subhalo tracers.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "tracer_subhalos.h"
#include "tracers.h"
#include "../config.h"
#include "../utils.h"
#include "../halos/halo.h"

#if DO_TRACER_SUBHALOS

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void checkSubhalos(Halo *halo);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Add a new subhalo tracer. This function needs to check whether the same subhalo is already being
 * traced, since subhalos can enter a given host multiple times (after having left temporarily).
 *
 * If the halo has existed in previous snapshots, it carries a small history of its position and
 * velocity which can be used to reconstruct the trajectory at previous snapshots.
 */
void addSubhaloTracer(int snap_idx, Halo *host, Halo *sub, TracerStatistics *ts)
{
	int i, sidx;
	Tracer *tcr, temp_tcr;
	HaloID tracer_id, *idp;

	/*
	 * Check a few trivial cases in which to abort this function: if the halo does not carry
	 * subhalo tracers, and if the subhalo is merging away in the next snap.
	 */
	if (!host->instr_tcr[SHO])
		return;

	if (!sub->cd.mmp)
		return;

	/*
	 * Check ignore list
	 */
	tracer_id = haloOriginalID(sub);
	idp = (HaloID*) bsearch(&tracer_id, host->tt[SHO].iid.ids, host->tt[SHO].iid.n, sizeof(HaloID),
			&compareLongsAscending);
	if (idp != NULL)
	{
		ts->n[TCRSTAT_IGNORED]++;
		return;
	}

	/*
	 * Check whether this tracer already exists; this happens if the same halo has fallen in before,
	 * and has not yet finished all its results.
	 */
	temp_tcr.id = tracer_id;
	tcr = (Tracer*) bsearch(&temp_tcr, host->tt[SHO].tcr.tcr, host->tt[SHO].tcr.n, sizeof(Tracer),
			&compareTracers);
	if (tcr != NULL)
	{
		debugTracer(host, tcr, "Found already existing tracer, not adding new one.");
		return;
	}

#if CAREFUL
	for (i = 0; i < host->tt[SHO].tcr.n; i++)
	{
		if ((host->tt[SHO].tcr.tcr[i].desc_id == sub->cd.desc_id)
				|| (host->tt[SHO].tcr.tcr[i].desc_id == sub->cd.id))
		{
			return;
		}
	}
#endif

	/*
	 * If all checks have been passed, add the tracer object using the original ID of the subhalo.
	 * Note that has_entered_halo is always 1 for subhalo tracers.
	 */
	tcr = createTracerObject(host, SHO, tracer_id, snap_idx, sub->cd.x, host->tt[SHO].iid.n, 0);
	if (tcr != NULL)
	{
		tcr->desc_id = sub->cd.desc_id;
		tcr->status_entered = TCR_ENTERED_R200M;
#if DO_TRACER_SMR
		tcr->smr = haloMassRatio(host, sub);
		debugTracer(host, tcr, "Subhalo tracer tagged with SMR %.3f.", tcr->smr);
#endif

#if CAREFUL
		float *host_x, *host_v, *sub_x, *sub_v;

		if ((host->first_snap < snap_idx) && (sub->first_snap < snap_idx))
		{
			host_x = haloX(host, snap_idx - 1, snap_idx);
			host_v = haloV(host, snap_idx - 1, snap_idx);
			sub_x = haloX(sub, snap_idx - 1, snap_idx);
			sub_v = haloV(sub, snap_idx - 1, snap_idx);

			for (i = 0; i < 3; i++)
			{
				if (host_x[i] < INVALID_INDICATOR)
					error(__FFL__, "Found invalid last x in host halo ID %ld, first snap %d.\n",
							host->cd.id, host->first_snap);
				if (host_v[i] < INVALID_INDICATOR)
					error(__FFL__, "Found invalid last v in host halo ID %ld, first snap %d.\n",
							host->cd.id, host->first_snap);
				if (sub_x[i] < INVALID_INDICATOR)
					error(__FFL__, "Found invalid last x in sub halo ID %ld, first snap %d.\n",
							sub->cd.id, sub->first_snap);
				if (sub_v[i] < INVALID_INDICATOR)
					error(__FFL__, "Found invalid last v in sub halo ID %ld, first snap %d.\n",
							sub->cd.id, sub->first_snap);
			}
		}
#endif

		/*
		 * Note that we have insured in the compile parameters that STCL_HALO_X and STCL_HALO_V
		 * go back far enough (2 snapshots). In principle, we could try to reconstruct the
		 * trajectory further if STCL_TCR is also larger.
		 *
		 * Note that history_x is in raw box coordinates, but the tracerTrajectory function takes
		 * periodic BCs into account.
		 */
		for (i = 0; i < 2; i++)
		{
			if ((host->history_x[i][0] > 0.0) && (sub->history_x[i][0] > 0.0))
			{
				sidx = snap_idx - 2 + i;
				tracerTrajectory(sidx, tcr, host->history_x[i], host->history_v[i],
						sub->history_x[i], sub->history_v[i], haloR200m(host, sidx), 1);
			}
		}
		tracerTrajectory(snap_idx, tcr, host->cd.x, host->cd.v, sub->cd.x, sub->cd.v,
				haloR200m(host, snap_idx), 1);
		ts->n[TCRSTAT_NEW]++;
		debugTracer(host, tcr, "Subhalo tracer added, mmp %d.", sub->cd.mmp);
	} else
	{
		/*
		 * If this tracer was not created, it might have been added to the ignore list which we
		 * then need to re-sort because this same function might be called on the same host halo.
		 */
		qsort(host->tt[SHO].iid.ids, host->tt[SHO].iid.n, sizeof(HaloID), &compareLongsAscending);
	}
}

/*
 * Given a subhalo and a new halo catalog record, update the subhalo.
 */
void connectSubhalo(int snap_idx, Halo *halo, Tracer *tcr, HaloCatalogData *cd,
		TracerStatistics *ts)
{
	debugTracer(halo, tcr, "Connecting, mmp %d.", cd->mmp);
	tcr->desc_id = cd->desc_id;
	tcr->status = TCR_STATUS_ACTIVE;
	tracerTrajectory(snap_idx, tcr, halo->cd.x, halo->cd.v, cd->x, cd->v, haloR200m(halo, snap_idx),
			1);

	if (!(cd->mmp))
	{
		tcr->status = TCR_STATUS_DELETE_MMP;
		debugTracer(halo, tcr, "Setting to be deleted later, mmp %d.", cd->mmp);
	}
}

void beforeTracerResultsSubhalos(Halo *halo, int snap_idx, TracerStatistics *ts)
{
}

void afterTracerResultsSubhalos(Halo *halo, int snap_idx, TracerStatistics *ts)
{
	int i;
	Tracer *tcr;

	for (i = 0; i < halo->tt[SHO].tcr.n; i++)
	{
		tcr = &(halo->tt[SHO].tcr.tcr[i]);

		if (tcr->status == TCR_STATUS_DELETE_MMP)
		{
			tcr->status = TCR_STATUS_DELETE;
			ts->n[TCRSTAT_UNCONNECTED]++;
		}
	}

#if PARANOID
	if (snap_idx < config.n_snaps - 1)
		checkSubhalos(halo);
#endif
}

/*
 * Note that this function is different from the general checkDuplicateTracers() function. First,
 * it also checks for invalid desc_ids, and second, it compares desc_id rather than id which might
 * be unique even though desc_id is no (for subhalo tracers).
 */
void checkSubhalos(Halo *halo)
{
	int i;

	qsort(halo->tt[SHO].tcr.tcr, halo->tt[SHO].tcr.n, sizeof(Tracer), &compareTracers);
	for (i = 0; i < halo->tt[SHO].tcr.n - 1; i++)
	{
		if (halo->tt[SHO].tcr.tcr[i].desc_id == INVALID_ID)
			error(__FFL__, "Found subhalo with ID %ld, host %ld.\n",
					halo->tt[SHO].tcr.tcr[i].desc_id, halo->cd.id);

		if (halo->tt[SHO].tcr.tcr[i].desc_id == halo->tt[SHO].tcr.tcr[i + 1].desc_id)
		{
			if ((halo->tt[SHO].tcr.tcr[i].status != TCR_STATUS_DELETE)
					&& (halo->tt[SHO].tcr.tcr[i + 1].status != TCR_STATUS_DELETE))
				error(__FFL__, "Found duplicate subhalo %ld, host %ld.\n",
						halo->tt[SHO].tcr.tcr[i].desc_id, halo->cd.id);
		}
	}
}

#endif
