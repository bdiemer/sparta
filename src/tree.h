/*************************************************************************************************
 *
 * This is fast3tree, a fast BSP tree implementation.
 *
 * (c) 2010 Peter Behroozi, Stanford University. Adapted for SPARTA by Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _TREE_H_
#define _TREE_H_

#include "global_types.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

#define TREE_TYPE Particle
#define TREE_DIM 3

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct
{
	float x[TREE_DIM];
} TreeDefaultPoint;

typedef struct TreeNode TreeNode;

struct TreeNode
{
	float min[TREE_DIM], max[TREE_DIM];
	int64_t num_points;
	int32_t div_dim;
	TreeNode *left, *right, *parent;
	TREE_TYPE *points;
};

typedef struct
{
	TREE_TYPE *points;
	int64_t num_points;
	TreeNode *root;
	int64_t num_nodes;
	int64_t allocated_nodes;
} Tree;

typedef struct
{
	int64_t num_points;
	int64_t num_allocated_points;
	TREE_TYPE **points;
} TreeResults;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

Tree* treeInit(int64_t n, TREE_TYPE *p);
void treeFree(Tree **t);

TreeResults* treeResultsInit(void);
void treeResultsAlloc(TreeResults *res, int n);
void treeResultsFree(TreeResults *res);

void treeRebuild(Tree *t, int64_t n, TREE_TYPE *p);
void treeMaxMinRebuild(Tree *t);

void treeFindSphere(Tree *t, TreeResults *res, float c[TREE_DIM], float r);
int treeFindSpherePeriodicSimple(Tree *t, TreeResults *res, float c[TREE_DIM], float r);
int treeFindSpherePeriodicCorrected(Tree *t, TreeResults *res, float c[TREE_DIM], float r);

#endif
