/*************************************************************************************************
 *
 * This unit implements a domain decomposition based on a space-filling curve. The Hilbert curve
 * algorithm is adapted from the ART code (Kravtsov et al. 1997, Rudd et al. 2008).
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <mpi.h>

#include "domain_sfc.h"
#include "domain.h"
#include "../global.h"
#include "../config.h"
#include "../utils.h"

#if DOMAIN_DECOMP_SFC

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

SFCDomain sfcDomain;
#if DOMAIN_SFC_INDEX_TABLE
SFCIndexLookup sfcIndexLookup;
#endif

/*************************************************************************************************
 * INTERNAL CONSTANTS
 *************************************************************************************************/

#define rollLeft(x,y,mask) ((x<<y) | (x>>(SFC_NDIM-y))) & mask
#define rollRight(x,y,mask) ((x>>y) | (x<<(SFC_NDIM-y))) & mask

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int mortonIndex(int coords[SFC_NDIM]);
int hilbertIndex_compute(int coords[SFC_NDIM]);
void hilbertCoords(int index, int coords[SFC_NDIM]);
void hilbertCoords_compute(int index, int coords[SFC_NDIM]);

void setEqualBoundaries();

#if DOMAIN_SFC_INDEX_TABLE
void createLookupTable();
#endif

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initialDomainDecomposition_sfc()
{
	int i;

	/*
	 * Compute the conversion between SFC indices and length once and for all.
	 */
	sfcDomain.index_conv = (float) SFC_MAX_1D / config.box_size;
	sfcDomain.index_conv_inv = 1.0 / sfcDomain.index_conv;

#if DOMAIN_SFC_INDEX_TABLE
	/*
	 * If desired, compute an index lookup table. This consumes memory, but makes the index
	 * computations faster.
	 */
	createLookupTable();
#endif

	/*
	 * Split the domain into equal segments of the Hilbert curve
	 */
	if (is_main_proc)
		setEqualBoundaries();

	/*
	 * Communicate the new boundaries to all procs
	 */
	MPI_Bcast(&sfcDomain, sizeof(SFCDomain), MPI_BYTE, MAIN_PROC, MPI_COMM_WORLD);

	/*
	 * Update this proc's box
	 */
	updateProcDomainBox_sfc();
	for (i = 0; i < 3; i++)
		proc_domain_box.periodic[i] = 0;
}

/*
 * Since we are using the SFC domain decomposition, we need no topology in the MPI communicator.
 */
void createMPICommunicator_sfc()
{
	comm = MPI_COMM_WORLD;
}

#if DOMAIN_SFC_INDEX_TABLE
/*
 * Brute-force compute the Hilbert index for each position in the SFC once, and store the results
 * both sorted by index and sorted by position.
 */
void createLookupTable()
{
	int i, j, k, coor[3], idx, ijk;

	for (i = 0; i < SFC_MAX_1D; i++)
	{
		coor[0] = i;
		for (j = 0; j < SFC_MAX_1D; j++)
		{
			coor[1] = j;
			for (k = 0; k < SFC_MAX_1D; k++)
			{
				coor[2] = k;
				idx = hilbertIndex_compute(coor);
				ijk = i * SFC_MAX_1D * SFC_MAX_1D + j * SFC_MAX_1D + k;
				sfcIndexLookup.idx[ijk] = idx;
				sfcIndexLookup.pos[idx * 3] = coor[0];
				sfcIndexLookup.pos[idx * 3 + 1] = coor[1];
				sfcIndexLookup.pos[idx * 3 + 2] = coor[2];
			}
		}
	}
}
#endif

/*
 * Given the set of Hilbert indices this proc is responsible for, compute the spatial extent of
 * it's volume. Since the Hilbert curve is contiguous, the space of indices on each process is
 * contiguous too, meaning there are no "holes" in the volumes. Furthermore, the resulting
 * volume is by definition non-periodic.
 */
void updateProcDomainBox_sfc()
{
	int i, j, idx_min, idx_max, max_coor[3], min_coor[3], coor[3];

	idx_min = sfcDomain.index_bdry[proc];
	idx_max = sfcDomain.index_bdry[proc + 1];

	for (i = 0; i < 3; i++)
	{
		min_coor[i] = SFC_MAX_1D;
		max_coor[i] = 0;
	}
	for (i = idx_min; i < idx_max; i++)
	{
		hilbertCoords(i, coor);
		for (j = 0; j < 3; j++)
		{
			min_coor[j] = imin(min_coor[j], coor[j]);
			max_coor[j] = imax(max_coor[j], coor[j]);
		}
	}

	for (j = 0; j < 3; j++)
	{
		proc_domain_box.min[j] = min_coor[j] * sfcDomain.index_conv_inv;
		proc_domain_box.max[j] = max_coor[j] * sfcDomain.index_conv_inv;
	}
}

/*
 * Split the domain into equal segments of Hilbert curve
 */
void setEqualBoundaries()
{
	int sfc_step, i;

	sfc_step = SFC_MAX / n_proc;
	for (i = 0; i < n_proc + 1; i++)
		sfcDomain.index_bdry[i] = i * sfc_step;
	for (i = n_proc + 1; i < MAX_PROCS + 1; i++)
		sfcDomain.index_bdry[i] = -1;

	sfcDomain.index_bdry[n_proc] = imin(sfcDomain.index_bdry[n_proc], SFC_MAX);
}

int indexFromPosition(float *x)
{
	int i, coords[3];

	for (i = 0; i < 3; i++)
		coords[i] = (int) floor(x[i] * sfcDomain.index_conv);

	return hilbertIndex(coords);
}

int procFromPosition_sfc(float *x)
{
	return procFromIndex(indexFromPosition(x));
}

int procFromIndex(int idx)
{
	int p;

	p = 0;
	while (p <= n_proc)
	{
		if (sfcDomain.index_bdry[p + 1] > idx)
			break;
		p++;
	}

	if (p == n_proc + 1)
		error(__FFL__,
				"Failed to assign proc from Hilbert curve, index %d, max Hilbert index %d.\n", idx,
				SFC_MAX);

	return p;
}

/*
 * Get the Morton (or z-order) index.
 */
int mortonIndex(int coords[SFC_NDIM])
{
	int i, d;
	int mortonnumber = 0;
	int bitMask = (int) 1 << (SFC_BITS_PER_DIM - 1);

	/* interleave bits of coordinates */
	for (i = SFC_BITS_PER_DIM; i > 0; i--)
	{
		for (d = 0; d < SFC_NDIM; d++)
		{
			mortonnumber |= (coords[d] & bitMask) << (((SFC_NDIM - 1) * i) - d);
		}
		bitMask >>= 1;
	}

	return mortonnumber;
}

/*
 * Get the index along a space-filling Hilbert curve. The algorithm is based on
 *
 * Alternative Algorithm for Hilbert's Space-Filling Curve,
 * A.R. Butz, IEEE Trans on Comp., p. 424, 1971
 */
int hilbertIndex_compute(int coords[SFC_NDIM])
{
	int i, j;
	int hilbertnumber;
	int singleMask;
	int dimMask;
	int numberShifts;
	int principal;
	int o;
	int rho;
	int w;
	int interleaved;

	/* begin by transposing bits */
	interleaved = mortonIndex(coords);

	/* mask out NDIM and 1 bit blocks starting
	 * at highest order bits */
	singleMask = 1 << ((SFC_BITS_PER_DIM - 1) * SFC_NDIM);

	dimMask = singleMask;
	for (i = 1; i < SFC_NDIM; i++)
	{
		dimMask |= singleMask << i;
	}

	w = 0;
	numberShifts = 0;
	hilbertnumber = 0;

	while (singleMask)
	{
		o = (interleaved ^ w) & dimMask;
		o = rollLeft(o, numberShifts, dimMask);

		rho = o;
		for (j = 1; j < SFC_NDIM; j++)
		{
			rho ^= (o >> j) & dimMask;
		}

		hilbertnumber |= rho;

		/* break out early (we already have complete number
		 * no need to calculate other numbers) */
		if (singleMask == 1)
		{
			break;
		}

		/* calculate principal position */
		principal = 0;
		for (i = 1; i < SFC_NDIM; i++)
		{
			if ((hilbertnumber & singleMask) != ((hilbertnumber >> i) & singleMask))
			{
				principal = i;
				break;
			}
		}

		/* complement lowest bit position */
		o ^= singleMask;

		/* force even parity by complementing at principal position if necessary
		 * Note: lowest order bit of hilbertnumber gives you parity of o at this
		 * point due to xor operations of previous steps */
		if (!(hilbertnumber & singleMask))
		{
			o ^= singleMask << principal;
		}

		/* rotate o right by numberShifts */
		o = rollRight(o, numberShifts, dimMask);

		/* find next numberShifts */
		numberShifts += (SFC_NDIM - 1) - principal;
		numberShifts %= SFC_NDIM;

		w ^= o;
		w >>= SFC_NDIM;

		singleMask >>= SFC_NDIM;
		dimMask >>= SFC_NDIM;
	}

	return hilbertnumber;
}

/*
 * This function should be used. If the table lookup is activated, it returns the fast lookup
 * version, if not it reverts to the actual computation.
 */
int hilbertIndex(int coords[SFC_NDIM])
{
#if DOMAIN_SFC_INDEX_TABLE
	return sfcIndexLookup.idx[coords[0] * SFC_MAX_1D * SFC_MAX_1D + coords[1] * SFC_MAX_1D
			+ coords[2]];
#else
	return hilbertIndex_compute(coords);
#endif
}

/*
 * The inverse of hilbertIndex, i.e. getting a position from an index.
 */
void hilbertCoords_compute(int index, int coords[SFC_NDIM])
{
	int i, j, dimMask, singleMask, sigma, sigma_, tau, tau_, num_shifts, principal, w, x = 0;

	assert(index >= 0);

	w = 0;
	sigma_ = 0;
	num_shifts = 0;

	singleMask = (int) 1 << ((SFC_BITS_PER_DIM - 1) * SFC_NDIM);

	dimMask = singleMask;
	for (i = 1; i < SFC_NDIM; i++)
	{
		dimMask |= singleMask << i;
	}

	for (i = 0; i < SFC_BITS_PER_DIM; i++)
	{
		sigma = ((index & dimMask) ^ ((index & dimMask) >> 1)) & dimMask;
		sigma_ |= rollRight(sigma, num_shifts, dimMask);

		principal = SFC_NDIM - 1;
		for (j = 1; j < SFC_NDIM; j++)
		{
			if ((index & singleMask) != ((index >> j) & singleMask))
			{
				principal = SFC_NDIM - j - 1;
				break;
			}
		}

		/* complement nth bit */
		tau = sigma ^ singleMask;

		/* if even parity, complement in principal bit position */
		if (!(index & singleMask))
		{
			tau ^= singleMask << (SFC_NDIM - principal - 1);
		}

		tau_ = rollRight(tau, num_shifts, dimMask);

		num_shifts += principal;
		num_shifts %= SFC_NDIM;

		w |= ((w & dimMask) ^ tau_) >> SFC_NDIM;

		dimMask >>= SFC_NDIM;
		singleMask >>= SFC_NDIM;
	}

	x = w ^ sigma_;

	/* undo bit interleaving to get coordinates */
	for (i = 0; i < SFC_NDIM; i++)
	{
		coords[i] = 0;

		singleMask = (int) 1 << (SFC_NBITS - 1 - i);

		for (j = 0; j < SFC_BITS_PER_DIM; j++)
		{
			if (x & singleMask)
			{
				coords[i] |= (int) 1 << (SFC_BITS_PER_DIM - j - 1);
			}
			singleMask >>= SFC_NDIM;
		}
	}
}

/*
 * This function should be used. If the table lookup is activated, it returns the fast lookup
 * version, if not it reverts to the actual computation.
 */
void hilbertCoords(int index, int coords[SFC_NDIM])
{
#if DOMAIN_SFC_INDEX_TABLE
	memcpy(coords, &(sfcIndexLookup.pos[3 * index]), 3 * sizeof(int));
#else
	return hilbertCoords_compute(index, coords);
#endif
}

#endif
