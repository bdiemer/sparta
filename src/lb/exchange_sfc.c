/*************************************************************************************************
 *
 * This unit implements the exchange of halos between processes for the space-filling curve based
 * domain decomposition. The exchange is done asynchronously, meaning that processes do not wait
 * for sent messages to be received.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stddef.h>

#include "exchange_sfc.h"
#include "exchange.h"
#include "domain.h"
#include "domain_sfc.h"
#include "../config.h"
#include "../memory.h"
#include "../halos/halo.h"

#if DOMAIN_DECOMP_SFC

/*************************************************************************************************
 * INTERNAL CONSTANTS
 *************************************************************************************************/

#define EXC_LOGLEVEL1 3
#define EXC_LOGLEVEL2 4

/*
 * If this switch is defined, the exchange process will be aborted after a certain wait time.
 * This introduces an extra MPI call though.
 */
//#define EXCHANGE_MAX_WAIT_TIME 50.0

/*
 * The following switches can be turned on to output extra information about particular
 * processes for debugging.
 */
//#define EXCHANGE_DEBUG_PROC_FROM 21
//#define EXCHANGE_DEBUG_PROC_TO 11

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void setExchangeTargets_sfc(DynArray *halos);
void doExchange(DynArray *halos, int *n_rcv_halos, int *n_rcv_subs, int *n_rcv_da);
int compareExchangeNumbers(const void *a, const void *b);
void sendHalos(DynArray *halos, HaloMessage *hmes, int *n_sent_halos, int *proc_done);
void receiveHalos(DynArray *halos, MPI_Status *mpi_status, int *n_recv_halos, int *n_rcv_subs,
		int *n_rcv_da, long int *msg_id);
void sendDynArray(short int proc_to, DynArrayMessage *halo_msg, size_t offset,
		MPI_Datatype *mpi_type, DynArrayMessage *msg, int tag);
int receiveDynArray(short int proc_from, Halo *h_rcv_buf, int n_rcv_halos, size_t offset,
		MPI_Datatype *mpi_type, int tag);
void freeHaloMessage(HaloMessage *hmes);
int freeDynArrayMessage(DynArrayMessage *msg);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Exchange halos between processes. Because of subhalos that need to be on the same process as
 * their host, the exchange is split into multiple stages. In particular:
 *
 * - All hosts are exchanged if they lie outside the tolerated domain boundaries.
 * - All subs are exchanged with their host (this includes subs who will become hosts in the next
 *   snap).
 */
void exchangeHalos_sfc(int snap_idx, DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs)
{
	int i, n_rcv_halos, n_rcv_subs, n_rcv_da[N_HALO_DA];

	/*
	 * Step 0: Collect statistics to compare to later.
	 */
#if CAREFUL
	collectHaloStatistics(halos, gs);
	collectTracerStatistics(halos, ls, gs);
#endif
	n_rcv_halos = 0;
	n_rcv_subs = 0;
	for (i = 0; i < N_HALO_DA; i++)
		n_rcv_da[i] = 0;

	/*
	 * Step 1: Figure out where host halos need to be sent.
	 */
	setExchangeTargets_sfc(halos);

	/*
	 * Step 2: Now assign subhalos the same procs as their hosts. At this point, some hosts are not
	 * on this proc, and we need to resolve them with the other procs.
	 */
	resolveLocalSubhaloDestinations(halos, NULL);
	resolveGlobalSubhaloDestinations(halos, snap_idx);

	/*
	 * Step 3: Now we are ready for the actual exchange step
	 */
	doExchange(halos, &n_rcv_halos, &n_rcv_subs, n_rcv_da);

	/*
	 * Check that everything went OK
	 */
#if CAREFUL
	checkExchangeResults(halos, ls, gs);
#endif

	/*
	 * Add exchange statistics to global stats
	 */
	updateExchangeStats(gs, n_rcv_halos, n_rcv_subs, n_rcv_da);
}

void setExchangeTargets_sfc(DynArray *halos)
{
	int i, sfc_idx, min_idx, max_idx;
	Halo *h;

	sortHalos(halos, 0);

	min_idx = sfcDomain.index_bdry[proc];
	max_idx = sfcDomain.index_bdry[proc + 1];

	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);

		if (isHost(h))
		{
			sfc_idx = indexFromPosition(h->cd.x);
			if ((sfc_idx < min_idx) || (sfc_idx >= max_idx))
			{
				h->cd.proc = procFromIndex(sfc_idx);
				debugHalo(h, "Marking for move to proc %d.", h->cd.proc);
			}
		}
	}
}

/*
 * Perform the actual exchange of halos between processes. It would be bad if all procs started to
 * send to proc 0, 1 etc. because that would increase the wait times for messages to be received.
 * Instead, all procs start with their largest halo chunk and move down to the smallest.
 *
 * The fundamental design principle of the exchange is that no process should use blocking calls
 * unless it is absolutely certain that they do not cause a mutual blockage between two processes.
 *
 * Furthermore, MPI_Wait and MPI_Test only check whether a message was picked up by the MPI
 * library, but NOT whether the message was actually received by the code on the other process.
 * Thus, we manually confirm the receipt of halo chunks with a confirmation message. In this case,
 * it is clear which message the confirmation refers to because every process can only ever send
 * one message to any other process.
 */
void doExchange(DynArray *halos, int *n_rcv_halos, int *n_rcv_subs, int *n_rcv_da)
{
	int i, p, n_waiting_mes, finished, hmes_idx, attempt_receive, n_proc_finished, has_completed,
			finish_sent, n_to_send_tot, n_rcvd_total, n_sent_total, n_sent_to_proc[n_proc],
			n_rcvd_from_proc[n_proc], proc_done, n_sent, n_rcvd, old_req_finished;
	short int proc_to, proc_from;
	long int confirmed_id;
	HaloMessage *hmes;
	MPI_Status mpi_status;
	MPI_Request done_request, proc_finish_requests[n_proc];
	SFCExchangeNumber exc_num[n_proc];
	HaloReceipt receipts[n_proc];

#ifdef EXCHANGE_MAX_WAIT_TIME
	double t0;
	if (is_main_proc)
		t0 = MPI_Wtime();
	MPI_Bcast(&t0, 1, MPI_DOUBLE, MAIN_PROC, comm);
#endif

#if CAREFUL
	int n_halos_before_exc = halos->n;
#endif

	/*
	 * Count how many halos we need to send to each proc, and sort descendingly. We also remember
	 * the mask of those halos, as we'll need to delete them eventually.
	 */
	n_to_send_tot = 0;
	for (i = 0; i < n_proc; i++)
	{
		exc_num[i].proc = i;
		exc_num[i].n_exc = 0;
		n_sent_to_proc[i] = 0;
		n_rcvd_from_proc[i] = 0;
		proc_finish_requests[i] = MPI_REQUEST_NULL;
		receipts[i].req = MPI_REQUEST_NULL;
		receipts[i].msg_id = -1;
	}
	for (i = 0; i < halos->n; i++)
	{
		p = halos->h[i].cd.proc;
		exc_num[p].n_exc++;
		if (p != proc)
			n_to_send_tot++;
	}
	qsort(exc_num, n_proc, sizeof(SFCExchangeNumber), &compareExchangeNumbers);

	/*
	 * Prepare message array and flags
	 */
	p = 0;
	n_waiting_mes = 0;
	hmes = (HaloMessage*) memAlloc(__FFL__, MID_EXCHANGEBUF,
			sizeof(HaloMessage) * config.snap_max_waiting_messages);
	for (i = 0; i < config.snap_max_waiting_messages; i++)
	{
		hmes[i].in_use = 0;
		hmes[i].allocated = 0;
		hmes[i].confirmed = 0;
		hmes[i].proc = -1;
		hmes[i].msg_id = -1;
	}
	n_proc_finished = 0;
	finished = 0;
	finish_sent = 0;
	n_sent_total = 0;
	n_rcvd_total = 0;

	/*
	 * Loop until all halos have been sent and received
	 */
	while (!finished)
	{
		/*
		 * If we are supposed to send any halos to this proc, and there are free message slots,
		 * send a message. Note that p does not correspond to a process but to an index in a list
		 * of processes.
		 */
		if ((p < n_proc) && (n_waiting_mes < config.snap_max_waiting_messages))
		{
			/*
			 * First check: if this is the local proc of there are no messages to send, continue
			 */
			proc_to = exc_num[p].proc;
			if ((proc_to == proc) || (exc_num[p].n_exc == 0))
			{
				p++;
				continue;
			}

			/*
			 * Find a free message object
			 */
			hmes_idx = 0;
			while (hmes[hmes_idx].in_use)
				hmes_idx++;

#ifdef EXCHANGE_DEBUG_PROC_FROM
			if ((proc == EXCHANGE_DEBUG_PROC_FROM) && (proc_to == EXCHANGE_DEBUG_PROC_TO))
			output(0, "time %.5f: Sending message from proc %d to proc %d\n",
					(MPI_Wtime() - t0) * 1000.0,
					EXCHANGE_DEBUG_PROC_FROM, EXCHANGE_DEBUG_PROC_TO);
#endif

			/*
			 * Set the proc and message ID in the message struct, then send halos. Only if the
			 * function indicates that all halos have been sent to this proc, we increase the
			 * process counter.
			 */
			hmes[hmes_idx].proc = proc_to;
			sendHalos(halos, &(hmes[hmes_idx]), &n_sent, &proc_done);
			n_sent_to_proc[proc_to] += n_sent;
			n_sent_total += n_sent;
			n_waiting_mes++;
			if (proc_done)
				p++;

#ifdef EXCHANGE_DEBUG_PROC_FROM
			if ((proc == EXCHANGE_DEBUG_PROC_FROM) && (proc_to == EXCHANGE_DEBUG_PROC_TO))
			output(0, "time %.5f: Done sending message from proc %d to proc %d\n",
					(MPI_Wtime() - t0) * 1000.0,
					EXCHANGE_DEBUG_PROC_FROM, EXCHANGE_DEBUG_PROC_TO);
#endif
		}

		/*
		 * Read messages, add to buffers
		 */
		attempt_receive = 1;
		while (attempt_receive)
		{
			MPI_Iprobe(MPI_ANY_SOURCE, HMES_TAG_HALO, comm, &has_completed, &mpi_status);
			if (has_completed)
			{
#ifdef EXCHANGE_DEBUG_PROC_FROM
				if ((proc == EXCHANGE_DEBUG_PROC_TO)
						&& (mpi_status.MPI_SOURCE == EXCHANGE_DEBUG_PROC_FROM))
				{
					int tmp_count;
					MPI_Get_count(&mpi_status, mpiTypes.halo, &tmp_count);
					output(0, "time %.5f: On proc %d, found message from %d, count %d.\n",
							(MPI_Wtime() - t0) * 1000.0, EXCHANGE_DEBUG_PROC_TO,
							EXCHANGE_DEBUG_PROC_FROM, tmp_count);
				}
#endif

				proc_from = mpi_status.MPI_SOURCE;
				if (receipts[proc_from].req == MPI_REQUEST_NULL)
				{
					old_req_finished = 1;
				} else
				{
					MPI_Test(&(receipts[proc_from].req), &old_req_finished, MPI_STATUS_IGNORE);
				}

				if (old_req_finished)
				{
					receiveHalos(halos, &mpi_status, &n_rcvd, n_rcv_subs, n_rcv_da,
							&(receipts[proc_from].msg_id));
					MPI_Isend(&(receipts[proc_from].msg_id), 1, MPI_LONG, proc_from, HMES_TAG_RCVD,
							comm, &(receipts[proc_from].req));
					n_rcvd_from_proc[proc_from] += n_rcvd;
					n_rcvd_total += n_rcvd;
				} else
				{
					attempt_receive = 0;
				}
			} else
			{
				attempt_receive = 0;
			}
		}

		/*
		 * Check for received messages, step 1.
		 *
		 * Here, we test whether MPI has completed the message sending process. Note that this
		 * does NOT mean that the recipient process actually picked up the message! It does,
		 * however, mean that the message has been transferred into an MPI buffer, and that we
		 * can free the message buffer.
		 */
		for (i = 0; i < config.snap_max_waiting_messages; i++)
		{
			if ((hmes[i].in_use) && (hmes[i].allocated))
			{
				if (hmes[i].msg_halos.req == MPI_REQUEST_NULL)
				{
					has_completed = 1;
				} else
				{
					MPI_Test(&(hmes[i].msg_halos.req), &has_completed, MPI_STATUS_IGNORE);
#ifdef EXCHANGE_DEBUG_PROC_FROM
					if ((proc == EXCHANGE_DEBUG_PROC_FROM)
							&& (hmes[i].proc == EXCHANGE_DEBUG_PROC_TO))
					{
						output(0, "time %.5f: Proc %d completed message %d to proc %d, %d halos.\n",
								(MPI_Wtime() - t0) * 1000.0, EXCHANGE_DEBUG_PROC_FROM, i,
								EXCHANGE_DEBUG_PROC_TO, hmes[i].msg_halos.n_items);
					}
#endif
				}

				if (has_completed)
					freeHaloMessage(&(hmes[i]));
			}
		}

		/*
		 * Check for received messages, step 2.
		 *
		 * Now we check for messages indicating that a recipient process has actually picked up
		 * and completed a halo message. To be extra careful, we avoid any blocking MPI calls and
		 * only free the halo message if all MPI messages test positive for completion.
		 */
		attempt_receive = 1;
		while (attempt_receive)
		{
			MPI_Iprobe(MPI_ANY_SOURCE, HMES_TAG_RCVD, comm, &has_completed, &mpi_status);
			if (has_completed)
			{
				proc_from = mpi_status.MPI_SOURCE;
				MPI_Recv(&confirmed_id, 1, MPI_LONG, proc_from, HMES_TAG_RCVD, comm,
						MPI_STATUS_IGNORE);
				for (i = 0; i < config.snap_max_waiting_messages + 1; i++)
				{
					if (i == config.snap_max_waiting_messages)
						error(__FFL__,
								"Could not find halo message record matching received confirmation from proc %d, ID %ld.\n",
								proc_from, confirmed_id);

					if ((hmes[i].in_use) && (hmes[i].proc == proc_from)
							&& (hmes[i].msg_id == confirmed_id))
					{
#ifdef EXCHANGE_DEBUG_PROC_FROM
						if ((proc == EXCHANGE_DEBUG_PROC_FROM)
								&& (hmes[i].proc == EXCHANGE_DEBUG_PROC_TO))
						{
							output(0,
									"time %.5f: Proc %d received confirmation for message %d to proc %d, %d halos.\n",
									(MPI_Wtime() - t0) * 1000.0, EXCHANGE_DEBUG_PROC_FROM, i,
									EXCHANGE_DEBUG_PROC_TO, hmes[i].msg_halos.n_items);
						}
#endif

						hmes[i].confirmed = 1;
						break;
					}
				}
			} else
			{
				attempt_receive = 0;
			}
		}

		/*
		 * Check for received messages, step 3.
		 *
		 * If a message has both been confirmed and deallocated, we open the slot again and delete
		 * the halos from the local buffer.
		 */
		for (i = 0; i < config.snap_max_waiting_messages; i++)
		{
			if ((hmes[i].in_use) && (hmes[i].allocated == 0) && (hmes[i].confirmed))
			{
				hmes[i].in_use = 0;
				hmes[i].confirmed = 0;
				hmes[i].proc = -1;
				hmes[i].msg_id = -1;
				n_waiting_mes--;
			}
		}

		/*
		 * Check for confirmations that have been picked up by MPI. This step does not need to be
		 * performed at this point, but eventually we will want to check that all requests are
		 * fulfilled.
		 */
		for (i = 0; i < n_proc; i++)
		{
			if (receipts[i].req != MPI_REQUEST_NULL)
				MPI_Test(&(receipts[i].req), &has_completed, MPI_STATUS_IGNORE);
		}

		/*
		 * If we have gone through each proc and have no waiting messages, we let the main proc
		 * know. Once all procs are done, we can stop the loop.
		 */
		if ((p >= n_proc) && (n_waiting_mes == 0) && (!finish_sent))
		{
#ifdef EXCHANGE_DEBUG_PROC_FROM
			if ((proc == EXCHANGE_DEBUG_PROC_FROM) || (proc == EXCHANGE_DEBUG_PROC_TO))
			output(0, "time %.5f: Proc %d sending complete signal to main.\n",
					(MPI_Wtime() - t0) * 1000.0, proc);
#endif
			MPI_Isend(NULL, 0, MPI_INT, MAIN_PROC, HMES_TAG_DONE, comm, &(done_request));
			finish_sent = 1;
			output(EXC_LOGLEVEL1, "[%4d]          Exchange: Sent complete signal.\n", proc);
		}

		/*
		 * On the main proc, check for procs that are done and count them. If all procs are
		 * send a message to abort the loop.
		 */
		if (is_main_proc)
		{
			attempt_receive = 1;
			while (attempt_receive)
			{
				MPI_Iprobe(MPI_ANY_SOURCE, HMES_TAG_DONE, comm, &has_completed, &mpi_status);
				if (has_completed)
				{
#ifdef EXCHANGE_DEBUG_PROC_FROM
					if ((proc == EXCHANGE_DEBUG_PROC_FROM) || (proc == EXCHANGE_DEBUG_PROC_TO))
					output(0, "time %.5f: Main received complete signal from %d.\n",
							(MPI_Wtime() - t0) * 1000.0, mpi_status.MPI_SOURCE);
#endif
					MPI_Recv(NULL, 0, MPI_INT, mpi_status.MPI_SOURCE, HMES_TAG_DONE, comm,
							MPI_STATUS_IGNORE);
					n_proc_finished++;
					output(EXC_LOGLEVEL1,
							"[Main]          Exchange: Got finished signal from proc %d, %d procs have finished.\n",
							mpi_status.MPI_SOURCE, n_proc_finished);
				} else
				{
					attempt_receive = 0;
				}
			}

			if (n_proc_finished == n_proc)
			{
				for (i = 0; i < n_proc; i++)
				{
#ifdef EXCHANGE_DEBUG_PROC_FROM
					if ((i == EXCHANGE_DEBUG_PROC_FROM) || (i == EXCHANGE_DEBUG_PROC_TO))
					output(0, "time %.5f: Main sending finish signal to %d.\n",
							(MPI_Wtime() - t0) * 1000.0, i);
#endif
					MPI_Isend(NULL, 0, MPI_INT, i, HMES_TAG_ALL_DONE, comm,
							&(proc_finish_requests[i]));
				}
				output(EXC_LOGLEVEL1,
						"[Main]          Exchange: Sent final signal to all procs.\n");
			}
		}

		/*
		 * On all procs, check whether the main proc has sent an abort message. If so, the done
		 * message from this proc must have been received, and we force completion of the process
		 * with MPI_Wait.
		 */
		MPI_Iprobe(MAIN_PROC, HMES_TAG_ALL_DONE, comm, &has_completed, &mpi_status);
		if (has_completed)
		{
#if CAREFUL
			if (!finish_sent)
				error(__FFL__, "Found abort message before done.\n");
#endif

#ifdef EXCHANGE_DEBUG_PROC_FROM
			if ((proc == EXCHANGE_DEBUG_PROC_FROM) || (proc == EXCHANGE_DEBUG_PROC_TO))
			output(0, "time %.5f: Proc %d received finish signal from main.\n",
					(MPI_Wtime() - t0) * 1000.0, proc);
#endif

			MPI_Recv(NULL, 0, MPI_INT, MAIN_PROC, HMES_TAG_ALL_DONE, comm, MPI_STATUS_IGNORE);
			MPI_Wait(&done_request, MPI_STATUS_IGNORE);
			finished = 1;
		}

		/*
		 * Check whether a time limit has been exceeded. In this case, one of the processes appears
		 * to have crashed.
		 */
#ifdef EXCHANGE_MAX_WAIT_TIME
		if ((MPI_Wtime() - t0) > EXCHANGE_MAX_WAIT_TIME)
		{
			/*
			 * First, output which proc is hanging, i.e. hasn't finished yet
			 */
			output(0, "Proc %2d, p %d, n_waiting_mes %d, finish_sent %d.\n", proc, p, n_waiting_mes,
					finish_sent);
			for (i = 0; i < config.snap_max_waiting_messages; i++)
			{
				if (hmes[i].in_use)
				{
					output(0, "Proc %2d, message %d in use, allocated %d, proc %d, n %d.\n", proc,
							i, hmes[i].allocated, hmes[i].proc, hmes[i].msg_halos.n_items);
				}
			}

			/*
			 * Wait a bit for each proc, output the communication they've had
			 */
			delay(500 + proc * 500);
			for (i = 0; i < n_proc; i++)
			{
				if ((n_sent_to_proc[i] != 0) || (n_rcvd_from_proc[i] != 0))
					output(0, "Proc %2d, exc with proc %2d sent %7d rcvd %7d.\n", proc, i,
							n_sent_to_proc[i], n_rcvd_from_proc[i]);
				if (receipts[i].req != MPI_REQUEST_NULL)
					output(0, "Proc %2d, exc with proc %2d, non-NULL request.\n", proc, i);
			}

			/*
			 * Eventually, throw an error to stop the code
			 */
			if (proc == MAIN_PROC)
			{
				delay(n_proc * 500 + 5000);
				error(__FFL__, "Exceeded time limit in exchange routine.\n");
			}

			t0 = MPI_Wtime();
		}
#endif
	}

	/*
	 * Force the completion of rcvd confirmations on all procs.
	 */
	for (i = 0; i < n_proc; i++)
	{
		if (receipts[i].req != MPI_REQUEST_NULL)
			MPI_Wait(&(receipts[i].req), MPI_STATUS_IGNORE);
	}

	/*
	 * Force the completion of the finish messages on main proc.
	 */
	if (is_main_proc)
	{
		for (i = 0; i < n_proc; i++)
		{
			if (proc_finish_requests[i] == MPI_REQUEST_NULL)
				error(__FFL__, "Found null proc finish request.\n");
			MPI_Wait(&(proc_finish_requests[i]), MPI_STATUS_IGNORE);
		}
	}

#ifdef EXCHANGE_DEBUG_PROC_FROM
	if ((proc == EXCHANGE_DEBUG_PROC_FROM) || (proc == EXCHANGE_DEBUG_PROC_TO))
	output(0, "time %.5f: Proc %d exited loop.\n", (MPI_Wtime() - t0) * 1000.0, proc);
#endif

#if PARANOID
	/*
	 * Check consistency locally.
	 */
	if (n_to_send_tot != n_sent_total)
		error(__FFL__, "Was supposed to send %d halos, but sent %d.\n", n_to_send_tot,
				n_sent_total);
	if (n_rcvd_total - n_sent_total != halos->n - n_halos_before_exc)
		error(__FFL__,
				"Gained %d halos (received %d, sent %d), but added %d to halo array (initial %d, now %d).\n",
				n_rcvd_total - n_sent_total, n_rcvd_total, n_sent_total,
				halos->n - n_halos_before_exc, n_halos_before_exc, halos->n);

	/*
	 * Check consistency globally. We comine the records of all exchanges on main.
	 */
	int all_n_sent[n_proc * n_proc];
	int all_n_rcvd[n_proc * n_proc];
	int j, i1, i2;

	MPI_Gather(n_sent_to_proc, n_proc, MPI_INT, all_n_sent, n_proc, MPI_INT, MAIN_PROC, comm);
	MPI_Gather(n_rcvd_from_proc, n_proc, MPI_INT, all_n_rcvd, n_proc, MPI_INT, MAIN_PROC, comm);

	if (is_main_proc)
	{
		for (i = 0; i < n_proc; i++)
		{
			for (j = 0; j < n_proc; j++)
			{
				i1 = n_proc * i + j;
				i2 = n_proc * j + i;
				if (all_n_sent[i1] != all_n_rcvd[i2])
					error(__FFL__, "Proc %d sent %d halos to proc %d, but %d were received.\n", i,
							all_n_sent[i1], j, all_n_rcvd[i2]);
			}
		}
	}
#endif

	/*
	 * Free memory
	 */
	memFree(__FFL__, MID_EXCHANGEBUF, hmes, sizeof(HaloMessage) * config.snap_max_waiting_messages);

	/*
	 * Set output
	 */
	*n_rcv_halos = n_rcvd_total;
}

/*
 * Send halos to a particular process. We begin by creating a mask and a halo buffer. Eventually,
 * that buffer as well as all DynArrays are sent with an asynchronous MPI send, and the buffers
 * are kept in the HaloMessage structure.
 *
 * Note that the messages struct must already contain the correct proc.
 */
void sendHalos(DynArray *halos, HaloMessage *hmes, int *n_sent_halos, int *proc_done)
{
	int i, tt, rs, al, reached_last_halo;
	long counter;
	short int proc_to;
	float cumulative_mem, halo_mem;
	int_least8_t *mask;
	Halo *h;
	size_t mask_size;

	/*
	 * Set some message fields
	 */
	proc_to = hmes->proc;
	hmes->in_use = 1;
	hmes->allocated = 1;
	hmes->msg_halos.n_items = 0;

	/*
	 * Create a mask of the halos to be sent. We try not to exceed the maximum message size,
	 * meaning we simply stop if a halo would make the total exceed the message size. However,
	 * if this halo is the only halo in the message, we need to try and send it anyway.
	 */
	mask_size = sizeof(int_least8_t) * halos->n;
	mask = (int_least8_t*) memAlloc(__FFL__, MID_EXCHANGEBUF, mask_size);
	memset(mask, 0, sizeof(int_least8_t) * halos->n);
	cumulative_mem = 0.0;
	reached_last_halo = 0;
	for (i = 0; i < halos->n; i++)
	{
		if (halos->h[i].cd.proc == proc_to)
		{
			halo_mem = MEGABYTE * haloMemory(&(halos->h[i]));
			if (cumulative_mem + halo_mem > config.exc_max_message_size)
			{
				if (hmes->msg_halos.n_items != 0)
				{
					break;
				} else
				{
					warningOrError(__FFL__, config.err_level_max_exchange_size,
							"[%4d] Exchange message size %.2f MB (for halo ID %ld to proc %d) larger than max %.2f MB. Sending anyway.\n",
							proc, halo_mem, halos->h[i].cd.id, proc_to,
							config.exc_max_message_size);
				}
			}
			mask[i] = 1;
			hmes->msg_halos.n_items++;
			cumulative_mem += halo_mem;
		}

		if (i == halos->n - 1)
			reached_last_halo = 1;
	}
	*proc_done = reached_last_halo;

#if CAREFUL
	if (hmes->msg_halos.n_items == 0)
		error(__FFL__,
				"Found zero halos to send to proc %d, should have marked proc as done earlier.\n",
				proc_to);
#endif

	/*
	 * We allocate the halo buffer using the MID_EXCHANGEBUF memory field because both the halo
	 * and DA message buffers will be freed by the same function below.
	 */
	hmes->msg_halos.n_bytes = hmes->msg_halos.n_items * sizeof(Halo);
	hmes->msg_halos.buffer = (char*) memAlloc(__FFL__, MID_EXCHANGEBUF, hmes->msg_halos.n_bytes);
	counter = 0;
	for (i = 0; i < halos->n; i++)
	{
		if (mask[i])
		{
			memcpy(&(hmes->msg_halos.buffer[counter]), &(halos->h[i]), sizeof(Halo));
			counter += sizeof(Halo);
		}
	}

	output(EXC_LOGLEVEL2,
			"[%4d]          Exchange: Sending halos to proc %d, allocated %ld bytes.\n", proc,
			proc_to, hmes->msg_halos.n_bytes);

	/*
	 * Set the ID field as the ID of the first sent halo. We store the number of halos sent for the
	 * delete procedure later.
	 */
	h = (Halo*) &(hmes->msg_halos.buffer[0]);
	hmes->msg_id = h->cd.id;
	*n_sent_halos = hmes->msg_halos.n_items;

	/*
	 * Send the halo message and dynamica arrays
	 */
	output(EXC_LOGLEVEL2, "[%4d]          Exchange: Sending halos to proc %d, copied to buffer.\n",
			proc, proc_to);

	MPI_Isend(hmes->msg_halos.buffer, hmes->msg_halos.n_items, mpiTypes.halo, proc_to,
	HMES_TAG_HALO, comm, &(hmes->msg_halos.req));

	output(EXC_LOGLEVEL2, "[%4d]          Exchange: Sending halos to proc %d, sending DA subs.\n",
			proc, proc_to);

	sendDynArray(proc_to, &(hmes->msg_halos), offsetof(Halo, subs), &(mpiTypes.sub),
			&(hmes->msg_subs), HMES_TAG_SUBS);
	for (tt = 0; tt < NTT; tt++)
	{
		output(EXC_LOGLEVEL2,
				"[%4d]          Exchange: Sending halos to proc %d, sending DA tcr %d.\n", proc,
				proc_to, tt);
		sendDynArray(proc_to, &(hmes->msg_halos), offsetof(Halo, tt[tt].tcr), &(mpiTypes.tcr),
				&(hmes->msg_tcr[tt]), HMES_TAG_TCR + tt);
		output(EXC_LOGLEVEL2,
				"[%4d]          Exchange: Sending halos to proc %d, sending DA iid %d.\n", proc,
				proc_to, tt);
		sendDynArray(proc_to, &(hmes->msg_halos), offsetof(Halo, tt[tt].iid), &(mpiTypes.iid),
				&(hmes->msg_iid[tt]), HMES_TAG_IID + tt);
		for (rs = 0; rs < NRS; rs++)
		{
			output(EXC_LOGLEVEL2,
					"[%4d]          Exchange: Sending halos to proc %d, sending DA rs %d %d.\n",
					proc, proc_to, tt, rs);
			sendDynArray(proc_to, &(hmes->msg_halos), offsetof(Halo, tt[tt].rs[rs]),
					&(mpiTypes.rs[rs]), &(hmes->msg_rs[tt][rs]), HMES_TAG_RS + tt * NRS + rs);
		}
	}
	for (al = 0; al < NAL; al++)
	{
		output(EXC_LOGLEVEL2,
				"[%4d]          Exchange: Sending halos to proc %d, sending DA al %d.\n", proc,
				proc_to, al);

		sendDynArray(proc_to, &(hmes->msg_halos), offsetof(Halo, al[al]), &(mpiTypes.al[al]),
				&(hmes->msg_al[al]), HMES_TAG_AL + al);
	}
	output(EXC_LOGLEVEL2, "[%4d]          Exchange: Sending halos to proc %d, sent DA messages.\n",
			proc, proc_to);

	/*
	 * Delete halos, free memory
	 */
	deleteHalos(halos, mask);
	memFree(__FFL__, MID_EXCHANGEBUF, mask, mask_size);

	output(EXC_LOGLEVEL2, "[%4d]          Exchange: Finished sending %d halos to proc %d.\n", proc,
			hmes->msg_halos.n_items, proc_to);
}

/*
 * Send all items in dynamic arrays of a particular kind.
 */
void sendDynArray(short int proc_to, DynArrayMessage *halo_msg, size_t offset,
		MPI_Datatype *mpi_type, DynArrayMessage *msg, int tag)
{
	int i, size, array_pos;
	char *h;
	DynArray *da;
	Halo *h_buffer;

	// Count items to send
	msg->n_items = 0;
	size = 1;
	h_buffer = (Halo*) halo_msg->buffer;
	for (i = 0; i < halo_msg->n_items; i++)
	{
		h = (char*) (&(h_buffer[i]));
		da = (DynArray*) (h + offset);
		msg->n_items += da->n;
		if (size == 1)
			size = da->size;
	}
	msg->n_bytes = (size_t) msg->n_items * size;

	output(EXC_LOGLEVEL2, "[%4d]          Exchange: Sending DA to proc %d, counted %ld bytes.\n",
			proc, proc_to, msg->n_bytes);

	// Create and fill buffer
	msg->buffer = memAlloc(__FFL__, MID_EXCHANGEBUF, msg->n_bytes);
	array_pos = 0;
	for (i = 0; i < halo_msg->n_items; i++)
	{
		h = (char*) (&(h_buffer[i]));
		da = (DynArray*) (h + offset);
		memcpy(msg->buffer + (size_t ) array_pos * size, da->bytes, (size_t ) size * da->n);
		array_pos += da->n;
	}

	output(EXC_LOGLEVEL2,
			"[%4d]          Exchange: Sending DA to proc %d, created buffer, array_pos %ld, size %d.\n",
			proc, proc_to, array_pos, size);

	// Send
	MPI_Isend(msg->buffer, msg->n_items, *mpi_type, proc_to, tag, comm, &(msg->req));
}

void receiveHalos(DynArray *halos, MPI_Status *mpi_status, int *n_recv_halos, int *n_rcv_subs,
		int *n_rcv_da, long int *msg_id)
{
	int i, tt, rs, al, n_recv;
	short int proc_id;
	Halo *h_rcv_buf, *h;

	proc_id = mpi_status->MPI_SOURCE;
	MPI_Get_count(mpi_status, mpiTypes.halo, &n_recv);

	if (n_recv == 0)
		error(__FFL__, "Received halo message with 0 halos.\n");

	output(EXC_LOGLEVEL2, "[%4d]          Exchange: Receiving %d halos from proc %d.\n", proc,
			n_recv, proc_id);

	h_rcv_buf = (Halo*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(Halo) * n_recv);

	/*
	 * Receive halos, message ID is the ID of the first halo
	 */
	MPI_Recv(h_rcv_buf, n_recv, mpiTypes.halo, proc_id, HMES_TAG_HALO, comm, MPI_STATUS_IGNORE);
	h = (Halo*) h_rcv_buf;
	*msg_id = h->cd.id;

	/*
	 * Now receive dynamic arrays
	 */
	*n_rcv_subs += receiveDynArray(proc_id, h_rcv_buf, n_recv, offsetof(Halo, subs),
			&(mpiTypes.sub),
			HMES_TAG_SUBS);
	for (tt = 0; tt < NTT; tt++)
	{
		n_rcv_da[(2 + NRS) * tt + 0] += receiveDynArray(proc_id, h_rcv_buf, n_recv,
				offsetof(Halo, tt[tt].tcr), &(mpiTypes.tcr),
				HMES_TAG_TCR + tt);
		n_rcv_da[(2 + NRS) * tt + 1] += receiveDynArray(proc_id, h_rcv_buf, n_recv,
				offsetof(Halo, tt[tt].iid), &(mpiTypes.iid),
				HMES_TAG_IID + tt);
		for (rs = 0; rs < NRS; rs++)
			n_rcv_da[(2 + NRS) * tt + 2 + rs] += receiveDynArray(proc_id, h_rcv_buf, n_recv,
					offsetof(Halo, tt[tt].rs[rs]), &(mpiTypes.rs[rs]),
					HMES_TAG_RS + tt * NRS + rs);
	}
	for (al = 0; al < NAL; al++)
		n_rcv_da[(2 + NRS) * NTT + al] += receiveDynArray(proc_id, h_rcv_buf, n_recv,
				offsetof(Halo, al[al]), &(mpiTypes.al[al]), HMES_TAG_AL + al);

	for (i = 0; i < n_recv; i++)
	{
		h = addHalo(__FFL__, halos, 0);
		memcpy(h, &(h_rcv_buf[i]), sizeof(Halo));
	}

	memFree(__FFL__, MID_EXCHANGEBUF, h_rcv_buf, sizeof(Halo) * n_recv);

	*n_recv_halos = n_recv;

	output(EXC_LOGLEVEL2, "[%4d]          Exchange: Finished receiving %d halos from proc %d.\n",
			proc, n_recv, proc_id);
}

int receiveDynArray(short int proc_from, Halo *h_rcv_buf, int n_rcv_halos, size_t offset,
		MPI_Datatype *mpi_type, int tag)
{
	int i, array_pos, n_rcv_da, size;
	char *da_rcv_buf, *h;
	DynArray *da;

	// Count number of elements to receive
	size = 1;
	n_rcv_da = 0;
	for (i = 0; i < n_rcv_halos; i++)
	{
		h = (char*) (&(h_rcv_buf[i]));
		da = (DynArray*) (h + offset);
		n_rcv_da += da->n;
		if (size == 1)
			size = da->size;
	}

	output(EXC_LOGLEVEL2,
			"[%4d]          Exchange: Receiving DA from proc %d, created buffer, n_rcv_da %d, size %d, total %ld.\n",
			proc, proc_from, n_rcv_da, size, (size_t) size * n_rcv_da);

	// Create buffer, receive message
	da_rcv_buf = memAlloc(__FFL__, MID_EXCHANGEBUF, (size_t) size * n_rcv_da);
	MPI_Recv(da_rcv_buf, n_rcv_da, *mpi_type, proc_from, tag, comm, MPI_STATUS_IGNORE);

	// Shift elements into dynamic arrays
	array_pos = 0;
	for (i = 0; i < n_rcv_halos; i++)
	{
		h = (char*) (&(h_rcv_buf[i]));
		da = (DynArray*) (h + offset);
		resetDynArray(__FFL__, da);
#if CAREFUL
		if (da->n_alloc < da->n)
		{
			error(__FFL__, "DynArray exchange %d %d size %d n rcv %d\n", da->n_alloc, da->n, size,
					n_rcv_halos);
		}
#endif
		memcpy(da->bytes, da_rcv_buf + (size_t ) array_pos * size, (size_t ) da->n * size);
		array_pos += da->n;
	}

	// Free buffer
	memFree(__FFL__, MID_EXCHANGEBUF, da_rcv_buf, (size_t) size * n_rcv_da);

	return n_rcv_da;
}

/*
 * This function might be called any time after the first (halo) message has been received. Thus, we
 * need to make sure that all messages were completed before freeing the buffer. This function does
 * not block: if any message has not been completed it returns 0.
 */
void freeHaloMessage(HaloMessage *hmes)
{
	int ret, success, tt, rs, al;

	if (!hmes->allocated)
		return;

	success = 1;

	// Free halo buffer
	ret = freeDynArrayMessage(&(hmes->msg_halos));
	success = (success && ret);

	// Free dyn array buffers
	ret = freeDynArrayMessage(&(hmes->msg_subs));
	success = (success && ret);
	for (tt = 0; tt < NTT; tt++)
	{
		ret = freeDynArrayMessage(&(hmes->msg_tcr[tt]));
		success = (success && ret);
		ret = freeDynArrayMessage(&(hmes->msg_iid[tt]));
		success = (success && ret);
		for (rs = 0; rs < NRS; rs++)
		{
			ret = freeDynArrayMessage(&(hmes->msg_rs[tt][rs]));
			success = (success && ret);
		}
	}
	for (al = 0; al < NAL; al++)
	{
		ret = freeDynArrayMessage(&(hmes->msg_al[al]));
		success = (success && ret);
	}

	if (success)
		hmes->allocated = 0;
}

/*
 * Make sure the request has been completed before deleting the buffer, or that the request is
 * already NULL.
 */
int freeDynArrayMessage(DynArrayMessage *msg)
{
	int ret, has_completed;

	ret = 1;
	has_completed = 0;

	if (msg->req == MPI_REQUEST_NULL)
	{
		has_completed = (msg->n_bytes > 0);
	} else
	{
		MPI_Test(&(msg->req), &has_completed, MPI_STATUS_IGNORE);
		if (!has_completed)
			ret = 0;
	}

	if (has_completed)
	{
		memFree(__FFL__, MID_EXCHANGEBUF, msg->buffer, msg->n_bytes);
		msg->n_bytes = 0;
		msg->n_items = 0;
	}

	return ret;
}

/*
 * This function sorts descendingly because we want the highest number of halos to send first.
 */
int compareExchangeNumbers(const void *a, const void *b)
{
	if (((SFCExchangeNumber*) a)->n_exc > ((SFCExchangeNumber*) b)->n_exc)
		return -1;
	else if (((SFCExchangeNumber*) a)->n_exc < ((SFCExchangeNumber*) b)->n_exc)
		return 1;
	else
		return 0;
}

#endif
