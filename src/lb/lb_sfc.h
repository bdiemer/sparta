/*************************************************************************************************
 *
 * This unit implements load balancing for a space-filling curve based domain decomposition. Each
 * process is allocated a range of SFC indices.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _LB_SFC_H_
#define _LB_SFC_H_

#include "../statistics.h"

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

/*
 * The information the main process needs about each halo in order to determine the best domain
 * decomposition.
 */
typedef struct
{
	int idx;
	float weight;
} SFCHaloProperties;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void loadBalance_sfc(int snap_idx, DynArray *halos);

#endif
