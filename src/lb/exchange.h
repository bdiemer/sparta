/*************************************************************************************************
 *
 * This unit impelments routines that are general to the exchange of halos in SPARTA.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _EXCHANGE_H_
#define _EXCHANGE_H_

#include "../statistics.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void exchangeHalos(int snap_idx, DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs);
void resolveLocalSubhaloDestinations(DynArray *halos, int_least8_t *mask);
void resolveGlobalSubhaloDestinations(DynArray *halos, int snap_idx);
void checkExchangeResults(DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs);
void updateExchangeStats(GlobalStatistics *gs, int n_rcv_halos, int n_rcv_subs, int *n_rcv_da);

#endif
