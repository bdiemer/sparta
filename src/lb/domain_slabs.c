/*************************************************************************************************
 *
 * This unit implements a slab-based domain decomposition where the box is split into slabs in
 * each dimension. Each process controls a rectilinear sub-volume.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <mpi.h>

#include "domain.h"
#include "domain_slabs.h"
#include "../config.h"
#include "../utils.h"

#if DOMAIN_DECOMP_SLABS

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

SlabDomain slabDomain;
int slab_domain_coor[3];

/*************************************************************************************************
 * INTERNAL CONSTANTS
 *************************************************************************************************/

#define MAX_N_PRIMES 20

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void smallestFactorDecomposition(int n, int *primes);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void smallestFactorDecomposition(int N, int *primes)
{
	int i, j, n;
	int primes_[MAX_N_PRIMES];

	/*
	 * First, find prime factors of two and add them to the list
	 */
	n = N;
	j = 0;
	while (n % 2 == 0)
	{
		primes_[j] = 2;
		n = n / 2;
		j++;
	}

	/*
	 * Try dividing by other numbers
	 */
	for (i = 3; i <= sqrt(n); i = i + 2)
	{
		while (n % i == 0)
		{
			primes_[j] = i;
			n = n / i;
			j++;
		}
	}

	if (n > 2)
	{
		primes_[j] = n;
		j++;
	}

	/*
	 * Either fill array up with ones, or sort array and in turn multiply the lowest of the top
	 * three factors by the highest of the smaller factors, and remove that factor.
	 */
	if (j < 3)
	{
		for (i = j; i < 3; i++)
		{
			primes_[i] = 1;
			j++;
		}
	} else
	{
		while (j > 3)
		{
			qsort(primes_, j, sizeof(int), &compareIntegersDescending);
			primes_[2] *= primes_[3];
			primes_[3] = primes_[j - 1];
			j--;
		}
	}

#if CAREFUL
	assert(j == 3);
#endif

	qsort(primes_, 3, sizeof(int), &compareIntegersDescending);
	for (i = 0; i < 3; i++)
		primes[i] = primes_[i];

#if CAREFUL
	assert(primes[0] * primes[1] * primes[2] == N);
#endif
}

void initialDomainDecomposition_slabs()
{
	int d, i;

	if (is_main_proc)
	{
		smallestFactorDecomposition(n_proc, slabDomain.n_split);

		for (d = 0; d < 3; d++)
		{
			if (slabDomain.n_split[d] > MAX_DOMAIN_SPLITS)
				error(__FFL__, "Too many domain splits (%d) along dimension %d.\n",
						slabDomain.n_split[d], d);

			for (i = 0; i < slabDomain.n_split[d]; i++)
			{
				slabDomain.boundary[d][i] = (float) i * config.box_size
						/ (float) slabDomain.n_split[d];
			}
			slabDomain.boundary[d][slabDomain.n_split[d]] = config.box_size;
		}

		output(0, "[Main] Dividing domain into [%d, %d, %d] subvolumes.\n", slabDomain.n_split[0],
				slabDomain.n_split[1], slabDomain.n_split[2]);

		for (d = 0; d < 3; d++)
		{
			output(2, "[Main] Dividing domain in dimension %d, boundaries:\n", d);
			for (i = 0; i < slabDomain.n_split[d] + 1; i++)
				output(2, "[Main] Boundary %d = %.3f\n", i, slabDomain.boundary[d][i]);
		}
	}

	MPI_Bcast(&slabDomain, sizeof(SlabDomain), MPI_BYTE, MAIN_PROC, MPI_COMM_WORLD);
}

/*
 * Create communicator with topology, and store this proc's coordinates. Note that we are assuming
 * the domain to be periodic in all three dimensions. Also store global variables for convenience:
 * the coordinates and box of this proc.
 */
void createMPICommunicator_slabs()
{
	int ret;
	int periodic[3] =
		{1, 1, 1};

	ret = MPI_Cart_create(MPI_COMM_WORLD, 3, slabDomain.n_split, periodic, 1, &comm);
	if (ret != MPI_SUCCESS)
	{
		output(0, "[%4d] MPI error %d while creating topology. Terminating.\n", proc, ret);
		MPI_Abort(comm, ret);
	}

	MPI_Cart_coords(comm, proc, 3, slab_domain_coor);
	updateProcDomainBox_slabs();

	output(2, "[%4d] MPI coordinates [%d %d %d], box [%6.2f..%6.2f  %6.2f..%6.2f  %6.2f..%6.2f]\n",
			proc, slab_domain_coor[0], slab_domain_coor[1], slab_domain_coor[2],
			proc_domain_box.min[0], proc_domain_box.max[0], proc_domain_box.min[1],
			proc_domain_box.max[1], proc_domain_box.min[2], proc_domain_box.max[2]);
}

int procFromPosition_slabs(float *x)
{
	int d, p, coor[3];

	p = INVALID_PROC;
	for (d = 0; d < 3; d++)
	{
		coor[d] = 0;
		while (x[d] > slabDomain.boundary[d][coor[d]])
			coor[d]++;
		coor[d]--;
		if (coor[d] > slabDomain.n_split[d] - 1)
			error(__FFL__,
					"Could not find split for coordinate %.2f in dimension %d, n_split %d.\n", x[d],
					d, slabDomain.n_split[d]);
	}

	MPI_Cart_rank(comm, coor, &p);

	return p;
}

void updateProcDomainBox_slabs()
{
	int d;

	for (d = 0; d < 3; d++)
	{
		proc_domain_box.min[d] = slabDomain.boundary[d][slab_domain_coor[d]];
		proc_domain_box.max[d] = slabDomain.boundary[d][slab_domain_coor[d] + 1];
		proc_domain_box.periodic[d] = 0;
	}
}

/*
 * Return the distance from the domain box in dimension dim and direction dir (down/up). If the
 * position is in the domain box, return a negative number.
 */
float distanceFromDomainBox(float pos, int relative, int dim, int dir)
{
	float x, sv_size, sv_midpoint, dx;

	/*
	 * If the domain box encompasses the entire domain in this dimension, the position must lie
	 * within the domain box.
	 */
	if (slabDomain.n_split[dim] == 1)
		return -1.0;

	sv_size = proc_domain_box.max[dim] - proc_domain_box.min[dim];
	sv_midpoint = (proc_domain_box.max[dim] + proc_domain_box.min[dim]) * 0.5;
	x = pos - sv_midpoint;

	if ((sv_midpoint < config.box_size * 0.5) && (x > config.box_size * 0.5))
		x -= config.box_size;

	if ((sv_midpoint >= config.box_size * 0.5) && (x <= -config.box_size * 0.5))
		x += config.box_size;

	if (dir == 0)
		dx = (proc_domain_box.min[dim] - sv_midpoint) - x;
	else
		dx = x - (proc_domain_box.max[dim] - sv_midpoint);

	if (relative)
		dx = dx / sv_size;

	return dx;
}

/*
 * Return the maximum distance a point is from a box, and in what dimension (3) and direction (6).
 * The distance is in units of the box length in that dimension if relative == 1, otherwise it is
 * in physical length units.
 */
void maxDistanceFromDomainBox(float *x, int relative, float *max_dist, int *max_dim, int *max_dir)
{
	int d, max_dist_dim, max_dist_dir, dir;
	float dx, sv_size, sv_midpoint, c;

	*max_dist = 0.0;
	max_dist_dim = -1;
	max_dist_dir = -1;
	for (d = 0; d < 3; d++)
	{
		if (slabDomain.n_split[d] == 1)
			continue;

		sv_size = proc_domain_box.max[d] - proc_domain_box.min[d];
		sv_midpoint = (proc_domain_box.max[d] + proc_domain_box.min[d]) * 0.5;
		c = x[d] - sv_midpoint;

		if ((sv_midpoint < config.box_size * 0.5) && (c > config.box_size * 0.5))
			c -= config.box_size;

		if ((sv_midpoint >= config.box_size * 0.5) && (c <= -config.box_size * 0.5))
			c += config.box_size;

		if ((c < 0.0))
		{
			dx = (proc_domain_box.min[d] - sv_midpoint) - c;
			dir = 0;
		} else
		{
			dx = c - (proc_domain_box.max[d] - sv_midpoint);
			dir = 1;
		}

		if (relative)
			dx = dx / sv_size;

		if (dx > *max_dist)
		{
			*max_dist = dx;
			max_dist_dim = d;
			max_dist_dir = dir;
		}
	}
	*max_dir = 2 * max_dist_dim + max_dist_dir;
	*max_dim = max_dist_dim;
}

#endif
