/*************************************************************************************************
 *
 * This unit impelments routines that are general to the exchange of halos in SPARTA.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <stddef.h>

#include "exchange.h"
#include "exchange_slabs.h"
#include "exchange_sfc.h"
#include "domain.h"
#include "domain_slabs.h"
#include "../config.h"
#include "../memory.h"
#include "../utils.h"
#include "../geometry.h"
#include "../halos/halo.h"

/*************************************************************************************************
 * INTERNAL CONSTANTS
 *************************************************************************************************/

/*
 * Flags that are transmitted through a process number. The flag is added to n_proc, meaning that
 * its value must be zero or positive.
 */
#define EXCHANGE_TAG_HOST_ENDED 0

/*
 * The host-sub moves should be resolved within one or at most two iterations, otherwise there
 * would need to be a hierarchy of N sub-sub relations where all hosts move process, which seems
 * very unlikely.
 */
#define EXCHANGE_RESOLVE_MAX_ITER 10

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct HostSubConnection
{
	HaloID id;
	HaloID pid;
	int idx;
	int_least8_t resolved;
	int_least8_t changed;
} HostSubConnection;

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int compareHostSubConnections(const void *a, const void *b);
int* findNewHostlessSubs(DynArray *halos, int *n_found);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void exchangeHalos(int snap_idx, DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs)
{
#if DOMAIN_DECOMP_SLABS
	exchangeHalos_slabs(snap_idx, halos, ls, gs);
#endif

#if DOMAIN_DECOMP_SFC
	exchangeHalos_sfc(snap_idx, halos, ls, gs);
#endif
}

/*
 * Return a mask indicating all those halos that are becoming subs and whose host halo is not
 * on this proc.
 */
int* findNewHostlessSubs(DynArray *halos, int *n_found)
{
	int i, *mask;
	Halo *h, *temp_halo, *host;

	mask = (int*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(int) * halos->n);
	temp_halo = (Halo*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(Halo));
	sortHalos(halos, 0);

	*n_found = 0;
	for (i = 0; i < halos->n; i++)
	{
		mask[i] = 0;
		h = &(halos->h[i]);

		if (isEnteringHost(h))
		{
			if (h->cd.pid == -1)
				error(__FFL__, "Found halo that is becoming sub but has pid = -1, ID %ld\n",
						h->cd.id);
			debugHalo(h, "Is entering host.");
			temp_halo->id_sort = h->cd.pid;
			host = (Halo*) bsearch(temp_halo, halos->h, halos->n, sizeof(Halo), &compareHalos);
			mask[i] = (host == NULL);
			if (mask[i])
			{
				*n_found += 1;
				debugHalo(h, "Marking subhalo as hostless sub.");
			}
		}
	}

	memFree(__FFL__, MID_EXCHANGEBUF, temp_halo, sizeof(Halo));

	return mask;
}

/*
 * Find all new subhalos whose hosts reside (or will reside) on other procs. The exact functionality
 * varies slightly with the type of domain decomposition:
 *
 * slabs:  After halos have been exchanged, find hosts on other procs and send back that proc
 * sfc:    Before halos are exchanged, find hosts on other procs and send back their future proc
 */
void resolveGlobalSubhaloDestinations(DynArray *halos, int snap_idx)
{
	int i, counter, n_hostless, *mask, *mask_found, *found_procs, n_requested[n_proc],
			displacements[n_proc], n_req_all;
	int *mask_res_all = NULL;
	Halo *temp_halo, *match;
	HaloID *hls_pids, *request_pids = NULL;

#if DO_SUBSUBS
	int all_resolved, iter;
	HaloID *hls_ids, *request_ids = NULL;
	HostSubConnection *conns = NULL, tmp_con, *parent_conn;
#endif

	/*
	 * Find all hostless subs, and create an array with their IDs
	 */
	mask = findNewHostlessSubs(halos, &n_hostless);
	hls_pids = (HaloID*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(HaloID) * n_hostless);
#if DO_SUBSUBS
	hls_ids = (HaloID*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(HaloID) * n_hostless);
#endif
	counter = 0;
	for (i = 0; i < halos->n; i++)
	{
		if (mask[i])
		{
			hls_pids[counter] = halos->h[i].cd.pid;
#if DO_SUBSUBS
			hls_ids[counter] = halos->h[i].cd.id;
#endif
			counter++;
		}
	}

	/*
	 * Collect the number of hostless IDs on the main proc and send their sum out to all procs.
	 */
	MPI_Gather(&n_hostless, 1, MPI_INT, n_requested, 1, MPI_INT, MAIN_PROC, comm);
	n_req_all = 0;
	if (is_main_proc)
	{
		for (i = 0; i < n_proc; i++)
		{
			displacements[i] = n_req_all;
			n_req_all += n_requested[i];
		}
	}
	MPI_Bcast(&n_req_all, 1, MPI_INT, MAIN_PROC, comm);

	/*
	 * Collect the hostless IDs on the main proc.
	 */
	request_pids = (HaloID*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(HaloID) * n_req_all);
	MPI_Gatherv(hls_pids, n_hostless, MPI_LONG, request_pids, n_requested, displacements, MPI_LONG,
	MAIN_PROC, comm);
	memFree(__FFL__, MID_EXCHANGEBUF, hls_pids, sizeof(HaloID) * n_hostless);

	/*
	 * Treat a special case: if we are allowing sub-subhalos, it is possible that a requested host
	 * is actually a sub and moving itself. In this case, we also send the sub IDs and let the main
	 * proc change the parent IDs to the upids if necessary.
	 */
#if DO_SUBSUBS
	if (is_main_proc)
		request_ids = (HaloID*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(HaloID) * n_req_all);
	MPI_Gatherv(hls_ids, n_hostless, MPI_LONG, request_ids, n_requested, displacements, MPI_LONG,
	MAIN_PROC, comm);
	memFree(__FFL__, MID_EXCHANGEBUF, hls_ids, sizeof(HaloID) * n_hostless);

	/*
	 * In principle, there could be an infinite hierarchy of subhalo relations, so we need to
	 * iteratively find hosts that are actually hostless subs themselves.
	 */
	if (is_main_proc)
	{
		/*
		 * Create a sortable/searchable array of the host-sub connections
		 */
		conns = (HostSubConnection*) memAlloc(__FFL__, MID_EXCHANGEBUF,
				sizeof(HostSubConnection) * n_req_all);
		for (i = 0; i < n_req_all; i++)
		{
			conns[i].idx = i;
			conns[i].id = request_ids[i];
			conns[i].pid = request_pids[i];
			conns[i].resolved = 0;
			conns[i].changed = 0;
		}
		qsort(conns, n_req_all, sizeof(HostSubConnection), &compareHostSubConnections);

		/*
		 * Now iteratively resolve hosts that are also in the hostless array. If we find a pid in
		 * the id array, we assign the pid's pid instead and mark the halo as not yet resolved, as
		 * there could be higher hierarchies. Note that we do not need to re-sort after each
		 * iteration because we are sorting ids and changing pids.
		 */
		all_resolved = 0;
		iter = 0;
		while (!all_resolved)
		{
			all_resolved = 1;
			for (i = 0; i < n_req_all; i++)
			{
				if (conns[i].resolved)
					continue;

				tmp_con.id = conns[i].pid;
				parent_conn = (HostSubConnection*) bsearch(&tmp_con, conns, n_req_all,
						sizeof(HostSubConnection), &compareHostSubConnections);
				if (parent_conn == NULL)
				{
					conns[i].resolved = 1;
				} else
				{
#if DEBUG_HALO
					if (conns[i].id == DEBUG_HALO)
						output(0,
								"[Main] *** Halo ID %ld: Changing exchange PID from %ld to %ld to follow moving host.\n",
								conns[i].id, conns[i].pid, parent_conn->pid);
#endif
					conns[i].pid = parent_conn->pid;
					conns[i].resolved = 0;
					conns[i].changed = 1;
					all_resolved = 0;
				}
			}

			/*
			 * Check that nothing's going wrong
			 */
			iter++;
			if (iter > EXCHANGE_RESOLVE_MAX_ITER)
				error(__FFL__, "Could not resolve host-sub exchanges within %d iterations.\n",
				EXCHANGE_RESOLVE_MAX_ITER);
		}

		/*
		 * Write back the corrected pids
		 */
		for (i = 0; i < n_req_all; i++)
		{
			if (conns[i].changed)
				request_pids[conns[i].idx] = conns[i].pid;
		}

		memFree(__FFL__, MID_EXCHANGEBUF, request_ids, sizeof(HaloID) * n_req_all);
		memFree(__FFL__, MID_EXCHANGEBUF, conns, sizeof(HostSubConnection) * n_req_all);
	}
#endif

	/*
	 * Now (possibly after fixing them) send out the requested parent IDs to all procs so they can
	 * look for them.
	 */
	MPI_Bcast(request_pids, n_req_all, MPI_LONG, MAIN_PROC, comm);

	/*
	 * Search this proc's halos for the requested IDs. If we find it, send back this proc's ID.
	 * There is a special case: if the halo has died (e.g., because it jumped), we need to let
	 * the other proc know so it can delete the orphaned subhalos. Otherwise, they might be sent
	 * to this proc which could end fatally if the halo jumped.
	 *
	 * If we are not allowing sub-subhalos, the found host cannot be a subhalo.
	 */
	mask_found = (int*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(int) * n_req_all);
	temp_halo = (Halo*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(Halo));
	for (i = 0; i < n_req_all; i++)
	{
		temp_halo->id_sort = request_pids[i];
		match = (Halo*) bsearch(temp_halo, halos->h, halos->n, sizeof(Halo), &compareHalos);
		if (match == NULL)
		{
			mask_found[i] = INVALID_PROC;
		} else
		{
#if !DO_SUBSUBS
			if (match->cd.pid != -1)
				error(__FFL__,
						"[%4d] Found host %ld for hostless sub, but this halo is a sub itself, status %d.\n",
						proc, match->cd.id, match->status);
#endif
			if (hasEnded(match))
			{
				mask_found[i] = n_proc + EXCHANGE_TAG_HOST_ENDED;
			} else
			{
#if DOMAIN_DECOMP_SLABS
				mask_found[i] = proc;
#else
				mask_found[i] = match->cd.proc;
#endif
			}
		}
	}

	/*
	 * Gather all results on the main proc
	 */
	if (is_main_proc)
		mask_res_all = (int*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(int) * n_req_all);
	MPI_Reduce(mask_found, mask_res_all, n_req_all, MPI_INT, MPI_MAX, MAIN_PROC, comm);
#if CAREFUL
	if (is_main_proc)
	{
		for (i = 0; i < n_req_all; i++)
		{
			if (mask_res_all[i] == INVALID_PROC)
				warningOrError(__FFL__, config.err_level_exc_halo_not_found,
						"Could not find halo descendant ID %ld on any process.\n", request_pids[i]);
		}
	}
#endif

	/*
	 * Main sends the relevant results back to each proc. There are a few cases: no host proc was
	 * found, that's an error; a valid host proc was found; the host halo has ended.
	 */
	found_procs = (int*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(int) * n_hostless);
	MPI_Scatterv(mask_res_all, n_requested, displacements, MPI_INT, found_procs, n_hostless,
			MPI_INT,
			MAIN_PROC, comm);
	counter = 0;
	for (i = 0; i < halos->n; i++)
	{
		if (mask[i])
		{
			if (found_procs[counter] == INVALID_PROC)
			{
				error(__FFL__,
						"[%4d] Could not find host ID %ld for hostless sub ID %ld on any proc.\n",
						proc, halos->h[i].cd.pid, halos->h[i].cd.id);
			} else if (found_procs[counter] >= n_proc)
			{
				int err_code;
				err_code = found_procs[counter] - n_proc;
				if (err_code == EXCHANGE_TAG_HOST_ENDED)
				{
					endHalo(&(halos->h[i]), snap_idx, snap_idx, HALO_ENDED_HOST_ENDED);
					warningOrError(__FFL__, config.err_level_exc_host_ended,
							"[%4d] Received HOST_ENDED tag for subhalo %ld, host %ld. Ending halo.\n",
							proc, halos->h[i].cd.id, halos->h[i].cd.pid);
				} else
				{
					error(__FFL__,
							"[%4d] Received invalid host proc error code %d, halo ID %ld, pid %ld.\n",
							proc, err_code, halos->h[i].cd.id, halos->h[i].cd.pid);
				}
			} else
			{
				halos->h[i].cd.proc = found_procs[counter];
				debugHalo(&(halos->h[i]), "Changing destination to process %d where host resides.",
						halos->h[i].cd.proc);
			}
			counter++;
		}
	}

	/*
	 * Free memory
	 */
	if (is_main_proc)
		memFree(__FFL__, MID_EXCHANGEBUF, mask_res_all, sizeof(int) * n_req_all);
	memFree(__FFL__, MID_EXCHANGEBUF, temp_halo, sizeof(Halo));
	memFree(__FFL__, MID_EXCHANGEBUF, request_pids, sizeof(HaloID) * n_req_all);
	memFree(__FFL__, MID_EXCHANGEBUF, mask, sizeof(int) * halos->n);
	memFree(__FFL__, MID_EXCHANGEBUF, mask_found, sizeof(int) * n_req_all);
	memFree(__FFL__, MID_EXCHANGEBUF, found_procs, sizeof(int) * n_hostless);

	/*
	 * Now go through subhalo assignments again, if we allow sub-subhalos, since their sub-hosts
	 * could have changed destination.
	 */
#if DO_SUBSUBS
	resolveLocalSubhaloDestinations(halos, NULL);
#endif
}

/*
 * Go through all local subhalos and set their destinations to their hosts, if available on this
 * process. The function can process the information in two different ways:
 *
 * - If mask is NULL, use the proc field in each halo, which indicates the process the host/sub
 *   will be sent to.
 * - If mask is not NULL, look for differences in the mask and adjust those. This way is used when
 *   doing direction-by-direction exchanges.
 */
void resolveLocalSubhaloDestinations(DynArray *halos, int_least8_t *mask)
{
	int i, all_resolved, iter, host_idx, do_change;
	Halo temp_halo, *h, *host;

	all_resolved = 0;
	iter = 0;
	host_idx = -1;
	while (!all_resolved)
	{
		all_resolved = 1;

		for (i = 0; i < halos->n; i++)
		{
			h = &(halos->h[i]);
			if (!isSub(h))
				continue;

#if CAREFUL
			if (h->cd.pid == -1)
				error(__FFL__, "Found halo that is a sub but has pid = -1, ID %ld, status %d\n",
						h->cd.id, h->status);
#endif

			temp_halo.id_sort = h->cd.pid;
			host = (Halo*) bsearch(&temp_halo, halos->h, halos->n, sizeof(Halo), &compareHalos);
			if (host != NULL)
			{
				if (mask == NULL)
				{
					do_change = (h->cd.proc != host->cd.proc);
				} else
				{
					host_idx = host - halos->h;
					do_change = (mask[i] != mask[host_idx]);
				}
				if (do_change)
				{
					if (mask == NULL)
					{
						h->cd.proc = host->cd.proc;
						if (h->cd.proc != proc)
							debugHalo(h, "Moving sub with host to proc %d.", h->cd.proc);
					} else
					{
						mask[i] = mask[host_idx];
					}
					all_resolved = 0;
				}
			} else
			{
#if !DO_SUBSUBS
				if (!isEnteringHost(h))
					error(__FFL__, "Could not find host ID %ld for sub %ld.\n", h->cd.pid,
							h->cd.id);
#endif
			}
		}

#if DO_SUBSUBS
		if (iter > EXCHANGE_RESOLVE_MAX_ITER)
			error(__FFL__,
					"Could not resolve local subhalo host destinations within %d iterations.\n",
					EXCHANGE_RESOLVE_MAX_ITER);
#else
		if (iter > 1)
			error(__FFL__,
					"Could not resolve local subhalo host destinations within one iteration as expected.\n");
#endif
		iter++;
	}
}

int compareHostSubConnections(const void *a, const void *b)
{
	if (((HostSubConnection*) a)->id > ((HostSubConnection*) b)->id)
		return 1;
	else if (((HostSubConnection*) a)->id < ((HostSubConnection*) b)->id)
		return -1;
	else
		return 0;
}

/*
 * Compare the new halo population on each proc with the pre-exchange statistics given in ls and
 * gs.
 */
void checkExchangeResults(DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs)
{
	int i, tt, rs, al;
	GlobalStatistics *gs_temp = NULL;

	/*
	 * Check that all halos were sent to the correct proc
	 */
	for (i = 0; i < halos->n; i++)
		if (halos->h[i].cd.proc != proc)
			error(__FFL__, "Found halo destined for proc %d, ID %ld, x %.2f %.2f %.2f.\n",
					halos->h[i].cd.proc, halos->h[i].cd.id, halos->h[i].cd.x[0],
					halos->h[i].cd.x[1], halos->h[i].cd.x[2]);

	/*
	 * Check that we still have the same number of halos etc as before. We have to be a little
	 * careful since subhalos can be ended in the exchange step if their hosts have ended, so we
	 * cannot compare the number of subs.
	 */
	if (is_main_proc)
	{
		gs_temp = (GlobalStatistics*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(GlobalStatistics));
		initGlobalStatistics(gs_temp);
	}
	collectHaloStatistics(halos, gs_temp);
	collectTracerStatistics(halos, ls, gs_temp);
	if (is_main_proc)
	{
		/*
		 * This is a particularly nasty error, so we investigate in some detail.
		 */
		if (gs->halos.n != gs_temp->halos.n)
			error(__FFL__, "Found %d halos before and %d halos after exchange.\n", gs->halos.n,
					gs_temp->halos.n);
		assert(gs->n_halos_host == gs_temp->n_halos_host);
		for (tt = 0; tt < NTT; tt++)
		{
			assert(gs->tt[tt].tcr.n == gs_temp->tt[tt].tcr.n);
			assert(gs->tt[tt].iid.n == gs_temp->tt[tt].iid.n);
			for (rs = 0; rs < NRS; rs++)
				assert(gs->tt[tt].rs[rs].da.n == gs_temp->tt[tt].rs[rs].da.n);
		}
		for (al = 0; al < NAL; al++)
			assert(gs->al[al].da.n == gs_temp->al[al].da.n);
		memFree(__FFL__, MID_EXCHANGEBUF, gs_temp, sizeof(GlobalStatistics));
	}
}

void updateExchangeStats(GlobalStatistics *gs, int n_rcv_halos, int n_rcv_subs, int *n_rcv_da)
{
	int tt, rs, al;

	MPI_Reduce(&n_rcv_halos, &(gs->halos.n_exc), 1, MPI_INT, MPI_SUM, MAIN_PROC, comm);
	MPI_Reduce(&n_rcv_subs, &(gs->n_exd_subs), 1, MPI_INT, MPI_SUM, MAIN_PROC, comm);
	for (tt = 0; tt < NTT; tt++)
	{
		MPI_Reduce(&(n_rcv_da[tt * (2 + NRS) + 0]), &(gs->tt[tt].tcr.n_exc), 1, MPI_INT, MPI_SUM,
		MAIN_PROC, comm);
		MPI_Reduce(&(n_rcv_da[tt * (2 + NRS) + 1]), &(gs->tt[tt].iid.n_exc), 1, MPI_INT, MPI_SUM,
		MAIN_PROC, comm);
		for (rs = 0; rs < NRS; rs++)
			MPI_Reduce(&(n_rcv_da[tt * (2 + NRS) + 2 + rs]), &(gs->tt[tt].rs[rs].da.n_exc), 1,
					MPI_INT, MPI_SUM, MAIN_PROC, comm);
	}
	for (al = 0; al < NAL; al++)
		MPI_Reduce(&(n_rcv_da[NTT * (2 + NRS) + al]), &(gs->al[al].da.n_exc), 1, MPI_INT, MPI_SUM,
		MAIN_PROC, comm);
}
