/*************************************************************************************************
 *
 * This unit implements load balancing for the slab-based domain decomposition. The boundaries
 * of the slabs are shifted to even out the load.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _LB_SLABS_H_
#define _LB_SLABS_H_

#include "../statistics.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void loadBalance_slabs(LocalStatistics *ls, int snap_idx);

#endif
