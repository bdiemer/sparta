/*************************************************************************************************
 *
 * This unit implements routines that are general to the domain decomposition in SPARTA.
 * Some of the routines are implemented by specific domain decomposition algorithms.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <mpi.h>

#include "domain.h"
#include "domain_slabs.h"
#include "domain_sfc.h"
#include "../config.h"
#include "../halos/halo.h"
#include "../geometry.h"

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

Box proc_domain_box;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initialDomainDecomposition()
{
#if DOMAIN_DECOMP_SLABS
	initialDomainDecomposition_slabs();
#endif
#if DOMAIN_DECOMP_SFC
	initialDomainDecomposition_sfc();
#endif
}

void createMPICommunicator()
{
#if DOMAIN_DECOMP_SLABS
	createMPICommunicator_slabs();
#endif
#if DOMAIN_DECOMP_SFC
	createMPICommunicator_sfc();
#endif
}

int procFromPosition(float *x)
{
#if DOMAIN_DECOMP_SLABS
	return procFromPosition_slabs(x);
#endif
#if DOMAIN_DECOMP_SFC
	return procFromPosition_sfc(x);
#endif
}

/*
 * For each halo, we must evaluate two positions: its center plus and minus the desired
 * radius. We take into account both hosts and subs since they can both have particles. Those
 * subtleties are determined when the search radius is set.
 */
void computeHaloBox(DynArray *halos, Box *box)
{
	int i, d, simple_min_max;
	float x, xl, xh, sv_midpoint;
	Halo *h;

	if (halos->n == 0)
	{
		setEmptyBox(box);
		return;
	} else
	{
		resetBox(box);
	}

	for (d = 0; d < 3; d++)
	{
		/*
		 * CASE 1: This proc covers all of the domain (in a slab decomposition). In an SFC
		 * decomposition, we only apply the simple min/max if the SFC indices occupy virtually
		 * the entire domain in this dimension. Due to periodic BCs, the simple case very often
		 * leads to a complete coverage of the domain which can lead to very large volumes.
		 */
#if DOMAIN_DECOMP_SLABS
		simple_min_max = (slabDomain.n_split[d] == 1);
#else
		simple_min_max = (config.box_size + proc_domain_box.min[d] - proc_domain_box.max[d]
				< 0.1 * config.box_size);
#endif
		if (simple_min_max)
		{
			for (i = 0; i < halos->n; i++)
			{
				xl = halos->h[i].cd.x[d] - halos->h[i].r_search_com_guess;
				xh = halos->h[i].cd.x[d] + halos->h[i].r_search_com_guess;

				if (xl < (*box).min[d])
					(*box).min[d] = xl;
				if (xh > (*box).max[d])
					(*box).max[d] = xh;
			}

			/*
			 * We need to worry about periodic boundary conditions. If the max is bigger than
			 * the box size, there could be particles at x = 0.0 that need to be included, and
			 * vice versa for the min. Thus, the only correct solution is to set the box to the
			 * full coordinate extent if the periodic bcs are crossed at either end.
			 */
			if (((*box).min[d] < 0.0) || ((*box).max[d] > config.box_size))
			{
				(*box).min[d] = 0.0;
				(*box).max[d] = config.box_size;
			}

			continue;
		}

		/*
		 * CASE 2: There is more than one subvolumes in this dimension (in a slab decomposition),
		 * or, in an SFC decomposition, the SFC indices do not cover the entire domain.
		 * Go through halos, get min/max which can lie outside the domain. First, we need to
		 * define a line above/below which we consider the position for a min/max of the halo
		 * box. This line is drawn at the midpoint of the volume not covered by this proc's
		 * part of the domain.
		 */
		sv_midpoint = (proc_domain_box.max[d] + proc_domain_box.min[d]) * 0.5;

		for (i = 0; i < halos->n; i++)
		{
			h = &(halos->h[i]);
			x = h->cd.x[d] - sv_midpoint;

			if ((sv_midpoint < config.box_size * 0.5) && (x > config.box_size * 0.5))
				x -= config.box_size;

			if ((sv_midpoint >= config.box_size * 0.5) && (x <= -config.box_size * 0.5))
				x += config.box_size;

			xl = x - h->r_search_com_guess;
			if (xl < (*box).min[d])
				(*box).min[d] = xl;

			xh = x + h->r_search_com_guess;
			if (xh > (*box).max[d])
				(*box).max[d] = xh;
		}
		(*box).min[d] += sv_midpoint;
		(*box).max[d] += sv_midpoint;

		if ((*box).min[d] < 0.0)
		{
			(*box).min[d] += config.box_size;
			(*box).periodic[d] = 1;
		} else if ((*box).max[d] > config.box_size)
		{
			(*box).max[d] -= config.box_size;
			(*box).periodic[d] = 1;
		} else
		{
			(*box).periodic[d] = 0;
		}

		if (((*box).min[d] < 0.0) || ((*box).max[d] < 0.0) || ((*box).min[d] > config.box_size)
				|| ((*box).max[d] > config.box_size))
		{
			warningOrError(__FFL__, config.err_level_invalid_domain_box,
					"[%4d] Invalid halo box in dim %d [%.4f .. %.4f], proc box [%.4f .. %.4f]. Setting to total domain.\n",
					proc, d, (*box).min[d], (*box).max[d], proc_domain_box.min[d],
					proc_domain_box.max[d]);
			(*box).min[d] = 0.0;
			(*box).max[d] = config.box_size;
		}
	}

#if CAREFUL
	/*
	 * Trivial check that all halos are in volume. For ghosts, this condition does not have to be
	 * fulfilled since we could not know their true positions when we designed the halo box.
	 */
	float tmp_pos[3];

	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		if (isGhost(h))
			continue;

		if (!inBox(h->cd.x, box))
		{
			error(__FFL__,
					"[%4d] Halo ID %ld not in halo box! xyz %.6f %.6f %.6f rsearch %.6f box [%.6f..%.6f  %.6f..%.6f  %.6f..%.6f p %d %d %d]\n",
					proc, h->cd.id, h->cd.x[0], h->cd.x[1], h->cd.x[2], h->r_search_com_guess,
					box->min[0], box->max[0], box->min[1], box->max[1], box->min[2], box->max[2],
					box->periodic[0], box->periodic[1], box->periodic[2]);
		}
		for (d = 0; d < 3; d++)
		{
			memcpy(tmp_pos, h->cd.x, sizeof(float) * 3);
			tmp_pos[d] += h->r_search_com_guess * 0.999;
			tmp_pos[d] = correctPeriodic(tmp_pos[d], config.box_size);
			if (!inBox(tmp_pos, box))
			{
				error(__FFL__,
						"[%4d] Halo ID %ld (plus search radius) not in halo box! dim %d, new coor %.6f, xyz %.6f %.6f %.6f rsearch %.6f box [%.6f..%.6f  %.6f..%.6f  %.6f..%.6f p %d %d %d]\n",
						proc, h->cd.id, d, tmp_pos[d], h->cd.x[0], h->cd.x[1], h->cd.x[2],
						h->r_search_com_guess, box->min[0], box->max[0], box->min[1], box->max[1],
						box->min[2], box->max[2], box->periodic[0], box->periodic[1],
						box->periodic[2]);
			}
			memcpy(tmp_pos, h->cd.x, sizeof(float) * 3);
			tmp_pos[d] -= h->r_search_com_guess * 0.999;
			tmp_pos[d] = correctPeriodic(tmp_pos[d], config.box_size);
			if (!inBox(tmp_pos, box))
			{
				error(__FFL__,
						"[%4d] Halo ID %ld (minus search radius) not in halo box! dim %d, new coor %.6f, xyz %.6f %.6f %.6f rsearch %.6f box [%.6f..%.6f  %.6f..%.6f  %.6f..%.6f p %d %d %d]\n",
						proc, h->cd.id, d, tmp_pos[d], h->cd.x[0], h->cd.x[1], h->cd.x[2],
						h->r_search_com_guess, box->min[0], box->max[0], box->min[1], box->max[1],
						box->min[2], box->max[2], box->periodic[0], box->periodic[1],
						box->periodic[2]);
			}
		}
	}
#endif
}
