/*************************************************************************************************
 *
 * This unit implements the exchange of halos between processes for the slab-based domain
 * decomposition.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _EXCHANGE_SLABS_H_
#define _EXCHANGE_SLABS_H_

#include "../statistics.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void exchangeHalos_slabs(int snap_idx, DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs);

#endif
