/*************************************************************************************************
 *
 * This unit implements the exchange of halos between processes for the slab-based domain
 * decomposition.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stddef.h>

#include "exchange_slabs.h"
#include "domain.h"
#include "domain_slabs.h"
#include "exchange.h"
#include "../config.h"
#include "../memory.h"
#include "../geometry.h"
#include "../halos/halo.h"

#if DOMAIN_DECOMP_SLABS

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int_least8_t* getExchangeMaskHosts(DynArray *halos, int dim, int dir);
int_least8_t* getExchangeMaskFromDestination(DynArray *halos, int dim, int dir);
void exchangeDimDir(DynArray *halos, int_least8_t *mask, int dim, int dir, int *n_rcv_halos,
		int *n_rcv_subs, int *n_rcv_da, int set_proc);
int exchangeHaloDynArray(int p_snd, int p_rcv, Halo *halo_snd_buf, int n_snd_halos,
		Halo *halo_rcv_buf, int n_rcv_halos, int offset_da);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Exchange halos between processes. Because of subhalos that need to be on the same process as
 * their host, the exchange is split into multiple stages. In particular:
 *
 * - All hosts are exchanged if they lie outside the tolerated domain boundaries.
 * - All subs are exchanged with their host (this includes subs who will become hosts in the next
 *   snap).
 */
void exchangeHalos_slabs(int snap_idx, DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs)
{
	int i, dim, dir, n_rcv_halos, n_rcv_subs, n_rcv_da[N_HALO_DA], n_halos_prev;
	int_least8_t *mask;

	/*
	 * For the slab case, we need to make sure that all procs have finished the domain
	 * decomposition step before exchanging.
	 */
	MPI_Barrier(comm);

	/*
	 * Step 0: Collect statistics to compare to later.
	 */
#if CAREFUL
	collectHaloStatistics(halos, gs);
	collectTracerStatistics(halos, ls, gs);
#endif

	/*
	 * Step 1: Exchange host halos and their local subhalos. If there are no splits in this
	 * dimension, there is nothing to do here.
	 */
	n_rcv_halos = 0;
	n_rcv_subs = 0;
	for (i = 0; i < N_HALO_DA; i++)
		n_rcv_da[i] = 0;

	for (dim = 0; dim < 3; dim++)
	{
		if (slabDomain.n_split[dim] == 1)
			continue;

		for (dir = 0; dir < 2; dir++)
		{
			n_halos_prev = halos->n;
			mask = getExchangeMaskHosts(halos, dim, dir);
			resolveLocalSubhaloDestinations(halos, mask);
			exchangeDimDir(halos, mask, dim, dir, &n_rcv_halos, &n_rcv_subs, n_rcv_da, 1);
			memFree(__FFL__, MID_EXCHANGEBUF, mask, sizeof(int_least8_t) * n_halos_prev);
		}
	}

	/*
	 * Step 2: Find halos that are becoming subhalos, but whose future host is on a different
	 * process, or sub-subhalos whose host moved.
	 */
	resolveGlobalSubhaloDestinations(halos, snap_idx);

	/*
	 * Step 3: Exchange those halos whose proc destination has now changed. In this case, the
	 * proc field indicates the final destination of a halo, so we do not set it when exchanging.
	 */
	for (dim = 0; dim < 3; dim++)
	{
		if (slabDomain.n_split[dim] == 1)
			continue;

		for (dir = 0; dir < 2; dir++)
		{
			n_halos_prev = halos->n;
			mask = getExchangeMaskFromDestination(halos, dim, dir);
			exchangeDimDir(halos, mask, dim, dir, &n_rcv_halos, &n_rcv_subs, n_rcv_da, 0);
			memFree(__FFL__, MID_EXCHANGEBUF, mask, sizeof(int_least8_t) * n_halos_prev);
		}
	}

	/*
	 * Step 4: Check that everything went OK. Check that all halos ended up in the correct domain
	 * box. This only makes sense for the slabs domain decomposition.
	 */
#if CAREFUL
	checkExchangeResults(halos, ls, gs);

	int max_dim, max_dir;
	float max_dist;
	for (i = 0; i < halos->n; i++)
	{
		if (isHost(&(halos->h[i])))
		{
			maxDistanceFromDomainBox(halos->h[i].cd.x, 1, &max_dist, &max_dim, &max_dir);
			if (max_dist > config.lb_slabs_drift_tolerance)
				error(__FFL__,
						"[%4d] Found halo ID %ld, not within tolerance of domain (dx %l4f), status %d, pid %ld, desc_pid %ld.\n",
						proc, halos->h[i].cd.id, max_dist, halos->h[i].status, halos->h[i].cd.pid,
						halos->h[i].cd.desc_pid);
		}
	}
#endif

	/*
	 * Step 5: Add exchange statistics to global stats
	 */
	updateExchangeStats(gs, n_rcv_halos, n_rcv_subs, n_rcv_da);
}

/*
 * Figure out which halos need to be sent in a given dimension and direction. For subhalos, we find
 * their host halo and assimilate their status to that of the host. This includes halos who have
 * become subs in this snap.
 *
 * Note that we fail if an existing sub's host cannot be found, but not if a new sub's host
 * cannot be found. The latter case will be treated separately.
 */
int_least8_t* getExchangeMaskHosts(DynArray *halos, int dim, int dir)
{
	int i;
	int_least8_t *mask;
	Halo *h;

	mask = (int_least8_t*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(int_least8_t) * halos->n);
	sortHalos(halos, 0);

	for (i = 0; i < halos->n; i++)
	{
		mask[i] = 0;
		h = &(halos->h[i]);

		if (isHost(h))
		{
			if (inBox(h->cd.x, &proc_domain_box))
			{
				mask[i] = 0;
			} else
			{
				mask[i] = (distanceFromDomainBox(h->cd.x[dim], 1, dim, dir)
						> config.lb_slabs_drift_tolerance);
				if (mask[i])
					debugHalo(h, "Moving in dim %d dir %d.", dim, dir);
			}
		}
	}

	return mask;
}

int_least8_t* getExchangeMaskFromDestination(DynArray *halos, int dim, int dir)
{
	int i, proc_coor[3], mpi_dir, p_rcv, p_snd, snd_coor_this_shift;
	int_least8_t *mask;
	Halo *h;

	mask = (int_least8_t*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(int_least8_t) * halos->n);
	if (dir)
		mpi_dir = 1;
	else
		mpi_dir = -1;
	MPI_Cart_shift(comm, dim, mpi_dir, &p_rcv, &p_snd);
	MPI_Cart_coords(comm, p_snd, 3, proc_coor);
	snd_coor_this_shift = proc_coor[dim];

	for (i = 0; i < halos->n; i++)
	{
		mask[i] = 0;
		h = &(halos->h[i]);

		if (h->cd.proc != proc)
		{
			MPI_Cart_coords(comm, h->cd.proc, 3, proc_coor);
			mask[i] = (proc_coor[dim] == snd_coor_this_shift);
			debugHalo(h,
					"Marking for shift, result %d, dim %d, dir %d, target_coor %d, snd_coor_this_shift %d, new proc %d.",
					mask[i], dim, dir, proc_coor[dim], snd_coor_this_shift, h->cd.proc);
		}
	}

	return mask;
}

/*
 * Send a particular set of halos identified by a mask to the proc that is up in dimension dim and
 * direction dir, and receive halos from the proc that is down (i.e. opposite up). Note that the
 * n_rcv_* counters passed are added to rather than re-set.
 */
void exchangeDimDir(DynArray *halos, int_least8_t *mask, int dim, int dir, int *n_rcv_halos,
		int *n_rcv_subs, int *n_rcv_da, int set_proc)
{
	int i, tt, rs, al, n_snd, n_rcv, p_snd, p_rcv, mpi_dir, counter;
	MPI_Request req_snd_n, req_snd_halos;
	Halo *h_snd_buf, *h_rcv_buf, *h;

	/*
	 * Figure out which proc we're communicating with
	 */
	if (dir)
		mpi_dir = 1;
	else
		mpi_dir = -1;
	MPI_Cart_shift(comm, dim, mpi_dir, &p_rcv, &p_snd);

#if CAREFUL
	if (proc == p_snd)
		error(__FFL__, "dim %d dir %d exchange proc is this proc.\n", dim, dir);
#endif

	/*
	 * Count halos to send, send/receive number of halos
	 */
	n_snd = 0;
	for (i = 0; i < halos->n; i++)
		if (mask[i])
			n_snd++;

	MPI_Isend(&n_snd, 1, MPI_INT, p_snd, 0, comm, &req_snd_n);
	MPI_Recv(&n_rcv, 1, MPI_INT, p_rcv, 0, comm, MPI_STATUS_IGNORE);

	/*
	 * Create buffers, copy halos to be sent into buffer
	 */
	h_snd_buf = (Halo*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(Halo) * n_snd);
	h_rcv_buf = (Halo*) memAlloc(__FFL__, MID_EXCHANGEBUF, sizeof(Halo) * n_rcv);

	counter = 0;
	for (i = 0; i < halos->n; i++)
	{
		if (mask[i])
		{
			memcpy(&(h_snd_buf[counter]), &(halos->h[i]), sizeof(Halo));
			counter++;
		}
	}

	/*
	 * Send / received halos
	 */
	if (n_snd > INT_MAX)
		error(__FFL__,
				"Cannot send MPI halo message of size %ld (%d halos of %ld bytes), too many halos.\n",
				n_snd * sizeof(Halo), n_snd, sizeof(Halo));
	MPI_Isend(h_snd_buf, n_snd, mpiTypes.halo, p_snd, 0, comm, &req_snd_halos);
	MPI_Recv(h_rcv_buf, n_rcv, mpiTypes.halo, p_rcv, 0, comm, MPI_STATUS_IGNORE);

	/*
	 * We have the halos, now send / receive particles and results. This is done within the send
	 * and receive buffer arrays.
	 */
	*n_rcv_subs += exchangeHaloDynArray(p_snd, p_rcv, h_snd_buf, n_snd, h_rcv_buf, n_rcv,
			offsetof(Halo, subs));
	for (tt = 0; tt < NTT; tt++)
	{
		n_rcv_da[(2 + NRS) * tt + 0] += exchangeHaloDynArray(p_snd, p_rcv, h_snd_buf, n_snd,
				h_rcv_buf, n_rcv, offsetof(Halo, tt[tt].tcr));
		n_rcv_da[(2 + NRS) * tt + 1] += exchangeHaloDynArray(p_snd, p_rcv, h_snd_buf, n_snd,
				h_rcv_buf, n_rcv, offsetof(Halo, tt[tt].iid));
		for (rs = 0; rs < NRS; rs++)
		{
			n_rcv_da[(2 + NRS) * tt + 2 + rs] += exchangeHaloDynArray(p_snd, p_rcv, h_snd_buf,
					n_snd, h_rcv_buf, n_rcv, offsetof(Halo, tt[tt].rs[rs]));
		}
	}
	for (al = 0; al < NAL; al++)
		n_rcv_da[(2 + NRS) * NTT + al] += exchangeHaloDynArray(p_snd, p_rcv, h_snd_buf, n_snd,
				h_rcv_buf, n_rcv, offsetof(Halo, al[al]));

	/*
	 * We need to delete the array items BEFORE adding new ones, since otherwise the mask is not
	 * valid any more.
	 */
	deleteHalos(halos, mask);

	/*
	 * Add the received halos
	 */
	for (i = 0; i < n_rcv; i++)
	{
		output(5, "[%4d] Received halo ID %ld from proc %d\n", proc, h_rcv_buf[i].cd.id,
				h_rcv_buf[i].cd.proc);

		h = addHalo(__FFL__, halos, 0);
		memcpy(h, &(h_rcv_buf[i]), sizeof(Halo));
		if (set_proc)
			h->cd.proc = proc;
	}
	memFree(__FFL__, MID_EXCHANGEBUF, h_rcv_buf, sizeof(Halo) * n_rcv);

	/*
	 * At this point, we have to make sure that all our sends actually reached the target.
	 * Otherwise, we'll free a buffer while it is still being used.
	 */
	MPI_Wait(&req_snd_n, MPI_STATUS_IGNORE);
	MPI_Wait(&req_snd_halos, MPI_STATUS_IGNORE);
	memFree(__FFL__, MID_EXCHANGEBUF, h_snd_buf, sizeof(Halo) * n_snd);

	*n_rcv_halos += n_rcv;
}

/*
 * Exchange a dynamically allocated array of any data type. The function returns the number of
 * received datasets.
 *
 * This function is not necessarily specific to the slab-based domain decomposition, but only works
 * if two processes are mutually exchanging halos.
 */
int exchangeHaloDynArray(int p_snd, int p_rcv, Halo *halo_snd_buf, int n_snd_halos,
		Halo *halo_rcv_buf, int n_rcv_halos, int offset_da)
{
	int i, array_pos, n_snd_da, n_rcv_da, size;
	char *da_snd_buf, *da_rcv_buf, *h;
	DynArray *da;
	MPI_Request req_snd_da;

	/*
	 * Compute size of buffers, offsets etc
	 */
	n_snd_da = 0;
	size = 1;
	for (i = 0; i < n_snd_halos; i++)
	{
		h = (char*) (&(halo_snd_buf[i]));
		da = (DynArray*) (h + offset_da);
		n_snd_da += da->n;
		if (size == 1)
			size = da->size;
	}

	n_rcv_da = 0;
	for (i = 0; i < n_rcv_halos; i++)
	{
		h = (char*) (&(halo_rcv_buf[i]));
		da = (DynArray*) (h + offset_da);
		n_rcv_da += da->n;
		if (size == 1)
			size = da->size;
	}

	da_snd_buf = memAlloc(__FFL__, MID_EXCHANGEBUF, (size_t) size * n_snd_da);
	da_rcv_buf = memAlloc(__FFL__, MID_EXCHANGEBUF, (size_t) size * n_rcv_da);

	/*
	 * Shift send data into buffer
	 */
	array_pos = 0;
	for (i = 0; i < n_snd_halos; i++)
	{
		h = (char*) (&(halo_snd_buf[i]));
		da = (DynArray*) (h + offset_da);
		memcpy(da_snd_buf + (size_t ) array_pos * size, da->bytes, (size_t ) size * da->n);
		array_pos += da->n;
	}

	/*
	 * MPI calls. This should be converted to using the correct MPI type for the DynArray to
	 * send instead of MPI_BYTE. However, if this error ever occurs, the error message should
	 * clearly tell the user what's going on.
	 */
	if (n_snd_da * size > INT_MAX)
		error(__FFL__,
				"Cannot send MPI DynArray message of size %ld (%d items of %ld bytes), too large.\n",
				n_snd_da * size, n_snd_da, size);
	MPI_Isend(da_snd_buf, n_snd_da * size, MPI_BYTE, p_snd, 0, comm, &req_snd_da);
	MPI_Recv(da_rcv_buf, n_rcv_da * size, MPI_BYTE, p_rcv, 0, comm, MPI_STATUS_IGNORE);

	array_pos = 0;
	for (i = 0; i < n_rcv_halos; i++)
	{
		h = (char*) (&(halo_rcv_buf[i]));
		da = (DynArray*) (h + offset_da);
		resetDynArray(__FFL__, da);
#if CAREFUL
		if (da->n_alloc < da->n)
		{
			error(__FFL__, "DA exc %d %d size %d n rcv %d\n", da->n_alloc, da->n, size,
					n_rcv_halos);
		}
#endif
		memcpy(da->bytes, da_rcv_buf + (size_t ) array_pos * size, (size_t ) da->n * size);
		array_pos += da->n;
	}

	/*
	 * Wait to delete the buffers. Also, this function should not return while a request has
	 * not been completed, that could lead to all kinds of funny behavior.
	 */
	MPI_Wait(&req_snd_da, MPI_STATUS_IGNORE);
	memFree(__FFL__, MID_EXCHANGEBUF, da_rcv_buf, (size_t) size * n_rcv_da);
	memFree(__FFL__, MID_EXCHANGEBUF, da_snd_buf, (size_t) size * n_snd_da);

	return n_rcv_da;
}

#endif
