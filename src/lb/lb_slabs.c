/*************************************************************************************************
 *
 * This unit implements load balancing for the slab-based domain decomposition. The boundaries
 * of the slabs are shifted to even out the load.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <mpi.h>

#include "lb_slabs.h"
#include "domain_slabs.h"
#include "exchange_slabs.h"
#include "../config.h"
#include "../memory.h"

#if DOMAIN_DECOMP_SLABS

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int adjustmentDimension(int snap_idx, double *proc_t, double **slab_t);
void adjustDomainBoundariesDimension(int dim, double *t);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * The slab load balancing is based on the time taken by the procs. When tracer particles are
 * disabled, this method does not work very well (and the code generally runs very fast anyway).
 */
void loadBalance_slabs(LocalStatistics *ls, int snap_idx)
{
#if DO_TRACER_PARTICLES

	int dim;
	double *buffer = NULL, *slab_t;

	/*
	 * Only do domain decomposition after a certain number of snaps, and not in the last snap as
	 * that would be a waste of time.
	 */
	if ((snap_idx < config.lb_adjust_min_snap) || (snap_idx == config.n_snaps - 1))
		return;

	/*
	 * Gather the independent run time of each process.
	 */
	if (is_main_proc)
	{
		buffer = (double *) memAlloc(__FFL__, MID_DOMAINDECOMP, sizeof(double) * n_proc);
		slab_t = (double *) memAlloc(__FFL__, MID_DOMAINDECOMP, sizeof(double) * MAX_DOMAIN_SPLITS);
	}
	MPI_Gather(&(ls->timers_1[T1_PROCESS]), 1, MPI_DOUBLE, buffer, 1, MPI_DOUBLE, MAIN_PROC, comm);

	if (is_main_proc)
	{
		dim = adjustmentDimension(snap_idx, buffer, &slab_t);
		adjustDomainBoundariesDimension(dim, slab_t);
	}

	MPI_Bcast(&slabDomain, sizeof(SlabDomain), MPI_BYTE, MAIN_PROC, MPI_COMM_WORLD);
	updateProcDomainBox_slabs();

	if (is_main_proc)
	{
		memFree(__FFL__, MID_DOMAINDECOMP, buffer, sizeof(double) * n_proc);
		memFree(__FFL__, MID_DOMAINDECOMP, slab_t, sizeof(double) * MAX_DOMAIN_SPLITS);
	}

#endif
}

/*
 * Perform load balancing in the most unbalanced dimension
 */
void adjustDomainBoundariesDimension(int dim, double *t)
{
	int i;
	double target_area, current_area[MAX_DOMAIN_SPLITS], boundaries[MAX_DOMAIN_SPLITS], dA, diff,
			delta, prop_bndry, old_bndry, new_bndry;

	target_area = 0.0;
	for (i = 0; i < slabDomain.n_split[dim]; i++)
	{
		current_area[i] = t[i] * (slabDomain.boundary[dim][i + 1] - slabDomain.boundary[dim][i]);
		target_area += current_area[i];
		boundaries[i] = slabDomain.boundary[dim][i + 1];
	}
	target_area /= slabDomain.n_split[dim];

	for (i = 0; i < slabDomain.n_split[dim] - 1; i++)
	{
		dA = target_area - current_area[i];
		if (current_area[i] > current_area[i + 1])
		{
			boundaries[i] = boundaries[i] + dA / t[i + 1];
		} else
		{
			boundaries[i] = boundaries[i] + dA / t[i];
		}
		current_area[i] += dA;
		current_area[i + 1] -= dA;
	}

	for (i = 0; i < slabDomain.n_split[dim] - 1; i++)
	{
		prop_bndry = boundaries[i];
		old_bndry = slabDomain.boundary[dim][i + 1];

		diff = prop_bndry - old_bndry;
		delta = fabs(diff) / config.box_size * config.lb_slabs_adjust_factor;
		delta = fmax(delta, config.lb_slabs_adjust_max) * config.box_size;
		new_bndry = old_bndry + diff * delta / fabs(diff);
		slabDomain.boundary[dim][i + 1] = new_bndry;

		output(2,
				"[Main]          Load balancing: Slab %d, target load %.2f new %.2f, old bndry %.3f proposed %.3f new %.3f\n",
				i, target_area, current_area[i], old_bndry, prop_bndry, new_bndry);
	}
}

/*
 * Figure out which dimension to adjust in.
 */
int adjustmentDimension(int snap_idx, double *proc_t, double **slab_t)
{
	int i, j, k, d, d1, d2, coor[3], p, dim, n_proc_per_slab;
	double t_slab[3][MAX_DOMAIN_SPLITS], t_min[3], t_max[3], imbalance[3], max_imbalance;

	dim = -1;
	max_imbalance = -1.0;

	for (d = 0; d < 3; d++)
	{
		if (slabDomain.n_split[d] == 1)
			continue;

		d1 = (d + 1) % 3;
		d2 = (d + 2) % 3;
		n_proc_per_slab = slabDomain.n_split[d1] * slabDomain.n_split[d2];
		t_min[d] = 1E20;
		t_max[d] = -1;

		for (k = 0; k < slabDomain.n_split[d]; k++)
		{
			coor[d] = k;
			t_slab[d][k] = 0.0;
			for (i = 0; i < slabDomain.n_split[d1]; i++)
			{
				coor[d1] = i;
				for (j = 0; j < slabDomain.n_split[d2]; j++)
				{
					coor[d2] = j;
					MPI_Cart_rank(comm, coor, &p);
					/*
					 * The imbalance can be determined in a number of ways, e.g. as the average
					 * over a slab, or the max runtime of any member. Here we use the average.
					 */
					t_slab[d][k] += proc_t[p] / (double) n_proc_per_slab;
					//t_slab[d][k] = fmax(t_slab[d][k], proc_t[p]);
				}
			}

			if (t_slab[d][k] < t_min[d])
				t_min[d] = t_slab[d][k];
			if (t_slab[d][k] > t_max[d])
				t_max[d] = t_slab[d][k];
		}
		imbalance[d] = t_max[d] / t_min[d];
		if (imbalance[d] > max_imbalance)
		{
			max_imbalance = imbalance[d];
			dim = d;
		}

		for (k = 0; k < slabDomain.n_split[d]; k++)
		{
			output(2, "[Main]          Load balancing: Dim %d, slab %d, t %.2f\n", d, k, t_slab[d][k]);
		}
		output(2, "[Main]          Load balancing: Dim %d, t_min %.2f, t_max %.2f, imbalance = %.3f.\n", d,
				t_min[d], t_max[d], imbalance[d]);

	}
	output(2, "[Main]          Load balancing: Largest imbalance %.3f is in dim %d.\n", max_imbalance, dim);

	for (i = 0; i < slabDomain.n_split[dim]; i++)
		(*slab_t)[i] = t_slab[dim][i];

	return dim;
}

#endif
