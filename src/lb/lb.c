/*************************************************************************************************
 *
 * This unit implements general load balancing routines.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <mpi.h>

#include "lb.h"
#include "lb_slabs.h"
#include "lb_sfc.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void loadBalance(int snap_idx, DynArray *halos, LocalStatistics *ls_lastsnap)
{
#if DOMAIN_DECOMP_SLABS
	loadBalance_slabs(ls_lastsnap, snap_idx);
#endif

#if DOMAIN_DECOMP_SFC
	loadBalance_sfc(snap_idx, halos);
#endif
}
