/*************************************************************************************************
 *
 * This unit implements a domain decomposition based on a space-filling curve. The Hilbert curve
 * algorithm is adapted from the ART code (Kravtsov et al. 1997, Rudd et al. 2008).
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _DOMAIN_SFC_H_
#define _DOMAIN_SFC_H_

#include "../global.h"

#if DOMAIN_DECOMP_SFC

#include "../statistics.h"
#include "../geometry.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

#define SFC_NDIM 3
#define SFC_BITS_PER_DIM 8
#define SFC_NBITS (SFC_NDIM * SFC_BITS_PER_DIM)
#define SFC_MAX_1D (1 << SFC_BITS_PER_DIM)
#define SFC_MAX (SFC_MAX_1D*SFC_MAX_1D*SFC_MAX_1D)

#if (DOMAIN_SFC_INDEX_TABLE && (SFC_NDIM != 3))
#error "Index table can only be used for three dimensions."
#endif

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

/*
 * This struct describes the SFC domain decomposition simply as a list of lower and upper limits
 * of Hilbert indices for each proc. The index_conv number is the conversion between physical
 * length units and Hilbert indices, i.e. index = (integer) (length * index_conv).
 *
 * Note that the upper limit for each proc is exclusive, making the index range for proc i
 *
 * index_bdry[i] <= idx < index_bdry[i + 1]
 */
typedef struct
{
	int index_bdry[MAX_PROCS + 1];
	float index_conv;
	float index_conv_inv;
} SFCDomain;

#if DOMAIN_SFC_INDEX_TABLE
/*
 * To evaluate the SFC indices faster, we compute them once at the beginning of the program and
 * look them up from there on.
 */
typedef struct
{
	int *idx;
	int *pos;
} SFCIndexLookup;
#endif

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

extern SFCDomain sfcDomain;
#if DOMAIN_SFC_INDEX_TABLE
extern SFCIndexLookup sfcIndexLookup;
#endif

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initialDomainDecomposition_sfc();
void createMPICommunicator_sfc();
int procFromPosition_sfc(float *x);
int procFromIndex(int idx);
int indexFromPosition(float *x);
void updateProcDomainBox_sfc();
int hilbertIndex(int coords[SFC_NDIM]);

#endif
#endif
