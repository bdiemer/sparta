/*************************************************************************************************
 *
 * This unit implements a slab-based domain decomposition where the box is split into slabs in
 * each dimension. Each process controls a rectilinear sub-volume.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _DOMAIN_SLABS_H_
#define _DOMAIN_SLABS_H_

#include "../global.h"

#if DOMAIN_DECOMP_SLABS

#include "../geometry.h"
#include "../statistics.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

#define MAX_DOMAIN_SPLITS 20

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct
{
	int n_split[3];
	float boundary[3][MAX_DOMAIN_SPLITS + 1];
} SlabDomain;

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

extern SlabDomain slabDomain;
extern int slab_domain_coor[3];

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initialDomainDecomposition_slabs();
void createMPICommunicator_slabs();
int procFromPosition_slabs(float *x);
void updateProcDomainBox_slabs();
float distanceFromDomainBox(float pos, int relative, int dim, int dir);
void maxDistanceFromDomainBox(float *x, int relative, float *max_dist, int *max_dim, int *max_dir);

#endif
#endif
