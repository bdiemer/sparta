/*************************************************************************************************
 *
 * This unit implements the exchange of halos between processes for the space-filling curve based
 * domain decomposition. The exchange is done asynchronously, meaning that processes do not wait
 * for sent messages to be received.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _EXCHANGE_SFC_H_
#define _EXCHANGE_SFC_H_

#include "../statistics.h"

/*************************************************************************************************
 * STRUCTURES
 *************************************************************************************************/

typedef struct
{
	short int proc;
	int n_exc;
} SFCExchangeNumber;

/*
 * Sent message structures. Each MPI message has a type corresponding to the size of an item,
 * meaning that we need to distinguish the number of items sent and the number of bytes in the
 * buffers. For clarity, both are stored.
 *
 * The message also carries an ID in case multiple messages are sent to the same process and need
 * to be distinguished. In this case, the msg_order field counts up the number of messages to the
 * same proc whereas msg_id contains the ID of the first halo sent.
 */
typedef struct
{
	int n_items;
	size_t n_bytes;
	MPI_Request req;
	char *buffer;
} DynArrayMessage;

typedef struct
{
	short int in_use;
	short int allocated;
	short int confirmed;
	short int proc;
	long int msg_id;
	DynArrayMessage msg_halos;
	DynArrayMessage msg_subs;
	DynArrayMessage msg_tcr[NTT];
	DynArrayMessage msg_iid[NTT];
	DynArrayMessage msg_rs[NTT][NRS];
	DynArrayMessage msg_al[NAL];
} HaloMessage;

typedef struct
{
	long int msg_id;
	MPI_Request req;
} HaloReceipt;

/*
 * Message identifiers. These cannot be defined as an enum because the tracer, DynArray etc tags
 * are increased to identify the index of the tracers etc. As an extra precaution, the tag space
 * does not overlap with other asynchronous communications such as particle exchange.
 */
#define HMES_TAG_RCVD 20
#define HMES_TAG_DONE 21
#define HMES_TAG_ALL_DONE 22
#define HMES_TAG_HALO 23
#define HMES_TAG_SUBS 24
#define HMES_TAG_TCR 30
#define HMES_TAG_IID 40
#define HMES_TAG_RS 100
#define HMES_TAG_AL 200

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void exchangeHalos_sfc(int snap_idx, DynArray *halos, LocalStatistics *ls, GlobalStatistics *gs);

#endif
