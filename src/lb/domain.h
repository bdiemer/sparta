/*************************************************************************************************
 *
 * This unit implements routines that are general to the domain decomposition in SPARTA.
 * Some of the routines are implemented by specific domain decomposition algorithms.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _DOMAIN_H_
#define _DOMAIN_H_

#include "../geometry.h"
#include "../statistics.h"

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

/*
 * The proc_domain_box is, by definition, a non-periodic volume that this process is responsible
 * for. Depending on the domain decomposition method, the exact meaning can vary. If slabs are
 * used, the proc_domain_box is exactly the volume of this proc. In the case of a space-filling
 * curve, the boxes of different procs can overlap.
 *
 * Thus, this box should not be taken to contain all the halos on this proc, or that a halo in
 * this box would live on this proc. It should only be used as a guess for the actual volume the
 * proc is working on.
 */
extern Box proc_domain_box;

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initialDomainDecomposition();
void createMPICommunicator();
int procFromPosition(float *x);
void computeHaloBox(DynArray *halos, Box *box);

#endif
