/*************************************************************************************************
 *
 * This unit implements load balancing for a space-filling curve based domain decomposition. Each
 * process is allocated a range of SFC indices.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <mpi.h>

#include "lb_sfc.h"
#include "exchange_sfc.h"
#include "domain_sfc.h"
#include "../memory.h"
#include "../halos/halo.h"

#if DOMAIN_DECOMP_SFC

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

#define LB_SFC_INERTIA 0.9
#define LB_ALGORITHM 1

/*
 * These switches can be used to write out extra information about the SFC load balancing for
 * debugging.
 */
#define WRITE_WEIGHT_FILE 0
#define WRITE_BOUNDARY_FILE 0

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

float haloWeight(Halo *h);
int compareSFCHaloProps(const void *a, const void *b);
void computeNewBoundaries(SFCHaloProperties *halos, int n);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Compute the weight (defined in some way) of each halo and gather those weights and SFC indices
 * on the main proc. Then decide how to split the weight as evenly as possible along the SFC.
 */
void loadBalance_sfc(int snap_idx, DynArray *halos)
{
	int i, j, counter, n_local, n_recv[n_proc], n_halos_all, displacements[n_proc], sub_idx,
			n_combined;
	float max_weight;
	int_least8_t *mask;
	SFCHaloProperties *buf_local, *buf_all, *buf_combined;
	Halo *h;

	/*
	 * Compute the SFC index and weight for each halo on this proc. Subhalos should be counted
	 * into the weight of their hosts, as they will very likely end up on the same process (unless
	 * they become isolated again etc). We ignore the more complicated cases of hosts that are
	 * becoming subs and subs that are becoming isolated again.
	 */
	n_local = 0;
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		if (isHost(h))
			n_local++;
	}
	buf_local = (SFCHaloProperties*) memAlloc(__FFL__, MID_DOMAINDECOMP,
			sizeof(SFCHaloProperties) * n_local);
	counter = 0;
	//max_weight_idx = -1;
	max_weight = 0.0;
	for (i = 0; i < halos->n; i++)
	{
		h = &(halos->h[i]);
		if (isHost(h))
		{
			buf_local[counter].idx = indexFromPosition(h->cd.x);
			buf_local[counter].weight = haloWeight(h);

			for (j = 0; j < h->subs.n; j++)
			{
				sub_idx = h->subs.sp[j].idx;
				buf_local[counter].weight += haloWeight(&(halos->h[sub_idx]));
			}

			if (buf_local[counter].weight > max_weight)
			{
				max_weight = buf_local[counter].weight;
				//max_weight_idx = i;
			}

#if CAREFUL
			if ((buf_local[counter].idx > SFC_MAX) || (buf_local[counter].idx < 0))
				error(__FFL__, "Found SFC index %d, invalid. Pos is %.2f %.2f %.2f.\n",
						buf_local[counter].idx, h->cd.x[0], h->cd.x[1], h->cd.x[2]);
#endif

			counter++;
		}
	}
#if CAREFUL
	assert(counter == n_local);
#endif

	/*
	 * Send / receive number of halos
	 */
	MPI_Gather(&n_local, 1, MPI_INT, n_recv, 1, MPI_INT, MAIN_PROC, comm);
	buf_all = NULL;
	n_halos_all = 0;
	if (is_main_proc)
	{
		for (i = 0; i < n_proc; i++)
		{
			displacements[i] = n_halos_all;
			n_halos_all += n_recv[i];
		}

		buf_all = (SFCHaloProperties*) memAlloc(__FFL__, MID_DOMAINDECOMP,
				sizeof(SFCHaloProperties) * n_halos_all);

		for (i = 0; i < n_proc; i++)
		{
			n_recv[i] *= sizeof(SFCHaloProperties);
			displacements[i] *= sizeof(SFCHaloProperties);
		}
	}

	/*
	 * Send / receive halo properties. After that, we can free the local buffer.
	 */
	MPI_Gatherv(buf_local, n_local * sizeof(SFCHaloProperties), MPI_BYTE, buf_all, n_recv,
			displacements, MPI_BYTE,
			MAIN_PROC, comm);
	memFree(__FFL__, MID_DOMAINDECOMP, buf_local, sizeof(SFCHaloProperties) * n_local);

	if (is_main_proc)
	{
		/*
		 * Sort by SFC index, and combine all halos that have the same index as they are
		 * indistinguishable in terms of the domain decomposition (i.e., they will end up on the
		 * same proc anyway).
		 */
		qsort(buf_all, n_halos_all, sizeof(SFCHaloProperties), &compareSFCHaloProps);

		mask = (int_least8_t*) memAlloc(__FFL__, MID_DOMAINDECOMP,
				sizeof(int_least8_t) * n_halos_all);
		for (i = 0; i < n_halos_all; i++)
			mask[i] = 0;

		i = 0;
		j = 1;
		n_combined = 1;
		while (j < n_halos_all)
		{
			if (buf_all[i].idx == buf_all[j].idx)
			{
				buf_all[i].weight += buf_all[j].weight;
				mask[j] = 1;
				j++;
			} else
			{
				n_combined++;
				j++;
				i = j - 1;
			}
		}

		buf_combined = (SFCHaloProperties*) memAlloc(__FFL__, MID_DOMAINDECOMP,
				sizeof(SFCHaloProperties) * n_combined);
		counter = 0;
		for (i = 0; i < n_halos_all; i++)
		{
			if (!mask[i])
			{
				memcpy(&(buf_combined[counter]), &(buf_all[i]), sizeof(SFCHaloProperties));
				counter++;
			}
		}
		memFree(__FFL__, MID_DOMAINDECOMP, buf_all, sizeof(SFCHaloProperties) * n_halos_all);
		memFree(__FFL__, MID_DOMAINDECOMP, mask, sizeof(int_least8_t) * n_halos_all);

		/*
		 * If desired, write all the weights to a text file. This can be used to test other domain
		 * decomposition algorithms on realistic data.
		 */
#if WRITE_WEIGHT_FILE
		FILE *fWeights;
		char line[3000];

		sprintf(line, "weights_%02d.txt", snap_idx);
		fWeights = fopen(line, "w");
		for (i = 0; i < n_combined; i++)
		fprintf(fWeights, "%10d %.6f\n", buf_combined[i].idx, buf_combined[i].weight);
		fclose(fWeights);
#endif

		/*
		 * Compute the new boundaries
		 */
		computeNewBoundaries(buf_combined, n_combined);
		memFree(__FFL__, MID_DOMAINDECOMP, buf_combined, sizeof(SFCHaloProperties) * n_combined);
	}

	/*
	 * Communicate the new boundaries to all procs
	 */
	MPI_Bcast(&sfcDomain, sizeof(SFCDomain), MPI_BYTE, MAIN_PROC, comm);
	updateProcDomainBox_sfc();

	/*
	 * If necessary, write out a text file with the new index boundaries. This can be used to
	 * analyze the domain decomposition later.
	 */
#if WRITE_BOUNDARY_FILE
	if (is_main_proc)
	{
		FILE *fBoundaries;
		char line[3000];

		if (snap_idx == config.lb_adjust_min_snap)
		fBoundaries = fopen("boundaries.txt", "w");
		else
		fBoundaries = fopen("boundaries.txt", "a");
		sprintf(line, "%3d  ", snap_idx);
		for (i = 0; i <= n_proc; i++)
		{
			sprintf(line, "%s %.5f", line, (float) sfcDomain.index_bdry[i] / SFC_MAX);
		}
		sprintf(line, "%s\n", line);
		fprintf(fBoundaries, line);
		fclose(fBoundaries);
	}
#endif
}

/*
 * The actual load balancing algorithm. Given weights and indices, find the optimal index
 * boundaries. This is executed only on the main proc.
 *
 * The function expects that the weights and indices are already sorted, and that each index
 * is unique (i.e., that halos with the same index have already been combined).
 */
void computeNewBoundaries(SFCHaloProperties *halos, int n)
{
	int i, j, index_bdry_new[MAX_PROCS + 1], max_weight_idx;
	float cumulative, target_weight, weight_proposed[MAX_PROCS], weight_new[MAX_PROCS], max_weight;

	/*
	 * Compute the cumulative weight along the curve, and thus the target weight for a perfectly
	 * equal decomposition.
	 */
	cumulative = 0.0;
	max_weight = 0.0;
	max_weight_idx = 0;
	for (i = 0; i < n; i++)
	{
#if CAREFUL
		if ((halos[i].idx > SFC_MAX) || (halos[i].idx < 0))
			error(__FFL__, "Found SFC index %d, invalid.\n", halos[i].idx);
#endif
		cumulative += halos[i].weight;

		if (halos[i].weight > max_weight)
		{
			max_weight = halos[i].weight;
			max_weight_idx = i;
		}
	}
	target_weight = cumulative / n_proc;

	output(2,
			"[Main]          Load balancing: Max mem halo is idx %d, SFC idx %d, mem %.2e MB (incl subs), %.4f of total, %.4f of target.\n",
			max_weight_idx, halos[max_weight_idx].idx, halos[max_weight_idx].weight,
			halos[max_weight_idx].weight / cumulative,
			halos[max_weight_idx].weight / target_weight);

	/*
	 * Reset the weights
	 */
	for (i = 0; i <= n_proc; i++)
	{
		weight_proposed[i] = 0.0;
		weight_new[i] = 0.0;
	}

	/*
	 * Compute the index boundaries. There are a number of algorithms that can be used.
	 */
	switch (LB_ALGORITHM)
	{
	case 0:
		/*
		 * A greedy algorithm that goes through each proc and adds more weight until a target
		 * weight has been reached. In the end, the new and old index boundaries are weighted with
		 * the inertia parameter to avoid fast changes. This algorithm has a number of known
		 * problems:
		 *
		 * - if the weight of an index gets close to the target weight, a proc can receive a very
		 *   large weight up to max + target.
		 * - if such a case happens, the procs behind the high-weight proc might get weight zero.
		 * - averaging in index space does not necessarily make sense in weight space.
		 */
		index_bdry_new[0] = 0;
		j = 1;
		cumulative = 0.0;
		for (i = 0; i < n; i++)
		{
			cumulative += halos[i].weight;
			weight_proposed[j - 1] += halos[i].weight;
			if (cumulative > j * target_weight)
			{
				index_bdry_new[j] = halos[i].idx;
				j++;
			}
		}
		for (i = j; i <= n_proc; i++)
			index_bdry_new[i] = SFC_MAX - (n_proc - i);
		for (i = 0; i <= n_proc; i++)
		{
			sfcDomain.index_bdry[i] = (int) (index_bdry_new[i] * (1.0 - LB_SFC_INERTIA)
					+ sfcDomain.index_bdry[i] * LB_SFC_INERTIA);
		}
		break;

	case 1:
		/*
		 * Another greedy algorithm, but one that recomputes the target weight at each step. When
		 * the next weight exceeds the target, it is added only if...
		 */
		index_bdry_new[0] = 0;
		j = 1;
		cumulative = 0.0;

		float w_normal, w_target_new;
		int k;

		w_normal = 0.0;
		for (i = 0; i < n; i++)
		{
			if (halos[i].weight < target_weight)
				w_normal += halos[i].weight;
		}
		w_target_new = w_normal / n_proc;

		for (i = 0; i < n; i++)
		{
			if (j < n_proc)
			{
				if ((halos[i].weight > target_weight)
						|| (cumulative + halos[i].weight - w_target_new > halos[i].weight * 0.5))
				{
					index_bdry_new[j] = halos[i].idx;
//					output(0, "j %d, bdry %10d, w_target_new %.4f\n", j, index_bdry_new[j],
//							w_target_new);
					j++;

					w_target_new = 0.0;
					for (k = i; k < n; k++)
					{
						if (halos[k].weight < target_weight)
							w_target_new += halos[k].weight;
					}
					w_target_new /= (float) (n_proc - j + 1);
					cumulative = 0.0;
				}
			}

			cumulative += halos[i].weight;
			weight_proposed[j - 1] += halos[i].weight;
		}

		for (i = j; i <= n_proc; i++)
			index_bdry_new[i] = SFC_MAX - (n_proc - i);
		for (i = 0; i <= n_proc; i++)
			sfcDomain.index_bdry[i] = index_bdry_new[i];
		break;
	}

	/*
	 * Count actual weight that has been assigned to each process
	 */
	j = 0;
	for (i = 0; i < n; i++)
	{
		if (halos[i].idx >= sfcDomain.index_bdry[j + 1])
			j++;
		weight_new[j] += halos[i].weight;
	}

	for (i = 0; i < n_proc; i++)
		output(2,
				"[Main]          New SFC boundary %2d = %8d = %.5f SFC_MAX, proposed weight %.2e (%.2f), new weight %.2e (%.2f)\n",
				i, sfcDomain.index_bdry[i + 1], (float) sfcDomain.index_bdry[i + 1] / SFC_MAX,
				weight_proposed[i], weight_proposed[i] / target_weight, weight_new[i],
				weight_new[i] / target_weight);
}

/*
 * This function determines how halos are weighted in the domain decomposition. For example, one
 * can weight by memory consumption or by the anticipated computational load.
 */
float haloWeight(Halo *h)
{
	return (float) (MEGABYTE * haloMemory(h));
}

int compareSFCHaloProps(const void *a, const void *b)
{
	if (((SFCHaloProperties*) a)->idx > ((SFCHaloProperties*) b)->idx)
		return 1;
	else if (((SFCHaloProperties*) a)->idx < ((SFCHaloProperties*) b)->idx)
		return -1;
	else
		return 0;
}

#endif
