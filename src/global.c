/*************************************************************************************************
 *
 * This unit implements the system of compiler flags used in SPARTA, as well as constants and
 * functions such as output, errors and warnings.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <stdarg.h>

#include "global.h"
#include "config.h"
#include "memory.h"
#include "geometry.h"
#include "lb/domain_sfc.h"

/*************************************************************************************************
 * GLOBAL VARIABLES
 *************************************************************************************************/

MPI_Comm comm = COMM_NO_MPI;
int proc;
int n_proc;
int is_main_proc;
MPITypes mpiTypes;

/*************************************************************************************************
 * FUNCTIONS - OUTPUT
 *************************************************************************************************/

/*
 * This function should be used for all standard output in SPARTA. If desired, the output is
 * flushed after every write operation. The impact of this parameter depends on the system to some
 * extent.
 */
void output(int level, const char *format, ...)
{
	if (level <= config.log_level)
	{
		va_list args;
		va_start(args, format);
		vprintf(format, args);
		va_end(args);

		if (config.log_flush)
			fflush(stdout);
	}
}

void printLine(int level)
{
	output(level,
			"---------------------------------------------------------------------------------------------------------------------------------------------\n");
}

/*
 * The warning function does not take a log level: either the warning is output or it is not.
 */
void warning(const char *format, ...)
{
	output(0,
			"*************************************         WARNING         *************************************\n");
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
	output(0,
			"***************************************************************************************************\n");
	fflush(stdout);
}

void error(const char *file, const char *func, int line, const char *format, ...)
{
	output(0,
			"*************************************          ERROR          *************************************\n");
	output(0, "[%4d] FILE %s, FUNCTION %s, LINE %d:\n", proc, file, func, line);
	va_list args;
	va_start(args, format);
	vprintf(format, args);
	va_end(args);
	output(0,
			"***************************************************************************************************\n");
	fflush(stdout);
	abortCode();
}

/*
 * To give the user control over the abundance of warnings, we use the err_level_* config parameters
 * which determine whether an issue is ignored or acted upon.
 */
void warningOrError(const char *file, const char *func, int line, int error_level,
		const char *format, ...)
{
	if (error_level == ERR_LEVEL_IGNORE)
		return;

	char tmpstr[1000];

	va_list args;
	va_start(args, format);
	vsprintf(tmpstr, format, args);
	va_end(args);

	if (error_level == ERR_LEVEL_WARNING)
		warning(tmpstr);
	else if (error_level == ERR_LEVEL_ERROR)
		error(file, func, line, tmpstr);
	else
		error(__FFL__, "Unknown error level %d (internal error).\n", error_level);
}

/*************************************************************************************************
 * FUNCTIONS - BASICS
 *************************************************************************************************/

void abortCode()
{
	freeGlobalMemory();
	if (comm != COMM_NO_MPI)
		MPI_Abort(comm, EXIT_FAILURE);
	else
		exit(1);
}

void delay(int milliseconds)
{
	long pause;
	clock_t now, then;

	pause = milliseconds * (CLOCKS_PER_SEC / 1000);
	now = then = clock();
	while ((now - then) < (clock_t) pause)
		now = clock();
}

/*************************************************************************************************
 * FUNCTIONS - INITIALIZE AND DESTROY SPECIFIC TYPES
 *************************************************************************************************/

/*
 * Allocate global variables that are not fixed-size. This function needs the config to be fully
 * loaded.
 */
void allocateGlobalMemory()
{
#if DO_READ_PARTICLES
	snap_boxes = (Box*) memAlloc(__FFL__, MID_BOXES,
			sizeof(Box) * config.n_snaps * config.n_files_per_snap);
#endif

#if DOMAIN_DECOMP_SFC
#if DOMAIN_SFC_INDEX_TABLE
	sfcIndexLookup.idx = memAlloc(__FFL__, MID_SFCINDEXLOOKUP, sizeof(int) * SFC_MAX);
	sfcIndexLookup.pos = memAlloc(__FFL__, MID_SFCINDEXLOOKUP, sizeof(int) * SFC_MAX * 3);
#endif
#endif
}

/*
 * This function must mirror the allocate function above.
 */
void freeGlobalMemory()
{
#if DO_READ_PARTICLES
	memFree(__FFL__, MID_BOXES, snap_boxes, sizeof(Box) * config.n_snaps * config.n_files_per_snap);
#endif

#if DOMAIN_DECOMP_SFC
#if DOMAIN_SFC_INDEX_TABLE
	memFree(__FFL__, MID_SFCINDEXLOOKUP, sfcIndexLookup.idx, sizeof(int) * SFC_MAX);
	memFree(__FFL__, MID_SFCINDEXLOOKUP, sfcIndexLookup.pos, sizeof(int) * SFC_MAX * 3);
#endif
#endif
}

