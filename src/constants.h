/*************************************************************************************************
 *
 * This file contains mathematical, physical and astronomical constants.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

/*************************************************************************************************
 * MATHEMATICAL CONSTANTS
 *************************************************************************************************/

#define MATH_pi 3.141592653589
#define MATH_2pi 6.28318530717959

#define MATH_e 2.7182818284

/*************************************************************************************************
 * PHYSICS CONSTANTS
 *************************************************************************************************/

// speed of light in vacuum (def) cm/s (PDG 2018)
#define PHY_c 2.99792458E10

// proton mass g (PDG 2018)
#define PHY_mp 1.672621898e-24

// Boltzmann constant (PDG 2018)
#define PHY_kB 1.38064852E-16

// Gravitational constant in cm^3 / g / s^2 (PDG 2018)
#define PHY_G 6.67408E-8

// eV in erg (PDG 2018)
#define PHY_eV 1.6021766208E-12

// Planck constant erg s (PDG 2018)
#define PHY_h 6.626070040E-27

// Rydberg energy (mnucleus = infinity) eV (PDG 2018)
#define PHY_Rydberg 13.605693009

/*************************************************************************************************
 * ASTRONOMY CONSTANTS
 *************************************************************************************************/

// pc / kpc / Mpc in cm (PDG 2018)
#define AST_pc  3.08567758149E18
#define AST_kpc 3.08567758149E21
#define AST_Mpc 3.08567758149E24

// 1 year / Gyr in seconds (PDG 2018)
#define AST_year 31556925.2
#define AST_Gyr 3.15569252E16

// Solar mass (IAU 2015)
#define AST_Msun 1.9884754153381438E33

// G in kpc km^2 / M_sun / s^2. This constant is computed from the cgs version but given with more
// significant digits to preserve consistency with the Mpc and Msun units.
#define AST_G 4.300917270038E-6

// G in kpc^3 / M_sun / Gyr^2. This constant is computed from the cgs version but given with more
// significant digits to preserve consistency with the Mpc and Msun units.
#define AST_G_Gyr 4.498309769494E-6

/*
 * Background density in units of M_sun h^2 / kpc^3 or / Mpc^3. This
 * number is calculated from
 *
 * rho_crit = (3 H_0^2) / (8 pi G)
 *
 * Taking units into account, the value is
 *
 * 3.0 * 100 * 100 / 8.0 / MATH_pi / PHY_G / AST_Msun * AST_pc * 1000.0 / 1E6 * 1E10
 *
 * Since this is somewhat complicated, the numerical value is hard-coded here
 */
#define AST_RHO_CRIT_0_KPC3 2.77536627245708E2
#define AST_RHO_CRIT_0_MPC3 2.77536627245708E11

/*
 * The critical collapse density is actually a weak function of cosmology and redshift, but for
 * now treated as a constant.
 */
#define AST_DELTA_COLLAPSE 1.68647

/*
 * Convert kinetic energy per unit mass [units (kpc / h / Gyr)^2] to potential per unit mass and
 * unit gravitational constant, phi/G [units Msun/kpc]. Thus,
 *
 * [E_kin / m / G] = (kpc / h / Gyr)^2 / G = (1 / h^2) Msun / kpc
 *
 * where G is in (kpc^3 / M_sun / Gyr^2) (see above). To convert to phi/G units, we divide by the
 * value of h^2.
 */
#define CONVERSION_KIN_TO_POT (1.0 / (AST_G_Gyr * config.h * config.h))

/*
 * We can convert back to velocity dispersions by dividing by the constant above, and then convert
 * to (km/s)^2 by multiplying by this number.
 */
#define	CONVERSION_KIN_TO_KMS (pow(AST_kpc / 1E5 / AST_Gyr / config.h, 2))

#endif
