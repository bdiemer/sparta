/*************************************************************************************************
 *
 * This unit implements functions that are common to all result types.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "results.h"
#include "../utils.h"
#include "../halos/halo.h"
#include "../tracers/tracers.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initConfigResults(int step)
{
	int rs;

	for (rs = 0; rs < NRS; rs++)
	{
		if (rsprops[rs].initConfig != NULL)
			rsprops[rs].initConfig(step);
	}
}

/*
 * Perform all necessary analysis routines on the tracers in this halo.
 */
void runResults(Halo *halo, int snap_idx, LocalStatistics *ls)
{
	int i, tt, rs, n_rs_previous;
	double current_time;
	TracerType *ttp;
	Tracer *tcr;

	current_time = MPI_Wtime();
	ls->timers_2[T2_HALOLOOP] += current_time;
	ls->timers_2[T2_TCR_RESULTS] -= current_time;

	for (tt = 0; tt < NTT; tt++)
	{
		ttp = &(halo->tt[tt]);

		/*
		 * If this tracer type is turned off altogether, we need not even check the indiviual
		 * results.
		 */
		if (!halo->instr_tcr[tt])
			continue;

		/*
		 * If this tracer type is subhalos and we are dealing with a subhalo, we can continue.
		 */
#if DO_TRACER_SUBHALOS
		if ((tt == SHO) && (isSub(halo)))
			continue;
#endif

		/*
		 * Go through result types. We run each result on all tracers of a type an then sort the
		 * result array to make sure it can be searched. This order is important, as some results
		 * may even look for previously computed results.
		 */
		for (rs = 0; rs < NRS; rs++)
		{
			ls->timers_3[tt * NRS + rs] -= current_time;

			if (halo->instr_rs[tt][rs])
			{
				n_rs_previous = halo->tt[tt].rs[rs].n;
				for (i = 0; i < halo->tt[tt].tcr.n; i++)
				{
					tcr = &(halo->tt[tt].tcr.tcr[i]);
					if (isActive(tcr) && resultShouldBeRun(tcr->status_rs[rs]))
					{
						rsprops[rs].runResult(snap_idx, halo, tcr, ttp, n_rs_previous,
								(void*) &(ls->tt[tt].rs[rs]));
					}
				}
			}
			qsort(halo->tt[tt].rs[rs].data, halo->tt[tt].rs[rs].n, rsprops[rs].size,
					&compareResults);

			current_time = MPI_Wtime();
			ls->timers_3[tt * NRS + rs] += current_time;
		}
	}

	current_time = MPI_Wtime();
	ls->timers_2[T2_TCR_RESULTS] += current_time;
	ls->timers_2[T2_HALOLOOP] -= current_time;
}

/*
 * When a tracer is found but is on the ignore list, results get a chance to say whether this
 * tracer should become active again. This function should only be called if there is at least
 * one result that is active and has a non-NULL check function.
 *
 * However, we still need to check that a) this tracer type is not turned off altogether for
 * this halo and b) this result is not turned off. Only if those are true, we evaluate the check
 * function.
 */
int checkRecreateIgnoredTracer(Halo *halo, int snap_idx, ID id, int tt)
{
	int rs, ret;

	ret = 0;
	if (!halo->instr_tcr[tt])
		return ret;

	for (rs = 0; rs < NRS; rs++)
	{
		if ((halo->instr_rs[tt][rs]) && (rsprops[rs].checkRecreateIgnoredTracer != NULL))
		{
			ret = rsprops[rs].checkRecreateIgnoredTracer(halo, snap_idx, id, tt);
			if (ret)
				return ret;
		}
	}

	return ret;
}

int resultShouldBeRun(int rs_status)
{
#if CAREFUL
	if (rs_status == RS_STATUS_NONE)
		error(__FFL__, "Found RS_STATUS_NONE.\n");
#endif
	return rs_status >= RS_STATUS_SEEKING;
}

int resultIsDone(int rs_status)
{
#if CAREFUL
	if (rs_status == RS_STATUS_NONE)
		error(__FFL__, "Found RS_STATUS_NONE.\n");
#endif
	return rs_status < RS_STATUS_SEEKING;
}

int resultIsNotDone(int rs_status)
{
#if CAREFUL
	if (rs_status == RS_STATUS_NONE)
		error(__FFL__, "Found RS_STATUS_NONE.\n");
#endif
	return rs_status >= RS_STATUS_SEEKING;
}
