/*************************************************************************************************
 *
 * This unit implements splashback results, i.e. records the times, radii, and enclosed masses
 * where tracers reach the apocenter of their first orbit.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _RESULT_SPLASHBACK_H_
#define _RESULT_SPLASHBACK_H_

#include "results.h"
#include "../statistics.h"
#include "../io/io_hdf5.h"

#if DO_RESULT_SPLASHBACK

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * The standrad fields plus statuses indicating whether we are searching for pericenter or
 * apocenter.
 */
#define SBK_STATUS_NONE RS_STATUS_NONE
#define SBK_STATUS_SUCCESS RS_STATUS_SUCCESS
#define SBK_STATUS_FAILED RS_STATUS_FAILED
#define SBK_STATUS_NOT_SEEKING RS_STATUS_NOT_SEEKING
#define SBK_STATUS_MAINTAIN RS_STATUS_MAINTAIN
#define SBK_STATUS_SEEKING RS_STATUS_SEEKING
#define SBK_STATUS_SEEKING_MIN (SBK_STATUS_SEEKING + 1)
#define SBK_STATUS_MIN_FOUND (SBK_STATUS_SEEKING + 2)
#define SBK_STATUS_SEEKING_MAX (SBK_STATUS_SEEKING + 3)

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initResultSplashback(ResultSplashback *res_inf);
int compareResultSplashbackTime(const void *a, const void *b);
void runResultSplashback(int snap_idx, Halo *halo, Tracer *tcr, TracerType *tt, int n_rs_previous,
		void *rs_vp);
int outputFieldsResultSplashback(OutputField *outfields, int max_n_fields);

#endif

#endif
