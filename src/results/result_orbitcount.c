/*************************************************************************************************
 *
 * This unit implements orbit counting.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>

#include "result_orbitcount.h"
#include "../config.h"
#include "../utils.h"
#include "../geometry.h"
#include "../halos/halo.h"
#include "../tracers/tracers.h"

#if DO_RESULT_ORBITCOUNT

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Parameters of the orbit count algorithm:
 *
 * MAX_ALIGNMENT_FIRSTPERI       If we detect a first pericenter, we compute its alignment (dot
 *                               product) with the direction where the particle was first tracked.
 *                               The current alignment must be less than this number to be counted
 *                               as a pericenter (i.e., if the pericenter occurs close to the
 *                               direction of infall, we conclude that it is likely spurious).
 *
 * MAX_ALIGNMENT_ZEROPERI        If a tracer's alignment (dot product) is less than this number
 *                               but no pericenter or lower limit has been recorded, we conclude
 *                               set the count to be a lower limit because this tracer is likely
 *                               on a very circular orbit, meaning that it is on the other side
 *                               of the halo without a clear switch in its radial velocity.
 *
 * MAX_VELOCITY_SWITCHES         If a tracer switches velocity from positive to negative and back
 *                               this many times, but has no pericenter recorded, we conclude that
 *                               the orbit is unresolved in time and set n_pericenter to be a lower
 *                               limit.
 */
#define MAX_ALIGNMENT_FIRSTPERI 0.5
#define MAX_ALIGNMENT_ZEROPERI -0.8
#define MAX_VELOCITY_SWITCHES 3

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int orbitCountIsFinal(ResultOrbitCount *res_oct);
void ensureOctExists(int snap_idx, ResultOrbitCount **res_oct_ptr, Tracer *tcr, TracerType *tt);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initConfigResultOrbitCount(int step)
{
	switch (step)
	{
	case CONFIG_INIT_STEP_DEFAULT:
		config.res_oct_max_norbit = DEFAULT_RES_OCT_MAX_NORBIT;
		break;

	case CONFIG_INIT_STEP_CHECK:
		assert(config.res_oct_max_norbit > 0);
		break;

	case CONFIG_INIT_STEP_CONFIG:
		break;

	case CONFIG_INIT_STEP_PRINT:
		output(0, "[Main] Result: Orbit count\n");
		output(0, "[Main]     res_oct_max_norbit            %d\n", config.res_oct_max_norbit);
		break;
	}
}

void initResultOrbitCount(ResultOrbitCount *res_oct)
{
	res_oct->tracer_id = INVALID_ID;
	res_oct->n_pericenter = 0;
	res_oct->last_pericenter_snap = 0;
	res_oct->n_is_lower_limit = 0;
	res_oct->is_final = 0;
}

/*
 * This function returns True if an orbit count result is final, that is, will not be updated
 * any more. Due to the different meaning of the is_final field depending on whether we have found
 * any apocenters, this decision is abstracted into a function.
 */
int orbitCountIsFinal(ResultOrbitCount *res_oct)
{
	return ((res_oct->n_pericenter > 0) && (res_oct->is_final));
}

/*
 * Check whether an OCT result has already been found or created, and if not, create it. We set the
 * direction to infalling regardless of whether a tracer is created outside or inside the halo, in
 * either case we want to be watching for pericenter events.
 */
void ensureOctExists(int snap_idx, ResultOrbitCount **res_oct_ptr, Tracer *tcr, TracerType *tt)
{
	ResultOrbitCount *res_oct;

	res_oct = *res_oct_ptr;

	if (res_oct != NULL)
		return;

	debugTracer(NULL, tcr, "OCT, creating new event, rs_status %d, oct_direction %d",
			tcr->status_rs[OCT], OCT_DIRECTION_INFALLING);

	res_oct = addResultOrbitCount(__FFL__, &(tt->rs[OCT]));
	res_oct->tracer_id = tcr->id;
	res_oct->n_pericenter = 0;
	res_oct->n_crossings = 0;
	res_oct->n_is_lower_limit = 0;

	if (tcr->status_entered == TCR_ENTERED_R200M)
		res_oct->infall_snap = snap_idx;
	else
		res_oct->infall_snap = INVALID_IDX;

	*res_oct_ptr = res_oct;
}

void runResultOrbitCount(int snap_idx, Halo *halo, Tracer *tcr, TracerType *tt, int n_rs_previous,
		void *rs_vp)
{
	ResultOrbitCount *res_oct = NULL;
	ResultStatistics *rs;
	void *ptr;

	/*
	 * We start with a few paranoid checks.
	 */
#if PARANOID
	/*
	 * The orbit count result is turned off for ghost halos
	 */
#if DO_GHOSTS
	if (isGhost(halo))
		error(__FFL__,
				"Trying to run orbit count result on tracer ID %ld in ghost halo ID %ld. This should never happen.\n",
				tcr->id, halo->cd.id);
#endif

	/*
	 * If the status is SUCCESS, this routine should not be called in the first place.
	 */
	if (tcr->status_rs[OCT] == OCT_STATUS_SUCCESS)
	{
		error(__FFL__, "Found tracer with status success in OCT; ID %ld, halo %ld.\n", tcr->id,
				halo->cd.id);
	}

	/*
	 * Later on, we check for the TCR_ENTERED_NO status. This status cannot be set when the tracer
	 * was born in the halo, if it was that would lead to inconsistencies.
	 */
	if ((tcr->status_entered == TCR_ENTERED_NO) && (tcr->status_rs[OCT] == OCT_STATUS_BORN_IN_HALO))
	{
		error(__FFL__,
				"Found tracer %ld in halo %ld, has not entered R200m but born inside halo.\n",
				tcr->id, halo->cd.id);
	}
#endif

	/*
	 * A few initial tasks
	 */
	debugTracer(halo, tcr, "OCT, analyzing, rs_status %d, direction %d.", tcr->status_rs[OCT],
			tcr->oct_direction);
	rs = (ResultStatistics*) rs_vp;
	rs->n[RSSTAT_ANALYZED]++;

	/*
	 * If this is the first time we are encountering this tracer, we set its direction to infalling,
	 * as in, we are looking for pericenters.
	 */
	if (tcr->oct_direction == -1)
		tcr->oct_direction = OCT_DIRECTION_INFALLING;

	/*
	 * Look for a pre-existing result. We always need to check this case first because a tracer
	 * can have existed, been deleted, and recreated. In this case, its orbit counter may have
	 * reached the maximum and we would set its status to OCT_STATUS_SUCCESS.
	 *
	 * In this function, we assume that we are treating particles that were born outside the halo.
	 * Otherwise, they will first be analyzed with status OCT_STATUS_BORN_IN_HALO which we treat
	 * below. Thus, we should be safe to assume that they are on an infalling trajectory initially.
	 *
	 * For subhalo tracers, the logic is a little different. They are almost always born inside the
	 * halo (formally), but should never have had any previous orbits by construction. Thus, when a
	 * subhalo tracer is born "inside" the halo, the SEEKING status is set instead of
	 * OCT_STATUS_BORN_IN_HALO.
	 */
	ptr = bsearch(&(tcr->id), tt->rs[OCT].data, n_rs_previous, rsprops[OCT].size, &compareResults);
	if (ptr != NULL)
	{
		res_oct = (ResultOrbitCount*) ptr;
		if (orbitCountIsFinal(res_oct))
			tcr->status_rs[OCT] = OCT_STATUS_SUCCESS;

		if (res_oct->n_pericenter == 0)
		{
			debugTracer(halo, tcr,
					"OCT, found existing event, n_peri = %d, ll = %d, n_crossings = %d, infall_snap = %d, oct_direction = %d.",
					res_oct->n_pericenter, res_oct->n_is_lower_limit, res_oct->n_crossings,
					res_oct->infall_snap, tcr->oct_direction);
		} else
		{
			debugTracer(halo, tcr,
					"OCT, found existing event, n_peri = %d, ll = %d, final = %d, last_peri_snap = %d, oct_direction = %d.",
					res_oct->n_pericenter, res_oct->n_is_lower_limit, res_oct->is_final,
					res_oct->last_pericenter_snap, tcr->oct_direction);
		}
	}

	/*
	 * If tracer was set to SUCCESS above (because an old result was found that was final), we can
	 * stop. This can happen if a tracer is recreated based on another tracer result, but the
	 * status of OCT is set to SEEKING in such cases.
	 *
	 * Note that there is no point in checking for SUCCESS before looking for the existing result:
	 * in that case, this analysis would not even be executed in the first place.
	 */
	if (tcr->status_rs[OCT] == OCT_STATUS_SUCCESS)
		return;

	/*
	 * If the particle is inside R200m, we definitely have to create an OCT result for a number
	 * of reasons:
	 *
	 * - we need to record the snapshot of infall for one of the trajectory checks below
	 * - if the tracer was born in the halo, we need to record a lower limit
	 *
	 * If the tracer has not entered R200m yet, we avoid creating an OCT because there are often
	 * many tracers that are tracked at some point but never end up in the halo.
	 *
	 * If the tracer has entered the halo but its infall snap has not been set in previous
	 * snapshots, this must be its infall snap and we set it.
	 *
	 * There is also the possibility that a tracer was created during subtagging
	 * (TCR_ENTERED_SUBTAG). In that case, we also create the OCT because the resulting
	 * OCT_STATUS_BORN_IN_HALO status will trigger setting a lower limit below. This is a tricky
	 * case: tracers can be detected when subtagging even though their trajectory would be fine for
	 * detecting pericenters. On the other hand, this will generally happen to subhalos where the
	 * orbit counter quickly becomes meaningless anyway.
	 */
	if (tcr->status_entered != TCR_ENTERED_NO)
	{
		ensureOctExists(snap_idx, &res_oct, tcr, tt);

		if (res_oct->infall_snap == INVALID_IDX)
		{
			res_oct->infall_snap = snap_idx;
		}
	}

	/*
	 * If OCT_STATUS_BORN_IN_HALO, we set a flag indicating that the orbit count is a lower limit.
	 * This can happen even if there was a pre-existing event, namely if a tracer was deleted and
	 * recreated. In that case, its count is still a lower limit because we did not resolve the
	 * most recent infall.
	 */
	if (tcr->status_rs[OCT] == OCT_STATUS_BORN_IN_HALO)
	{
		if (res_oct == NULL)
			error(__FFL__,
					"OCT: Found OCT_STATUS_BORN_IN_HALO but no OCT created yet (halo orig ID %ld, halo status %d, tracer ID %ld, has_entered %d, r %.2f, R %.2f, r/R %.2f).\n",
					haloOriginalID(halo), halo->status, tcr->id, tcr->status_entered,
					tcr->r[STCL_TCR - 1], haloR200m(halo, snap_idx),
					tcr->r[STCL_TCR - 1] / haloR200m(halo, snap_idx));

		res_oct->n_is_lower_limit = 1;
		debugTracer(halo, tcr, "OCT, tracer was born in halo, set lower limit.");
	}

	/*
	 * If we've made it to this point, we're SEEKING in future snapshots.
	 */
	tcr->status_rs[OCT] = OCT_STATUS_SEEKING;

	/*
	 * If the trajectory has fewer than three elements, we cannot run this analysis.
	 */
	if (snap_idx - tcr->first_snap < 2)
	{
		debugTracer(halo, tcr, "OCT, trajectory is not long enough yet (first snap %d).",
				tcr->first_snap);
		return;
	}

	/*
	 * At this point, we need to seriously consider that the tracer might be undergoing a pericenter
	 * at this snapshot. We create certain shortcuts for the snapshots involved, that is, this
	 * snapshot and two snapshots back in time. The pericenter will always happen between i0 and
	 * i1, if it happens.
	 */
	int i, idx, i1, i2, s1, s2, do_peri, seeking_first_peri, alignment_available;
	float tdyn_since_infall, vec_ifl[3], vec_tcr[3], alignment[3];

	i1 = STCL_TCR - 2;
	i2 = STCL_TCR - 1;
	s1 = snap_idx - 1;
	s2 = snap_idx;

	/*
	 * General check whether we have already detected a pericenter or a lower limit. The algorithm
	 * proceeds quite differently depending on this distinction.
	 */
	seeking_first_peri = (!((res_oct != NULL)
			&& (res_oct->n_is_lower_limit || (res_oct->n_pericenter > 0))));

	/*
	 * If we are looking for a first pericenter and are using the 3D position, we will need it
	 * either to check the validity of a pericenter or to look for negative alignment. We compute
	 * the alignment at i0, i1, and i2.
	 *
	 * In some cases, the infall coordinates could not be computed because a particle was not
	 * tracked before it entered the halo. In this case, we fall back to the non-3D algorithm.
	 * Note that the indication of invalid coordinates is that they are zero, but checking the
	 * born_in_halo flag is more reliable.
	 *
	 * If the tracer was born inside the halo, the OCT has already been set to lower limit. Also,
	 * if it is a lower limit, we are not using the alignment to evaluate any further pericenters.
	 * Thus, we need to compute the alignment only if the OCT is not a lower limit, and we are
	 * guaranteed to have a valid initial angle in this case.
	 */
	alignment_available = 0;

	if (seeking_first_peri)
	{
#if PARANOID
		if (tcr->theta_ini < -10.0)
			error(__FFL__,
					"Found uninitialized initial angle theta_ini in tracer ID %ld (first snap %d).\n",
					tcr->id, tcr->first_snap);
		if (tcr->phi_ini < -10.0)
			error(__FFL__,
					"Found uninitialized initial angle phi_ini in tracer ID %ld (first snap %d).\n",
					tcr->id, tcr->first_snap);
#endif
		cartesianCoordinatesUnit(tcr->theta_ini, tcr->phi_ini, &(vec_ifl[0]), &(vec_ifl[1]),
				&(vec_ifl[2]));
		for (i = 0; i < 3; i++)
		{
			idx = STCL_TCR - 3 + i;
#if PARANOID
			if (tcr->theta[idx] < -10.0)
				error(__FFL__,
						"Found uninitialized angle theta in tracer ID %ld (first snap %d, stencil idx %d).\n",
						tcr->theta[idx], tcr->first_snap, idx);
			if (tcr->phi[idx] < -10.0)
				error(__FFL__,
						"Found uninitialized angle phi in tracer ID %ld (first snap %d, stencil idx %d).\n",
						tcr->phi[idx], tcr->first_snap, idx);
#endif
			cartesianCoordinatesUnit(tcr->theta[idx], tcr->phi[idx], &(vec_tcr[0]), &(vec_tcr[1]),
					&(vec_tcr[2]));
			alignment[i] = dotProduct(vec_ifl, vec_tcr);
		}

		alignment_available = 1;
	}

	/*
	 * For a pericenter, we impose the following conditions:
	 *
	 * - the tracer must be INFALLING rather than OUTGOING
	 * - the radial velocity must be positive in at least one time bin
	 *
	 * If we are currently outgoing, we look for an apocenter to reverse the direction.
	 */
	if ((tcr->oct_direction == OCT_DIRECTION_INFALLING) && (tcr->vr[i1] > 0.0))
	{
		/*
		 * At this point, a pericenter is likely, but one time bin with positive radial velocity
		 * could easily be due to noise. We ensure that the pericenter is proper with an algorithm
		 * that depends on  whether we have access to the full 3D position information.
		 *
		 * If we do, we check that the pericenter happens in a direction that is sufficiently far
		 * away from the direction of infall, as seen from the halo center. We require the dot
		 * product to be lower than the limit at snapshots i0 or i1; also including i2 does not
		 * improve the results: if the pericenter is real, vr will probably stay positive for
		 * another snapshot and the pericenter will be triggered then. If vr becomes negative
		 * again, the pericenter is likely just noise.
		 *
		 * If we do not have 3D information, we demand two consecutive time bins of positive
		 * radial velocity. This algorithm leads to an explicit dependence on time resolution,
		 * which is why the former is preferable.
		 */
		do_peri = 0;
		if (seeking_first_peri && alignment_available)
		{
			do_peri = ((alignment[0] <= MAX_ALIGNMENT_FIRSTPERI)
					|| (alignment[1] <= MAX_ALIGNMENT_FIRSTPERI));
		} else
		{
			do_peri = (tcr->vr[i2] > 0.0);
		}

		/*
		 * We have found a pericenter, update the orbit count result. Note that the snapshot
		 * before which the pericenter occurred is one snapshot back in time!
		 */
		if (do_peri)
		{
			ensureOctExists(snap_idx, &res_oct, tcr, tt);

			res_oct->n_pericenter++;
			res_oct->last_pericenter_snap = s1;

			/*
			 * We switch the direction only if both velocities in i1 and i2 are positive, since we are
			 * otherwise already infalling again. Note that we should not reset the direction if we
			 * did not find a pericenter: especially early on in the orbit, it is possible that
			 * there is a long period of positive velocities. If we set the direction to outgoing,
			 * we prevent future pericenters from being counted, even if they are valid (e.g.,
			 * because the dot product goes down).
			 */
			if (tcr->vr[i2] > 0.0)
				tcr->oct_direction = OCT_DIRECTION_OUTGOING;

			/*
			 * Debug now so we have the right pericenter count and direction, but before the next
			 * possible message about hitting the max. counter.
			 */
			if (alignment_available)
			{
				debugTracer(halo, tcr,
						"OCT, found pericenter %d. Updated event: last_peri_snap %d, oct_direction %d, vr %.2f, alignment %.2f/%.2f.",
						res_oct->n_pericenter, res_oct->last_pericenter_snap, tcr->oct_direction,
						tcr->vr[i1], alignment[0], alignment[1]);
			} else
			{
				debugTracer(halo, tcr,
						"OCT, found pericenter %d. Updated event: last_peri_snap %d, oct_direction %d, vr %.2f/%.2f.",
						res_oct->n_pericenter, res_oct->last_pericenter_snap, tcr->oct_direction,
						tcr->vr[i1], tcr->vr[i2]);
			}

			/*
			 * Stop if we have reached the maximum number of orbits.
			 */
			if (res_oct->n_pericenter == config.res_oct_max_norbit)
			{
				tcr->status_rs[OCT] = OCT_STATUS_SUCCESS;
				res_oct->is_final = 1;
				debugTracer(halo, tcr, "OCT, reached max orbits, setting to status success.");
			} else
			{
				res_oct->is_final = 0;
			}

#if CAREFUL
			if (res_oct->n_pericenter > config.res_oct_max_norbit)
				error(__FFL__,
						"OCT analysis: Found n_pericenter = %d, greater than res_oct_max_norbit (tcr ID %ld).\n",
						res_oct->n_pericenter, tcr->id);
#endif

			/*
			 * There are still other checks later, so we need to update the seeking_first_peri
			 * switch.
			 */
			seeking_first_peri = 0;
		} else
		{
			/*
			 * Also write output about why we did not accept this pericenter.
			 */
			if (alignment_available)
			{
				debugTracer(halo, tcr,
						"OCT, potential peri not valid, vr %.2f, alignment %.2f/%.2f.", tcr->vr[i1],
						alignment[0], alignment[1]);
			} else
			{
				debugTracer(halo, tcr, "OCT, potential peri not valid, vr %.2f/%.2f.", tcr->vr[i1],
						tcr->vr[i2]);
			}
		}
	} else if ((tcr->oct_direction == OCT_DIRECTION_OUTGOING) && (tcr->vr[i2] < 0.0))
	{
		tcr->oct_direction = OCT_DIRECTION_INFALLING;
		debugTracer(halo, tcr, "OCT, switched direction to infalling.");
	} else if (tcr->oct_direction == -1)
	{
		error(__FFL__, "OCT, found tracer %ld with invalid direction -1.\n", tcr->id);
	}

	/*
	 * A check unrelated to radial velocity: if we have not found a pericenter or lower limit yet
	 * but the dot product with infall is close to -1, that means that the tracer may be on a
	 * circular orbit. Otherwise, it is not clear how the tracer could be on the other side of the
	 * halo but not had a pericenter. Thus, we set it to lower limit.
	 */
	if (seeking_first_peri && alignment_available)
	{
		if (alignment[0] < MAX_ALIGNMENT_ZEROPERI)
		{
			ensureOctExists(snap_idx, &res_oct, tcr, tt);
			res_oct->n_is_lower_limit = 1;
			res_oct->lower_limit_snap = s2;
			seeking_first_peri = 0;

			debugTracer(halo, tcr,
					"OCT, found no pericenters but strong mis-alignment with infall (%.2f), setting to lower limit.",
					alignment[0]);
		}
	}

	/*
	 * Algorithm to filter for orbits that are unresolved in time. If we have found three up-
	 * crossings in velocity but no pericenters, and if these upcrossings happened within R200m
	 * and more than half a dynamical time after infall, we have reason to suspect that this orbit
	 * has, in fact, undergone pericenters, but that they are unresolved. We do not attempt to
	 * guess how many pericenters, but instead set the lower limit flag. As a result, this tracer
	 * will be included in profiles from here on.
	 */
	if (seeking_first_peri)
	{
		if ((tcr->r[i1] < haloR200m(halo, s1)) && (tcr->r[i2] < haloR200m(halo, s2))
				&& (tcr->vr[i1] < 0.0) && (tcr->vr[i2] > 0.0))

		{
			tdyn_since_infall = (config.snap_t[s2] - config.snap_t[res_oct->infall_snap])
					/ config.snap_t_dyn[res_oct->infall_snap];
			if (tdyn_since_infall > 0.5)
			{
				res_oct->n_crossings++;
				if (res_oct->n_crossings >= MAX_VELOCITY_SWITCHES)
				{
					res_oct->n_is_lower_limit = 1;
					res_oct->lower_limit_snap = s2;

					debugTracer(halo, tcr,
							"OCT, found potential pericenter #%d (%.2f dynamical times after infall); counter set to lower limit.",
							res_oct->n_crossings, tdyn_since_infall);
				} else
				{
					debugTracer(halo, tcr,
							"OCT, found potential pericenter #%d (%.2f dynamical times after infall).",
							res_oct->n_crossings, tdyn_since_infall);
				}
			}
		}
	}
}

/*
 * This function checks whether we should recreate a tracer in order to continue the orbit counting.
 * The answer is always yes, unless there is a result with a final count.
 */
int checkRecreateIgnoredTracerOrbitCount(Halo *halo, int snap_idx, ID id, int tt)
{
	int ret = 0;
	void *ptr;
	ResultOrbitCount *res_oct = NULL;

	ret = 1;
	ptr = bsearch(&id, halo->tt[tt].rs[OCT].data, halo->tt[tt].rs[OCT].n, rsprops[OCT].size,
			&compareResults);
	if (ptr != NULL)
	{
		res_oct = (ResultOrbitCount*) ptr;
		ret = !orbitCountIsFinal(res_oct);
	}

	if (ret)
		debugTracerByID(id, "Halo %ld, set tracer to recreate status %d.", haloOriginalID(halo),
				ret);

	return ret;
}

void outputConfigResultOrbitCount(hid_t grp)
{
	hdf5WriteAttributeInt(grp, "max_norbit", config.res_oct_max_norbit);
}

int outputFieldsResultOrbitCount(OutputField *outfields, int max_n_fields)
{
	int counter;

	counter = 0;

	outfields[counter].dtype = DTYPE_INT64;
	outfields[counter].offset = offsetof(ResultOrbitCount, tracer_id);
	sprintf(outfields[counter].name, "tracer_id");
	counter++;

#if OUTPUT_RESULT_ORBITCOUNT_OCT
	outfields[counter].dtype = DTYPE_INT16;
	outfields[counter].offset = offsetof(ResultOrbitCount, n_pericenter);
	sprintf(outfields[counter].name, "n_pericenter");
	counter++;
#endif

#if OUTPUT_RESULT_ORBITCOUNT_LOWER_LIMIT
	outfields[counter].dtype = DTYPE_INT8;
	outfields[counter].offset = offsetof(ResultOrbitCount, n_is_lower_limit);
	sprintf(outfields[counter].name, "n_is_lower_limit");
	counter++;
#endif

#if OUTPUT_RESULT_ORBITCOUNT_LAST_SNAP
	outfields[counter].dtype = DTYPE_INT16;
	outfields[counter].offset = offsetof(ResultOrbitCount, last_pericenter_snap);
	sprintf(outfields[counter].name, "last_pericenter_snap");
	counter++;
#endif

	return counter;
}

#endif
