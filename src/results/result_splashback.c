/*************************************************************************************************
 *
 * This unit implements splashback results, i.e. records the times, radii, and enclosed masses
 * where tracers reach the apocenter of their first orbit.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "result_splashback.h"
#include "../config.h"
#include "../memory.h"
#include "../tracers/tracers.h"
#include "../halos/halo.h"
#include "../halos/halo_particles.h"

#if DO_RESULT_SPLASHBACK

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * If all maximum solutions have merit below this threshold, use the trivial solution of taking
 * the time where the zero crossing in velocity occurs and interpolating the radius.
 */
#define MERIT_THRESH 0.2

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int analyzeMin(float *r, float *v, float *t, float *t_mm, float *r_mm, Tracer *tcr);
int analyzeMax(float *r, float *v, float *t, float *t_mm, float *r_mm, Tracer *tcr, float t_dyn);
int analyzeSplashback(Tracer *tcr, float *t, float *R, ResultSplashback *p_res, float t_dyn,
		int first_analysis);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initResultSplashback(ResultSplashback *res_sbk)
{
	res_sbk->tracer_id = INVALID_ID;
	res_sbk->tsp = INVALID_T;
	res_sbk->rsp = INVALID_R;
#if DO_RESULT_SPLASHBACK_MSP
	res_sbk->msp = INVALID_M;
#endif
	res_sbk->tracer_id = INVALID_ID;
#if DO_RESULT_SPLASHBACK_RRM
	res_sbk->rrm = 0.0;
#endif
#if DO_RESULT_SPLASHBACK_POS
	res_sbk->theta = INVALID_ANGLE;
	res_sbk->phi = INVALID_ANGLE;
#endif
}

int compareResultSplashbackTime(const void *a, const void *b)
{
	if (((ResultSplashback*) a)->tsp > ((ResultSplashback*) b)->tsp)
		return 1;
	else if (((ResultSplashback*) a)->tsp < ((ResultSplashback*) b)->tsp)
		return -1;
	else
		return 0;
}

/*
 * Analyze the trajectories of particles that aren't set to be ignored, and that have been tracked
 * for a sufficient number of snapshots.
 */
void runResultSplashback(int snap_idx, Halo *halo, Tracer *tcr, TracerType *tt, int n_rs_previous,
		void *rs_vp)
{
	int i, ret, s_idx, do_analysis;
	float halo_R_phys[STCL_TCR], snap_times[STCL_TCR];
	ResultSplashback *res_sbk, *new_res;
	ResultStatistics *rs;

	/*
	 * The splashback result cannot deal with tracers in subhalos, and this should be prevented by
	 * the result status system. If we get a subhalo at this point, something went wrong.
	 */
#if CAREFUL
	if (isSubPermanently(halo))
		error(__FFL__,
				"Trying to run splashback result on subhalo tracer ID %ld (halo ID %ld, status %d). This should never happen.\n",
				tcr->id, halo->cd.id, halo->status);
#if DO_GHOSTS
	if (isGhost(halo))
		error(__FFL__,
				"Trying to run splashback result on tracer ID %ld in ghost halo ID %ld. This should never happen.\n",
				tcr->id, halo->cd.id);
#endif
#endif

	rs = (ResultStatistics*) rs_vp;
	res_sbk = (ResultSplashback*) memAlloc(__FFL__, MID_RESSBK, sizeof(ResultSplashback));

	/*
	 * We need to determine whether we can perform the splashback analysis on this tracer. That is
	 * more complicated than it seems, because some tracers (subhalos) can have trajectory data
	 * going further back than their first_snap.
	 */
	do_analysis = (snap_idx - tcr->first_snap >= STCL_TCR - 1);
	do_analysis = (do_analysis
			|| ((tcr->r[0] > 0.0) && (tcr->r[1] > 0.0) && (tcr->r[2] > 0.0) && (tcr->r[3] > 0.0)));
	if (do_analysis)
	{
		/*
		 * Preset the times of the stencil around this snap.
		 */
		for (i = 0; i < STCL_TCR; i++)
		{
			s_idx = snap_idx - STCL_TCR + i + 1;
			if (s_idx >= 0)
			{
				halo_R_phys[i] = haloR200m(halo, s_idx);
				snap_times[i] = config.snap_t[s_idx];
			} else
			{
				halo_R_phys[i] = INVALID_R;
				snap_times[i] = (float) INVALID_T;
			}
		}

		debugTracer(halo, tcr,
				"SBK, analyzing, rs_status %d, t [%.2f %.2f %.2f %.2f] r [%.1e %.1e %.1e %.1e] v [%.1e %.1e %.1e %.1e]",
				tcr->status_rs[SBK], snap_times[0], snap_times[1], snap_times[2], snap_times[3],
				tcr->r[0], tcr->r[1], tcr->r[2], tcr->r[3], tcr->vr[0], tcr->vr[1], tcr->vr[2],
				tcr->vr[3]);
		ret = analyzeSplashback(tcr, snap_times, halo_R_phys, res_sbk, config.snap_t_dyn[snap_idx],
				(tcr->status_rs[SBK] == SBK_STATUS_SEEKING));
		rs->n[RSSTAT_ANALYZED]++;

		if (ret == SBK_STATUS_SUCCESS)
		{
			/*
			 * Find the splashback mass corresponding to the splashback radius. This cannot
			 * be done in the trajectory function, as it has no access to the mass profile.
			 */
			new_res = addResultSplashback(__FFL__, &(tt->rs[SBK]));
#if DO_RESULT_SPLASHBACK_MSP
			res_sbk->msp = enclosedMass(halo, snap_idx, res_sbk->rsp, res_sbk->tsp);
#endif
			res_sbk->tracer_id = tcr->id;
			memcpy(new_res, res_sbk, sizeof(ResultSplashback));
			tcr->status_rs[SBK] = SBK_STATUS_SUCCESS;
			rs->n[RSSTAT_NEW_RES]++;
			debugTracer(halo, tcr, "SBK, success, rs_status %d", tcr->status_rs[SBK]);
		} else if (ret == SBK_STATUS_FAILED)
		{
			tcr->status_rs[SBK] = SBK_STATUS_FAILED;
			rs->n[RSSTAT_FAILED]++;
			debugTracer(halo, tcr, "SBK, failure, rs_status %d", tcr->status_rs[SBK]);
		}
	} else
	{
		debugTracer(halo, tcr, "SBK, not analyzing because too young, rs_status %d",
				tcr->status_rs[SBK]);
	}

	memFree(__FFL__, MID_RESSBK, res_sbk, sizeof(ResultSplashback));
}

/*
 * Given STCL_TCR points of the r-v trajectory, try to find a minimum in r (crossing through the
 * halo) or a maximum (Rsp). This function can also set the status of the particle to IGNORE if
 * there is no reason to further track it.
 */
int analyzeSplashback(Tracer *tcr, float *t, float *R, ResultSplashback *p_res, float t_dyn,
		int first_analysis)
{
	int ret;
	float *r, *v;

	r = tcr->r;
	v = tcr->vr;
	ret = -1;

#if CAREFUL
	if (!isActive(tcr))
		error(__FFL__, "Particle status cannot be %d when analyzing trajectory.\n", tcr->status);
	int i;
	for (i = 0; i < STCL_TCR; i++)
	{
		if ((r[i] < 0.0) || (v[i] <= INVALID_V))
			error(__FFL__,
					"Splashback result found r[%d] = %.2e, v[%d] = %.2e in tracer ID %ld (first snap %d).\n",
					i, tcr->r[i], i, tcr->vr[i], tcr->id, tcr->first_snap);
	}
#endif

	/*
	 * Check for all up-crossings in v, regardless of whether they are valid (at least two
	 * bins on each side) or too narrow (just one value on each side). We still wish to count
	 * the latter (as long as they occur within the halo) as they might indicate unresolved
	 * orbits. If more than one crossing has occurred, we abort this trajectory.
	 *
	 * First, there is a special case when this trajetory is analyzed the first time: if there
	 * is an upcrossing in the first time bin, we need to check for it separately.
	 */
	if (first_analysis)
	{
		if ((v[0] < 0.0) && (v[1] > 0.0) && (r[0] < R[0]) && (r[1] < R[1]))
		{
			tcr->n_up_crossings++;
			debugTracer(NULL, tcr, "SBK, Detected up-crossing in first snap, number %d.",
					tcr->n_up_crossings);
		}
	}
	if ((v[1] < 0.0) && (v[2] > 0.0) && (r[1] < R[1]) && (r[2] < R[2]))
	{
		tcr->n_up_crossings++;
		debugTracer(NULL, tcr, "SBK, Detected up-crossing number %d.", tcr->n_up_crossings);

		if (tcr->r_min < 0.0)
		{
			if (tcr->n_up_crossings > 1)
			{
				debugTracer(NULL, tcr,
						"SBK, Failing due to too many up-crossings before min was found.");
				return SBK_STATUS_FAILED;
			}
		} else
		{
			if (tcr->n_up_crossings > 2)
			{
				debugTracer(NULL, tcr,
						"SBK, Failing due to too many up-crossings after min was found.");
				return SBK_STATUS_FAILED;
			}
		}
	}

	/*
	 * If no t_min has been found yet, look for that; otherwise look for a max.
	 */
	if (tcr->r_min < 0.0)
	{
		ret = SBK_STATUS_SEEKING_MIN;

		/*
		 * Check for an upward zero crossing in the middle of the stencil. If we find one, see if
		 * it's valid. Note that we perform this step even if the particle is outside the halo
		 * radius where we allow minima: the interpolation could place the particle at a smaller
		 * radius than any of the r[] elements.
		 */
		if (v[0] < 0.0 && v[1] < 0.0 && v[2] > 0.0 && v[3] > 0.0)
		{
			int im;
			float t_m, r_m, R_min;

			im = analyzeMin(r, v, t, &t_m, &r_m, tcr);
			debugTracer(NULL, tcr, "SBK, Analyzed minimum, im %d.", im);

			/*
			 * If im is not -1, we found a minimum. We analyze whether it is within the allowed
			 * region, and if so, set the status to look for a maximum.
			 *
			 * If the minimum is outside the allowed region, or if im is -1, we need to decide
			 * whether to continue this trajectory. If the unsuccessful minimum occurred within
			 * a certain fraction of R200m, we fail the trajectory since we might otherwise find
			 * a second minimum instead and fool ourselves into using that second splashback.
			 */
			if (im != -1)
			{
				R_min = R[im] + (R[im + 1] - R[im]) * (t_m - t[im]) / (t[im + 1] - t[im]);
				if (r_m <= R_min)
				{
					tcr->r_min = r_m;
					tcr->R_min = R_min;
					ret = SBK_STATUS_MIN_FOUND;
					debugTracer(NULL, tcr, "SBK, min found t %.3f r %.3f R %.3f", t_m, r_m, R_min);
				} else
				{
					im = -1;
				}
			}

			/*
			 * Now check again whether the minimum failed, either because it couldn't be found
			 * or because it wasn't within the zone. If it is smaller than some absolute limit,
			 * we should abort. Note that this limit does not depend on where the min_threshold
			 * is, but represents some absolute limit below which we consider a min to be a sign
			 * of first infall.
			 */
			if ((im == -1) && (r[1] <= R[1]))
				ret = SBK_STATUS_FAILED;
		}
	} else
	{
		ret = SBK_STATUS_SEEKING_MAX;

		/*
		 * First, check for a few cases that would cause us to abort. If none of those happen,
		 * check for an downward zero crossing in the middle of the stencil.
		 */
		if (v[0] > 0.0 && v[1] > 0.0 && v[2] < 0.0 && v[3] > 0.0)
		{
			debugTracer(NULL, tcr, "SBK, Failing because v is only negative for one snap.");
			ret = SBK_STATUS_FAILED;
		} else if (v[0] > 0.0 && v[1] > 0.0 && v[2] < 0.0 && v[3] < 0.0)
		{
			int im;
			float t_m, r_m;

			im = analyzeMax(r, v, t, &t_m, &r_m, tcr, t_dyn);
			debugTracer(NULL, tcr, "SBK, analyzed maximum, im %d", im);

			if (im != -1)
			{
				debugTracer(NULL, tcr, "SBK, t_m %.3f r_m %.3f r_min %.3f R_min %.3f", t_m, r_m,
						tcr->r_min, tcr->R_min);

				/*
				 * We need to save not only the Rsp result radius, time, and mass, but also
				 * the rR of closest approach as this criterion may be used to exclude this
				 * point later.
				 */
				p_res->tsp = t_m;
				p_res->rsp = r_m;
#if DO_RESULT_SPLASHBACK_RRM
				p_res->rrm = tcr->r_min / tcr->R_min;
#endif
#if DO_RESULT_SPLASHBACK_POS
				int i1, i2;
				if (t_m < t[im])
				i1 = im - 1;
				else
				i1 = im;
				i2 = i1 + 1;
				interpolateSphericalCoordinates(r[i1], tcr->theta[i1], tcr->phi[i1], t[i1], r[i2],
						tcr->theta[i2], tcr->phi[i2], t[i2], t_m, NULL, &(p_res->theta),
						&(p_res->phi));
#endif
				ret = SBK_STATUS_SUCCESS;
				debugTracer(NULL, tcr, "SBK, max found t %.2f r %.2f", p_res->tsp, p_res->rsp);
			} else
			{
				ret = SBK_STATUS_FAILED;
			}
		} else if (r[3] < tcr->r_min)
		{
			debugTracer(NULL, tcr,
					"SBK, Failing because radius %.3e smaller than previous min %.3e.", r[3],
					tcr->r_min);
			ret = SBK_STATUS_FAILED;
		}
	}

	return ret;
}

/*
 * We have found an upwards zero crossing of the velocity at the center of the stencil. Find the
 * exact time and radius of this minimum, if possible.
 */
int analyzeMin(float *r, float *v, float *t, float *t_mm, float *r_mm, Tracer *tcr)
{
	int i, im;
	float t_m, r_m, t_m_interp;

	t_m = 0.0;
	r_m = 0.0;
	im = -1;

	/*
	 * Check whether the radii before and after the crossing indicate a min.
	 */
	for (i = 1; i <= 2; i++)
	{
		if ((r[i] <= r[i - 1]) && (r[i] <= r[i + 1]))
		{
			im = i;
			if (r[0] < r[im])
				im = 0;
			break;
		}
	}
	if ((im == -1) && (r[0] < r[1]) && (r[0] < r[2]))
		im = 0;

	/*
	 * If we found a min in the radii, use our knowledge about the velocity to interpolate.
	 * If this interpolation does not give a reasonable result, we just pick the minimum point
	 * itself.
	 */
	if (im != -1)
	{
		t_m = t[im];
		r_m = r[im];

		if ((im == 1) || (im == 2))
		{
			// Equation 9 (sign reversed)
			t_m_interp = (r[2] - r[1] + t[1] * v[1] - t[2] * v[2]) / (v[1] - v[2]);
			debugTracer(NULL, tcr, "SBK, analyze min, im %d t_m_interp %.3f t1 %.3f t2 %.3f", im,
					t_m_interp, t[1], t[2]);

			if ((t_m_interp > t[1]) && (t_m_interp < t[2]))
			{
				t_m = t_m_interp;
				r_m = r[1] + v[1] * (t_m_interp - t[1]);
				r_m = fmaxf(r_m, 0.0);
			}
		}
	}

	*t_mm = t_m;
	*r_mm = r_m;

	return im;
}

/*
 * We have found a downwards zero crossing of the velocity at the center of the stencil. Find the
 * exact time and radius of this maximum, if possible.
 */
int analyzeMax(float *r, float *v, float *t, float *t_mm, float *r_mm, Tracer *tcr, float t_dyn)
{
	int im1, im2, im;
	float t_m, r_m, t_m_interp, fr1, fr2, ft1, ft2, t_cross, merit1, merit2, t_m1, r_m1, t_m2, r_m2;

	t_m = 0.0;
	t_m1 = 0.0;
	t_m2 = 0.0;
	r_m = 0.0;
	r_m1 = 0.0;
	r_m2 = 0.0;
	im = -1;
	im1 = -1;
	im2 = -1;
	fr1 = 0.0;
	fr2 = 0.0;
	ft1 = 0.0;
	ft2 = 0.0;

	/*
	 * Check whether the radii before and after the crossing indicate a max.
	 */
	if ((r[1] >= r[0]) && (r[1] >= r[2]))
	{
		im1 = 1;
		fr1 = r[1] / (r[2] + r[0]) * 2.0 - 1.0;
	} else if ((r[2] >= r[1]) && (r[2] >= r[3]))
	{
		im1 = 2;
		fr1 = r[2] / (r[1] + r[3]) * 2.0 - 1.0;
	}

	/*
	 * If we found a max in the radii, use our knowledge about the velocity to interpolate. If this
	 * interpolation does not give a reasonable result (interpolated time lies between the bins
	 * next to the crossing in v), we just pick the maximum point itself.
	 *
	 * Note that the division by v1 - v2 is OK since those numbers cannot be the same.
	 */
	if (im1 != -1)
	{
		// Equation 11
		t_m_interp = (r[2] - r[1] + t[1] * v[1] - t[2] * v[2]) / (v[1] - v[2]);
		if ((t_m_interp > t[1]) && (t_m_interp < t[2]))
		{
			t_m = t_m_interp;
			// Equation 12
			r_m = r[1] + v[1] * (t_m_interp - t[1]);
			*t_mm = t_m;
			*r_mm = r_m;

			return im1;
		} else
		{
			t_m1 = t[im1];
			r_m1 = r[im1];
		}
	}

	/*
	 * Now check for another solution; are the 0 and 3 points of the stencil higher than the bins
	 * next to the crossing in v?
	 */
	im2 = -1;
	if ((r[0] > r[1]) && (r[0] > r[2]) && (r[0] > r[3]))
		im2 = 0;
	else if ((r[3] > r[0]) && (r[3] > r[1]) && (r[3] > r[2]))
		im2 = 3;

	/*
	 * If neither solution worked, give up (this should never happen though).
	 */
	if ((im1 == -1) && (im2 == -1))
		return -1;

	/*
	 * Now we need to weigh the two solutions we might have found, as well as a third solution of
	 * just taking the crossing time for face value. We compute a merit for solutions 1 and 2, and
	 * accept the one of higher merit. If neither passes a certain minimum merit criterion, we use
	 * solution 3.
	 *
	 * Equation 13:
	 */
	t_cross = t[1] - v[1] * (t[2] - t[1]) / (v[2] - v[1]);

	if (im1 == -1)
	{
		merit1 = 0.0;
	} else
	{
		ft1 = fmax(fabs(t_m1 - t_cross), t_dyn * 0.01) / t_dyn;
		merit1 = fr1 / ft1;
	}

	if (im2 == -1)
	{
		merit2 = 0.0;
	} else
	{
		t_m2 = t[im2];
		r_m2 = r[im2];
		fr2 = r[im2] / fmax(r[1], r[2]) - 1.0;
		ft2 = fmax(fabs(t_m2 - t_cross), t_dyn * 0.01) / t_dyn;
		merit2 = fr2 / ft2;
	}

	debugTracer(NULL, tcr,
			"SBK, analyze max, im1 %d fr1 %.3f ft1 %.3f merit1 %.3f im2 %d fr2 %.3f ft2 %.3f merit2 %.3f tcross %.3f",
			im1, fr1, ft1, merit1, im2, fr2, ft2, merit2, t_cross);

	if ((merit1 < MERIT_THRESH) && (merit2 < MERIT_THRESH))
	{
		im = 1;
		t_m = t_cross;
		// Equation 15
		r_m = r[1] + (t_m - t[1]) / (t[2] - t[1]) * (r[2] - r[1]);
	} else
	{
		if (merit1 >= merit2)
		{
			im = im1;
			t_m = t_m1;
			r_m = r_m1;
		} else
		{
			im = im2;
			t_m = t_m2;
			r_m = r_m2;
		}
	}

	*t_mm = t_m;
	*r_mm = r_m;

	return im;
}

int outputFieldsResultSplashback(OutputField *outfields, int max_n_fields)
{
	int counter;

	counter = 0;

	outfields[counter].dtype = DTYPE_INT64;
	outfields[counter].offset = offsetof(ResultSplashback, tracer_id);
	sprintf(outfields[counter].name, "tracer_id");
	counter++;

	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultSplashback, tsp);
	sprintf(outfields[counter].name, "tsp");
	counter++;

	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultSplashback, rsp);
	sprintf(outfields[counter].name, "rsp");
	counter++;

#if OUTPUT_RESULT_SPLASHBACK_MSP
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultSplashback, msp);
	sprintf(outfields[counter].name, "msp");
	counter++;
#endif

#if OUTPUT_RESULT_SPLASHBACK_RRM
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultSplashback, rrm);
	sprintf(outfields[counter].name, "rrm");
	counter++;
#endif

#if OUTPUT_RESULT_SPLASHBACK_POS
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultSplashback, theta);
	sprintf(outfields[counter].name, "theta");
	counter++;

	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultSplashback, phi);
	sprintf(outfields[counter].name, "phi");
	counter++;
#endif

	return counter;
}

#endif
