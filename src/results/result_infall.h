/*************************************************************************************************
 *
 * This unit implements infall results which are recorded when a tracer first falls into a halo.
 * The properties of an infall event include its time, radial and tangential velocity, and
 * possibly the mass ratio of the subhalo at infall.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _RESULT_INFALL_H_
#define _RESULT_INFALL_H_

#include "../statistics.h"
#include "../io/io_hdf5.h"

#if DO_RESULT_INFALL

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initResultInfall(ResultInfall *res_inf);
void runResultInfall(int snap_idx, Halo *halo, Tracer *tcr, TracerType *tt, int n_rs_previous,
		void *rs_vp);
int outputFieldsResultInfall(OutputField *outfields, int max_n_fields);

#endif

#endif
