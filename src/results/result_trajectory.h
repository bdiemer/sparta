/*************************************************************************************************
 *
 * This unit implements trajectory results, i.e. records of the full trajectory of a tracer. Due
 * to the large amount of data recorded, this result is typically not saved for a large number
 * of tracers.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _RESULT_TRAJECTORY_H_
#define _RESULT_TRAJECTORY_H_

#include "../statistics.h"
#include "../io/io_hdf5.h"

#if DO_RESULT_TRAJECTORY

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initResultTrajectory(ResultTrajectory *res_inf);
void runResultTrajectory(int snap_idx, Halo *halo, Tracer *tcr, TracerType *tt, int n_rs_previous,
		void *rs_vp);
int outputFieldsResultTrajectory(OutputField *outfields, int max_n_fields);

#endif

#endif
