/*************************************************************************************************
 *
 * This unit implements functions that are common to all result types.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _RESULTS_H_
#define _RESULTS_H_

#include "../global_types.h"
#include "../statistics.h"

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * The first fields of all individual result statuses must match these fundamental statuses that
 * are common to all results. Thus, the order matters!!!
 *
 * RS_STATUS_NONE         An invalid status; this value is used to indicate non-sensical situations
 *                        such as subhalo tracers in subhalos, and can be used to catch errors.
 * RS_STATUS_SUCCESS      The result has succeeded. Typically a unique result output was saved,
 *                        such as an infall or splashback event.
 * RS_STATUS_FAILED       The result failed and should not be reattempted.
 * RS_STATUS_NOT_SEEKING  Result is not active
 * RS_STATUS_MAINTAIN     A special status that is neither done or not done. This can be set
 *                        in response to the INSTR_SUBTRACK_CONTD instruction and means that
 *                        as the halo becomes a subhalo its result status is maintained. Most
 *                        results will change their status due to the halo becoming a subhalo.
 *                        It can also be set in response to the INSTR_TCR_RECREATED instruction,
 *                        where it means that the standard (not re-created) status should be used.
 * RS_STATUS_SEEKING      This result is active. This status is at the end because individual
 *                        results may add more status fields that indicate various states while
 *                        active.
 */
enum
{
	RS_STATUS_NONE,
	RS_STATUS_SUCCESS,
	RS_STATUS_FAILED,
	RS_STATUS_NOT_SEEKING,
	RS_STATUS_MAINTAIN,
	RS_STATUS_SEEKING
};

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initConfigResults(int step);
void runResults(Halo *halo, int snap_idx, LocalStatistics *ls);
int checkRecreateIgnoredTracer(Halo *halo, int snap_idx, ID id, int tt);

int resultShouldBeRun(int rs_status);
int resultIsDone(int rs_status);
int resultIsNotDone(int rs_status);

#endif
