/*************************************************************************************************
 *
 * This unit implements trajectory results, i.e. records of the full trajectory of a tracer. Due
 * to the large amount of data recorded, this result is typically not saved for a large number
 * of tracers.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "result_trajectory.h"
#include "../config.h"
#include "../memory.h"
#include "../utils.h"
#include "../tracers/tracers.h"

#if DO_RESULT_TRAJECTORY

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initResultTrajectory(ResultTrajectory *res_tra)
{
	int i, j;

	res_tra->tracer_id = INVALID_ID;

	for (i = 0; i < MAX_SNAPS; i++)
	{
#if DO_RESULT_TRAJECTORY_R
		res_tra->r[i] = INVALID_R;
#endif
#if DO_RESULT_TRAJECTORY_VR
		res_tra->vr[i] = INVALID_V;
#endif
#if DO_RESULT_TRAJECTORY_VT
		res_tra->vt[i] = INVALID_V;
#endif

		for (j = 0; j < 3; j++)
		{
#if DO_RESULT_TRAJECTORY_X
			res_tra->x[i][j] = INVALID_R;
#endif
#if DO_RESULT_TRAJECTORY_V
			res_tra->v[i][j] = INVALID_V;
#endif
		}
	}
}

/*
 * If this tracer is new, we create a trajectory result and add all existing trajectory data.
 * Otherwise, we add to an existing trajectory result. Either way, we need to search the trajectory
 * results.
 *
 * When adding new tracers we try go go back in time, if the tracer has historical trajectory
 * information. This is particularly useful for subhalo tracers which are only born when they enter
 * the halo. However, we do not know how much historical information we have, and the historical
 * information may not cover all result fields (e.g. the full x and v information). Thus,
 * we only set the first_snap field to the earlier time if all information is available.
 *
 * In this loop, i indicates how many snapshots back we are going. The math is a little
 * tricky: while we can go STCL_TCR - 1 steps back, how many of those fields are filled
 * will depend on the tracer. For new subhalo tracer, STCL_HALO fields back are filled
 * (plus the current snapshot). For particle tracers, they may go back further.
 */
void runResultTrajectory(int snap_idx, Halo *halo, Tracer *tcr, TracerType *tt, int n_rs_previous,
		void *rs_vp)
{
	ResultTrajectory *temp_res, *res;

	temp_res = (ResultTrajectory *) memAlloc(__FFL__, MID_RESTJY, sizeof(ResultTrajectory));
	temp_res->tracer_id = tcr->id;
	res = (ResultTrajectory *) bsearch(temp_res, tt->rs[TJY].rs_tjy, n_rs_previous,
			sizeof(ResultTrajectory), &compareResults);
	memFree(__FFL__, MID_RESTJY, temp_res, sizeof(ResultTrajectory));

	if (res == NULL)
	{
		int i, idx_res, idx_tcr;

		res = addResultTrajectory(__FFL__, &(tt->rs[TJY]));
		res->tracer_id = tcr->id;
		res->first_snap = snap_idx;

		i = 1;
		while ((i < STCL_TCR) && (tcr->r[STCL_TCR - i - 1] > 0.0) && (snap_idx - i >= 0))
		{
			idx_res = snap_idx - i;
			idx_tcr = STCL_TCR - i - 1;

#if DO_RESULT_TRAJECTORY_R
			res->r[idx_res] = tcr->r[idx_tcr];
#endif
#if DO_RESULT_TRAJECTORY_VR
			res->vr[idx_res] = tcr->vr[idx_tcr];
#endif
#if DO_RESULT_TRAJECTORY_VT
			res->vt[idx_res] = tcr->vt[idx_tcr];
#endif
			i++;
		}
		i--;

#if (!DO_RESULT_TRAJECTORY_X && !DO_RESULT_TRAJECTORY_V)
		res->first_snap = snap_idx - i;
#endif

		debugTracer(halo, tcr, "TJY, adding trajectory result, reconstructed %d previous snaps.",
				i);
	} else
	{
		debugTracer(halo, tcr, "TJY, updating trajectory result.");
	}

#if DO_RESULT_TRAJECTORY_R
	res->r[snap_idx] = tcr->r[STCL_TCR - 1];
#endif
#if DO_RESULT_TRAJECTORY_VR
	res->vr[snap_idx] = tcr->vr[STCL_TCR - 1];
#endif
#if DO_RESULT_TRAJECTORY_VT
	res->vt[snap_idx] = tcr->vt[STCL_TCR - 1];
#endif
#if DO_RESULT_TRAJECTORY_X
	int dx;
	for (dx = 0; dx < 3; dx++)
		res->x[snap_idx][dx] = tcr->x[dx];
#endif
#if DO_RESULT_TRAJECTORY_V
	int dv;
	for (dv = 0; dv < 3; dv++)
		res->v[snap_idx][dv] = tcr->v[dv];
#endif

	res->last_snap = snap_idx;
}

int outputFieldsResultTrajectory(OutputField *outfields, int max_n_fields)
{
	int counter;

	counter = 0;

	outfields[counter].dtype = DTYPE_INT64;
	outfields[counter].offset = offsetof(ResultTrajectory, tracer_id);
	sprintf(outfields[counter].name, "tracer_id");
	counter++;

	outfields[counter].dtype = DTYPE_INT16;
	outfields[counter].offset = offsetof(ResultTrajectory, first_snap);
	sprintf(outfields[counter].name, "first_snap");
	counter++;

	outfields[counter].dtype = DTYPE_INT16;
	outfields[counter].offset = offsetof(ResultTrajectory, last_snap);
	sprintf(outfields[counter].name, "last_snap");
	counter++;

#if OUTPUT_RESULT_TRAJECTORY_R
	outfields[counter].n_dims = 2;
	outfields[counter].dim_ext[1] = config.n_snaps;
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultTrajectory, r);
	sprintf(outfields[counter].name, "r");
	counter++;
#endif

#if OUTPUT_RESULT_TRAJECTORY_VR
	outfields[counter].n_dims = 2;
	outfields[counter].dim_ext[1] = config.n_snaps;
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultTrajectory, vr);
	sprintf(outfields[counter].name, "vr");
	counter++;
#endif

#if OUTPUT_RESULT_TRAJECTORY_VT
	outfields[counter].n_dims = 2;
	outfields[counter].dim_ext[1] = config.n_snaps;
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultTrajectory, vt);
	sprintf(outfields[counter].name, "vt");
	counter++;
#endif

#if OUTPUT_RESULT_TRAJECTORY_X
	outfields[counter].n_dims = 3;
	outfields[counter].dim_ext[1] = config.n_snaps;
	outfields[counter].dim_ext[2] = 3;
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultTrajectory, x);
	sprintf(outfields[counter].name, "x");
	counter++;
#endif

#if OUTPUT_RESULT_TRAJECTORY_V
	outfields[counter].n_dims = 3;
	outfields[counter].dim_ext[1] = config.n_snaps;
	outfields[counter].dim_ext[2] = 3;
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultTrajectory, v);
	sprintf(outfields[counter].name, "v");
	counter++;
#endif

	return counter;
}

#endif
