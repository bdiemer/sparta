/*************************************************************************************************
 *
 * This unit implements orbit counting.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#ifndef _RESULT_ORBITCOUNT_H_
#define _RESULT_ORBITCOUNT_H_

#include "../statistics.h"
#include "results.h"
#include "../io/io_hdf5.h"

#if DO_RESULT_ORBITCOUNT

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*
 * Default config parameters
 */
#define DEFAULT_RES_OCT_MAX_NORBIT 3

/*
 * Status fields match the standard result statuses, except for OCT_STATUS_BORN_IN_HALO which
 * indicates that the result is active but that the initial orbit state was ambiguous.
 */
#define OCT_STATUS_NONE RS_STATUS_NONE
#define OCT_STATUS_SUCCESS RS_STATUS_SUCCESS
#define OCT_STATUS_FAILED RS_STATUS_FAILED
#define OCT_STATUS_NOT_SEEKING RS_STATUS_NOT_SEEKING
#define OCT_STATUS_MAINTAIN RS_STATUS_MAINTAIN
#define OCT_STATUS_SEEKING RS_STATUS_SEEKING
#define OCT_STATUS_BORN_IN_HALO (OCT_STATUS_SEEKING + 1)

enum
{
	OCT_DIRECTION_INFALLING,
	OCT_DIRECTION_OUTGOING
};

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initConfigResultOrbitCount(int step);
void initResultOrbitCount(ResultOrbitCount *res_oct);
void runResultOrbitCount(int snap_idx, Halo *halo, Tracer *tcr, TracerType *tt, int n_rs_previous,
		void *rs_vp);
int checkRecreateIgnoredTracerOrbitCount(Halo *halo, int snap_idx, ID id, int tt);
int outputFieldsResultOrbitCount(OutputField *outfields, int max_n_fields);
void outputConfigResultOrbitCount(hid_t grp);

#endif

#endif
