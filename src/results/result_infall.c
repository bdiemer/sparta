/*************************************************************************************************
 *
 * This unit implements infall results which are recorded when a tracer first falls into a halo.
 * The properties of an infall event include its time, radial and tangential velocity, and
 * possibly the mass ratio of the subhalo at infall.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include "result_infall.h"
#include "results.h"
#include "../config.h"
#include "../tracers/tracers.h"
#include "../halos/halo.h"

#if DO_RESULT_INFALL

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

ResultInfall *createResultInfall(Tracer *tcr, TracerType *tt, int snap_idx, int born_in_halo);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void initResultInfall(ResultInfall *res_inf)
{
	res_inf->tracer_id = INVALID_ID;
	res_inf->born_in_halo = 0;
	res_inf->tifl = INVALID_T;
#if DO_RESULT_INFALL_SMR
	res_inf->smr = INVALID_SMR;
#endif
#if DO_RESULT_INFALL_VRV200
	res_inf->vrv200 = -1.0;
#endif
#if DO_RESULT_INFALL_VTV200
	res_inf->vtv200 = -1.0;
#endif
#if DO_RESULT_INFALL_X
	int j;
	for (j = 0; j < 3; j++)
		res_inf->x[j] = INVALID_X;
#endif
}

/*
 * Convenience function to create an infall result for a tracer. Most fields just copy tracer
 * fields, but there are some fields that will must be overwritten such as the time of infall
 * and any position information.
 *
 * If born_in_halo, we automatically set the position of the infall to the halo center at the
 * current snapshot, that is zero (as opposed to invalid).
 */
ResultInfall *createResultInfall(Tracer *tcr, TracerType *tt, int snap_idx, int born_in_halo)
{
	ResultInfall *new_res;

	new_res = addResultInfall(__FFL__, &(tt->rs[IFL]));
	new_res->tracer_id = tcr->id;
	new_res->born_in_halo = born_in_halo;

#if DO_RESULT_INFALL_SMR
	new_res->smr = tcr->smr;
#endif

	if (born_in_halo)
	{
		new_res->tifl = config.snap_t[snap_idx];
#if DO_RESULT_INFALL_X
		int j;
		for (j = 0; j < 3; j++)
			new_res->x[j] = 0.0;
#endif
	}

	return new_res;
}

/*
 * Analyze trajectories for infall
 */
void runResultInfall(int snap_idx, Halo *halo, Tracer *tcr, TracerType *tt, int n_rs_previous,
		void *rs_vp)
{
	int i, i0, i1, i2, s1, s2;
	float r1, r2, R1, R2, t1, t2, f;
	ResultInfall *new_res;
	ResultStatistics *rs;

	/*
	 * Increase counter and print if necessary
	 */
	rs = (ResultStatistics *) rs_vp;
	rs->n[RSSTAT_ANALYZED]++;

	debugTracer(halo, tcr, "IFL, analyzing, rs_status %d", tcr->status_rs[IFL]);

	/*
	 * Check whether this is a subhalo. Depending on whether subhalo tracking is on, that would
	 * mean different things:
	 *
	 * SH tracking off: There should be no tracers in subhalos, throw error.
	 * SH tracking on:  This tracer was determined to belong to a subhalo. This is possible because
	 *                  tracers could be in subhalos for reasons other than a previous infall
	 *                  event (e.g., being strongly bound). In this case, we want to save an infall
	 *                  event that reflects this situation.
	 */
	if (isSubPermanently(halo))
	{
#if DO_SUBHALO_TRACKING
		new_res = createResultInfall(tcr, tt, snap_idx, 1);
		tcr->status_rs[IFL] = RS_STATUS_SUCCESS;
		rs->n[RSSTAT_NEW_RES]++;

		debugTracer(halo, tcr, "IFL, success, rs_status %d, tracer part of subhalo.",
				tcr->status_rs[IFL]);
		return;
#else
		error(__FFL__,
				"Trying to run infall result on subhalo tracer ID %ld (halo ID %ld). Subhalo tracking is off, this should never happen.\n",
				tcr->id, halo->cd.id);
#endif
	}

	/*
	 * If this is the tracer's first snap, there is a possibility that the trajectory is not
	 * outside R200m at all. In that case, we create an infall event with a flag indicating that
	 * the actual infall was not tracked.
	 */
	i0 = STCL_TCR - 2;
	if (tcr->first_snap == snap_idx)
	{
		i0 = -1;
		for (i = 0; i < STCL_TCR; i++)
		{
			if (tcr->r[i] > 0.0)
			{
				i0 = i;
				break;
			}
		}

		if (i0 == -1)
			error(__FFL__, "Found no trajectory data in new tracer.");

		if ((i0 == STCL_TCR - 1) && (tcr->first_snap < snap_idx))
			error(__FFL__,
					"Last radius of tracer is invalid, though the tracer was alive at past snapshot.");

		r1 = tcr->r[i0];
		s1 = snap_idx - STCL_TCR + i0 + 1;
		R1 = haloR200m(halo, s1);

		/*
		 * There are two ways we can find out whether the tracer is within the halo. First, we
		 * compare radii. The second option seems strange: why should the status_entered flag be
		 * set if the tracer is outside the halo? This can happen because tracers are created
		 * before the final halo radius is computed. If the first guess (catalog) radius was larger,
		 * the tracer might have been inside that radius but is not inside of R1. In this case, we
		 * still create an infall result to avoid very nasty issues with having status_entered be
		 * set but no infall event. We could, of course, also change the flag, but the content of
		 * tracers should not be changed by results.
		 */
		if ((r1 < R1) || (tcr->status_entered != TCR_ENTERED_NO))
		{
			new_res = createResultInfall(tcr, tt, snap_idx, 1);
			tcr->status_rs[IFL] = RS_STATUS_SUCCESS;
			rs->n[RSSTAT_NEW_RES]++;

			debugTracer(halo, tcr,
					"IFL, success, rs_status %d, first snap of tracer already in host.",
					tcr->status_rs[IFL]);
			return;
		}
	}

	/*
	 * It's not the first snapshot, and the tracer is/was outside the halo. We look for an actual
	 * infall event.
	 */
	for (i1 = i0; i1 <= STCL_TCR - 2; i1++)
	{
		i2 = i1 + 1;
		s1 = snap_idx - STCL_TCR + i1 + 1;
		s2 = s1 + 1;

		r1 = tcr->r[i1];
		r2 = tcr->r[i2];
		R1 = haloR200m(halo, s1);
		R2 = haloR200m(halo, s2);

		/*
		 * Check if the tracer is still outside the halo. In this case, we abort and wait for the next
		 * snapshot.
		 */
		if (r2 > R2)
			continue;

		/*
		 * At this point, r1 should be >= R1 or something went wrong
		 */
#if CAREFUL
		if (r1 < R1)
		{
			error(__FFL__, "Found r1 < R1, unexpected, first_snap %d, tcr ID %ld, i0 %d, i1 %d\n",
					tcr->first_snap, tcr->id, i0, i1);
		}
#endif

		/*
		 * We have found an infall event, i.e. r1 > R1 and r2 < R2. Interpolate to
		 * find the time of infall.
		 */
		t1 = config.snap_t[s1];
		t2 = config.snap_t[s2];
		f = (r1 - R1) / (R2 - R1 + r1 - r2);

		new_res = createResultInfall(tcr, tt, snap_idx, 0);
		new_res->tifl = t1 + f * (t2 - t1);
#if (DO_RESULT_INFALL_VRV200 || DO_RESULT_INFALL_VTV200)
		float v200m, v200m_1, v200m_2;
		v200m_1 = R1 * config.snap_v200m_factor[s1];
		v200m_2 = R2 * config.snap_v200m_factor[s2];
		v200m = v200m_1 + f * (v200m_2 - v200m_1);
#endif
#if DO_RESULT_INFALL_VRV200
		new_res->vrv200 = (tcr->vr[i1] + f * (tcr->vr[i2] - tcr->vr[i1])) / v200m;
#endif
#if DO_RESULT_INFALL_VTV200
		new_res->vtv200 = (tcr->vt[i1] + f * (tcr->vt[i2] - tcr->vt[i1])) / v200m;
#endif
#if DO_RESULT_INFALL_X
		interpolateSphericalCoordinatesToX(r1, tcr->theta[i1], tcr->phi[i1], t1, r2, tcr->theta[i2],
				tcr->phi[i2], t2, new_res->tifl, &(new_res->x[0]), &(new_res->x[1]),
				&(new_res->x[2]));
#endif

		tcr->status_rs[IFL] = RS_STATUS_SUCCESS;
		rs->n[RSSTAT_NEW_RES]++;
		debugTracer(halo, tcr, "IFL, success, rs_status %d", tcr->status_rs[IFL]);
		break;
	}
}

int outputFieldsResultInfall(OutputField *outfields, int max_n_fields)
{
	int counter;

	counter = 0;

	outfields[counter].dtype = DTYPE_INT64;
	outfields[counter].offset = offsetof(ResultInfall, tracer_id);
	sprintf(outfields[counter].name, "tracer_id");
	counter++;

#if OUTPUT_RESULT_INFALL_BORNINHALO
	outfields[counter].dtype = DTYPE_INT8;
	outfields[counter].offset = offsetof(ResultInfall, born_in_halo);
	sprintf(outfields[counter].name, "born_in_halo");
	counter++;
#endif
#if OUTPUT_RESULT_INFALL_TIME
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultInfall, tifl);
	sprintf(outfields[counter].name, "t_infall");
	counter++;
#endif
#if OUTPUT_RESULT_INFALL_SMR
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultInfall, smr);
	sprintf(outfields[counter].name, "sub_mass_ratio");
	counter++;
#endif
#if OUTPUT_RESULT_INFALL_VRV200
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultInfall, vrv200);
	sprintf(outfields[counter].name, "vrv200");
	counter++;
#endif
#if OUTPUT_RESULT_INFALL_VTV200
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultInfall, vtv200);
	sprintf(outfields[counter].name, "vtv200");
	counter++;
#endif
#if OUTPUT_RESULT_INFALL_X
	outfields[counter].n_dims = 2;
	outfields[counter].dim_ext[1] = 3;
	outfields[counter].dtype = DTYPE_FLOAT;
	outfields[counter].offset = offsetof(ResultInfall, x);
	sprintf(outfields[counter].name, "x");
	counter++;
#endif

	return counter;
}

#endif
