/*************************************************************************************************
 *
 * Test suite for the Rsp model.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "tests_model_rsp.h"

#if TESTS_DO_MODEL_RSP

#include "../src/global.h"
#include "../src/halos/halo_definitions.h"
#include "../tools/models/models_rsp.h"

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Print a text file that contains columns with all the information needed to reproduce the
 * Rsp/Msp fitting function in other codes.
 */
void testModelRsp()
{
	int i, k, i1, i2, i3, i4;
	float Gamma, Om, nu, v;
	FILE *f;
	HaloDefinition def;
	char def_str[20];

	const float Gamma_all[3] =
		{0.01, 3.0, 11.9};
	const float nu_all[3] =
		{0.3, 1.0, 5.0};
	const float Omegam_all[2] =
		{0.3, 0.9};

	initHaloDefinition(&def);
	def.type = HDEF_TYPE_SPLASHBACK;
	f = fopen("test_rspmodel.txt", "w");

	for (k = 0; k < 2; k++)
	{
		if (k == 0)
		{
			def.is_error = HDEF_ERR_NO;
		} else
		{
			def.is_error = HDEF_ERR_1SIGMA;
		}

		for (i = 0; i < 3; i++)
		{
			if (i == 0)
			{
				def.subtype = HDEF_SUBTYPE_SP_APOCENTER_RADIUS_MEAN;
			} else if (i == 1)
			{
				def.subtype = HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE;
				def.percentile = 50.0;
			} else
			{
				def.subtype = HDEF_SUBTYPE_SP_APOCENTER_RADIUS_PERCENTILE;
				def.percentile = 90.0;
			}

			for (i1 = 0; i1 < 2; i1++)
			{
				if (i1 == 0)
					def.quantity = HDEF_Q_RADIUS;
				else
					def.quantity = HDEF_Q_MASS;

				for (i2 = 0; i2 < 3; i2++)
				{
					Gamma = Gamma_all[i2];
					for (i3 = 0; i3 < 2; i3++)
					{
						Om = Omegam_all[i3];

						for (i4 = 0; i4 < 3; i4++)
						{
							nu = nu_all[i4];

							if (k == 0)
							{
								v = modelDiemer20Splashback(&def, Gamma, nu, Om);
							} else
							{
								v = modelDiemer20Scatter(&def, Gamma, nu);
							}

							haloDefinitionToString(def, def_str);
							fprintf(f, "%-15s  %5.2f  %.2f  %.2f  %.8f\n", def_str, Gamma, nu, Om,
									v);
						}
					}
				}
			}
		}
	}
	fclose(f);
}

#endif
