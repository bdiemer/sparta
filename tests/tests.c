/*************************************************************************************************
 *
 * Test suite for SPARTA.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <tests.h>

#include "../src/global_types.h"
#include "tests_cosmology.h"
#include "tests_gde.h"
#include "tests_hdf5.h"
#include "tests_halodefinition.h"
#include "tests_hilbert.h"
#include "tests_model_rsp.h"
#include "tests_potential.h"

/*************************************************************************************************
 * MAIN
 *************************************************************************************************/

int main(int argc, char **argv)
{
#if TESTS_DO_COSMOLOGY
	testCosmology();
#endif

#if TESTS_DO_GDE
	testGDE();
#endif

#if TESTS_DO_HALODEFINITIONS
	testHaloDefinitions();
#endif

#if TESTS_DO_HDF5
	testHDF5();
#endif

#if TESTS_DO_HILBERT
	testHilbertCurve(TESTS_HILBERT_N);
#endif

#if TESTS_DO_MODEL_RSP
	testModelRsp();
#endif

#if TESTS_DO_POTENTIAL
	testPotential();
#endif

	return EXIT_SUCCESS;
}
