/*************************************************************************************************
 *
 * Combine SPARTA with Mark Vogelsberger's geodesic equation (GDE) output from Gadget
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tests_gde.h"
#include "../src/global.h"
#include "../src/memory.h"
#include "../src/config.h"
#include "../src/io/io_snaps_lgadget.h"
#include "../src/io/io_snaps.h"

#if TESTS_DO_GDE

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

const char *TEST_FILE =
		"/Users/benedito/University/data/Box_L0063_N0256_CBol_gde/Snaps/snapshot_053.0";

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void readParticlesLGadget_GDE(char *filename, Particle **particles, int *n_particles,
		int check_corrupt);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void testGDE()
{
	SnapshotHeaderLGadget *header;
	Particle *particles;
	int n_particles;

	header = readSnapshotHeaderLGadgetInternal((char *) TEST_FILE);
	printLGadgetHeader(header);

	config.box_size = header->BoxSize;

	readParticlesLGadget_GDE((char *) TEST_FILE, &particles, &n_particles, 0);
}

void readParticlesLGadget_GDE(char *filename, Particle **particles, int *n_particles,
		int check_corrupt)
{
	FILE *f;
	SnapshotHeaderLGadget header;
	int i, dummy, *buffer_int, ret;
	long *buffer_long;
	float *buffer_float, root_a;

	f = fopen(filename, "r");
	if (f == NULL)
		error(__FFL__, "Could not open file %s.\n", filename);

	/*
	 * Read header, compute number of particles and velocity conversion factor.
	 */
	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	fread(&header, sizeof(SnapshotHeaderLGadget), (size_t) 1, f);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	root_a = sqrt(header.time);
	*n_particles = 0;
	for (i = 0; i < 5; i++)
		*n_particles += header.npart[i];

	/*
	 * Allocate memory and read the particle positions. When transcribing the positions,
	 * make sure to take the periodic bcs into account.
	 */
	*particles = (Particle*) memAlloc(__FFL__, MID_PARTICLES, sizeof(Particle) * (*n_particles));
	buffer_float = (float*) memAlloc(__FFL__, MID_SNAPREADBUF,
			sizeof(float) * (*n_particles) * 3);

	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "Positions", dummy,
			dummy / (*n_particles));
	fread(buffer_float, (size_t) (*n_particles), 3 * sizeof(float), f);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	if (check_corrupt)
	{
		for (i = 0; i < (*n_particles); ++i)
		{
			ret = checkParticlePosition(&(buffer_float[i * 3 + 0]));
			if (ret != PART_OK)
				error(__FFL__,
						"Detected file corruption: invalid position (status %d) in particle %d: %.4e %.4e %.4e, file %s.\n",
						ret, i, buffer_float[i * 3 + 0], buffer_float[i * 3 + 1],
						buffer_float[i * 3 + 2], filename);
		}
	}

	for (i = 0; i < (*n_particles); ++i)
	{
		(*particles)[i].x[0] = buffer_float[i * 3 + 0];
		(*particles)[i].x[1] = buffer_float[i * 3 + 1];
		(*particles)[i].x[2] = buffer_float[i * 3 + 2];
	}

	/*
	 * Read the particle velocities.
	 */
	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "Velocities", dummy,
			dummy / (*n_particles));
	fread(buffer_float, (size_t) (*n_particles), 3 * sizeof(float), f);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	if (check_corrupt)
	{
		for (i = 0; i < (*n_particles); ++i)
		{
			ret = checkParticleVelocity(&(buffer_float[i * 3 + 0]));
			if (ret != PART_OK)
				error(__FFL__,
						"Detected file corruption: invalid velocity (status %d) in particle %d: %.4e %.4e %.4e, file %s.\n",
						ret, i, buffer_float[i * 3 + 0], buffer_float[i * 3 + 1],
						buffer_float[i * 3 + 2], filename);
		}
	}

	for (i = 0; i < (*n_particles); ++i)
	{
		(*particles)[i].v[0] = buffer_float[i * 3 + 0] * root_a;
		(*particles)[i].v[1] = buffer_float[i * 3 + 1] * root_a;
		(*particles)[i].v[2] = buffer_float[i * 3 + 2] * root_a;
	}

	memFree(__FFL__, MID_SNAPREADBUF, buffer_float, sizeof(float) * (*n_particles) * 3);

	/*
	 * Read the particle IDs. Depending on the file format, the IDs may be ints or longs.
	 */
	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "IDs", dummy,
			dummy / (*n_particles));
	if (dummy == (*n_particles) * 4)
	{
		buffer_int = (int *) memAlloc(__FFL__, MID_SNAPREADBUF, sizeof(int) * (*n_particles));
		fread(buffer_int, (size_t) (*n_particles), sizeof(int), f);
		for (i = 0; i < (*n_particles); ++i)
			(*particles)[i].id = (HaloID) buffer_int[i];
		memFree(__FFL__, MID_SNAPREADBUF, buffer_int, sizeof(int) * (*n_particles));

	} else if (dummy == (*n_particles) * 8)
	{
		buffer_long = (long int *) memAlloc(__FFL__, MID_SNAPREADBUF,
				sizeof(long int) * (*n_particles));
		fread(buffer_long, (size_t) (*n_particles), sizeof(long int), f);
		for (i = 0; i < (*n_particles); ++i)
			(*particles)[i].id = buffer_long[i];
		memFree(__FFL__, MID_SNAPREADBUF, buffer_long, sizeof(long int) * (*n_particles));

	} else
	{
		error(__FFL__,
				"[%4d] Size of ID block in LGadget file corresponds neither to int nor long (%d, %d).\n",
				proc, *n_particles, dummy);
	}
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	if (check_corrupt)
	{
		for (i = 0; i < (*n_particles); ++i)
		{
			ret = checkParticleID((*particles)[i].id);
			if (ret != PART_OK)
				error(__FFL__,
						"Detected file corruption: invalid ID (status %d) in particle %d: %ld, file %s.\n",
						ret, i, (*particles)[i].id, filename);
		}
	}

	/*
	 * GDE part
	 */
	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "TidalTensorPS", dummy,
			dummy / (*n_particles));
	fseek(f, dummy, SEEK_CUR);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "DistortionTensorPS",
			dummy, dummy / (*n_particles));
	fseek(f, dummy, SEEK_CUR);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "FlowDeterminant", dummy,
			dummy / (*n_particles));
	fseek(f, dummy, SEEK_CUR);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "PhaseSpaceDensity", dummy,
			dummy / (*n_particles));
	fseek(f, dummy, SEEK_CUR);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "AnnihilationRadiation",
			dummy, dummy / (*n_particles));
	fseek(f, dummy, SEEK_CUR);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "StreamDensity", dummy,
			dummy / (*n_particles));
	fseek(f, dummy, SEEK_CUR);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "LastCaustic", dummy,
			dummy / (*n_particles));
	fseek(f, dummy, SEEK_CUR);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "SheetOrientation", dummy,
			dummy / (*n_particles));
	fseek(f, dummy, SEEK_CUR);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "InitDensity", dummy,
			dummy / (*n_particles));
	fseek(f, dummy, SEEK_CUR);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	fread(&dummy, sizeof(dummy), (size_t) 1, f);
	output(0, "Block size for %22s %10ld byte, %5d byte per particle\n", "CausticCounter", dummy,
			dummy / (*n_particles));
	buffer_int = (int*) memAlloc(__FFL__, MID_SNAPREADBUF, sizeof(int) * (*n_particles));
	fread(buffer_int, (size_t) (*n_particles), sizeof(int), f);
	//fseek(f, dummy, SEEK_CUR);
	fread(&dummy, sizeof(dummy), (size_t) 1, f);

	/*
	 * Output caustic counts
	 */
	FILE *fout;
	fout = fopen("caustic_counts.txt", "w");
	for (i = 0; i < (*n_particles); i++)
	{
		fprintf(fout, "%d\n", buffer_int[i]);
	}
	fclose(fout);

	memFree(__FFL__, MID_SNAPREADBUF, buffer_int, sizeof(int) * (*n_particles));

	fclose(f);
}

#endif
