/*************************************************************************************************
 *
 * Test suite for the Hilbert curve module.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tests_hilbert.h"
#include "../src/global.h"
#include "../src/lb/domain_sfc.h"

#if TESTS_DO_HILBERT

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void testHilbertCurve(int N)
{
	int i, j, k, hi;
	int coords[SFC_NDIM] =
		{0, 0, 0};
	FILE *f;

	f = fopen("hilbert_test.txt", "w");
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
		{
			for (k = 0; k < N; k++)
			{
				coords[0] = i;
				coords[1] = j;
				coords[2] = k;
				hi = hilbertIndex(coords);
				fprintf(f, "%4d %4d %4d %8d\n", i, j, k, hi);
			}
		}
	}
	delay(1000);
	fclose(f);
}

#endif
