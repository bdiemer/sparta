/*************************************************************************************************
 *
 * Test suite for the cosmology module.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "tests_cosmology.h"
#include "../src/global.h"
#include "../src/cosmology.h"
#include "../src/utils.h"

#if TESTS_DO_COSMOLOGY

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

int test_cosmology_growthFactor();
int test_cosmology_peakHeight();
int test_cosmology_interpolation_D();
int test_cosmology_interpolation_sigma();
int test_cosmology_sigmaInversion();
int test_cosmology_selfSimilar();

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void testCosmology()
{
	test_cosmology_growthFactor();
	test_cosmology_peakHeight();
	test_cosmology_interpolation_D();
	test_cosmology_interpolation_sigma();
	test_cosmology_sigmaInversion();
	test_cosmology_selfSimilar();
}

int test_cosmology_growthFactor()
{
	const double Z_TEST[4] =
		{-0.7, 0.0, 2.7, 28.0};
	const double D_TEST[4] =
		{1.30955757, 0.99999991, 0.35221912, 0.04536128};

	Cosmology cosmo;
	double D, diff;
	int i, test_result;

	test_result = 1;
	cosmo = getCosmology(COSMOLOGY_BOLSHOI);
	cosmo.interpolate = 0;
	for (i = 0; i < 4; i++)
	{
		D = growthFactor(&cosmo, Z_TEST[i]);
		diff = fabs(D_TEST[i] - D) / D_TEST[i];
		output(0, "test_cosmology_peakHeight: D_colossus = %.6f, D = %.ff, diff = %.2e\n",
				D_TEST[i], D, diff);
		test_result = test_result && (diff < 1E-3);
	}

	return test_result;
}

int test_cosmology_peakHeight()
{
	const double Z_TEST[4] =
		{-0.7, 0.0, 2.7, 28.0};
	const double M_TEST[3] =
		{1E7, 1E11, 1E15};
	const double NU_TEST[4][3] =
		{
			{0.20619288, 0.47594547, 2.42467479},
			{0.27002147, 0.62327805, 3.17525152},
			{0.76662915, 1.76957455, 9.01498818},
			{5.95268561, 13.7403085, 69.99915242}};

	int i, j, test_result;
	double nu, diff;
	Cosmology cosmo;

	cosmo = getCosmology(COSMOLOGY_BOLSHOI);
	test_result = 1;

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 3; j++)
		{
			nu = peakHeight(&cosmo, Z_TEST[i], M_TEST[j]);
			diff = fabs(NU_TEST[i][j] - nu) / NU_TEST[i][j];
			output(0,
					"test_cosmology_peakHeight: z %6.3f, M %.2e, nu_colossus = %9.6f, nu = %9.6f, diff = %.2e\n",
					Z_TEST[i], M_TEST[j], NU_TEST[i][j], nu, diff);
			test_result = test_result && (diff < 1E-3);
		}
	}

	return test_result;
}

int test_cosmology_interpolation_D()
{
	const int N_TEST = 1000;
	int i, test_result;
	double z[N_TEST], D1, D2, max_err, err, log_min, log_max;
	Cosmology cosmo;

	cosmo = getCosmology(COSMOLOGY_BOLSHOI);

	log_min = log10(1.0 + COSMO_TABLE_Z_MIN);
	log_max = log10(1.0 + COSMO_TABLE_Z_MAX);
	linearIndicesDouble(z, N_TEST, log_min, log_max);
	max_err = 0.0;
	for (i = 0; i < N_TEST; i++)
	{
		z[i] = pow(10, z[i]) - 1.0;
		cosmo.interpolate = 1;
		D1 = growthFactor(&cosmo, z[i]);
		cosmo.interpolate = 0;
		D2 = growthFactor(&cosmo, z[i]);
		err = fabs(D1 / D2 - 1.0);
		max_err = fmax(max_err, err);
	}
	freeCosmology(&cosmo);

	output(0, "test_cosmology_interpolation_D: Maximum error is %.2e.\n", max_err);
	test_result = (max_err < 1E-3);

	return test_result;
}

int test_cosmology_interpolation_sigma()
{
	const int N_TEST = 1000;
	int i, test_result;
	double logR, R, s1, s2, max_err, log_width, err;
	Cosmology cosmo;

	cosmo = getCosmology(COSMOLOGY_BOLSHOI);

	log_width = (log10(COSMO_TABLE_R_SIGMA_MAX) - log10(COSMO_TABLE_R_SIGMA_MIN)) / (N_TEST - 1);
	max_err = 0.0;
	for (i = 0; i < N_TEST; i++)
	{
		logR = log10(COSMO_TABLE_R_SIGMA_MIN) + i * log_width;
		R = pow(10, logR);
		cosmo.interpolate = 1;
		s1 = sigma(&cosmo, 0.0, R, 0);
		cosmo.interpolate = 0;
		s2 = sigma(&cosmo, 0.0, R, 0);
		err = fabs(s1 / s2 - 1.0);
		max_err = fmax(max_err, err);
	}
	freeCosmology(&cosmo);

	output(0, "test_cosmology_interpolation_sigma: Maximum error is %.2e.\n", max_err);
	test_result = (max_err < 1E-3);

	return test_result;
}

int test_cosmology_sigmaInversion()
{
	const double Z_TEST[4] =
		{-0.7, 0.0, 2.7, 28.0};
	const double M_TEST[3] =
		{1E7, 1E11, 1E15};

	int i, j, test_result;
	double nu, M, diff;
	Cosmology cosmo;

	cosmo = getCosmology(COSMOLOGY_BOLSHOI);
	test_result = 1;

	for (i = 0; i < 4; i++)
	{
		for (j = 0; j < 3; j++)
		{
			nu = peakHeight(&cosmo, Z_TEST[i], M_TEST[j]);
			M = massFromPeakHeight(&cosmo, Z_TEST[i], nu);
			diff = fabs(M - M_TEST[j]) / M_TEST[j];
			output(0, "test_cosmology_sigmaInversion: z %6.3f, M %.2e, diff = %.2e\n", Z_TEST[i],
					M_TEST[j], diff);
			test_result = test_result && (diff < 1E-6);
		}
	}

	return test_result;
}

int test_cosmology_selfSimilar()
{
	int i, j, test_result;
	float s;
	double t;
	Cosmology cosmo;

	test_result = 1;

	cosmo = getCosmology(COSMOLOGY_BOLSHOI);
	cosmo.ps_type = POWERSPECTRUM_TYPE_POWERLAW;
	cosmo.ps_filter = FILTER_TOPHAT;
	cosmo.ps_norm = -1.0;
	cosmo.ns = -2.0;
	resetCosmology(&cosmo);

	for (j = 0; j < 2; j++)
	{
		cosmo.interpolate = j;

		t = clock();
		for (i = 0; i < 10000; i++)
			s = sigma(&cosmo, 0.0, 0.1, 0);
		t = clock() - t;
		t /= 1000000.0;

		output(0, "sigma %.6f, took %7.4f s with interp %d\n", s, t, j);
	}

	return test_result;
}

#endif
