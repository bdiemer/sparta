/*************************************************************************************************
 *
 * Test string-definition conversion.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "tests_halodefinition.h"
#include "../src/global.h"
#include "../src/halos/halo_definitions.h"

#if TESTS_DO_HALODEFINITIONS

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

void testHaloDefinitions()
{
	int i0, i1, i2, i3, i4, i5, i6, n_subtypes, counter;
	HaloDefinition def, def2;
	char tmp_str[DEFAULT_HALO_DEF_STRLEN], tmp_str2[DEFAULT_HALO_DEF_STRLEN];

	initHaloDefinition(&def);

	counter = 0;
	for (i0 = 0; i0 < HDEF_Q_N; i0++)
	{
		def.quantity = i0;

		/*
		 * Note that this for loop stars at 1 because 0 is invalid.
		 */
		for (i1 = 1; i1 < HDEF_TYPE_N; i1++)
		{
			def.type = i1;

			/*
			 * Now we need to start being careful. Not all types exist for all quantities.
			 * Moreover, there are different numbers of subtypes for the different types.
			 */
			if ((def.quantity == HDEF_Q_VCIRC) && (def.type != HDEF_TYPE_VMAX))
				continue;

			n_subtypes = 0;
			switch (def.type)
			{
			case HDEF_TYPE_SO:
				n_subtypes = HDEF_SUBTYPE_SO_N;
				def.overdensity = 200.0;
				break;
			case HDEF_TYPE_SPLASHBACK:
				n_subtypes = HDEF_SUBTYPE_SP_N;
				def.percentile = 75.0;
				break;
			case HDEF_TYPE_FOF:
				n_subtypes = HDEF_SUBTYPE_FOF_N;
				def.linking_length = 0.28;
				break;
			case HDEF_TYPE_VMAX:
				n_subtypes = HDEF_SUBTYPE_VMAX_N;
				def.overdensity = 0.0;
				break;
			case HDEF_TYPE_ORBITING:
				n_subtypes = HDEF_SUBTYPE_ORB_N;
				def.percentile = 90.0;
				break;
			default:
				error(__FFL__, "Unknown type, %d.\n", def.type);
				break;
			}

			for (i2 = 0; i2 < n_subtypes; i2++)
			{
				def.subtype = i2;

				for (i3 = 0; i3 < HDEF_TIME_N; i3++)
				{
					def.time = i3;

					for (i4 = 0; i4 < HDEF_PTLSEL_N; i4++)
					{
						def.ptl_select = i4;

						for (i5 = 0; i5 < HDEF_ERR_N; i5++)
						{
							def.is_error = i5;

							for (i6 = 0; i6 < HDEF_SOURCE_N; i6++)
							{
								def.source = i6;

								haloDefinitionToString(def, tmp_str);
								output(0, "Testing definition %d %d %d %d %d %d %d   %s\n",
										def.quantity, def.type, def.subtype, def.time,
										def.ptl_select, def.source, def.is_error, tmp_str);
								def2 = stringToHaloDefinition(tmp_str, 0);
								haloDefinitionToString(def2, tmp_str2);
								if (compareHaloDefinitions(def, def2) != HDEF_DIFF_NONE)
								{
									error(__FFL__,
											"Disagreement in definitions %s and %s, float %.1f / %.1f.\n",
											tmp_str, tmp_str2, def.overdensity, def2.overdensity);
								}

								counter++;
							}
						}
					}
				}
			}
		}
	}

	output(0, "Tested %d halo definitions.\n", counter);
}

#endif
