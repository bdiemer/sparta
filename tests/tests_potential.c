/*************************************************************************************************
 *
 * Test suite for the tree potential module.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "tests_potential.h"
#include "../src/global.h"
#include "../src/memory.h"
#include "../src/tree_potential.h"

#if TESTS_DO_POTENTIAL

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void testRunPotential(int N, float L, float M, float force_res, float err_tol, int points_per_leaf);
void runPotential(int verbose, int N, float L, float M, float force_res, float err_tol,
		int points_per_leaf, float *err_rms, float *err_max, float *time);

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * Test cases for the error tolerance and points per leaf. We generate a random particle
 * distribution and compare the tree potential with direct summation.
 *
 * These tests seem to indicate that 0.1 and 10 are good values for error tolerancec and points
 * per leaf, though this will likely depend on the number of particles, the particle
 * configuration, and the type of computer.
 */
void testPotential()
{
	const int N = 10000;
	const float L = 1.0;
	const float M = 1.0;
	const float FORCE_RES = 0.2;
	const float ERR_TOL = 0.1;
	const int POINTS_PER_LEAF = 10;

	int test_ppl[9] =
		{2, 5, 10, 15, 20, 50, 100, 300, 1000};
	float test_tol[9] =
		{1E3, 1E2, 1E1, 1E0, 1E-1, 1E-2, 1E-3, 1E-4, 1E-5};

	int i;

	for (i = 0; i < 9; i++)
		testRunPotential(N, L, M, FORCE_RES, ERR_TOL, test_ppl[i]);
	for (i = 0; i < 9; i++)
		testRunPotential(N, L, M, FORCE_RES, test_tol[i], POINTS_PER_LEAF);
}

/*
 * A test function that outputs timing and error information given parameters
 */
void testRunPotential(int N, float L, float M, float force_res, float err_tol, int points_per_leaf)
{
	float err_rms, err_max, t;

	runPotential(0, N, L, M, force_res, err_tol, points_per_leaf, &err_rms, &err_max, &t);
	output(0, "N %6d err_tol %10.5f ppl %4d -- RMS err %.1e max err %.1e -- %.1e seconds\n", N,
			err_tol, points_per_leaf, err_rms, err_max, t);
}

/*
 * Set up a distribution of test points and compute their potential with the tree and direct
 * summation.
 */
void runPotential(int verbose, int N, float L, float M, float force_res, float err_tol,
		int points_per_leaf, float *err_rms, float *err_max, float *time)
{
	/*
	 * Reserve memory
	 */
	int i, d;
	float ratio, *phi_dir, *phi_tree, rms_err, max_err;
	double t;
	TreePotentialPoint *pot;

	pot = (TreePotentialPoint *) memAlloc(__FFL__, MID_TCRPTL, N * sizeof(TreePotentialPoint));
	phi_dir = (float *) memAlloc(__FFL__, MID_TCRPTL, N * sizeof(float));
	phi_tree = (float *) memAlloc(__FFL__, MID_TCRPTL, N * sizeof(float));

	/*
	 * Set up initial conditions
	 */
	for (i = 0; i < N; i++)
		for (d = 0; d < 3; d++)
			pot[i].x[d] = (float) rand() / (float) (RAND_MAX / L);

	/*
	 * Compute potentials
	 */
	computeDirectPotential(pot, N, M, force_res);
	for (i = 0; i < N; i++)
		phi_dir[i] = pot[i].pe;

	t = clock();
	computeTreePotential(pot, N, M, force_res, err_tol, points_per_leaf);
	t = clock() - t;
	t /= 1000000.0;

	for (i = 0; i < N; i++)
		phi_tree[i] = pot[i].pe;

	/*
	 * Print results
	 */
	rms_err = 0.0;
	max_err = 0.0;
	for (i = 0; i < N; i++)
	{
		if (phi_dir[i] != 0.0)
			ratio = phi_tree[i] / phi_dir[i] - 1.0;
		else
			ratio = 0.0;
		rms_err += ratio * ratio;
		max_err = fmax(max_err, fabs(ratio));
		if (verbose)
			output(0, "Direct %.6e tree %.6e diff %8.5f\n", phi_dir[i], phi_tree[i], ratio);
	}
	rms_err = sqrt(rms_err / N);
	if (verbose)
		output(0, "RMS error is %.4f\n", rms_err);

	/*
	 * Free memory
	 */
	memFree(__FFL__, MID_TCRPTL, pot, N * sizeof(TreePotentialPoint));
	memFree(__FFL__, MID_TCRPTL, phi_dir, N * sizeof(float));
	memFree(__FFL__, MID_TCRPTL, phi_tree, N * sizeof(float));

	*err_rms = rms_err;
	*err_max = max_err;
	*time = (float) t;
}

#endif
