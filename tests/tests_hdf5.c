/*************************************************************************************************
 *
 * Test string-definition conversion.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

#include "tests_halodefinition.h"
#include "../src/global.h"
#include "../src/memory.h"
#include "../src/halos/halo.h"
#include "../src/io/io_hdf5.h"

#if TESTS_DO_HDF5

/*************************************************************************************************
 * CONSTANTS
 *************************************************************************************************/

#define ACTION_TEST_COMPRESSION 0
#define ACTION_TEST_REOPENING 1
#define ACTION_TEST_OPENING_MANY 2

#define ACTION ACTION_TEST_OPENING_MANY

#define STEP_CREATE_FILE 0
#define STEP_REOPEN_FILE 1
#define STEP STEP_CREATE_FILE

#define DSET_SIZE 200

/*************************************************************************************************
 * FORWARD DECLARATIONS
 *************************************************************************************************/

void testCompression();
void createTestFile();
void reopenFile();
void testOpenMany();

/*************************************************************************************************
 * FUNCTIONS
 *************************************************************************************************/

/*
 * This test is a two-step process, and we must restart the code between the steps to simulate a
 * crash, that is, to make sure that the HDF5 buffer does not hold on to any data from the first
 * step.
 */
void testHDF5()
{
	switch (ACTION)
	{
	case ACTION_TEST_COMPRESSION:
		testCompression();
		break;
	case ACTION_TEST_REOPENING:
		switch (STEP)
		{
		case STEP_CREATE_FILE:
			createTestFile();
			break;
		case STEP_REOPEN_FILE:
			reopenFile();
			break;
		}
		break;
	case ACTION_TEST_OPENING_MANY:
		testOpenMany();
		break;
	}
}

/*
 * This routine tests issues with opening/closing HDF5 files. On a typical system (e.g. iOS), the
 * number of open files is limited to 254 (for some reason).
 *
 * One issue is that a file can remain open even if H5Fclose() was called on it, seemingly because
 * not all elements in the file (e.g., groups) were closed. This issue cannot be detected with
 * by checking the error return code in the file ID.
 */
void testOpenMany()
{
	int i, n_files, counter;
	struct dirent **files;
	hid_t file_id, grp_id;
	char tmp_str[200];
	const char path[200] = "moria_test";
	const int n_files_create = 500;

	output(0, "Creating %d files.\n", n_files_create);
	for (i = 0; i < n_files_create; i++)
	{
		sprintf(tmp_str, "%s/test_%03d.hdf5", path, i);
		file_id = H5Fcreate(tmp_str, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
		hdf5WriteAttributeInt(file_id, "counter", i);

		grp_id = H5Gcreate(file_id, "test_grp", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		hdf5WriteAttributeInt(grp_id, "counter2", i + 1);
		H5Gclose(grp_id);

		H5Fclose(file_id);
		hdf5CheckForError(__FFL__, file_id, "closing file");
	}

	output(0, "Scanning directory...\n");
	n_files = scandir(path, &files, 0, alphasort);
	output(0, "Found %d files, reading.\n", n_files);

	counter = 0;
	for (i = 0; i < n_files; i++)
	{
		if (files[i]->d_name[0] == '.')
			continue;
		counter++;
		sprintf(tmp_str, "%s/%s", path, files[i]->d_name);
		file_id = H5Fopen(tmp_str, H5F_ACC_RDONLY, H5P_DEFAULT);
		hdf5CheckForError(__FFL__, file_id, "opening file for reading");
		H5Fclose(file_id);
	}
}

hid_t hdf5DatasetFixedTest(hid_t loc_id, hid_t dsp, const char *name, int dtype,
		int compression_level)
{
	int i, ndims;
	hid_t params, dataset;

	ndims = H5Sget_simple_extent_ndims(dsp);
	hsize_t dsp_dims[ndims], dsp_max_dims[ndims], chunk_size[ndims];
	H5Sget_simple_extent_dims(dsp, dsp_dims, dsp_max_dims);

	for (i = 0; i < ndims; i++)
	{
		if (dsp_max_dims[i] > HDF5_DEFAULT_CHUNK_SIZE)
			chunk_size[i] = HDF5_DEFAULT_CHUNK_SIZE;
		else
			chunk_size[i] = dsp_max_dims[i];
	}

	params = H5Pcreate(H5P_DATASET_CREATE);
	H5Pset_chunk(params, ndims, chunk_size);
	H5Pset_deflate(params, compression_level);
	dataset = H5Dcreate(loc_id, name, hdf5DatatypeFromSparta(dtype), dsp, H5P_DEFAULT, params,
	H5P_DEFAULT);
	H5Pclose(params);

	return dataset;
}

void hdf5WriteDatasetTest(hid_t loc_id, const char *name, int dtype, int count, void *mem,
		int compression_level)
{
	hid_t dset, dsp, dtype_hdf5;

	dset = 0;
	dsp = hdf5Dataspace1DFixed(count);
	dtype_hdf5 = hdf5DatatypeFromSparta(dtype);
	dset = hdf5DatasetFixedTest(loc_id, dsp, name, dtype, compression_level);
	H5Dwrite(dset, dtype_hdf5, H5S_ALL, H5S_ALL, H5P_DEFAULT, mem);
	H5Dclose(dset);
	H5Sclose(dsp);
}

/*
 * Create a large array and save it to a file with different compression levels.
 */
void testCompression()
{
	const int N = 50000000;
	const int dtype = DTYPE_FLOAT;
	const int do_constant = 0;

	int i, j, *data_int;
	float size, size_0, *data_float;
	double t, t_0;
	void *data;
	char tmpstr[100];
	hid_t id_file;
	clock_t t1, t2;
	struct stat file_info;

	/*
	 * Prepare data array
	 */
	switch (dtype)
	{
	case DTYPE_INT32:
		data_int = (int*) memAlloc(__FFL__, MID_IGNORE, sizeof(int) * N);
		for (i = 0; i < N; i++)
		{
			if (do_constant)
				data_int[i] = -1;
			else
				data_int[i] = i;
		}
		data = (void*) data_int;
		break;
	case DTYPE_FLOAT:
		data_float = (float*) memAlloc(__FFL__, MID_IGNORE, sizeof(float) * N);
		for (i = 0; i < N; i++)
		{
			if (do_constant)
				data_float[i] = -1.0;
			else
				data_float[i] = (float) i;
		}
		data = (void*) data_float;
		break;
	default:
		error(__FFL__, "Unknown data type.\n");
	}

	output(0, "Array with %.1e elements, dtype %d, constant %d.\n", (float) N, dtype, do_constant);
	for (j = 0; j < 10; j++)
	{
		sprintf(tmpstr, "test_comp_%d.hdf5", j);
		id_file = H5Fcreate(tmpstr, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
		t1 = clock();
		hdf5WriteDatasetTest(id_file, "test_dset", dtype, N, data, j);
		t2 = clock();
		H5Fclose(id_file);

		t = ((double) (t2 - t1)) / CLOCKS_PER_SEC;
		if (j == 0)
			t_0 = t;

		stat(tmpstr, &file_info);
		size = ((float) file_info.st_size) / 1024.0 / 1024.0;
		if (j == 0)
			size_0 = size;

		output(0, "Comp level %d, time %6.3f s, rel %6.2f, file size %7.2f MB, rel %5.1f%%\n", j, t,
				t / t_0, size, size / size_0 * 100.0);
	}

	switch (dtype)
	{
	case DTYPE_INT32:
		memFree(__FFL__, MID_IGNORE, data_int, sizeof(int) * N);
		break;
	case DTYPE_FLOAT:
		memFree(__FFL__, MID_IGNORE, data_float, sizeof(float) * N);
		break;
	default:
		error(__FFL__, "Unknown data type.\n");
	}
}

void reopenFile()
{
	hid_t id_file, id_grp, id_dset_var;
	float attr;
	int i, tmp_dim, *data, data_write[DSET_SIZE];

	// Copy file
	system("cp test.hdf5 test2.hdf5");

	// Open file in rw mode
	id_file = H5Fopen("test2.hdf5", H5F_ACC_RDWR, H5P_DEFAULT);

	// Read previous data
	id_grp = H5Gopen(id_file, "test_group", H5P_DEFAULT);
	hdf5ReadAttributeFloat(id_grp, "test_attr", &attr);
	hdf5ReadDataset(id_grp, "test_dset", DTYPE_INT32, &tmp_dim, (void*) &data, MID_IGNORE);

	// Output read data
	output(0, "Found dset with size %d\n", tmp_dim);
	memFree(__FFL__, MID_IGNORE, data, sizeof(int) * tmp_dim);

	hdf5ReadDataset(id_grp, "test_dset_var", DTYPE_INT32, &tmp_dim, (void*) &data, MID_IGNORE);
	output(0, "Found variable dset with size %d\n", tmp_dim);
	memFree(__FFL__, MID_IGNORE, data, sizeof(int) * tmp_dim);

	// Add new data
	hdf5WriteAttributeFloat(id_grp, "test_attr2", 6.2);
	hdf5WriteDataset(id_grp, "test_dset2", DTYPE_INT32, DSET_SIZE - 2, (void*) data);

	// Add new variable data
	for (i = 0; i < DSET_SIZE; i++)
		data_write[i] = i * 3;

	id_dset_var = H5Dopen(id_grp, "test_dset_var", H5P_DEFAULT);
	hdf5Dataset1DResize(id_dset_var, DSET_SIZE * 3);
	hdf5WriteStructMemberInc(id_dset_var, DSET_SIZE * 2, data_write, DSET_SIZE, sizeof(int), 0,
			DTYPE_INT32);

	// Close file properly
	H5Gclose(id_grp);
	H5Fclose(id_file);

	output(0, "Successfully opened and appended the test file.\n");
}

void createTestFile()
{
	hid_t id_file, id_grp, id_dsp, id_dset_var;
	int i, data[DSET_SIZE];

	// Create file
	id_file = H5Fcreate("test.hdf5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

	// Write group and dataset
	id_grp = H5Gcreate(id_file, "test_group", H5P_DEFAULT, H5P_DEFAULT,
	H5P_DEFAULT);
	hdf5WriteAttributeFloat(id_grp, "test_attr", 4.1);
	for (i = 0; i < DSET_SIZE; i++)
		data[i] = i * 2;
	hdf5WriteDataset(id_grp, "test_dset", DTYPE_INT32, DSET_SIZE, (void*) data);

	// Create dset with variable dimensions
	id_dsp = hdf5Dataspace1DVar(100, H5S_UNLIMITED);
	id_dset_var = hdf5Dataset1DVar(id_grp, id_dsp, "test_dset_var", DTYPE_INT32, 100);

	hdf5Dataset1DResize(id_dset_var, DSET_SIZE);
	hdf5WriteStructMemberInc(id_dset_var, 0, data, DSET_SIZE, sizeof(int), 0, DTYPE_INT32);

	hdf5Dataset1DResize(id_dset_var, DSET_SIZE * 2);
	hdf5WriteStructMemberInc(id_dset_var, DSET_SIZE, data, DSET_SIZE, sizeof(int), 0, DTYPE_INT32);

	// Flush HDF5 to create stable file state
	H5Fflush(id_file, H5F_SCOPE_GLOBAL);
}

#endif
