###############################################################
#
# Central makefile for SPARTA
#
# This file is designed to be included in a local makefile for
# a specific code setup. See build/Makefile for an example.
#
# (c) Benedikt Diemer
#
###############################################################

###############################################################
# PROCESS USER SETTINGS
###############################################################

# The platform variable is set in one of three ways:
# 1) if the user has specified it in a Makefile that includes
#    this file
# 2) if it is set as the environment variable PLATFORM
# 3) if neither is set, it defaults to "default", a platform
#    for which no additional settings are added.

ifeq ($(PLTFRM),)
PLTFRM=$(PLATFORM)
ifeq ($(PLTFRM),)
PLTFRM := default
endif
endif

# The SPARTA code directory must be set by the user, or we 
# assume that we are in the standard build directory.

ifeq ($(SPARTA_DIR),)
SPARTA_DIR := ..
endif

$(info Building SPARTA on platform "$(PLTFRM)" in path "$(SPARTA_DIR)")

###############################################################
# GENERAL SETTINGS
###############################################################

CC = mpicc
CFLAGS += -Wall -O2

###############################################################
# MACHINE-DEPENDENT PATHS AND OPTIONS
###############################################################

ifeq ($(PLTFRM),default)
CFLAGS +=
LD_FLAGS +=
endif

ifeq ($(PLTFRM),osx)
CFLAGS += -Wno-format-overflow
LD_FLAGS += -L/opt/local/lib
endif

ifeq ($(PLTFRM),midway)
CFLAGS += -Wno-comment
endif

ifeq ($(PLTFRM),odyssey)
CFLAGS += 
LD_FLAGS += 
endif

ifeq ($(PLTFRM),cori)
CC = cc
CFLAGS += -I/opt/cray/pe/hdf5/1.10.2.0/include -I/global/common/sw/cray/cnl7/haswell/gsl/2.5/intel/19.0.3.199/7twqxxq/include
LD_FLAGS += -L/global/common/sw/cray/cnl7/haswell/gsl/2.5/intel/19.0.3.199/7twqxxq/lib -lgsl -lgslcblas
endif

ifeq ($(PLTFRM),deepthought2)
CFLAGS += -I$(GSL_INC) -I$(HDF5INC) -Wno-format-overflow -Wno-restrict
LD_FLAGS += -L$(PWD) -L$(GSL_LIB) -Wl,-rpath,$GSL_LIB -L$(HDF5LIB) -lz
endif

ifeq ($(PLTFRM),umdastro)
CFLAGS += -I/home/diemer/code/gsl/include
LD_FLAGS += -L/home/diemer/code/gsl/lib
endif

###############################################################
# BUILD
###############################################################

EXEC_SPARTA = sparta
EXEC_MORIA = moria
EXEC_TESTS = tests

MAIN_SPARTA = $(SPARTA_DIR)/src/sparta.o
MAIN_MORIA = $(SPARTA_DIR)/tools/moria/moria.o
MAIN_TESTS = $(SPARTA_DIR)/tests/tests.o

LIBS = -lm -lgsl -lgslcblas -lhdf5

.PHONY: default all clean

default: $(EXEC_SPARTA)
all: $(EXEC_SPARTA) $(EXEC_MORIA) $(EXEC_TESTS)

OBJECTS = $(patsubst $(SPARTA_DIR)/src/%.c, $(SPARTA_DIR)/src/%.o, $(filter-out $(SPARTA_DIR)/src/sparta.c, $(wildcard $(SPARTA_DIR)/src/*.c)))
OBJECTS += $(patsubst $(SPARTA_DIR)/src/analyses/%.c, $(SPARTA_DIR)/src/analyses/%.o, $(wildcard $(SPARTA_DIR)/src/analyses/*.c))
OBJECTS += $(patsubst $(SPARTA_DIR)/src/halos/%.c, $(SPARTA_DIR)/src/halos/%.o, $(wildcard $(SPARTA_DIR)/src/halos/*.c))
OBJECTS += $(patsubst $(SPARTA_DIR)/src/io/%.c, $(SPARTA_DIR)/src/io/%.o, $(wildcard $(SPARTA_DIR)/src/io/*.c))
OBJECTS += $(patsubst $(SPARTA_DIR)/src/lb/%.c, $(SPARTA_DIR)/src/lb/%.o, $(wildcard $(SPARTA_DIR)/src/lb/*.c))
OBJECTS += $(patsubst $(SPARTA_DIR)/src/results/%.c, $(SPARTA_DIR)/src/results/%.o, $(wildcard $(SPARTA_DIR)/src/results/*.c))
OBJECTS += $(patsubst $(SPARTA_DIR)/src/tracers/%.c, $(SPARTA_DIR)/src/tracers/%.o, $(wildcard $(SPARTA_DIR)/src/tracers/*.c))

OBJECTS_TOOLS = $(patsubst $(SPARTA_DIR)/tools/models/%.c, $(SPARTA_DIR)/tools/models/%.o, $(wildcard $(SPARTA_DIR)/tools/models/*.c))
OBJECTS_TOOLS += $(patsubst $(SPARTA_DIR)/tools/moria/%.c, $(SPARTA_DIR)/tools/moria/%.o, $(filter-out $(SPARTA_DIR)/tools/moria/moria.c, $(wildcard $(SPARTA_DIR)/tools/moria/*.c)))

OBJECTS_TESTS += $(patsubst $(SPARTA_DIR)/tests/%.c, $(SPARTA_DIR)/tests/%.o, $(filter-out $(SPARTA_DIR)/tests/tests.c, $(wildcard $(SPARTA_DIR)/tests/*.c)))

# Note that we are adding the headers in the user's build directory. 
# We do NOT link to the example headers in /build.
HEADERS = $(wildcard *.h)
HEADERS += $(wildcard $(SPARTA_DIR)/src/*.h)
HEADERS += $(wildcard $(SPARTA_DIR)/src/analyses/*.h)
HEADERS += $(wildcard $(SPARTA_DIR)/src/halos/*.h)
HEADERS += $(wildcard $(SPARTA_DIR)/src/io/*.h)
HEADERS += $(wildcard $(SPARTA_DIR)/src/lb/*.h)
HEADERS += $(wildcard $(SPARTA_DIR)/src/results/*.h)
HEADERS += $(wildcard $(SPARTA_DIR)/src/tracers/*.h)

HEADERS += $(wildcard $(SPARTA_DIR)/tools/models/*.h)
HEADERS += $(wildcard $(SPARTA_DIR)/tools/moria/*.h)

HEADERS += $(wildcard $(SPARTA_DIR)/tests/*.h)

%.o: %.c $(HEADERS)
	$(CC) $(CFLAGS) -c $< -o $@

$(EXEC_SPARTA): $(OBJECTS) $(MAIN_SPARTA)
	$(CC) $(OBJECTS) $(MAIN_SPARTA) $(LD_FLAGS) $(LIBS) -o $@

$(EXEC_MORIA): $(OBJECTS) $(OBJECTS_TOOLS) $(MAIN_MORIA) 
	$(CC) $(OBJECTS) $(OBJECTS_TOOLS) $(MAIN_MORIA) $(LD_FLAGS) $(LIBS) -o $@ 

$(EXEC_TESTS): $(OBJECTS) $(OBJECTS_TESTS) $(OBJECTS_TOOLS) $(MAIN_TESTS) 
	$(CC) $(OBJECTS) $(OBJECTS_TESTS) $(OBJECTS_TOOLS) $(MAIN_TESTS) $(LD_FLAGS) $(LIBS) -o $@ 

###############################################################
# CLEAN
###############################################################

clean:
	-rm -f $(SPARTA_DIR)/src/*.o
	-rm -f $(SPARTA_DIR)/src/analyses/*.o
	-rm -f $(SPARTA_DIR)/src/halos/*.o
	-rm -f $(SPARTA_DIR)/src/io/*.o
	-rm -f $(SPARTA_DIR)/src/lb/*.o
	-rm -f $(SPARTA_DIR)/src/results/*.o
	-rm -f $(SPARTA_DIR)/src/tracers/*.o
	-rm -f $(SPARTA_DIR)/tools/models/*.o
	-rm -f $(SPARTA_DIR)/tools/moria/*.o
	-rm -f $(SPARTA_DIR)/tests/*.o
	-rm -f $(EXEC_SPARTA)
	-rm -f $(EXEC_MORIA)
	-rm -f $(EXEC_TESTS)
