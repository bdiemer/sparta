/*************************************************************************************************
 *
 * This file contains all user-defined compile-time settings for the SPARTA test suite.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

/*
 * Perform basic tests of the cosmology module
 */
#define TESTS_DO_COSMOLOGY 0

/*
 * Check the halo definition system that translates strings into definitions and vice versa.
 */
#define TESTS_DO_HALODEFINITIONS 0

/*
 * Test reading a Gadget file that was created with the geodisic equation extension (GDE) of
 * Mark Vogelsberger. This test will not run on any system.
 */
#define TESTS_DO_GDE 0

/*
 * Output the coordinates on a Hilbert curve to test the domain decomposition system. The Hilbert
 * curve will distinguish TESTS_HILBERT_N coordinates per dimension.
 */
#define TESTS_DO_HILBERT 0
#define TESTS_HILBERT_N 32

/*
 * Test the tree potential unit against direct summation.
 */
#define TESTS_DO_POTENTIAL 0

/*
 * Test HDF5 operations
 */
#define TESTS_DO_HDF5 0
