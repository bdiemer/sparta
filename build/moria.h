/*************************************************************************************************
 *
 * The MORIA tool writes SPARTA output back into catalog files. This file contains all
 * user-defined compile-time settings used in MORIA.
 *
 * (c) Benedikt Diemer
 *
 *************************************************************************************************/

/*
 * Perform internal self-checks. This makes the code slower but is useful for debugging.
 */
#define MORIA_CAREFUL 1

/*
 * If this number is not zero, print all output related to this halo. Useful for debugging.
 */
#define MORIA_DEBUG_HALO 0
